const translation_fi = {
  components: {
    slider: {
      button: 'Näytä kaikki',
    },
  },
  app: {
    seotitle: 'Äänikirjat ja e-kirjat – kuuntele ja lue ilmaiseksi kännykässä',
    seodescription:
      'Kokeile tuhansia äänikirjoja ja e-kirjoja ilmaiseksi 14 päivää! Lataa sovellus ja striimaa iPhonella, Androidilla tai iPadilla missä vain – Nextorylla.',
    notifications: {
      p1:
        'Maksutietosi on päivitetty ja tilauksesi jatkuu toistaiseksi. Kiitos, että jatkat tilaustasi.',
      p2: 'Tilaustasi muutetaan',
      p3: 'Alemmas siirto on keskeytetty.',
      p4: 'Tilisi on jälleen aktivoitu.',
      p5: 'Kampanjakoodi on rekisteröity.',
    },
    common: {
      silver: 'Hopea',
      silverplus: 'Hopea Plus',
      gold: 'Kulta',
      family: 'Perhe',
      familyplus: 'Perhe Plus',
      family2: 'Perhe 2',
      family3: 'Perhe 3',
      family4: 'Perhe 4',

      silver_capital: 'HOPEA',
      gold_capital: 'KULTA',
      family2_capital: 'PERHE 2',
      family3_capital: 'PERHE 3',
      family4_capital: 'PERHE 4',
    },
  },
  cookiebanner: {
    p1:
      'Nextory.fi käyttää evästeitä, jotta sivuston käyttö olisi mahdollisimman sujuvaa. Jatkamalla verkkosivustomme käyttöä hyväksyt evästeiden käytön.',
    link: 'Lue lisää.',
    button: 'Ymmärrän.',
  },
  menu: {
    logo_alttext: 'Nextory logo',
    navigation: {
      ebooks: 'E-kirjat',
      soundbooks: 'Äänikirjat',
      help: 'Asiakaspalvelu',
    },
    buttons: {
      try_free: 'Kokeile 14 päivää ilmaiseksi',
      login: 'Kirjaudu',
      logout: 'Kirjaudu ulos',
      complete_registration: 'Saata rekisteröityminen loppuun',
      listen_to_the_phone: 'Kuuntele kännykällä',
      enable_nextory_now: 'Aktivoi Nextory nyt',
      my_account: 'Minun tilini',
    },
  },
  views: {
    cookies: {
      heading: 'Evästeistä',
      paragraph:
        'Nextory käyttää evästeitä, kun käyt verkkosivustollamme. Eväste on informaatiotiedosto, joka tallennetaan käyttäjän tietokoneelle. Eväste ei voi tunnistaa sinua henkilökohtaisesti, vaan vain sen selaimen, joka on asennettuna tietokoneellesi ja jota käytät verkkosivustollamme käydessäsi. Ellet halua, että tietokoneellesi tallennetaan evästeitä, voit estää sen selaimesi asetuksilla. Eväste tallennetaan ajaksi, jona olet kytkettynä palveluun, tai ajaksi, jona tietoja tarvitaan sellaisen velvoituksen tai oikeuden täyttämiseksi, joka liittyy palveluun.',
    },
  },

  /**************** NEW HOMEPAGE ******************/
  /* New HomePage Menu Section */
  newhomepage: {
    menu: {
      navigation: {
        ebooks: 'E-kirjat',
        soundbooks: 'Äänikirjat',
        help: 'Asiakaspalvelu',
        my_account: 'Minun tilini',
        login: 'Kirjaudu',
        logout: 'Kirjaudu ulos',
      },
      buttons: {
        try_free: 'Kokeile 14 päivää ilmaiseksi.',
        listen_to_the_phone: 'Kuuntele kännykällä',
        complete_registration: 'Saata rekisteröityminen loppuun',
        enable_nextory_now: 'Aktivoi Nextory nyt',
        therefore_nextory: 'Siksi Nextory',
      },
    },

    /* New HomePage Hero Section */
    hero: {
      text: 'Lue ja kuuntele missä haluat, milloin haluat.',
      subheading: 'Rajaton pääsy e-kirjoihin ja äänikirjoihin kännykästäsi tai tabletiltasi. Alkaen 13,99 €/kk',
      buttons: {
        try_free: 'Kokeile 14 päivää ilmaiseksi.',
        complete_registration: 'Saata rekisteröityminen loppuun',
        enable_nextory_now: 'Aktivoi Nextory nyt',
      },
      images_alt_text: {
        hejdasaker: 'BokomslagHejdåsaker',
        paxmaran: 'BokomslagPaxMaran',
        veckansvego: 'BokomslagVeckansvego',
        halsorev: 'BokomslagHälsorevolutionen',
        handboksuperhjaltar: 'BokomslagHandbokförsuperhjältar',
        omgivenavidioter: 'BokomslagOmgivenavidioter',
        traskkungensdotter: 'BokomslagTräskkungensdotter',
        glommig: 'BokomslagGlömmig',
        densistaparrish: 'BokomslagDensistaMrsParrish',
        foodpharmacy: 'Bokomslag',
        hjarnstark: 'BokomslagHjärnstark',
        sveasson: 'BokomslagSveasson',
        storstavallt: 'BokomslagStörstavallt',
        factfullnes: 'BokomslagFactfulnes',
        cover1793: 'Bokomslag1793',
        siggesally: 'BokomslagSiggeochSally',
        small_9789510426500: 'Aki Hintsa	Bonnier',
        small_9789510426791: 'Murra tunnelukkosi : työstä tunteet, toimi toisin	Bonnier',
        small_9789510432143: 'Veitola	Bonnier',
        small_9789510435823: 'Hyvän mielen iltasatuja	Bonnier',
        small_9789511317753: 'Faktojen maailma	Otava',
        small_9789511317838: 'Enkelten kosketus	Otava',
        small_9789511322504: 'Taiteilijan vaimo	Otava',
        small_9789511326236: 'Kahden perheen Ebba	Otava',
        small_9789511327134: 'Hän lupasi soittaa	Otava',
        small_9789511327141: 'Ennen kuolemaani	Otava',
        small_9789512412075: 'Suden hetki	Gummerus',
        small_9789513199708: 'En palaa takaisin koskaa, luulen	Bonnier',
        small_9789513199876: 'Ella ja kaverit konsertissa	Bonnier',
        small_9789520118051: 'Erään tapon tarina	Otava',
        small_9789520400002: 'Treenaa kotona 10, 20 tai 30 minuuttia	Bonnier',
        small_9789520400101: 'Ylös, ulos treenaamaan!	Bonnier',
      },
    },

    /* New HomePage iPhone Section */
    iphone: {
      heading: 'Lue tai kuuntele',
      paragraph1: {
        heading: 'Suosikkikirjailijasi',
        content: 'Kymmenientuhansien kirjojen joukosta löydät ne, joista pidät.',
      },
      paragraph2: {
        heading: 'Löydä oikea kirja',
        content: 'Löydät nopeasti hyvät kirjat top-listoilta tai kirjavinkeistä.',
      },
      paragraph3: {
        heading: 'Lopeta milloin haluat',
        content:
          'Tilauksella ei ole sitoutumisaikaa ja voit muuttaa tai lopettaa tilauksen milloin haluat.',
      },
    },

    /* New HomePage iPad Section */
    ipad: {
      text: 'Mitä luette tänä iltana?',
    },

    /* New HomePage Children Section */
    children: {
      text:
        'Nextorylla ei ikinä tarvitse kyllästyä lastenkirjaan. Valitse kirja iän tai tyylilajin mukaan. Perhetilauksella voitte luoda käyttäjiä pelkästään lastenkirjoja varten.',
      images_alt_text: {
        falletskattkartan: 'Bokomslag Fallet Skattkartan ',
        supercharlie: 'Bokomslag Super Charlie ',
        fiffigakroppen: 'Bokomslag Saga-sagor',
        slottsmysteriet: 'Bokomslag Slottsmysteriet',
        elefanten: 'Bokomslag Elefanten som så gärna ville somna ',
        mammamu: 'Bokomslag Mamma Mu åker bobb ',
        pellesvanslos: 'Bokomslag Pelle Svanslös ',
        paxmaran: 'Bokomslag Pax Maran ',
        lassemaja: 'Bokomslag Biografmysteriet ',
        samsigge: 'Bokomslag Sam och Sigge ',
        letaspoken: 'Bokomslag Ingrid och Ivar ',
        handboksuperhjaltar: 'Bokomslag Handbok för Superhjältar ',
        rum123: 'Bokomslag Rum 123',
        bajsboken: 'Bokomslag Bajsboken',
        vemsbyxor: 'Bokomslag Vems Byxor ',
        harbrandbild: 'Bokomslag Här kommer brandbilen ',
        small_9789510425138: 'Milja	Bonnier',
        small_9789510430453: 'Yökoulu ja kadonnut opettaja	bonnier',
        small_9789510432983: 'Emilia Kent	Bonnier',
        small_9789510434147: 'Tohtori Proktorin aika-amme	Bonnier',
        small_9789510434406: 'Hyppy Ajassa	Bonnier',
        small_9789510435823: 'Hyvän mielen iltasatuja	Bonnier',
        small_9789511288756: 'Ihana tallipoika	Otava',
        small_9789511326236: 'Kahden perheen Ebba	Otava',
        small_9789511331506: 'Haamuja ja hirviöitä: Seikkailuserkut 4	Otava',
        small_9789512408306: 'Seleesian näkijä	Gummerus',
        small_9789513197384: 'Siiri ja kumma Kasperi	Bonnier',
        small_9789513197421: 'Risto Räppääjä ja väärä Vincent	Bonnier',
        small_9789513199814: 'Vattenporin simpukka	Bonnier',
        small_9789513199876: 'Ella ja kaverit konsertissa	Bonnier',
        small_9789520400835: 'Maailman rikkain poika	Bonnier',
        small_9789520402952: 'Maresi	Bonnier',
      },
    },

    /* New HomePage Sub Section */
    subsection: {
      heading: 'Kirjat odottavat',
      currency: '€/kk',
      features: {
        owl: 'Lue ja kuuntele niin paljon kuin vain haluat',
        clock: 'Keskeytä, vaihda tai lopeta tilaus milloin vain',
        devices: 'Nextory toimii sekä kännykässä että tabletissa',
      },
      package1: {
        heading: 'Hopea',
        content: {
          p1: '14 päivää ilmaiseksi, sitten',
          p2: 'Uusimmat kirjat',
          p3: '1 käyttäjä',
        },
      },
      package2: {
        heading: 'Kulta',
        content: {
          p1: '14 päivää ilmaiseksi, sitten',
          p2: 'Koko valikoima',
          p3: '1 käyttäjä',
          p4: 'Suosituin',
        },
      },
      package3: {
        heading: 'Perhe',
        content: {
          p1: '14 päivää ilmaiseksi, sen jälkeen alkaen',
          p2: 'Koko valikoima',
          p3: '2-4 käyttäjää',
          p4: 'Suurin määrä tyytyväisiä asiakkaita',
        },
      },
    },

    /* New HomePage Ratings Section */
    ratings: {
      rating1: {
        text:
          'Jos pidät kirjojen lukemisesta tai kuuntelemisesta, voin suositella tätä sovellusta.',
        user: 'KeitaroM',
      },
      rating2: {
        text:
          'Minusta sovellus on helppokäyttöinen ja kirjojen ja äänikirjojen valikoima on suuri. Sovellus on hyvä kaikenikäisille. Sovellusta voi hyvin käyttää ilman wifiä.',
        user: 'Tindrad',
      },
      rating3: {
        text: 'Paras tekemäni päätös! Olen tosi tyytyväinen. :-)',
        user: 'Jenniebengtsson',
      },
      disclaimer: '5 499 App Storen käyttäjää antaa Nextorylle 4,5/5.',
    },

    /* New HomePage Footer Section */
    footer: {
      heading: 'Ota yhteyttä',
      links: {
        press: 'Media',
        apps: 'Sovellukset',
        om_nextory: 'Tietoa Nextorysta',
        giftcards: 'Lahjakortti',
        job: 'Työpaikat',
        questions_and_answers: 'Kysymyksiä ja vastauksia',
        campaign_code: 'Kampanjakoodi',
        sell_via_nextory: 'Myy Nextoryn kautta',
      },
      copyrights: {
        copyright_text: 'Copyright ©',
        nextory_ab: 'Nextory AB',
        terms: 'Jäsenehdot',
        cookies: 'Evästeet',
      },
    },

    /* New HomePage Seo Section */
    seo: {
      find_audio_books: {
        heading: 'Löydä äänikirjoja kuunneltaviksi',
        content:
          'Jos haluat kuunnella äänikirjoja suoraan iPhonella, Androidilla tai tabletilla, saat Nextoryn avulla käyttöösi laajan kirjaston kirjoja ja kirjailijoita. Saat käyttöösi rajattomasti suosituimpia eri tyylilajien teoksia, kuten dekkareita, romantiikkaa, fantasiaa ja äänikirjoja lapsille, suoraan kännykkääsi. Kokeile ilmaiseksi ja kuuntele sitten rajattomasti alkaen 13,99 €/kk.',
      },
      listen_to_audio: {
        heading: 'Kuuntele äänikirjaa joka tilanteessa',
        content: {
          p1:
            'Sinulle, joka olet kiinnostunut Nextorysta ja joka haluat nähdä enemmän kirjojamme, suosittelemme, että käyt verkkosivustollamme ja tutkit koko äänikirjavalikoimaamme. Sovelluksessamme on tuhansittain kirjoja kuunneltaviksi äänikirjojen ja e-kirjojen eri formaateissa.',
          p2:
            'Nextoryn ja kirjastosta lainaamisen ero on se, että Nextorylta saat tuhansia e-äänikirjoja käytettäviksi suoraan kännykkääsi. Kiinteän kuukausimaksun avulla voit kuunnella uusimpia uutuuksia missä vain, milloin vain. Nextoryssa lataat äänikirjat striimaamiseen sijaan laitteellesi niin, että voit lukea kaikkia kirjojasi myös offline-tilassa.',
          p3:
            'Vaikka et nyt olisikaan valmis maksamaan palvelusta, mutta olet kuitenkin kiinnostunut äänikirjojen lataamisesta, voit klikata painiketta "Kokeile 14 päivää ilmaiseksi" testataksesi kaikessa rauhassa.',
        },
      },
      ebooks_for_all: {
        heading: 'E-kirjoja kaikille',
        content:
          'Täältä löydät suuren digitaalisen e-kirjojen kirjaston kännykkääsi tai tablettiisi. Kirjat voit lukea Nextoryn sovelluksella suoraan iPhonella ja Android-puhelimella tai tabletilla. Nextoryn avulla saat rajattoman määrän lukemista, missä vain ja milloin vain, suoraan kännykkään.',
      },
      information_about_ebooks: {
        heading: 'Tietoa e-kirjoista',
        content: {
          p1:
            'E-kirja on digitaalinen kirja, jonka voi lukea suoraan mobiililaitteella, ilman että laitteen on oltava online-tilassa. Kirjojen lukeminen sovellusten avulla on tullut vuosi vuodelta yhä suositummaksi. Yksi selitys on, että älypuhelimia ja tabletteja käytetään yhä enemmän. Nextoryn avulla saat tuhansia kirjoja ja kirjailijoita koottuna yhteen paikkaan, ja siten ne välittömästi käyttöösi ilman, että sinun on mentävä kirjastoon lainaaamaan tai etsittävä verkosta. ',
          p2:
            'Kokeile lukea e-kirjojamme ilmaiseksi 14 päivän koejakson ajan. Lisätietoja valikoimastamme ja kaikki kirjamme löydät kunkin tyylilajin kohdalta tai klikkaamalla kirjoja ylempänä. Löydät sekä jännittäviä uutuuksia, suosittuja kirjailijoita, top-listoja että paljon eri tyylilajien kirjoja.',
        },
      },
    },
    /* New Summer HomePage Hero Section */
    summerHero: {
      text: 'Läs och lyssna var du vill. Avsluta när du vill.',
      subheading:
        'Obegränsad tillgång till e-böcker och ljudböcker i din mobil eller surfplatta. Från 13,99 €/kk.',
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
      },
      images_alt_text: {
        Book1: 'Husmoderns död',
        Book2: 'Hinsides väktare',
        Book3: 'Supergott och supersnabbt',
        Book4: 'Hälsorevolutionen',
        Book5: 'De vises sten',
        Book6: 'Alltid din dotter',
        Book7: 'Ett mörker mitt ibland oss',
        Book8: 'Omgiven av idioter',
        Book9: 'Syndabocken',
        Book10: 'Factfullnes ',
        Book11: 'Vildplockat',
        Book12: 'Den frusna elden',
        Book13: 'Konsten att hålla sig flytande',
        Book14: '12 livsregler',
        Book15: 'Hunger alma katsu',
        Book16: 'ödesryttarna',
      },
    },

    /* New Summer HomePage iPhone Section */
    summerIphone: {
      heading: 'Alltid en bra bok',
      paragraph1: {
        heading: 'När och var du vill',
        content: 'På stranden, under utlandsresan, i fikakön. Var du än befinner dig har du hela vårt sortiment med dig.',
      },
      paragraph2: {
        heading: 'Massor av böcker',
        content: 'Vårt utbud av ljudböcker och e-böcker kommer du aldrig att tröttna på. Både på svenska och engelska.',
      },
      paragraph3: {
        heading: 'Avsluta när du vill',
        content: 'Du har ingen bindningstid och kan byta eller avsluta abonnemanget när du vill.',
      },
    },

    /* New Summer HomePage iPad Section */
    summerIpad: {
      text: 'Njut av sommaren',
    },

    /* New Summer HomePage Children Section */
    summerChildren: {
      heading1: 'Offline-läge',
      text1: 'Ladda ner dina böcker inför resan så slipper du tänka på wifi.',
      heading2: 'Sommarens mat',
      text2: 'Ligg i hängmattan och hitta sommarens matinspiration. Botaniseras bland allt från smarriga sallader till mumsiga chokladbollar.',
      heading3: 'Läs i solen',
      text3: 'Sol eller moln? Glömt läsglasögonen? Mörk bakgrund för nattläsning? Inga problem, hos oss anpassar du skärmen efter dina behov.',
      heading4: 'Barnböcker',
      text4: 'Vi har böcker för barn i alla åldrar. Hos oss kan barnen dessutom ha sin egen profil anpassad för dem.',

      images_alt_text: {
        Book1: 'Sallader året runt',
        Book2: 'Hinsides väktare',
        Book3: 'Vego på 30 min',
        Book4: 'Pirater',
        Book5: 'Supergott och supersnabbt',
        Book6: 'Min bästis målvakten',
        Book7: 'Frukost hela dagen',
        Book8: 'De vises sten',
        Book9: 'Japansk grillning',
        Book10: 'ödesryttarna ',
        Book11: 'Det gröna skafferiet',
        Book12: 'Handbok för superhjältar 3',
        Book13: 'Baka med godis',
        Book14: 'Till alla killar',
        Book15: 'Chokladbollar',
        Book16: 'Bajsboken',
      },
    },

    /* New Summer HomePage Ratings Section */
    summerRatings: {
      rating1: {
        text: 'Jos haluat lukea tai kuunnella kirjaa, voin suositella tätä sovellusta.',
        user: 'Keitaro',
      },
      rating2: {
        text:
          'Mielestäni sovellus on helppokäyttöinen, siellä on laaja valikoima kirjoja ja äänikirjoja.Sovellus sopii kaikenikäisille.On hienoa käyttää sovellusta ilman wi-fiä.',
        user: 'Tindrad',
      },
      rating3: {
        text: 'Paras päätös olen tehnyt! On erittäin tyytyväinen:)',
        user: 'Jenniebengtsson',
      },
      disclaimer: 'App Storeen 7 413 asiakasta saa Nextory 4.5: sta viidestä',
    },
  },
  /**************** END NEW HOMEPAGE ******************/

  /**************** HOMEPAGE/B or OLD HOMEPAGE ******************/
  /* HomePage/b Hero Section */
  homepage: {
    seotitle: 'Äänikirjoja ja e-kirjoja – kuuntele ja lue ilmaiseksi kännykällä',
    seodescription:
      'Kokeile tuhansia äänikirjoja ja e-kirjoja ilmaiseksi 14 päivää! Lataa sovellus ja striimaa iPhonelle, Androidille tai iPadille missä vain – Nextorylla.',

    hero: {
      heading_part1: 'Lue ja kuuntele',
      heading_part2: '- missä vain, milloin vain.',
      subheading_part1: 'Rajaton määrä e-kirjoja ja äänikirjoja käytettävissäsi',
      subheading_part2: 'tabletilla tai kännykällä. Alkaen Euro/kuukausi.',
      buttons: {
        try_free: 'Kokeile 14 päivää ilmaiseksi',
        complete_registration: 'Saata rekisteröityminen päätökseen',
        enable_nextory_now: 'Aktivoi Nextory nyt',
      },
    },

    /* HomePage/b Tab Section */
    tabs: {
      books: {
        tabname: 'Niin monta kirjaa',
        heading: 'Tuhansia e-kirjoja ja äänikirjoja suosikkikirjailijoiltasi.',
        links: {
          popular_ebooks: 'Suosittuja e-kirjoja',
          popular_audiobooks: 'Suosittuja äänikirjoja',
          news: 'Uutuudet',
        },
        buttons: {
          try_free: 'Kokeile 14 päivää ilmaiseksi',
          complete_registration: 'Saata rekisteröityminen  päätökseen',
          enable_nextory_now: 'Aktivoi Nextory nyt',
          show_all: 'näytä kaikki',
        },
      },

      listen: {
        tabname: 'Lue ja kuuntele missä vain',
        heading: 'Keksi kirjavapaus – uusimmat kirjat suoraan taskuusi.',
        subheading: 'Rajaton määrä e-kirjoja ja äänikirjoja kännykkään ja tablettiin.',
        text: 'Sekä iOs että Android.',
        img: {
          alt_text1: 'Nexory sovellusikoni.',
          alt_text2: 'Nextory eri laitteille',
        },
        buttons: {
          try_free: 'Kokeile 14 päivää ilmaiseksi',
          complete_registration: 'EuroEuroEuro',
          enable_nextory_now: 'Aktivoi Nextory nyt',
        },
      },

      price: {
        tabname: 'Valitse hinta',
        heading: 'Valitse tilaus lukeaksesi ja kuunnellaksesi niin paljon kuin haluat Nextorylla.',
        silver: 'Hopea',
        gold: 'Kulta',
        family: 'Perhe',
        options: {
          option1: 'Hinta 14 ilmaisen päivän jälkeen',
          option2: 'Uusimmat kirjat',
          option3: 'Henkilöt, jotka voivat lukea ja kuunnella samalla tilauksella',
          option4: 'Lue ja kuuntele kännykälläsi tai tabletillasi',
          option5: 'Rajaton määrä e-kirjoja ja äänikirjoja',
          option6: 'Lopeta milloin vain',
        },
        prices: {
          silver: '€',
          gold: '€',
          family: '€',
        },
        buttons: {
          try_free: 'Kokeile 14 päivää ilmaiseksi',
          complete_registration: 'Saata rekisteröityminen päätökseen',
          enable_nextory_now: 'Aktivoi Nextory nyt',
        },
      },

      abort: {
        tabname: {
          p1: 'Ei velvoita mihinkään,',
          p2: 'Lopeta milloin haluat.',
        },
        heading: 'Tuntuuko, ettei Nextory sovi sinulle? Ei sitoutumisaikaa. Lopeta milloin haluat.',
        buttons: {
          try_free: 'Kokeile 14 päivää ilmaiseksi',
          complete_registration: 'Saata rekisteröityminen päätökseen',
          enable_nextory_now: 'Aktivoi Nextory nyt',
        },
        images_alt_text: 'Nextory eri laitteilla',
      },
    },

    /* HomePage/b Seo Section */
    seo: {
      find_audio_books: {
        heading: 'Löydä kuunneltavia äänikirjoja',
        content:
          'Jos haluat kuunnella äänikirjoja suoraan iPhonella, Androidilla tai tabletilla, saat Nextoryn avulla käyttöösi laajan kirjaston kirjoja ja kirjailijoita. Saat rajattomasti käyttöösi suosituimmat eri tyylilajien kirjat, kuten dekkarit, romantiikka, fantasy ja äänikirjoja lapsille, suoraan kännykkään. Kokeile ilmaiseksi ja kuuntele sitten rajattomasti alkaen 13,99€/kk.',
      },
      listen_to_audio: {
        heading: 'Kuuntele äänikirjaa joka tilanteessa',
        p1:
          'Sinulle, joka olet kiinnostunut Nextorysta ja joka haluat nähdä enemmän kirjojamme, suosittelemme, että käyt verkkosivustollamme ja tutkit koko äänikirjavalikoimaamme. Sovelluksessamme on tuhansittain kirjoja kuunneltaviksi äänikirjojen ja e-kirjojen eri formaateissa.',
        p2:
          'Nextoryn ja kirjastosta lainaamisen ero on se, että Nextorylta saat tuhansia e-äänikirjoja käytettäviksi suoraan kännykkääsi. Kiinteän kuukausimaksun avulla voit kuunnella uusimpia uutuuksia missä vain, milloin vain. Nextoryssa lataat äänikirjat striimaamiseen sijaan laitteellesi niin, että voit lukea kaikkia kirjojasi myös offline-tilassa.',
        p3:
          'Vaikka et nyt olisikaan valmis maksamaan palvelusta, mutta olet kuitenkin kiinnostunut äänikirjojen lataamisesta, voit klikata painiketta "Kokeile 14 päivää ilmaiseksi" testataksesi kaikessa rauhassa.',
      },
    },
    ebooks_for_all: {
      heading: 'E-kirjoja kaikille',
      content:
        'Täältä löydät laajan digitaalisen kirjaston e-kirjoja kännykkääsi tai tablettiisi. Kirjat voit lukea Nextoryn sovelluksella suoraan iPhonella tai Android-puhelimella tai tabletilla. Nextoryn avulla saat rajattoman määrän lukemista, missä vain ja milloin vain, suoraan kännykkääsi.',
    },
    information_about_ebooks: {
      heading: 'Tietoa e-kirjoista',
      content: {
        p1:
          'E-kirja on digitaalinen kirja, jonka voi lukea suoraan mobiililaitteella, ilman että laitteen on oltava online. Kirjojen lukeminen sovellusten avulla on tullut vuosi vuodelta yhä suositummaksi. Yksi selitys on, että älypuhelimia ja tabletteja käytetään yhä enemmän. Nextoryn avulla saat tuhansia kirjoja ja kirjailijoita koottuna yhteen paikkaan, ja siten ne välittömästi käyttöösi ilman, että sinun on lainattava kirjastosta tai etsittävä verkosta. ',
        p2:
          'Kokeile lukea e-kirjojamme ilmaiseksi 14 päivän koejakson ajan. Lisätietoja valikoimastamme ja kaikki kirjamme löydät kunkin lajin kohdalta tai klikkaamalla kirjoja ylempää. Löydät sekä jännittäviä uutuuksia, suosittuja kirjailijoita, top-listoja että paljon eri lajien kirjoja.',
      },
    },
  },

  footer: {
    heading: '',
    links: {
      press: 'Media',
      apps: 'Sovellukset',
      nextory_ab: 'Nextory AB',
      giftcards: 'Lahjakortti',
	integrity_terms: 'Yksityisyyden suoja',
      membership_terms: 'Jäsenehdot',
      job: 'Työpaikat',
      questions_and_answers: 'Kysymyksiä ja vastauksia',
      to_cookies: 'Evästeistä',
      campaign_code: 'Kampanjakoodi',
      sell_via_nextory: 'Myy Nextoryn kautta',
    },
    copyrights: {
      copyright_text: 'Copyright ©',
      address: {
        p1: 'Nextory AB  |  Organisaationumero: 556708-4149',
        p2: 'Dalagatan 7, 111 23 Stockholm  |  08-411 17 15',
      },
    },
  },
  /**************** END HOMEPAGE/B or OLD HOMEPAGE ******************/

  /**************** SEARCH PAGE ******************/
  /*Search Page*/
  search: {
    placeholder: 'Hae kirjaa...',
    type: 'Tyyppi',
    all: 'Kaikki',
    ebooks: 'E-kirjat',
    soundbooks: 'Äänikirjat',
    what_are: 'Mitä haet? Kirjoita yllä olevaan ruutuun.',
    loading_results: 'Lataa hakutuloksia',
    search_results: 'Hakutulokset',
    found_nothing_on: 'Ei löytänyt mitään:',
    searchform_error: 'Virhe',
  },
  /**************** END SEARCH PAGE ******************/

  /**************** EBOOKS PAGE ******************/
  ebooks: {
    heading: 'E-kirjat',
    books: {
      heading: 'Kirjat',
    },
    category: {
      seotitle: 'Lataa ilmainen äänikirja/e-kirja online',
      seodescription: 'Löydä suosikkisi verkon suurimmasta ääni- ja e-kirjojen valikoimasta!',
      messages: {
        error: 'Virhe',
        loading: 'Kirjoja ladataan',
      },
      listall: {
        seotitle: 'Lataa ilmaisia äänikirjoja/e-kirjoja online',
        seodescription: 'Löydä suosikkisi verkon suurimmasta ääni- ja e-kirjojen valikoimasta!',
      },
    },
    singlebook: {
      bookdetails: {
        read_as_ebook: 'Saatavilla e-kirjana',
        listen_as_audio: 'Saatavilla äänikirjana',
        buttons: {
          try_free: 'Kokeile kirjaa ilmaiseksi',
          listen_on_mobile: 'Kuuntele kirjaa kännykällä',
        },
        and: "ja",
        _Of: "Of ",
        more_info: 'Lisätietoja',
        narrator: 'Lukija',
        length: 'Pituus',
        of_the: 'osa',
        series: 'Sarja',
        type: 'Tyyppi',
        format: {
          ebook: 'E-kirja',
          audiobook: 'Äänikirja',
          ebook_and_audiobook: 'E-kirja, äänikirja',
        },
        included_in: 'Sisältyy',
        published: 'Julkaistu',
        translator: 'Kääntäjä',
        language: 'Kieli',
        isbn: 'ISBN',
        publishers: 'Kustantamo',
      },
      messages: {
        fetchposts: 'Virhe.',
      },
      all_books_in_the_series: 'Kaikki sarjan kirjat',
    },
    soundbooks: {
      heading: 'Äänikirjat',
    },
    toplist: {
      seotitle: 'Lataa ilmainen äänikirja/e-kirja online',
      seodescription: 'Löydä suosikkisi verkon suurimmasta ääni- ja e-kirjojen valikoimasta!',
      messages: {
        error: 'Virhe',
        loading: 'Lataa kirjoja',
      },
    },
    error: 'Virhe.',
  },
  /**************** END EBOOKS PAGE ******************/

  /**************** ACCOUNT PAGE ******************/
  account: {
    changepass: {
      newpassword: 'Valitse salasana',
      repeatpassword: 'Toista salasana',
      presubmit:
        'Linkissä, jonka olet saanut s-postitse meiltä, on virhe. Täältä voit lähettää uuden viestin salasanan uusimiseksi.',
    },
    changepayment: {
      notifyTrustlyFail:
        'Rekisteröityminen Mobliilin Pankki-ID:n avulla keskeytyi!  Koeta uudelleen. Ellei se onnistu, tarkista pankkitilisi. Muuten ota yhteyttä pankkiisi.',
      heading: 'Päivitä maksutietosi',
      listitem: 'Ei velvoita mihinkään, voit lopettaa tilauksen netissä milloin vain',
      paymenttitle: 'Valitse maksutapa',
      secure_server: 'Turvallinen palvelin (SSL-salattu)',
      credit_or_debit_card: 'Luotto- tai pankkikortti',
      mobile_bankid: 'Mobiili Pankki-ID',
    },
    changesubscription: {
      heading: 'Vaihda tilausta',
      subheading: 'Voit muuttaa tilaustasi milloin tahansa. Sinua veloitetaan uuden tilauksesi mukaisesti heti seuraavasta maksupäivästäsi alkaen.',
      currency: 'Euro',
      price_per_month: 'Hinta/kuukausi',
      prices: {
        silver: '€',
        gold: '€',
        family: '€',
      },
      list: {
        item1: 'Uusimmat kirjat',
        item2: 'Henkilöt, jotka voivat lukea ja kuunnella samalla tilauksella',
        item3: 'Lue ja kuuntele kännykällä tai tabletilla',
        item4: 'Rajaton määrä e-kirjoja ja äänikirjoja',
        item5: 'Lopeta milloin vain',
      },
      buttons: {
        save_current: 'Tallenna valittu tilaus',
        change_to: 'Vaihda tilaukseen:',
        change_to_b: 'Vaihda tilaukseen',
        currency_per_month: '€/kk',
      },
      content: {
        p1: 'Jos vaihdat Perheestä Kultaan, menetät perhetilauksen, jossa voi olla 4 profiilia.',
        p2: 'Jos vaihdat Perheestä Hopeaan, menetät perhetilauksen, jossa voi olla 4 profiilia sekä uusimmat kirjat.',
        p3: 'Klikkaa “Vaihda tilaukseen: Perhe” päivittääksesi tilauksesi Kullasta Perheeseen. Se tarkoittaa, että saat käyttöösi perhetilauksen, johon voit hankkia maksimissaan 4 profiilia.',
        p4: 'Jos vaihdat Kullasta Hopeaan, menetät uusimmat kirjat.',
        p5: 'Klikkaa ”Vaihda tilaukseen: Perhe” päivittääksesi tilauksesi Hopeasta Perheeseen. Se tarkoittaa, että saat käyttöösi perhetilauksen, johon voit hankkia maksimissaan 4 profiilia.',
        p6: 'Klikkaa Muuta Kullaksi päivittääksesi Hopeasta Kultaan. Se merkitsee, että saat käyttöösi uusimmat kirjat. Laskutamme 16,99 €/kk seuraavana maksupäivänä. Saat Kulta-tilin käyttöösi välittömästi.',
      },
    },
    close_account: {
      buttons: {
        change_subscription: 'Vaihda tilausta',
        cancel: 'Peruuta',
        end: 'Lopeta tilaus',
      },
      step1: {
        freemember: {
          heading: 'Juuri nyt kokeilet Nextorya ilmaiseksi!',
          subheading:
            'Huom! Jos lopetat, et enää voi käyttää Nextorya ja menetät loput ilmaiset käyttöpäiväsi.',
          p1:
            'Tiesitkö, että voit vaihtaa tilausta milloin vain? Esimerkiksi Hopea-tilauksemme maksaa vain 13,99 €/kk ja sisältää tuhansia kirjoja.',
        },
        member: {
          heading: 'Vinkkejä!',
          subheading: 'Meillä Nextorylla on erilaisia tilauksia, joita voit halutessasi kokeilla.',
          p1_p1:
            'Esimerkiksi Hopea-tilaus maksaa vain 13,99 €/kk ja tarjoaa tuhansia kirjoja. Haluatko vaihtaa Hopea-tilaukseen ja',
          p1_p2: 'maksaa vain 13,99 €/kk?',
        },
      },
      step2: {
        heading: 'Auta meitä tulemaan vieläkin paremmiksi!',
        subheading:
          'Haluamme aina tarjota asiakkaillemme parhaan lukukokemuksen. Mielipiteesi on meille tärkeä.',
        email_label: 'Miksi halusit lopettaa tilauksesi?',
        list: {
          option1: 'Valitse yksi vaihtoehto',
          option2: 'Minulla ei ole aikaa hyödyntää palvelua',
          option3: 'Palvelun hinta ei ole riittävän edullinen',
          option4: 'En löydä kirjoja, joita haluan lukea',
          option5: 'Sovellus ei toimi kuten sen pitäisi',
          option6: 'Olen valinnut toisen palvelun',
          option7: 'Muu',
        },
        labels: {
          label1: 'Mainitse yksi tai useampi kirja, joita et löytänyt',
          label2: 'Muu syy',
        },
        errors: {
          error1: 'Valitse yksi edellä mainituista',
          error2:
            'Virhe. Kokeile uudestaan. Jos virhe toistuu, ota yhteyttä asiakaspalveluun Euro',
        },
        buttonred_value: 'Lopeta tilaus',
        afterclose: {
          heading1: 'Tilauksesi on nyt lopetettu',
          heading2: 'Tilauksesi lopetetaan:',
          p1:
            'Kiitokset ajastasi Nextorylla. Toivottavasti palaat takaisin! Voit aina jatkaa tilaustasi käyttämällä vanhaa käyttäjätunnustasi ja salasanaasi osoitteessa www.nextory.fi',
          p2: 'Kirjat ovat edelleen käytettävissä',
          p2_b: 'asti.',
          button1: 'Jatka tilausta',
          button2: 'Minun tililleni',
        },
        uncancel_thankyou: {
            heading1: 'Tilauksesi irtisanominen on nyt peruttu.',
            p1: 'Hauskaa, että haluat jatkaa Nextoryn käyttämistä!',
            button1: 'OK',
        },
      },
    },
    forgotpassword: {
      send: 'Lähetä',
      Invlidlink: 'Virheellinen linkki!',
      InvlidlinkdESC: 'Tätä linkkiä käytetään jo muutettaessa salasanaa. Vaihda salasana käyttämällä uutta linkkiä. joka on Nextory lähettää sinulle rekisteröidyn sähköpostiosoitteesi.',
      Resend: 'Uudelleen lähetä',
      heading: 'Unohditko salasanan?',
      subheading: 'Kirjoita sähköpostiosoite, jota käytit rekisteröityessäsi, niin lähetämme sinulle uuden!',
      validate: {
        errors: {
          email: 'Täytä voimassa olea s-postiosoite',
        },
      },
      mailsent: {
        heading: 'Salasanan vaihtaminen',
        subheading: 'Olemme lähettäneet s-postiviestin, jossa on ohjeet salasanan vaihtamiseksi.',
      },
      forgotform: {
        email_label: 'S-postiosoite',
      },
    },
    loginpage: {
      heading: 'Kirjaudu',
      disclaimer: 'Uusi Nextorylla?',
      register_now: 'Rekisteröidy nyt.',
      validate: {
        errors: {
          email: 'Kirjoita voimassa oleva s-postiosoite',
          password: 'Kirjoita salasana',
          password_length: 'Salasanan on oltava 4-25v merkkiä',
          password_chars: 'Sallitut merkit: 0-9, a-z, A-Z sekä erikoismerkit: -, @, #, $, %, _',
        },
      },
      saga: {
        errors: {
          email_error1: 'Kirjautumisessa on ongelmia juuri nyt',
          email_error2: 'Tällä s-postiosoitteella ei löydy käyttäjää.',
          password: 'Salasana on väärä.',
        },
      },
      loginform: {
        email: 'S-postiosoite',
        password: 'Salasana',
        presubmit: 'Oletko unohtanut salasanan? Klikkaa tästä.',
        login: 'Kirjaudu',
      },
    },
    my_account: {
      accountform: {
        email: 'S-postiosoite',
        password: 'Salasana',
        firstname: 'Kirjoita etunimi',
        lastname: 'Kirjoita sukunimi',
        address: 'Kirjoita osoite',
        postalcode: 'Kirjoita postinumero',
        city: 'Kirjoita paikkakunta',
        phone: 'Kirjoita puhelinnumero',
        save_data: 'Tallenna tiedot',
      },
      collabs: {
        heading: 'Minun tarjoukseni',
        p1: 'Täältä löydät tarjoukset, jotka ovat vain sinulle, Nextoryn jäsenelle.',
      },
      headings: {
        my_details: 'Minun tietoni',
        account_has_ended: 'tili on lopetettu',
        enable_your_account_again: 'Aktivoi tili uudelleen',
        account_is_active_even: 'Tili on aktivoitu (Euro infoga datum här) asti',
        you_have_subscription: 'Sinulla on tilaus',
        people: 'henkilöä',
        end_subscriptions: 'Lopeta tilaus',
      },

      content: {
        p1: 'Seuraavana laskutuspäivänä',
        p2: 'tilauksesi muutetaan',
        p3: 'Tili on aktivoitu ( infoga datum här) asti',
        p4: 'Seuraava maksu tapahtuu',
        currency: '€',
        currency_per_month: '€/kk',
      },
      buttons: {
        cancel_downgrade: 'Keskeytä vaihtaminen',
        change_payment_details: 'Muuta maksutietoja',
        enable_your_account_again: 'Aktivoi tili uudelleen',
        change_subscription: 'Muuta tilausta',
        end_subscriptions: 'Lopeta tilaus',
        logout: 'Kirjaudu ulos',
      },
      orders: {
        order: 'Tilaus',
        currency: '€',
        order_history: 'Tilaustiedot',
      },
      validate: {
        errors: {
          email: 'Kirjoita voimassa oleva s-postiosoite',
          password: 'Salasanan on oltava 4-25 merkkiä',
          password_chars: 'Sallitut merkit: 0-9, a-z, A-Z sekä erikoismerkit: -, @, #, $, %, _',
          phone: 'Puhelinnumeron on oltava 7-12 numeroa',
        },
        warnings: {
          firstname: 'Kirjoita etunimi',
          lastname: 'Kirjoita sukunimi',
          address: 'Kirjoita osoite',
          postalcode: 'Kirjoita postinumero',
          city: 'Kirjoita paikkakunta',
          phone: 'Kirjoita puhelinnumero',
        },
      },
    },
    reactivate_payment: {
      notifytrustlyfail: {
        success:
          'Rekisteröityminen Mobiilin Pankki-ID:n avulla keskeytyi! Koeta uudelleen. Ellei se toimi, tarkista pankkitili. Muuten ota yhteyttä pankkiisi.',
      },
      heading: 'Kirjoita laskutustiedot aktivoidaksesi tilauksesi.',
      listitem: 'Ei velvoitteita, voit lopettaa online milloin vain.',
      paymenttitle: 'Valitse maksutapa',
      secure_server: 'Turvallinen palvelin (SSL-salattu)',
      credit_or_debit_card: 'Pankki- tai luottokortti',
      mobile_bankid: 'Mobiili Pankki-ID',
    },
    reactivate_subscription: {
      heading: 'Tervetuloa takaisin! Valitse tilaus, joka sopii sinulle parhaiten.',
      subheading: 'Voit vaihtaa tilausta milloin vain.',
      buttons: {
        continue: 'Jatka',
        go_back: 'Peruuta'
      },
    },
    receipt: {
      heading: 'Kuitti',
      subheading: 'Tietoa tilauksesta:',
      date: 'Päivämäärä',
      currency: '€',
      subsctiption: 'Tilaus',
      payment_method: 'Maksutapa',
      receiver: 'Vastaanottaja',
      vat_number: 'VAT-numero:',
      email: 'S-posti',
      vat: 'Alv.',
      total: 'Yhteensä',
      payment_methods: {
        credit_card: 'Luottokortti',
        trustly_direct: 'Trustly-suoramaksu',
        campaign_code: 'Kampanjakoodi',
        gift_cards: 'Lahjakortti',
      },
    },
    saga: {
      updateprofile_email_error: 'Tämä s-postiosoite on jo rekisteröity meillä.',
    },
  },
  /**************** END ACCOUNT PAGE ******************/

  /**************** FILTER DROPDOWN PAGE ******************/
  filter: {
    category: 'Luokka',
    all: 'Kaikki',
    sort_by: 'Lajittele',
    sort: {
      hotness: 'Suosituin',
      published: 'Julkaistu',
      title: 'Nimi',
    },
    type: {
      type: 'Tyyppi',
      ebook: 'E-kirja',
      audiobook: 'Äänikirja',
    },
  },
  /**************** END FILTER DROPDOWN PAGE ******************/

  /**************** GIFTCARD PAGE ******************/
  giftcard: {
    heading: 'Lahjakortti – äänikirjoja ja e-kirjoja',
    subheading:
      'Lahjakortti lähetetään suoraan sinulle s-postitse. Voit joko tulostaa lahjakortin ja antaa sen itse tai lähettää sen s-postitse vastaanottajalle.',
    redeem_wrapper: {
      heading: 'Lunasta lahjakortti',
      button: {
        not_logged_in: 'Uusi asiakas',
        logged_in: 'Jo asiakas',
      },
      active_redeem_form: {
        heading1: 'Olen uusi Nextorylla',
        heading2: 'Minulla on tili Nextorylla',
      },
    },
    validation: {
      firstname: 'Kirjoita etunimi',
      lastname: 'Kirjoita sukunimi',
      giftvoucher: 'Kirjoita lahjakoodi',
      email: 'Kirjoita voimassa oleva s-postiosoite',
      repeatemail: 'S-postiosoitteet eivät täsmää',
      password: 'Kirjoita salasana',
      password_length: 'Salasanan on oltava 4-25 merkkiä',
      password_chars: 'Sallitut merkit: 0-9, a-z, A-Z sekä erikoismerkit: -, @, #, $, %, _',
    },
    saga: {
      giftvoucher_error: 'Koodi ei ole voimassa.',
      password_error: 'Salasana ei täsmää',
      email_error: 'Tällä sähköpostiosoitteella ei ole tiliä.',
    },
    redeem_giftcard_success: {
      heading: 'Tervetuloa Nextorylle',
      subheading: 'Lahjakorttisi on nyt lunastettu ja voit Nextorya ( infoga datum här) asti',
      content:
        'Lataa Nextory-sovellus puhelimeesi tai tablettiisi ja saat käyttöösi kirjat, missä vain.',
    },
    gift_reguserform: {
      giftvoucher: 'Kirjoita lahjakoodi',
      email: 'S-postiosoite',
      repeatemail: 'Toista s-postiosoite',
      password: 'Salasana',
      submit: 'Lunasta lahjakortti',
    },
    gift_card_success: {
      heading: 'Kiitos ostosta',
      subheading:
        'Lahjakortti on lähetetty s-postiosoitteeseesi. Voit tulostaa sen tai antaa sen digitaalisesti. Olet myös saanut varmistuksen ostosta s-postitse.',
      p1: 'Tilausnumerosi on:',
      p2:
        'Lataa Nextory-sovellus puhelimeesi tai tablettiisi ja saat käyttöösi kirjat, missä vain.',
    },
    gift_buyform: {
      one_month: '1 kuukausi',
      three_month: '3 kuukautta',
      six_month: '6 kuukautta',
      price1: 'Hinta: €',
      price2: 'Hinta: €',
      price3: 'Hinta: €',
      labels: {
        firstname: 'Etunimi',
        lastname: 'Sukunimi',
        email: 'S-postiosoite',
      },
      buttons: {
        continue: 'Jatka',
      },
    },
    subscriptionpage: {
      heading: 'Valitse tilaus, jota haluat kokeilla ilmaiseksi',
      subheading: 'Voit muuttaa tilausta milloin vain',
      buttons: {
        silver: 'Hopea',
        gold: 'Kulta',
        family: 'Perhe',
        continue: 'Jatka',
      },
      p1: 'Hinta',
      p2: 'ilmeisten päivien jälkeen, jotka jatkuvat',
      prices: {
        silver: '€',
        gold: '€',
        family: '€',
      },
      list: {
        item1: 'Uusimmat kirjat',
        item2: 'Henkilöt, jotka voivat lukea ja kuunnella samalla tilauksella',
        item3: 'Lue ja kuuntele kännykällä tai tabletilla',
        item4: 'Rajaton määrä s-kirjoja ja äänikirjoja',
        item5: 'Lopeta milloin vain',
      },
    },
    registercard: {
      notifytrustfail:
        'Rekisteröityminen Mobiilin Pankki-ID:n avulla keskeytyi! Koeta uudelleen. Ellei se toimi, tarkista pankkitili. Muuten ota yhteyttä pankkiisi.',
      heading: 'Anna lahjakortti',
      on: 'päällä',
      list: {
        item1: 'Kuuntele ja lue missä vain, milloin vain.',
        item2: 'Tuhansia kirjoja suoraan kännykkääsi tai tablettiisi',
        item3: 'Saat lahjakortin suoraan s-postiisi',
      },
      secure_server: 'Turvallinen palvelin (SSL-salattu)',
      credit_or_debit_card: 'Luotto- tai pankkikortti',
      mobile_bankid: 'Mobiilil Pankki-ID',
    },
  },
  /**************** END GIFTCARD PAGE ******************/

  /**************** DIRECTACTIVATION PAGE ******************/
  direct_activation: {
    links: {
      terms: 'jäsenehdot',
      latest_titles: 'uusimmat kirjamme',
    },
    new_member: {
      heading_p1: 'Tarjous sinulle: Kokeile Nextorya hintaan',
      heading_p2: ' €/kk',
      subheading_p1: 'Klikkaakr',
      subheading_p2: 'Aloita kampanjajakso',
      subheading_p3: 'lukeaksesi ja kuunnellaksesi',

      disclaimer: {
        p1: 'Kun klikkaat painiketta',
        p2: 'Aloita kampanjajakso',
        p3: 'hyväksyt meidän',
        p4: 'ja aloitat tilauksen.',
      },
    },
    existing_member: {
      heading: 'Olet jo Nextoryn jäsen.',
      subheading: 'Tarjous on valitettavasti vain niille, jotka eivät ole jäseniä.',
      p1: 'Miksi et katsoisi',
      p2: 'sen sijaan?',
    },
    invalid_code: {
      heading: 'Vaikuttaa siltä, ​​että tämä aktivointilinkki on joko vanhentunut tai virheellinen',
      subheading: 'Tämä kampanjakoodi ei ole enää aktiivinen.',
    },
    error: 'Virhe, ota yhteyttä asiakaspalveluun.',
  },
  /**************** END DIRECTACTIVATION PAGE ******************/

  /**************** CAMPAIGN PAGE ******************/
  campaign: { //Ref:Translation.campaign
    formheading_a: 'Rekisteröidy aloittaaksesi kampanjajakson',
    formheading_b: 'Aikarajoitteinen tarjous – ala lukea jo tänään!',
    heading1: 'Kirjoita kampanjakoodi saadaksesi tarjouksen.',
    heading2: 'Haluatko kokeilla Nextorya kampanjakoodilla?',
    disclaimer: 'Klikkaamalla painiketta "Jatka" hyväksyt',
    link: 'ehtomme',
    list: {
      item1: 'Lue ja kuuntele miten paljon vain.',
      item2: 'Valitse kirjoja kännykällesi tai tabletillesi tuhansien kirjojen joukosta.',
      item3: 'Ei sitoutumisaikaa, lopeta milloin vain.',
    },
    subscriptionpage: {//Ref:Translation.campaign.subscriptionpage
      month: 'kuukausi',
      months: 'kuukautta',
      campaigninfo: {
        currency: '€',
        in: '€',
        then: '. Sen jälkeen',
        daysfree_then: 'päivää ilmaiseksi, sen jälkeen',
        discount_on: '% alennusta: ',
        discount_in: '% alennusta',
        price_after_yours: 'Hinta sinun',
        free_of_charge_current: 'ilmainen, joka jatkuu ( Infoga datum här) asti',
        days_free_of_charge: 'päivää ilmaiseksi, jatkuu ( Infoga datum här) asti',
      },
      buttons: {
        silver: 'Hopea',
        gold: 'Kulta',
        family: 'Perhe',
      },
      prices: {
        silver: '€',
        gold: '€',
        family: '€',
      },
      list: {
        p1: 'Uusimmat kirjat',
        p2: 'Henkilöt, jotka voivat lukea ja kuunnella samalla tilauksella',
        p3: 'Lue ja kuuntele kännykälläsi tai tabletillasi',
        p4: 'Rajaton määrä e-kirjoja ja äänikirjoja',
        p5: 'Lopeta milloin haluat',
      },
      registerwrapper: {
        campaignmessage: {
          month:'kuukauden ajan',
          months:'kuukauden ajan',
          message_a:'Valitse yksi kolmesta tilausvaihtoehdostamme - et maksa mitään ennen kuin ilmainen kokeilujaksosi on päättynyt.',
          message_b:'Valitse yksi kolmesta tilausvaihtoehdostamme - maksat vain ',
          message_c:'Valitse yksi kolmesta tilauksestamme. Saat ',
          in:'kuukaudessa',
          discount_in: 'alennusta',
          currency:'€',
          },
        stage: 'Vaihe',
        of: '/',
        pre_stage_heading: 'Valitse yksi kolmesta tilausvaihtoehdostamme',
        pre_stage_body_a: 'Et maksa mitään ennen kuin ilmaisjaksosi on päättynyt.',
        pre_stage_body_b: 'Valitse yksi kolmesta tilausvaihtoehdostamme - et maksa mitään ennen kuin ilmaisjaksosi on päättynyt.',
        in: '' ,
        pre_stage_button: 'Näytä tilausvaihtoehdot',
        heading1: 'Valitse tilaus, jota haluat kokeilla ilmaiseksi',
        heading2: 'Valitse tilaus, jota haluat kokeilla tarjousjaksosi ajan',
        subheading: 'Voit vaihtaa tilausta milloin vain',
        continue: 'Jatka',
        choose: 'Valitse',
        for: '',
        off: 'alennusta',
        when: '. Saat',
        free_for: '',
        days: '',
        at_the_cost_of: 'hintaan ',
        for_free_days: 'päivää ilmaiseksi',
        currency_per_month: '€/kk',
        family: {
          heading: 'Kuinka moni haluaa lukea ja kuunnella tilausta?',
          subheading: 'Valitse kuinka monta käyttäjää haluat tilauksellesi.',
          users: 'käyttäjää',
          days_free_then: 'päivää ilmaiseksi. Sen jälkeen',
          currency_per_month: '€/kk',
          choose: 'Valitse',
          in: '',
          after: 'ajan. Sen jälkeen',
          month: 'kuukauden',
          months: 'kuukaden',
          discount: 'alennusta',
        },
        campaign_message: {
          free_days: 'Hinta ilmaisjaksosi jälkeen, joka päättyy',
          fixedprice: 'Hinta tarjousjaksosi jälkeen, joka päättyy',
          discount: 'Hinta tarjousjaksosi jälkeen, joka päättyy',
      },
        button_go_back: 'Peruuta',
      },
      registercard: {//Ref:Translation.campaign.subscriptionpage.registercard
        notifytrustlyfail: 'Rekisteröityminen Mobiilin Pankki-ID:n avulla keskeytyi! Koeta uudelleen. Ellei se toimi, tarkista pankkitili. Muuten ota yhteyttä pankkiisi.',
        campaigninfo: {//Ref:Translation.campaign.subscriptionpage.registercard.campaigninfo
          currency: '€',
          in: '',
          then: '. Sen jälkeen',
          daysfree_then: 'päivää ilmaiseksi, sen jälkeen',
          discount_in: '% alennusta €',
          discount: '% alennusta',
          free_in: 'Ilmainen',
          days: 'päivää.',
          month: 'kuukauden.',
          months: 'kuukautta.',
        },
        discount: 'Tarjouksesi',
        currency_per_month:'€/kk',
        month:'kuukauden ajan',
        months:'kuukauden ajan',
        days_free_then:'päivää ilmaiseksi, sen jälkeen',
        in:'kuukaudessa',
        free:'ilmaiseksi',
        heading: 'Anna maksutietosi',
        subheading_a_part1: 'Lopeta tilauksesi ennen',
        subheading_a_part2: 'niin et maksa mitään. Et sitoudu mihinkään ja voit lopettaa tilauksesi netissä milloin vain.',
        subheading_b_part1: 'Tarjoushintasi on voimassa',
        subheading_b_part2: 'asti. Et sitoudu mihinkään ja voit lopettaa tilauksesi netissä milloin haluat.',
        paymenttitle: 'Valitse maksutapa',
        your_offer: 'Tarjouksesi:',
        currency:'€',
        p1: 'Hinta/kuukausi kampanjajakson jälkeen:',
        p2: 'Ei velvoituksia, lopeta tilaus nettisivullamme milloin vain.',
        secure_server: 'Turvallinen palvelin (SSL-salattu)',
        credit_or_debit_card: 'Luotto- tai pankkikortti',
        mobile_bankid: 'Mobiili Pankki-ID',
      },
    },
    campaignreguserform: {//Ref:Translation.campaign.campaignreguserform
      formlabels: {
        campaign_code: 'Kirjoita kampanjakoodi',
        email: 'S-postiosoite',
        repeat_email: 'Toista s-postiosoite',
        password: 'Salasana',
      },
      buttons: {
        continue: 'Jatka',
      },
    },
    customcampaign: {
      seodescription:
        'Onko sinulla Nextoryn kampanjakoodi? Kirjoita se saadaksesi Nextoryn koko fantastisen kirjojen ja kirjallisuuden maailman käyttöösi!',
    },
    validation: {
      campaign_code_error: 'Kirjoita kampanjakoodi',
      email_error: 'Kirjoita voimassa oleva s-postiosoite',
      repeat_email_error: 'S-postiosoitteet eivät täsmää',
      password_error: 'Kirjoita salasana',
      password_length_error: 'Salasanan on oltava 4-25 merkkiä',
      password_length_error2: 'Sallitut merkit: 0-9, a-z, A-Z sekä erikoismerkit: -, @, #, $, %, _',
    },
    saga_error: 'Tämä kampanjakoodi ei ole voimassa.',
  },

  /**************** END CAMPAIGN PAGE ******************/

  /**************** REGISTRATION PAGES ******************/
  // #Ref:Translation.registration
  registration: {
    stage: 'Vaihe',
    of: '/',
    h1: 'Luo tili',
    subinfo: 'Rekisteröidy sähköpostiosoitteellasi ja luo salasana, niin voit lukea & kuunnella Nextorya missä vain, milloin vain.',
    buttons: {
      continue: 'Jatka',
      go_back: 'Peruuta',
      viewsub: 'Näytä tilausvaihtoehdot',
      choose: 'Valitse',
      for_free_14_days: '14 päivää ilmaiseksi',
      create_account: 'Luo tili',
    },
    stage1: {
      heading_a: 'Valitse yksi kolmesta tilausvaihtoehdostamme',
      subheading_a: 'Et maksa mitään ennen kuin ilmaisjaksosi on päättynyt.',
      heading: 'Valitse tilaus, jota haluat kokeilla ilmaiseksi',
      subheading: 'Voit muuttaa tilaustasi milloin vain.',
      tabletitle: 'Valitse tilaus',
      heading_family: 'Kuinka moni haluaa lukea ja kuunnella tilausta?',
      subheading_family: 'Valitse kuinka monta käyttäjää haluat tilauksellesi.',
      features: {
        feature1:
          'Kuukausihinta, kun 14 vuorokauden ilmaisjakso on ohi. Ilmaisjaksosi päättyy ( Infoga datum här).',
        feature1_prices: {
          silver: '€',
          gold: '€',
          family: '€',
        },
        feature2: 'Uusimmat kirjat',
        feature3: 'Henkilöiden määrä, joka voi lukea ja kuunnella samaa tilausta',
        feature4: 'Lue ja kuuntele kännykälläsi tai tabletillasi',
        feature5: 'Rajaton määrä e-kirjoja ja äänikirjoja',
        feature6: 'Lopeta milloin vain',
      },
      buttons: {
        silver: 'Hopea',
        gold: 'Kulta',
        family: 'Perhe',
      },
    },
    stage2: {
      heading: 'Valitse tilaus, jota haluat kokeilla ilmaiseksi',
      subheading: 'Voit vaihtaa tilausta milloin vain',
      registered_user: {
        heading: 'Tili luotu',
        subheading: 'Käytä tätä s-postiosoitetta päästäksesi tilillesi:',
      },
      new_user: {
        heading: 'Rekisteröidy ilmaisjaksosi aloittamiseksi',
        subheading: 'Täytä tietosi jatkaaksesi.',
        disclaimer: 'Klikkaamalla Jatka hyväksyt',
        links: {
          terms: 'ehtomme',
        },
      },
      formlabels: {
        email: 'S-postiosoite',
        repeat_email: 'Toista s-postiosoite',
        password: 'Salasana',
      },
      validation: {
        email_error: 'Kirjoita voimassa oleva s-postiosoite',
        repeat_email_error: 'S-postiosoitteet eivät täsmää',
        password_error: 'Kirjoita salasana',
        password_length_error: 'Salasanan on oltava 4-25 merkkiä',
        password_length_error2:
          'Sallitut merkit: 0-9, a-z, A-Z sekä erikoismerkit: -, @, #, $, %, _',
      },
    },
    stage3: {
      messages: {
        trustyfail_success:
          'Rekisteröityminen Mobiilin Pankki-ID:n avulla keskeytyi! Koeta uudelleen. Ellei se toimi, tarkista pankkitili. Muuten ota yhteyttä pankkiisi.',
      },
      heading: 'Kirjoita maksutiedot',
      bullets: {
        point1_p1: 'Lopeta tilauksesi ennen',
        point1_p2: 'niin et maksa mitään. Et sitoudu mihinkään ja voit lopettaa tilauksesi netissä milloin vain.',
        point2: 'Ei velvoita mihinkään, voit lopettaa tilauksen netissä milloin vain',
      },
      secure_server: 'Turvallinen palvelin (SSL-salattu)',
      credit_or_debit_card: 'Luotto- tai pankkikortti',
      mobile_bankid: 'Mobiili Pankki-ID',
      regcardform: {
        paymenttitle: 'Valitse maksutapa',
        formlabel: 'Vanhentumiskuukausi',
        buttons: {
          text1: 'Päivitä kortin tiedot',
          text2: 'Aloita ilmaisjakso',
          text3: 'Aktivoi tilaus',
          text4: 'Aloita tilaus',
          text5: 'Osta lahjakortti',
          text6: 'Aloita tilauksesi',
          text7: 'Aloita tilauksesi',
        },
        credit_debit_option: {
          card_number: 'Kortin numero',
          expiration_month: 'Viimeinen voimassaolokuukausi',
          months: {
            month: 'Kuukausi',
            january: 'Tammikuu',
            february: 'Helmikuu',
            march: 'Maaliskuu',
            april: 'Huhtikuu',
            may: 'Toukokuu',
            june: 'Kesäkuu',
            july: 'Heinäkuu',
            august: 'Elokuu',
            september: 'Syyskuu',
            october: 'Lokakuu',
            november: 'Marraskuu',
            december: 'Joulukuu',
          },
          expiration: 'Viimeinen voimassaolovuosi',
          year: 'Vuosi',
          security_code: 'Turvakoodi (CVV)',
          error: {
            presubmit: 'Kortin rekisteröinti keskeytyi. Näin kävi luultavasti koska:',
            bullets: {
              point1: 'Et ole aktivoinut korttia verkkomaksuja varten.',
              point2: 'Kirjoitit kortin tiedot väärin.',
              point3: 'Korttisi ei ole voimassa.',
              point4: 'Pankki ei sallinut maksua.',
            },
            please_try_again: 'Koeta uudelleen.',
          },
        },
      },
      regtrustly: {
        mobile_bankid_option: {
          initial_text:
            'Aloita ilmainen jakso digitaalisella allekirjoituksella Mobiilin Pankki-ID:n avulla. Klikkaa "Använd Mobilt BankID" saattaaksesi rekisteröitymisen loppuun.',
          second_to_payment:
            'Haluatko mieluummin käyttää Mobiilia Pankki-ID:tä? Klikkaa "Använd Mobilt BankID" vaihtaaksesi maksutapaa.',
          enable_payment:
            'Aloita tilaus allekirjoittamalla suoraveloitus digitaalisesti Mobiililla Pankki-ID:llä.  Klikkaa "Använd Mobilt BankID" saattaaksesi aktivoinnin loppuun.',
          register_giftcard:
            'Osta lahjakortti digitaalisella allekirjoituksella Mobiilin Pankki-ID:n avulla. Klikkaa "Använd Mobilt BankID" oston loppuun saattamiseksi.',
          register_campaign:
            'Aloita kampanjajakso digitaalisella allekirjoituksella Mobiilin Pankki-ID:n avulla. Klikkaa "Använd Mobilt BankID" saattaaksesi rekisteröitymisen loppuun.',
        },
        buttons: {
          use_mobile_bankid: 'Käytä Mobiilia Pankki-ID:tä',
        },
      },
      validation: {
        card_number_error: 'Kirjoita voimassa oleva kortin numero',
        month_error: 'Kirjoita voimassa oleva kortin numero',
        year_error: 'Kirjoita voimassa oleva kortin numero',
        cvv_error: 'Kirjoita voimassa oleva kortin numero',
        cvv_length_error: 'Kirjoita voimassa oleva kortin numero',
      },
    },
    register_success: {
      reactivated: {
        heading: 'Tervetuloa takaisin Nextorylle!',
        subheading:
          'Tilisi on jälleen aktivoitu ja tiliäsi on laskutettu valitsemasi tilauksen mukaan. Tarkistathan seuraavat tiedot paremman käytettävyyden takaamiseksi.',
      },
      new: {
        heading: 'Tervetuloa Nextorylle! Tilauksesi on nyt aloitettu.',
        subheading: 'Täytäthän seuraavat tiedot paremman käytettävyyden takaamiseksi.',
      },
      register_success_form: {
        first_name: 'Etunimi',
        lastname: 'Sukunimi',
        cellphone: 'Kännykkänumero',
      },
      cellphone_error: 'Puhelinnumeron on oltava 7-12 numeroa.',
    },
    saga: {
      campaign: {
        password_error: 'Salasana ei täsmää.',
        campaign_error: 'Kampanjakoodi ei ole voimassa.',
      },
      giftvoucher: {
        auth_error: 'Osoite on jo rekisteröitynä meillä.',
        validation_email_error: 'Osoite ei ole rekisteröitynä meillä.',
        validation_giftvoucher_error: 'Lahjakoodi ei ole voimassa.',
      },
      trial_signup: {
        email_error: 'Sähköpostiosoite on jo rekisteröitynä meillä.',
      },
    },
  },
  /**************** END REGISTRATION PAGES ******************/

  /**************** 404 PAGES ******************/
  not_found: {
    heading: 'Hupsista!',
    subheading: 'Valitettavasi hakemaasi sivua ei löytynyt.',
    go_back: 'Siirry takaisin etusivulle klikkaamalla ',
    content:
      'Jos virheitä ilmenee toistuvasti tai sinulla on kysyttävää, ota yhteyttä asiakaspalveluumme asiakaspalevlu@nextory.fi. Mukavaa päivänjatkoa hyvän kirjan ',
    links: {
      click_here: 'tästä.',
      nice_book: 'parissa.',
    },
  },
  /**************** END 404 PAGES ******************/

  /**************** APP PAGES ******************/
  app_page: {
    heading: 'Lataa älykäs sovelluksemme',
    content:
      'Rekisteröidy ilmaisjakson käyttäjäksi kotisivulla ja lataa sitten sovelluksemme, joko iOS tai Android. Voit alkaa lukea tai kuunnella heti. Kirjat saat käyttöön, kun kirjaudut sovellukseen.',
  },
  /**************** END APP PAGES ******************/

  /**************** FAQ PAGES ******************/
  faq: {
    seotitle: 'Tavallisia kysymyksiä ja vastauksia',
    seodescription:
      'Kokeile tuhansia äänikirjoja ja e-kirjoja ilmaiseksi 14 päivää! Lataa sovellus ja striimaa iPhonessa, Androidissa tai iPadissa missä vain Nextrorylla.',
    faq1: {
      heading: 'Kuinka löydän haljuamani kirjat?',
      content: {
        p1:
          'Jos haet tiettyä kirjaa, kirjailijaa tai lukijaa, voi käyttää sovelluksen hakuruutua, klikkaa suurennuslasia. Jos haluat valita suositelluista kirjoista, löydät uutuudet, suositut kirjat ja muut kirjavinkit sovelluksen aloitussivulta. Haluatko hakea lajeista? Klikkaa vasemman kulman valikkoa ja valitse luettelosta laji.',
        p2:
          'Monet kirjoistamme ovat olemassa sekä e- että äänikirjana. Näet valitsemasi formaatin kannen symbolista. Jos löydät kirjan, mutta haluat sen toisessa formaatissa, valitse ”näytä e-kirja” tai ”näytä äänikirja” kirjan tietosivulla. Ellei sellaista painiketta ole, meillä on kirja tällä hetkellä vain siinä formaatissa, jossa se näytetään.',
      },
    },
    faq2: {
      heading: 'Kuinka Minun listani toimii?',

      content: {
        p1:
          'Kun löydät kirjan, jonka haluat lukea tai kuunnella, klikkaat sitä. Silloin pääset kirjan tietosivulle. Siellä voit lisätä kirjan Minun listaani klikkaamalla painikketta + Minun listani.',

        p2:
          'Kirjojen lisääminen listaan on tapa pitää järjestystä siinä, mitä kirjoja haluat kirjastoosi. Jotta voit alkaa lukea tai kuunnella kirjaa, sinun on aloitettava valitsemalla ”Lue” tai ”Kuuntele”.',
      },
    },
    faq3: {
      heading: 'Kuinka aloitan kirjan?',
      content: {
        p1:
          'Kun klikkaat kirjaa Minun listallani, haussa tai suosituksissa alkusivulla, tulet valitun kirjan tietosivulle. Siellä voit valita Lue tai Kuuntele (sen mukaan, onko kyse e- vai äänikirjasta) aloittaaksesi välittömästi.',
        p2:
          'Jos kirja on olemassa toisessa formaatissa, valitse ”Näytä äänikirja” tai ”Näytä e-kirja” vaihtaaksesi niihin.',
      },
    },
    faq4: {
      heading: 'Lue ja kuuntele offline',
      content: {
        p1:
          'Nextoryn sovelluksessa kaikki kirjat, joita luet ja kuuntelet, ladataan sovellukseen. Niin tehdään, jotta vältyt häiriöiltä, joita striimatussa lukemisessa voi esiintyä. Aktivoimalla kirjan silloin, kun voit käyttää wifiä, vältyt sitä paitsi maksamasta mahdollisia operaattorisi tietoliikenteen lisämaksuja.',
        p2:
          'Haluatko kuunnella offline? Se onnistuu erinomaisesti, kunhan kirja on aktivoitu ja kokonaan ladattu kännykkään tai tablettiin.',
      },
    },
    faq5: {
      heading: 'Kuinka e-kirjan lukulaite toimii?',
      content: {
        p1:
          'Kun luet e-kirjaa, voit itse muuttaa kirjan ulkonäköä muuttamalla valon vahvuutta, sivujen marginaaleja, taustaväriä sekä tekstin kokoa. Voit myös laittaa omia kirjanmerkkejä kirjaan sekä merkitä tekstiä ja tehdä muistiinpanoja. Meny-kuvakkeesta löydät luvun, kirjanmerkit ja merkinnät.',
        p2:
          'Kun suljet kirjan, kirjanmerkki tallentuu automaattisesti ja voit jatkaa lukemista siltä sivulta, jolla olit.',
      },
    },
    faq6: {
      heading: 'Kuinka äänikirjan lukulaite toimii?',
      content: {
        p1:
          'Voit kuunnella suoraan laitteen kaiuttimista, kuulokkeilla tai ulkoisella kaiuttimiella. Kun kuuntelet äänikirjaa säädät volyymin sovelluksen säätimestä tai laitteesi säätimestä.',
        p2:
          'Painamalla taukopainiketta. Jos suljet sovelluksen, kirja menee automaatisesti tauolle ja kirjanmerkki tallentuu kohtaan, johon lopetit kuuntelun. Seuraavan kerran kuunteleminen alkaa siitä, mihin viimeksi keskeytit kuuntelun.',
        p3:
          'Voit kelata eteen- ja taaksepäin kirjassa ja merkitä omia kirjanmerkkejä, jos haluat. Äänikirjoille on toiminto, jos haluat nukahtaa kirjaa kuunnellen, mutta aamulla haluat välttyä etsimästä kohtaa, jossa nukahdit. Kun kirja on käynnissä, painat unisymbolia ja valitset, kuinka kauan haluat kirjan olevan käynnissä, ennen kuin se automaattisesti pysähtyy.',
        p4: 'Voit kuunnella kirjaa normaalinopeudella tai lisätä lukemisen nopeutta.',
      },
    },
    faq7: {
      heading: 'Haluan poistaa kirjan, kuinka teen?',
      content: {
        p1:
          'Poista kirja painamalla kolmen pisteen symbolia kirjassa, jota et halua lukea. Valitse ”Poista ja lisää Minun listaani”. Kirja on jäljellä listallasi niin, että voit ladata sen myöhemmin, jos haluat aktivoida sen uudelleen.',
        p2:
          'Jos haluat poistaa kirjan myös listaltasi, klikkaa listan kirjaa ja valitse ”Poista Minun listaltani”.',
      },
    },
    faq8: {
      heading: 'Miksi en voi ladata kirjoja?',
      content: {
        p1:
          'Luultavasti sinulla on monia kirjoja aktiivisina samanaikaisesti. Koska jokainen kirja vie muistitilaa laitteelta, voi olla, että joudut poistamaan muutamia kirjoja ennen kuin voit aktivoida uusia.',
        p2:
          'Poista kirja painamalla kolmen pisteen symbolia kirjassa, jota et halua lukea. Valitse ”Poista ja siirrä Minun listaani”. Kirja on edelleenkin listallasi niin, että voit ladata sen myöhemmin, jos haluat aktivoida sen uudelleen. Nyt olet vapauttanut muistitilaa niin, että voit aktivoida uusia kirjoja.',
      },
    },
    faq9: {
      heading: 'Voinko käyttää Nextorya useilla laitteilla?',
      content: {
        p1:
          'Voit käyttää Nextoryn sovellusta useilla laitteilla. Mutta vain yhdellä kerrallaan, jos tilauksesi on Kulta tai Hopea. Jos tilauksesi on Perhe, neljä henkeä voi lukea tai kuunnella samanaikaisesti eri kännyköillä tai tableteilla. Nämä neljä perheprofiilia voivat myös käyttää useampia eri laitteita, mutta vain yhtä kerrallaan.',
        p2:
          'Lataa sovellus niihin laitteisiin, joita haluat käyttää, ja kirjaudu kirjautumistiedoillasi. Perhetilauksessa valitset, kuka perheessä on kirjautunut.',
        p3:
          'Muista, että kaikki kirjat, jotka aktivoit, tallennetaan sille laitteelle, jolla olet kirjan aktivoinut, eli kun vaihdat laitetta, sinun täytyy aktivoida kirja uudelleen.',
      },
    },
    faq10: {
      heading: 'Kuinka lisään käyttäjiä Perhetilaukseeni?',
      content: {
        p1:
          'Kun olet valinnut tilauksen Perhe, voit luoda neljä profiilia. Lisäät profiilit suoraan sovelluksessa, ylinnä valikossa. Lapsiprofiilit on rajattu niin, että valikoima koostuu vain lastenkirjallisuudesta. Voit milloin vain muokata profiileja.',
        p2:
          'Lataa sovellus perheesi laitteille ja kirjaudu kirjautumistiedoillasi. Valitse sitten, kun perheestä käyttää sovellusta. Voit helposti vaihtaa käyttäjää valikosta.',
        p3: 'Jokaisella perheenjäsenellä on oma profiili listoineen ja aktiivisine kirjoineen.',
      },
    },
    faq11: {
      heading: 'Kuinka muutan tilaustani?',
      content: {
        p1:
          'Voit vaihtaa tilausta milloin vain. Sen teet Minun tililläni Nextoryn verkkosivustolla. Jos vaihdat pienempään tilaukseen, nykyinen tilauksesi säilyy seuraavaan maksukertaan asti. Jos vaihdat laajempaan tilaukseen, saat uuden tilauksen ja ja siihen kuuluvat kirjat käyttöösi välittömästi.',
        p2:
          'Kun vaihdat laajempaan tilaukseen, sinun on kirjauduttava pois sovelluksesta ja kirjauduttava uudelleen, jotta uusi tilauksesi tulisi käyttöön. Tämä koskee kaikkia laitteita, joilla käytät Nextorya.',
      },
    },
    faq12: {
      heading: 'Mikä sovellus minun kannattaa valita?',
      content: {
        family: 'Perhe',
        gold: 'Kulta',
        silver: 'Hopea',
        p1:
          'Voit valita kolmesta eri tilauksesta saadaksesi ne toiminnot, jotka sopivat sinulle parhaiten.',
        p2:
          'maksaa Euro kuukaudessa. Tähän kuuluu koko valikoima, jossa on kymmeniä tuhansia kirjoja, uutuudet sekä neljä profiilia, jotka voivat käyttää tiliä. Jokaisella perheenjäsenellä on uniikki käyttäjäprofiili, jolla voi lukea ja kuunnella samanaikaisesti eri laitteilla. Sopii sille, jonka taloudessa on enemmän kuin yksi henkilö ja joka haluaa kaikki kirjat koko perheelle. Käyttäjämäärä: 4.',
        p3:
          'maksaa Euro kuukaudessa ja sisältää koko laajan e- ja äänikirjavalikoimamme. Jos valitset Kullan, saat myös uutuudet käyttöösi. Sopii sille, joka haluaa kaikki kirjat käyttöönsä. Käyttäjämäärä: 1.',
        p4:
          'maksaa Euro kuukaudessa ja sisältää kymmeniä tuhansia e- ja äänikirjoja, mutta ei uutuuksia eikä tiettyjä kirjoja sopimussyistä. Saat käyttöösi noin 80 % laajasta valikoimastamme, jota koko ajan päivitetään. Hopea sopii sille, joka haluaa erittäin edullisen vaihtoehdon. Käyttäjämäärä: 1.',
      },
    },
    faq13: {
      heading: 'Kuinka maksaminen tapahtuu?',
      content: {
        p1:
          'Tilauksesi uusiutuu automaattisesti joka 30. päivä, toisin sanoen seuraavana maksupäivänä. Silloin sinua laskutetaan sen tilauksen ja maksutavan mukaan, jonka olet valinnut. Jos käytät maksutapana Trustlyä, maksutapahtuma näkyy pankkitililläsi pari päivää laskutuksen jälkeen. Kortilla maksettaessa on tavallisinta, että maksutapahtuma näkyy suoraan pankissa.',
      },
    },
    faq14: {
      heading: 'Kuinka lopetan tilauksen?',
      content: {
        p1:
          'Palvelun irtisanominen on yksinkertaisinta Minun tililläni Nextoryn verkkosivustolla. Voit myös ottaa yhteyttä asiakaspalveluumme sähköpostitse tai puhelimitse.',
        p2:
          'Jos haluat lopettaa tilauksen ennen tulevaa maksua, se tulee tehdä vähintään päivää ennen maksupäivää kotisivun Minun tililläni, tai vähintään kaksi päivää ennen maksupäivää, jos olet yhteydessä asiakaspalveluun.',
      },
    },
    faq15: {
      heading: 'Ongelmia lukemisessa/kuuntelussa offline',
      content: {
        p1:
          'Kirjaa laitteelle ladattaessa on voinut tapahtua virhe. Kokeile laittaa laite online-tilaan ja lataa kirja uudelleen. Pidä huolta, että sinulla on hyvä yhteys, mielellään vakaa wifi-verkko. Tarkista, että 100 % on latautunut, ennen kuin suljet sovelluksen, laitat laitteen offline-tilaan tai ala kuunnella.',
        p2: 'Varmista myös, että puhelimella on riittävästi muistitilaa, jotta voi ladata kirjoja.',
        p3:
          'Ellei edellä mainittu auta, voi puhelimen uudelleen käynnistäminen auttaa. Jos sovellus takkuilee, voi olla, että joudut poistamaan sen ja asentamaan sen uudelleen. Kirjasi tallentuvat eivätkä häviä tililtäsi, vaikka sovellus poistettaisiinkin.',
      },
    },
    faq16: {
      heading: 'Sovellus ei toimi, en voi lukea tai kuunnella.',
      content: {
        p1:
          'Tarkista, että sinulla on sekä sovelluksen että laitteen käyttöjärjestelmän uusimmat versiot.',
        p2:
          'Jos olet vaihtanut tilausta, sinun on kirjauduttava pois sovelluksesta ja kirjauduttava uudelleen, jotta saisit uuden tilauksen käyttöösi. Tämä koskee kaikkia laitteita, joilla käytät Nextorya.',
        p3:
          'Varmista myös, että puhelimessasi tai tabletissasi on riittävästi muistitilaa, jotta voit aktivoida kirjasi.',
        p4:
          'Jos tilauksesi on Hopea, voit lukea ja kuunnella noin 80 % valikoimastamme. Jos haluat käyttöösi myös premium-valikoimamme ja uutuudet, sinun on vaihdettava Kultaan tai Perheeseen.',
        p5:
          'Ellei sovellus muista syistä toimi kuten sen pitäisi, voit koettaa sulkea sovelluksen ja käynnistää sen uudelleen, tai käynnistää puhelimen uudelleen.',
        p6_p1: 'Jos ongelma jatkuu – ota yhteyttä asiakaspalveluumme sähköpostitse ',
        p6_p2: 'tai soittamalla 08-411 17 15.',
        p7:
          'Voit myös koettaa poistaa sovelluksen puhelimestasi kokonaan. Asenna sovellus sitten uudelleen ja käynnistä puhelin uudelleen. Kirjasi on tallennettu eivätkä ne häviä tililtäsi, vaikka sovellus poistettaisiinkin.',
      },
    },
    faq17: {
      heading: 'Kuinka otan yhteyttä Nextoryyn?',
      content: {
        p1: 'Haluamme olla käytettävissä. Siksi voit ottaa yhteyttä meihin useilla eri tavoilla.',
        p2:
          'Kotisivu. Pääset tilillesi kirjautumalla kotisivumme kautta. Siellä voit helpposti muuttaa tilaustasi.',
        p3:
          'Jos haluat henkilökohtaista opastusta, voit soittaa meille Euro. Toimistomme on avoinna maanantaista perjantaihin klo Euro.',
        p4_p1: 'S-posti. Voit milloin vain lähettä sähköpostia',
        p4_p2: 'Otamme useimmiten yhteyttä samana päivänä tai seuraavana arkipäivänä.',
        p5: 'Älä epäröi ottaa yhteyttä, jos tarvitset apua.',
      },
    },
  },
  /**************** END FAQ PAGES ******************/

  /**************** GET STARTED PAGES ******************/
  getstarted: {
    reactivated: {
      heading: 'Näin pääset alkuun',
      content: {
        p1: 'Kirjaudu Nextoryn sovellukseen kännykälläsi tai tabletillasi',
        p2:
          'Voit nyt jatkaa kirjojen lukemista ja kuuntelemista listaltasi tai valitsemalla kymmenien tuhansien muiden kirjojen joukosta.',
      },
      tips:
        'Vinkki: tutustu uusiin toimintoihin. Teemme koko ajan päivityksiä ja parannuksia sovellukseen. Käytä muutama minuutti tutustuaksesi uusiin mahdollisuksiin, joita Nextory tarjoaa.',
      app_removed:
        'Oletko poistanut sovelluksen? Lataa se uudelleen Apple Storesta tai Google Playsta.',
    },
    new: {
      heading: 'Näin voit alkaa käyttää Nextorya',
      content: {
        p1:
          'Lataa Nextoryn sovellus App Storesta tai Google Playsta kännykkääsi ja/tai tabletillesi.',
        p2: 'Kirjaudu sovellukseen sähköpostiosoitteellasi ja salasanallasi.',
        p3: 'Valmista! Ala lukea ja kuunnella!',
      },
      tips: 'vinkit:',
    },
  },
  /**************** END GET STARTED PAGES ******************/

  /**************** SALJA PAGES ******************/
  saljapage: {
    seotitle: 'Haluatko myydä digitaalisia kirjoja Nextoryn avulla?',
    seodescription:
      'Myy kirjojasi Nextorylla. Tarjoamme tilauspalveluja sekä e- että äänikirjoille.',
    heading: 'Haluatko myydä digitaalisia kirjoja Nextoryssa?”',
    content: {
      p1:
        'Tarvitset firman, jolla on Y-tunnus sekä e- ja/tai äänikirjoja, joilla on uniikki ISBN-tunnus',
      p2_p1:
        'Haluamme myös, että toimitat kirjasi digitaalisen jakelijan kautta. Ota mielellään yhteyttä meihin osoitteeseemme',
      p2_p2: 'niin kerromme lisää!',
    },
  },

  /**************** END SALJA PAGES ******************/

  /**************** SILVERFAQ PAGES ******************/
  silverfaq: {
    seotitle: 'Tavallisia kysymyksiä ja vastauksia',
    seodescription:
      'Kokeile tuhansia äänikirjoja ja e-kirjoja ilmaiseksi 14 päivää! Lataa sovellus ja striimaa iPhonessa, Androidissa tai iPadissa missä vain Nextoryn avulla.',
    heading: 'Tavallisia kysymyksiä Hopea-tilauksesta',
    links: {
      h1: 'Miksi en voi kuunnella uusimpia kirjoja Hopeassa?',
      h2: 'Miksi osa vanhemmista kirjoista ei ole käytettävissä Hopeassa?',
      h3: 'Kuinka Hopean tarjonta eroaa Kullasta ja Perheestä?',
      h4: '€',
      h5: 'Miksi osa sarjan kirjoista puuttuu Hopeassa?',
      h6: 'Voinko päivittää tilausta tilapäisesti?',
      h7: 'Eri tilausvaihtoehtomme',
    },
    contents: {
      page1: {
        heading: 'Miksi en voi kuunnella uusimpia kirjoja Hopeassa?',
        content: {
          p1:
            'Hopean alhaisempi hinta on mahdollinen rajallisen valikoiman ansiosta. Uusimmat kirjat, yleensä 0–6 kuukautta, maksavat enemmän lukukertaa kohti ja vaativat kalliimman tilauksen, kuten Kulta tai Perhe. ',
          p2: 'Hopea-tilaus antaa käyttöösi noin 85 % Nextoryn koko valikoimasta.',
        },
      },
      page2: {
        heading: 'Miksi osa vanhemmista kirjoista ei ole käytettävissä Hopeassa?',
        content: {
          p1:
            'Se, mitkä kirjat ovat käytettävissä Hopeassa, riippuu sopimuksistamme kustantamojen kanssa sekä kustantamojen ehdoista kirjailijoiden kanssa.',
          p2: 'Toimimme koko ajan, jotta voisimme laajentaa Hopean valikoimaa.',
        },
      },
      page3: {
        heading: 'Kuinka Hopean valikoima eroaa Kullasta ja Perheestä?',
        content: {
          p1:
            'Hopea-tilauksella saat käyttöösi noin 85 % Kullan ja Perheen valikoimasta. Kirjoja on melkein yhtä paljon kuin ruotsalaisessa keskivertokirjastossa.',
        },
      },
      page4: {
        heading: 'Kuinka usein Hopean valikoima päivitetään?',
        content: {
          p1:
            'Uusia kirjoja tulee joka päivä. Löydät uutuusluettelot aloitussivulta ja lajisivuilta.',
        },
      },
      page5: {
        heading: 'Miksi sarjan yksi kirja puuttuu Hopeassa?',
        content: {
          p1:
            'Kirjasarjan uusin kirjan saattaa puuttu Hopeassa, koska uudet kirjat, yleensä 0–6 kuukautta, maksavat enemmän lukukertaa kohti ja vaativat kalliimman tilauksen, kuten Kullan tai Perheen. ',
        },
      },
      page6: {
        heading: 'Voinko päivittää tilausta tilapäisesti?',
        content: {
          p1: 'Kyllä, verkkosivustollamme voi koska vain vaihtaa tilaustasi.',
        },
      },
      page7: {
        heading: 'Eri tilauksistamme',
        content: {
          p1:
            'Nestorylla valitset kolmesta eri tilauksesta. Kulta ja Perhe antavat koko valikoiman käyttöösi, ja Perhe sallii 4 eri käyttäjää. Hopealla saat edullisemman hinnan mutta valikoima on hieman rajattu.',
          p2: 'Voit vaihtaa tilausta milloin vain.',
        },
      },
    },
  },
  /**************** END SILVERFAQ PAGES ******************/

  /**************** STIPENDIUM PAGES ******************/
  stipendum_pages: {
    heroprocess: 'Ansökningsprocessen',
    herostipendie: {
      heading: 'Stipendi innovatiivisille ideoille e- ja äänikirjoista',
      subheading: 'Saat 15 000 kr opintoihin. Lukuvuosi 2018',
    },
    application: {
      seotitle: 'Hakuprosessi - Nextorystipendi',
      seodescription:
        '1. Vastaa kysymykseen.  vaihe 2. Lähetä panoksesi. Vaihe 3. Voittaja julkistetaan.',
      content1: {
        heading: 'Vaihe 1. Vastaa kysymykseen',
        p1: 'Mieti aihetta ja vastaa seuraavaan kysymykseen ruotsiksi käyttäen 1 500–2 000 sanaa.',
        quote:
          'Kuinka digitalisaatio ja kirjat voidaan yhdistää niin, että yhä useampi Ruotsissa voisi lukea? Mitkä mielestäsi ovat tärkeimmät tekijät, jotka vaikuttavat arkisiin lukutottumuksiimme digitalisaation yhteydessä ja kuinka voimme kohdata tämän muutoksen lukemisen lisäämiseksi? Kuinka e-kirjojen tulo vaikuttaa lukemiseen? Milloin ja kuinka ihmiset kuuntelevat äänikirjoja?',
        p2:
          'Voit vapaasti valita näkökulman ja metodin, joten koeta olla luoa. Hyödynnä omaa opintotaustaasi ja omia erityisiä taitojasi, kokemustasi, opintojasi ja mielenkiintosi kohteita, jotta voit tehdä uusia huomioita aiheesta.',
        p3:
          'Sinun ei tarvitse opiskella tiettyä ohjelmaa voidaksesi tehdä hakemuksen: Nextorystipendi on avoin kaikille opiskelijoille, jotka ovat rekisteröitynä ruotsalaisessa yliopistossa tai korkeakoulussa, jotka palavat kirjallisuudelle ja joilla on inspiroivia ideoita digitalisaation tulevasta kehityksestä. Huolimatta siitä, mitä opiskelet, olet tervetullut kaikkein luovimpine ja innovatiivisimpine ideoinesi.',
      },
      content2: {
        heading: 'Vaihe 3: Lähetä panoksesi',
        p1_p1: 'Ok, nyt olet vastannut kysymykseen ja voit lähettää panoksesi. Lähetä se',
        p1_p2:
          "ole tarkka, että liität mukaan kopion kevät- tai syyslukukauden yliopisto- tai korkeakoulurekisteröinnistä PDF-muodossa, kun lähetät panoksesi. Lähetä se ennen 31. elokuuta 2018 klo 23.59. Huomaa, että hakemuksia, jotka tulevat tämän päivämäärän jälkeen, ei hyväksytä. Jotta olisit oikeutettu stipendiin, sinun on oltava rekisteröitynyt ruotsalaiseen yliopistoon tai korkeakouluun kevät- tai syyslukukaudella 2018 import DataLayer from 'containers/App/datalayer'; i ja oltava kirjoilla Ruotsissa.",
      },
      content3: {
        heading: 'Vaihe 3: Nextorystipendin 2018 voittajan julkistaminen',
        p1:
          'Kaikki hakemukset arvioi tarkkaan raatimme, joka koostuu alan asiantuntijoista. Ennen syyskuun viimeistä saat vastauksen, oletko saanut stipendin.',
        p2_p1: 'Hakemalla Nextoryn stipendiä hyväksyt myös',
        p2_p2: 'yleiset jäsenehdot.',
        buttons: {
          back_to_nextory_scholarship: 'Takaisin Nextorystipendiin',
        },
      },
    },
    index: {
      seotitle: 'Lukeminen digitaalisessa tulevaisuudessa – e-kirja ja äänikirja keskiössä',
      seodescription:
        'Haluamme, että kaikkien käytettävissä on digitaalinen kirjasto. Siksi sinä opiskelijana voit saada stipendin, joka on 15 000 kr.',
      heading: 'Lukeminen digitaalisessa tulevaisuudessa – e-kirja ja äänikirja keskiössä',
      p1:
        'Nextoryn visio on, että kaikilla on käytettävissään kaikki maailman kijat e-kirjoina ja äänikirjoina. Haluamme, että kaikki voivat lukea samalla käyttöönsä digitaalisen kirjaston, joka koostuu e- ja äänikirjoista. Siksi sinä opiskelijana voit saada stipendin, joka on 15 000 kr. Saadaksesi stipendin sinun on ajateltava uudella lailla digitalisaatiota ja lukemista, lähinnä e- ja äänikirjoja online ja sovelluksissa.',
      p2:
        'Saadaksesi stipendin sinun on kirjoitettava 1 500–2 000 sanana mittainen aine, jossa kuvaat, kuinka digitalisaatio ja kirjat voidaan yhdistää lukemisen lisäämiseksi Ruotsissa. Aineessa on vastattava kysymykseen:',
      quote:
        'Kuinka digitalisaatio ja kirjat voidaan yhdistää niin, että yhä useampi Ruotsissa voisi lukea? Mitkä mielestäsi ovat tärkeimmät tekijät, jotka vaikuttavat arkisiin lukutottumuksiimme digitalisaation yhteydessä ja kuinka voimme kohdata tämän muutoksen lukemisen lisäämiseksi? Kuinka e-kirjojen tulo vaikuttaa lukemiseen? Milloin ja kuinka ihmiset kuuntelevat äänikirjoja?',
      p3:
        'Stipendistä päättää Nextoryn valitsema sisäinen raati. Arviointi perustuu opiskelijan uudenlaiselle ajattelulle, innovaatiotasolle sekä sille, kuinka hyvin opiskelija onnistuu digitalisaation ja lukemisen yhdistämisessä.',
      buttons: {
        read_more: 'Lue lisää ja hae',
      },
    },
    terms: {
      seotitle: 'Nextorystipendin yleiset ehdot syyslukukaudella 2018',
      seodescription: 'Lue ehtomme Nexyorystipendin yleiset ehdot syyslukukaudella 2018.',
      heading: 'Nextorystipendin yleiset ehdot syyslukukaudella 2018',
      p1_p1:
        'Nextory, Dalagatan 7, 111 23 Tukholma, rekisterinumero 525 118, järjestää stipendin nimeltä.',
      p1_p2:
        'Osallistuminen on ilmaista eikä osallistuminen edellytä, että hakijalla on aktiivinen tilauspalvelus.',
      links: {
        nextory_scholarship: 'Nextorystipendi',
      },
      h2: 'Osallistuessaan osallistuja suostuu automaattisesti seuraaviin ehtoihin:',
      h4: '§ 1 Stipendin muotoilu',
      p1: 'Hakaika on 1.3.2018–31.8.2018 (myöhemmin hakuaika). ',
      p2:
        'Osallistuminen on ilmaista eikä osallistujalla tarvitse olla aktiivista tilauspalvelua osallistuakseen.',
      p3: 'Tilauksen tekeminen tai uutiskirjeen tilaaminen ei lisää mahdollisuuksia voittaa.',
      p4:
        'Osallistuakseen osallistujan on mietittävä kysymystä Kuinka digitalisaatio ja kirjat voidaan yhdistää niin, että yhä useampi Ruotsissa voisi lukea? Mitkä mielestäsi ovat tärkeimmät tekijät, jotka vaikuttavat arkisiin lukutottumuksiimme digitalisaation yhteydessä ja kuinka voimme kohdata tämän muutoksen lukemisen lisäämiseksi? Saadakseen stipendin osallistujan on oltava rekisteröitynyt ruotsalaisessa yliopistossa tai korkeakoulussa kevät- tai syyslukukaudella 2018. Osallistuessaan osallistujan on lähetettävä kopio kevät- tai syyslukukauden yliopisto- tai korkeakoulurekisteröinnistä PDF-muodossa.',
      p5:
        'Nextory sisäisesti valitsee voittajan, joka saa kertasumman 15 000 kr. Voittajalle ilmoitetaan neljän viikon kuluessa hakuajan päättymisen jälkeen.',
      p6:
        'Osallistumalla osallistuja hyväksyy, että Nextory saa julkaista nimen sivulla www.nextory.fi sekä muilla Nextoryn verkkosivuilla, sosiaalisessa mediassaja muilla ulkoisilla verkkosivustoilla.',
    },
    list2: {
      h4: '§ 2 Osallistuminen ja hylkääminen',
      p1:
        'Osallistuja on oltava vähintään 18 vuotta, asuttava Ruotsissa ja pystyttävä vahvistamaan tämä. Osallistua saa vain kerran hakujaksona.',
      p2: 'Nextoryn henkilöstö ei saa osallistua.',
      p3: 'Nextory pidättää oikeudn hylätä osallistujat, jotka rikkovat yleisiä ehtoja.',
      p4:
        'Osallistuja, joka koettaa manipuloida järjestelmää antaakseen itselleen etuja, hylätään. Jos stipendi on voitettu tälla tavalla, palkinto vaaditaan takaisin.',
    },
    list3: {
      h4: '§ 3 Soveltaminen ja prosessi',
      p1:
        'Osallistujat vastaavat kysymykseen Kuinka digitalisaatio ja kirjat voidaan yhdistää niin, että yhä useampi Ruotsissa voisi lukea? Mitkä mielestäsi ovat tärkeimmät tekijät, jotka vaikuttavat arkisiin lukutottumuksiimme digitalisaation yhteydessä ja kuinka voimme kohdata tämän muutoksen lukemisen lisäämiseksi? Kuinka e-kirjojen tulo vaikuttaa lukemiseen? Milloin ja kuinka ihmiset kuuntelevat äänikirjoja?”  kirjoittamalla 1 500–2 000 sanan pituisen kirjallisen aineen.',
      p2_p1: 'Valmis hakemus lähetetään',
      p2_p2: 'mainittuna hakuajanjaksona.',
      p3:
        'Kilpailu järjestetään Ruotsissa. Vain opiskelijat, jotka ovat rekisteröityneet ruotsalaisessa yliopistossa tai korkeakoulussa saavat osallistua kilpailuun. Todistus rekisteröitymisestä on liitettävä mukaan raporttiin.',
      p4:
        'Voittajan valitsee Nextoryn valitsema raati, joka koostuu sisäisestä henkilöstöstä. Raati valitsee kilpailun voittajan panoksen sisällön mukaan, ja huomioon otetaan asiaankuuluvat ja innovatiiviset ajatukset, ideat, lähteiden käsittely ja mietteet.',
      p5:
        'Nextory ilmoittaa voittajalle kirjallisesti (s-posti) tai puhelimitse. Nextory pidättää oikeuden luovuttaa tietoja voittajasta kolmannelle osapuolelle, jos niin vaaditaan voiton toimittamiseksi tai voitosta ilmoittamiseksi. Ellei voittajaan saada yhteyttä tai ellei voittaja ota itse yhteyttä neljän viikon kuluessa voitosta ilmoittamisesta, valitaan uusi voittaja tai vaihtoehtoisesti palkinto luovutetaan hyväntekeväisyyteen. Osallistuja on velvollinen ilmoittamaan korrektit yhteystiedot.',
      p6:
        'Voittaja voi vain henkilökohtaisesti vastaanottaa palkinnon, eikä palkintoa saa luovuttaa kellekään muulle.',
      p7: 'Kysymykset ja valitukset on lähetettävä s-postitse',
    },
    list4: {
      h4: '§ 4 Kilpailun ennenaikainen lopettaminen',
      p1:
        'Nextory pidättää oikeuden lopettaa kilpailun milloin vain ilman ennakkovaroitusta. Nextorylla on oikeus päättää kilpailu teknisten ongelmien takia (esim. tietokonevirus, laitteiston tai ohjelmiston manipulointi) tai jos on muita sitä, joiden takia kilpailua ei voida toteuttaa kunnolla. Jos osallistuja aiheuttaa sen, että kilpailu on lopetettava etuajassa, Nexory voi vaatia korvausta osallistujalta.',
    },
    list5: {
      h4: '§ 5 Tietosuoja',
      p1:
        'Osallistuja hyväksyy, että Nextory tallentaa osallistujan henkilötiedot ja käyttää niitä kilpailussa. Osallistuja suostuu myös antamaan Nextorylle kaiken tarvittavan tiedon kilpailun toteuttamiseksi.',
      p2:
        'Osallistujan käyttäjätietoja kytetään vain stipendin yhteydessä ja voimassa olevien lakien mukaisesti. Nextory voi luovuttaa tietoa kolmannelle osapuolelle voidakseen mahdollistaa palkinnon toimittamisen. Tietoa ei ikinä luovuteta kolmannelle osapuolelle, ellei se liity stipendin toteuttamiseen. Poikkeus on, että tietoa voidaan luovuttaa asianomaisille viranomaisille laissa määrättyjen velvollisuuksien mukaan.',
      p3_p1:
        'Jos niin toivotaan, Nextory kertoo osallistujille, mitä tietoja heistä on tallennettu ja pyynnöstä poistaa ne järjestelmsät. Osallistujia kehotetaan lähettämään kirje Nextory, Dalagatan 7, 111 23 Tukholma tai lähettämää s-postia ',
      p3_p2: 'jos he haluavat tietonsa poistettavan.',
    },
    list6: {
      h4: '§ 6 Vastuu sisällöstä',
      p1:
        'Osallistuja vastaa kokonaan sisällöstä, jonka hän on luonut stipendiä varten. Sisältöä, joka joko on laitonta tai rikkoo voimassa olevia sääntöjä ja ehtoja vastaan, ei voi käyttää raportissa, ei myöskään sisältöä, jonka tarkoituksena on loukata, panetella, kiihdyttää tai muulla tavalla vahingoittaa muita yksilöitä.  Nextory ei vastaa mistään suorista tai epäsuorista seuraamusvahingoistga tai kustannuksista, mukaan lukien asianajajakustanukset, joita kolmas osapuoli aiheuttaa tekijänoikeudellisista syistä tai laittomalla materiaalilla osallistujan raportissa. Sama koskee vaatimuksia kilpailuehtojen puutteiden takia.',
    },
    list7: {
      h4: '§ 7 Vastuu',
      p1:
        'Nextory tekee kohtuullisen yrityksen organisoida ja toteutta kilpailu parhaalla mahdollisella tavalla.',
      p2:
        'Nextory irtisanoutuu kaikesta vastuusta, paitsi seuraavissa tapauksissa: a. Vastuu terveysvahingoista, joita aiheuttaa tahallisuus tai huolimattomuus Nextorylla tai heidän juridisella edustajallaan. b. Vastuu muista vahingoista, jotka aiheutuvat tahallisesti Nextoryn osalta tai jos Nextory tai heidän juridinen edustajansa on laiminlyönyt velvollisuutensa.',
    },
    list8: {
      h4: '§ 8 Muuta',
      p1:
        'Nextory toimii noudattaen kilpailuille määrättyjä ja sovellettavissa olevia lainmukaisia ehtoja.',
      p2_p1:
        'Osallistujat, joilla on eriävä mielipide kilpailusta, lähettävät kirjeen Nexory, Dalagatan 7,111 23 Tukholma tai s-postin',
      p2_p2: 'Nextory vastaa mahdollisimman pian.',
      p3: 'Nextorylla on oikeus muuttaa yleisiä ehtoja milloin vain stipendikaudella.',
      p4:
        'Osallistujat ovat tietoa, että palkinto, jonka kilpailusta saa, joudutaan ehkä raportoimaan voittoveroa varten Ruotsissa. Osallistuja muistaa, että hän voi olla velvollinen raportoimaan voiton paikallisille veroviranomaisille, jos Ruotsin lainsäädännössä niin sanotaan.',
    },
  },

  /**************** END STIPENDIUM PAGES ******************/

  /**************** SUBSCRIPTION TABLE ******************/
  subscription_table: {//Ref:Translation.subscription_table
    buttons: {//Ref:Translation.subscription_table.buttons
      silver: 'Hopea',
      gold: 'Kulta',
      family: 'Perhe',
    },
    prices: {//Ref:Translation.subscription_table.prices
      silver: '13,99 €',
      gold: '16,99 €',
      family: '27,99 €',
      currency: '€',
    },
    packages: {
      silver: 'Hopea',
      silverPlus: 'Hopea Plus',
      gold: 'Kulta',
      family: 'Perhe',
      family2: 'Perhe 2',
      family3: 'Perhe 3',
      family4: 'Perhe 4',
  },
    listitems: {//Ref:Translation.subscription_table.listitems
      monthly_cost: 'Kuukausimaksu',
      item1: "Hinta ilmaisjaksosi jälkeen, joka",
      item1b: 'Hinta 14 päivän vapaasta kuluttua',
      item1c: 'Kuukausihinta',
      item1d: "Hinta jälkeen sinun",
      item1e: "vapaat päivät",
      item1f: "Hinta ilmaisjaksosi jälkeen, joka päättyy",
      item2: "Käyttäjämäärä",
      item3: "Uusimmat kirjat",
      item4: 'Lue & kuuntele kännykällä ja tabletilla',
      item5: "Lue & kuuntele rajattomasti",
      item6: 'Käyttö offline-tilassa',
      item7: 'Kirjoja myös englanniksi ja ruotsiksi',
      item8: 'Näytön muokkaus auringonvalossa ja pimeässä lukemiseen',
      item9: 'Lopeta tilaus milloin haluat',
      item10: 'päättyy',
      infotext: '(2 käyttäjää + 4 €/lisäkäyttäjä)',
    },
    family: {
      price_from: 'Alkaen: ',
      users: 'käyttäjää',
      days_free_then: '14 päivää ilmaiseksi. Sen jälkeen',
      currency_per_month: '€/kk',
      choose: 'Valitse',
    },
  },
  /**************** END SUBSCRIPTION TABLE ******************/
  /**************** FORMS ******************/
  forms: {
    privaceTerm_a: 'Hyväksyn Nextoryn',
    privaceTerm_b: 'jäsenehdot',
    privaceTerm_c: 'yksityisyyden suojan',
    submit: 'Jatka',
    and: 'ja',
    labels: {
      email: 'Sähköpostiosoitteesi',
      email_placeholder: 'Sähköpostiosoitteesi',
      repeat_email: 'Sähköpostiosoitteesi uudelleen',
      password_label: 'Valitse salasana',
      password: 'Salasana',
      new_password: 'Valitse salasana',
      repeat_password: 'Toista salasana',
      cardnumber: 'Kortin numero',
      cardnumber_placeholder: 'Kortin numero',
      cardmonth: 'Viimeinen voimassaolokuukausi',
      cardmonth_placeholder: 'Kuukausi',
      cardyear: "Viimeinen voimassaolovuosi",
      cardyear_placeholder: 'Vuosi',
      cardcvv: 'Turvakoodi (CVV)',
      firstname: 'Etunimi',
      lastname: 'Sukunimi',
      address: "Katuosoite",
      postalcode: 'Postinumero',
      city: 'Postitoimipaikka',
      phone: 'Puhelinnumerosi',
      campaigncode: "Anna kampanjakoodisi",
      giftvoucher: "Anna lahjakorttisi koodi",
    },
    validation: {
      email: 'Anna kelvollinen sähköpostiosoite',
      repeat_email: "Sähköpostiosoitteet eivät täsmää",
      email_exists: "Tämä sähköpostiosoite on jo rekisteröitynyt meille.",
      Salasana: 'Anna salasana',
      password: 'Sinun täytyy valita salasana jatkaaksesi',
      password_length: 'Salasanasi on oltava 4 - 25 merkkiä',
      password_chars: 'Sallitut merkit: 0-9, a-z, A-Z ja erikoismerkit: -, @, #, $,%, _',
      password_error: 'Salasana ei näytä vastaavan.',
      password_repeat: 'Salasanat eivät täsmää',
      termserror: 'Sinun on hyväksyttävä käyttöehdot ja rehellisyyskäytäntö.',
      campaigncode: "Anna tarjouskoodi",
      campaigncode_error: 'Tämä tarjouskoodi ei ole kelvollinen.',
      giftvoucher: 'Anna lahjakoodisi',
      giftvoucher_error: "Tämä lahjakoodi ei ole kelvollinen",
      firstname: 'Anna etunimi',
      lastname: 'Ole hyvä ja anna sukunimesi',
      phone: 'Puhelinnumeron on oltava vähintään 7 ja enintään 12 numeroa',
      card_real_number_error: 'Antamasi numero ei näytä luottokortin numerolta.Ystävällinen kollaasi.',
      card_number_error: 'Vänligen fyll i ett giltigt kortnummer',
      month_error: 'Vänligen Välj månad',
      year_error: 'Vänligen Välj år',
      cvv_error: 'Fyll i CVV nummer',
      cvv_length_error: 'Three or fyra siffror',
      connection_error: '',
      warnings: {
        firstname: 'Ole hyvä ja anna etunimi',
        lastname: 'Ole hyvä ja anna sukunimesi',
        address: "Täytä osoite",
        postalcode: 'Ole hyvä ja täytä postinumero',
        city: 'Täytä missä asut',
        phone: 'Täytä puhelinnumerosi',
      },
    },
  },
  /**************** END FORMS ******************/
  /**************** API MESSAGES ******************/
  api_messages: {
    error: {
      email1_error: "Tämä sähköpostiosoite on jo rekisteröitynyt meille.",
      email2_error: 'Käyttäjää ei löytynyt kyseisellä sähköpostiosoitteella.',
      email3_error: 'Näyttää olevan ongelmia kirjautumisessa juuri nyt',
      password_error: 'Salasana ei näytä vastaavan.',
      giftvoucher_error: "Tämä koodi ei ole kelvollinen.",
      campaigncode_error: "Tämä tarjouskoodi ei ole kelvollinen.",
      connection_error: "Meillä on teknisiä ongelmia, yritä uudelleen pian",
      other_region_error: 'Olet käyttäjä toisella maantieteellisellä alueella',
    },
  },
  /**************** END API MESSAGES ******************/

  /**************** INTEGRITY PAGES ******************/
  integrity_pages: {
    content: `<h1> Tietosuoja </h1>
  <p>
    Nextory on digitaalinen tilauspalvelu, joka antaa sinulle rajoittamattoman lukemisen e-kirjoja ja
    äänikirjat. palvelu toimittaa Nextory AB, organisaationumero 556708-4149.
  </p>
  <p>
    Tämä tietosuojakäytäntö kertoo, mitä henkilötietoja Nextory käsittelee
    että rekisteröit tilin meille ja käytät Nextorin palvelua. Voimme myös hoitaa
    Tiedot, kun käytät sovellusta tai ostoa ilman rekisteröitymistä
    lahjakortti meiltä.
  </p>
  <p>
    Sinun koskemattomuus on meille tärkeä, ja olet aina turvallinen, kun lähdet
    henkilötietoja lähimmälle. Henkilökohtaisten tietojen käsittely on sopusoinnussa
    nykyiset lait ja asetukset, mukaan lukien tietosuojasäädös ("GDpR").
  </p>
  <p>
    Nextory on tehnyt teknisiä ja organisatorisia toimenpiteitä sen varmistamiseksi
    Henkilötietoja suojataan luvattomalta käytöltä ja käyttöön.
  </p>
<h3> personuppgiftsansvarig </h3>
  <p>
    Nextory on henkilökohtaisesti vastuullinen henkilö, joka vastaa henkilötietojesi käsittelystä. Onko sinulla mitään
    Kysymyksiä tästä olet tervetullut ottamaan yhteyttä henkilökohtaiseen asiakaspalveluun sähköpostitse
    kundservice@nextory.se tai puhelinnumero 08-411 17 15.
  </p>
  <h3> Henkilötietojen keruuajat </h3>
  <p> Nextory kerää henkilökohtaisia ​​tietoja sinusta: </p>
  <ul>
    <li>
      Kun rekisteröidät tilin Nextoryn kanssa ja käytät palvelua. Tämä pätee myös
      kun luot tilin, mutta älä kirjoita maksutietoja.
    </li>
    <li>
      Ota yhteyttä Nextorin asiakaspalveluun tai pyydä meiltä tietoja.
    </li>
    <li>
      Rekisteröitymisen yhteydessä sähköpostitse tai tilaajana
      viestintä, kuten push-ilmoitukset.
    </li>
    <li>
      Kun jakat tietoja Nextoryn kautta asiakastutkimuksissa tai
      muita muotoja.
    </li>
    <li>
      Kun käytät tarjouskoodia tai osallistut tarjouskilpailuun
      jonka on julkaissut Nextory.
    </li>
  </ul>
  <p>
    Nextory kerää myös tietoja omien tai kolmansien osapuolten evästeiden ja muiden vastaavien kautta
    seuranta tekniikoita, jotka voivat kirjautua toimintaan ja valintoihin, kun käytät
    palvelumme tai vierailla Nextorin verkkosivuilla.
  </p>
  <h3> henkilötietolain </h3>
  <p>
    Nextory käsittelee lähinnä tehtäviä, joita annoit meille yhteydessä
    rekisteröintiä. Nämä tehtävät voivat olla nimi, sähköpostiosoite, salasana ja puhelinnumero.
    Ilmoittautumisen yhteydessä annat myös tiedot maksutavan valinnasta. meidän
    Alihankkijat ja pankki tarjoavat turvallisia maksuratkaisuja, joissa maksutietosi ovat
    suojattu. Nextory ottaa huomioon joitakin tietoja maksutavastamme
    palveluntarjoajille. Saat esimerkiksi maksukortin voimassaoloa koskevia tietoja osoitteeseen
    Muistuttaa sinua, kun vanhentumispäivä päättyy, jotta voimme tarjota palvelumme ilman
    häiriöt.
  </p>
  <p> Seuraava henkilö voi käsitellä seuraavia tietoja. </p>
  <ul>
    <li>
      Tehtävät, jotka tarjoavat henkilökohtaisemman yhteyden seuraavan käyttäjän ja seuraavan käyttäjän välillä:
      etunimi, sukunimi ja käyttäjätunnus.
</li>
    <li>
      Käyttäjien ylläpitämiseen ja hyvän palvelun tarjoamiseen liittyvät tehtävät:
      Yhteystiedot, kuten sähköpostiosoite, majoitus ja puhelinnumero.
    </li>
    <li>
      Tiedot liiketoimien suorittamiseen: tietoja maksutavasta (esim.
      maksukortti 4 viimeistä numeroa ja voimassaoloaikaa) sekä tietoja
      maksuhistoriaasi.
    </li>
    <li>
      Tiedot, joilla varmistetaan palvelun ja tarjonnan hyvä laatu
      Käyttäjän suojaus palvelua käytettäessä: salasana, laitteen tyyppi,
      sovellusversio, sovellusasetukset, otsikko ja ISBN kirjassa.
    </li>
    <li>
      Tehtävät tarjota asiakkaillemme yksilöllinen räätälöinti sekä tarjouksen
      asiakkaitamme parhaita kirjan vinkkejä ja suosituksia: lukeminen ja kuunteleminen,
      hakuhistoria, tallennetut kirjat, kirjojen luokittelu, sovellusten käyntien määrä ja aika
      käytettiin sovelluksessa, painikkeen painalluksia ja lisättyjä profiileja
      perhe tilaus.
    </li>
    <li>
      Tiedot parantaa palvelumme ja täyttävät asiakkaidemme toiveet ja
      Odotukset: kyselyvastaukset ja vastaukset kyselyjen avulla (esim. Asiakastutkimukset ja
      syyt tilauksen irtisanomiseen).
    </li>
    <li>
      Tiedot tiedonvaihdon parantamiseksi asiakkaidemme kanssa: vuorovaikutus
      sähköpostit, sovelluksen viestit ja push-ilmoitukset kaikki. puheluita.
    </li>
    <li>
      Tiedot parantaa palveluamme ja täyttävät asiakkaiden vaatimukset ja
      takaisinotto: tiedot, jotka asiakas on toimittanut asiakaspalvelumme yhteydessä
      asiakasasiat esim vianmäärityksestä ja tuesta.
    </li>
  </ul>
  <br/>
  <h3> Aikarajaus henkilötietojen hallinnassa </h3>
  <p>
palvelun lopussa asiakkaan tili keskeytetään kahden vuoden ajan. Tämä tarkoittaa
    että asiakkaalla on samat henkilökohtaiset tiedot ja käyttäjätunnukset, jotka on liitetty hänen tiliinsä alla
    tätä ajanjaksoa. Nextory voi siten tarjota asiakkaalle palvelun, jotta hänen tiliään ja
    Tämän ajanjakson aikana ylläpidä lukuhistoriaa ja asiakaslehtisiä lukulistoja.
  </p>
  <p>
    Nextory myös säästää tehtäviä tänä ajanjaksona väärinkäytön ehkäisemiseksi
    vapaa-ajanjaksoja ja muita myynninedistämistarjouksia, jotka ovat voimassa vain kerran asiakkaalta.
  </p>
  <p>
    Jotkin henkilökohtaiset tiedot voidaan tallentaa kauemmin kuin kaksi vuotta, jotta
    Nextory AB voi täyttää lainsäädännölliset velvoitteensa muun lain, kuten
    esim. Kirjanpitolakia.
  </p>
  <h3> Mitä me teemme henkilötietojesi kanssa? </h3>
  <p>
    On monia syitä, miksi Nextory käsittelee henkilökohtaisia ​​tietoja, mutta ensisijaisesti
    me toimitamme palvelun sinulle. Katso esimerkki alla.
  </p>
  <p>
    <strong> Hallinta. </strong> käsittelemme tietoja, kuten sähköpostiosoitetta, salasanaa,
    palvelun käyttämiseen liittyvä asiakastunnus ja maksutiedot.
  </p>
  <p>
    <strong> Kehittäminen. </strong> Tietoja käyttäytymisestä sovelluksessa (esim
    puuttuu ja jos käytetään uusia ominaisuuksia) kehitämme ja parannamme
    palvelua. Käytämme myös asiakkaiden kyselytutkimuksia, jotta voimme kehittää Nextorya.
    Henkilökohtaiset tiedot voivat olla pohjana analyyseille siitä, miten asiakaskunta käyttäytyy
    parasta palvelua asiakkaidemme parissa.
  </p>
<p>
    <strong> personal Experience. </strong> Information about using the service (e.g.
    what books are you reading or listening to, what genres you prefer and how often you are
    using Nextory) can be used to give you personal recommendations or targeted
    offers. We may also use personal data on platforms outside of Nextory, as
    such as Facebook.
  </p>
  <p>
    <strong> prevent fraud. </strong> Your personal information can also be used to
    maintain our member terms and prevent fraud (eg abuse of
    sample periods). We also share personal information with companies outside Nextory, which work
    as personal information assistants. They may not use personal data for their own purposes and
    Their rights are governed by agreements with us. Companies with access to
    personal data uses these to assist us with customer service, delivering communications
    to customers and manage customer surveys.
  </p>
  <h3> Legal basis for treatment </h3>
  <p>
    Nextory processes personal information to provide and administer the service.
    Information required for this is e-mail address, password and payment details.
  </p>
  <p>
    We also deal with tasks like your book history, where Nextori's interest in treating
    These user charges weigh heavier is the risk of your privacy as it could
    means.
  </p>
  <p>
    Certain personal information is processed with your consent. For example, you can choose to
    unsubscribe from marketing, but still use our service.
  </p>
  <h3> Security </h3>
  <p>
    Nextory has taken technical and administrative measures to protect security
    around your personal information from unauthorized use and change. It is the responsibility
    User to protect their login information so that unauthorized users can not log in
    account. Should this be suspected, the user should immediately contact Nextorys
    customer service.
  </p>
  <h3> Updates to privacy policy </h3>
  <p>

Nextory has the right to adjust the privacy policy. Should we carry out essentials
    Changes will be notified via email or in the service.
  </p>
  <h3> Your rights </h3>
  <p>
    If you have questions regarding our processing of personal Data, please contact
    kundservice@nextory.se. You can also turn around if you wish to practice any of yours
    rights under the Data protection Ordinance. Your rights in the Data protection Ordinance
    comprising:
  </p>
  <ul>
    <li>
      Right of access - Article 15 of the Data protection Ordinance has a registered
      the right to access their personal data and certain information regarding
      processing them. The information is shown in this document.
    </li>
    <li>
      Right to rectification - Article 16 of the Data protection Ordinance has a registered
      the right to have incorrect personal information corrected and to supplement incomplete
      personal data.
    </li>
    <li>
      Right to erasure - Under certain circumstances, a registered person has been registered under Article 17 of
      Data protection Ordinance entitled to have their personal data deleted.
    </li>
    <li>
      Right to limit treatment - Under certain circumstances, a registered person has
      according to Article 18 of the Data protection Ordinance, the right to restrict the treatment as it
      the manager takes.
    </li>
<li>
      Right to data loss - According to Article 20 of the Data protection Ordinance, one
      registered right to receive their personal data (or, in certain circumstances, obtain
      those transferred to another person responsible) in a structured, widely used
      and machine-readable format.
    </li>
    <li>
      Right to object - According to Article 21 of the Data protection Ordinance, one
      registered under certain circumstances the right to object to the treatment of
      personal data. This applies to treatment based on interest balance, such as
      for the purpose of the information.
    </li>
  </ul>
  <br/>
  <p> Nextory AB, June 1, 2018 </p>`,
  },
};

export default translation_fi;
