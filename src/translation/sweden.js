const translation_se = {
  components: {
    slider: {
      button: 'Visa alla',
    },
  },
  app: {
    seotitle: 'Ljudböcker & E-böcker - Lyssna & läs gratis i mobilen',
    seodescription:
      'Prova tusentals Ljudböcker och E-böcker Gratis i 14 dagar! Ladda ner appen och streama i din iPhone, Android eller iPad var du än är med Nextory.',
    notifications: {
      p1:
        'Dina betalningsuppgifter har blivit uppdaterade och ditt abonnemang fortsätter tillsvidare. Tack för att du fortsätter att vara abonnent hos oss.',
      p2: 'Ditt abonnemang kommer att ändras',
      p3: 'Negradering avbruten.',
      p4: 'Ditt konto är aktiverat igen.',
      p5: 'Kampanjkoden är registrerad.',
    },
    common: {
      silver: 'Silver',
      silverplus: 'Silver Plus',
      gold: 'Guld',
      family: 'Familj',
      familyplus: 'Familj Plus',
      family2: 'Familj 2',
      family3: 'Familj 3',
      family4: 'Familj 4',

      silver_capital: 'SILVER',
      gold_capital: 'GULD',
      family2_capital: 'FAMILJ 2',
      family3_capital: 'FAMILJ 3',
      family4_capital: 'FAMILJ 4',
    },
  },
  cookiebanner: {
    p1:
      'På nextory.se använder vi cookies för att ditt besök ska bli så bra som möjligt. Genom att fortsätta använda vår webbplats accepterar du att cookies används.',
    link: 'Läs mer.',
    button: 'Jag förstår',
  },
  menu: {
    logo_alttext: 'Nextory logotyp',
    navigation: {
      ebooks: 'E-böcker',
      soundbooks: 'Ljudböcker',
      help: 'Hjälp',
    },
    buttons: {
      try_free: 'Prova Gratis i 14 dagar',
      login: 'Logga in',
      logout: 'Logga ut',
      complete_registration: 'Slutför registrering',
      listen_to_the_phone: 'Lyssna i mobilen',
      enable_nextory_now: 'Aktivera Nextory nu',
      my_account: 'Mitt konto',
    },
  },
  views: {
    cookies: {
      heading: 'Om Cookies',
      paragraph:
        'Nextory använder cookies när du besöker vår websida. En cookie är en informationsfil som lagras i användarens dator. Cookien kan inte identifiera dig personligen, endast den webbläsare som finns installerad på din dator och som du använder vid besöket. Om du inte vill tillåta lagring av cookies i din dator kan du hindra det genom inställningarna i din webbläsare. Cookien lagras under den tid du är ansluten till tjänsten, eller under så lång tid uppgifterna behövs för att kunna uppfylla de förpliktelse eller rättigheter som är förknippade med tjänsten.',
    },
  },

  /**************** NEW HOMEPAGE ******************/
  /* New HomePage Menu Section */
  newhomepage: {
    menu: {
      navigation: {
        ebooks: 'E-böcker',
        soundbooks: 'Ljudböcker',
        help: 'Hjälp',
        my_account: 'Mitt konto',
        login: 'Logga in',
        logout: 'Logga ut',
      },
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        listen_to_the_phone: 'Lyssna i mobilen',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
        therefore_nextory: 'Därför Nextory',
      },
    },

    /* New HomePage Hero Section */
    hero: {
      text: 'Läs och lyssna var du vill. <br>Avsluta när du vill.',
      subheading: 'Obegränsad tillgång till e-böcker och ljudböcker i din mobil eller surfplatta. Från 139 kr/mån.',
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
      },
      images_alt_text: {
        hejdasaker: 'Bokomslag Hejdåsaker',
        paxmaran: 'Bokomslag PaxMaran',
        vildplockat: 'Bokomslag Vildplockat',
        halsorev: 'Bokomslag Hälsorevolutionen',
        handboksuperhjaltar: 'Bokomslag Handbokförsuperhjältar',
        omgivenavidioter: 'Bokomslag Omgivenavidioter',
        traskkungensdotter: 'Bokomslag Träskkungensdotter',
        glommig: 'Bokomslag Glömmig',
        densistaparrish: 'Bokomslag DensistaMrsParrish',
        foodpharmacy: 'Bokomslag ',
        hjarnstark: 'Bokomslag Hjärnstark',
        sveasson: 'Bokomslag Sveasson',
        storstavallt: 'Bokomslag Störstavallt',
        factfullnes: 'Bokomslag Factfulnes',
        cover1793: 'Bokomslag 1793',
        siggesally: 'Bokomslag SiggeochSally',
      },
    },

    /* New HomePage iPhone Section */
    iphone: {
      heading: 'Läs eller lyssna',
      paragraph1: {
        heading: 'Dina favoritförfattare',
        content: 'Med tiotusentals böcker att välja bland kan du hitta böckerna som du gillar.',
      },
      paragraph2: {
        heading: 'Hitta rätt bok',
        content: 'Du hittar snabbt bra böcker i topplistorna eller genom boktipsen.',
      },
      paragraph3: {
        heading: 'Avsluta när du vill',
        content: 'Du har ingen bindningstid och kan byta eller avsluta abonnemanget när du vill.',
      },
    },

    /* New HomePage iPad Section */
    ipad: {
      text: 'Vad läser ni ikväll?',
    },

    /* New HomePage Children Section */
    children: {
      text:
        'Med Nextory behöver du aldrig tröttna på en barnbok. Välj bok efter ålder eller genre. Med familjeabonnemang kan ni skapa användare med endast barnböcker.',
      images_alt_text: {
        falletskattkartan: 'Bokomslag Fallet Skattkartan ',
        supercharlie: 'Bokomslag Super Charlie ',
        fiffigakroppen: 'Bokomslag Saga-sagor',
        slottsmysteriet: 'Bokomslag Slottsmysteriet',
        elefanten: 'Bokomslag Elefanten som så gärna ville somna ',
        mammamu: 'Bokomslag Mamma Mu åker bobb ',
        pellesvanslos: 'Bokomslag Pelle Svanslös ',
        paxmaran: 'Bokomslag Pax Maran ',
        lassemaja: 'Bokomslag Biografmysteriet ',
        samsigge: 'Bokomslag Sam och Sigge ',
        letaspoken: 'Bokomslag Ingrid och Ivar ',
        handboksuperhjaltar: 'Bokomslag Handbok för Superhjältar ',
        rum123: 'Bokomslag Rum 123',
        bajsboken: 'Bokomslag Bajsboken',
        vemsbyxor: 'Bokomslag Vems Byxor ',
        harbrandbild: 'Bokomslag Här kommer brandbilen ',
      },
    },

    /* New HomePage Sub Section */
    subsection: {
      heading: 'Böckerna väntar',
      currency: 'kr/mån',
      features: {
        owl: 'Du läser och lyssnar hur mycket du vill',
        clock: 'Pausa, byt eller avsluta abonnemanget när som helst',
        devices: 'Nextory fungerar på både mobiler och surfplattor',
      },
      package1: {
        heading: 'Silver',
        content: {
          p1: '14 dagar fritt, sedan',
          p2: 'Senaste böckerna',
          p3: '1 användare',
        },
      },
      package2: {
        heading: 'Guld',
        content: {
          p1: '14 dagar fritt, sedan',
          p2: 'Hela utbudet',
          p3: '1 användare',
          p4: 'Mest populärt',
        },
      },
      package3: {
        heading: 'Familj',
        content: {
          p1: '14 dagar fritt, sedan från',
          p2: 'Hela utbudet',
          p3: '2-4 användare',
          p4: 'Flest nöjda kunder',
        },
      },
    },

    /* New HomePage Ratings Section */
    ratings: {
      rating1: {
        text: 'Gillar du att läsa eller lyssna på en bok så kan jag rekommendera denna app.',
        user: 'KeitaroM',
      },
      rating2: {
        text:
          'Jag tycker appen är lätt att använda, finns ett stort urval av böcker och ljudböcker. Appen passar för alla åldrar. Det går mycket bra att använda appen utan wi-fi.',
        user: 'Tindrad',
      },
      rating3: {
        text: 'Bästa beslut jag har gjort! Är supernöjd :)',
        user: 'Jenniebengtsson',
      },
      disclaimer: '5 499 kunder i app-store ger Nextory 4,5 av 5',
    },

    /* New HomePage Footer Section */
    footer: {
      heading: 'Kontakta oss på 08-411 17 15',
      links: {
        press: 'Press',
        apps: 'Appar',
        om_nextory: 'Om Nextory',
        giftcards: 'Presentkort',
        job: 'Jobb',
        questions_and_answers: 'Frågor och svar',
        campaign_code: 'Kampanjkod',
        sell_via_nextory: 'Sälj via Nextory',
      },
      copyrights: {
        copyright_text: 'Copyright ©',
        nextory_ab: 'Nextory AB',
        terms: 'Villkor',
        cookies: 'Cookies',
      },
    },

    /* New HomePage Seo Section */
    seo: {
      find_audio_books: {
        heading: 'Hitta ljudböcker att lyssna på',
        content:
          'Om du vill lyssna på ljudböcker direkt i din iphone, android eller surfplatta får du genom Nextory tillgång till ett gediget bibliotek med böcker och författare. Du får obegränsad tillgång till de mest populära titlarna inom genrer som deckare, romantik, fantasy och ljudböcker för barn direkt i mobilen. Prova gratis och lyssna sedan obegränsat från 139 kr/mån.',
      },
      listen_to_audio: {
        heading: 'Lyssna till en ljudbok vid varje tillfälle',
        content: {
          p1:
            'För dig som är intresserad av Nextory och vill se mer av våra böcker rekommenderar vi att du söker på hemsidan och botaniserar bland hela vårt utbud av ljudböcker. Hos Nextory finns tusentals böcker i de olika formaten ljudbok eller e-bok att lyssna på i spelaren i vår app.',
          p2:
            'Skillnaden mellan Nextory och att låna från bibliotek är att du hos Nextory får tusentals e-ljudböcker tillgängliga direkt i mobilen. Till en fast månadsavgift kan du lyssna på de senaste nyheterna när du vill, var du vill. Till skillnad från att streama ljudböcker erbjuder Nextory möjligheten att lyssna på alla böcker i offline-läge.',
          p3:
            'Även om du inte är beredd att betala för tjänsten i nuläget men ändå är intresserad av att ladda ner ljudböcker, kan du klicka på knappen Prova Gratis i 14 Dagar för att testa i lugn och ro.',
        },
      },
      ebooks_for_all: {
        heading: 'E-böcker för alla',
        content:
          'Här hittar du ett stort digitalt bibliotek av e-böcker till din mobil eller surfplatta. Böckerna kan du läsa i Nextorys app direkt i din iPhone och Android-telefon eller surfplatta. Genom Nextory får du obegränsat med läsning, var du vill, när du vill, direkt i mobilen.',
      },
      information_about_ebooks: {
        heading: 'Information om e-böcker',
        content: {
          p1:
            'En e-bok är en digital bok som går att läsa direkt i en mobil enhet, utan att du behöver vara online. Att läsa böcker via appar har blivit mer populärt för varje år. En förklaringsfaktor är den ökade användningen av smartphones och surfplattor. Genom Nextory får du tusentals boktitlar och författare samlade på en och samma plats och därmed omedelbar tillgång till dessa utan att behöva låna från biblioteket eller leta på nätet.',
          p2:
            'Prova att läsa våra e-böcker gratis genom en provperiod på 14 dagar. Mer information om vårt utbud och alla våra titlar hittar du under respektive kategori eller genom att klicka på böckerna här ovan. Du hittar allt från spännande nyheter, populära författare, topplistor samt en uppsjö av böcker inom olika genrer.',
        },
      },
    },


    /* New Summer HomePage Hero Section */
    summerHero: {
      text: 'Läs och lyssna var du vill. Avsluta när du vill.',
      subheading:
        'Obegränsad tillgång till e-böcker och ljudböcker i din mobil eller surfplatta. Från 139 kr/mån.',
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
      },
      images_alt_text: {
        Book1: 'Husmoderns död',
        Book2: 'Hinsides väktare',
        Book3: 'Supergott och supersnabbt',
        Book4: 'Hälsorevolutionen',
        Book5: 'De vises sten',
        Book6: 'Alltid din dotter',
        Book7: 'Ett mörker mitt ibland oss',
        Book8: 'Omgiven av idioter',
        Book9: 'Syndabocken',
        Book10: 'Factfullnes ',
        Book11: 'Vildplockat',
        Book12: 'Den frusna elden',
        Book13: 'Konsten att hålla sig flytande',
        Book14: '12 livsregler',
        Book15: 'Hunger alma katsu',
        Book16: 'ödesryttarna',
      },
    },

    /* New Summer HomePage iPhone Section */
    summerIphone: {
      heading: 'Alltid en bra bok',
      paragraph1: {
        heading: 'När och var du vill',
        content: 'På stranden, under utlandsresan, i fikakön. Var du än befinner dig har du hela vårt sortiment med dig.',
      },
      paragraph2: {
        heading: 'Massor av böcker',
        content: 'Vårt utbud av ljudböcker och e-böcker kommer du aldrig att tröttna på. Både på svenska och engelska.',
      },
      paragraph3: {
        heading: 'Avsluta när du vill',
        content: 'Du har ingen bindningstid och kan byta eller avsluta abonnemanget när du vill.',
      },
    },

    /* New Summer HomePage iPad Section */
    summerIpad: {
      text: 'Njut av sommaren',
    },

    /* New Summer HomePage Children Section */
    summerChildren: {
      heading1: 'Offline-läge',
      text1: 'Ladda ner dina böcker inför resan så slipper du tänka på wifi.',
      heading2: 'Sommarens mat',
      text2: 'Ligg i hängmattan och hitta sommarens matinspiration. Botaniseras bland allt från smarriga sallader till mumsiga chokladbollar.',
      heading3: 'Läs i solen',
      text3: 'Sol eller moln? Glömt läsglasögonen? Mörk bakgrund för nattläsning? Inga problem, hos oss anpassar du skärmen efter dina behov.',
      heading4: 'Barnböcker',
      text4: 'Vi har böcker för barn i alla åldrar. Hos oss kan barnen dessutom ha sin egen profil anpassad för dem.',

      images_alt_text: {
        Book1: 'Sallader året runt',
        Book2: 'Hinsides väktare',
        Book3: 'Vego på 30 min',
        Book4: 'Pirater',
        Book5: 'Supergott och supersnabbt',
        Book6: 'Min bästis målvakten',
        Book7: 'Frukost hela dagen',
        Book8: 'De vises sten',
        Book9: 'Japansk grillning',
        Book10: 'ödesryttarna ',
        Book11: 'Det gröna skafferiet',
        Book12: 'Handbok för superhjältar 3',
        Book13: 'Baka med godis',
        Book14: 'Till alla killar',
        Book15: 'Chokladbollar',
        Book16: 'Bajsboken',
      },
    },

    /* New Summer HomePage Ratings Section */
    summerRatings: {
      rating1: {
        text: 'Gillar du att läsa eller lyssna på en bok så kan jag rekommendera denna app.',
        user: 'KeitaroM',
      },
      rating2: {
        text:
          'Jag tycker appen är lätt att använda, finns ett stort urval av böcker och ljudböcker. Appen passar för alla åldrar. Det går mycket bra att använda appen utan wi-fi.',
        user: 'Tindrad',
      },
      rating3: {
        text: 'Bästa beslut jag har gjort! Är supernöjd :)',
        user: 'Jenniebengtsson',
      },
      disclaimer: '7 413 kunder i app-store ger Nextory 4,5 av 5',
    },


  },
  /**************** END NEW HOMEPAGE ******************/




  /**************** HOMEPAGE/B or OLD HOMEPAGE ******************/
  /* HomePage/b Hero Section */
  homepage: {
    seotitle: 'Ljudböcker & E-böcker - Lyssna & läs gratis i mobilen',
    seodescription:
      'Prova tusentals Ljudböcker och E-böcker Gratis i 14 dagar! Ladda ner appen och streama i din iPhone, Android eller iPad var du än är med Nextory.',
  },
  hero: {
    heading_part1: 'Läs och lyssna',
    heading_part2: '-var som helst, när som helst',
    subheading_part1: 'Obegränsad tillgång till E-böcker och Ljudböcker',
    subheading_part2: 'i din surfplatta eller mobil. Från 139 kr/mån.',
    buttons: {
      try_free: 'Prova Gratis i 14 dagar',
      complete_registration: 'Slutför registrering',
      enable_nextory_now: 'Aktivera Nextory nu',
    },
  },

  /* HomePage/b Tab Section */
  tabs: {
    books: {
      tabname: 'Så många böcker',
      heading: 'Tusentals E-böcker och Ljudböcker från dina favoritförfattare.',
      links: {
        popular_ebooks: 'Populära e-böcker',
        popular_audiobooks: 'Populära ljudböcker',
        news: 'Nyheter',
      },
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
        show_all: 'visa alla',
      },
    },
    listen: {
      tabname: 'Läs och lyssna var du vill',
      heading: 'Upptäck din bokfrihet med de senaste titlarna direkt i din ficka.',
      subheading: 'Obegränsat med e-böcker och ljudböcker i mobilen och surfplattan.',
      text: 'Finns både för iOS och Andriod.',
      img: {
        alt_text1: 'Nextory app ikon',
        alt_text2: 'Nextory på olika enheter',
      },
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
      },
    },
    price: {
      tabname: 'Välj ditt pris',
      heading: 'Välj abonnemang för att läsa och lyssna hur mycket du vill hos Nextory.',
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
      },
    },
    abort: {
      tabname: {
        p1: 'Inga förpliktelser',
        p2: 'avsluta när du vill',
      },
      heading: 'Känner du att Nextory inte passar dig? Ingen bindningstid. Avsluta när du vill.',
      buttons: {
        try_free: 'Prova Gratis i 14 dagar',
        complete_registration: 'Slutför registrering',
        enable_nextory_now: 'Aktivera Nextory nu',
      },
      images_alt_text: 'Nextory on different units',
    },
  },

  /* HomePage/b Seo Section */
  seo: {
    find_audio_books: {
      heading: 'Hitta ljudböcker att lyssna på',
      content:
        'Om du vill lyssna på ljudböcker direkt i din iphone, android eller surfplatta får du genom Nextory tillgång till ett gediget bibliotek med böcker och författare. Du får obegränsad tillgång till de mest populära titlarna inom genrer som deckare, romantik, fantasy och ljudböcker för barn direkt i mobilen. Prova gratis och lyssna sedan obegränsat från 139 kr/mån.',
    },
    listen_to_audio: {
      heading: 'Lyssna till en ljudbok vid varje tillfälle',
      content: {
        p1:
          'För dig som är intresserad av Nextory och vill se mer av våra böcker rekommenderar vi att du söker på hemsidan och botaniserar bland hela vårt utbud av ljudböcker. Hos Nextory finns tusentals böcker i de olika formaten ljudbok eller e-bok att lyssna på i spelaren i vår app.',
        p2:
          'Skillnaden mellan Nextory och att låna från bibliotek är att du hos Nextory får tusentals e-ljudböcker tillgängliga direkt i mobilen. Till en fast månadsavgift kan du lyssna på de senaste nyheterna när du vill, var du vill. Till skillnad från att streama ljudböcker erbjuder Nextory möjligheten att lyssna på alla böcker i offline-läge.',
        p3:
          'Även om du inte är beredd att betala för tjänsten i nuläget men ändå är intresserad av att ladda ner ljudböcker, kan du klicka på knappen Prova Gratis i 14 Dagar för att testa i lugn och ro.',
      },
    },
    ebooks_for_all: {
      heading: 'E-böcker för alla',
      content:
        'Här hittar du ett stort digitalt bibliotek av e-böcker till din mobil eller surfplatta. Böckerna kan du läsa i Nextorys app direkt i din iPhone och Android-telefon eller surfplatta. Genom Nextory får du obegränsat med läsning, var du vill, när du vill, direkt i mobilen.',
    },
    information_about_ebooks: {
      heading: 'Information om e-böcker',
      content: {
        p1:
          'En e-bok är en digital bok som går att läsa direkt i en mobil enhet, utan att du behöver vara online. Att läsa böcker via appar har blivit mer populärt för varje år. En förklaringsfaktor är den ökade användningen av smartphones och surfplattor. Genom Nextory får du tusentals boktitlar och författare samlade på en och samma plats och därmed omedelbar tillgång till dessa utan att behöva låna från biblioteket eller leta på nätet.',
        p2:
          'Prova att läsa våra e-böcker gratis genom en provperiod på 14 dagar. Mer information om vårt utbud och alla våra titlar hittar du under respektive kategori eller genom att klicka på böckerna här ovan. Du hittar allt från spännande nyheter, populära författare, topplistor samt en uppsjö av böcker inom olika genrer.',
      },
    },
  },

  /* HomePage/b Footer Section */
  footer: {
    heading: 'Frågor? Kontakta oss på 08-411 17 15',
    links: {
      press: 'Press',
      apps: 'Appar',
      nextory_ab: 'Nextory AB',
      giftcards: 'Presentkort',
      membership_terms: 'Medlemsvillkor',
      integrity_terms: 'Integritetspolicy',
      job: 'Jobb',
      questions_and_answers: 'Frågor och svar',
      to_cookies: 'Om cookies',
      campaign_code: 'Kampanjkod',
      sell_via_nextory: 'Sälj via Nextory',
    },
    copyrights: {
      copyright_text: 'Copyright ©',
      address: {
        p1: 'Nextory AB  |  Org. Nr: 556708-4149 ',
        p2: 'Dalagatan 7, 111 23 Stockholm  |  08-411 17 15',
      },
    },
  },
  /**************** END HOMEPAGE/B or OLD HOMEPAGE ******************/

  /**************** SEARCH PAGE ******************/
  /*Search Page*/
  search: {
    placeholder: 'Sök bok...',
    type: 'Typ',
    all: 'Alla',
    ebooks: 'E-böcker',
    soundbooks: 'Ljudböcker',
    what_are: 'Vad söker du? Skriv i rutan här ovan.',
    loading_results: 'Laddar sökresultat för ',
    search_results: 'Sökresultat',
    found_nothing_on: 'Hittade inget på',
    searchform_error: 'Hoppsan, nåt gick fel',
  },
  /**************** END SEARCH PAGE ******************/

  /**************** EBOOKS PAGE ******************/
  ebooks: {
    heading: 'E-böcker',
    books: {
      heading: 'Böcker',
    },
    category: {
      seotitle: 'Ladda ner Gratis Ljudbok/E-bok Online',
      seodescription: 'Hitta din favorit bland nätets största utbud av Ljudböcker & E-böcker!',
      messages: {
        error: 'Hoppsan, nåt gick fel',
        loading: 'Laddar böcker',
      },
      listall: {
        seotitle: 'Ladda ner Gratis Ljudbok/E-bok Online',
        seodescription: 'Hitta din favorit bland nätets största utbud av Ljudböcker & E-böcker!`',
      },
    },
    singlebook: {
      bookdetails: {
        read_as_ebook: 'Finns som e-bok',
        listen_as_audio: 'Finns som ljudbok',
        buttons: {
          try_free: 'Prova boken gratis',
          listen_on_mobile: 'Lyssna på boken i mobilen',
        },
        and: "och",
        _Of: "Av ",
        more_info: 'Mer information',
        narrator: 'Uppläsare',
        length: 'Längd',
        of_the: 'del',
        series: 'Serie',
        type: 'Typ',
        format: {
          ebook: 'E-bok',
          audiobook: 'Ljudbok',
          ebook_and_audiobook: 'E-bok, Ljudbok',
        },
        included_in: 'Ingår i',
        published: 'Utgiven',
        translator: 'Översättare',
        language: 'Språk',
        isbn: 'ISBN',
        publishers: 'Förlag',
      },
      messages: {
        fetchposts: 'Hoppsan, nåt gick fel.',
      },
      all_books_in_the_series: 'Alla böcker i serien',
    },
    soundbooks: {
      heading: 'Ljudböcker',
    },
    toplist: {
      seotitle: 'Ladda ner Gratis Ljudbok/E-bok Online',
      seodescription: 'Hitta din favorit bland nätets största utbud av Ljudböcker & E-böcker!',
      messages: {
        error: 'Hoppsan, nåt gick fel',
        loading: 'Laddar böcker',
      },
    },
    error: 'Hoppsan, nåt gick fel.',
  },
  /**************** END EBOOKS PAGE ******************/

  /**************** ACCOUNT PAGE ******************/
  account: {
    changepass: {
      presubmit:
        'Det verkar vara fel på länken du fått i ditt mail från oss. Här kan du skicka ett nytt mail för att återställa ditt lösenord.',
    },
    changepayment: {
      notifyTrustlyFail:
        'Oj, din registrering med Mobilt BankID avbröts! Vänligen prova igen. Funkar det inte, säkerställ att du har inte täckning i ditt bankkonto. Annars kontakta din bank.',
      heading: 'Uppdatera dina betalningsuppgifter.',
      listitem: 'Inga förpliktelser, avsluta online när du vill.',
      paymenttitle: 'Välj betalsätt',
      secure_server: 'Säker server (SSL-krypterad)',
      credit_or_debit_card: 'Kredit- eller bankkort',
      mobile_bankid: 'Mobilt BankID',
    },
    changesubscription: {
      heading: 'Byt abonnemang',
      subheading: 'Du kan när som helst upp- eller nedgradera ditt abonnemang. Vid nästa betaltillfälle kommer du debiteras för ditt nya abonnemang.',
      buttons: {
        save_current: 'Spara nuvarande abonnemang',
        change_to: 'Byt till',
        change_to_b: 'Ändra till',
        currency_per_month: 'kr/mån',
      },
      currency: 'kr',
      content: {
        p1: 'Om du väljer att nedgradera från Familj till Guld kommer du att gå miste om familjeabonnemanget med upp till 4 profiler.',
        p2: 'Om du väljer att nedgradera från Familj till Silver kommer du att gå miste om familjeabonnemanget med upp till 4 profiler samt de senaste böckerna.',
        p3: 'Klicka på Ändra till Familj för att uppgradera från Guld till Familj. Detta innebär att du får tillgång till ett familjeabonnemang med upp till 4 profiler.',
        p4: 'Om du väljer att nedgradera från Guld till Silver kommer du att gå miste om de senaste böckerna.',
        p5: 'Klicka på Ändra till Familj för att uppgradera från Silver till Familj. Detta innebär att du får tillgång till ett familjeabonnemang med upp till 4 profiler.',
        p6: 'Klicka på Ändra till Guld för att uppgradera från Silver till Guld. Detta innebär att du får tillgång till de senaste böckerna.Vi kommer att  debitera 169 kr/mån vid nästa betalningsdatum. Du får tillgång till ditt  Guld-konto direkt.',
      },
    },
    close_account: {
      buttons: {
        change_subscription: 'Byt abonnemang',
        cancel: 'Avbryt',
        end: 'Avsluta',
      },
      step1: {
        freemember: {
          heading: 'Just nu provar du Nextory gratis!',
          subheading:
            'OBS: Om du avslutar kommer du inte längre kunna använda Nextory och du går miste om dina kvarvarande gratisdagar.',
          p1:
            ' Vet du om att du kan uppgradera och nedgradera närsomhelst? T.ex. kostar vårt Silver abonnemang endast 139 kr/mån och innehåller tusentals titlar.',
        },
        member: {
          heading: 'Tips!',
          subheading:
            'På Nextory har vi olika abonnemang där du kan uppgradera och nedgradera när du vill.',
          p1_p1:
            ' Vi har t.ex. abonnemanget Silver för endast 139 kr/mån med tusentals titlar. Vill du byta till abonnemanget Silver och',
          p1_p2: 'betala bara 139 kr/mån?',
        },
      },
      step2: {
        heading: 'Hjälp oss att bli ännu bättre!',
        subheading:
          'Vi vill alltid ge våra kunder den bästa läsupplevelsen. Dina synpunkter är viktiga för oss.',
        email_label: 'Varför vill du avsluta ditt abonnemang?',
        list: {
          option1: 'Vänligen välj ett alternativ',
          option2: 'Jag har inte tid att utnyttja tjänsten',
          option3: 'Tjänsten är inte tillräckligt prisvärd',
          option4: 'Jag hittar inte de böcker jag vill läsa',
          option5: 'Appen har inte fungerat som den ska',
          option6: 'Jag har valt en annan tjänst',
          option7: 'Annat',
        },
        labels: {
          label1: 'Vänligen ange en eller flera böcker du inte hittade',
          label2: 'Annan anledning',
        },
        errors: {
          error1: 'Vänligen välj ett alternativ ovan',
          error2:
            'Hoppsan, något gick fel. Vänligen testa igen, om felet kvarstår, vänligen kontakta kundtjänst på 08-411 17 15',
        },
        buttonred_value: 'Avsluta konto',
        afterclose: {
          heading1: 'Ditt abonnemang är nu avslutat',
          heading2: 'Ditt abonnemang avslutas:',
          p1:
            'Stort tack för din tid på Nextory. Vi hoppas att du kommer tillbaka till oss igen. Du kan alltid återuppta ditt abonnemang genom att använda samma användarnamn och lösenord på nextory.se',
          p2: 'Du har fortfarande tillgång till dina böcker fram till ',
          p2_b: '',
          button1: 'Återuppta abonnemang',
          button2: 'Till mitt konto',
        },
        uncancel_thankyou: {
          heading1: 'Dina avbokningar återställs framgångsrikt.',
          p1: 'Vi är glada att se att du vill stanna',
          button1: 'OK',
        },
      },
    },
    forgotpassword: {
      send: 'Skicka',
      Invlidlink: 'Ogiltig länk!',
      InvlidlinkdESC: 'Den här länken används redan för att ändra lösenordet.Vänligen använd den nya länken för att ändra lösenordet.som är Nextory skickar dig din registrerade e-postadress.',
      Resend: 'Skicka igen',
      heading: 'Glömt lösenord?',
      subheading: 'Fyll i den e-post du registrerade dig med så skickar vi ett nytt!',
      mailsent: {
        heading: 'Lösenordsåterställning',
        subheading:
          'Vi har skickat ett mejl till dig där du kan byta lösenord. Vänligen kolla din inkorg.',
      },
    },
    loginpage: {
      heading: 'Logga in',
      disclaimer: 'Ny hos Nextory? ',
      register_now: 'Registrera dig nu.',
      loginform: {
        presubmit: 'Har du glömt ditt lösenord? Klicka här.',
        login: 'Logga in',
      },
    },
    my_account: {
      accountform: {
        save_data: 'Spara uppgifter',
      },
      collabs: {
        heading: 'Mina erbjudanden',
        p1: 'Här hittar du erbjudanden som är exklusiva för dig som är medlem hos Nextory.',
      },
      headings: {
        my_details: 'Mina Uppgifter',
        account_has_ended: 'kontot är avslutat',
        enable_your_account_again: 'Aktivera ditt konto igen',
        account_is_active_even: 'Konto är aktivt till och med',
        you_have_subscription: 'Du har abonnemanget',
        people: 'personer',
        end_subscriptions: 'Avsluta abonnemang',
      },

      content: {
        p1: 'Vid nästa debiteringsdag',
        p2: 'kommer ditt abonnemang att ändras till',
        p3: 'Konto är aktivt till och med',
        p4: 'Nästa betalning sker',
        p5_p1:
          'Vid uppsägning av abonnemanget var kontot i en betalprocess med Trustly. Det innebär att du kan se hur länge ditt konto kommer att vara aktivt först när betalprocessen har slutförts. Vanligtvis sker detta inom 24 timmar, men kan ta längre tid om uppsägningen gjordes på en helgdag.',
        p5_p2: 'Återkom nästkommande vardag för att se hur länge du kan fortsätta använda Nextory.',
        currency: 'kr',
        currency_per_month: 'kr/månad',
      },
      buttons: {
        cancel_downgrade: 'Avbryt nedgradering',
        change_payment_details: 'Ändra betalningsuppgifter',
        enable_your_account_again: 'Aktivera kontot igen',
        change_subscription: 'Ändra abonnemang',
        end_subscriptions: 'Avsluta abonnemang',
        logout: 'Logga ut',
      },
      orders: {
        order: 'Order',
        currency: 'kr',
        order_history: 'Orderhistorik',
      },
    },
    reactivate_payment: {
      notifytrustlyfail: {
        success:
          'Oj, din registrering med Mobilt BankID avbröts! Vänligen prova igen. Funkar det inte, säkerställ att du har inte täckning i ditt bankkonto. Annars kontakta din bank.',
      },
      heading: 'Ange din betalningsinformation för att aktivera ditt abonnemang',
      listitem: 'Inga förpliktelser, avsluta online när du vill.',
      paymenttitle: 'Välj betalsätt',
      secure_server: 'Säker server (SSL-krypterad)',
      credit_or_debit_card: 'Kredit- eller bankkort',
      mobile_bankid: 'Mobilt BankID',
    },
    reactivate_subscription: {
      heading: 'Välkommen tillbaka! Välj det abonnemang som passar dig bäst',
      subheading: 'Du kan uppgradera och nedgradera när du vill',
      buttons: {
        continue: 'Fortsätt',
        go_back: 'Gå tillbaka'
      },
    },
    receipt: {
      heading: 'Kvitto',
      subheading: 'Information angående order:',
      date: 'Datum',
      currency: 'kr',
      subsctiption: 'Abonnemang',
      payment_method: 'Betalningsmetod',
      receiver: 'Mottagare',
      vat_number: 'VAT-nummer',
      email: 'E-post',
      vat: 'Moms',
      total: 'Totalt',
      payment_methods: {
        credit_card: 'Kontokort',
        trustly_direct: 'Trustly direktbetalning',
        campaign_code: 'Kampanjkod',
        gift_cards: 'Presentkort',
      },
    },
  },
  /**************** END ACCOUNT PAGE ******************/

  /**************** FILTER DROPDOWN PAGE ******************/
  filter: {
    category: 'Kategori',
    all: 'Alla',
    sort_by: 'Sortera',
    sort: {
      hotness: 'Populärast',
      published: 'Utgiven',
      title: 'Titel',
    },
    type: {
      type: 'Typ',
      ebook: 'E-bok',
      audiobook: 'Ljudbok',
    },
  },
  /**************** END FILTER DROPDOWN PAGE ******************/

  /**************** GIFTCARD PAGE ******************/
  giftcard: {
    heading: 'Presentkort - Ljudböcker & E-böcker',
    subheading:
      'Presentkortet skickas direkt till dig via mejl. Du kan välja att skriva ut presentkortet och ge bort det själv eller skicka det via mejl till mottagaren.',
    redeem_wrapper: {
      heading: 'Lös in ditt presentkort',
      button: {
        not_logged_in: 'Ny kund',
        logged_in: 'Redan kund',
      },
      active_redeem_form: {
        heading1: 'Jag är ny hos Nextory',
        heading2: 'Jag har ett konto hos Nextory',
      },
    },
    redeem_giftcard_success: {
      heading: 'Välkommen till Nextory',
      subheading: 'Ditt presentkort är nu inlöst och du har Nextory till och med',
      content:
        'Ladda ned Nextory appen i din telefon eller surfplatta och få tillgång till dina böcker var du än är',
    },
    gift_reguserform: {
      submit: 'Lös in presentkort',
    },
    gift_card_success: {
      heading: 'Tack för ditt köp',
      subheading:
        'Ditt presentkort är skickat till din e-post och går bra att skriva ut eller ge bort digitalt. Du har också fått en bekräftelse på ditt köp via e-post.',
      p1: 'Ditt ordernummer är:',
      p2:
        'Ladda ned Nextory appen i din telefon eller surfplatta och få tillgång till dina böcker var du än är',
    },
    gift_buyform: {
      one_month: '1 månad',
      three_month: '3 månader',
      six_month: '6 månader',
      price1: 'Pris: 199 kr',
      price2: 'Pris: 597 kr',
      price3: 'Pris: 1 194 kr',
      buttons: {
        continue: 'Fortsätt',
      },
    },
    subscriptionpage: {
      heading: 'Välj det abonnemang du vill prova gratis',
      subheading: 'Du kan uppgradera och nedgradera när du vill',
      buttons: {
        continue: 'Fortsätt',
      },
    },
    registercard: {
      notifytrustfail:
        'Oj, din registrering med Mobilt BankID avbröts! Vänligen prova igen. Funkar det inte, säkerställ att du har inte täckning i ditt bankkonto. Annars kontakta din bank.',
      heading: 'Ge bort ett presentkort',
      on: 'på',
      list: {
        item1: 'Lyssna och läs var som helst, när som helst',
        item2: '1 000-tals böcker direkt i din mobil eller surfplatta',
        item3: 'Få presentkortet direkt i din mail',
      },
      secure_server: 'Säker server (SSL-krypterad)',
      credit_or_debit_card: 'Kredit- eller bankkort',
      mobile_bankid: 'Mobilt BankID',
    },
  },
  /**************** END GIFTCARD PAGE ******************/

  /**************** DIRECTACTIVATION PAGE ******************/
  direct_activation: {
    links: {
      terms: 'villkor',
      latest_titles: 'våra senaste titlar',
    },
    new_member: {
      heading_p1: 'Ditt erbjudande: Prova Nextory för',
      heading_p2: ' kr i en månad',
      subheading_p1: 'Klicka på',
      subheading_p2: 'Starta kampanjperioden',
      subheading_p3: 'för att börja läsa och lyssna',

      disclaimer: {
        p1: 'När du klickar på knappen',
        p2: 'Starta kampanjperioden',
        p3: 'så godkänner du våra',
        p4: 'och påbörjar abonnemanget.',
      },
    },
    existing_member: {
      heading: 'Det verkar som om du redan är medlem hos Nextory',
      subheading: 'Detta erbjudande gäller tyvärr endast icke-medlemmar.',
      p1: 'Varför inte kika på',
      p2: 'istället?',
    },
    invalid_code: {
      heading: 'Det verkar som om den här aktiveringslänken antingen är utgått eller ogiltig',
      subheading: 'Den här kampanjkoden är inte längre aktiv.',
    },
    error: 'Ett fel har uppstått, vänligen kontakta kundtjänst',
  },
  /**************** END DIRECTACTIVATION PAGE ******************/

  /**************** CAMPAIGN PAGE ******************/
  campaign: {//Ref:Translation.campaign
    formheading_a: 'Registrera dig för att starta din kampanjperiod',
    formheading_b: 'Tidsbegränsat erbjudande – börja läs redan idag!',
    heading1: 'Ange din kampanjkod för att ta del av ett erbjudande.',
    heading2: 'Vill du prova Nextory genom en kampanjkod?',
    disclaimer: 'Genom att klicka på Fortsätt godkänner du våra',
    link: 'villkor',
    list: {
      item1: 'Läs och lyssna hur mycket du vill',
      item2: 'Välj bland tusentals titlar i mobilen och surfplattan',
      item3: 'Ingen bindningstid, avsluta när du vill',
    },
    subscriptionpage: {
      registerwrapper: {
        campaignmessage: {
          month: 'månad',
          months: 'månader',
          message_a: 'Du betalar ingenting förrän din gratisperiod har löpt ut.',
          message_b: 'Du betalar endast ',
          message_c: 'Du får ',
          in: 'i',
          discount_in: 'rabatt i',
          currency: 'kr',
        },
        stage: 'Steg',
        of: 'av',
        pre_stage_heading: 'Välj ett av våra tre abonnemang',
        pre_stage_button: 'Visa Abonnemang',
        pre_stage_body_a: 'Du betalar ingenting förrän din gratisperiod har löpt ut.',
        pre_stage_body_b: 'Välj ett av våra tre abonnemang - du betalar endast ',
        in: 'i',
        // heading1: 'Välj det abonnemang du vill prova gratis',
        // heading2: 'Välj det abonnemang du vill prova med din rabattperiod',
        heading1: 'Välj abonnemang',
        heading2: 'Välj abonnemang',
        subheading: 'Du kan uppgradera och nedgradera när du vill',
        continue: 'Fortsätt',
        choose: 'Välj',
        for: 'för',
        off: '',
        when: '',
        free_for: 'gratis i',
        days: 'dagar',
        at_the_cost_of: '',
        for_free_days: '',
        currency_per_month: 'kr/mån',
        family: {
          heading: 'Hur många är det som ska läsa & lyssna?',
          subheading: 'Välj antal personer som ska kunna läsa & lyssna från samma abonnemang.',
          users: 'användare',
          days_free_then: 'dagar gratis sedan',
          currency_per_month: 'kr/mån',
          choose: 'Välj',
          in: 'i',
          after: 'sedan',
          month: 'månad',
          months: 'månader',
          discount: 'rabatt',
        },
        campaign_message: {
          free_days: 'Kostnad efter din gratisperiod som slutar',
          fixedprice: 'Kostnad efter din rabattperiod som slutar',
          discount: 'Kostnad efter din rabattperiod som slutar',
        },
        button_go_back: 'Gå tillbaka',
      },
      registercard: {//Ref:Translation.campaign.subscriptionpage.registercard
        notifytrustlyfail: 'Oj, din registrering med Mobilt BankID avbröts! Vänligen prova igen. Funkar det inte, säkerställ att du har inte täckning i ditt bankkonto. Annars kontakta din bank.',
        discount: 'rabatt',
        currency_per_month: 'kr/månad',
        month: 'månad',
        months: 'månader',
        days_free_then: 'dagar gratis, därefter',
        in: 'i',
        free: 'gratis',
        heading: 'Ange din betalningsinformation',
        subheading_a_part1: 'Avsluta innan',
        subheading_a_part2: 'så betalar du ingenting. Inga förpliktelser, avsluta online när du vill.',
        subheading_b_part1: 'Du har rabatterat pris till',
        subheading_b_part2: '. Inga förpliktelser, avsluta online när du vill.',
        paymenttitle: 'Välj betalsätt',
        your_offer: 'Ditt erbjudande:',
        currency: 'kr',
        p1: 'Pris/månad efter kampanjperioden:',
        p2: 'Inga förpliktelser, avsluta online när du vill.',
        secure_server: 'Säker server (SSL-krypterad)',
        credit_or_debit_card: 'Kredit- eller bankkort',
        mobile_bankid: 'Mobilt BankID',
      },
    },
    campaignreguserform: {//Ref:Translation.campaign.campaignreguserform
      buttons: {
        continue: 'Fortsätt',
      },
    },
    customcampaign: {
      seodescription:
        'Har du en Nextory kampanjkod? Ange den här för att ta del av hela Nextorys fantastiska värld av böcker och litteratur!',
    },
  },

  /**************** END CAMPAIGN PAGE ******************/

  /**************** REGISTRATION PAGES ******************/
  // #Ref:Translation.registration
  registration: {
    stage: 'Steg',
    of: 'av',
    h1: 'Skapa ditt konto',
    subinfo: 'Registrera med din e-postadres och ett lösenord så kan du läsa & lyssna med Nextory var du vill, när du vill.',
    buttons: {
      continue: 'Fortsätt',
      go_back: 'Gå tillbaka',
      viewsub: 'Visa abonnemang',
      choose: 'Välj',
      for_free_14_days: 'gratis i 14 dagar',
      create_account: 'Skapa konto',
    },
    stage1: {
      heading_a: 'Välj ett av våra tre abonnemang',
      subheading_a: 'Du betalar ingenting förrän din gratisperiod har löpt ut.',
      // heading: 'Välj det abonnemang du vill prova gratis',
      heading: 'Välj abonnemang',
      subheading: 'Du kan uppgradera och nedgradera när du vill.',
      tabletitle: 'Välj ditt abonnemang',
      heading_family: 'Hur många är det som ska läsa & lyssna?',
      subheading_family: 'Välj antal personer som ska kunna läsa & lyssna från samma abonnemang.',
    },
    stage2: {
      heading: 'Välj det abonnemang du vill prova gratis',
      subheading: 'Du kan uppgradera och nedgradera när du vill',
      registered_user: {
        heading: 'Konto skapat',
        subheading: 'Använd den här e-postadressen för att komma åt ditt konto:',
      },
      new_user: {
        heading: 'Registrera dig för att starta din gratisperiod',
        subheading: 'Vänligen fyll i dina uppgifter.',
        disclaimer: 'Genom att klicka på Fortsätt godkänner du våra',
        links: {
          terms: 'villkor',
          forgot_pass: 'Har du glömt ditt lösenord? Klicka här.',
        },
      },
    },
    stage3: {
      messages: {
        trustyfail_success:
          'Oj, din registrering med Mobilt BankID avbröts! Vänligen prova igen. Funkar det inte, säkerställ att du har inte täckning i ditt bankkonto. Annars kontakta din bank.',
      },
      heading: 'Ange din betalningsinformation',
      bullets: {
        point1_p1: 'Avsluta innan',
        point1_p2: 'så betalar du ingenting. Inga förpliktelser, avsluta online när du vill.',
        point2: 'Inga förpliktelser, avsluta online när du vill.',
      },
      heading2: 'Ange din betalningsinformation',
      bullets2: {
        point1: 'Som återkommande kund har du ingen provperiod och kan direkt komma igång med dina tidigare läslistor.',
        point2: 'Första debiteringen sker i samband med registrering av betaluppgifter.',
        point3: 'Inga förpliktelser, avsluta online när du vill!',
      },
      secure_server: 'Säker server (SSL-krypterad)',
      credit_or_debit_card: 'Kredit- eller bankkort',
      mobile_bankid: 'Mobilt BankID',
      regcardform: {
        paymenttitle: 'Välj betalsätt',
        formlabel: 'Utgångsdatum',
        buttons: {
          text1: 'Uppdatera kortuppgifter',
          text2: 'Starta din gratisperiod',
          text3: 'Aktivera ditt abonnemang',
          text4: 'Starta din gratisperiod',
          text5: 'Köp presentkort',
          text6: 'Starta din gratisperiod',
          text7: 'Starta ditt abonnemang',
        },
        credit_debit_option: {
          months: {
            month: 'Månad',
            january: 'Januari',
            february: 'Februari',
            march: 'Mars',
            april: 'April',
            may: 'Maj',
            june: 'Juni',
            july: 'Juli',
            august: 'Augusti',
            september: 'September',
            october: 'Oktober',
            november: 'November',
            december: 'December',
          },
          year: 'År',
          error: {
            presubmit: 'Oj, din kortregistrering avbröts. Detta skedde troligtvis pga. att:',
            bullets: {
              point1: 'Du inte har aktiverat ditt kort för internetbetalningar.',
              point2: 'Du skrev fel kortuppgifter.',
              point3: 'Ditt kort har gått ut.',
              point4: 'Du nekades av din bank.',
            },
            please_try_again: 'Vänligen försök igen.',
          },
        },
      },
      regtrustly: {
        mobile_bankid_option: {
          initial_text:
            'Starta din gratisperiod genom att signera digitalt autogiro med Mobilt BankID. Klicka på "Använd Mobilt BankID" för att slutföra registreringen.',
          second_to_payment:
            'Vill du hellre använda Mobilt BankID? Klicka på "Använd Mobilt BankID" för att ändra betalmedel.',
          enable_payment:
            'Starta ditt abonnemang genom att signera digitalt autogiro med Mobilt BankID. Klicka på "Använd Mobilt BankID" för att slutföra aktiveringen.',
          register_giftcard:
            'Köp presentkort genom att signera digitalt autogiro med Mobilt BankID. Klicka på "Använd Mobilt BankID" för att slutföra köpet.',
          register_campaign:
            'Starta din kampanjperiod genom att signera digitalt autogiro med Mobilt BankID. Klicka på "Använd Mobilt BankID" för att slutföra registreringen',
        },
        buttons: {
          use_mobile_bankid: 'Använd Mobilt BankID',
        },
      },
    },
    register_success: {
      reactivated: {
        heading: 'Välkommen tillbaka till Nextory!',
        subheading:
          'Ditt konto är återaktiverat och ditt konto har debiterats enligt ditt valda abonnemang. Vänligen kontrollera dina uppgifter nedan för en förbättrad upplevelse.',
      },
      new: {
        heading: 'Välkommen till Nextory! Ditt abonnemang är nu startat.',
        subheading: 'Vänligen fyll i dina uppgifter nedan för en förbättrad upplevelse.',
      },
      cellphone_error: 'Telefonnummer bör vara minst 7 och max 12 siffror',
    },
  },
  /**************** END REGISTRATION PAGES ******************/

  /**************** 404 PAGES ******************/
  not_found: {
    heading: 'Hoppsan!',
    subheading: 'Vi kan tyvärr inte hitta sidan du sökte.',
    go_back: 'Gå tillbaka till vår första sida genom att ',
    content:
      'Ifall du får upprepade fel eller har frågor kontakta gärna vår kundservice via telefonnummer 08-411 17 15. Ha en skön dag med en ',
    links: {
      click_here: 'klicka här',
      nice_book: 'skön bok',
    },
  },
  /**************** END 404 PAGES ******************/

  /**************** APP PAGES ******************/
  app_page: {
    heading: 'Ladda ned vår smarta app',
    content:
      'Registrera dig för en gratisperiod via hemsidan och ladda sedan ned vår app för iOS eller Android. Du kan då börja läsa eller lyssna direkt. Böcker kommer du åt när du loggar in i appen.',
  },
  /**************** END APP PAGES ******************/

  /**************** FAQ PAGES ******************/
  faq: {
    seotitle: 'Vanliga frågor och svar',
    seodescription:
      'Prova tusentals Ljudböcker och E-böcker Gratis i 14 dagar! Ladda ner appen och streama i din iPhone, Android eller iPad var du än är med Nextory.',
    faq1: {
      heading: 'Hur hittar jag de böcker jag vill ha?',
      content: {
        p1:
          'Letar du efter en specifik bok, författare eller uppläsare kan du använda sökrutan i appen, klicka på förstoringsglaset. Vill du välja bland rekommenderade böcker hittar du nyheter, populära böcker och andra boktips på appens startsida. Vill du söka på kategorier? Klicka på menyn i vänstra hörnet och välj kategori i listan.',
        p2:
          'Många av våra böcker finns både som e-bok och ljudbok. Viket format den finns i ser du på symbolerna på omslaget. Om du hittar en bok men vill ha den i ett annat format väljer du ”visa e-bok” eller ”visa ljudbok” på informationssidan om boken. Om ingen sådan knapp finns där så har vi just nu boken i det format som den visas i.',
      },
    },
    faq2: {
      heading: 'Hur fungerar Min Lista?',
      content: {
        p1:
          'När du hittat en bok du vill läsa eller lyssna på klickar du på den. Då kommer du till en informationssida om den valda boken. Där lägger du till boken till Min lista genom att klicka på knappen + Min Lista.',
        p2:
          'Att lägga till böcker till listan är ett sätt att hålla ordning på vilka böcker du vill ha i ditt bibliotek. För att börja läsa eller lyssna på en bok behöver du starta den genom att välja ”Läs” eller ”Lyssna”',
      },
    },
    faq3: {
      heading: 'Hur startar jag en bok?',
      content: {
        p1:
          'När du klickat på en bok i Min lista, i en sökning eller från rekommendationerna på startsidan kommer du till en informationssida om den valda boken. Där kan du välja Läs eller Lyssna (beroende på om det är en e-bok eller ljudbok) för att starta direkt.',
        p2:
          'Om boken finns i ett annat format kan du välja ”Visa ljudbok” eller ”Visa e-bok” för att byta till dessa.',
      },
    },
    faq4: {
      heading: 'Läs och lyssna offline',
      content: {
        p1:
          'I Nextorys app laddas alla böcker du läser och lyssnar på ner till appen. Det är för att du ska slippa de störningar i upplevelsen som strömmad läsning kan ge. Genom att aktivera boken när du har tillgång till WIFI slipper du dessutom betala eventuella extrakostnader för datatrafik till din operatör.',
        p2:
          'Så kan du lyssna offline? Ja det går utmärkt, så länge boken först är aktiverad och fullt nedladdad på mobilen eller surfplattan.',
      },
    },
    faq5: {
      heading: 'Hur fungerar e-boksläsaren?',
      content: {
        p1:
          'När du läser en e-bok kan du själv ändra utseendet på e-boken genom att välja ändra ljusstyrkan, sidmarginaler, bakgrundfärgen samt storlek på texten. Du kan även sätta dina egna bokmärken i boken samt markera text och lägga till anteckningar. Under meny-ikonen hittar du kapitel, dina bokmärken och markeringar.',
        p2:
          'När du stänger ned boken sparas ett automatiskt bokmärke och du kan fortsätta läsa på den sida du befann dig.',
      },
    },
    faq6: {
      heading: 'Hur fungerar ljudboksspelaren?',
      content: {
        p1:
          'Du kan lyssna direkt från enhetens högtalare, med hörlurar eller från en extern högtalare. När du lyssnar på en ljudbok ställer du in volymen genom reglaget i appen eller med reglaget på din enhet.',
        p2:
          'Du pausar ljudboken genom att trycka på pausknappen. Om du stänger appen pausas boken automatiskt och ett bokmärke sparas där du slutat lyssna. Nästa gång du vill lyssna fortsätter boken från där du senast pausade uppspelningen.',
        p3:
          'Du kan spola framåt och bakåt i boken och sätta egna bokmärken om du vill. För ljudböcker finns en funktion för dig som vill somna till en bok men som morgonen efter vill slippa leta reda på var i boken du somnade. När boken är igång trycker du på symbolen för sömn och väljer hur länge du vill att boken ska spela innan den automatiskt pausas.',
        p4:
          'Du kan välja om du vill lyssna på boken i normal hastighet eller öka hastigheten på uppläsningen.',
      },
    },
    faq7: {
      heading: 'Jag vill ta bort en bok, hur gör jag?',
      content: {
        p1:
          'Ta bort en bok genom att trycka på symbolen med tre punkter på den bok du inte vill läsa för tillfället. Välj ”Ta bort och flytta till min lista”. Boken finns kvar i din lista så att du kan ladda ner den igen senare ifall du skulle vilja aktivera den igen.',
        p2:
          'Om du vill ta bort boken även från din lista, klicka på boken i listan och välj ”Ta bort från min lista”',
      },
    },
    faq8: {
      heading: 'Varför kan jag inte ladda ner böcker?',
      content: {
        p1:
          'Förmodligen har du många böcker i aktivt läge samtidigt. Eftersom varje bok tar utrymme av internminnet i din enhet kan du behöva inaktivera några böcker innan du kan aktivera nya.',
        p2:
          'Ta bort en bok genom att trycka på symbolen med tre punkter på den bok du inte vill läsa för tillfället. Välj ”Ta bort och flytta till Min lista”. Boken finns kvar i din lista så att du kan ladda ner den igen senare ifall du skulle vilja aktivera den igen. Nu har du frigjort minne för att kunna aktivera nya böcker.',
      },
    },
    faq9: {
      heading: 'Kan jag använda Nextory på flera enheter?',
      content: {
        p1:
          'Ja, du kan använda Nextorys app i fler enheter. Men bara på en i taget om du har abonnemang Guld eller Silver. Har du abonnemanget Familj kan upp till fyra personer läsa eller lyssna samtidigt på olika mobiler eller surfplattor. De fyra familjeprofilerna kan också användas på fler olika enheter, men bara på en i taget.',
        p2:
          'Ladda ner appen på de enheter du vill använda och logga in med dina användaruppgifter. För Familjeabonnemanget väljer du vem i familjen det är som är inloggad.',
        p3:
          'Kom ihåg att alla böcker du aktiverar sparas på den enhet de aktiveras på, så när du växlar mellan enheter behöver du aktivera boken igen.',
      },
    },
    faq10: {
      heading: 'Hur lägger jag till användare på mitt Familjeabonnemang?',
      content: {
        p1:
          'När du har valt abonnemanget Familj skapar du upp till fyra profiler. Du lägger till dina profiler direkt i appen, överst i menyn. För barn-profiler begränsas innehållet så att utbudet enbart består av barnlitteratur. Du kan när som helst redigera dina profiler.',
        p2:
          'Ladda ner appen på din familjs enheter och logga in med dina användaruppgifter. Välj sedan vem i familjen det är som använder appen. Du kan enkelt växla mellan olika användare i menyn.',
        p3: 'Varje familjemedlem har sin egen profil med listor och aktiva böcker.',
      },
    },
    faq11: {
      heading: 'Hur upp- eller nedgraderar jag mitt abonnemang?',
      content: {
        p1:
          'Du kan uppgradera eller nedgradera mellan olika abonnemang när du vill. Det gör du via Mitt Konto på Nextorys hemsida. Om du nedgraderar behåller du ditt pågående abonnemang fram tills nästa betalningstillfälle. Uppgraderar du får du direkt tillgång till det nya abonnemanget med alla böcker som ingår.',
        p2:
          'När du uppgraderat behöver du logga ut ur appen och logga in på nytt för att ditt nya abonnemang ska bli tillgängligt. Det gäller på samtliga enheter där du använder Nextory.',
      },
    },
    faq12: {
      heading: 'Vilket abonnemang ska jag välja?',
      content: {
        family: 'Familj',
        gold: 'Guld',
        silver: 'Silver',
        p1:
          'Du kan välja mellan tre olika abonnemang för att få tillgång till de möjligheter som passar dig bäst.',
        p2:
          'kostar 199 kr per månad. Här inkluderas hela sortimentet med samtliga tiotusentals titlar och nyheter och upp till fyra personer kan dela på ett konto. Med unika användarprofiler för varje familjemedlem kan ni läsa eller lyssna samtidigt på olika enheter. Passar dig som är mer än en i hushållet och vill ha full bokfrihet åt hela familjen. Antal användare: upp till 4.',
        p3:
          'kostar 169 kr per månad och innehåller hela vårt breda sortiment av e-böcker och ljudböcker. Du som väljer Guld får även tillgång till alla våra nyheter. Abonnemanget passar dig som vill ha full bokfrihet. Antal användare:1.',
        p4:
          'kostar 139 kr per månad och innehåller tiotusentals e-böcker och ljudböcker men inte de senaste nyheterna eller vissa titlar på grund av avtalsskäl. Du får tillgång till drygt 80 procent av vårt stora utbud som uppdateras kontinuerligt. Silver passar dig som vill ha ett riktigt prisvärt alternativ. Antal användare:1',
      },
    },
    faq13: {
      heading: 'Hur fungerar betalningen?',
      content: {
        p1:
          'Ditt abonnemang förnyas per automatik var 30:e dag, det vill säga vid nästa betalningsdatum. Då debiteras du enligt det abonnemang och betalsätt du valt. Om du använder Trustly som betalningssätt blir betalningen synlig på ditt bankkonto ett par dagar efter att debitering har gjorts. Vid kortbetalning är det vanligaste att det syns direkt hos banken.',
      },
    },
    faq14: {
      heading: 'Hur avslutar jag?',
      content: {
        p1:
          'Uppsägning av tjänsten görs enklast via Mitt Konto på Nextorys hemsida. Du kan också kontakta vår kundtjänst via mejl eller telefon.',
        p2:
          'Om du vill avsluta innan kommande betalning ska det göras minst en dag innan betalningsdag från Mitt konto på hemsidan, eller minst två arbetsdagar innan betalningsdag vid kontakt med kundtjänst.',
      },
    },
    faq15: {
      heading: 'Problem med att läsa/lyssna offline',
      content: {
        p1:
          'Något kan ha gått fel när du laddade ner boken till enheten. Prova först att sätta enheten i online-läge och ladda ner boken igen. Se till att du har bra uppkoppling, gärna ett stabilt WIFI-nät. Se till att 100% laddats ner innan du stänger appen, sätter enheten i offline eller börjar lyssna.',
        p2:
          'Säkerställ också att du har tillräckligt med internminne på telefonen för att kunna ladda ner dina böcker.',
        p3:
          'Om det ändå inte fungerar kan en omstart av telefonen hjälpa. Har appen ”hakat upp sig” kan du behöva radera appen och hämta hem den på nytt. Dina böcker sparas centralt och försvinner inte från ditt konto för att appen raderas.',
      },
    },
    faq16: {
      heading: 'Appen fungerar inte, går inte att läsa eller lyssna',
      content: {
        p1: 'Se till att du har senaste versionen av både appen och enhetens operativsystem.',
        p2:
          'Har du uppgraderat eller nedgraderat behöver du logga ut ur appen och logga in på nytt för att det nya abonnemanget ska bli tillgängligt. Det gäller på samtliga enheter där du använder Nextory.',
        p3:
          'Säkerställ också att du har tillräckligt med internminne på telefonen eller surfplattan för att kunna aktivera dina böcker.',
        p4:
          'Har du abonnemanget Silver så kan du läsa eller lyssna på ungefär 80 procent av vårt utbud. Vill du ha tillgång även till vårt premiumsortiment och nyheter behöver du uppgradera till Guld eller Familj.',
        p5:
          'Om appen inte fungerar som den ska av andra anledningar, kan du först testa att stänga ner appen och starta den igen eller att starta om telefonen.',
        p6_p1: 'Om problemet kvarstår – kontakta gärna vår kundtjänst genom att maila',
        p6_p2: 'eller ringa oss på 08-411 17 15.',
        p7:
          'Du kan även testa att radera appen helt från din telefon. Installera sedan appen på nytt och starta om telefonen. Dina böcker sparas centralt och försvinner inte från ditt konto för att appen raderas.',
      },
    },
    faq17: {
      heading: 'Hur kontaktar jag Nextory?',
      content: {
        p1:
          'Vi vill vara tillgängliga för dig. Därför kan komma i kontakt med oss på fler olika sätt.',
        p2:
          'Vår hemsida. Du når ditt konto genom att logga in från vår hemsida. Där kan du enkelt göra ändringar i ditt abonnemang.',
        p3:
          'Behöver du personlig guidning går det utmärkt att ringa oss på 08-411 17 15. Vi har öppet måndag-fredag 09.00-17.00.',
        p4_p1: 'E-post. Du kan mejla oss när som helst på',
        p4_p2: 'Vi återkommer oftast samma dag eller nästkommande vardag.',
        p5: 'Tveka inte att höra av dig till oss om du vill ha hjälp med något.',
      },
    },
  },
  /**************** END FAQ PAGES ******************/

  /**************** GET STARTED PAGES ******************/
  getstarted: {
    reactivated: {
      heading: 'Så här kommer du igång igen',
      content: {
        p1: 'Logga in i Nextorys app på din mobil eller surfplatta',
        p2:
          'Du kan nu fortsätta att läsa och lyssna på böckerna i din lista eller välja bland tiotusentals andra titlar.',
      },
      tips:
        'Tips: Utforska dina nya funktioner. Vi gör ständiga uppdateringar och förbättringar av appen. Ta dig någon minut för att utforska vilka nya möjligheter du har i ditt Nextory.',
      app_removed:
        'Har du tagit bort appen? Ladda ner den på nytt från Apple Store eller Google Play.',
    },
    new: {
      heading: 'Så här kommer du igång med Nextory',
      content: {
        p1:
          'Ladda ner Nextory appen från Apple Store eller Google Play till din mobil och/eller surfplatta.',
        p2: 'Logga in i appen med din e-postadress och lösenord',
        p3: 'Klart, börja läs och lyssna!',
      },
      tips: '',
    },
  },
  /**************** END GET STARTED PAGES ******************/

  /**************** SALJA PAGES ******************/
  saljapage: {
    seotitle: 'Vill du sälja dina digitala böcker via Nextory',
    seodescription:
      'Sälj dina böcker hos Nextory. Vi erbjuder en abonnemangstjänst med både e-böcker och ljudböcker.',
    heading: 'Vill du sälja dina digitala böcker via Nextory?',
    content: {
      p1:
        'Då behöver du ha ett företag med organisationsnummer och ge ut e-böcker och/eller ljudböcker som har unika isbn (artikelnummer).',
      p2_p1:
        'Vi vill också att du distribuerar dina böcker via en digital distributör. Hör gärna av dig till oss på',
      p2_p2: 'så berättar vi mer!',
    },
  },

  /**************** END SALJA PAGES ******************/

  /**************** SILVERFAQ PAGES ******************/
  silverfaq: {
    seotitle: 'Vanliga frågor och svar',
    seodescription:
      'Prova tusentals Ljudböcker och E-böcker Gratis i 14 dagar! Ladda ner appen och streama i din iPhone, Android eller iPad var du än är med Nextory.',
    heading: 'Vanliga frågor om Silverabonnemanget',
    links: {
      h1: 'Varför kan jag inte lyssna på de senaste böckerna i Silver?',
      h2: 'Varför är vissa äldre böcker inte tillgängliga i Silver?',
      h3: 'Hur skiljer sig utbudet i Silver mot Guld och Familj?',
      h4: 'Hur ofta uppdateras utbudet i Silver?',
      h5: 'Varför saknas en av böckerna i en serie i Silver?',
      h6: 'Kan jag uppgradera abonnemanget tillfälligt?',
      h7: 'Våra olika abonnemang',
    },
    contents: {
      page1: {
        heading: 'Varför kan jag inte lyssna på de senaste böckerna i Silver? ',
        content: {
          p1:
            ' Det lägre priset på Silver är möjligt tack vare ett avgränsat utbud. De senaste böckerna, oftast 0-6 månader, kostar mer per läsning och kräver ett dyrare abonnemang som Guld eller Familj.',
          p2: 'Silverabonnemanget ger dig tillgång till ca 85% av Nextorys totala utbud.',
        },
      },
      page2: {
        heading: 'Varför är vissa äldre böcker inte tillgängliga i Silver? ',
        content: {
          p1:
            'Vilka böcker som finns tillgängliga i Silver beror på våra avtal med förlagen och deras villkor med sina författare.',
          p2: 'Vi arbetar hela tiden för att öka utbudet i Silver.',
        },
      },
      page3: {
        heading: 'Hur skiljer sig utbudet i Silver mot Guld och Familj? ',
        content: {
          p1:
            'Med Silverabonnemanget har du tillgång till omkring 85% av utbudet i Guld och Familj. Det är nästan lika många böcker som i ett genomsnittligt svenskt bibliotek.',
        },
      },
      page4: {
        heading: 'Hur ofta uppdateras utbudet i Silver?',
        content: {
          p1:
            'Det kommer nya titlar varje dag. Du hittar listor med nyheter på startsidan och på genresidorna.',
        },
      },
      page5: {
        heading: 'Varför saknas en av böckerna i en serie i Silver? ',
        content: {
          p1:
            'I en bokserie kan den senaste boken saknas i Silver eftersom nya böcker, oftast 0-6 månader, kostar mer per läsning och kräver ett dyrare abonnemang som Guld eller Familj.',
        },
      },
      page6: {
        heading: 'Kan jag uppgradera abonnemanget tillfälligt?',
        content: {
          p1: 'Ja, på webbplatsen kan du när som helst upp- eller nedgradera ditt abonnemang.',
        },
      },
      page7: {
        heading: 'Om våra olika abonnemang',
        content: {
          p1:
            'Med Nextory väljer du mellan tre olika abonnemang. Guld och Familj ger dig tillgång till hela utbudet, Familj tillåter också upp till 4 användare. Med Silver får du ett lägre pris men också ett något begränsat utbud.',
          p2: 'Du kan uppdatera och nedgradera mellan de olika abonnemangen när du vill.',
        },
      },
    },
  },
  /**************** END SILVERFAQ PAGES ******************/

  /**************** INTEGRITY PAGES ******************/
  integrity_pages: {
    content: `<h1>Integritetspolicy</h1>
  <P>
    Nextory är en digital abonnemangstjänst som ger dig obegränsad läsning av e-böcker och
    ljudböcker. Tjänsten tillhandahålls av Nextory AB, organisationsnummer 556708-4149.
  </P>
  <P>
    Denna integritetspolicy beskriver vilka personuppgifter Nextory behandlar i samband med
    att du registrerar ett konto hos oss och använder Nextorys tjänst. Vi kan även behandla
    uppgifter när du, utan att ha registrerat ett konto, använder appen eller köper
    presentkort av oss.
  </P>
  <P>
    Din integritet är viktig för oss och du ska alltid känna dig trygg när du lämnar
    personuppgifter till Nextory. Vår behandling av dina personuppgifter sker i enlighet med
    gällande lagar och regler, inklusive dataskyddsförordningen (“GDPR”).
  </P>
  <P>
    Nextory har vidtagit tekniska och organisatoriska åtgärder för att säkerställa att dina
    personuppgifter är skyddade från obehörig åtkomst och användning.
  </P>
  <h3>Personuppgiftsansvarig</h3>
  <P>
    Nextory är personuppgiftsansvarig för behandling av dina personuppgifter. Har du några
    frågor gällande detta är du välkommen att kontakta vår personliga kundservice via e-post
    kundservice@nextory.se eller telefon 08-411 17 15.
  </P>
  <h3>Tidpunkter för insamling av personuppgifter</h3>
  <P>Nextory samlar in personuppgifter om dig:</P>
  <UL>
    <li>
      Vid registrering av konto hos Nextory och användande av vår tjänst. Det gäller även
      när du skapar ett konto men inte fyller i betaluppgifter.
    </li>
    <li>
      I samband med att du kontaktar Nextorys kundtjänst eller ber om information från oss.
    </li>
    <li>
      I samband med att du anmäler dig som prenumerant av e-postutskick eller annan
      kommunikation, som t.ex. push-notiser.
    </li>
    <li>
      I samband med att du delger information till Nextory via kundundersökningar eller
      andra formulär.
    </li>
    <li>
      I samband med att du använder en kampanjkod eller tar del av ett kampanjerbjudande
      utfärdat av Nextory.
    </li>
  </UL>
  <P>
    Nextory samlar även information genom egna eller tredjepartscookies och liknande
    uppföljningstekniker som kan komma att logga dina aktiviteter och val när du använder
    vår tjänst eller besöker Nextorys hemsida.
  </P>
  <h3>Personuppgifter</h3>
  <P>
    Nextory behandlar främst de uppgifter du själv lämnat till oss i samband med
    registreringen. Dessa uppgifter kan vara namn, e-postadress, lösenord och telefonnummer.
    Vid registreringen anger du även uppgifter om val av betalningsmetod. Våra
    underleverantörer och din bank erbjuder säkra betallösningar där dina betaluppgifter är
    skyddade. Nextory tar del av vissa uppgifter om betalmedlet från våra
    tjänsteleverantörer. Exempelvis får vi information om betalkorts giltighetstid för att
    kunna påminna dig om när giltighetstiden löper ut så att vi kan erbjuda vår tjänst utan
    störningar.
  </P>
  <P>Följande uppgifter om dig kan komma att behandlas av Nextory.</P>
  <UL>
    <li>
      Uppgifter för att erbjuda en mer personlig kontakt mellan Nextory och användaren:
      förnamn, efternamn samt användar-ID.
    </li>
    <li>
      Uppgifter för att hålla kontakt med användare och kunna erbjuda en god service:
      Kontaktuppgifter som e-postadress, boendeort och telefonnummer.
    </li>
    <li>
      Uppgifter för att kunna genomföra transaktioner: uppgifter om ditt betalmedel (t.ex.
      ditt betalkorts 4 sista siffror och giltighetstid) samt information om
      betalningshistorik.
    </li>
    <li>
      Uppgifter för att kunna säkerställa en god kvalitet av vår tjänst samt erbjuda
      säkerhet för användaren vid användande av tjänsten: lösenord, typ av enhet,
      app-version, inställningar i appen, titel och ISBN på bok.
    </li>
    <li>
      Uppgifter för att kunna erbjuda en personlig anpassning av vår tjänst samt erbjuda
      våra kunder de bästa boktipsen och rekommendationerna: läs- och lyssningshistorik,
      sökhistorik, sparade böcker, betygsättning av böcker, antal besök i appen, tid
      spenderat i appen, knapptryckningar och antal tillagda profiler på
      familjeabonnemanget.
    </li>
    <li>
      Uppgifter för att kunna förbättra vår tjänst och möta våra kunders önskemål och
      förväntningar: enkätsvar och svar via undersökningar (t.ex.kundundersökningar och
      orsaker till avslut av abonnemang).
    </li>
    <li>
      Uppgifter för att förbättra vår kommunikation med våra kunder: Interaktion med
      e-postmeddelanden, meddelanden i appen och push-notiser alt. telefonsamtal.
    </li>
    <li>
      Uppgifter för att förbättra vår service och tillmötesgå kundens önskemål och
      feed-back: Information som kund har förmedlat till vår kundservice i samband med
      kundärenden t.ex. rörande felsökning och support.
    </li>
  </UL>
  <br />
  <h3>Tidsbegränsning i hantering av personuppgifter</h3>
  <P>
    Vid avslut av tjänsten är kundens konto pausat under en period på två år. Detta innebär
    att kunden har samma personuppgifter och användarhistorik kopplade till sitt konto under
    denna period. Nextory kan därmed erbjuda kunden servicen att återaktivera sitt konto och
    under denna period bibehålla läshistorik och kundspecifika läslistor.
  </P>
  <P>
    Nextory sparar även uppgifter under denna tidsperiod för att förhindra missbruk av
    gratisperioder och andra kampanjerbjudanden som endast gäller en gång per kund.
  </P>
  <P>
    Vissa personuppgifter kan komma att sparas under en längre period än två år för att
    Nextory AB ska kunna uppfylla sina juridiska skyldigheter i enlighet med andra lagar som
    t.ex. bokföringslagen.
  </P>
  <h3>Vad gör vi med personuppgifterna?</h3>
  <P>
    Det finns olika syften till varför Nextory behandlar dina personuppgifter, men främst
    gör vi det för att kunna leverera tjänsten till dig. Se exempel nedan.
  </P>
  <P>
    <strong>Administrering.</strong> Vi behandlar uppgifter som e-postadress, lösenord,
    kund-ID och betaluppgifter för att du ska kunna nyttja tjänsten.
  </P>
  <P>
    <strong>Utveckling.</strong> Uppgifter om ditt beteende i appen (t.ex. sök på böcker som
    saknas och om nya funktioner används) använder vi för att utveckla och förbättra
    tjänsten. Vi använder även insikter från kundundersökningar för att utveckla Nextory.
    Personuppgifterna kan ligga som grund till analyser över hur vår kundbas beter sig för
    att på bästa sätt kunna skapa en bättre tjänst för våra kunder.
  </P>
  <P>
    <strong>Personlig upplevelse.</strong> Uppgifter om hur du använder tjänsten (t.ex.
    vilka böcker du läser eller lyssnar på, vilka genrer du föredrar och hur ofta du
    använder Nextory) kan användas för att ge dig personliga rekommendationer eller riktade
    erbjudanden. Vi kan även använda personuppgifterna på plattformar utanför Nextory, som
    exempelvis Facebook.
  </P>
  <P>
    <strong>Förhindra bedrägerier.</strong> Dina personuppgifter kan även användas för att
    upprätthålla våra medlemsvillkor och förhindra bedrägerier (t.ex. missbruk av
    provperioder). Vi delar även personuppgifter med företag utanför Nextory, vilka fungerar
    som personuppgiftsbiträden. De får inte använda personuppgifterna för egna ändamål och
    deras rättigheter regleras genom avtal med oss. Företag som har tillgång till
    personuppgifter använder dessa för att bistå oss med kundservice, leverera kommunikation
    till kunder och hantera kundundersökningar.
  </P>
  <h3>Rättslig grund för behandling</h3>
  <P>
    Nextory behandlar personuppgifter för att tillhandahålla och administrera tjänsten.
    Uppgifter som krävs för detta är bland annat e-postadress, lösenord och betaluppgifter.
  </P>
  <P>
    Vi behandlar även uppgifter som din bokhistorik, där Nextorys intresse av att behandla
    dessa användaravgifter väger tyngre är risken för din integritet som det skulle kunna
    innebära.
  </P>
  <P>
    Vissa personuppgifter behandlas med stöd av ditt samtycke. Exempelvis kan du välja att
    frånsäga dig marknadsföring, men ändå använda vår tjänst.
  </P>
  <h3>Datasäkerhet</h3>
  <P>
    Nextory har vidtagit tekniska och administrativa åtgärder för att skydda säkerheten
    kring dina personuppgifter från obehörig användning och förändring. Det åligger
    användaren att skydda sina inloggningsuppgifter så att obehöriga inte kan logga in på
    kontot. Skulle misstanke om detta ske bör användaren genast ta kontakt med Nextorys
    kundtjänst.
  </P>
  <h3>Uppdateringar av integritetspolicyn</h3>
  <P>
    Nextory har rätten att justera integritetspolicyn. Skulle vi genomföra väsentliga
    förändringar kommer du att meddelas via mejl eller i tjänsten.
  </P>
  <h3>Dina rättigheter</h3>
  <P>
    Om du har frågor avseende vår behandling av Personuppgifterna, kan du vända dig till
    kundservice@nextory.se. Du kan även vända dig dit om du önskar utöva någon av dina
    rättigheter enligt Dataskyddsförordningen. Dina rättigheter i Dataskyddsförordningen
    omfattar:
  </P>
  <UL>
    <li>
      Rätt till tillgång – Enligt artikel 15 i Dataskyddsförordningen har en registrerad
      rätt att få tillgång till sina personuppgifter samt viss information rörande
      behandlingen av dem. Den informationen framgår av detta dokument.
    </li>
    <li>
      Rätt till rättelse – Enligt artikel 16 i Dataskyddsförordningen har en registrerad
      rätt att få felaktiga personuppgifter rättade samt att komplettera ofullständiga
      personuppgifter.
    </li>
    <li>
      Rätt till radering – Under vissa omständigheter har en registrerad enligt artikel 17 i
      Dataskyddsförordningen rätt att få sina personuppgifter raderade.
    </li>
    <li>
      Rätt till begränsning av behandling – Under vissa omständigheter har en registrerad
      enligt artikel 18 i Dataskyddsförordningen rätt att begränsa behandlingen som den
      ansvarige vidtar.
    </li>
    <li>
      Rätt till dataportabilitet – Enligt artikel 20 i Dataskyddsförordningen har en
      registrerad rätt att få ut sina personuppgifter (eller under vissa omständigheter få
      dem överförda till en annan personuppgiftsansvarig) i ett strukturerat, allmänt använt
      och maskinläsbart format.
    </li>
    <li>
      Rätt att göra invändningar – Enligt artikel 21 i Dataskyddsförordningen har en
      registrerad under vissa omständigheter rätt att invända mot behandling av
      personuppgifter. Detta gäller vid behandling med stöd av intresseavvägning, som t.ex.
      för Informationsändamålet.
    </li>
  </UL>
  <br />
  <P>Nextory AB, 1 juni 2018</P>`,
  },

  /**************** STIPENDIUM PAGES ******************/
  stipendum_pages: {
    heroprocess: 'Ansökningsprocessen',
    herostipendie: {
      heading: 'Stipendiet för innovativa idéer för e-böcker och ljudböcker',
      subheading: 'Få 15 000 kr till dina studier. Läsåret 2018',
    },
    application: {
      seotitle: 'Ansökningsprocessen - Nextorystipendiet',
      seodescription:
        '1. Ge ett svar på frågeställningen. Steg 2. Skicka in ditt bidrag. Steg 3. Vinnaren offentliggörs.',
      content1: {
        heading: 'Steg 1: Ge ett svar på frågeställningen',
        p1:
          ' Fundera kring ämnet och besvara följande fråga på svenska, inom ramen för 1500 – 2000 ord.',
        quote:
          'Hur kan digitaliseringen och böcker kombineras för att fler ska kunna läsa i Sverige? Vad förutspår du vara de viktigaste faktorerna som kommer att påverka våra vardagliga läsvanor i samband med digitaliseringen och hur kan denna förändring mötas för att öka läsandet? Hur påverkar e-bokens intåg läsandet? När och hur lyssnar människor på ljudböcker',
        p2:
          'Du har fritt spelrum att välja infallsvinkel och metod, så försök att vara kreativ. Dra nytta av din egen akademiska bakgrund och dina specifika kunskaper, erfarenheter, studier och intressen för att komma med nya insikter inom ämnet.',
        p3:
          'Du behöver inte studera något specifikt program för att ansöka: Nextorystipendiet är öppet för alla studenter som är registrerade vid ett svenskt universitet eller högskola, har en passion för litteratur och inspirerande idéer för digitaliseringens framtida utveckling. Oavsett vad du studerar välkomnar vi dina mest kreativa och innovativa idéer.',
      },
      content2: {
        heading: 'Steg 2: Skicka in ditt bidrag',
        p1_p1:
          'Ok, nu har du besvarat frågan och är redo att skicka in ditt bidrag. Vänligen skicka det till',
        p1_p2:
          ", var noga med att inkludera en kopia i PDF-format av din universitets- eller högskoleregistrering för vårterminen eller höstterminen 2018 när du skickar in ditt bidrag. Skicka in detta innan den 31 augusti 2018 23.59. Notera att ansökningar som inkommer efter detta datum kommer inte att accepteras. För att vara berättigad till stipendiet måste du vara inskriven vid ett universitet eller högskola i Sverige för vårterminen eller hösttermimport DataLayer from 'containers/App/datalayer'; inen 2018 och vara bosatt i Sverige.",
      },
      content3: {
        heading: 'Steg 3: Offentliggörande av vinnaren av Nextorystipendiet 2018',
        p1:
          'Alla ansökningar kommer att noggrant bedömas och utvärderas av vår jury bestående av experter inom området. Innan den sista september kommer du att få svar om du har erhållit stipendiet.',
        p2_p1: 'Genom att ansöka om Nextorystipendiet godkänner du även de',
        p2_p2: 'allmänna villkoren.',
        buttons: {
          back_to_nextory_scholarship: 'Tillbaka till Nextorystipendiet',
        },
      },
    },
    index: {
      seotitle: 'Läsning i en digitaliserad framtid – Eboken & ljudboken i fokus',
      seodescription:
        'Vi vill att alla ska få tillgång till ett digitalt bibliotek. Därför kan du som student få ett stipendium på 15 000 kr.',
      heading: 'Läsning i en digitaliserad framtid – Eboken & ljudboken i fokus',
      p1:
        'Nextorys vision är att alla ska ha tillgång till alla världens böcker i form av e-böcker och ljudböcker. Vi vill att alla ska kunna läsa genom att få tillgång till ett digitalt bibliotek av eböcker och ljudböcker. Därför kan du som student få ett stipendium på 15 000 kr. För att få stipendiet behöver du bidra med nytänkande kring digitalisering och läsande, i huvudsak runt e böcker och ljudböcker online och i appar.',
      p2:
        'För att kunna tilldelas stipendiet ska du skriva en uppsats på 1 500 – 2 000 ord i en och samma uppsats om hur digitaliseringen och böcker kan kombineras för att öka läsandet i Sverige. I uppsatsen ska deltagare svara på frågan:',
      quote:
        'Hur kan digitaliseringen och böcker kombineras för att fler ska kunna läsa i Sverige? Vad förutspår du vara de viktigaste faktorerna som kommer att påverka våra vardagliga läsvanor i samband med digitaliseringen och hur kan denna förändring mötas för att öka läsandet? Hur påverkar e-bokens intåg läsandet? När och hur lyssnar människor på ljudböcker',
      p3:
        'Stipendiet kommer att delas ut av en intern jury bestämd av Nextory. Bedömningen kommer att grundas utifrån studentens nytänkande, innovationsgrad samt hur väl studenten lyckats fånga samspelet mellan det digitala och läsandet.',
      buttons: {
        read_more: 'Läs mer och ansök',
      },
    },
    terms: {
      seotitle: 'Allmänna villkor för Nextorystipendiet höstterminen 2018',
      seodescription: 'Läs om våra villkor gällande Nextorystipendiet höstterminen 2018.',
      heading: 'Allmänna villkor för Nextorystipendiet höstterminen 2018',
      p1_p1:
        'Nextory, Dalagatan 7, 111 23 Stockholm, med registreringsnummer 525 118 anordnar ett stipendium med namnet',
      p1_p2:
        '. Deltagande är gratis och kräver inte att man har en aktiv abonnemangstjänst för att delta.',
      links: {
        nextory_scholarship: 'Nextorystipendiet',
      },
      h2: 'Vid deltagande ger deltagaren automatiskt samtycke till följande villkor:',

      list1: {
        h4: '§ 1 Stipendiets utformande',
        p1:
          'Ansökningsperioden pågår mellan 03.01.2018 – 08.31.2018 (hädanefter ”ansökningsperioden”).',
        p2:
          'Deltagande är gratis och deltagare behöver inte ha en aktiv abonnemangstjänst för att delta.',
        p3:
          'Att teckna en abonnemangstjänst eller att anmäla sig till nyhetsbrevet ökar inte chanserna att vinna.',
        p4:
          'För att delta måste deltagaren reflektera över frågan "Hur kan digitaliseringen och böcker kombineras för att öka läsandet i Sverige? Vad förutspår du vara de viktigaste faktorerna som kommer att påverka våra vardagliga läsvanor i samband med digitaliseringen och hur kan denna förändring mötas för att öka läsandet?” och redovisa idéerna genom ett skriftligt dokument innehållande 1500 – 2000 ord. För att kunna erhålla stipendiet måste deltagare vara registrerad på ett universitet eller högskola i Sverige under vårterminen eller höstterminen 2018. Vid deltagande måste deltagaren skicka in en PDF-kopia av sin universitets- eller högskoleregistrering för vårterminen eller höstterminen 2018.',
        p5:
          'Vinnaren kommer att bli vald internt på Nextory och kommer att erhålla en engångssumma på 15 000 SEK. Vinnaren kommer att bli underrättad inom fyra veckor efter avslutandet av ansökningsperioden.',
        p6:
          'Genom deltagande godkänner deltagare att Nextory får publicera deras namn på www.nextory.se samt på övriga Nextory webbsidor, sociala medier samt på andra externa webbsidor.',
      },
      list2: {
        h4: '§ 2 Deltagande och diskvalificering',
        p1:
          'Deltagare måste vara minst 18 år, bosatt i Sverige och måste kunna styrka detta. Deltagande får endast ske en gång per ansökningsperiod.',
        p2: 'Anställda på Nextory är exkluderade från att delta.',
        p3:
          'Nextory förbehåller sig rätten att diskvalificera deltagare som bryter mot de allmänna villkoren.',
        p4:
          'Deltagare som försöker manipulera systemet för att ge sig själva en fördel kommer att diskvalificeras. Om man har vunnit på detta sätt kommer priset att återkrävas.',
      },
      list3: {
        h4: '§ 3 Implementering och process',
        p1:
          'Deltagare svarar på frågan "Hur kan digitaliseringen och böcker kombineras för att fler ska kunna läsa i Sverige? Vad förutspår du vara de viktigaste faktorerna som kommer att påverka våra vardagliga läsvanor i samband med digitaliseringen och hur kan denna förändring mötas för att öka läsandet? Hur påverkar e-bokens intåg läsandet? När och hur lyssnar människor på ljudböcker” genom ett skriftligt dokument innehållande 1500 – 2000 ord.',
        p2_p1: 'Den färdigställda ansökan skickas till',
        p2_p2: 'under den specificerade ansökningsperioden.',
        p3:
          'Tävlingen arrangeras i Sverige. Enbart studenter registrerade på ett svenskt universitet eller högskola är berättigade att delta i tävlingen och ett sådant intyg skall bifogas när deltagaren skickar in sin rapport.',
        p4:
          'Vinnaren kommer att utses av en jury utsedd av Nextory, juryn består av interna personer. Juryn kommer att utse en vinnare av tävlingen baserat på bidragets innehåll sett till relevanta och innovativa tankegångar, idéer, källhantering och reflektioner.',
        p5:
          'Nextory kommer att informera vinnaren skriftligt (via e-mail) eller över telefon. Nextory reserverar sig rätten till att överlåta detaljer av vinnaren till en tredje part om det krävs för att leverera och/eller informera om vinsten. Om vinnaren inte är kontaktbar och/eller inte återkopplar inom fyra veckor från tillkännagivandet av vinsten kommer en ny vinnare att utses, alternativt kommer priset att bli donerat till ett välgörande ändamål. Deltagare är ansvarig för att uppge korrekt kontaktinformation.',
        p6:
          'Priset kan endast tas emot av vinnaren personligen och får inte överlåtas till någon annan.',
        p7: 'Frågor, förfrågningar eller klagomål skall ske genom att skriva ett e-mail till',
      },
      list4: {
        h4: '§ 4 Tidigare avslutande av tävlingen',
        p1:
          'Nextory förbehåller sig rätten att avsluta tävlingen när som helst utan förvarning. Nextory har rätt att avsluta tävlingen vid tekniska problem (t.ex. datavirus, manipulerande av hård- eller mjukvara) eller om det finns andra anledningar som gör att tävlingen inte kan genomföras ordentligt. Om en deltagare orsakar att tävlingen måste avslutas i förtid kan Nextory komma att begära ersättning från denne.',
      },
      list5: {
        h4: '§ 5 Dataskydd',
        p1:
          'Deltagare godkänner att Nextory lagrar deltagares personuppgifter och använder dem i tävlingen. Deltagare går också med på att förse Nextory med all nödvändig information för tävlingens genomförande.',
        p2:
          'Deltagares användaruppgifter kommer endast att användas för stipendiets räkning och helt i enlighet med gällande lagar. Nextory kan överlämna information till tredje part för att kunna möjliggöra leverans av priset. Information lämnas aldrig ut till tredje part om denne inte har något med stipendiets genomförande att göra. Undantaget är att uppgifter kan lämnas ut till behöriga myndigheter i enlighet med lagstadgade skyldigheter.',
        p3_p1:
          'Om så önskas kommer Nextory att informera deltagare om vilka uppgifter som lagrats om dem och kommer också på begäran att ta bort dem från systemet. Deltagare uppmanas att skriva ett brev till Nextory, Dalagatan 7, 111 23 Stockholm eller skicka ett mail till',
        p3_p2: 'om de vill ha sina uppgifter rensade.',
      },
      list6: {
        h4: '§ 6 Ansvar för innehåll',
        p1:
          'Deltagaren ansvarar helt för allt innehåll skapade av dem för stipendiets ändamål. Inget innehåll som antingen är olagligt och/eller bryter mot regler och villkor som gäller kan användas i rapporten, inte heller innehåll som har till avsikt att förolämpa, förtala, uppröra eller på andra sätt skada andra individer. Nextory ansvarar inte för några direkta eller indirekta, följdskador eller kostnader inklusive advokatkostnader som tredje part orsakar genom upphovsrättsliga skäl och/eller olagligt material tillhandahållet i deltagares rapport. Detsamma gäller för fordringar på grund av brister i konkurrensvillkoren.',
      },
      list7: {
        h4: '§ 7 Ansvar',
        p1:
          'Nextory ska göra rimliga försök att organisera och genomföra denna tävling på bästa sätt.',
        p2:
          'Nextory frånsäger sig allt ansvar, förutom i följande fall: a. Ansvar för hälsoskador orsakade av uppsåt eller vårdslöshet från Nextorys eller deras juridiska ombuds sida. b. Ansvar för andra skador som orsakats av uppsåt från Nextorys sida eller att Nextory eller dess juridiska ombud försummat sina förpliktelser. ',
      },
      list8: {
        h4: '§ 8 Övrigt',
        p1: 'Nextory agerar i enlighet med tillämpliga lagstadgade villkor för tävlingar.',
        p2_p1:
          'Deltagare som har invändningar angående tävlingen skickar ett brev till Nextory, Dalagatan 7, 111 23 Stockholm eller skriver ett mail till ',
        p2_p2: ' Nextory kommer att svara så fort som möjligt.',
        p3:
          'Nextory har rätt att förändra de allmänna villkoren när som helst under stipendieperioden.',
        p4:
          'Deltagare är medvetna om att priset som erhålls av tävlingen kan behöva rapporteras för vinstskatt i Sverige. Deltagare har i åtanke att de kan vara skyldiga att rapportera vinsten till de lokala skattemyndigheterna som den svenska lagstiftningen nämner detta.',
      },
    },
  },

  /**************** END STIPENDIUM PAGES ******************/

  /**************** SUBSCRIPTION TABLE ******************/
  subscription_table: {//Ref:Translation.subscription_table
    buttons: {
      silver: 'Silver',
      gold: 'Guld',
      family: 'Familj',
    },
    dates: {
      date: 'dagar',
      date1: 'Upp till',
    },
    prices: {
      silver: '139 kr',
      gold: '169 kr',
      family: '199 kr',
      currency: 'kr',
    },
    packages: {
      silver: 'Silver',
      silverPlus: 'Silver Plus',
      gold: 'Guld',
      family: 'Familj',
      family2: 'Familj 2',
      family3: 'Familj 3',
      family4: 'Familj 4',
    },
    listitems: {//Ref:Translation.subscription_table.listitems
      monthly_cost: 'Månadskostnad',
      item1: 'Kostnad efter din gratisperiod',
      item1b: 'Pris efter dina gratis 14 dagar',
      item1c: 'Pris per månad',
      item1d: 'Pris efter dina',
      item1e: 'dagar gratis som pågår t.o.m',
      item1f: 'Månadskostnad efter',
      item2: 'Antal användare',
      item3: 'Senaste böckerna',
      item4: 'Läs & lyssna på mobil och läsplatta',
      item5: 'Obegränsad läsning & lyssning',
      item6: 'Offlineläge',
      item7: 'Böcker på engelska',
      item8: 'Lägen för läsning i solljus & mörker',
      item9: 'Avsluta när du vill',
      item10: 'som slutar',
      infotext: '(2 användare +40 kr per extra användare)',
    },
    family: {
      price_from: 'fr. ',
      users: 'användare',
      days_free_then: '14 dagar gratis sedan',
      currency_per_month: 'kr/mån',
      choose: 'Välj',
    },
  },
  /**************** END SUBSCRIPTION TABLE ******************/
  /**************** FORMS ******************/
  forms: {
    privaceTerm_a: 'Jag godkänner Nextorys',
    privaceTerm_b: 'användarvillkor',
    privaceTerm_c: 'integritetspolicy',
    submit: 'Fortsätt',
    and: 'och',
    labels: {
      email: 'Din E-postadress',
      email_placeholder: 'E-postadress',
      repeat_email: 'Upprepa e-postadress',
      password_label: 'Välj lösenord',
      password: 'Lösenord',
      new_password: 'Välj ett lösenord',
      repeat_password: 'Upprepa lösenord',
      cardnumber: 'Kortnummer',
      cardnumber_placeholder: 'Kortnummer',
      cardmonth: 'Utgångsdatum',
      cardmonth_placeholder: 'Månad',
      cardyear: 'Utgångsår',
      cardyear_placeholder: 'År',
      cardcvv: 'Säkerhetskod (CVV)',
      firstname: 'Förnamn',
      lastname: 'Efternamn',
      address: 'Fyll gärna i din adress',
      postalcode: 'Fyll gärna i ditt postnummer',
      city: 'Fyll gärna i vart du bor',
      phone: 'Fyll gärna i ditt telefonnummer',
      campaigncode: 'Fyll i din kampanjkod',
      giftvoucher: 'Fyll i din presentkod',
    },
    validation: {
      email: 'Vänligen fyll i en giltig e-postadress',
      repeat_email: 'E-postadresserna stämmer inte överens',
      email_exists: 'Denna mailadress är redan registrerad hos oss.',
      password: 'Du måste fylla i ett lösenord för att fortsätta',
      password_length: 'Ditt lösenord måste vara mellan 4 – 25 tecken',
      password_chars: 'Tillåtna tecken: 0-9, a-z, A-Z samt specialtecken: -, @, #, $, %, _',
      password_error: 'Det lösenordet verkar inte stämma.',
      password_repeat: 'Lösenorden stämmer inte överens',
      termserror: 'Du måste godkänna våra användarvillkor och integritetspolicy.',
      campaigncode: 'Vänligen fyll i en kampanjkod',
      campaigncode_error: 'Denna kampanjkoden är inte giltig.',
      giftvoucher: 'Vänligen fyll i din presentkod',
      giftvoucher_error: 'Denna presentkod är inte giltig',
      firstname: 'Vänligen fyll i ditt förnamn',
      lastname: 'Vänligen fyll i ditt efternamn',
      phone: 'Telefonnummer bör vara minst 7 och max 12 siffror',
      card_real_number_error:
        'Det inmatade numret ser inte ut som ett kreditkortsnummer. Vänligen kolla.',
      card_number_error: 'Vänligen fyll i ett giltigt kortnummer',
      month_error: 'Vänligen välj månad',
      year_error: 'Vänligen välj år',
      cvv_error: 'Fyll i CVV nummer',
      cvv_length_error: 'Tre eller fyra siffror',
      connection_error: 'Vi har tekniska problem, försök snart igen',
      warnings: {
        firstname: 'Fyll gärna i ditt förnamn',
        lastname: 'Fyll gärna i ditt efternamn',
        address: 'Fyll gärna i din adress',
        postalcode: 'Fyll gärna i ditt postnummer',
        city: 'Fyll gärna i vart du bor',
        phone: 'Fyll gärna i ditt telefonnummer',
      },
    },
  },
  /**************** END FORMS ******************/
  /**************** API MESSAGES ******************/
  api_messages: {
    error: {
      email1_error: 'Denna mailadress finns redan registrerad hos oss.',
      email2_error: 'Hittade ingen användare med den mailadressen.',
      email3_error: 'Det verkar vara problem att logga in just nu',
      password_error: 'Det lösenordet verkar inte stämma.',
      giftvoucher_error: 'Denna koden är inte giltig.',
      campaigncode_error: 'Denna kampanjkoden är inte giltig.',
      connection_error: 'Vi har tekniska problem, försök snart igen',
      other_region_error: 'Du är en användare i ett annat geografiskt område',
    },
  },
  /**************** END API MESSAGES ******************/
};

export default translation_se;
