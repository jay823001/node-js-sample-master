import { internationalizationLanguage } from 'containers/App/api';
import translation_fi from './finland';
import translation_se from './sweden';

var translation;

if (internationalizationLanguage === "SW") {
  translation = translation_se;
} else if (internationalizationLanguage === "FI") {
  translation = translation_fi;
}

export default translation;
