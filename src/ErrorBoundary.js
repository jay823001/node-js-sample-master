import { Component } from 'react';
import * as Sentry from '@sentry/browser';

export default class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(err, info) {
    this.setState({ hasError: true });
    Sentry.captureException(err);
  }

  render() {
    //when something went wrong redirect to an error page
    // if (this.state.hasError) {
    //    return <h1>Something went wrong.</h1>;
    // }
    return this.props.children;
  }
}