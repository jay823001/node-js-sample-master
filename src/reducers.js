/**
 * Combine all  Redux reducers in this file and export the combined reducers.
 */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import { AppReducers } from './containers/App/reducers/app-reducers';
import { BookReducers } from './containers/Views/Books/reducers';
import { AccountReducers } from './containers/Views/Account/reducer';
import { SignupReducers } from './containers/Views/Registration/reducer';
import { SearchReducer } from './containers/Search/reducer';

// New ui reducers
import UserModule from 'newui/domain/Modules/User';
import BooksModule from 'newui/domain/Modules/Books';

export default combineReducers({
  route: routerReducer,
  appstate: AppReducers,
  signup: SignupReducers,
  bookdata: BookReducers,
  form: formReducer,
  account: AccountReducers,
  search: SearchReducer,

  // New ui reducers
  authKeys: UserModule.reducers.authKeysReducer,
  paymentCheckout: UserModule.reducers.checkoutCardReducer,
  trustlyPaymentUrl: UserModule.reducers.checkoutTrustlyReducer,
  loginState: UserModule.reducers.loginReducer,
  registrationKeys: UserModule.reducers.registrationReducer,
  subscriptionInfo: UserModule.reducers.subscriptionInfoReducer,
  updateUser: UserModule.reducers.updateUserReducer,
  refreshAuth: UserModule.reducers.refreshAuthReducer,
  logout: UserModule.reducers.logoutReducer,
  bookInfo: BooksModule.reducers.bookInfoReducer,
  bookGroup: BooksModule.reducers.bookGroupReducer,
  bookSearch: BooksModule.reducers.bookSearchReducer,
  campaignInfo: UserModule.reducers.campaignInfoReducer,
  campaignDetails: UserModule.reducers.campaignDetailsReducer,
  orders: UserModule.reducers.ordersReducer,
  offers: UserModule.reducers.offersReducer,
  userDetails: UserModule.reducers.userDetailsReducer,
  forgotPassword: UserModule.reducers.forgotPasswordReducer,
  selectedSubscription: UserModule.reducers.selectSubscriptionReducer,
  cms: UserModule.reducers.cmsReducer
});
