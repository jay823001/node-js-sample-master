import UserModule from 'newui/domain/Modules/User'
import { call, put, takeLatest, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { 
  OFFER_DETAILS_REQUEST, 
  OFFER_DETAILS_SUCCESS, 
  OFFER_DETAILS_FAILURE
 } from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* offersSaga() {
  yield takeLatest(OFFER_DETAILS_REQUEST, myOffers)
}

function* myOffers() {
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.getMyOffers, user_authkeys.authkey)
    yield put({ type: OFFER_DETAILS_SUCCESS, offers: response.data.data })
  } catch (error) {
    const err = errorHandler(error, null, null, 'b')
    yield put({ type: OFFER_DETAILS_FAILURE, err: err })
  }
}