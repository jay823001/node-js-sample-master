import { push } from 'react-router-redux'
import UserModule from 'newui/domain/Modules/User'
import { stopSubmit, startSubmit } from 'redux-form'
import { takeLatest, call, put, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  UPDATE_USER_DETAILS_STORE,
  USER_DEATILS_REQUEST,
  ACTIVATE_AGAIN_REQUEST
} from 'newui/domain/Modules/User/actionTypes'

const userUpdateRequest = UserModule.actions.userUpdateRequest
const api = UserModule.api

// watcher saga: watches for actions dispatched to the store, starts worker saga
export function* userDetailSaga() {
  yield takeLatest(UPDATE_USER_REQUEST, workerSaga)
}

// worker saga: makes the api call when watcher saga sees the action
function* workerSaga(details) {
  const userDetails = details.payload
  yield put(startSubmit(userDetails.formName))

  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.UpdateUserDetails, userDetails.formData, user_authkeys)
    yield put({ type: UPDATE_USER_SUCCESS, response })
    yield put({ type: UPDATE_USER_DETAILS_STORE })
    yield put(push(userDetails.nonFormData.navigate))
  } catch (error) {
    const err = errorHandler(error, userDetails, userUpdateRequest, 'b')
    if (err !== 9000) {
      yield put({ type: UPDATE_USER_FAILURE, err })
    }
  }

  yield put(stopSubmit(userDetails.formName))
}

////////////////////////////////////////////////////////////////////////////////////////////////////

export function* fetchUserDetailSaga() {
  yield takeLatest(USER_DEATILS_REQUEST, getUserDetails)
}

function* getUserDetails() {
  yield put({ type: UPDATE_USER_DETAILS_STORE })
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

export function* activateAgainSaga() {
  yield takeLatest(ACTIVATE_AGAIN_REQUEST, activateAgain)
}

function* activateAgain() {
  const user_authkeys = yield select(state => state.authKeys)
  const user_details = yield select(state => state.userDetails)
  yield call(api.activateAgain, user_details, user_authkeys)
  yield put({ type: UPDATE_USER_DETAILS_STORE })
}
