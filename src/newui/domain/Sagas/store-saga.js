import UserModule from 'newui/domain/Modules/User'
import { takeLatest, call, put, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  UPDATE_USER_DETAILS_STORE,
  SET_USERDETAILS_REQUEST
} from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

// Update userdetail in store
export function* updateUserStoreSaga() {
  yield takeLatest(UPDATE_USER_DETAILS_STORE, updateUser)
}

function* updateUser() {
  const user_authkeys = yield select(state => state.authKeys)
  try {
    const user = yield call(api.fetchUserDetails, user_authkeys.authkey)
    yield put({ type: SET_USERDETAILS_REQUEST, user: user.data.data })
  } catch (error) {
    errorHandler(error, null)
  }
}
