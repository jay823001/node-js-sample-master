import { store } from 'store'
import UserModule from 'newui/domain/Modules/User'
import { call, put, takeLatest, select } from 'redux-saga/effects'
import {
  REFRESHAUTH_REQUEST,
  REGISTRATION_SUCCESS
} from 'newui/domain/Modules/User/actionTypes'

const logoutRequest = UserModule.actions.logoutRequest
const api = UserModule.api

function* RefreshAuth(authData) {
  const requestData = authData.data
  const actionData = requestData.data.data
  const actionToFire = requestData.action
  try {

    const user_authkeys = yield select(state => state.authKeys)
    user_authkeys['flow'] = requestData.flow
    const tokenResponse = yield call(api.refreshAuthTokens, user_authkeys)
    const registrationData = {
      authkey: tokenResponse.headers['nx-at'],
      refreshkey: user_authkeys.refreshkey,
    }
    yield put({ type: REGISTRATION_SUCCESS, registrationData })
    store.dispatch(actionToFire(actionData))

  } catch (error) {

    store.dispatch(logoutRequest())

  }
}

// Register Registration saga to global sagas
export function* refreshAuthTokenSaga() {
  yield takeLatest(REFRESHAUTH_REQUEST, RefreshAuth)
}