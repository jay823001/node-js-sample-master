import { push } from 'react-router-redux'
import UserModule from 'newui/domain/Modules/User'
import { takeLatest, call, put, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  SUBSCRIPTION_DETAILS_SUCCESS,
  SUBSCRIPTION_DETAILS_FAILURE,
  SUBSCRIPTION_DETAILS_REQUEST,
} from 'newui/domain/Modules/User/actionTypes'

const subscriptionDetailsRequest = UserModule.actions.subscriptionDetailsRequest
const api = UserModule.api

// watcher saga: watches for actions dispatched to the store, starts worker saga
export function* subscriptionDetailSaga() {
  yield takeLatest(SUBSCRIPTION_DETAILS_REQUEST, workerSaga)
}

// worker saga: makes the api call when watcher saga sees the action
function* workerSaga(subscriptionDetails) {
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.fetchSubscriptionDetails, subscriptionDetails, user_authkeys)
    const subscriptionDdata = response.data
    yield put({ type: SUBSCRIPTION_DETAILS_SUCCESS, subscriptionDdata })
    if (subscriptionDdata.data.paymentrequired) {
      yield put(push(subscriptionDetails.subscriptionDetails.navigate))
    } else {
      // if not required to pay code goes here
    }

  } catch (error) {
    
    const err = errorHandler(error, subscriptionDetails, subscriptionDetailsRequest, 'b')
    if (err !== 9000) {
      yield put({ type: SUBSCRIPTION_DETAILS_FAILURE, err })
    }

  }

}