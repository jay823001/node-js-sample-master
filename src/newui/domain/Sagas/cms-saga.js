import UserModule from 'newui/domain/Modules/User'
import { call, put, takeLatest } from 'redux-saga/effects'
// import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { 
  CMS_CONTENT_REQUEST, 
  CMS_CONTENT_SUCCESS
 } from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* cmsSaga() {
  yield takeLatest(CMS_CONTENT_REQUEST, cmsContent)
}

function* cmsContent(data) {
  try {
    const content_response = yield call(api.getCMScontent, data.payload)
    const meta_response = yield call(api.getCMSmeta, data.payload)
    yield put({ type: CMS_CONTENT_SUCCESS, content: content_response.data.data, meta: meta_response.data.data })
  } catch (error) {
    // NOTE: Api needs to send proper error resoponses

    // const err = errorHandler(error, null, null, 'b')
    // yield put({ type: OFFER_DETAILS_FAILURE, err: err })
  }
}