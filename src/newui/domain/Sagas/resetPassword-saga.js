import { push } from 'react-router-redux'
import UserModule from 'newui/domain/Modules/User'
import { takeLatest, call, put } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE
} from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* resetPasswordSaga(data) {
  yield takeLatest(RESET_PASSWORD_REQUEST, resetPassword)
}

function* resetPassword(data) {
  try {
    yield call(api.resetPassword, data.payload)
    yield put({ type: RESET_PASSWORD_SUCCESS })
    yield put(push(`${data.payload.navigate}`))
  } catch (error) {
    const err = errorHandler(error, null, null, 'b')
    yield put({ type: RESET_PASSWORD_FAILURE, err: err })
  }  
}
