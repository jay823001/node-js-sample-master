import { persistor } from 'store'
import { push } from 'react-router-redux'
import { removeCookies } from 'newui/utils/helperFunctions'
import { SET_AUTH } from 'containers/Views/Account/LoginPage/constants'
import UserModule from 'newui/domain/Modules/User'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  LOGOUT_REQUEST,
  REMOVE_USERDETAILS_REQUEST,
  SUBSCRIPTION_DETAILS_FAILURE,
  AUTH_KEYS_CLEAR
} from 'newui/domain/Modules/User/actionTypes'
import {
  call,
  put,
  takeLatest,
  all,
  select
} from 'redux-saga/effects'

const logoutUser = UserModule.api.logoutUser

function* Logout(isVisitor) {

  // old flow requirments
  const cookiesToRemove = ['user', 'reg', 'gift', 'retry', 'trustly',]
  yield all(cookiesToRemove.map(cookieName => call(removeCookies, cookieName, { path: '/', maxAge: 1209600 })))
  localStorage.clear()
  yield put({ type: SET_AUTH, newAuthState: false })

  //new flow requirements
  persistor.purge()
  if (isVisitor) {
    const user_authkeys = yield select(state => state.authKeys)
    try {
      yield call(logoutUser, user_authkeys.authkey)
    } catch (error) {
      errorHandler(error, isVisitor)
    }    
  }
  yield put({ type: AUTH_KEYS_CLEAR })
  yield put({ type: REMOVE_USERDETAILS_REQUEST })
  yield put({ type: SUBSCRIPTION_DETAILS_FAILURE })
  yield put(push('/'))
}

export function* logoutFlowSaga() {
  yield takeLatest(LOGOUT_REQUEST, Logout)
}