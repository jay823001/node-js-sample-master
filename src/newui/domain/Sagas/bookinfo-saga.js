import _ from 'lodash'
import { replace } from 'react-router-redux'
import { call, all, put, takeLatest } from 'redux-saga/effects'

import BooksModule from 'newui/domain/Modules/Books'

import { BOOKINFO_REQUEST, BOOKINFO_SUCCESS } from 'newui/domain/Modules/Books/actionTypes'

function* bookInfo({ data }) {
  const response = yield call(BooksModule.api.fetchBookInfo, { data: data })

  if (_.isEmpty(response.data.data) || response.data.data.error) {
    return yield put(replace('/hoppsan'))
  }

  const books = [response.data.data.books]

  if (books[0].relisbn) {
    const related = yield call(BooksModule.api.fetchBookInfo, {
      data: { id: books[0].relisbn },
    })

    books.push(related.data.data.books)
  }

  const relatedGroups = yield call(BooksModule.api.fetchBookGroups, {
    data: { view: 'related', bookId: books[0].id },
  })

  let relatedItems = []

  if (relatedGroups.data.data.bookgroups) {
    const holder = [
      relatedGroups.data.data.bookgroups.find(item => item.slugname.includes('series')),
      relatedGroups.data.data.bookgroups.find(item => item.slugname.includes('rec')),
      { id: `r_${books[0].id}_authors`, type: books[0].type },
    ]

    relatedItems = yield all(
      holder.map(item => {
        if (item) {
          return BooksModule.api
            .fetchBooks(item.id, item.id.includes('series') ? 0 : 12, books[0].type)
            .then(response => {
              return { id: item.id, data: response.data.data.books }
            })
            .catch(error => { })
        }

        return { id: null, data: [] }
      })
    )
  }

  yield put({ type: BOOKINFO_SUCCESS, books: books, related: relatedItems })
}

export function* bookInfoSaga() {
  yield takeLatest(BOOKINFO_REQUEST, bookInfo)
}
