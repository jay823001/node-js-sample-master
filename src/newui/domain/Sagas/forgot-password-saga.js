import { push } from 'react-router-redux'
import UserModule from 'newui/domain/Modules/User'
import { startSubmit, stopSubmit } from 'redux-form'
import { call, put, takeLatest } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { 
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE
  } from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* forgotPasswordSaga() {
  yield takeLatest(FORGOT_PASSWORD_REQUEST, forgotPassword)
}

function* forgotPassword(data) {
  const FormDetails = data.payload
  let errors = {}
  yield put(startSubmit('ForgotPasswordForm'))
  try {
    yield call(api.forgotPassword, FormDetails)
    yield put({ type: FORGOT_PASSWORD_SUCCESS })
    yield put(push(`${FormDetails.navigate}`))
  } catch (error) {
    const err = errorHandler(error, data)
    errors.email = err
    yield put({ type: FORGOT_PASSWORD_FAILURE, err: err })
  }
  
  yield put(stopSubmit('ForgotPasswordForm', errors))
}