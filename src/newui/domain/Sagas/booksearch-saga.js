import { call, put, takeLatest } from 'redux-saga/effects'

import BooksModule from 'newui/domain/Modules/Books'

import { BOOKSEARCH_REQUEST, BOOKSEARCH_SUCCESS } from 'newui/domain/Modules/Books/actionTypes'

function* bookSearch({ data }) {
  let books = [{ data: [], page: 1, count: 0 }]

  if (data.search) {
    const response = yield call(BooksModule.api.fetchSearch, {
      data: { query: data.query, page: parseInt(data.page) - 1, type: data.type },
    })

    books = [
      {
        page: parseInt(data.page) || 1,
        data: response.data.data.books,
        count: response.data.data.bookcount,
      },
    ]
  }

  const fetchFacets = yield call(BooksModule.api.fetchSearchFacets, {
    data: { query: data.query },
  })
  const facets = fetchFacets.data.data.books

  yield put({ type: BOOKSEARCH_SUCCESS, books: books, facets: facets })
}

export function* bookSearchSaga() {
  yield takeLatest(BOOKSEARCH_REQUEST, bookSearch)
}
