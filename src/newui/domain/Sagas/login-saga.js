import { Base64 } from 'js-base64'
import { push } from 'react-router-redux'
import UserModule from 'newui/domain/Modules/User'
import { setCookies } from 'newui/utils/helperFunctions'
import { startSubmit, stopSubmit } from 'redux-form'
import { call, put, takeLatest } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  LOGIN_NEW_REQUEST,
  REGISTRATION_SUCCESS,
  SET_USERDETAILS_REQUEST,
  LOGIN_NEW_SUCCESS,
  LOGIN_NEW_FAILURE
} from 'newui/domain/Modules/User/actionTypes'
import { REFRESH_STORE } from 'containers/Views/Account/MyAccount/constants'

const api = UserModule.api

export function* loginSaga() {
  yield takeLatest(LOGIN_NEW_REQUEST, login)
}

function* login(data) {
  const FormDetails = data.payload
  yield put(startSubmit(FormDetails.formName))

  const logindata = {
    email: FormDetails.formData.email,
    password: FormDetails.formData.password
  }

  try {
    const userdata = yield call(api.fetchLogin, logindata)
    if (userdata.data.data.ismember || userdata.data.data.isoldmember) {
      const userDetails = yield call(api.fetchUserDetails, userdata.headers['nx-at'])
      const registrationData = {
        authkey: userdata.headers['nx-at'],
        refreshkey: userdata.headers['nx-rt']
      }
      userdata.data.email = FormDetails.formData.email
      userdata.data.p = FormDetails.formData.password
      userdata.data.authkey = userdata.headers['nx-at']
      userdata.data.refreshkey = userdata.headers['nx-rt']
      userdata.data.UserDetails = userDetails
      const userEncode = Base64.encode(JSON.stringify(userdata.data))
      yield call(setCookies, 'user', userEncode, { path: '/', maxAge: 1209600 })

      yield put({ type: LOGIN_NEW_SUCCESS })
      yield put(stopSubmit(FormDetails.formName))
      yield put({ type: REGISTRATION_SUCCESS, registrationData })
      yield put({ type: SET_USERDETAILS_REQUEST, user: userDetails.data.data })
      yield put({ type: REFRESH_STORE, authkey: userdata.authkey }) // old flow call
      yield put(push(`${FormDetails.navigate}`))
    } else {
      yield put(push(FormDetails.trialFlowUrl))
    }
  } catch (error) {
    const err = errorHandler(error, data)
    yield put({ type: LOGIN_NEW_FAILURE, err: err })
  }
  yield put(stopSubmit(FormDetails.formName))

}