
import UserModule from 'newui/domain/Modules/User'
import errorHandler from 'newui/domain/Modules/Common/error-handler'

import { Base64 } from 'js-base64'
import { push } from 'react-router-redux'
import { SET_AUTH } from 'containers/Views/Account/LoginPage/constants' // Old App
import { CREATEUSER_SETAUTH } from 'containers/Views/Registration/constants' // Old App
import {
  put,
  call,
  select,
  takeLatest
} from 'redux-saga/effects'
import {
  cookies,
  setCookies,
  removeCookies,
  createUserCookieData
} from 'newui/utils/helperFunctions'
import {
  PAYMENT_SUCCESS,
  MEMBER_LOGIN_SUCCESS,
  REGISTRATION_SUCCESS,
  PAYMENTRESPONSE_SUCCESS,
  UPDATE_USER_DETAILS_STORE,
  SET_USERDETAILS_REQUEST
} from 'newui/domain/Modules/User/actionTypes'

const createPaymentResponseRequest = UserModule.actions.createPaymentResponseRequest
const api = UserModule.api

function* CompletePayment(data) {
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const user_state = yield select(state => state.userDetails)
    const response = yield call(api.postPaymentResponse, data.data, user_authkeys)

    if (user_state.isLoggedIn) {
      // update userdetails in the store
      const UserDetails = yield call(api.fetchUserDetails, user_authkeys.authkey)
      yield put({ type: SET_USERDETAILS_REQUEST, user: UserDetails.data.data })

      yield put(push(`${data.data.successURL}`))
      yield put({ type: PAYMENT_SUCCESS, authkey: UserDetails.headers['nx-at'] })

    } else {
      response['authkey'] = response.headers['nx-at']
      response['refreshkey'] = response.headers['nx-rt']

      // get user details from cookies
      const loginData = {}
      if (cookies.get('visitor')) {
        loginData['data'] = JSON.parse(Base64.decode(cookies.get('visitor')))
      } else {
        loginData['data'] = JSON.parse(Base64.decode(cookies.get('user')))
      }
      loginData.data.authkey = data.data.authkey

      try {

        const loginUserResponse = yield call(api.fetchLoginUser, loginData, user_authkeys)

        const authkey2 = loginUserResponse.headers['nx-at']
        const refreshkey2 = loginUserResponse.headers['nx-rt']

        loginUserResponse['authkey'] = authkey2
        loginUserResponse['refreshkey'] = refreshkey2

        // START Integration with old Flow
        /**********************************************/
        // update userdetails in the store
        const UserDetails = yield call(api.fetchUserDetails, authkey2)
        yield put({ type: SET_USERDETAILS_REQUEST, user: UserDetails.data.data })

        // set a cookie token to preserve logged in user on page refresh
        const userEncode = createUserCookieData(UserDetails, authkey2, refreshkey2)
        yield call(setCookies, 'user', userEncode, { path: '/', maxAge: 1209600 })

        // set auth to true
        yield put({ type: SET_AUTH, newAuthState: true })

        // logout registration user
        yield call(removeCookies, 'reg', { path: '/' })
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: false })
        /**********************************************/
        // END Integration with old Flow

        yield put({ type: MEMBER_LOGIN_SUCCESS, userDetails: loginUserResponse })

        const registrationData = {
          authkey: authkey2,
          refreshkey: refreshkey2,
        }

        yield put({ type: REGISTRATION_SUCCESS, registrationData })
        yield put({ type: UPDATE_USER_DETAILS_STORE })

        // create member cookie since user is no more a visitor after paying
        yield call(setCookies, 'member', cookies.get('visitor'), { path: '/', maxAge: 1209600 })
        yield call(removeCookies, 'visitor', { path: '/' })
        // redirect user to transresp page
        let campaignName = data.data.flow === 'campaign' ? `&campaignname=${data.data.campaign.name}` : ''
        yield put(push(`${data.data.successURL}?orderid=${response.data.data.orderref}${campaignName}`))
        window.location.reload()
      } catch (loginError) {
        errorHandler(loginError, data, createPaymentResponseRequest, 'b')
      }

      //dispatch payment success details to store
      yield put({ type: PAYMENT_SUCCESS, authkey: data.data.authkey })
    }

  } catch (error) {
    const initialPaymentRequestDetails = JSON.parse(Base64.decode(cookies.get('paymentRequest')))
    yield put(push(`${initialPaymentRequestDetails.data.route}?paymentstatus=failed`))
  }
}

// Register PaymentResponse saga to global sagas
export function* paymentResponseSaga() {
  yield takeLatest(PAYMENTRESPONSE_SUCCESS, CompletePayment)
}