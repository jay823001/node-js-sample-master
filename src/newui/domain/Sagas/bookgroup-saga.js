import _ from 'lodash'
import { replace } from 'react-router-redux'
import { call, all, put, takeLatest } from 'redux-saga/effects'

import BooksModule from 'newui/domain/Modules/Books'

import { BOOKGROUP_REQUEST, BOOKGROUP_SUCCESS } from 'newui/domain/Modules/Books/actionTypes'

function* bookGroup({ data }) {
  let response = yield call(BooksModule.api.fetchBookGroups, {
    data: { id: data.id, view: data.id ? 'level2' : 'main' },
  })

  if (_.isEmpty(response.data.data) && data.id) {
    response.data.data.bookgroups = [{ id: data.id, slugname: data.id, type: '' }]
  }

  const fetchCategories = yield call(BooksModule.api.fetchBookGroups, {
    data: { view: 'main' },
  })
  const categories = fetchCategories.data.data.bookgroups.filter(item =>
    item.type.includes(' Category')
  )
  const parent = categories.find(
    item => item.id === _.get(response.data.data, 'bookgroups[0].parentid')
  )

  const holder = response.data.data.bookgroups.filter(item => {
    const status = item.type.includes(' Category') || item.type.includes('Sub-Category')
    const subCategory = item.parentid ? !item.parentid.includes('sc_') : true

    return subCategory && status && (parent ? parent.haschild !== 0 : true)
  })

  const offset = (parseInt(data.page) - 1) * 5 || 0

  let filter =
    holder.length < 1 ? [{ slugname: data.id, id: data.id }] : holder.slice(offset, offset + 5)

  const bookGroups = filter

  const books = yield all(
    bookGroups.map(item => {
      return BooksModule.api
        .fetchBooks(
          item.slugname,
          holder.length < 1 ? 42 : 12,
          data.type,
          holder.length < 1 ? parseInt(data.page) - 1 : 0
        )
        .then(response => {
          return {
            id: item.id,
            page: parseInt(data.page) || 1,
            data: response.data.data.books,
            count: response.data.data.bookcount,
            title: parent ? parent.title : undefined,
          }
        })
        .catch(error => { })
    })
  )

  if (_.isEmpty(books[0]) || _.isEmpty(books[0].data)) {
    return yield put(replace('/hoppsan'))
  }

  yield put({
    type: BOOKGROUP_SUCCESS,
    groups: bookGroups,
    books: books,
    categories: categories,
    allGroups: holder,
  })
}

export function* bookGroupSaga() {
  yield takeLatest(BOOKGROUP_REQUEST, bookGroup)
}
