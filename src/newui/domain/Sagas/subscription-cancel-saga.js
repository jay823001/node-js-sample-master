import { push } from 'react-router-redux'
import UserModule from 'newui/domain/Modules/User'
import { startSubmit, stopSubmit } from 'redux-form'
import { setCookies } from 'newui/utils/helperFunctions'
import { call, put, takeLatest, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { 
  CANCEL_SUBSCRIPTION_REQUEST,
  UPDATE_USER_DETAILS_STORE
 } from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* cancelSubscriptionSaga() {
  yield takeLatest(CANCEL_SUBSCRIPTION_REQUEST, cancelSubscription)
}

function* cancelSubscription(data) {
  yield put(startSubmit('cancelSubscriptionReason'))
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.cancelSubscription, data.payload.data, user_authkeys.authkey)
    yield put({ type: UPDATE_USER_DETAILS_STORE })
    yield call(setCookies, 'cancelDate', response.data.data.nextrundate, { path: '/', maxAge: 1209600 })
    yield put(push(data.payload.data.navigate))
  } catch (error) {
    errorHandler(error, data)
  }
  yield put(stopSubmit('cancelSubscriptionReason'))
}