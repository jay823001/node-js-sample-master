import { push } from 'react-router-redux'
import { Base64 } from 'js-base64'
import { 
     REGISTER_CAMPAIGN_REQUEST,
     CAMPAIGN_SELECTED,
     REGISTRATION_FAILURE,
     CAMPAIGN_DETAILS_REQUEST,
     CAMPAIGN_DETAILS_SUCCESS,
     CAMPAIGN_DETAILS_FAILURE
} from 'newui/domain/Modules/User/actionTypes'
import { call, put, takeLatest, select } from 'redux-saga/effects'
import UserModule from 'newui/domain/Modules/User'
import { startSubmit, stopSubmit } from 'redux-form'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { setCookies } from 'newui/utils/helperFunctions'
import { CREATEUSER_SETAUTH } from 'containers/Views/Registration/constants' // Old App

const api = UserModule.api

function* registerCampaign(data) {
    let errors = {}
    const registrationDetails = data.registrationDetails
    yield put(startSubmit(registrationDetails.formName))
    try {
      
        const user_authkeys = yield select(state => state.authKeys)

        // START Integration with old Flow
        /**********************************************/
    
        const reg = {
          authkey: user_authkeys.authkey,
          email: registrationDetails.data.email,
          password: registrationDetails.data.base64Password,
        }
        const userEncode = Base64.encode(JSON.stringify(reg))
        yield call(setCookies, 'reg', userEncode, { path: '/', maxAge: 1209600 })
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: true })
    
        /**********************************************/
        // END Integration with old Flow

        const response = yield call(api.registerCampaignCode, registrationDetails.campaignCode, user_authkeys.authkey)
        response.data.data['code'] = registrationDetails.campaignCode
        yield put({ type: CAMPAIGN_SELECTED, payload: response.data.data })
        yield put(push(registrationDetails.navigate))
      } catch (error) {
        errors.code = 'registerForm.campaigncode_error'
        const err = errorHandler(error, registrationDetails)
        yield put({ type: REGISTRATION_FAILURE, err })
      }
      yield put(stopSubmit(registrationDetails.formName, errors))
}

function* getCampaignDetails(data) {
  try {
    const response = yield call(api.campaignDetails, data.payload)
    if (response.status === 200) {
      if (response.data.status === 200) {
        yield put({ type: CAMPAIGN_DETAILS_SUCCESS, campaignData: response.data.data })
      } else {
        yield put({ type: CAMPAIGN_DETAILS_FAILURE, err: response.data.error })
      }      
    }
  } catch (error) {
    const err = errorHandler(error, data)
    yield put({ type: CAMPAIGN_DETAILS_FAILURE, err })
  }
  
}

// Register Registration saga to global sagas
export function* campaignRegisterSaga() {
  yield takeLatest(REGISTER_CAMPAIGN_REQUEST, registerCampaign)
}

export function* campaignDetailsSaga() {
  yield takeLatest(CAMPAIGN_DETAILS_REQUEST, getCampaignDetails)
}
