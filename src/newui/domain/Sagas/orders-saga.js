import UserModule from 'newui/domain/Modules/User'
import { call, put, takeLatest, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { 
  ORDER_DETAILS_REQUEST,
  ORDER_DETAILS_SUCCESS,
  ORDER_DETAILS_FAILURE,
  ORDER_INFO_REQUEST,
  ORDER_INFO_SUCCESS } from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* ordersSaga() {
  yield takeLatest(ORDER_DETAILS_REQUEST, myOrders)
}

export function* orderReceiptSaga() {
  yield takeLatest(ORDER_INFO_REQUEST, receiptDetails)
}

function* myOrders() {
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.getmyOrders, user_authkeys.authkey)
    yield put({ type: ORDER_DETAILS_SUCCESS, orders: response.data.data })
  } catch (error) {
    const err = errorHandler(error, null)
    yield put({ type: ORDER_DETAILS_FAILURE, err: err })
  }
}

function* receiptDetails(data) {
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.getReceiptDetails, data.payload.orderId, user_authkeys.authkey)
    yield put({ type: ORDER_INFO_SUCCESS, receipt: response.data.data })
  } catch (error) {
    const err = errorHandler(error, null)
    yield put({ type: ORDER_DETAILS_FAILURE, err: err })
  }
}