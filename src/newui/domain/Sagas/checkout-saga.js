
import UserModule from 'newui/domain/Modules/User'
import errorHandler from 'newui/domain/Modules/Common/error-handler'

import { Base64 } from 'js-base64'
import { push } from 'react-router-redux'
import { SET_AUTH } from 'containers/Views/Account/LoginPage/constants' // Old App
import { CREATEUSER_SETAUTH } from 'containers/Views/Registration/constants' // Old App
import {
  stopSubmit,
  startSubmit
} from 'redux-form'
import {
  put,
  call,
  select,
  takeLatest
} from 'redux-saga/effects'
import {
  cookies,
  redirectTo,
  setCookies,
  removeCookies
} from 'newui/utils/helperFunctions'
import {
  PAYMENT_REQUEST,
  PAYMENT_SUCCESS,
  PAYMENT_FAILURE,
  MEMBER_LOGIN_SUCCESS,
  REGISTRATION_SUCCESS,
  UPDATE_USER_DETAILS_STORE
} from 'newui/domain/Modules/User/actionTypes'

const createPaymentRequest = UserModule.actions.createPaymentRequest
const api = UserModule.api

function* MakePayment(data) {
  yield put(startSubmit('CreditCardPaymentForm'))
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const user_state = yield select(state => state.userDetails)
    const response = yield call(api.postPaymentDetails, data, user_authkeys)

    if (user_state.isLoggedIn) {
      if (data.data.paymentgateway !== 'ADYEN') {
        yield call(setCookies, 'paymentRequest', Base64.encode(JSON.stringify(data)), { path: '/', maxAge: 1209600 })
        // redirect user to - 3rd party payment site Ex: Trustly, Sofort, Giropay etc...
        yield call(redirectTo, response.data.data.redirecturl)
      } else {
        yield put({ type: UPDATE_USER_DETAILS_STORE })
        yield put(push(`${data.data.successURL}`))
        yield put({ type: PAYMENT_SUCCESS, authkey: user_authkeys.authkey })
      }

    } else {
      const authkey = response.headers['nx-at']
      const refreshkey = response.headers['nx-rt']

      response['authkey'] = authkey
      response['refreshkey'] = refreshkey

      // get user details from cookies
      const loginData = {}
      if (cookies.get('visitor')) {
        loginData['data'] = JSON.parse(Base64.decode(cookies.get('visitor')))
      } else {
        loginData['data'] = JSON.parse(Base64.decode(cookies.get('user')))
      }
      loginData.data.authkey = data.data.authkey

      try {
        if (data.data.paymentgateway !== 'ADYEN') {
          yield call(setCookies, 'paymentRequest', Base64.encode(JSON.stringify(data)), { path: '/', maxAge: 1209600 })
          // redirect user to - 3rd party payment site Ex: Trustly, Sofort, Giropay etc...
          yield call(redirectTo, response.data.data.redirecturl)

        } else {
          const loginUserResponse = yield call(api.fetchLoginUser, loginData, user_authkeys)
          
          const authkey2 = loginUserResponse.headers['nx-at']
          const refreshkey2 = loginUserResponse.headers['nx-rt']

          loginUserResponse['authkey'] = authkey2
          loginUserResponse['refreshkey'] = refreshkey2

          // START Integration with old Flow
          /**********************************************/
          // user details fetch
          // const UserDetails = yield call(api.fetchUserDetails, authkey2)
          // yield put({ type: SET_USERDETAILS_REQUEST, user: UserDetails.data.data })

          // set a cookie token to preserve logged in user on page refresh
          // const userEncode = createUserCookieData(UserDetails, authkey2, refreshkey2)
          // yield call(setCookies, 'user', userEncode, { path: '/', maxAge: 1209600 })

          // set auth to true
          yield put({ type: SET_AUTH, newAuthState: true })

          // logout registration user
          yield call(removeCookies, 'reg', { path: '/' })
          yield put({ type: CREATEUSER_SETAUTH, newAuthState: false })
          /**********************************************/
          // END Integration with old Flow

          yield put({ type: MEMBER_LOGIN_SUCCESS, userDetails: loginUserResponse })

          const registrationData = {
            authkey: authkey2,
            refreshkey: refreshkey2,
          }

          yield put({ type: REGISTRATION_SUCCESS, registrationData })
          yield put({ type: UPDATE_USER_DETAILS_STORE })

          // create member cookie since user is no more a visitor after paying
          yield call(setCookies, 'member', cookies.get('visitor'), { path: '/', maxAge: 1209600 })
          yield call(removeCookies, 'visitor', { path: '/' })
          // redirect user to transresp page
          if (data.data.flow === 'trial') {
            yield put(push(`${data.data.successURL}?orderid=${response.data.data.orderid}`))
          } else if (data.data.flow === 'campaign') {
            yield put(push(`${data.data.successURL}?orderid=${response.data.data.orderid}&campaignname=${data.data.campaign.name}`))
          } else if (data.data.flow === 're-activate') {
            yield put(push(`${data.data.successURL}`))
          } else {
            // default goes to trial page
            yield put(push(`${data.data.successURL}?orderid=${response.data.data.orderid}`))
          }

        }
      } catch (loginError) {
        errorHandler(loginError, data, createPaymentRequest, 'b')
      }

      //dispatch payment success details to store
      yield put({ type: PAYMENT_SUCCESS, authkey: data.data.authkey })
    }

  } catch (error) {
    const err = errorHandler(error, data, createPaymentRequest, 'b')
    if (err !== 9000) {
      yield put({ type: PAYMENT_FAILURE, err })
    }

  }
  yield put(stopSubmit('CreditCardPaymentForm'))
}

// Register Registration saga to global sagas
export function* createPaymentRequestSaga() {
  yield takeLatest(PAYMENT_REQUEST, MakePayment)
}