import UserModule from 'newui/domain/Modules/User'
import { call, put, takeLatest, select } from 'redux-saga/effects'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import {
  UPDATE_SUBSCRIPTION_REQUEST,
  UPDATE_SUBSCRIPTION_SUCCESS,
  UPDATE_USER_DETAILS_STORE,
  UPDATE_SUBSCRIPTION_FAILURE
} from 'newui/domain/Modules/User/actionTypes'

const api = UserModule.api

export function* updateSubscriptionNewSaga() {
  yield takeLatest(UPDATE_SUBSCRIPTION_REQUEST, updateSubscription)
}

function* updateSubscription(data) {
  try {
    const user_authkeys = yield select(state => state.authKeys)
    const response = yield call(api.updateSubscription, data.payload, user_authkeys.authkey)
    yield put({ type: UPDATE_SUBSCRIPTION_SUCCESS, subscription: response.data.data })
    yield put({ type: UPDATE_USER_DETAILS_STORE })
  } catch (error) {
    errorHandler(error, data)
    yield put({ type: UPDATE_SUBSCRIPTION_FAILURE, err: 'error' })
  }
}