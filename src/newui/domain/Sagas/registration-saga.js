import { Base64 } from 'js-base64'
import UserModule from 'newui/domain/Modules/User'
import { startSubmit, stopSubmit } from 'redux-form'
import { setCookies } from 'newui/utils/helperFunctions'
import { call, put, takeLatest } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import errorHandler from 'newui/domain/Modules/Common/error-handler'
import { CREATEUSER_SETAUTH } from 'containers/Views/Registration/constants' // Old App
import {
  REGISTRATION_REQUEST,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAILURE,
  REGISTER_CAMPAIGN_REQUEST,
  LOGIN_NEW_REQUEST
} from 'newui/domain/Modules/User/actionTypes'
import { fetchLogin } from 'containers/App/api'

const api = UserModule.api

function* Registration(details) {
  const registrationDetails = details.payload

  if (registrationDetails.formName === 'RegistrationForm') {
    yield put(startSubmit(registrationDetails.formName))
  }

  if (registrationDetails.formName === 'CampaignRegistrationForm' && registrationDetails.loggedIn) {

    yield put({ type: REGISTER_CAMPAIGN_REQUEST, registrationDetails })
  } else {
    try {
      const response = yield call(api.postUserRegister, registrationDetails.data)
      const visitorEncode = Base64.encode(JSON.stringify(registrationDetails))

      yield call(setCookies, 'visitor', visitorEncode, { path: '/', maxAge: 1209600 })

      // START Integration with old Flow
      /**********************************************/

      const reg = {
        authkey: response.headers['nx-at'],
        email: registrationDetails.data.email,
        password: registrationDetails.data.base64Password,
      }
      const userEncode = Base64.encode(JSON.stringify(reg))
      yield call(setCookies, 'reg', userEncode, { path: '/', maxAge: 1209600 })
      yield put({ type: CREATEUSER_SETAUTH, newAuthState: true })

      /**********************************************/
      // END Integration with old Flow

      const registrationData = {
        authkey: response.headers['nx-at'],
        refreshkey: response.headers['nx-rt']
      }

      yield put({ type: REGISTRATION_SUCCESS, registrationData })
      if (registrationDetails.formName === 'CampaignRegistrationForm') {
        yield put({ type: REGISTER_CAMPAIGN_REQUEST, registrationDetails })
      } else if (registrationDetails.formName === 'RegistrationForm') {
        yield put(push(registrationDetails.navigate))
        yield put(stopSubmit(registrationDetails.formName))
      }

    } catch (error) {
      const isUserExists = (
        error.response.status === 409 &&
        error.response.data.error.code === 6000 &&
        error.response.data.error.msg === 'USER_ALREADY_EXSISTS'
      )

      if (registrationDetails.formName === 'CampaignRegistrationForm' && isUserExists) {

        const logindata = {
          data: {
            authkey: null,
            email: registrationDetails.data.email,
            password: registrationDetails.data.base64Password,
          },
        }

        const userdata = yield call(fetchLogin, logindata)
        if (userdata.status === 200) {

          const UserDetails = yield call(api.fetchUserDetails, userdata.authkey)
          userdata.data.email = registrationDetails.data.email
          userdata.data.p = registrationDetails.data.password
          userdata.data.authkey = userdata.authkey
          userdata.data.refreshkey = userdata.refreshkey
          userdata.data.UserDetails = UserDetails
          const memberEncode = Base64.encode(JSON.stringify(userdata.data))
          yield call(setCookies, 'visitor', memberEncode, { path: '/', maxAge: 1209600 })

          // START Integration with old Flow
          /**********************************************/

          const reg = {
            authkey: userdata.authkey,
            email: registrationDetails.data.email,
            password: registrationDetails.data.base64Password,
          }

          const userEncode = Base64.encode(JSON.stringify(reg))
          yield call(setCookies, 'reg', userEncode, { path: '/', maxAge: 1209600 })
          yield put({ type: CREATEUSER_SETAUTH, newAuthState: true })

          /**********************************************/
          // END Integration with old Flow

          const registrationData = {
            authkey: userdata.authkey,
            refreshkey: userdata.refreshkey
          }

          yield put({ type: REGISTRATION_SUCCESS, registrationData })

          yield put({ type: REGISTER_CAMPAIGN_REQUEST, registrationDetails })
        } else {
          if (userdata.error) {
            const customErrObject = {
              oldflowerror: true,
              status: 401,
              code: 3001
            }
            const err = errorHandler(customErrObject, logindata)
            yield put({ type: REGISTRATION_FAILURE, err })
            yield put(stopSubmit(details.payload.formName))
          }
        }
      } else if (isUserExists) {
        const payload = {
          formData: {
            authkey: null,
            email: registrationDetails.data.email,
            password: registrationDetails.data.base64Password,
          },
          navigate: registrationDetails.myAccoutUrl,
          formName: 'LoginForm',
        }

        yield put({ type: LOGIN_NEW_REQUEST, payload })

      }
      else {
        const err = errorHandler(error, registrationDetails)
        yield put({ type: REGISTRATION_FAILURE, err })
        yield put(stopSubmit(details.payload.formName))
      }
    }
  }
}

// Register Registration saga to global sagas
export function* userRegisterSaga() {
  yield takeLatest(REGISTRATION_REQUEST, Registration)
}