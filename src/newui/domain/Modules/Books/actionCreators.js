import * as actions from './actionTypes'

export const getBookInfo = data => ({
  type: actions.BOOKINFO_REQUEST,
  data,
})

export const getBookGroup = data => ({
  type: actions.BOOKGROUP_REQUEST,
  data,
})

export const getBookSearch = data => ({
  type: actions.BOOKSEARCH_REQUEST,
  data,
})
