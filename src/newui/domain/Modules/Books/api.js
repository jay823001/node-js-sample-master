import axios from 'axios'

import Config from 'newui/config'
import i18next from 'newui/i18n'
import { getApiDataFromConfig } from 'newui/utils/helperFunctions'

const defaultParams = {}
const language = i18next.language ? i18next.language : 'sv_SE'

const ApiData = getApiDataFromConfig()

export const fetchBooks = async (bookGroup, rows, type, pagenumber) => {
  return axios({
    method: 'GET',
    url: `${ApiData.apiBooks}booksforbookgroup${bookGroup ? '/' + bookGroup : ''}`,
    headers: {
      locale: language,
      Authorization: Config.fetchAuth,
    },
    params: Object.assign(
      {
        pagenumber: pagenumber || 0,
        rows: rows || 99,
        type: type,
        sort: 'default',
      },
      defaultParams
    ),
  })
}

export const fetchBookInfo = async ({ data: { id } }) => {
  return axios({
    method: 'GET',
    url: `${ApiData.apiBooks}bookinfo`,
    headers: {
      locale: language,
      Authorization: Config.fetchAuth,
    },
    params: Object.assign({ isbn: id }, defaultParams),
  })
}

export const fetchBookGroups = async ({ data: { id, view, bookId, forceId } }) => {
  return axios({
    method: 'GET',
    url: `${ApiData.apiBooks}groups${id ? '/' + id : ''}`,
    headers: {
      locale: language,
      Authorization: Config.fetchAuth,
    },
    params: Object.assign(
      {
        bookid: bookId,
        view: view,
      },
      defaultParams
    ),
  })
}

export const fetchSearch = async ({ data: { query, page, type } }) => {
  return axios({
    method: 'GET',
    url: `${ApiData.apiBooks}searchbooks`,
    headers: {
      locale: language,
      Authorization: Config.fetchAuth,
    },
    params: Object.assign(
      {
        pagenumber: page || 0,
        pagesize: 42,
        query: query,
        sort: 'title',
        type: type || 0,
      },
      defaultParams
    ),
  })
}

export const fetchSearchFacets = async ({ data: { query } }) => {
  return axios({
    method: 'GET',
    url: `${ApiData.apiBooks}searchbooksfacets`,
    headers: {
      locale: language,
      Authorization: Config.fetchAuth,
    },
    params: Object.assign(
      {
        sort: 'title',
        query: query,
      },
      defaultParams
    ),
  })
}
