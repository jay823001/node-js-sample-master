import { fetchBooks, fetchBookInfo, fetchBookGroups, fetchSearch, fetchSearchFacets } from './api'
import { getBookInfo, getBookGroup, getBookSearch } from './actionCreators'
import Reducers from './Reducers'

const BooksModule = {
  api: {
    fetchBooks: fetchBooks,
    fetchBookInfo: fetchBookInfo,
    fetchBookGroups: fetchBookGroups,
    fetchSearch: fetchSearch,
    fetchSearchFacets: fetchSearchFacets,
  },
  actions: {
    getBookInfo: getBookInfo,
    getBookGroup: getBookGroup,
    getBookSearch: getBookSearch,
  },
  reducers: {
    bookInfoReducer: Reducers.bookInfoReducer,
    bookGroupReducer: Reducers.bookGroupReducer,
    bookSearchReducer: Reducers.bookSearchReducer,
  },
}

export default BooksModule
