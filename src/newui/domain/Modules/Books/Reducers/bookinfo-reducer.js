import { BOOKINFO_REQUEST, BOOKINFO_SUCCESS, BOOKINFO_FAILURE } from '../actionTypes'

const initialState = {
  books: [],
  loaded: false,
  related: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case BOOKINFO_REQUEST:
      return { ...state, loaded: false, books: [], related: [] }
    case BOOKINFO_SUCCESS:
      return { ...state, loaded: true, books: action.books, related: action.related }
    case BOOKINFO_FAILURE:
      return { ...state, loaded: false, books: [], related: [] }
    default:
      return state
  }
}
