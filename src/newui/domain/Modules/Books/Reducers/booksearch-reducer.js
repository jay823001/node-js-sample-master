import { BOOKSEARCH_REQUEST, BOOKSEARCH_SUCCESS, BOOKSEARCH_FAILURE } from '../actionTypes'

const initialState = { loaded: false, books: [], groups: [], facets: [], allGroups: [] }

export default (state = initialState, action) => {
  switch (action.type) {
    case BOOKSEARCH_REQUEST:
      return { ...state, loaded: false, books: [], groups: [], allGroups: [], facets: [] }
    case BOOKSEARCH_SUCCESS:
      return {
        ...state,
        loaded: true,
        books: action.books,
        groups: [],
        allGroups: [],
        facets: action.facets,
      }
    case BOOKSEARCH_FAILURE:
      return { ...state, loaded: false, books: [], groups: [], allGroups: [], facets: [] }
    default:
      return state
  }
}
