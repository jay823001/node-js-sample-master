import { BOOKGROUP_REQUEST, BOOKGROUP_SUCCESS, BOOKGROUP_FAILURE } from '../actionTypes'

const initialState = {
  books: [],
  groups: [],
  loaded: false,
  allGroups: [],
  categories: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case BOOKGROUP_REQUEST:
      return { ...state, loaded: false, books: [], groups: [], categories: [], allGroups: [] }
    case BOOKGROUP_SUCCESS:
      return {
        ...state,
        loaded: true,
        books: action.books,
        groups: action.groups,
        allGroups: action.allGroups,
        categories: action.categories,
      }
    case BOOKGROUP_FAILURE:
      return { ...state, loaded: false, books: [], groups: [], categories: [], allGroups: [] }
    default:
      return state
  }
}
