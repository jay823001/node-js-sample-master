import BookInfoReducer from './bookinfo-reducer'
import BookGroupReducer from './bookgroup-reducer'
import BookSearchReducer from './booksearch-reducer'

const Reducers = {
  bookInfoReducer: BookInfoReducer,
  bookGroupReducer: BookGroupReducer,
  bookSearchReducer: BookSearchReducer,
}

export default Reducers
