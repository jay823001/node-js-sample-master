import * as Raven from 'raven-js'
import UserModule from 'newui/domain/Modules/User'

import { store } from 'store'

const refreshAuthRequest = UserModule.actions.refreshAuthRequest
const logoutRequest = UserModule.actions.logoutRequest

const handleError = (error, data, action = null, flow = 'b') => {
  let errMsg = 'lang_error:common.internal_error'

  // Sending error to sentry
  Raven.captureException(error)
  if (typeof error === 'object') {
    if (error.response) {
      if (error.oldflowerror) {
  
        if (error.status === 401 && error.code === 3001) {
          errMsg = 'lang_error:registerForm.invalid_cred'
        }
      } else {
  
        let statusCode = error.response.status
        switch (statusCode) {
          case 401:
            if (error.response.data.error.code === 9000) {
              //errMsg = error.response.data.error.code
              refreshAuth(data, action, flow)
            } else if (error.response.data.error.code === 3001) {
              errMsg = 'lang_error:registerForm.invalid_cred'
            } else {
              errMsg = null
            }
            break
          case 500:
            errMsg = 'lang_error:common.internal_error'
            break
          case 404:
            errMsg = 'lang_error:common.internal_error'
            break
          case 400:
            if (error.response.data.error.code === 6000) {
              errMsg = 'lang_error:registerForm.existing_email'
            } else if (error.response.data.error.code === 4000) {
              errMsg = null
            } else if (error.response.data.error.code === 4001) {
              errMsg = null
            } else if (error.response.data.error.code === 3001) {
              errMsg = 'lang_error:registerForm.email_not_exist'
            } else if (error.response.data.error.code === 3003) {
              errMsg = 'lang_error:resetPassword.invalid_link'
            }
            break
          default:
            errMsg = 'lang_error:common.internal_error'
        }
      }
    } else if (error.request) {
      store.dispatch(logoutRequest())
    } else {
      errMsg = 'lang_error:common.internal_error'
    }
  } else {
    errMsg = 'lang_error:common.internal_error'
  }

  
  return errMsg
}

const refreshAuth = (data, action, flow) => {
  const details = {
    data, action, flow
  }
  store.dispatch(refreshAuthRequest(details))
}

export default handleError