import { REGISTRATION_SUCCESS, AUTH_KEYS_CLEAR } from '../actionTypes'

const initialRegistrationState = {
  authkey: '',
  refreshkey: '',
  isAuthenticated: false
}

const AuthKeyReducer = (state = initialRegistrationState, action) => {
  switch (action.type) {
    case REGISTRATION_SUCCESS:
      return { ...state, authkey: action.registrationData.authkey, refreshkey: action.registrationData.authkey, isAuthenticated: true }
    case AUTH_KEYS_CLEAR:
      return { ...state, authkey: '', refreshkey: '', isAuthenticated: false }
    default:
      return state
  }
}

export default AuthKeyReducer