import { MEMBER_LOGIN_SUCCESS, LOGIN_NEW_REQUEST, LOGIN_NEW_SUCCESS, LOGIN_NEW_FAILURE } from '../actionTypes'

const initialMemberLoginState = {
  userDetails: '',
  fetching : false,
  error: null
}

const LoginReducer = (state = initialMemberLoginState, action) => {
  switch (action.type) {
    case MEMBER_LOGIN_SUCCESS:
      return action.userDetails
    case LOGIN_NEW_REQUEST:
      return {...state, fetching: true, error: null}
    case LOGIN_NEW_SUCCESS:
      return {...state, fetching: false, error: null}
    case LOGIN_NEW_FAILURE:
      return {...state, fetching: false, error: action.err}
    default:
      return state
  }
}

export default LoginReducer