import {
  REGISTRATION_SUCCESS,
  REGISTRATION_REQUEST,
  REGISTRATION_FAILURE
} from '../actionTypes'

const initialRegistrationState = {
  fetching: false,
  registerError: null,
  // registrationKeys: {
  //   authkey: '',
  //   refreshkey: '',
  // }
}

const RegistrationReducer = (state = initialRegistrationState, action) => {
  switch (action.type) {
    case REGISTRATION_REQUEST:
      return { ...state, fetching: true, registerError: null }
    case REGISTRATION_SUCCESS:
      return { ...state, fetching: false,  }
    case REGISTRATION_FAILURE:
      return { ...state, fetching: false, registerError: action.err }
    default:
      return state
  }
}

export default RegistrationReducer
