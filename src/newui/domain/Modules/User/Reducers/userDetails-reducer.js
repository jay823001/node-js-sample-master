import {
  SET_USERDETAILS_REQUEST,
  REMOVE_USERDETAILS_REQUEST
} from '../actionTypes'

const initialDetailsState = {
  userInfo: [],
  isLoggedIn: false
}

const UserDetailsReducer = (state = initialDetailsState, action) => {
  switch (action.type) {
    case SET_USERDETAILS_REQUEST:
      return { ...state, userInfo: action.user, isLoggedIn: true }
    case REMOVE_USERDETAILS_REQUEST:
      return { ...state, userInfo: [], isLoggedIn: false }
    default:
      return state
  }
}

export default UserDetailsReducer