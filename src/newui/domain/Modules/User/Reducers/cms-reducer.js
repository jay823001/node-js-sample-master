import { CMS_CONTENT_REQUEST, CMS_CONTENT_SUCCESS, CMS_CONTENT_FAILURE } from '../actionTypes'

const initialcmsState = {
  content: [],
  fetching : true,
  error: null,
  meta: []
}

const cmsReducer = (state = initialcmsState, action) => {
  switch (action.type) {
    case CMS_CONTENT_REQUEST:
      return {...state, fetching: true, error: null, content: [], meta: []}
    case CMS_CONTENT_SUCCESS:
      return {...state, fetching: false, error: null, content: action.content, meta: action.meta}
    case CMS_CONTENT_FAILURE:
      return {...state, fetching: false, error: action.err, content: [], meta: []}
    default:
      return state
  }
}

export default cmsReducer