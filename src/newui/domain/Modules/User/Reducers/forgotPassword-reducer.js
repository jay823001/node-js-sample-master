import {
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE
} from '../actionTypes'

const initialState = {
  fetching: false,
  error: null
}

const ForgotPasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case FORGOT_PASSWORD_REQUEST:
      return { ...state, fetching: true, error: null }
    case FORGOT_PASSWORD_SUCCESS:
      return { ...state, fetching: false, error: null }
    case FORGOT_PASSWORD_FAILURE:
      return { ...state, fetching: false, error: action.err }
    case RESET_PASSWORD_REQUEST:
      return { ...state, fetching: true, error: null }
    case RESET_PASSWORD_SUCCESS:
      return { ...state, fetching: false, error: null }
    case RESET_PASSWORD_FAILURE:
      return { ...state, fetching: false, error: action.err }
    default:
      return state
  }
}

export default ForgotPasswordReducer