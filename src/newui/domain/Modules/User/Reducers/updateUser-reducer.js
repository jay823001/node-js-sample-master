import {
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE
} from '../actionTypes'

const initialDetailsState = {
  fetching: false,
  userInfo: null,
  userError: null
}

const UpdateUserReducer = (state = initialDetailsState, action) => {
  switch (action.type) {
    case UPDATE_USER_REQUEST:
      return { ...state, fetching: true, userError: null }
    case UPDATE_USER_SUCCESS:
      return { ...state, fetching: false, userIfo: action.userData }
    case UPDATE_USER_FAILURE:
      return { ...state, fetching: false, userIfo: null, userError: action.err }
    default:
      return state
  }
}

export default UpdateUserReducer