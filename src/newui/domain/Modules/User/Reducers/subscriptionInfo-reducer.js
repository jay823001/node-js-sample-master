import {
  SUBSCRIPTION_DETAILS_REQUEST,
  SUBSCRIPTION_DETAILS_FAILURE,
  SUBSCRIPTION_DETAILS_SUCCESS,
  UPDATE_SUBSCRIPTION_SUCCESS,
  UPDATE_SUBSCRIPTION_REQUEST,
  UPDATE_SUBSCRIPTION_FAILURE
} from '../actionTypes'

const initialDetailsState = {
  fetching: false,
  subscriptionInfo: null,
  error: null,
  updateInfo: null
}

const SubscriptionInfoReducer = (state = initialDetailsState, action) => {
  switch (action.type) {
    case SUBSCRIPTION_DETAILS_REQUEST:
      return { ...state, fetching: true, error: null, updateInfo: null }
    case SUBSCRIPTION_DETAILS_SUCCESS:
      return { ...state, fetching: false, subscriptionInfo: action.subscriptionDdata, updateInfo: null }
    case SUBSCRIPTION_DETAILS_FAILURE:
      return { ...state, fetching: false, subscriptionInfo: null, error: action.err, updateInfo: null }
    case UPDATE_SUBSCRIPTION_SUCCESS:
      return { ...state, fetching: false, subscriptionInfo: null, error: null, updateInfo: action.subscription }
    case UPDATE_SUBSCRIPTION_REQUEST:
      return { ...state, fetching: true, subscriptionInfo: null, error: null, updateInfo: null }
    case UPDATE_SUBSCRIPTION_FAILURE:
      return { ...state, fetching: false, subscriptionInfo: null, error: action.err, updateInfo: null }
    default:
      return state
  }
}

export default SubscriptionInfoReducer