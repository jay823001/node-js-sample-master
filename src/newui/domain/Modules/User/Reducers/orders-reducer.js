import {
  ORDER_DETAILS_REQUEST,
  ORDER_DETAILS_SUCCESS,
  ORDER_INFO_SUCCESS,
  ORDER_DETAILS_FAILURE,
  ORDER_INFO_REQUEST
} from '../actionTypes'

const initialDetailsState = {
  fetching: false,
  allorders: [],
  orderDetails: null,
  requestError: null
}

const OrdersReducer = (state = initialDetailsState, action) => {
  
  switch (action.type) {
    case ORDER_DETAILS_REQUEST:
      return { ...state, fetching: true, allorders: [], orderDetails: null, requestError: null }
    case ORDER_INFO_REQUEST:
      return { ...state, fetching: true, allorders: [], orderDetails: null, requestError: null }
    case ORDER_DETAILS_SUCCESS:
      return { ...state, fetching: false, allorders: action.orders, orderDetails: null, requestError: null }
    case ORDER_INFO_SUCCESS:
      return { ...state, fetching: false, allorders: [], orderDetails: action.receipt, requestError: null }
    case ORDER_DETAILS_FAILURE:
      return { ...state, fetching: false, allorders: [], orderDetails: null, requestError: action.err }
    default:
      return state
  }
}

export default OrdersReducer