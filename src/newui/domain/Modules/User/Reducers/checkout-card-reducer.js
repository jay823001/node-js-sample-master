import {
  PAYMENT_REQUEST,
  PAYMENT_SUCCESS,
  PAYMENT_FAILURE
} from '../actionTypes'

const initialDetailsState = {
  fetching: false,
  paymentInfo: null,
  paymentError: null
}

const CheckoutCardReducer = (state = initialDetailsState, action) => {
  switch (action.type) {
    case PAYMENT_REQUEST:
      return { ...state, fetching: true, paymentError: null }
    case PAYMENT_SUCCESS:
      return { ...state, fetching: false, paymentInfo: action.userData }
    case PAYMENT_FAILURE:
      return { ...state, fetching: false, paymentInfo: null, paymentError: action.err }
    default:
      return state
  }
}

export default CheckoutCardReducer