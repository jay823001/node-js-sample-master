import {
  OFFER_DETAILS_REQUEST,
  OFFER_DETAILS_SUCCESS,
  OFFER_DETAILS_FAILURE
} from '../actionTypes'

const initialDetailsState = {
  fetching: true,
  alloffers: [],
  requestError: null
}

const OffersReducer = (state = initialDetailsState, action) => {
  
  switch (action.type) {
    case OFFER_DETAILS_REQUEST:
      return { ...state, fetching: true, alloffers: [], requestError: null }
    case OFFER_DETAILS_SUCCESS:
      return { ...state, fetching: false, alloffers: action.offers, requestError: null }
    case OFFER_DETAILS_FAILURE:
      return { ...state, fetching: false, alloffers: [], requestError: action.err }
    default:
      return state
  }
}

export default OffersReducer