import { SELECT_SUBSCRIPTION } from '../actionTypes'

const initialSubscriptionState = {
  index: '',
  isfamily: ''
}

const SelectSubscriptionReducer = (state = initialSubscriptionState, action) => {
  switch (action.type) {
    case SELECT_SUBSCRIPTION:
      return action.payload
    default:
      return state
  }
}

export default SelectSubscriptionReducer
