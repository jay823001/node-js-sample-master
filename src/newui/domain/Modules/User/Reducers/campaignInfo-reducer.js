import { CAMPAIGN_SELECTED } from '../actionTypes'

const initialCampaignState = ''

const CampaignInfoReducer = (state = initialCampaignState, action) => {
  switch (action.type) {
    case CAMPAIGN_SELECTED:
      return action.payload
    default:
      return state
  }
}

export default CampaignInfoReducer
