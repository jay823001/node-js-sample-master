import {
  REFRESHAUTH_SUCCESS
} from '../actionTypes'

const initialRefreshState = {
  actionCall: null,
  actionData: null,
}

const RefreshAuthReducer = (state = initialRefreshState, action) => {
  switch (action.type) {
    case REFRESHAUTH_SUCCESS:
      return action.registrationData
    default:
      return state
  }
}

export default RefreshAuthReducer
