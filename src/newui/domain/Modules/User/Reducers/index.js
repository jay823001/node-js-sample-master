import AuthKeysReducer from './authKeys.reducer'
import CheckoutCardReducer from './checkout-card-reducer'
import CheckoutTrustlyReducer from './checkout-trustly-reducer'
import LoginReducer from './login-reducer'
import RegistrationReducer from './registration-reducer'
import SubscriptionInfoReducer from './subscriptionInfo-reducer'
import UpdateUserReducer from './updateUser-reducer'
import RefreshAuthReducer from './refreshAuth-reducer'
import LogoutReducer from './logout-reducer'
import CampaignInfoReducer from './campaignInfo-reducer'
import CampaignDetailsReducer from './campaignDetails-reducer'
import OrdersReducer from './orders-reducer'
import OffersReducer from './offers-reducer'
import UserDetailsReducer from './userDetails-reducer'
import ForgotPasswordReducer from './forgotPassword-reducer'
import SelectSubscriptionReducer from './selectSubscription-reducer'
import cmsReducer from './cms-reducer'

const Reducers = {
  authKeysReducer: AuthKeysReducer,
  checkoutCardReducer: CheckoutCardReducer,
  checkoutTrustlyReducer: CheckoutTrustlyReducer,
  loginReducer: LoginReducer,
  registrationReducer: RegistrationReducer,
  subscriptionInfoReducer: SubscriptionInfoReducer,
  updateUserReducer: UpdateUserReducer,
  refreshAuthReducer: RefreshAuthReducer,
  logoutReducer: LogoutReducer,
  campaignInfoReducer: CampaignInfoReducer,
  campaignDetailsReducer: CampaignDetailsReducer,
  ordersReducer: OrdersReducer,
  offersReducer: OffersReducer,
  userDetailsReducer: UserDetailsReducer,
  ForgotPasswordReducer: ForgotPasswordReducer,
  selectSubscriptionReducer: SelectSubscriptionReducer,
  cmsReducer: cmsReducer
}

export default Reducers