import {
    CAMPAIGN_DETAILS_SUCCESS,
    CAMPAIGN_DETAILS_FAILURE,
    CAMPAIGN_DETAILS_REQUEST
  } from '../actionTypes'
  import { LOCATION_CHANGE } from 'react-router-redux'
  
  const initialDetailsState = {
    fetching: false,
    campaignInfo: null,
    error: null
  }
  
  const CampaignDetailsReducer = (state = initialDetailsState, action) => {
    switch (action.type) {
      case CAMPAIGN_DETAILS_REQUEST:
        return { ...state, fetching: true, error: null }
      case CAMPAIGN_DETAILS_SUCCESS:
        return { ...state, fetching: false, campaignInfo: action.campaignData }
      case CAMPAIGN_DETAILS_FAILURE:
        return { ...state, fetching: false, campaignInfo: null, error: action.err }
      case LOCATION_CHANGE:
        return { ...state, fetching: false, campaignInfo: null, error: null }
      default:
        return state
    }
  }
  
  export default CampaignDetailsReducer