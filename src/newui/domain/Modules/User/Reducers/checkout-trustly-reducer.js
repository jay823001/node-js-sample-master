import { TRUSTLY_PAYMENT_SUCCESS } from '../actionTypes'

const initialTrustlyPaymentState = ''

const CheckoutTrustlyReducer = (state = initialTrustlyPaymentState, action) => {
  switch (action.type) {
    case TRUSTLY_PAYMENT_SUCCESS:
      return action.url
    default:
      return state
  }
}

export default CheckoutTrustlyReducer