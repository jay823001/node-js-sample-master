import qs from 'qs'
import axios from 'axios'
import Config from 'newui/config'
import { apiUrl } from 'containers/App/api'
import { getApiDataFromConfig } from 'newui/utils/helperFunctions'
import i18next from 'newui/i18n'

const ApiData = getApiDataFromConfig()
const fetchAuth = Config.fetchAuth
const language = i18next.language ? i18next.language : 'sv_SE'

/*
**@event: Get subscription details when going to payment method
**@return description: paymentInfo and subscription details
**@return type: object response
**@parameter description: campaign_code?optional, giftcard_code?optional, subscriptionID
**@parameter type: String, String, Number
*/
export const fetchSubscriptionDetails = async (subscriptionDetails, tokens) => {
  let params = null
  if (subscriptionDetails.subscriptionDetails.flow === 'campaign') {
    params = `subscriptionid=${subscriptionDetails.subscriptionDetails.subscriptionid}&ccode=${subscriptionDetails.subscriptionDetails.ccode}`
  } else if (subscriptionDetails.subscriptionDetails.flow === 'change-payment' || subscriptionDetails.subscriptionDetails.flow === 're-activate') {
    params = `subscriptionid=${subscriptionDetails.subscriptionDetails.subscriptionid}&caccount=true`
  } else {
    params = `subscriptionid=${subscriptionDetails.subscriptionDetails.subscriptionid}`
  }
  return axios({
    method: 'GET',
    url: `${ApiData.apiUrl}user/register/checkout?${params}`,
    headers: {
      'locale': language,
      'nx-at': tokens.authkey,
      Authorization: fetchAuth,
    },
  })
}

/*
**@event: Register user
**@return description: success or failure
**@return type: object response
**@parameter description: userDetails
**@parameter type: Object
*/
export const postUserRegister = async userDetails => {
  const details = qs.stringify(userDetails)

  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}user/register`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
      'locale': language,
      Authorization: fetchAuth,
    },
    data: details
  })
}

/*
**@event: logout user session
**@return description: empty string
**@return type: object response
**@parameter description: authkey,
**@parameter type: String
*/
export const logoutUser = async authkey => {
  return axios({
    url: `${ApiData.apiUrl}user/logout?locale=${language}`,
    headers: {
      method: 'GET',
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': authkey,
    },
    credentials: 'same-origin',
  })
}

/*
**@event: update user details 
**@return description: user details
**@return type: object response
**@parameter description: firstname, lastname, cellphone, auth, password, email, address, postcode, city, newsletter,
**@parameter type: all Strings
*/
export const UpdateUserDetails = async (data, tokens) => {
  const formData = new FormData()
  // formData.append('firstname', data.firstname)
  // formData.append('lastname', data.lastname)
  // formData.append('address', data.address)
  // formData.append('city', data.city)
  // formData.append('email', data.email)
  // formData.append('postcode', data.postcode)
  // formData.append('cellphone', data.cellphone)
  // formData.append('password', data.password)
  // formData.append('authkey', data.authkey.authkey)

  for (const key of Object.keys(data)) {
    formData.append(key, data[key])
  }

  return axios({
    method: 'POST',
    url: `${apiUrl}user/updateprofile?newsletter=true&locale=${language}`,
    headers: {
      Authorization: fetchAuth,
      Accept: 'application/json',
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      'nx-at': tokens.authkey,
      'locale': language
    },
    credentials: 'same-origin',
    data: formData
  })
}

/*
**@event: Set payment options and do payment
**@return description: success / failure
**@return type: object response
**@parameter description: payment details
**@parameter type: Object
*/
export const postPaymentDetails = async (paymentDetails, tokens) => {
  const formData = new FormData()
  formData.append('subscriptionid', paymentDetails.data.subscriptionid)
  formData.append('encryptedCardNumber', paymentDetails.data.encryptedCardNumber)
  formData.append('encryptedExpiryMonth', paymentDetails.data.encryptedExpiryMonth)
  formData.append('encryptedExpiryYear', paymentDetails.data.encryptedExpiryYear)
  formData.append('encryptedSecurityCode', paymentDetails.data.encryptedSecurityCode)
  formData.append('paymentgateway', paymentDetails.data.paymentgateway)

  if (paymentDetails.data.flow === 'campaign') {
    formData.append('ccode', paymentDetails.data.campaign.code)
  } else if (paymentDetails.data.flow === 'change-payment') {
    formData.append('caccount', true)
  }

  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}user/register/checkout`,
    headers: {
      'locale': language,
      'nx-at': tokens.authkey,
      Authorization: fetchAuth,
    },
    data: formData
  })
}

/*
**@event: Set payment options and do payment
**@return description: success / failure
**@return type: object response
**@parameter description: payment response from 3rd party payment sites
**@parameter type: Object
*/
export const postPaymentResponse = async (paymentDetails, tokens) => {
  const formData = new FormData()
  formData.append('payload', paymentDetails.payload)
  formData.append('type', paymentDetails.type)
  formData.append('resultCode', paymentDetails.resultCode)
  formData.append('provider', paymentDetails.provider)
  formData.append('orderref', paymentDetails.orderref)

  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}user/paymentresponse`,
    headers: {
      'locale': language,
      'nx-at': tokens.authkey,
      Authorization: fetchAuth,
    },
    data: formData
  })
}

/**LOGIN REQUEST --- THIS WAS COPIED FROM OLD API since existance of login api in v5.5 was not yet communicated to us yet*/
/*
**@event: Login user
**@return description: success / failure
**@return type: object response
**@parameter description: Credentials
**@parameter type: Object
*/
export const fetchLoginUser = async (data, tokens) => {

  const formData = new FormData()
  if (!data.data.authkey) {
    formData.append('email', data.data.email)
    formData.append('password', data.data.password)
  }
  formData.append('serverdetails', '127.0.0.1')

  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}user/login?locale=${language}`,
    headers: {
      'nx-at': tokens.authkey ? tokens.authkey : null,
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin',
    data: formData
  })
}

/*
**@event: refresh auth token 
**@return description: new auth token
**@return type: object response
**@parameter description: nonel
**@parameter type: none
*/
export const refreshAuthTokens = async (data) => {

  // because after registration have to use current api to refresh token as backend team mentioned
  let apiURL = ApiData.apiUrl
  if (data.flow === 'a') {
    apiURL = apiUrl
  } else {
    apiURL = ApiData.apiUrl
  }

  return axios({
    method: 'GET',
    url: `${apiURL}user/token?locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': data.authkey,
      'nx-rt': data.refreshkey,
      locale: language
    },
    credentials: 'same-origin',
  })
}

/*
**@event: validate the campaign code user entered 
**@return description: validation of the given campaign code
**@return type: object
**@parameter description: campaign code, authkey
**@parameter type: string, object
*/
export const registerCampaignCode = async (campaigncode, authkey) => {

  const formData = new FormData()
  formData.append('ccode', campaigncode)

  return axios({
    method: 'GET',
    url: `${ApiData.apiUrl}user/register/validate?ccode=${campaigncode}`,
    headers: {
      'Content-Type': 'multipart/form-data',
      'locale': language,
      Authorization: fetchAuth,
      'nx-at': authkey
    },
    // data: formData
  })
}

/*
**@event: get campaign details when slug provided 
**@return description: campaign details
**@return type: object
**@parameter description: slug
**@parameter type: string, object
*/
export const campaignDetails = async (slug) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}user/campaigndetails?campaignslug=${slug}&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    }
  })
}

/*
**@event: cancel current subscription
**@return description: cancel status
**@return type: object
**@parameter description: reason for cancelling
**@parameter type: string, object
*/
export const cancelSubscription = async (data, authkey) => {
  return axios({
    method: 'POST',
    url: `${apiUrl}cancelmembership?reason=${data.reason}&comment=${data.comment}&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': authkey
    }
  })
}

/*
**@event: get my orders
**@return description: order list
**@return type: object
**@parameter description: authkey
**@parameter type: String
*/
export const getmyOrders = async (authkey) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}orders?locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': authkey
    },
    credentials: 'same-origin'
  })
}

/*
**@event: get receipt details
**@return description: receipt details
**@return type: object
**@parameter description: orderId, authkey
**@parameter type: String, String
*/
export const getReceiptDetails = async (orderId, authkey) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}orderdetails?orderno=${orderId}&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': authkey
    }
  })
}

/*
**@event: get all offers
**@return description: offers related to loggedin user
**@return type: object
**@parameter description: authkey
**@parameter type: String
*/
export const getMyOffers = async (authkey) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}mycollabs?locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': authkey
    },
    credentials: 'same-origin'
  })
}

/*
**@event: update current subscription
**@return description: update status
**@return type: object
**@parameter description: subscription id, authkey
**@parameter type: String, String
*/
export const updateSubscription = async (subscriptionId, authkey) => {
  return axios({
    method: 'POST',
    url: `${apiUrl}subscription/update?newsubscriptionid=${subscriptionId}&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth,
      'nx-at': authkey
    },
    credentials: 'same-origin'
  })
}

export const fetchLogin = async (data) => {
  const formData = new FormData()
  formData.append('email', data.email)
  formData.append('password', data.password)
  formData.append('serverdetails', '127.0.0.1')

  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}login?locale=${language}`,
    headers: {
      'nx-at': null,
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin',
    data: formData
  })
}

export const fetchUserDetails = async (authKey) => {
  return axios({
    method: 'GET',
    url: `${ApiData.apiUrl}userdetails?locale=${language}`,
    headers: {
      'nx-at': authKey,
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin'
  })
}

export const forgotPassword = async (data) => {
  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}forgotpassword?email=${data.email}&locale=${language}`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin',
    body: JSON.stringify(data)
  })
}

export const resetPassword = async (data) => {
  return axios({
    method: 'POST',
    url: `${ApiData.apiUrl}changepassword?userid=${data.id}&hash=${data.authkey}&newpassword=${
      data.password
      }&processType=PROCESS_FORGOT_PASSWORD&locale=${language}`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin',
    body: JSON.stringify(data)
  })
}

export const getCMScontent = async (contentName) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}cms-page?pageUrl=${contentName}&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin'
  })
}

export const getCMSmeta = async (contentName) => {
  return axios({
    method: 'GET',
    url: `${apiUrl}page-meta?pageUrl=${contentName}/&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: fetchAuth
    },
    credentials: 'same-origin'
  })
}

export const activateAgain = async (user_details, authkeys) => {
  return axios({
    method: 'POST',
    url: `${apiUrl}usersignup?email=${user_details.userInfo.email}&confirmemail=${user_details.userInfo.email}
    &password=${user_details.userInfo.password}&subscriptionid=${user_details.userInfo.subscriptiondetails.subscriptionid}
    &serverdetails=127.0.0.1&signuptype=PURCHASE_SUBSCRIPTION&locale=${language}`,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      Authorization: fetchAuth,
      'nx-at': authkeys.authkey,
    },
    credentials: 'same-origin',
    body: JSON.stringify(user_details)
  })
}
