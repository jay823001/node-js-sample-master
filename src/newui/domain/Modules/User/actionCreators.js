import {
  PAYMENT_REQUEST,
  REGISTRATION_REQUEST,
  UPDATE_USER_REQUEST,
  SELECT_SUBSCRIPTION,
  SUBSCRIPTION_DETAILS_REQUEST,
  REFRESHAUTH_REQUEST,
  LOGOUT_REQUEST,
  CAMPAIGN_DETAILS_REQUEST,
  CANCEL_SUBSCRIPTION_REQUEST,
  ORDER_DETAILS_REQUEST,
  ORDER_INFO_REQUEST,
  OFFER_DETAILS_REQUEST,
  UPDATE_SUBSCRIPTION_REQUEST,
  LOGIN_NEW_REQUEST,
  FORGOT_PASSWORD_REQUEST,
  PAYMENTRESPONSE_SUCCESS,
  RESET_PASSWORD_REQUEST,
  CMS_CONTENT_REQUEST,
  USER_DEATILS_REQUEST,
  ACTIVATE_AGAIN_REQUEST
} from './actionTypes'

export const createPaymentRequest = data => ({
  type: PAYMENT_REQUEST,
  data
})

export const createPaymentResponseRequest = data => ({
  type: PAYMENTRESPONSE_SUCCESS,
  data
})

export const userRegisterRequest = data => ({
  type: REGISTRATION_REQUEST,
  payload: data
})

export const userUpdateRequest = data => ({
  type: UPDATE_USER_REQUEST,
  payload: data
})

export const selectSubscription = selectedPackage => {
  return {
    type: SELECT_SUBSCRIPTION,
    payload: selectedPackage
  }
}

export const subscriptionDetailsRequest = subscriptionDetails => ({
  type: SUBSCRIPTION_DETAILS_REQUEST,
  subscriptionDetails
})

export const refreshAuthRequest = data => ({
  type: REFRESHAUTH_REQUEST,
  data
})

export const logoutRequest = (isVisitor) => ({
  type: LOGOUT_REQUEST
})

export const campaignDetailRequest = data => ({
  type: CAMPAIGN_DETAILS_REQUEST,
  payload: data
})

export const cancelSubscriptionRequest = data => ({
  type: CANCEL_SUBSCRIPTION_REQUEST,
  payload: data
})

export const orderDetailsRequest = () => ({
  type: ORDER_DETAILS_REQUEST
})

export const receiptDetailsRequest = (data) => ({
  type: ORDER_INFO_REQUEST,
  payload: data
})

export const offerDetailsRequest = () => ({
  type: OFFER_DETAILS_REQUEST
})

export const updateSubscriptionRequest = (data) => ({
  type: UPDATE_SUBSCRIPTION_REQUEST,
  payload: data
})

export const loginRequest = (data) => ({
  type: LOGIN_NEW_REQUEST,
  payload: data
})

export const forgotPasswordRequest = (data) => ({
  type: FORGOT_PASSWORD_REQUEST,
  payload: data
})

export const resetPasswordRequest = (data) => ({
  type: RESET_PASSWORD_REQUEST,
  payload: data
})

export const cmsContentRequest = (data) => ({
  type: CMS_CONTENT_REQUEST,
  payload: data
})

export const userDetailsRequest = () => ({
  type: USER_DEATILS_REQUEST
})

export const activateAgainRequest = () => ({
  type: ACTIVATE_AGAIN_REQUEST
})

