import _ from 'lodash'
import React from 'react'
import { withNamespaces } from 'react-i18next'

import BookGrid from 'newui/components/BookGrid'
import BookGroup from 'newui/components/BookGroup'
import Pagination from 'newui/components/Pagination'
import SearchForm from 'newui/components/SearchForm'
import BookListFilter from 'newui/components/BookListFilter'
import BookListHeader from 'newui/components/BookListHeader'
import StickyButton from 'newui/components/StickyButton'
import { Link } from 'react-router-dom'

class BookList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showMenu: false,
      showFilter: false,
    }

    this.closeAll = this.closeAll.bind(this)
    this.toggleMenu = this.toggleMenu.bind(this)
    this.toggleFilter = this.toggleFilter.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.setState({
        showMenu: false,
        showFilter: false,
      })
    }
  }

  toggleMenu() {
    this.setState({ showMenu: ~this.state.showMenu, showFilter: false })
  }

  toggleFilter() {
    this.setState({ showFilter: ~this.state.showFilter, showMenu: false })
  }

  closeAll() {
    this.setState({
      showMenu: false,
      showFilter: false,
    })
  }

  checkAuth() {
    if (this.props.user && !this.props.user.isLoggedIn) {
      return true
    } else if (this.props.location.state && this.props.location.state.user && !this.props.location.state.user.isLoggedIn) {
      return true
    } else {
      return false
    }
  }

  setActive(type) {
    if (this.props.type === 0 || this.props.type === 1 || this.props.type === 2) {
      if (this.props.type === 1 && type === 1) {
        return true
      } else if (this.props.type === 2 && type === 2) {
        return true
      } else if (this.props.type === 0) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  render() {
    const { type, books, groups, allGroups, loaded, heading, categories, hideMenu } = this.props
    const query = new URLSearchParams(this.props.location.search).get('query')

    return (
      <span>
        <div className="bookList">
          <div className="bookList__header">
            <BookListHeader
              heading={heading ? heading : books[0].title}
              hideMenu={hideMenu}
              toggleMenu={this.toggleMenu}
              toggleFilter={this.toggleFilter}
            />
          </div>

          {this.state.showMenu || this.state.showFilter ? (
            <div className="bookList__filter">
              {this.state.showMenu ? (
                <BookListFilter
                  {...this.props}
                  type={type}
                  heading={this.props.t('bookListHeader.category')}
                  options={categories}
                  closeAll={this.closeAll}
                  closeBtn={this.props.t('bookSearch.closeFilterBtn')}
                />
              ) : null}
              {this.state.showFilter ? (
                <BookListFilter
                  {...this.props}
                  type={type}
                  heading={this.props.t('bookListHeader.format')}
                  options={[
                    {
                      title: this.props.t('bookList.heading.ebook'),
                      forceType: 1,
                      slugname: this.props.match.params.slug,
                      active: this.setActive(1)
                    },
                    {
                      title: this.props.t('bookList.heading.audio'),
                      forceType: 2,
                      slugname: this.props.match.params.slug,
                      active: this.setActive(2)
                    },
                  ]}
                  closeAll={this.closeAll}
                  closeBtn={this.props.t('bookSearch.closeFilterBtn')}
                />
              ) : null}
            </div>
          ) : null}
          {this.props.location.pathname.startsWith(
            this.props.t('lang_route:books.search')
          ) && (
              <React.Fragment>
                {this.state.showFilter ? (
                  <div className="bookList__filter">
                    <BookListFilter
                      {...this.props}
                      heading={this.props.t('bookListHeader.format')}
                      options={[
                        {
                          title: this.props.t('bookList.heading.ebook'),
                          active: this.setActive(1)
                        },
                        {
                          title: this.props.t('bookList.heading.audio'),
                          active: this.setActive(2)
                        },
                      ]}
                      closeAll={this.closeAll}
                      closeBtn={this.props.t('bookSearch.closeFilterBtn')}
                    />
                  </div>
                ) : (
                    <div className="bookList__search">
                      <SearchForm {...this.props} query={query} toggleFilter={this.toggleFilter} />
                    </div>
                  )}
              </React.Fragment>
            )}
          {!this.state.toggleFormat &&
            !this.state.toggleCategory &&
            (groups && allGroups.length <= 1 && loaded ? (
              !_.isEmpty(books[0].data) ? (
                <React.Fragment>
                  <div className="bookList__grid">
                    {books[0].data.map((book, key) => (
                      <div className="bookList__item" key={key}>
                        <BookGrid book={book} />
                      </div>
                    ))}
                  </div>
                  <div className="bookList__pagination">
                    <Pagination
                      search={this.props.location.search}
                      total={Math.ceil(books[0].count / 42)}
                      page={books[0].page}
                    />
                  </div>
                </React.Fragment>
              ) : null
            ) : (
                <React.Fragment>
                  {groups &&
                    groups.map((item, key) => {
                      const bookList = books.find(book => book.id === item.id).data

                      if (bookList.length > 0) {
                        return (
                          <div className="bookList__group" key={key}>
                            <BookGroup
                              heading={item.title}
                              books={bookList}
                              type={type}
                              link={item.slugname}
                            />
                          </div>
                        )
                      }
                      return null
                    })}
                  {books && groups && allGroups.length >= 1 ? (
                    <div className="bookList__pagination">
                      <Pagination
                        search={this.props.location.search}
                        total={Math.ceil(allGroups.length / 5)}
                        page={books[0].page}
                      />
                    </div>
                  ) : null}
                </React.Fragment>
              ))}
          {books[0] && _.isEmpty(books[0].data) && query && loaded ? (
            <div className="bookList__noResult">
              <p>{this.props.t('bookSearch.noResult')}</p>
            </div>
          ) : null}
          {query && !loaded ? <div className="loader loader--local" /> : null}
        </div>
        {this.checkAuth() &&
          <StickyButton stickyPhone={true} stickyDesktop={true}>
            <Link className="btn btn--primary" to={this.props.t('lang_route:registration.trialFlow.subscriptionMessage')}>{this.props.t('lang_common:cta')}</Link>
          </StickyButton>
        }
      </span>
    )
  }
}

export default withNamespaces(['lang_books'])(BookList)
