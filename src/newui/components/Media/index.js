import React from 'react'

import { imageHelper } from 'newui/assets/mixins/ImageHelper'

class Media extends React.Component {
  imageRatio() {
    const { width = 100, height = 100 } = this.props
    return Math.round(parseInt(height) * 100 / parseInt(width) * 100) / 100
  }

  render() {
    const { src, alt, external = false } = this.props

    return (
      <div className='media'>
        <img className='media__src' src={imageHelper(src, external)} alt={alt} />
        <div className='media__size' style={{ paddingTop: `${this.imageRatio()}%` }} />
      </div>
    )
  }
}

export default Media
