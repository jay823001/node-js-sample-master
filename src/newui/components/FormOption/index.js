import React from 'react'
import IconsInfo from 'newui/components/Icons/Info'

export default class FormOptions extends React.PureComponent {

  /*
  **@event: Show/hide credit card hint
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */
  showHint() {
    this.props.onClick()
  }

  render() {
    return (
      <div className='formOption'>
        <div className='formOption__legend'>
          <div className='formOption__label'> {this.props.label} </div>
          {this.props.instructions && (
            <div
              className='formOption__icon'
              id={this.props.instructions}
              onClick={this.showHint.bind(this)}
            >
              <IconsInfo />
            </div>
          )}
        </div>
        <div className='formOption__main'>{this.props.children}</div>
        <div className='formOption__message'>{this.props.message}</div>
      </div>
    )
  }
}
