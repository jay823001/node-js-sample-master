import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

class Pagination extends React.Component {
  render() {
    const { page = 0, total = 0, search } = this.props
    const query = search ? search.replace(/(\?|&)page=\d+/, '') : ''

    return (
      <div className="pagination">
        <p className="pagination__label">
          {this.props.t('bookList.pagination', {
            current: Math.max(page, 1),
            total: Math.max(total, 1),
          })}
        </p>
        <div className="pagination__list">
          <div className="pagination__item">
            <Link
              className={`btn btn--secondary btn--tight ${page <= 1 ? 'btn--disabled' : ''}`}
              style={{ pointerEvents: page <= 1 && 'none' }}
              to={`${query ? '?' + query.slice(1) + '&' : '?'}page=${Math.max(page - 1, 1)}`}
            >
              {this.props.t('bookList.button.prev')}
            </Link>
          </div>
          <div className="pagination__item">
            <Link
              className={`btn btn--secondary btn--tight ${page >= total ? 'btn--disabled' : ''}`}
              style={{ pointerEvents: page >= total && 'none' }}
              to={`${query ? '?' + query.slice(1) + '&' : '?'}page=${Math.min(page + 1, total)}`}
            >
              {this.props.t('bookList.button.next')}
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(Pagination)
