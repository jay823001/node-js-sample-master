import React from 'react'
import Checkmark from 'newui/components/Icons/Checkmark'

class BulletList extends React.PureComponent {

  render() {
    return (
      <div className='bulletList'>
        <div className='bulletList__main'>
          {this.props.items.map((item, index) =>
            <div className='bulletList__item' key={index}>
              <div className='bulletList__icon'>
                <Checkmark />
              </div>
              <div className='bulletList__label'>
                {item}
              </div>
            </div>
          )}

        </div>
      </div>
    )
  }
}

export default BulletList
