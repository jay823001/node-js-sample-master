import _ from 'lodash'
import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

import IconsRank from 'newui/components/Icons/Rank'
import IconsEbook from 'newui/components/Icons/Ebook'
import IconsAudioBook from 'newui/components/Icons/AudioBook'

class BookCard extends React.Component {
  convertURL(title, authors, isbn) {
    return _.kebabCase(_.deburr(`${title}-${authors}-${isbn}`))
  }

  render() {
    const {
      book: { isbn, type, title, authors, avgrate, imageurl, relatedbookid },
    } = this.props

    return (
      <Link
        className="bookCard"
        to={`${this.props.t('books.book')}/${this.convertURL(title, authors, isbn)}`}
      >
        <div className="bookCard__row">
          <div className="bookCard__info">
            <div className="bookCard__cover">
              <div className="bookCover bookCover--fixedHeight">
                <img
                  className="bookCover__image bookCover__image--fixedHeight"
                  itemProp="photo"
                  src={imageurl.replace('/130/', '/220/')}
                  alt={`${title} - ${authors.join(', ')}`}
                />
              </div>
            </div>
            <div className="bookCard__meta">
              <div className="bookCard__item">
                <div className="bookCard__icon bookCard__icon--rank">
                  <IconsRank />
                </div>
                <div className="bookCard__label">{avgrate.toFixed(1)}</div>
              </div>
              {(type === 1 || relatedbookid) && (
                <div className="bookCard__item">
                  <div className="bookCard__icon bookCard__icon--typeEbook">
                    <IconsEbook />
                  </div>
                </div>
              )}
              {(type === 2 || relatedbookid) && (
                <div className="bookCard__item">
                  <div className="bookCard__icon bookCard__icon--typeAudiobook">
                    <IconsAudioBook />
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="bookCard__row">
          <div className="bookCard__title" itemProp="name">
            {title}
          </div>
        </div>
      </Link>
    )
  }
}

export default withNamespaces(['lang_route'])(BookCard)
