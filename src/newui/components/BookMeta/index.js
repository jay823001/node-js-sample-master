import React from 'react'
import { withNamespaces } from 'react-i18next'

class BookMeta extends React.Component {
  render() {
    const { isbn, heading, pubdate, language, publisher } = this.props
    const trimmedPubDate = pubdate.slice(0, 10)

    return (
      <div className="bookDetails__metaGroup">
        <h3 className="bookDetails__metaHeading">{heading}</h3>
        <div className="bookDetails__metaTable">
          <div className="bookDetails__metaItem">
            <span className="bookDetails__metaLabel">{this.props.t('bookMeta.published')}</span>
            <span className="bookDetails__metaValue">
              {new Date(trimmedPubDate).toISOString().slice(0, 10)}
            </span>
          </div>
          <div className="bookDetails__metaItem">
            <span className="bookDetails__metaLabel">{this.props.t('bookMeta.language')}</span>
            <span className="bookDetails__metaValue">
              {this.props.t('bookMeta.languages')[language]}
            </span>
          </div>
          <div className="bookDetails__metaItem">
            <span className="bookDetails__metaLabel">{this.props.t('bookMeta.publisher')}</span>
            <span
              className="bookDetails__metaValue"
              itemProp="publisher"
              itemType="http://schema.org/Organization"
              itemScope=""
            >
              <span itemProp="name">{publisher}</span>
            </span>
          </div>
          <div className="bookDetails__metaItem">
            <span className="bookDetails__metaLabel">ISBN</span>
            <span className="bookDetails__metaValue" itemProp="isbn">
              {isbn}
            </span>
          </div>
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(BookMeta)
