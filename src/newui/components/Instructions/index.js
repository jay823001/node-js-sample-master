import React from 'react'
import BEMHelper from 'react-bem-helper'

class Instructions extends React.PureComponent {
  render() {
    let classes = new BEMHelper('grid')
    const { items } = this.props
    return (
      <div {...classes('')}>
        {items.map((item, index) => (
          <div {...classes('col', ["phone12", "tablet6"])} key={index}>
            {item.section.map((section, key) => (
              <React.Fragment key={key}>
                <h2>{section.heading}</h2>
                {section.text.map((text, i) => (
                  <p key={i}>{text.content}</p>
                ))}
              </React.Fragment>
            ))}
          </div>
        ))}
      </div>
    )
  }
}
export default Instructions