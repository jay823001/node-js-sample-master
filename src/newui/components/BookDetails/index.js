import React from 'react'
import BookMeta from 'newui/components/BookMeta'
import IconsRank from 'newui/components/Icons/Rank'
import IconsEbook from 'newui/components/Icons/Ebook'
import IconsAudioBook from 'newui/components/Icons/AudioBook'

import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

class BookDetails extends React.Component {

  render() {
    const { books, related } = this.props

    let type
    if (books[0]) {
      type =
        books[0].type === 1
          ? this.props.t('lang_route:books.categoriesEbooks')
          : this.props.t('lang_route:books.categoriesAudioBooks')
    }

    const audiobook = books.find(({ duration }) => duration)

    return (
      <React.Fragment>
        <div className="bookDetails">
          <div className="bookDetails__cover">
            <img
              className="bookDetails__image"
              itemProp="photo"
              src={books[0].imageurl.replace('/130/', '/400/')}
              alt=""
            />
          </div>
          <div className="bookDetails__info">
            <div className="bookDetails__heading" itemProp="name">
              {books[0].title}
            </div>
            <div className="bookDetails__authors" itemProp="author">
              {this.props.t('bookDetails.details.authors.authors', {
                authors: books[0].authors.join(' & '),
                context: books[0].narrators.length && 'narrators',
                narrators: books[0].narrators.length && books[0].narrators.join(' & '),
              })}
            </div>
            {books[0].numberinseries > 0 &&
              related[0] && (
                <div className="bookDetails__seriesName">
                  {this.props.t('bookDetails.details.series.series', {
                    number: books[0].numberinseries,
                    context: related[0].data.length > 0 && 'total',
                    total: related[0].data.length > 0 && related[0].data.length,
                  })}
                  <Link to={`${type}/${related.find(({ id }) => id.includes('series')).id}`}>
                    {` ${books[0].series}`}
                  </Link>.
                </div>
              )}
            <div className="bookDetails__props">
              <div className="bookDetails__propsItem">
                <div className="bookDetails__propsIcon bookDetails__propsIcon--rank">
                  <IconsRank />
                </div>
                <div className="bookDetails__propsLabel">{books[0].avgrate.toFixed(2)}</div>
              </div>
              {(books[0].type === 2 || books[0].relisbn) && (
                <div className="bookDetails__propsItem">
                  <div className="bookDetails__propsIcon bookDetails__propsIcon--typeAudiobook">
                    <IconsAudioBook large />
                  </div>
                  <div className="bookDetails__propsLabel">
                    {this.props.t('bookDetails.heading.audio')}
                    {audiobook.duration && `, ${audiobook.duration}`}
                  </div>
                </div>
              )}
              {(books[0].type === 1 || books[0].relisbn) && (
                <div className="bookDetails__propsItem">
                  <div className="bookDetails__propsIcon bookDetails__propsIcon--typeEbook">
                    <IconsEbook large />
                  </div>
                  <div className="bookDetails__propsLabel">
                    {this.props.t('bookDetails.heading.ebook')}
                  </div>
                </div>
              )}
            </div>
          </div>
          <div itemProp="description" className="bookDetails__description">
            <p>{books[0].descriptionfull}</p>
          </div>
          <div className="bookDetails__meta">
            {(books[0].type === 2 || books[0].relisbn) && (
              <BookMeta
                heading={this.props.t('bookDetails.heading.audio')}
                {...books.find(book => book.type === 2)}
              />
            )}
            {(books[0].type === 1 || books[0].relisbn) && (
              <BookMeta
                heading={this.props.t('bookDetails.heading.ebook')}
                {...books.find(book => book.type === 1)}
              />
            )}
          </div>
        </div>

      </React.Fragment>
    )
  }
}

export default withNamespaces(['lang_books'])(BookDetails)
