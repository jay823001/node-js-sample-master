import React from 'react'
import { withNamespaces } from 'react-i18next'

import IconsOwl from 'newui/components/Icons/Owl'
import IconsDevices from 'newui/components/Icons/Device'
import IconsStopWatch from 'newui/components/Icons/StopWatch'

class SubscriptionFeatures extends React.Component {
  render() {
    return (
      <div className="subscriptionFeatures">
        <h2 className="subscriptionFeatures__heading">
          {this.props.t('subscriptionFeatures.heading')}
        </h2>
        <div className="subscriptionFeatures__list">
          <div className="subscriptionFeatures__listItem">
            <div className="featurePro">
              <div className="featurePro__icon">
                <IconsOwl />
              </div>
              <div className="featurePro__label">
                {this.props.t('subscriptionFeatures.promoted.0')}
              </div>
            </div>
          </div>
          <div className="subscriptionFeatures__listItem">
            <div className="featurePro">
              <div className="featurePro__icon">
                <IconsStopWatch />
              </div>
              <div className="featurePro__label">
                {this.props.t('subscriptionFeatures.promoted.1')}
              </div>
            </div>
          </div>
          <div className="subscriptionFeatures__listItem">
            <div className="featurePro">
              <div className="featurePro__icon">
                <IconsDevices />
              </div>
              <div className="featurePro__label">
                {this.props.t('subscriptionFeatures.promoted.2')}
              </div>
            </div>
          </div>
        </div>
        <div className="subscriptionFeatures__box">
          {this.props.t('subscriptionFeatures.packages').map((copyText, key) => {
            let context

            if (this.props.books.length <= 1) {
              context = this.props.books[0].type === 1 ? 'ebook' : 'audiobook'
            }
            if (!(this.props.books[0].allowedforstring.split(',').length % 2) && key === 0) {
              context = 'missing'
            }

            return (
              <div className="subscriptionFeatures__boxItem" key={key}>
                <div className="featureBox">
                  <h3 className="featureBox__heading">{copyText.heading}</h3>
                  <p className="featureBox__description">{copyText.description}</p>
                  <div className="featureBox__list">
                    <div className="featureBox__item">
                      <div
                        className={`featureBox__label ${key === 0 &&
                          'featureBox__label--notIncluded'}`}
                      >
                        {copyText.bullets[0]}
                      </div>
                    </div>
                    <div className="featureBox__item">
                      <div className="featureBox__label">{copyText.bullets[1]}</div>
                    </div>
                  </div>
                  <p className="featureBox__disclaimer">
                    {this.props.t(`subscriptionFeatures.packages.${key}.disclaimer`, {
                      name: this.props.books[0].title,
                      package: copyText.heading,
                      context: context,
                    })}
                  </p>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(SubscriptionFeatures)
