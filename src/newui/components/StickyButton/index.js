import React from 'react'
import BEMHelper from 'react-bem-helper'

import { uppercaseFirst } from 'newui/utils/helperFunctions'

class StickyButton extends React.PureComponent {

  // Set background color
  bgColor() {
    if (this.props.bgColor) {
      return `bg${uppercaseFirst(this.props.bgColor)}`
    }

    return 'bgWhite'
  }

  render() {
    var classes = new BEMHelper('stickyButton')
    const bgcolor = this.bgColor()
    const stickyPhone = this.props.stickyPhone ? 'stickyPhone' : ''
    const stickyDesktop = this.props.stickyDesktop ? 'stickyDesktop' : ''

    return (
      <div {...classes('', [bgcolor, stickyPhone, stickyDesktop])}>
        <div {...classes('main', [bgcolor, stickyPhone, stickyDesktop])}>
          <div {...classes('content', [bgcolor, stickyPhone, stickyDesktop])}>
            {this.props.children}
          </div>
          <div {...classes('fade', [bgcolor, stickyPhone, stickyDesktop])} />
        </div>
      </div>
    )
  }
}

export default StickyButton
