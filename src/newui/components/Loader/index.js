import React from 'react'

class Loader extends React.Component {
  render() {
    return (
      <div className="loader loader--local" />
    )
  }
}

export default Loader
