import React from 'react'
import renderField from 'newui/components/RenderField'
import getNormalizedValues from './FormInput.NormalizeMyValues'

import { Field } from 'redux-form'

export default class FormInput extends React.PureComponent {

  render() {
    const normalizedValue = getNormalizedValues(this.props.items)
    return (
      <div className='formInput'>
        <Field
          className='formInput__field'
          name={this.props.items.name ? this.props.items.name : 'textinput'}
          component={
            renderField
          }
          type={this.props.items.type ? this.props.items.type : 'text'}
          placeholder={
            this.props.items.placeholder ? this.props.items.placeholder : ''
          }
          maxLength={
            this.props.items.maxLength ? this.props.items.maxLength : null
          }
          value={this.props.items.value ? this.props.items.value : ''}
          pattern={this.props.items.pattern ? this.props.items.pattern : null}
          normalize={normalizedValue}
        />
      </div>
    )
  }
}
