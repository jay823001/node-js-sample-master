import formatCvvNumber from 'newui/components/PaymentOption/PaymentFunctions/CreditCard.FormatCvvNumber'
import formatCardNumber from 'newui/components/PaymentOption/PaymentFunctions/CreditCard.FormatCardNumber'
import formatVoucherCode from 'newui/containers/Views/Register/CampaignFlow/FormatVoucherCode'
import validatePhone from 'newui/containers/Views/Common/UserDetails/UserDetails.ValidatePhone'

/*
**@event: Set card normalize values
**@return description: card number and cvv validation formatting
**@return type: Object
**@parameter description: none
**@parameter type: none
*/

const getNormalizedValues = (items) => {
  if (items && items.normalize) {
    switch (items.normalize) {
      case 'cardNumber':
        return formatCardNumber
      case 'cardCvv':
        return formatCvvNumber
      case 'vouchercode':
        return formatVoucherCode
      case 'phoneNumber':
        return validatePhone
      default:
        return null
    }
  } else {
    return null
  }
}

export default getNormalizedValues
