import React from 'react'
import { withNamespaces } from 'react-i18next'

const renderSelectField = (props) => {
  const {
    input,
    label,
    children,
    meta: {
      touched,
      error
    }
  } = props
  return (
    <div>
      <label>{label}</label>
      <div>
        <select {...input} className={`formSelect__field ${touched && error ? 'input-has-error' : ''}`} >
          {children}
        </select>
        {touched && error && <span className='text-has-error'>{props.t(error)}</span>}
      </div>
    </div>
  )
}

export default withNamespaces(['lang_error'])(renderSelectField)