import React from 'react'
import BEMHelper from 'react-bem-helper'
import Media from 'newui/components/Media'

class ProductFeatures extends React.PureComponent {

  render() {
    let classes = new BEMHelper('productFeatures')
    const { heading, items, src, width, height, alt } = this.props

    return (
      <div {...classes('')}>
        <div {...classes('info')}>
          <h2 {...classes('heading')} > {heading}</h2>
          <div {...classes('list')}>
            {items.map((item, index) => (
              <div {...classes('listItem')} key={index}>
                <div {...classes('listHeading')}> {item.heading}</div>
                <div {...classes('listDescription')}> {item.description}</div>
              </div>
            ))}

          </div>
        </div>
        <div {...classes('image')}>
          <Media
            src={src}
            width={width}
            height={height}
            alt={alt}
          />
        </div>
      </div >
    )
  }
}

export default ProductFeatures


