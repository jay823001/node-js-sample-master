import React from 'react'

import Block from 'newui/components/Section/components/Block'
import Wrapper from 'newui/components/Section/components/Wrapper'

class Section extends React.Component {
  render() {
    const { children, ...modifiers } = this.props

    return (
      <Block {...modifiers}>
        <Wrapper {...modifiers}>{children}</Wrapper>
      </Block>
    )
  }
}

export default Section
