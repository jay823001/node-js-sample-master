import React from 'react'
import BEMHelper from 'react-bem-helper'

import { uppercaseFirst } from 'newui/utils/helperFunctions'

import './block.scss'

class Block extends React.Component {

  convertSpacing(spacing) {
    if (spacing.constructor === String) {
      spacing = [spacing.trim().split(',')]
    }
    if (spacing.constructor === Array) {
      return spacing.map(item => `${item}Spacing`)
    }

    return []
  }

  render() {
    const { bg, bleed, spacing = 'normal', children } = this.props

    const classes = new BEMHelper('block')
    const modifiers = [
      bg && `bg${uppercaseFirst(bg)}`,
      bleed ? `${bleed}Bleed` : `normalBleed`,
    ]

    return <div {...classes('', [...modifiers, ...this.convertSpacing(spacing)])}>{children}</div>
  }
}

export default Block
