import React from 'react'
import BEMHelper from 'react-bem-helper'

import { uppercaseFirst } from 'newui/utils/helperFunctions'

import './wrapper.scss'

class Wrapper extends React.Component {
  uppercaseFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  render() {
    const { align, maxCols, fullScreen, children } = this.props

    const classes = new BEMHelper('wrapper__content')
    const modifiers = [
      align ? `align${uppercaseFirst(align)}` : 'alignCenter',
      maxCols && `maxCols${maxCols}`,
      fullScreen && `fullScreen`,
    ]

    return (
      <div className='wrapper' >
        <div {...classes('', modifiers)}>{children}</div>
      </ div>
    )

  }
}

export default Wrapper
