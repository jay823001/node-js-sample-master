import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

import IconsToolFilter from 'newui/components/Icons/ToolFilter'

class SearchForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inputText: props.query || '',
      showAutoComplete: false,
    }

    this.onLink = this.onLink.bind(this)
    this.onSearch = this.onSearch.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      inputText: nextProps.query || '',
    })
  }

  onLink({ title }) {
    this.setState({ showAutoComplete: false })
    return this.props.onSearch(title, true)
  }

  onSearch(event) {
    if (event.target.value !== '') {
      this.setState({ showAutoComplete: true, inputText: event.target.value })
    } else {
      this.setState({ showAutoComplete: false, inputText: event.target.value })
    }

    return this.props.onSearch(event.target.value, event.key === 'Enter')
  }

  onSubmit(event) {
    if (event.key === 'Enter') {
      this.setState({ showAutoComplete: false })

      return this.props.onSearch(event.target.value, true)
    }
  }

  render() {
    const { facets } = this.props

    return (
      <div className="searchForm">
        <div className="searchForm__form">
          <div className="searchForm__inputWrapper">
            <div className="searchForm__input">
              <div className="formInput">
                <input
                  className="formInput__field"
                  placeholder={this.props.t('bookSearch.search')}
                  autoComplete="off"
                  type="text"
                  name="Search"
                  onChange={this.onSearch}
                  onKeyDown={this.onSubmit}
                  value={this.state.inputText}
                />
              </div>
            </div>
            {this.state.showAutoComplete ? (
              <div className="searchForm__autocomplete searchForm__autocomplete--active">
                {facets && facets.length > 0
                  ? facets.map((item, key) => {
                    return (
                      <Link
                        onClick={this.onLink.bind(this, item)}
                        to={`?query=${item.title}`}
                        key={key}
                      >
                        <div className="searchForm__autocompleteItem">{item.title}</div>
                      </Link>
                    )
                  })
                  : null}
              </div>
            ) : null}
          </div>
          <div className="searchForm__button" onClick={this.props.toggleFilter}>
            <div className="btn btn--secondary btn--icon btn--wide">
              <IconsToolFilter />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(SearchForm)
