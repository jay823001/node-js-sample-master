import React from 'react'
import Checkbox from 'newui/components/Checkbox'
import UserModule from 'newui/domain/Modules/User'
import Config from 'newui/config'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class AmountPicker extends React.PureComponent {

  static defaultProps = {
    items: [],
    activeModel: Config.default_family_subscription
  }

  /*
   **@event: change to active user package
   **@return description: none
   **@return type: none
   **@parameter description: array index of active user package
   **@parameter type: integer
   */
  setActiveItem(item) {
    item.active = true
    this.props.selectSubscriptionFamily(item)
  }

  changeSubscription = (index) => {
    const subscription = {
      isfamily: true,
      index: index
    }
    this.props.selectSubscription(subscription)
  }

  getSubscriptionId = () => {
    return this.props.selectedSubscription.isfamily ? this.props.selectedSubscription.index : this.props.activeModel
  }

  render() {
    const subscriptionId = this.getSubscriptionId()
    return (
      <div className='amountPicker'>
        {this.props.items.map((item, index) => (
          <div
            className='amountPicker__item'
            key={index}
            onClick={() => this.changeSubscription(index)}
          >
            <div
              className={`amountPicker__info amountPicker__info${
                item.index === subscriptionId ? '--active' : ''
                }`}
            >
              <p className='amountPicker__heading'>{item.heading}</p>
              <p className='amountPicker__description'>{item.label}</p>
              <p className='amountPicker__description'>{item.details}</p>
            </div>
            <div className='amountPicker__checkbox'>
              <Checkbox active={`${item.index === subscriptionId ? 'active' : ''}`} />
            </div>
          </div>
        ))}
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    selectedSubscription: state.selectedSubscription
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectSubscription: UserModule.actions.selectSubscription
    },
    dispatch
  )
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(AmountPicker)
