import React from 'react'
import BEMHelper from 'react-bem-helper'
import FeaturePro from 'newui/components/FeaturePro'
import FeatureBox from 'newui/components/FeatureBox'

class SubscriptionPros extends React.PureComponent {

  render() {
    let classes = new BEMHelper('subscriptionFeatures')
    const { pros, boxes, heading, bookTitle } = this.props

    return (
      <div {...classes('')}>
        <h2 {...classes('heading')} >{heading}</h2>
        <div {...classes('list')}>
          {pros.map((pro, index) => (
            <div {...classes('listItem')} key={index}>
              <FeaturePro
                text={pro.text}
                iconType={pro.iconType} />
            </div>
          ))}
        </div>

        <div {...classes('box')}>
          {boxes.map((box, index) => (
            <div {...classes('boxItem')} key={index}>
              <FeatureBox
                bookTitle={bookTitle}
                heading={box.heading}
                description={box.description}
                items={box.items}
                disclaimer={box.disclaimer} />
            </div>
          ))}
        </div>
      </div>
    )
  }
}

export default SubscriptionPros


