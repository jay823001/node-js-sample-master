import React from 'react'
import BEMHelper from 'react-bem-helper'

import { imageHelper } from 'newui/assets/mixins/ImageHelper'

class BookCover extends React.PureComponent {
  render() {
    const classes = new BEMHelper('bookCover')
    const { fixedHeight, src, alt } = this.props
    return (
      <div {...classes('', fixedHeight)} >
        <img {...classes('image', fixedHeight)} src={imageHelper(src)} alt={alt} />
      </div>
    )
  }
}

export default BookCover