import React from 'react'

class PageHeader extends React.PureComponent {
  render() {
    return (
      <div className='pageHeader'>
        {(this.props.label && this.props.label !== '') &&
          <p className='pageHeader__label'>{this.props.label}</p>
        }
        {(this.props.heading && this.props.heading !== '') &&
          <h1 className='pageHeader__heading'>{this.props.heading}</h1>
        }
        {(this.props.description && this.props.description !== '') &&
          <p className='pageHeader__description'>{this.props.description}</p>
        }
        {(this.props.children) &&
          <div className='pageHeader__content'>
            {this.props.children}
          </div>
        }
      </div>
    )
  }
}

export default PageHeader
