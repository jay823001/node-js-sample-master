import React from 'react'
import Checkbox from 'newui/components/Checkbox'

import { withNamespaces } from 'react-i18next'

class FormCheckbox extends React.PureComponent {

  state = {
    isChecked: false,
  }

  /*
  **@event: set checkbox value
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */
  setIsChecked = () => {
    const alternateCheck = !this.state.isChecked
    this.setState({
      isChecked: alternateCheck,
    })
    this.props.setacceptterms(alternateCheck)
  }

  /*
  **@event: set checkbox class
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */
  setActive = () => {
    return this.state.isChecked ? 'active' : ''
  }

  render() {
    // Show hide validation error
 
    return (
      <React.Fragment>
        <div className={(!this.state.isChecked && this.props.showcheckerror)?'formCheckbox formCheckbox_error':'formCheckbox'} >
          <div className='formCheckbox__icon' onClick={() => this.setIsChecked()}>
            <Checkbox active={this.setActive()} />
          </div>
          <div className='formCheckbox__label' dangerouslySetInnerHTML={{ __html: this.props.label }} />
        </ div>
        {!this.state.isChecked && this.props.showcheckerror === true &&
          <span className='text-has-error'>{this.props.t('registerForm.termserror')}</span>
        }
      </React.Fragment>
    )
  }
}

export default withNamespaces(['lang_error'])(FormCheckbox)