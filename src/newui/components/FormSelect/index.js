import React from 'react'
import renderSelectField from 'newui/components/RenderSelect'
import IconsCaretDown from 'newui/components/Icons/CaretDown'

import { Field } from 'redux-form'

class FormSelect extends React.PureComponent {
  render() {
    return (
      <div className='formSelect'>
        <Field className='formSelect__field'
          component={renderSelectField}
          name={this.props.name}
          placeholder={this.props.placeholder}
        >
          {this.props.items.map((item, index) => (
            <option key={index} value={item.value}>
              {item.text}
            </option>
          ))}
        </Field>
        <div className='formSelect__icon'>
          <IconsCaretDown />
        </div>
      </div>
    )
  }
}

export default FormSelect
