import React from 'react'
import Media from 'newui/components/Media'

class DownloadButtons extends React.PureComponent {
  render() {
    return (
      <div className='downloadButtons'>
        {this.props.items.map((item, index) => (
          <div className='downloadButtons__item' key={index}>
            <a className='downloadButtons__link' href={item.link} target={item.target}>
              <Media src={item.src} width={240} height={84} />
            </a>
          </div>
        ))}
      </div>
    )
  }
}

export default DownloadButtons
