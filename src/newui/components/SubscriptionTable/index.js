import React from 'react'
// import 'newui/assets/styles/main.scss'
import Section from 'newui/components/Section'
import MainApp from 'newui/containers/Views/index'
import SubscriptionOptionsBasic from 'newui/components/SubscriptionTable/SubscriptionOptions/SubscriptionTable.Basic'
import SubscriptionOptionsFamily from 'newui/components/SubscriptionTable/SubscriptionOptions/SubscriptionTable.Family'

import { withNamespaces } from 'react-i18next'
import { IdentifyFlow } from 'newui/utils/helperFunctions'

class SubscriptionOptions extends React.Component {

  /*
  **@event: identify whether url is one of family subscription
  **@return description: true if yes, else false
  **@return type: boolean
  **@parameter description: none
  **@parameter type: none
  */
  checkIsSubscriptionFamily() {
    /** Add Family urls to the below object */
    const familyUrls = [
      this.props.t(`registration.${IdentifyFlow(this.props.flow)}.subscriptionFamily`),
      this.props.t('mypages.reActivationFlow.subscriptionFamily'),
      this.props.t('mypages.changeMySubscriptionFamily')
    ]

    const location = `${window.location.pathname}${window.location.hash}`
    return familyUrls.includes(location)
  }

  render() {
    const isSubscriptionFamily = this.checkIsSubscriptionFamily()
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <main className='app__main'>
          <Section maxCols={8} spacing={false}>
            {isSubscriptionFamily ? (
              <SubscriptionOptionsFamily bgColor={this.props.bgColor} header={this.props.header} flow={this.props.flow} campaignInfo={this.props.campaignInfo} />
            ) : (
                <SubscriptionOptionsBasic bgColor={this.props.bgColor} header={this.props.header} flow={this.props.flow} campaignInfo={this.props.campaignInfo} />
              )}
          </Section>
        </main>
      </MainApp>

    )
  }
}

export default withNamespaces(['lang_route'])(SubscriptionOptions)