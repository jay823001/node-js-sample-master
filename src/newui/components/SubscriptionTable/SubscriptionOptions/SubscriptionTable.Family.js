import React from 'react'
import Button from 'newui/components/Button'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import AmountPicker from 'newui/components/AmountPicker'
import StickyButton from 'newui/components/StickyButton'

import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import { SubscriptionConfig, getDynamicString } from './SubscriptionConfig'
import { setButtonProperties, IdentifyFlow } from 'newui/utils/helperFunctions'
import { getDynamicText } from 'newui/utils/translationHelper'

class SubscriptionOptionsFamily extends React.Component {

  getButtonProperties = () => {
    let Url = this.props.t('lang_route:registration.trialFlow.createAccountMessage')
    if (this.props.userDetails.isLoggedIn) {
      if (this.props.flow === 're-activate') {
        Url = this.props.t('lang_route:mypages.reActivationFlow.checkoutOptions')
      } else if (this.props.flow === 'change-payment') {
        Url = this.props.t('lang_route:mypages.subscriptionChangedMessage')
      } else {
        Url = this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.checkoutOptions`)
      }
    } else {
      if (this.props.flow === 'trial') {
        Url = this.props.t('lang_route:registration.trialFlow.createAccountMessage')
      } else if (this.props.flow === 'campaign') {
        Url = this.props.t('lang_route:registration.campaignFlow.checkoutOptions')
      } else if (this.props.flow === 're-activate') {
        Url = this.props.t('lang_route:mypages.reActivationFlow.checkoutOptions')
      } else {
        Url = this.props.t('lang_route:registration.trialFlow.createAccountMessage')
      }
    }

    return {
      url: Url,
      text: this.props.t('subscriptionOptions.subscriptionOptionsFamily.buttons.text'),
      className: 'btn--primary btn--arrow'
    }
  }

  render() {
    const packageDetails = SubscriptionConfig(this.props).subscriptionOptionsFamily
    return (
      <React.Fragment>
        <PageHeader
          label={this.props.flow !== 'change-payment' ? getDynamicString(this.props, 'headerLabel') : null}
          heading={this.props.t('subscriptionOptions.subscriptionOptionsFamily.heading')}
          description={this.props.t('subscriptionOptions.subscriptionOptionsFamily.description')}
        />
        <BoxWrapper
          maxCols={6}
          modifiers={['textCenter']}
          heading={getDynamicText(this.props, 'family_offer_clarification')}
        >
          <AmountPicker items={packageDetails.items} activeModel={packageDetails.activeModel} />
        </BoxWrapper>
        <StickyButton bgColor={this.props.bgColor} stickyPhone={true}>
          <Button items={setButtonProperties(this.getButtonProperties())} />
        </StickyButton>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails
  }
}

export default withNamespaces(['lang_register'])(connect(mapStateToProps)(SubscriptionOptionsFamily))
