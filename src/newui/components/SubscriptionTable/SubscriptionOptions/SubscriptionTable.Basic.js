import React from 'react'
import Button from 'newui/components/Button'
import SubscriptionTable from './SubscriptionTable'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import StickyButton from 'newui/components/StickyButton'

import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import { getDynamicText } from 'newui/utils/translationHelper'
import {
  SubscriptionConfig,
  getDynamicbenefits
} from './SubscriptionConfig'
import {
  IdentifyFlow,
  setButtonProperties
} from 'newui/utils/helperFunctions'

class SubscriptionOptionsBasic extends React.Component {

  /*
   **@event: set navigation url depending on package user selected
   **@return description: none
   **@return type: none
   **@parameter description: none
   **@parameter type: none
   */

  setButtonUrl() {
    // let details = this.state.pageDetails
    let url
    if (this.props.selectedSubscription.isfamily) {
      if (this.props.flow === 're-activate') {
        url = this.props.t('lang_route:mypages.reActivationFlow.subscriptionFamily')
      } else if (this.props.flow === 'change-payment') {
        url = this.props.t('lang_route:mypages.changeMySubscriptionFamily')
      } else {
        url = this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.subscriptionFamily`)
      }
    } else {
      if (this.props.userDetails.isLoggedIn) {
        if (this.props.flow === 're-activate') {
          url = this.props.t('lang_route:mypages.reActivationFlow.checkoutOptions')
        } else if (this.props.flow === 'change-payment') {
          url = this.props.t('lang_route:mypages.subscriptionChangedMessage')
        } else {
          url = this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.checkoutOptions`)
        }
      } else {
        if (this.props.flow === 'trial') {
          url = this.props.t('lang_route:registration.trialFlow.createAccountMessage')
        } else if (this.props.flow === 'campaign') {
          url = this.props.t('lang_route:registration.campaignFlow.checkoutOptions')
        } else if (this.props.flow === 're-activate') {
          url = this.props.t('lang_route:mypages.reActivationFlow.checkoutOptions')
        } else {
          url = this.props.t('lang_route:registration.trialFlow.createAccountMessage')
        }
      }
    }

    return url
  }

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:registration.trialFlow.createAccountMessage'),
      text: this.props.t('subscriptionOptions.subscriptionOptionsBasic.buttons.text'),
      className: 'btn--primary btn--arrow'
    }
  }

  render() {
    const getButtonProperties = this.getButtonProperties()
    const packageDetails = SubscriptionConfig(this.props).subscriptionOptionsBasic


    return (
      <React.Fragment>
        <PageHeader
          label={this.props.flow !== 'change-payment' ? this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionOptions.subscriptionOptionsBasic.label`) : null}
          heading={this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionOptions.subscriptionOptionsBasic.heading`)}
          description={this.props.t('subscriptionOptions.subscriptionOptionsBasic.description')}
        />
        <BoxWrapper maxCols={8}>
          <SubscriptionTable
            activeModel={packageDetails.activeModel}
            models={packageDetails.models}
            price={this.props.t('subscriptionOptions.subscriptionOptionsBasic.price')}
            priceLable={getDynamicText(this.props, 'subscription_date_offer')}
            users={packageDetails.users}
            benefits={getDynamicbenefits(this.props)}
            flow={this.props.flow}
          />
        </BoxWrapper>
        <StickyButton bgColor={this.props.bgColor} stickyPhone={true} stickyDesktop={true}>
          <Button items={setButtonProperties(getButtonProperties)} toUrl={this.setButtonUrl()} />
        </StickyButton>
      </React.Fragment >
    )
  }
}

function mapStateToProps(state) {
  return {
    selectedSubscription: state.selectedSubscription,
    userDetails: state.userDetails
  }
}

export default withNamespaces(['lang_register'])(connect(mapStateToProps)(SubscriptionOptionsBasic))
