import React from 'react'
import BEMHelper from 'react-bem-helper'
import Checkbox from 'newui/components/Checkbox'
import Cross from 'newui/components/Icons/Cross'
import UserModule from 'newui/domain/Modules/User'
import Checkmark from 'newui/components/Icons/Checkmark'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class SubscriptionTable extends React.PureComponent {

  /*
   **@event: change active subscription package
   **@return description: none
   **@return type: none
   **@parameter description: array index of active subscription
   **@parameter type: integer
   */

  changeSubscription = (index) => {
    let getIndex = (index === undefined) ? this.props.activeModel : index
    let isFamily = (getIndex === 2) ? true : false

    const subscription = {
      index: getIndex,
      isfamily: isFamily
    }
    this.props.selectSubscription(subscription)
  }

  componentDidMount() {
    this.changeSubscription()
  }

  render() {

    const classes = new BEMHelper('subscriptionTable')
    return (
      <div {...classes('', this.props.modifiers)}>
        <div className='subscriptionTable__header'>
          {this.props.models.map((item, index) => (
            <div
              className='subscriptionTable__model'
              key={index}
              onClick={() => this.changeSubscription(index)}
            >
              <div
                className={`subscriptionTable__name subscriptionTable__name${
                  index === this.props.selectedSubscription.index ? '--active' : ''
                  }`}
              >
                {item.name}
              </div>
              <div className='subscriptionTable__checkbox'>
                <Checkbox
                  active={index === this.props.selectedSubscription.index ? 'active' : ''}
                />
              </div>
            </div>
          ))}
        </div>
        <div className='subscriptionTable__main'>
          <div className='subscriptionTable__row'>
            <div className='subscriptionTable__label'>
              {this.props.priceLable.label}
            </div>
            <div className='subscriptionTable__list'>
              {this.props.price.values.map((item, index) => (
                <div
                  className={`subscriptionTable__item${
                    index === this.props.selectedSubscription.index
                      ? ' subscriptionTable__item--active'
                      : ''
                    }`}
                  key={index}
                >
                  {item}
                </div>
              ))}
            </div>
          </div>
          <div className='subscriptionTable__row'>
            <div
              className='subscriptionTable__label'
              v-text='$props.users.label'
            >
              {this.props.users.label}
            </div>
            <div className='subscriptionTable__list'>
              {this.props.users.values.map((item, index) => (
                <div
                  className={`subscriptionTable__item${
                    index === this.props.selectedSubscription.index
                      ? ' subscriptionTable__item--active'
                      : ''
                    }`}
                  key={index}
                >
                  {item}
                </div>
              ))}
            </div>
          </div>
          {this.props.benefits.map((item, index) => (
            <div className='subscriptionTable__row' key={index}>
              <div className='subscriptionTable__label'>{item.label}</div>

              <div className='subscriptionTable__list'>
                {item.values.map((item, index) => (
                  <div
                    className={`subscriptionTable__item${
                      index === this.props.selectedSubscription.index
                        ? ' subscriptionTable__item--active'
                        : ''
                      }`}
                    key={index}
                  >
                    {item ? <Checkmark /> : <Cross />}
                  </div>
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    selectedSubscription: state.selectedSubscription
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectSubscription: UserModule.actions.selectSubscription
    },
    dispatch
  )
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(SubscriptionTable)
