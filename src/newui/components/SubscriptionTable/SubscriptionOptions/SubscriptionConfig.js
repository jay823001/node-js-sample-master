import Config from "newui/config"
//import { getPluralUnitPeriod } from 'newui/utils/translationHelper'
import { getDynamicText } from "newui/utils/translationHelper"
import { IdentifyCampaign, IdentifyFlow } from "newui/utils/helperFunctions"

export const SubscriptionConfig = props => {
  return {
    subscriptionOptionsBasic: {
      activeModel: Config.default_basic_subscription,
      models: [
        {
          name: props.t(
            "lang_register:subscriptionOptions.subscriptionOptionsBasic.models")[0].name,
          index: 0,
          id: Config.subscriptionIds.silver
        },
        {
          name: props.t(
            "lang_register:subscriptionOptions.subscriptionOptionsBasic.models")[1].name,
          index: 1,
          id: Config.subscriptionIds.gold
        },
        {
          name: props.t(
            "lang_register:subscriptionOptions.subscriptionOptionsBasic.models")[2].name,
          index: 2,
          id: Config.subscriptionIds.family2
        }
      ],
      "price": props.t('lang_register:subscriptionOptions.subscriptionOptionsBasic.price'),
      users: {
        label: props.t('lang_register:subscriptionOptions.subscriptionOptionsBasic.users.label'),
        values: [
          "1",
          "1",
          "2-4"
        ]
      },
    },
    subscriptionOptionsFamily: {
      activeModel: Config.default_family_subscription,
      items: [
        {
          "heading": "2",
          "category": `${props.t('lang_register:subscriptionOptions.subscriptionOptionsBasic.models')[2].name}`,
          "label": props.t('lang_register:subscriptionOptions.subscriptionOptionsFamily.items')[0].label,
          "details": props.t('lang_register:subscriptionOptions.subscriptionOptionsFamily.items')[0].details,
          "index": 0,
          "id": Config.subscriptionIds.family2
        },
        {
          "heading": "3",
          "category": `${props.t('lang_register:subscriptionOptions.subscriptionOptionsBasic.models')[2].name}`,
          "label": props.t('lang_register:subscriptionOptions.subscriptionOptionsFamily.items')[1].label,
          "details": props.t('lang_register:subscriptionOptions.subscriptionOptionsFamily.items')[1].details,
          "index": 1,
          "id": Config.subscriptionIds.family3
        },
        {
          "heading": "4",
          "category": `${props.t('lang_register:subscriptionOptions.subscriptionOptionsBasic.models')[2].name}`,
          "label": props.t('lang_register:subscriptionOptions.subscriptionOptionsFamily.items')[2].label,
          "details": props.t('lang_register:subscriptionOptions.subscriptionOptionsFamily.items')[2].details,
          "index": 2,
          "id": Config.subscriptionIds.family4
        }
      ]
    }
  }
}

export const getDynamicbenefits = props => {
  let benefits = [
    {
      label: props.t("subscriptionOptions.subscriptionOptionsBasic.benefits")[0]
        .label,
      values: [false, true, true]
    },
    {
      label: props.t("subscriptionOptions.subscriptionOptionsBasic.benefits")[1]
        .label,
      values: [true, true, true]
    },
    {
      label: props.t("subscriptionOptions.subscriptionOptionsBasic.benefits")[2]
        .label,
      values: [true, true, true]
    },
    {
      label: props.t("subscriptionOptions.subscriptionOptionsBasic.benefits")[3]
        .label,
      values: [true, true, true]
    },
    {
      label: props.t("subscriptionOptions.subscriptionOptionsBasic.benefits")[4]
        .label,
      values: [true, true, true]
    },
    {
      // "label": props.t('subscriptionOptions.subscriptionOptionsBasic.benefits', { free_trial_days: Config.free_trial_days })[5].label,
      label: getDynamicText(props, "subscription_bullet_offer"),
      values: [true, true, true]
    },
    {
      label: props.t("subscriptionOptions.subscriptionOptionsBasic.benefits")[6]
        .label,
      values: [true, true, true]
    }
  ]

  if (props.flow === 'trial' || props.flow === 'campaign') {
    return benefits
  } else if (props.flow === 're-activate') {
    return benefits.filter(function (benefit) {
      return benefit.label !== props.flow
    })
  } else {
    return benefits
  }
}

export const getDynamicString = (props, attributeName) => {
  if (props.flow === "trial") {
    if (attributeName === "headerLabel") {
      return props.t(
        `register.${IdentifyFlow(
          props.flow
        )}.subscriptionOptions.subscriptionOptionsFamily.label`
      )
    } else {
      //-----------------------//
    }
  } else if (props.flow === "campaign") {
    if (attributeName === "headerLabel") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.subscriptionOptions.subscriptionOptionsFamily.label`
      )
    } else {
      //-----------------------//
    }
  } else if (props.flow === "re-activate") {
    if (attributeName === "headerLabel") {
      return props.t(
        `register.${IdentifyFlow(
          props.flow
        )}.subscriptionOptions.subscriptionOptionsFamily.label`
      )
    } else {
      //-----------------------//
    }
  } else {
    if (attributeName === "headerLabel") {
      return props.t(
        `register.${IdentifyFlow(
          props.flow
        )}.subscriptionOptions.subscriptionOptionsFamily.label`
      )
    } else {
      //-----------------------//
    }
  }
}
