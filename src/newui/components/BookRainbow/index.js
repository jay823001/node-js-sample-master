import React from 'react'
import BEMHelper from 'react-bem-helper'
import BookCover from 'newui/components/BookCover'

class BookRainBow extends React.Component {

  getShuffledBooks() {
    let books = this.props.items
    for (let i = (books.length - 1); i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [books[i], books[j]] = [books[j], books[i]];
    }
    return books
  }

  getBookList() {
    const numberOfBooks = 18 // limit the number of books returned
    let books = this.getShuffledBooks()
    books = books.slice(0, numberOfBooks)
    return books
  }
  render() {
    const classes = new BEMHelper('bookRainbow')
    const booklist = this.getBookList()
    return (
      <div {...classes('')}>
        <div {...classes('list')}>
          {booklist.map((book, index) => (
            <div {...classes('item')} key={index}>
              <BookCover
                src={`books/${book.coverimg}`}
                fixedHeight='false'
                alt={book.title} />
            </div>
          ))
          }
        </div>
      </div >
    )
  }
}

export default BookRainBow