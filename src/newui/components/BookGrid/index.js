import _ from 'lodash'
import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

import IconsRank from 'newui/components/Icons/Rank'
import IconsEbook from 'newui/components/Icons/Ebook'
import IconsAudioBook from 'newui/components/Icons/AudioBook'

class BookGrid extends React.Component {
  convertURL(title, authors, isbn) {
    return _.kebabCase(_.deburr(`${title}-${authors}-${isbn}`))
  }

  render() {
    const {
      book: { isbn, type, title, authors, avgrate, imageurl, relatedbookid },
    } = this.props

    return (
      <Link
        className="bookCardDetailed"
        to={`${this.props.t('books.book')}/${this.convertURL(title, authors, isbn)}`}
      >
        <div className="bookCardDetailed__cover">
          <div className="bookCover">
            <img className="bookCover__image" itemProp="photo" src={imageurl} alt="" />
          </div>
        </div>
        <div className="bookCardDetailed__info">
          <div className="bookCardDetailed__heading" itemProp="name">
            {title}
          </div>
          <div className="bookCardDetailed__authors" itemProp="author">
            {authors.join(', ')}
          </div>
          <div className="bookCardDetailed__meta">
            <div className="bookCardDetailed__item">
              <div className="bookCardDetailed__icon bookCardDetailed__icon--rank">
                <IconsRank />
              </div>
              <div className="bookCardDetailed__label">{avgrate.toFixed(1)}</div>
            </div>
            {(type === 1 || relatedbookid) && (
              <div className="bookCardDetailed__item">
                <div className="bookCardDetailed__icon bookCardDetailed__icon--typeEbook">
                  <IconsEbook />
                </div>
              </div>
            )}
            {(type === 2 || relatedbookid) && (
              <div className="bookCardDetailed__item">
                <div className="bookCardDetailed__icon bookCardDetailed__icon--typeAudiobook">
                  <IconsAudioBook />
                </div>
              </div>
            )}
          </div>
        </div>
      </Link>
    )
  }
}

export default withNamespaces(['lang_route'])(BookGrid)
