import React from 'react'
import BEMHelper from 'react-bem-helper'
import IconsCheckbox from 'newui/components/Icons/Checkbox'

class CheckBox extends React.PureComponent {

  render() {
    const classes = new BEMHelper('checkbox')
    return (
      <div {...classes('', this.props.active)}>
        <IconsCheckbox />
      </div>
    )
  }
}

export default CheckBox
