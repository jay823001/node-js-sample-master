import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

import StickyButton from 'newui/components/StickyButton'
import IconsCheckbox from 'newui/components/Icons/Checkbox'
import IconsCaretRight from 'newui/components/Icons/CaretRight'

class BookListFilter extends React.Component {
  constructor(props) {
    super(props)

    let active = 1

    if (
      props.location.state === 1 ||
      props.location.pathname.includes(props.t('books.categoriesEbooks'))
    ) {
      active = 0
    } 

    this.state = {
      active: active,
      optionList: []
    }

    this.chooseFilter = this.chooseFilter.bind(this)
  }

  getSelectedValue() {
    const temp_stateArray = []
    let ebooks =false
    let audiobooks = false
    let stateValue = 0

    this.state.optionList.forEach((option, key) => {
      const state = key + 1
      temp_stateArray.push({key: state, state: option.active})
    })
    
    temp_stateArray.forEach((option, key) => {     
      if ((key+1 === 1) && option.state) {
        ebooks = option.state
      }
      if ((key+1 === 2) && option.state) {
        audiobooks = option.state
      }
    })

    if (ebooks && audiobooks) {
      stateValue = 0
    } else if (ebooks) {
      stateValue = 1
    } else if (audiobooks) {
      stateValue = 2
    } else {
      stateValue = 0
    }

    return stateValue
  }

  chooseFilter(event, keyVal) {
    if (this.props.location.pathname.includes(this.props.t('books.categoriesEbooks'))) {
      return this.props.history.push({ pathname: this.props.t('books.categoriesAudioBooks') })
    }

    if (this.props.location.pathname.includes(this.props.t('books.categoriesAudioBooks'))) {
      return this.props.history.push({ pathname: this.props.t('books.categoriesEbooks') })
    }    

    this.props.options.forEach((option, key) => {
      if (keyVal===key) {
        if(this.props.options.filter(item => item.active === false).length === 0) {
          option.active = !option.active
        } else if (this.props.options.filter(item => item.active === false).length > 0 && !option.active) {
          option.active = !option.active
        }
        
      }
    })

    this.setState({
      optionList: this.props.options
    })
  }

  startSearch = () => {
    this.props.closeAll()
    return this.props.history.push({
      pathname: this.props.location.pathname,
      search: this.props.location.search,
      state: this.getSelectedValue(),
    })
  }

  componentDidMount() {
    this.setState({
      optionList: this.props.options
    })
  }  

  render() {
    const { type, heading, closeBtn } = this.props;
    
    return (
      <div className="bookListFilter">
        <div className="bookListFilter__group">
          <h2 className="bookListFilter__heading">{heading}</h2>
          <div className="bookListFilter__options">
            <div className="optionList">
              {this.state.optionList.map((item, key) => {
                const linkType = item.forceType || type

                return item.slugname ? (
                  <Link
                    className="optionList__item"
                    to={`${
                      linkType === 1
                        ? this.props.t('books.categoriesEbooks')
                        : this.props.t('books.categoriesAudioBooks')
                      }${item.slugname !== undefined ? `/${item.slugname}` : ''}`}
                    key={key}
                  >
                    <div className="optionList__label">{item.title}</div>
                    <div className="optionList__icon">
                      <IconsCaretRight />
                    </div>
                  </Link>
                ) : (
                    <div
                      className="optionList__item"
                      onClick={event => this.chooseFilter(event, key)}
                      key={key}
                    >
                      <div className="optionList__label">{item.title}</div>
                      <div className="optionList__icon">
                        <div
                          className={`checkbox${
                            item.active ? ' checkbox--active' : ''
                            }`}
                        >
                          <IconsCheckbox />
                        </div>
                      </div>
                    </div>
                  )
              })}
            </div>
          </div>
        </div>
        <div className="bookListFilter__button" onClick={this.startSearch}>
          <StickyButton stickyPhone={true}>
            <div className="btn btn--secondary">{closeBtn}</div>
          </StickyButton>
        </div>
      </div>
    )
  }
}

export default withNamespaces('lang_route')(BookListFilter)
