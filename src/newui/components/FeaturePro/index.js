import React from 'react'
import BEMHelper from 'react-bem-helper'
import IconsOwl from 'newui/components/Icons/Owl'
import IconsTimer from 'newui/components/Icons/Timer'
import IconsDevice from 'newui/components/Icons/Device'

class FeaturePro extends React.PureComponent {

  render() {
    let classes = new BEMHelper('featurePro')
    const { text, iconType } = this.props

    return (
      <div {...classes('')}>
        <div {...classes('icon')}>
          {iconType === 'owl' && <IconsOwl />}
          {iconType === 'timer' && <IconsTimer />}
          {iconType === 'device' && <IconsDevice />}
        </div>
        <div {...classes('label')}> {text}</div>
      </div>
    )
  }
}

export default FeaturePro