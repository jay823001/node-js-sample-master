import React from 'react'
import Brand from 'newui/components/Layout/Brand/index'

class HeaderClean extends React.PureComponent {

  render() {
    return (
      <div className='headerClean'>
        <div className='headerClean__brand'>
          <Brand color='blue' />
        </div>
      </div>
    )
  }
}

export default HeaderClean
