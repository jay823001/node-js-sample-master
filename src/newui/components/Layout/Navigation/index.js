import React from 'react'
import UserModule from 'newui/domain/Modules/User'

import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { withNamespaces } from 'react-i18next'


class Navigation extends React.PureComponent {

  logOut = () => {
    this.props.logoutRequest(true)
  }

  render() {
    const isNotAuthenticated = (!this.props.auth && window.location.pathname !== this.props.t('lang_route:mypages.login'))
    return (
      <div className="navigation">
        <ul className="navigation__list">
          {this.props.t('navigation').map((element, index) => (
            <li className="navigation__item" key={index}>
              {element.link ? (
                <a className="navigation__link" target="_blank" href={element.link} rel="noopener noreferrer">
                  {element.text}
                </a>
              ) : (
                  <Link className="navigation__link" to={this.props.t(element.url_link)}>
                    {element.text}
                  </Link>
                )}
            </li>
          ))}
          {(isNotAuthenticated && this.props.isPaymentRequired) &&
            <li className="navigation__item">
              <button type='button' className="navigation__link" to={this.props.t('lang_route:mypages.login')} onClick={() => this.logOut()}>
                {this.props.t('account_Navigation.logout.text')}
              </button>
            </li>
          }
          {(isNotAuthenticated && !this.props.isPaymentRequired) &&
            <li className="navigation__item">
              <Link className="navigation__link" to={this.props.t('lang_route:mypages.login')}>
                {this.props.t('account_Navigation.login.text')}
              </Link>
            </li>
          }
        </ul>
      </div>
    )
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { logoutRequest: UserModule.actions.logoutRequest },
    dispatch
  )
}

export default withNamespaces(['lang_header'])(connect(null, matchDispatchToProps)(Navigation))