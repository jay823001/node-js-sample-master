import React from 'react'
import BEMHelper from 'react-bem-helper'
import Brand from 'newui/components/Layout/Brand'
import IconsSearch from 'newui/components/Icons/Search'
import IconsProfile from 'newui/components/Icons/Profile'
import Navigation from 'newui/components/Layout/Navigation'

import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import {
  resizeNavigation,
  toggleNavigation
} from 'newui/components/Layout/Responsive/navigation'

class HeaderDefault extends React.PureComponent {

  constructor(props) {
    super(props)

    const nav = resizeNavigation()

    this.state = {
      isNavigationOpen: nav.isNavigationOpen,
      isNavigationShowing: nav.isNavigationShowing
    }

    this.toggleNav = this.toggleNav.bind(this)
  }

  toggleNav() {
    const nav = toggleNavigation()
    this.setState({
      isNavigationOpen: nav.isNavigationOpen,
      isNavigationShowing: nav.isNavigationShowing
    })
  }

  handleResize() {
    const nav = resizeNavigation()
    this.setState({
      isNavigationOpen: nav.isNavigationOpen,
      isNavigationShowing: nav.isNavigationShowing
    })
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize.bind(this))
  }

  render() {
    // TODO: This has to be changed to proper default navbar once all components complemented 
    const classes = new BEMHelper('headerDefault__area')
    const NavToggleclasses = new BEMHelper('headerDefault__navigationToggle')
    return (

      <div className="headerDefault">
        <div {...classes('', 'middle')}>
          <div className="headerDefault__brand">
            <Brand color="blue" />
          </div>
        </div>

        <div {...classes('', 'left')} >
          <div {...NavToggleclasses('', this.state.isNavigationOpen ? 'open' : '')} onClick={this.toggleNav} />
          {this.state.isNavigationShowing &&
            <div className="header-navigation">
              <div className="headerDefault__navigation">
                <Navigation auth={this.props.auth} isPaymentRequired={this.props.isPaymentRequired} />
              </div>
            </div>
          }
        </div>

        <div {...classes('', 'right')}>
          {/* NOTE: This condition has to be changed according to the requirement */}
          {this.props.auth ? (
            <Link className="headerDefault__navigationIcon" to={this.props.t('mypages.account')}>
              <IconsProfile />
            </Link>
          ) : (
              <Link className="headerDefault__navigationIcon" to={{
                pathname: this.props.t('books.search'),
                state: { user: this.props.user }
              }}>
                <IconsSearch />
              </Link>
            )}
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_route'])(HeaderDefault)