import React from 'react'
import BEMHelper from 'react-bem-helper'
import Media from 'newui/components/Media/index'

import { Link } from 'react-router-dom'

class Brand extends React.PureComponent {

  render() {
    const classes = new BEMHelper('brand')
    return (
      <div {...classes('')}>
        <Link className='brand__logo' to='/'>
          <Media src={`shared/logo-nextory-pink.svg`} width='95' height='24' />
        </Link>
      </div>
    )
  }
}

export default Brand
