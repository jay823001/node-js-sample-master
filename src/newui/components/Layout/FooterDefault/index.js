import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import Brand from 'newui/components/Layout/Brand/index'
import IconsFacebook from 'newui/components/Icons/Facebook'
import IconsInstagram from 'newui/components/Icons/Instagram'

class FooterDefault extends React.PureComponent {

  render() {
    const contactUrl = `${this.props.t('lang_footer:contact.linkType')}${this.props.t('lang_footer:contact.number')}`
    return (
      <div className="footerDefault">
        <div className="footerDefault__main">
          <h3 className="footerDefault__support">
            {this.props.t('lang_footer:contact.text')}
            <a className="footerDefault__link" href={contactUrl}> {this.props.t('lang_footer:contact.number')}</a>
          </h3>
          <div className="footerDefault__social">
            <a className="footerDefault__socialItem" href="https://www.nextory.se" target="_blank" rel="noopener noreferrer">
              <IconsFacebook />
            </a>
            <a className="footerDefault__socialItem" href="https://www.nextory.se" target="_blank" rel="noopener noreferrer">
              <IconsInstagram />
            </a>
          </div>
          <div className="footerDefault__navigation">
            {this.props.t('navigation').map((element, index) => (
              this.props.loggedIn && element.auth ? (
                null
              ) : (
                  element.ext_link ? (
                    <a className="footerDefault__navigationItem" key={index} href={element.ext_link} target="_blank" rel="noopener noreferrer">
                      {element.text}
                    </a>
                  ) : (
                      <Link className="footerDefault__navigationItem" key={index} to={this.props.t(element.url_link)}>
                        {element.text}
                      </Link>
                    )

                )
            ))}
          </div>
          <div className="footerDefault__disclaimer">
            <div className="footerDefault__copyright">
              {this.props.t('lang_footer:copyright')}
            </div>
            <div className="footerDefault__links">
              {this.props.t('links').map((element, index) => (
                <Link className="footerDefault__linksItem" key={index} to={this.props.t(element.url_link)}>
                  {element.text}
                </Link>
              ))}
            </div>
          </div>
          <div
            className="footerDefault__relations"
            dangerouslySetInnerHTML={{
              __html: this.props.t('lang_footer:relations',
                {
                  openUrlTag: `<a href='${this.props.t('lang_route:relations')}' target='_blank'>`,
                  closeUrlTag: '</a>'
                })
            }}>

          </div>
          <div className="footerDefault__brand">
            <Brand color="blue" />
          </div>
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_footer'])(FooterDefault)
