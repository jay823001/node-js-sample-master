import * as constants from './constants'

let isNavigationOpen = false
let isNavigationShowing = window.innerWidth >= constants.BREAKPOINTS.tablet

export const resizeNavigation = () => {
  if (window.innerWidth < constants.BREAKPOINTS.tablet) {
    isNavigationOpen = false
    isNavigationShowing = false
  } else {
    isNavigationShowing = true
  }
  return createObject(isNavigationOpen, isNavigationShowing)
}

export const toggleNavigation = () => {
  if (window.innerWidth < constants.BREAKPOINTS.tablet) {
    isNavigationOpen = !isNavigationOpen
    isNavigationShowing = !isNavigationShowing
  }
  return createObject(isNavigationOpen, isNavigationShowing)
}

export const closeNavigation = () => {
  if (window.innerWidth < constants.BREAKPOINTS.tablet) {
    isNavigationOpen = false
    isNavigationShowing = false
  }
  return createObject(isNavigationOpen, isNavigationShowing)
}

function createObject(isNavigationOpen, isNavigationShowing) {
  const data = {
    isNavigationOpen,
    isNavigationShowing
  }
  return data
}