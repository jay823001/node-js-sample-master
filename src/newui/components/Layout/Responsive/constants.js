export const BREAKPOINTS = {
  phone: 320,
  tablet: 676,
  laptop: 920,
  desktop: 1200,
  monitor: 1400,
  projector: 1600
}