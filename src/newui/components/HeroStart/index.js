import React from 'react'
import BEMHelper from 'react-bem-helper'
import Button from 'newui/components/Button'

class HeroStart extends React.PureComponent {

  getMaxColsModifier(value) {
    value = !value ? 6 : value
    return `maxCols${value}`
  }

  render() {
    let classes = new BEMHelper('heroStart')
    const { heading, description, buttons, maxCols, toUrl } = this.props
    const maxColsModifier = this.getMaxColsModifier(maxCols)

    return (
      <div {...classes('', maxColsModifier)} >
        <h1 {...classes('heading')} >{heading}</h1>
        <p {...classes('description')}>{description}</p>
        <div {...classes('actions')}>
          {this.props.buttons && <Button items={buttons} toUrl={toUrl} />}
        </div>
      </div>
    )
  }
}

export default HeroStart