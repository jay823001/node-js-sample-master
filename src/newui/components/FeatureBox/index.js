import React from 'react'
import BEMHelper from 'react-bem-helper'

class FeatureBox extends React.PureComponent {

  getDisclaimerText() {
    if (this.props.bookTitle !== '') {
      return this.props.disclaimer.replace('[BOOK_TITLE]', this.props.bookTitle)
    }
    return this.props.disclaimer
  }

  render() {
    let classes = new BEMHelper('featureBox')
    const { heading, description, items } = this.props
    const setDisclaimerText = this.getDisclaimerText()

    return (
      <div {...classes('')}>
        <h3 {...classes('heading')} > {heading} </h3>
        <p {...classes('description')} > {description} </p>
        <div {...classes('list')} >
          {items.map((item, index) => (
            <div {...classes('item')} key={index}>
              <div {...classes('label', (!item.included && 'notIncluded'))}> {item.text} </div>
            </div>
          ))}

        </div>
        <p {...classes('disclaimer')} > {setDisclaimerText}</p>
      </div >
    )
  }
}

export default FeatureBox