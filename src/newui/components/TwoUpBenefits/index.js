import React from 'react'
import { withNamespaces } from 'react-i18next'

import IconsEbook from 'newui/components/Icons/Ebook'
import IconsAudioBook from 'newui/components/Icons/AudioBook'

class TwoUpBenefits extends React.Component {
  render() {
    return (
      <div className="twoUpBenefits">
        <div className="twoUpBenefits__row">
          <div className="twoUpBenefits__rowItem">
            <div className="twoUpBenefits__header">
              <div className="twoUpBenefits__icon">
                <IconsEbook large />
              </div>
              <h2 className="twoUpBenefits__heading">
                {this.props.t('twoUpBenefits.ebooks.heading')}
              </h2>
            </div>
            <div className="twoUpBenefits__list">
              {this.props.t('twoUpBenefits.ebooks.bullets').map((copyText, key) => (
                <div className="twoUpBenefits__item" key={key}>
                  {copyText}
                </div>
              ))}
            </div>
          </div>
          <div className="twoUpBenefits__rowItem">
            <div className="twoUpBenefits__header">
              <div className="twoUpBenefits__icon">
                <IconsAudioBook large />
              </div>
              <h2 className="twoUpBenefits__heading">
                {this.props.t('twoUpBenefits.audiobooks.heading')}
              </h2>
            </div>
            <div className="twoUpBenefits__list">
              {this.props.t('twoUpBenefits.audiobooks.bullets').map((copyText, key) => (
                <div className="twoUpBenefits__item" key={key}>
                  {copyText}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(TwoUpBenefits)
