import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import SubmitButton from 'newui/components/Button/SubmitButton'

class Buttons extends React.PureComponent {

  static defaultProps = {
    items: {
      text: 'nextory',
      name: '/'
    }
  }

  /*
   **@event: set BEM styles
   **@return: none
   **@parameter description: pre-defined-color-name
   **@parameter type: none
   */
  getstyles(buttonConfigs) {
    if (!buttonConfigs.type) {
      return 'btn--primary'
    }
    return buttonConfigs.type
  }

  /*
  **@event: redirect to my account page
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */
  gotToMyAccount() {
    window.location.href = this.props.t('mypages.myaccount')
  }

  getDefaultUrl = () => {
    if (this.props.items instanceof Array) {
      return this.props.items[0].name
    } else {
      return this.props.items.name
    }
  }

  getButtons = () => {
    if (this.props.items instanceof Array) {
      return this.props.items
    } else {
      let buttons = []
      buttons.push({
        text: this.props.items.text,
        name: this.props.items.name,
        type: this.props.items.type,
        purpose: this.props.items.purpose,
        formname: this.props.items.formName
      })
      return buttons
    }
  }

  logOut = () => {
    this.props.logout()
  }

  render() {
    let toUrl = this.props.toUrl ? this.props.toUrl : this.getDefaultUrl()
    return (
      <div className='formLayout'>
        {this.getButtons().map((item, index) => (
          <div className='formLayout__item' key={index}>
            {/* This is because old application should render previous styles (body, html) which is in globaleStyles.js */}
            {toUrl === this.props.t('mypages.myaccount') ? (
              //eslint-disable-next-line
              <a className={`btn ${this.getstyles(item)}`} onClick={() => this.gotToMyAccount()}>{item.text}</a>
            ) : (

                this.props.logoutbtn ? (
                  <button className={`btn ${this.getstyles(item)}`} onClick={this.logOut}>{item.text}</button>
                ) : (
                    item.purpose && item.purpose === 'submit' ? (
                      <SubmitButton items={item} formName={item.formName} fetching={this.props.fetching ? this.props.fetching : null} />
                    ) : (
                        <Link
                          key={this.props.items.length > 1 ? index : toUrl}
                          to={this.props.items.length > 1 ? item.name : toUrl}
                          className={`btn ${this.getstyles(item)}`}
                        >
                          {item.text}
                        </Link>
                      )
                  )

              )}

          </div>
        ))}
      </div>
    )
  }
}

export default withNamespaces(['lang_route'])(Buttons)
