import React from 'react'

const SubmitButton = (props) => {
  return (
    <div className='formLayout'>
      <div className='formLayout__item'>
        <button
          type={props.items.buttonType}
          className={`btn ${getstyles(props.items)} ${fetching(props.fetching)}`}
        >
          {props.items.text}
        </button>
      </div>
    </div>
  )
}

/*
**@event: set BEM styles
**@return: none
**@parameter description: pre-defined-color-name
**@parameter type: none
*/
function getstyles(buttonConfigs) {
  if (!buttonConfigs.type) {
    return 'primary'
  }
  return buttonConfigs.type
}

/*
**@event: set loading icon
**@return description: loading icon class
**@return type: String
**@parameter description: fetching state
**@parameter type: Boolean
*/
function fetching(fetching) {
  if (fetching) {
    return 'btn--loading disabled'
  } else {
    return ''
  }
}

export default SubmitButton
