import React from 'react'
import BEMHelper from 'react-bem-helper'
import { imageHelper } from 'newui/assets/mixins/ImageHelper'

class Hero extends React.PureComponent {

  setBgImage = (key) => {
    const images = this.props.images
    return {
      backgroundImage: `url(${imageHelper(images[key])}`
    }
  }

  render() {
    let classes = new BEMHelper('hero')
    let { images } = this.props

    return (
      <div {...classes('', this.props.modifiers)}>
        <div {...classes('content')}></div>
        <div {...classes('backdrop')}>
          {
            Object.keys(images).map((key) => (
              <div key={key} {...classes('media', [key])}>
                <div {...classes('image')} style={this.setBgImage(key)} />
              </div>
            ))
          }
        </div>
      </div >
    )
  }
}

export default Hero