import React from 'react'
import BEMHelper from 'react-bem-helper'

class Html extends React.PureComponent {
  render() {
    const classes = new BEMHelper('html')
    return (
      this.props.content ? (
        <div {...classes('', this.props.margin ? 'margin' : 'noMargin')}>
          {this.props.children}
        </div>
      ) : (
          <div {...classes('', this.props.margin ? 'margin' : 'noMargin')}><slot /></div>
        )
    )
  }
}

export default Html
