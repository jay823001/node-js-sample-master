import React from 'react'

export default class Info extends React.PureComponent {

  render() {
    return (
      <svg width='14px' height='14px' viewBox='0 0 14 14'>
        <path d='M7,14 C3.13400675,14 0,10.8659932 0,7 C0,3.13400675 3.13400675,0 7,0 C10.8659932,0 14,3.13400675 14,7 C14,10.8659932 10.8659932,14 7,14 Z M6.307,4.18 L7.693,4.18 L7.693,2.849 L6.307,2.849 L6.307,4.18 Z M6.34,11 L7.66,11 L7.66,5.17 L6.34,5.17 L6.34,11 Z'></path>
      </svg>
    )
  }
}