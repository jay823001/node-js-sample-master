import React from 'react'

class IconsRank extends React.PureComponent {
  render() {
    return (
      <svg width="16px" height="16px" viewBox="0 0 16 16">
        <polygon points="8 12.7146667 3.06 16 4.5 10.0373333 0 6.112 5.84 5.71733333 8 0 10.16 5.71733333 16 6.112 11.5 10.0373333 12.94 16" />
      </svg>
    )
  }
}

export default IconsRank
