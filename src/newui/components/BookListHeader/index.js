import React from 'react'
import { withNamespaces } from 'react-i18next'

import IconsToolMenu from 'newui/components/Icons/ToolMenu'
import IconsToolFilter from 'newui/components/Icons/ToolFilter'

class BookListHeader extends React.Component {
  render() {
    const { heading, hideMenu } = this.props

    return (
      <div className="bookListHeader">
        <div className="bookListHeader__info">
          <h1 className="bookListHeader__heading">{heading}</h1>
        </div>
        {!hideMenu && (
          <React.Fragment>
            <div
              className="bookListHeader__tool bookListHeader__tool--left"
              onClick={this.props.toggleMenu}
            >
              <div className="bookListHeader__toolIcon">
                <IconsToolMenu />
              </div>
              <div className="bookListHeader__toolLabel">
                {this.props.t('bookListHeader.category')}
              </div>
            </div>
            <div
              className="bookListHeader__tool bookListHeader__tool--right"
              onClick={this.props.toggleFilter}
            >
              <div className="bookListHeader__toolIcon">
                <IconsToolFilter />
              </div>
              <div className="bookListHeader__toolLabel">
                {this.props.t('bookListHeader.format')}
              </div>
            </div>
          </React.Fragment>
        )}
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(BookListHeader)
