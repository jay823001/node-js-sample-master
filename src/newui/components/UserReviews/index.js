import React from 'react'
import BEMHelper from 'react-bem-helper'
import IconsStar from 'newui/components/Icons/Star'

class UserReviews extends React.PureComponent {

  render() {
    let classes = new BEMHelper('userReviews')
    const { items, description } = this.props

    return (
      <div {...classes('')}>
        {items.map((item, index) => (
          <div {...classes('list')} key={index}>
            <div {...classes('item', (index === 0 && 'active'))} >
              <div {...classes('rank')}>
                < div {...classes('rankItem')}>
                  <IconsStar />
                </div>
                <div {...classes('rankItem')}>
                  <IconsStar />
                </div>
                <div {...classes('rankItem')}>
                  <IconsStar />
                </div>
                <div {...classes('rankItem')}>
                  <IconsStar />
                </div>
                <div {...classes('rankItem', 'rankItem--disabled')}>
                  <IconsStar />
                </div>
              </div>
              <div {...classes('text')}> {item.text}</div>
              <div {...classes('name')}> {item.name} </div >
            </div>
          </div>
        ))}

        <div {...classes('pagination')} >
          {items.map((item, index) => (
            <div {...classes('paginationItem', (index === 0 && 'active'))} key={index} />
          ))}
        </div>
        <p {...classes('description')} >{description}</p>

      </div >
    )
  }
}

export default UserReviews