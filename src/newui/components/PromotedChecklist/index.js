import React from 'react'
import { withNamespaces } from 'react-i18next'

import IconsCheckmark from 'newui/components/Icons/Checkmark'

class PromotedChecklist extends React.Component {
  render() {
    return (
      <div className="promotedChecklist">
        <h2 className="promotedChecklist__heading">{this.props.t('promotedChecklist.heading')}</h2>
        <div className="promotedChecklist__list">
          {this.props.t('promotedChecklist.bullets').map((copyText, key) => (
            <div className="promotedChecklist__item" key={key}>
              <div className="promotedChecklist__icon">
                <IconsCheckmark />
              </div>
              <div className="promotedChecklist__label">{copyText}</div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(PromotedChecklist)
