import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

import BookCard from 'newui/components/BookCard'
import IconsCaretRight from 'newui/components/Icons/CaretRight'

class BookGroup extends React.Component {
  render() {
    const { type, link, books, heading } = this.props

    return (
      <div className="bookGroup">
        <div className="bookGroup__header">
          <div className="bookGroup__heading">{heading}</div>
          {link && (
            <div className="bookGroup__link">
              <Link
                className="arrowLink"
                to={`${
                  type === 1
                    ? this.props.t('lang_route:books.categoriesEbooks')
                    : this.props.t('lang_route:books.categoriesAudioBooks')
                  }/${link}`}
              >
                <div className="arrowLink__label">{this.props.t('bookGroup.showAll')}</div>
                <div className="arrowLink__icon">
                  <IconsCaretRight />
                </div>
              </Link>
            </div>
          )}
        </div>

        <div className="bookGroup__main">
          <ul className="bookGroup__list">
            {books.map((item, key) => (
              <li className="bookGroup__item" key={key}>
                <BookCard book={item} />
              </li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

export default withNamespaces(['lang_books'])(BookGroup)
