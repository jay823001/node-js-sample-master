import React from 'react'
import 'newui/assets/styles/main.scss'
import Section from 'newui/components/Section'
import MainApp from 'newui/containers/Views/index'
import PageHeader from 'newui/components/PageHeader'
import PaymentOption from 'newui/components/PaymentOption/Checkout'

import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import { getDynamicText } from 'newui/utils/translationHelper'
import { getTrialExpiryDate, IdentifyFlow } from 'newui/utils/helperFunctions'

class CheckoutOptions extends React.PureComponent {

  /*
  **@event: check authentication
  **@return description: true false depending on loggedIn status
  **@return type: boolean
  **@parameter description: none
  **@parameter type: none
  */

  getExpireDate = () => {
    if (this.props.flow === 'change-payment') {
      return this.props.userDetails.subscriptiondetails.nextrundate
    } else {
      return null
    }
  }

  render() {
    const authCheck = this.props.registrationKeys.isAuthenticated
    const TrialExpiryDate = getTrialExpiryDate()
    let paymentType = null
    if (this.props.option.state) {
      localStorage.setItem('paymentType', this.props.option.state.paymentType)
      paymentType = this.props.option.state.paymentType
    } else {
      paymentType = localStorage.getItem('paymentType')
    }
    return (
      <React.Fragment>

        {this.props.option.state === undefined && !paymentType ? (
          <Redirect to={this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.checkoutOptions`)} />
        ) : (
            <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
              <Section maxCols={8} spacing={false}>
                <PageHeader
                  label={this.props.flow !== 'change-payment' ? this.props.t(`register.${IdentifyFlow(this.props.flow)}.paymentOption.label`) : null}
                  heading={this.props.t('register.trialFlow.paymentOption.heading')}
                  description={getDynamicText(this.props, 'payment_reassurance_step2', this.getExpireDate())}
                />
                <PaymentOption
                  trialexpirydate={TrialExpiryDate}
                  bgColor={this.props.bgColor}
                  paymentType={paymentType} auth={authCheck}
                  header={this.props.header}
                  flow={this.props.flow}
                  campaignInfo={this.props.campaignInfo}
                />
              </Section>
            </MainApp>
          )
        }
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    registrationKeys: state.authKeys,
    isLoggedIn: state.account.loggedIn,
    isMember: state.account.userData.ismember,
    activeRegistration: state.signup.activeReg,
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_register'])(connect(mapStateToProps)(CheckoutOptions))
