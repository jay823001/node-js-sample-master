import { verifyCreditCardNumber } from 'newui/components/PaymentOption/PaymentFunctions/CreditCard.LuhnCheck'

const validate = values => {
  const errors = {}
  if (!values.cardnumber) {
    errors.cardnumber = 'PaymentForm.cardnumber.invalid'
  } else if (values.cardnumber.length !== 19) {
    errors.cardnumber = 'PaymentForm.cardnumber.invalid'
  } else if (verifyCreditCardNumber(values.cardnumber) === false) {
    errors.cardnumber = 'PaymentForm.cardnumber.unreal'
  }

  if (values.month === '0' || !values.month || values.month === 'month') {
    errors.month = 'PaymentForm.month'
  }

  if (values.year === '0' || !values.year) {
    errors.year = 'PaymentForm.year'
  }

  if (!values.cvv) {
    errors.cvv = 'PaymentForm.cvv.number'
  } else if (values.cvv.length < 3 || values.cvv.length > 4) {
    errors.cvv = 'PaymentForm.cvv.digits'
  }

  return errors
}

export default validate
