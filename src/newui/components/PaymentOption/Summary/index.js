import React from 'react'

import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import { IdentifyFlow } from "newui/utils/helperFunctions"
import { getDynamicText } from 'newui/utils/translationHelper'
import { SubscriptionConfig } from 'newui/components/SubscriptionTable/SubscriptionOptions/SubscriptionConfig'

class Summary extends React.PureComponent {

  /*
  **@event: Create object to send to trnaslation file
  **@return description: all the props and selected subscription price
  **@return type: Object
  **@parameter description: price of subscription 
  **@parameter type: number
  */
  createOfferObject = (subscriptioprice) => {
    return {
      props: this.props,
      subscriptionPrice: this.getFormattedPrice(subscriptioprice)
    }
  }

  getFormattedPrice = (price) => {
    const pricelabel = price.toString()
    return pricelabel.substr(0, price.indexOf(' '))
  }

  getFullOffer = (subscriptioprice) => {
    if (this.props.content.flow === 'trial') {
      return <span>({getDynamicText(this.createOfferObject(subscriptioprice), 'full_offer_step2')})</span>
    } else if (this.props.content.flow === 'campaign') {
      if (this.props.content.campaignInfo.type === 'DISCOUNT') {
        if (this.props.content.campaignInfo.free_days > 0) {
          return null
        } else {
          return <span>({getDynamicText(this.createOfferObject(subscriptioprice), 'full_offer_step2')})</span>
        }
      } else {
        return <span>({getDynamicText(this.createOfferObject(subscriptioprice), 'full_offer_step2')})</span>
      }
    } else if (this.props.content.flow === 're-activate') {
      return <span>({getDynamicText(this.createOfferObject(subscriptioprice), 'full_offer_step2')})</span>
    } else {
      // default to be trial
      return <span>({getDynamicText(this.createOfferObject(subscriptioprice), 'full_offer_step2')})</span>
    }
  }

  getchangeSubscriptionLink = () => {
    if (this.props.content.flow === 'trial' || this.props.content.flow === 'campaign') {
      return this.props.t(`lang_route:registration.${IdentifyFlow(this.props.content.flow)}.subscriptionBasic`)
    } else {
      return this.props.t(`lang_route:mypages.${IdentifyFlow(this.props.content.flow)}.subscriptionBasic`)
    }
  }

  render() {
    const subscriptionIndex = this.props.selectedSubscription.index
    let packageDetails = SubscriptionConfig(this.props)
    let subscription
    let subscriptionName = ''
    let subscriptionPrice = 0
    if (this.props.selectedSubscription.isfamily) {
      subscription = packageDetails.subscriptionOptionsFamily.items[subscriptionIndex]
      subscriptionName = `${subscription.category},  ${subscription.heading}  ${subscription.label}`
      subscriptionPrice = subscription.details
    } else {
      subscription = packageDetails.subscriptionOptionsBasic
      subscriptionName = subscription.models[subscriptionIndex].name
      subscriptionPrice = subscription.price.values[subscriptionIndex]
    }

    return (
      <div className='paymentOptionSummary'>
        <p className='paymentOptionSummary__heading'>
          {this.props.t('register.trialFlow.paymentOption.summary.subscriptionPrice.label')}
        </p>
        <div className='paymentOptionSummary__info'>
          <div className='paymentOptionSummary__text'>
            <span>{subscriptionName}</span>
            {getDynamicText(this.createOfferObject(subscriptionPrice), 'full_offer_step1')}
            {this.getFullOffer(subscriptionPrice)}
          </div>
          <Link
            className='paymentOptionSummary__link'
            to={this.getchangeSubscriptionLink()}
          >
            {this.props.t('register.trialFlow.paymentOption.summary.subscriptionPrice.link.text')}
          </Link>
        </div>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    selectedSubscription: state.selectedSubscription
  }
}
export default withNamespaces(['lang_register'])(connect(mapStateToProps)(Summary))
