import React from 'react'

import { imageHelper } from 'newui/assets/mixins/ImageHelper'

class ProviderIcons extends React.PureComponent {
  render() {
    return (
      <div className='paymentOption__providerList'>
        {this.props.paymentList.map((paymentType) => (
          paymentType.paymenttype === this.props.paymentType &&
          paymentType.providers.map((provider, index) =>
            <img
              className='paymentOption__providerItem'
              src={imageHelper(`payment/logo-${provider}.png`)}
              alt={`${provider}-logo`}
              key={index}
            />
          )))}
      </div>
    )
  }
}

export default ProviderIcons

