import React from 'react'
import IconsLock from 'newui/components/Icons/Lock'

class Disclaimer extends React.PureComponent {
  render() {
    return (
      <div className='paymentOption__disclaimer'>
        <div className='paymentOption__disclaimerIcon'>
          <IconsLock />
        </div>
        <div className='paymentOption__disclaimerText'>
          {this.props.disclaimer}
        </div>
      </div>
    )
  }
}

export default Disclaimer
