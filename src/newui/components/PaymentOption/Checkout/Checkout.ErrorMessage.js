
import React from 'react'

class ErrorMessage extends React.PureComponent {
  render() {
    return (
      this.props.isPaymentError &&
      <span className='text-has-error card_error'>
        {this.props.preSubmitError}
        <ul className='credit_card_error_list'>
          {this.props.errorBullets.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      </span>
    )
  }
}

export default ErrorMessage

