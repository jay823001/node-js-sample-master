export const CheckoutConfig = (props, originKey, environment) => {
  return {
    "form": {
      "adyenConfig": {
        "configObject": {
          "originKey": originKey
        },
        "context": environment,
        "rootNode": ".form-div",
        "paymentMethods": {
          "card": {
            "sfStyles": "",
            "placeholders": {
              "encryptedCardNumber": props.t('creditCard.form.cardNumber.items.placeholder'),
              "encryptedExpiryDate": props.t('creditCard.form.cardExpiry.placeholder'),
              "encryptedSecurityCode": props.t('creditCard.form.cardCvv.placeholder')
            }
          }
        }
      },
      "cvvInstructions": {
        "text": props.t('creditCard.form.cvvInstructions.text'),
        "button": props.t('creditCard.form.cvvInstructions.button')
      },
      "cardNumber": {
        "errors": {
          "unreal": props.t('lang_error:PaymentForm.cardnumber.unreal'),
          "invalid": props.t('lang_error:PaymentForm.cardnumber.invalid')
        },
        "items": {
          "component": "input",
          "name": "cardnumber",
          "placeholder": props.t('creditCard.form.cardNumber.items.placeholder'),
          "type": "tel",
          "normalize": "cardNumber"
        }
      },
      "cardMonth": {
        "label": props.t('creditCard.form.cardMonth.label'),
        "name": "month",
        "placeholder": props.t('creditCard.form.cardMonth.placeholder'),
        "errors": {
          "month": props.t('lang_error:PaymentForm.month')
        }
      },
      "cardYear": {
        "label": props.t('creditCard.form.cardYear.label'),
        "name": "year",
        "placeholder": props.t('creditCard.form.cardYear.placeholder'),
        "errors": {
          "year": props.t('lang_error:PaymentForm.year')
        }
      },
      "cardExpiry": {
        "label": props.t('creditCard.form.cardExpiry.label'),
        "placeholder": props.t('creditCard.form.cardExpiry.placeholder'),
        "items": [
          {
            "text": props.t('creditCard.form.cardExpiry.items.text')
          }
        ]
      },
      "cardCvv": {
        "label": props.t('creditCard.form.cardCvv.label'),
        "placeholder": props.t('creditCard.form.cardCvv.placeholder'),
        "name": "cvv",
        "errors": {
          "number": props.t('lang_error:PaymentForm.cvv.number'),
          "digits": props.t('lang_error:PaymentForm.cvv.digits')
        },
        "items": {
          "component": "input",
          "name": "cvv",
          "type": "tel",
          "maxLength": "3",
          "normalize": "cardCvv"
        }
      }
    }
  }
}