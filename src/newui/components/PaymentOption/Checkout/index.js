import React from 'react'
import Media from 'newui/components/Media'
import UserModule from 'newui/domain/Modules/User'
import BoxWrapper from 'newui/components/BoxWrapper'
import StickyButton from 'newui/components/StickyButton'
import SubmitButton from 'newui/components/Button/SubmitButton'
import Summary from 'newui/components/PaymentOption/Summary'
import Disclaimer from 'newui/components/PaymentOption/Checkout/Checkout.Disclaimer'
import ErrorMessage from 'newui/components/PaymentOption/Checkout/Checkout.ErrorMessage'
import ProviderIcons from 'newui/components/PaymentOption/Checkout/Checkout.ProviderIcons'
import CreditCardForm from 'newui/components/PaymentOption/Checkout/Checkout.CreditCardForm'
import validate from 'newui/components/PaymentOption/PaymentFunctions/CreditCard.Validate'

import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import { CheckoutConfig } from 'newui/components/PaymentOption/Checkout/Checkout.Config'
import { checkoutOptionsConfig } from 'newui/containers/Views/Common/Checkout/CheckoutOptions.Config'
import {
  IdentifyFlow,
  getOriginKey,
  IdentifyCampaign,
  processAdyenError,
  getAppEnvironment,
  getSubscriptionId,
  setButtonProperties
} from 'newui/utils/helperFunctions'

const createPaymentRequest = UserModule.actions.createPaymentRequest

class PaymentOption extends React.PureComponent {

  constructor(props) {
    super(props)
    this.initializeSecuredFields = this.initializeSecuredFields.bind(this)
  }

  state = {
    cvvInstructionsActive: false,
    encryptedValues: {},
    enableSubmitBtn: false,
    errorDetails: {}
  }

  getCheckoutOptionsUrl() {
    if (this.props.flow === 're-activate') {
      return this.props.t('lang_route:mypages.reActivationFlow.checkoutOptions')
    } else if (this.props.flow === 'change-payment') {
      return this.props.t('lang_route:mypages.checkoutOptions')
    } else {
      return this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.checkoutOptions`)
    }
  }

  // This submit function is common for All payment types including Adyen and trustly
  submit = (values) => {
    const subscriptionId = getSubscriptionId(this.props.selectedSubscription)
    if ((this.state.errorDetails !== null && this.state.enableSubmitBtn) || this.props.paymentType !== 'ADYEN') {
      const data = {
        //gcode: '', // provide giftcode for giftcard flow, else do not send this value
        subscriptionid: this.props.flow === 'change-payment' ? this.props.userDetails.subscriptiondetails.subscriptionid : subscriptionId,
        encryptedCardNumber: values.encryptedCardNumber,
        encryptedExpiryMonth: values.encryptedExpiryMonth,
        encryptedExpiryYear: values.encryptedExpiryYear,
        encryptedSecurityCode: values.encryptedSecurityCode,
        paymentgateway: this.props.paymentType,
        authkey: this.props.registrationKeys.authkey,
        route: this.getCheckoutOptionsUrl(),
        paymenttype: this.getPaymentType(),
        successURL: this.getsuccessUrl(),
        flow: this.props.flow,
        campaign: this.props.flow === 'campaign' ? this.props.campaignInfo : null
      }
      this.props.dispatch(createPaymentRequest(data))
    }
  }

  getPaymentType() {
    switch (this.props.flow) {
      case 'trial':
        return 'TRAIL'
      case 'campaign':
        return 'REDEEM_CAMPAIGN'
      case 're-activate':
        return 'PURCHASE_SUBSCRIPTION'
      default:
        return 'TRAIL'
    }
  }

  getsuccessUrl = () => {
    if (this.props.flow === 'trial' || this.props.flow === 'campaign') {
      return this.props.t('lang_route:mypages.userDetails')
    } else if (this.props.flow === 'change-payment') {
      return this.props.t('lang_route:mypages.paymentSuccess')
    } else if (this.props.flow === 're-activate') {
      return this.props.t('lang_route:mypages.account')
    } else {
      return this.props.t('lang_route:mypages.getStarted')
    }
  }

  /*
  **@event: toggle hint for card cvv
  **@return description: none
  **@return type: none
  **@parameter description: true false depending on show hide of hint
  **@parameter type: boolean
  */
  toggleccvInstructions(key) {
    this.setState({
      cvvInstructionsActive: key
    })
  }

  /*
  **@event: get subscription details selected by user
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */

  getButtonProperties = () => {
    let text = this.props.t(`register.${IdentifyFlow(this.props.flow)}.paymentOption.buttons.text`)
    let url = this.props.t('lang_route:mypages.userDetails')
    if (this.props.flow === 'campaign') {
      text = this.props.t(`register.${IdentifyFlow(this.props.flow)}.${IdentifyCampaign(this.props.campaignInfo)}.paymentOption.buttons.text`)
    } else if (this.props.flow === 're-activate') {
      url = this.props.t('lang_route:mypages.getStarted')
    } else if (this.props.flow === 'change-payment') {
      text = this.props.t(`lang_myPages:paymentDetails.checkout.savePayment`)
    }

    return {
      url: url,
      text: text,
      className: 'btn--primary btn--arrow'
    }
  }

  setAdyenScripts = () => {
    const environment = getAppEnvironment()
    const originKey = getOriginKey(this.props.subscriptionInfo)
    const formDetails = CheckoutConfig(this.props, originKey, environment).form
    return window.csf(formDetails.adyenConfig)
  }

  initializeSecuredFields = () => {
    let encryptedValues = []
    const that = this
    const securedFields = this.setAdyenScripts()
    const sendValues = function (encryptedValues) {
      Object.keys(encryptedValues).length === 4 && that.setState({ encryptedValues: encryptedValues })
    }

    securedFields.onFieldValid(function (responseField) {
      if (responseField.valid) {
        encryptedValues = { ...encryptedValues, [responseField.encryptedFieldName]: responseField.blob }
        sendValues(encryptedValues)
      }
    })

    securedFields.onAllValid(function (response) {
      let enableSubmitBtn = response.allValid ? true : false
      that.setState({ enableSubmitBtn })
    })

    securedFields.onError(function (errorObject) {
      let errorDetails = processAdyenError(errorObject, that.props)
      that.setState({ ...errorDetails, errorDetails: errorDetails })
    })

  }

  componentDidMount() {
    if (this.props.paymentType === 'ADYEN') {
      !window.location.href.includes('?paymentstatus=failed') && this.initializeSecuredFields()
    }    
  }

  showCvvInstructions = () => {
    const formDetails = CheckoutConfig().form
    return (
      <div className={`paymentOptionCreditCard__overlay paymentOptionCreditCard__overlay--active`} >
        <div className='paymentOptionCreditCard__image'>
          <Media
            src='payment/cvv-instructions.png'
            width='230'
            height='150' />
        </div>
        <p className='paymentOptionCreditCard__instructions'>
          {formDetails.cvvInstructions.text}
        </p>
        <div
          className='paymentOptionCreditCard__button'
          onClick={() => this.toggleccvInstructions(false)}
        >
          <div className='btn btn--secondary'>
            {formDetails.cvvInstructions.button}
          </div>
        </div>
      </div>
    )
  }

  render() {
    const { handleSubmit } = this.props
    const formDetails = CheckoutConfig(this.props).form
    const paymentTypes = checkoutOptionsConfig(this.props).paymenttypes
    return (

      <div className='form-div'>
        {/* See Adyen Guide Here: https://docs.adyen.com/developers/checkout/api-integration/build-your-own-payment-form#nextsteps */}
        {this.props.registrationKeys.authkey === '' && <Redirect to={this.props.t('lang_route:registration.trialFlow.createAccount')} />}

        <form className='payment-div' onSubmit={handleSubmit(() => this.submit(this.state.encryptedValues))}>
          <BoxWrapper maxCols={5}>
            <div className='paymentOption'>
              <div className='paymentOption__info'>

                {/* Payment provider icons */}
                <ProviderIcons paymentList={paymentTypes} paymentType={this.props.paymentType} />

                { /* Show Adyen or other payment types based on chosen paymenttype */
                  this.props.paymentType === 'ADYEN' ? (
                    <div className='paymentOption__form'>
                      <div className='paymentOptionCreditCard'>
                        {/* show/hide Cvv instructions */}
                        {this.state.cvvInstructionsActive && this.showCvvInstructions(formDetails)}
                        <CreditCardForm
                          hasCardError={this.state.errorDetails.card}
                          hasExpiryDateError={this.state.errorDetails.expiryDate}
                          hasCvcError={this.state.errorDetails.cvcNumber}
                        />
                      </div>
                    </div>

                  ) : ( /* non Adyen payment types */
                      < div className='paymentOption__instructions'>
                        {this.props.t('register.trialFlow.paymentOption.instructions')}
                      </div>
                    )
                }

                {/* Disclaimer */}
                <Disclaimer disclaimer={this.props.t('register.trialFlow.paymentOption.disclaimer')} />

              </div>

              {/* Summary */}
              {this.props.flow !== 'change-payment' &&
                <div className='paymentOption__summary'>
                  <Summary content={this.props} />
                </div>
              }

            </div>

            {/* error messages*/}
            <ErrorMessage
              isPaymentError={this.props.paymentCheckout.paymentError}
              preSubmitError={this.props.t('lang_error:PaymentForm.error.presubmit')}
              errorBullets={this.props.t('lang_error:PaymentForm.error.bullets')}
            />

          </BoxWrapper>

          {/* form submit button */}
          <StickyButton bgColor={this.props.bgColor} stickyPhone={true} stickyDesktop={true}>
            <SubmitButton items={setButtonProperties(this.getButtonProperties())} fetching={this.props.paymentCheckout.fetching} formName='CreditCardPaymentForm' />
          </StickyButton>

        </form>
      </div >
    )
  }
}

function mapStateToProps(state) {
  return {
    registrationKeys: state.authKeys,
    fetching: state.fetching,
    subscriptionInfo: state.subscriptionInfo,
    paymentCheckout: state.paymentCheckout,
    userDetails: state.userDetails.userInfo,
    selectedSubscription: state.selectedSubscription
  }
}

export default withNamespaces(['lang_register'])(connect(mapStateToProps, { createPaymentRequest })(
  reduxForm({
    form: 'CreditCardPaymentForm',
    validate,
    onSubmit: validate,
  })(PaymentOption))
)