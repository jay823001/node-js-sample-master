import React from 'react'
import FormOption from 'newui/components/FormOption'

class CreditCardForm extends React.PureComponent {

  render() {
    let highlightCardError = this.props.hasCardError ? 'input-has-error' : ''
    let highlightExpiryDateError = this.props.hasExpiryDateError ? 'input-has-error' : ''
    let highlightCvcError = this.props.hasCvcError ? 'input-has-error' : ''
    return (
      <div className='paymentOptionCreditCard__form'>
        <input type='hidden' name='txvariant' value='card' />

        <div className='formLayout'>
          <div className='formLayout__item'>
            <FormOption>
              <div className='formInput'>
                <span
                  className={`input-field formInput__field formInput__field--fixedHeight ${highlightCardError}`}
                  data-hosted-id='hostedCardNumberField' data-cse='encryptedCardNumber'></span>
                <span className='text-has-error'>{this.props.hasCardError}</span>
              </div>
            </FormOption>
          </div>
          <div className='formLayout__item formLayout__item--50'>
            <FormOption>
              <div className='formSelect'>
                <span className={`input-field formSelect__field formInput__field--fixedHeight ${highlightExpiryDateError}`}
                  data-hosted-id='hostedExpiryDateField' data-cse='encryptedExpiryDate'></span>
                <span className='text-has-error'>{this.props.hasExpiryDateError}</span>
              </div>
            </FormOption>
          </div>
          <div className='formLayout__item formLayout__item--50'>
            <FormOption>
              <div className='formInput'>
                <span className={`input-field formInput__field formInput__field--fixedHeight ${highlightCvcError}`}
                  data-hosted-id='hostedSecurityCodeField' data-cse='encryptedSecurityCode'></span>
                <span className='text-has-error'>{this.props.hasCvcError}</span>
              </div>
            </FormOption>
          </div>
        </div>
      </div>
    )
  }
}

export default CreditCardForm