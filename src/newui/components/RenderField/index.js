import React from 'react'
import { withNamespaces } from 'react-i18next'

const renderField = (props) => {
  const {
    input,
    label,
    type,
    placeholder,
    maxLength,
    pattern,
    meta: {
      touched,
      error
    }
  } = props
  return (
    <div>
      <label>{label}</label>
      <div>
        <input {...input}
          type={type}
          maxLength={maxLength}
          placeholder={placeholder}
          pattern={pattern}
          className={`formInput__field ${touched && error ? 'input-has-error' : ''}`} />
        {touched && error && <span className='text-has-error'>{props.t(error)}</span>}
      </div>
    </div>
  )
}

export default withNamespaces(['lang_error'])(renderField)
