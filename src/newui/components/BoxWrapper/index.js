import React from 'react'
import BEMHelper from 'react-bem-helper'

class BoxWrapper extends React.PureComponent {

  static defaultProps = {
    modifiers: [],
    heading: '',
    maxCols: 6
  }

  /*
  **@event: set box column size
  **@return description: colum size
  **@return type: String
  **@parameter description: none
  **@parameter type: none
  */
  maxColsModifier() {
    return `maxCols${this.props.maxCols}`
  }

  concatModifiers = () => {

    let concatModifiersArray = []
    if (this.props.modifiers.length > 0) {
      this.props.modifiers.forEach(element => {
        concatModifiersArray.push(element)
      })
    }
    let maxCols = this.maxColsModifier()
    concatModifiersArray.push(maxCols)
    return concatModifiersArray
  }

  render() {

    const classes = new BEMHelper('boxWrapper')
    const heading = this.props.heading
    return (
      <div {...classes('', this.concatModifiers())}>
        <h2 className='boxWrapper__heading'>{heading}</h2>
        <div className='boxWrapper__content'>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default BoxWrapper
