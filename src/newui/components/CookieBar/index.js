import React from 'react'

import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import {
  cookies,
  setCookies
} from 'newui/utils/helperFunctions'

class CookieBar extends React.PureComponent {

  state = {
    isCookiesAccepted: false
  }

  acceptCookies = () => {
    this.setState({ isCookiesAccepted: true })
    setCookies('acceptedTerms', 'true', { path: '/', maxAge: 1209600 })
  }

  componentDidMount() {
    cookies.get('acceptedTerms') && this.setState({ isCookiesAccepted: true })
  }

  render() {
    return (
      !this.state.isCookiesAccepted && (
        <div className='cookiebar'>
          <div className='cookiebar__info'>
            <div className='cookiebar__description'>{this.props.t('cookies.text')}</div>
            <Link className='cookiebar__link' to={this.props.t('lang_route:cookies')} >{this.props.t('cookies.readMore.text')}</Link>
          </div>
          <div className='cookiebar__button'>
            <button type='button' className='btn btn--secondary btn--wide' onClick={this.acceptCookies.bind(this)} >{this.props.t('cookies.button.text')}</button>
          </div>
        </div>
      )
    )
  }
}

export default withNamespaces('lang_common')(CookieBar)