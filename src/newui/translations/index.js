import en_GB from 'newui/translations/english'
import sv_SE from 'newui/translations/swedish'
import de_DE from 'newui/translations/german'
import fi_FI from 'newui/translations/finnish'

const languageArray = {
  'en_GB': en_GB,
  'sv_SE': sv_SE,
  'de_DE': de_DE,
  'fi_FI': fi_FI
}

let Languages = {}

Object.entries(languageArray).forEach(function (item) {
  Languages[`${item[0]}`] =
    {
      lang_books: item[1].lang_books,
      lang_register: item[1].lang_register,
      lang_error: item[1].lang_error,
      lang_route: item[1].lang_route,
      lang_myPages: item[1].lang_myPages,
      lang_header: item[1].lang_header,
      lang_footer: item[1].lang_footer,
      lang_common: item[1].lang_common,
      lang_faq: item[1].lang_faq,
      lang_homepage: item[1].lang_homepage,
      lang_bookRainbow: item[1].lang_bookRainbow,
      lang_giftCard: item[1].lang_giftCard
    }
})

export default Languages