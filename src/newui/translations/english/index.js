/** English **/
import registration from './registration.json'
import errors from './errors.json'
import routes from './routes.json'
import myAccount from './myAccount.json'
import header from './header.json'
import footer from './footer.json'
import common from './common.json'
import books from './books.json'
import faq from './faq.json'
import homepage from './homepage.json'
import bookList from './bookList.json'

const en_GB = {
  lang_books: books,
  lang_register: registration,
  lang_error: errors,
  lang_route: routes,
  lang_myPages: myAccount,
  lang_header: header,
  lang_footer: footer,
  lang_common: common,
  lang_faq: faq,
  lang_homepage: homepage,
  lang_bookRainbow: bookList
}

export default en_GB