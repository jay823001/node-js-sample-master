/**** API VERSION TO BE UTILIZED ****/
const ApiVersion = '5.6'
const ApiBooksVersion = '6.0'

/**** The top level domain => se, fi, de etc... */
const tld = window.location.origin.split('.').pop()

const Config = {
  /**** API TO BE UTILIZED ****
   * 0 => auto
   * 1 => development
   * 2 => production
   */
  current_api: 0,
  api: {
    /**** DEVELOPMENT API SETTINGS ****/
    development: {
      environment: 'Development',
      apiUrl: `/api/web/${ApiVersion}/`, // Set api version above
      apiBooks: `/api/web/product/${ApiBooksVersion}/`,
      adyenKey:
        '10001|8E8A9925D9CD9C1E3D3508AF09A58163A722DE04DEAD0CFCB145B765ED9C719132D99421FA08C476D0E1033A730FD9211CC36CDB4355226EF228E65C30C9EA22C5EC1183B006B221A6704559E2F9B02BFF2BDB7E7278BD778EE28A7E870D51E5CCF473002B06A5AFC250AC1CA4911784E7FED2A65B17098388E1404C6F73268317A04A0251F6D1D75D2B3455F112CE92364432AAC460342CEDEB375CAE7ABFE5A66ED9157C3D2724B116DAC8F39ED1EDE20758447512633E2C39753EFBBEBB647C857600B1938260833A0053E5FE9297867DA8F5B84D4C20694AE61A7F3CFB3417D09D4C56BB31F6E2E2E8A4C64A605BE0EE60B9ADF9E1CDED22C931000D72B1',
      smallCover: 'http://104.155.208.232:9190/coverimg/130/',
      largeCover: 'http://104.155.208.232:9190/coverimg/274/',
    },

    /**** PRODUCTION API SETTINGS ****/
    production: {
      environment: 'Production',
      apiUrl: `https://www.nextory.${tld}/api/web/${ApiVersion}/`,
      apiBooks: `https://www.nextory.${tld}/api/web/product/${ApiBooksVersion}/`,
      adyenKey:
        '10001|DB7C97720201F9ABE9A580275D98D33EAE1C9D2CA8D73468D0E83B4624DA567E2D14A3696B44A67783DEA8C12E4EA36A3A927AA0959F1C50DB04BB17E3896AC9253930CF06939E4345E6F06331A4E8612A83A2A1B739D7DFBFCFF710EFB2DA4F1505B1D58E9FB0D0CBB74556B6372169819E9ABCD4B7632B42CF5F2A14142EA7F171FABB0CE7589863DDB14C12BAE316C6E1DAD660CB231C2F6888CBC7A1BD66AC68F5D58DA38C88D121E8F83BABE08FE66C0B8168B8D8C04FE8AF577714843C61EEEED2FD7C17C56DC28C426B702B82D1B642EC1B9A71E1F92998122D2246919D8E3BEF4F164AF17DC89FDB557F0B766A2742540CD2EDBDBAC769F9FD896481',
      smallCover: `https://www.nextory.${tld}/coverimg/130/`,
      largeCover: `https://www.nextory.${tld}/coverimg/274/`
    }
  },

  /**** LOCALE ****
   * sv_SE => Swedish
   * en_GB => English
   * de_DE => German
   * fi_FI => Finnish  // Translations not yet added
   *
   * * NOTE:
   *      - This setting is mandatory for localhost and will effect only localhost.
   *      - getLocaleBasedOnHost() in utils/helperfunctions will auto determine the locale based on host for production and acceptance
   *      - The english locale must be corrected in helperfuctions when english is to be supported
   *      - Any new locale must be added to the above list and to getLocaleBasedOnHost()
   */
  locale: 'sv_SE',

  /**** DEFAULT BASIC SUBSCRIPTION ON LOAD ****
   * Gold => 0
   * Family => 2
   * Silver => 1
   */
  default_basic_subscription: 2,

  /**** DEFAULT FAMILY SUBSCRIPTION ON LOAD ****
   * Family2 => 0
   * Family3 => 1
   * Family4 => 2
   */
  default_family_subscription: 2,

  /**** BASIC AUTH KEY FOR API REQUESTS ****/
  fetchAuth: 'Basic bmV4dG9yeXVpOnRvYmVkZWNpZGVk',

  /**** SET FREE TRIAL DAYS ****/
  free_trial_days: 14,

  /**** SUBSCRIPTIONIDS CORRESPONDING TO API SUBSCRIPTIONIDS ****/
  subscriptionIds: {
    silver: 8,
    gold: 6,
    family2: 9,
    family3: 10,
    family4: 11
  },

  /**** ADTRACTION EVENT SPECIFIC SETTINGS ****/
  adtraction: {
    en_GB: {
      key: '', // not available yet
      currency: 'EUR'
    },
    sv_SE: {
      key: 1105175412,
      currency: 'SEK'
    },
    de_DE: {
      key: 1282090831,
      currency: 'EUR'
    },
    fi_FI: {
      key: 1248237634,
      currency: 'EUR'
    }
  },

}

export default Config
