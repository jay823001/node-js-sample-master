import React from 'react'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'

import 'newui/assets/styles/main.scss'

import { withNamespaces } from 'react-i18next'

class PageNotFound extends React.PureComponent {

  render() {
    let helpfulLinks = this.props.t(`lang_common:404.content`,
      {
        openUrlTag1: `<a href='${this.props.t('lang_route:homepage')}'>`, closeUrlTag1: '</a>',
        openUrlTag2: `<a href='${this.props.t('lang_route:books.eBooks')}'>`, closeUrlTag2: '</a>'
      }
    )
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <main className='app__main'>
          <Section maxCols={8} spacing={false} fullScreen={true}>
            <div className='registerStep'>
              <h1 className='registerStep__heading'>{this.props.t(`lang_common:404.heading`)}</h1>
              <h2 className='registerStep__heading'>{this.props.t(`lang_common:404.subheading`)}</h2>
              <div className='registerStep__content'>
                <p dangerouslySetInnerHTML={{ __html: helpfulLinks }} />
              </div>

            </div>
          </Section>
        </main>
      </MainApp>
    )
  }
}
export default withNamespaces(['lang_common'])(PageNotFound)
