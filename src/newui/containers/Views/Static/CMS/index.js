import React from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import { bindActionCreators } from 'redux'
import MainApp from 'newui/containers/Views'
import UserModule from 'newui/domain/Modules/User'
import Loader from 'newui/components/Loader'
import BoxWrapper from 'newui/components/BoxWrapper'

class CMSPage extends React.PureComponent {

  componentWillMount() {
    this.unlisten = this.props.history.listen((location) => {
      this.props.cmsContentRequest(location.pathname)
    });
  }

  componentDidMount() {
    this.props.cmsContentRequest(this.props.location.pathname)
  }

  render() {
    return (
      <React.Fragment>
        <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
          {this.props.cms.fetching && this.props.cms.content.length === 0 ? (
            <Loader />
          ) : (
              <React.Fragment>
                {this.props.cms.meta && this.props.cms.meta.pagemeta && this.props.cms.meta.pagemeta.metaList &&
                  <Helmet>
                    <title>{this.props.cms.meta.pagemeta.metaList[0].value}</title>
                    <meta name="description" content={this.props.cms.meta.pagemeta.metaList[1].value} />
                    <meta property="og:title" content={this.props.cms.meta.pagemeta.metaList[0].value} />
                    <meta property="og:description" content={this.props.cms.meta.pagemeta.metaList[1].value} />
                    <meta property="og:url" content={window.location.href} />
                    <meta name="twitter:description" content={this.props.cms.meta.pagemeta.metaList[1].value} />
                    <meta name="twitter:title" content={this.props.cms.meta.pagemeta.metaList[0].value} />
                  </Helmet>}

                <BoxWrapper maxCols={7}>
                  {this.props.cms.error ? (
                    <p>{this.props.t('common.internal_error')}</p>
                  ) : (
                      this.props.cms.content.cmsData ? (
                        <div dangerouslySetInnerHTML={{ __html: this.props.cms.content.cmsData.pageContent }} />
                      ) : (
                          <p className='text-has-error card_error'>{this.props.t('common.internal_error')}</p>
                        )
                    )}
                </BoxWrapper>
              </React.Fragment>
            )}
        </MainApp>
      </React.Fragment>
    )
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { cmsContentRequest: UserModule.actions.cmsContentRequest },
    dispatch
  )
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
  }
}

export default withNamespaces(['lang_error'])(connect(mapStateToProps, matchDispatchToProps)(CMSPage))