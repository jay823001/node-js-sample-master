import React from 'react'
import Html from 'newui/components/Html'
import Hero from 'newui/components/Hero'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import HeroStart from 'newui/components/HeroStart'
import BookRainbow from 'newui/components/BookRainbow'
import UserReviews from 'newui/components/UserReviews'
import Instructions from 'newui/components/Instructions'
import ProductFeatures from 'newui/components/ProductFeatures'
import SubscriptionPros from 'newui/components/SubscriptionPros'

import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'

class HomePage extends React.Component {

  getHeroImages = () => {
    return {
      "phone": "views/family-phone.jpg",
      "tablet": "views/family-tablet.jpg",
      "laptop": "views/family-laptop.jpg"
    }
  }

  getCtaUrl = () => {
    let url = this.props.t('lang_route:registration.trialFlow.subscriptionMessage')
    let buttonText = this.props.t('hero.buttons.default')
    let isPaymentRequired = this.props.subscriptionData && this.props.subscriptionData.data.paymentrequired

    if (!this.props.isLoggedIn && isPaymentRequired) {
      url = this.props.t('lang_route:registration.trialFlow.checkoutOptions')
      buttonText = this.props.t('hero.buttons.paymentRequired')
    }

    return {
      url: url,
      buttonText: buttonText
    }
  }

  render() {
    const setHeroImages = this.getHeroImages()
    const setCtaUrl = this.getCtaUrl()
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <div className='start'>
          <Section maxCols={6}>
            <HeroStart
              heading={this.props.t('hero.heading')}
              description={this.props.t('hero.description')}
              buttons={!this.props.isLoggedIn && setCtaUrl.buttonText}
              toUrl={setCtaUrl.url}
            />
          </Section>
          <Section spacing='false'>
            <BookRainbow items={this.props.t('lang_bookRainbow:ebooks')} />
          </Section>
          <Section maxCols={8}>
            <ProductFeatures
              heading={this.props.t('productFeatures.heading')}
              items={this.props.t('productFeatures.items')}
              src='views/start-phone-flat.png'
              width='500'
              height='980'
              alt='start-phone'
            />
          </Section>
          <Section bleed="false" spacing="smallBottom">
            <Hero modifiers={['alignCenter', 'halfScreen']} images={setHeroImages} />
          </Section>
          <Section maxCols={8} spacing="normalBottom">
            <Html margin={true} content={true}>
              <h2>{this.props.t('family.heading')} </h2>
              <p>{this.props.t('family.description')} </p>
            </Html>
          </Section>
          <Section spacing='false'>
            <BookRainbow items={this.props.t('lang_bookRainbow:ebooks')}
            />
          </Section>
          <Section maxCols={10}>
            <SubscriptionPros
              heading={this.props.t('subscriptionFeatures.heading')}
              pros={this.props.t('subscriptionFeatures.pros')}
              boxes={this.props.t('subscriptionFeatures.boxes')}
            />
          </Section>
          <Section maxCols={6} spacing='normalBottom'>
            <UserReviews
              description={this.props.t('reviews.description')}
              items={this.props.t('reviews.items')}
            />
          </Section>
          <Section spacing='normalBottom'>
            <Instructions items={this.props.t('instructions')} />
          </Section >
        </div >

      </MainApp >
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.userDetails.isLoggedIn,
    subscriptionData: state.subscriptionInfo.subscriptionInfo
  }
}

export default withNamespaces(['lang_homepage'])(connect(mapStateToProps)(HomePage))