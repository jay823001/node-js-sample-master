import React from 'react'
import { Link } from 'react-router-dom'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'
import IconsCaretRight from 'newui/components/Icons/CaretRight';

class SilverFaqQuestions extends React.Component {
  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={['normal']}>
          <PageHeader heading="Silver FAQ" />
          <BoxWrapper maxCols={6}>
            <div class="faqList">
              {this.props.t('silver_faq').map((item, index) => (
                <div class="faqList__item" key={index}>
                  <Link class="faqList__link" to={{ pathname: `${this.props.t('lang_route:silver_FAQ')}/${item.slug}`, state: { object: item } }}>
                    <div class="faqList__text">{item.question}</div>
                    <div class="faqList__icon">
                      <IconsCaretRight />
                    </div>
                  </Link>
                </div>
              ))}
            </div>
          </BoxWrapper>
        </Section>
      </MainApp>
    )
  }
}

export default withNamespaces(['lang_faq'])(SilverFaqQuestions)