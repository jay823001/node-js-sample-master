import React from 'react'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import BoxWrapper from 'newui/components/BoxWrapper'
import Html from 'newui/components/Html'
import { Redirect } from 'react-router-dom'

class SilverFaqAnswer extends React.Component {
  render() {
    return (
      this.props.location.state ? (
        <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
          <Section maxCols={8} spacing={['normal']}>
            <BoxWrapper maxCols={6} heading={this.props.location.state.object.question}>
              <Html margin={true} content={true}>
                {this.props.location.state.object.answer.map((item) => (
                  <p>{item}</p>
                ))}
              </Html>
            </BoxWrapper>
          </Section>
        </MainApp>
      ) : (
          <Redirect to={this.props.t('lang_route:silver_FAQ')} />
        )
    )
  }
}

export default withNamespaces(['lang_faq'])(SilverFaqAnswer)