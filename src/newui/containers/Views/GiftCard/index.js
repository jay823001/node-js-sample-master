import React from 'react'
import MainApp from 'newui/containers/Views'
import 'newui/assets/styles/main.scss'
import Section from 'newui/components/Section'
import Button from 'newui/components/Button'
import BulletList from 'newui/components/BulletList'

import { withNamespaces } from 'react-i18next'
import { setButtonProperties } from 'newui/utils/helperFunctions'

class GiftCardOptions extends React.PureComponent {

  getButtonProperties = () => {
    return [
      {
        url: this.props.t('lang_route:giftCard.purchase'),
        text: this.props.t('Message.buttons.purchase.text'),
        className: 'btn--primary btn--arrow'
      }, {
        url: this.props.t('lang_route:giftCard.redeem'),
        text: this.props.t('Message.buttons.redeem.text'),
        className: 'btn--secondary'
      }
    ]
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <main className='app__main'>
          <Section maxCols={8} spacing={false} fullScreen={true}>
            <div className='registerStep'>
              <p className='registerStep__label'>{this.props.t('Message.stepText')}</p>
              <h1 className='registerStep__heading'>{this.props.t('Message.h1Text')}</h1>
              <BulletList items={this.props.t('Message.bulletItems')} />
              <div className='registerStep__actions'>
                <Button items={setButtonProperties(this.getButtonProperties())} />
              </div>
            </div>
          </Section>
        </main>
      </MainApp>
    )
  }
}

export default withNamespaces(['lang_giftCard'])(GiftCardOptions)