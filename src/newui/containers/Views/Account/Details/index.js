import React from 'react'
import validate from './validate'
import { Base64 } from 'js-base64'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { Link } from 'react-router-dom'
import { formConfig } from './formConfig'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import UserModule from 'newui/domain/Modules/User'
import FormInput from 'newui/components/FormInput'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import FormOption from 'newui/components/FormOption'
import StickyButton from 'newui/components/StickyButton'
import SubmitButton from 'newui/components/Button/SubmitButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'

const userUpdateRequest = UserModule.actions.userUpdateRequest

class MyDetails extends React.Component {

  state = {
    disableForm: false
  }

  getButtonProperties = () => {
    return {
      url: null,
      text: this.props.t('myDetails.buttons.saveBtn'),
      className: 'btn--primary',
    }
  }

  submit = (values, userUpdateRequest) => {
    let details = {}
    details.email = values.email
    details.firstname = values.firstName
    details.lastname = values.lastName
    details.address = values.address
    details.postcode = values.zipCode
    details.city = values.city
    details.cellphone = values.phoneNumber
    details.newsletter = ''
    details.authkey = this.props.authKey.authkey
    details.password = values.password ? Base64.encode(values.password) : ''
    // const data = {
    //   password,
    //   email,
    //   firstname,
    //   lastname,
    //   address,
    //   postcode,
    //   city,
    //   cellphone,
    //   newsletter,
    //   authkey,
    //   navigate: this.props.t('lang_route:mypages.myDetails'),
    //   formName: 'AccountDetailsForm'
    // }

    const nonFormData ={
      navigate: this.props.t('lang_route:mypages.myDetails'),
      formName: 'AccountDetailsForm'
    }

    let sendData = {
      formData: details,
      nonFormData: nonFormData
    }

    userUpdateRequest(sendData)
  }

  checkActions = () => {

    let allowCancel = false
    this.props.userDetails.allowedactions.forEach(function (action) {
      if (action === 'CANCEL_FREE_MEMBERSHIP' || action === 'CANCEL_MEMBERSHIP') {
        allowCancel = true
      }
    })
    return allowCancel
  }

  componentDidMount() {
    this.props.initialize({
      email: this.props.userDetails.email,
      firstName: this.props.userDetails.firstname,
      lastName: this.props.userDetails.lastname,
      address: this.props.userDetails.address,
      zipCode: this.props.userDetails.postcode,
      city: this.props.userDetails.city,
      phoneNumber: this.props.userDetails.cellphone
    })

    const disable = this.props.userDetails.allowedactions.includes('UPDATE_PROFILE') ? false : true
    this.setState({ disableForm: disable })
  }

  render() {
    const formInputDetails = formConfig()
    const { userUpdateRequest, handleSubmit, userError, fetching } = this.props
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false}>
          <PageHeader heading={this.props.t('myDetails.heading')} />
          <form onSubmit={handleSubmit(values => this.submit(values, userUpdateRequest))} className="userDetailsForm">
            <fieldset disabled={this.state.disableForm}>
              <div className="accountInfo">
                <div className="accountInfo__main">
                  <BoxWrapper maxCols={6} modifiers={['textCenter']}>
                    <div className="formLayout">
                      <div className="formLayout__item formLayout__item--50 formLayout__item--stackPhone">
                        <FormOption label={this.props.t('myDetails.form.email.label')}>
                          <FormInput items={formInputDetails.email} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item formLayout__item--50 formLayout__item--stackPhone">
                        <FormOption label={this.props.t('myDetails.form.password.label')}>
                          <FormInput items={formInputDetails.password} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item formLayout__item--50 formLayout__item--stackPhone">
                        <FormOption label={this.props.t('myDetails.form.firstName.label')}>
                          <FormInput items={formInputDetails.firstName} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item formLayout__item--50 formLayout__item--stackPhone">
                        <FormOption label={this.props.t('myDetails.form.lastName.label')}>
                          <FormInput items={formInputDetails.lastName} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item">
                        <FormOption label={this.props.t('myDetails.form.address.label')}>
                          <FormInput items={formInputDetails.address} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item formLayout__item--33 formLayout__item--stackPhone">
                        <FormOption label={this.props.t('myDetails.form.zipCode.label')}>
                          <FormInput items={formInputDetails.zipCode} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item formLayout__item--67 formLayout__item--stackPhone">
                        <FormOption label={this.props.t('myDetails.form.city.label')}>
                          <FormInput items={formInputDetails.city} />
                        </FormOption>
                      </div>
                      <div className="formLayout__item">
                        <FormOption label={this.props.t('myDetails.form.phoneNumber.label')}>
                          <FormInput items={formInputDetails.phoneNumber} />
                        </FormOption>
                      </div>
                    </div>
                    {userError &&
                      <p className='text-has-error'>{this.props.t(userError)}</p>
                    }
                  </BoxWrapper>
                </div>
                {this.checkActions() &&
                  <div className="accountInfo__aside">
                    <Link className="accountInfo__link" to={this.props.t('lang_route:mypages.cancelSubscriptionMessage')}>{this.props.t('myDetails.link.text')}</Link>
                  </div>
                }
              </div>
              <StickyButton stickyPhone={true}>
                <SubmitButton items={setButtonProperties(this.getButtonProperties())} fetching={fetching} formName='AccountDetailsForm' />
              </StickyButton>
            </fieldset>
          </form>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    authKey: state.authKeys,
    fetching: state.updateUser.fetching,
    userError: state.updateUser.userError,
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, { userUpdateRequest })(reduxForm({
  form: 'AccountDetailsForm',
  validate
})(MyDetails)))