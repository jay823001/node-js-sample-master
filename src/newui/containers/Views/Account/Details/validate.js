export const validate = values => {
  const errors = {}

  if (!values.email) {
    errors.email = 'registerForm.email'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(values.email)) {
    errors.email = 'registerForm.email'
  }

  if (values.password !== undefined) {
    if (values.password.length < 4 || values.password.length > 25) {
      errors.password = 'registerForm.password_length'
    } else if (!/^[a-zA-Z0-9-@#$%_]+$/.test(values.password)) {
      errors.password = 'registerForm.password_chars'
    }
  }

  if (values.phoneNumber) {
    if (values.phoneNumber.length < 7 || values.phoneNumber.length > 12) {
      errors.phoneNumber = 'userForm.phoneNumber'
    }
  }

  return errors
}

export default validate