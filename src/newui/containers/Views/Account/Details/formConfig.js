export const formConfig = () => {
  return {
    "email": {
      "component": "input",
      "name": "email",
      "type": "email",
      "maxLength": "50"
    },
    "password": {
      "component": "input",
      "name": "password",
      "type": "password",
      "maxLength": "50",
      "placeholder": '********'
    },
    "firstName": {
      "component": 'input',
      "name": 'firstName',
      "type": 'text',
      "maxLength": 10,
      "placeholder": ''
    },
    "lastName": {
      "component": 'input',
      "name": 'lastName',
      "type": 'text',
      "maxLength": 10,
      "placeholder": ''
    },
    "address": {
      "component": 'input',
      "name": 'address',
      "type": 'text',
      "maxLength": 50,
      "placeholder": ''
    },
    "zipCode": {
      "component": 'input',
      "name": 'zipCode',
      "type": 'text',
      "maxLength": 10,
      "placeholder": ''
    },
    "city": {
      "component": 'input',
      "name": 'city',
      "type": 'text',
      "maxLength": 20,
      "placeholder": ''
    },
    "phoneNumber": {
      "component": 'input',
      "name": 'phoneNumber',
      "type": 'tel',
      "minLength": 7,
      "maxLength": 12,
      "placeholder": '',
      "normalize": "phoneNumber"
    }
  }
}