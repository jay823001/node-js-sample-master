import React from 'react'
import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'
import MainApp from 'newui/containers/Views'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import Button from 'newui/components/Button'

class PaymentDetails extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.checkoutOptions'),
      text: this.props.t('paymentDetails.buttons.changebtn'),
      className: 'btn--secondary',
    }
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <PageHeader heading={this.props.t('paymentDetails.heading')} />
          <div className="accountInfo">
            <div className="accountInfo__main">
              <BoxWrapper maxCols={6} modifiers={['textCenter']}>
                {/* <h2 className="accountInfo__heading">Payment Method</h2> */}
                <p className="accountInfo__description">{this.props.t('paymentDetails.description', { date: this.props.userDetails.subscriptiondetails.nextrundate })}</p>
              </BoxWrapper>
            </div>
          </div>
          {this.props.userDetails.allowedactions.includes('UPDATE_CARD') &&
            <StickyButton>
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </StickyButton>
          }
        </Section>
      </MainApp>
    )
  }

}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps)(PaymentDetails))