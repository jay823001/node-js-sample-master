import React from 'react'
import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import MainApp from 'newui/containers/Views'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import Button from 'newui/components/Button'

class PaymentSuccess extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.account'),
      text: this.props.t('paymentDetails.buttons.myaccount'),
      className: 'btn--secondary',
    }
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <div className="registerStep">
            <h1 className="registerStep__heading">{this.props.t('paymentDetails.completed.heading')}</h1>
            <p className="registerStep__description">{this.props.t('paymentDetails.checkout.description', { date: this.props.userDetails.subscriptiondetails.nextrundate })}</p>
            <div className="registerStep__actions">
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </div>
          </div>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps)(PaymentSuccess))