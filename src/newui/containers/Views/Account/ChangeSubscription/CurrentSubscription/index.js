import React from 'react'
import { connect } from 'react-redux'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'

class MySubscription extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.changeMySubscriptionBasic'),
      text: this.props.t('mySubscription.buttons.text'),
      className: 'btn--primary',
    }
  }

  isFamily = () => {
    if (this.props.userDetails.subscriptiondetails.subscriptionplanename.includes('Familj')) {
      return this.props.t('lang_register:common.users')
    }
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false}>
          <PageHeader heading={this.props.t('mySubscription.heading')} />
          <div className="accountInfo">
            <div className="accountInfo__main">
              <BoxWrapper maxCols={6} modifiers={['textCenter']}>
                <h2 className="accountInfo__heading">{this.props.userDetails.subscriptiondetails.subscriptionplanename} {this.isFamily()}</h2>
                <p className="accountInfo__description">{this.props.t('mySubscription.nxtPayment', { date: this.props.userDetails.subscriptiondetails.nextrundate })}</p>
              </BoxWrapper>
            </div>
          </div>
          {!this.props.userDetails.allowedactions.includes('ACTIVATE_AGAIN') &&
            <StickyButton>
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </StickyButton>
          }
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps)(MySubscription))