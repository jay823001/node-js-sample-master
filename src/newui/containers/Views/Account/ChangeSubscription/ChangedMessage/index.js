import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import Loader from 'newui/components/Loader'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import UserModule from 'newui/domain/Modules/User'
import { setButtonProperties, getSubscriptionId } from 'newui/utils/helperFunctions'
import { SubscriptionConfig } from 'newui/components/SubscriptionTable/SubscriptionOptions/SubscriptionConfig'

class SubscriptionChangedMessage extends React.PureComponent {

  getTranslatedPrice = (price) => {
    return `${price.substr(0, price.indexOf(' '))} ${this.props.t('lang_register:common.currency')}/${this.props.t('lang_register:common.month')}`
  }

  getPackageDetails = (type) => {
    if (this.props.selectedSubscription.index !== "") {
      const subscriptionIndex = this.props.selectedSubscription.index
      let packageDetails = SubscriptionConfig(this.props)
      let subscription
      let subscriptionName = ''
      let subscriptionPrice = 0
      if (this.props.selectedSubscription.isfamily) {
        subscription = packageDetails.subscriptionOptionsFamily.items[subscriptionIndex]
        subscriptionName = `${subscription.category},  ${subscription.heading}  ${subscription.label}`
        subscriptionPrice = subscription.details
      } else {
        subscription = packageDetails.subscriptionOptionsBasic
        subscriptionName = subscription.models[subscriptionIndex].name
        subscriptionPrice = subscription.price.values[subscriptionIndex]
      }
      return type === 1 ? subscriptionName : subscriptionPrice
    }

    // if (this.props.selectedSubscription.isfamily) {
    //   return type === 1 ? 
    //   `${this.props.subscriptionSelected.name}, ${this.props.subscriptionFamilySelected.heading} ${this.props.subscriptionFamilySelected.label}` :
    //   this.getTranslatedPrice(this.props.subscriptionFamilySelected.details)
    // } else {
    //   return type === 1 ? this.props.subscriptionSelected.name : this.getTranslatedPrice(this.props.subscriptionSelected.price)
    // }
  }

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.account'),
      text: this.props.t('cancelCompleted.buttons.text'),
      className: 'btn--secondary',
    }
  }

  componentWillMount() {
    const subscriptionId = getSubscriptionId(this.props.selectedSubscription)
    this.props.updateSubscriptionRequest(subscriptionId)
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          {this.props.subscription.fetching || !this.props.subscription.updateInfo ? (
            <Loader />
          ) : (
              <div className="registerStep">
                {this.props.subscription.updateInfo.changetype === 'UPGRADE' ? (
                  <React.Fragment>
                    <h1 className="registerStep__heading">{this.props.t('mySubscription.updated.upgrade.heading')}</h1>
                    <p className="registerStep__description">{this.props.t('mySubscription.updated.upgrade.description',
                      {
                        date: this.props.userDetails.subscriptiondetails.nextrundate,
                        packageName: this.getPackageDetails(1),
                        packagePrice: this.getPackageDetails(2)
                      })}
                    </p>
                  </React.Fragment>
                ) : (
                    <React.Fragment>
                      <h1 className="registerStep__heading">{this.props.t('mySubscription.updated.heading')}</h1>
                      <p className="registerStep__description">{this.props.t('mySubscription.updated.description',
                        {
                          date: this.props.userDetails.subscriptiondetails.nextrundate,
                          packageName: this.getPackageDetails(1),
                          packagePrice: this.getPackageDetails(2)
                        })}
                      </p>
                    </React.Fragment>
                  )}
                <div className="registerStep__actions">
                  <Button items={setButtonProperties(this.getButtonProperties())} />
                </div>
              </div>
            )}
        </Section>
      </MainApp >
    )
  }

}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { updateSubscriptionRequest: UserModule.actions.updateSubscriptionRequest },
    dispatch
  )
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo,
    selectedSubscription: state.selectedSubscription,
    subscription: state.subscriptionInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, matchDispatchToProps)(SubscriptionChangedMessage))