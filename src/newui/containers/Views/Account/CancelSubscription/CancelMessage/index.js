import React from 'react'
import { connect } from 'react-redux'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import { withNamespaces } from 'react-i18next'
import Button from 'newui/components/Button'
import { Link } from 'react-router-dom'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'

class CancelMessage extends React.PureComponent {

  getButtonProperties = () => {
    let buttons = []
    this.props.t('cancelSubscription.buttons').map((btn) => (
      buttons.push({
        url: this.props.t(btn.url),
        text: btn.text,
        className: btn.className
      })
    ))
    return buttons
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <PageHeader heading={this.props.t('cancelSubscription.heading')} description={this.props.t('cancelSubscription.description')} />
          {this.props.userDetails.allowedactions.includes('CHANGE_SUBSCRIPTION') &&
            <div className="accountInfo">
              <div className="accountInfo__main">
                <BoxWrapper maxCols={6} modifiers={['textCenter']}>
                  <p className="accountInfo__description">{this.props.t('cancelSubscription.info')}</p>
                  <div className="accountInfo__divider" />
                  <Link className="accountInfo__link" to={this.props.t(this.props.t('cancelSubscription.link.url'))}>{this.props.t('cancelSubscription.link.text')}</Link>
                </BoxWrapper>
              </div>
            </div>
          }
          <StickyButton>
            <Button items={setButtonProperties(this.getButtonProperties())} />
          </StickyButton>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps)(CancelMessage))