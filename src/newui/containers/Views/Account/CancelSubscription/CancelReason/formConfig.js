export const formConfig = () => {
  return {
    "missingBooks": {
      "component": "input",
      "name": "missingBooks",
      "type": "text",
      "maxLength": 100
    },
    "other": {
      "component": 'input',
      "name": 'other',
      "type": 'text',
      "maxLength": 100
    }
  }
}