import i18next from 'newui/i18n'

export const validate = values => {
  const errors = {}

  if (!values.reason || values.reason === i18next.t('lang_myPages:cancelReason.selectOptions')[0].value) {
    errors.reason = 'cancelSubscription.selectone'
  }

  return errors
}

export default validate