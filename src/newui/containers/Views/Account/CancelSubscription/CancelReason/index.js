import React from 'react'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import { withNamespaces } from 'react-i18next'
import FormOption from 'newui/components/FormOption'
import FormSelect from 'newui/components/FormSelect'
import FormInput from 'newui/components/FormInput'
import { reduxForm, formValueSelector } from 'redux-form'
import Button from 'newui/components/Button'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import validate from './validate'
import UserModule from 'newui/domain/Modules/User'
import { connect } from 'react-redux'
import { formConfig } from './formConfig'

const cancelSubscriptionRequest = UserModule.actions.cancelSubscriptionRequest

class CancelReason extends React.PureComponent {

  getButtonProperties = () => {
    let buttons = []
    this.props.t('cancelReason.buttons').map((btn) => (
      buttons.push({
        url: this.props.t(btn.url),
        text: btn.text,
        className: btn.className,
        purpose: this.props.t(btn.url) === this.props.t('lang_route:mypages.myDetails') ? 'link' : 'submit',
        formName: 'cancelSubscriptionReason'
      })
    ))
    return buttons
  }

  submit = (values, cancelSubscriptionRequest) => {
    let comment = null
    if (values.reason === this.props.t('cancelReason.selectOptions')[3].value) {
      comment = values.missingBooks
    } else if (values.reason === this.props.t('cancelReason.selectOptions')[6].value) {
      comment = values.other
    }
    const data = { reason: values.reason, comment: comment, navigate: this.props.t('lang_route:mypages.cancelSubscriptionCompleted') }

    cancelSubscriptionRequest({ data })
  }

  render() {
    const { cancelSubscriptionRequest, handleSubmit } = this.props
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <form onSubmit={handleSubmit((values) => this.submit(values, cancelSubscriptionRequest))}>
            <PageHeader heading={this.props.t('cancelReason.heading')} description={this.props.t('cancelReason.description')} />
            <div className="accountInfo">
              <div className="accountInfo__main">
                <BoxWrapper maxCols={6} modifiers={['textCenter']}>
                  <p className="accountInfo__description">{this.props.t('cancelReason.info')}</p>
                  <div className="accountInfo__content">
                    <FormOption label={this.props.t('cancelReason.selectLabel')}>
                      <FormSelect items={this.props.t('cancelReason.selectOptions')} name="reason" />
                    </FormOption>
                    {(this.props.selectedReason === this.props.t('cancelReason.selectOptions')[3].value ||
                      this.props.selectedReason === this.props.t('cancelReason.selectOptions')[6].value) && (
                        <FormOption label={this.props.selectedReason === this.props.t('cancelReason.selectOptions')[3].value ? (
                          this.props.t('cancelReason.resonLabelOne')
                        ) : (
                            this.props.t('cancelReason.resonLabelTwo')
                          )}>
                          <FormInput items={this.props.selectedReason === this.props.t('cancelReason.selectOptions')[3].value ? (
                            formConfig().missingBooks
                          ) : (
                              formConfig().other
                            )} />
                        </FormOption>
                      )}
                  </div>
                </BoxWrapper>
              </div>
            </div>
            <StickyButton>
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </StickyButton>
          </form>
        </Section>
      </MainApp>
    )
  }
}

const selector = formValueSelector('cancelSubscriptionReason')

function mapStateToProps(state) {
  return {
    selectedReason: selector(state, 'reason')
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, { cancelSubscriptionRequest })(reduxForm({
  form: 'cancelSubscriptionReason',
  validate
})(CancelReason)))