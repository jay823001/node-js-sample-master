import React from 'react'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'

import { withNamespaces } from 'react-i18next'
import { connect } from 'react-redux'
import {
  setButtonProperties,
  cookies,
  removeCookies
} from 'newui/utils/helperFunctions'

class CancelCompleted extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.myDetails'),
      text: this.props.t('cancelCompleted.buttons.text'),
      className: 'btn--secondary',
    }
  }

  componentWillUnmount() {
    removeCookies('cancelDate', { path: '/' })
  }

  render() {
    const allowedActions = this.props.userDetails.allowedactions
    const isTrialUser = allowedActions.includes('CANCEL_FREE_MEMBERSHIP') || allowedActions.includes("ACTIVATE_AGAIN")
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <div className="registerStep">
            <h1 className="registerStep__heading">{this.props.t('cancelCompleted.heading')}</h1>
            <p className="registerStep__description">
              {this.props.t('cancelCompleted.description')}
              {!isTrialUser && this.props.t('cancelCompleted.expiryMessage', { date: cookies.get('cancelDate') })}
            </p>
            <div className="registerStep__actions">
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </div>
          </div>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps)(CancelCompleted))