import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import { bindActionCreators } from 'redux'
import UserModule from 'newui/domain/Modules/User'

class NavigationMenu extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.login'),
      text: this.props.t('menu.buttons.logout.text'),
      className: 'btn--secondary',
    }
  }

  logOut = () => {
    this.props.logoutRequest()
  }

  activateAgain = () => {
    this.props.activateAgainRequest()
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <div className="accountNavigation">
            {this.props.t('menu.items').map((element, index) => (
              element.url_link === this.props.t('menu.items')[1].url_link ? (
                !this.props.userDetails.ismember && this.props.userDetails.allowedactions.includes('ACTIVATE_AGAIN') &&
                <div className="accountNavigation__item" key={index}>
                  <Link key={index} to={this.props.t(element.url_link)} className="accountNavigation__link">
                    {element.text}
                  </Link>
                </div>
              ) : (
                  element.url_link === this.props.t('menu.items')[0].url_link ? (
                    this.props.userDetails.ismember && this.props.userDetails.allowedactions.includes('REVERT_CANCEL_MEMBERSHIP') &&
                    <div className="accountNavigation__item" key={index}>
                      <Link key={index} className="accountNavigation__link" to={this.props.location.pathname} onClick={this.activateAgain}>
                        {element.text}
                      </Link>
                    </div>
                  ) : (
                      <div className="accountNavigation__item" key={index}>
                        <Link key={index} to={this.props.t(element.url_link)} className="accountNavigation__link">
                          {element.text}
                        </Link>
                      </div>
                    )
                )
            ))}
          </div>
          <StickyButton stickyPhone="true">
            <Button items={setButtonProperties(this.getButtonProperties())} logout={() => this.logOut()} logoutbtn={true} />
          </StickyButton>
        </Section >
      </MainApp >
    )
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { logoutRequest: UserModule.actions.logoutRequest , activateAgainRequest: UserModule.actions.activateAgainRequest},
    dispatch
  )
}

function mapStateToProps(state) {
  return {
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, matchDispatchToProps)(NavigationMenu))
