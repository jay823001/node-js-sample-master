import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import UserModule from 'newui/domain/Modules/User'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'
import qs from 'qs'
import { Redirect } from 'react-router-dom'
import Button from 'newui/components/Button'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import Loader from 'newui/components/Loader'

class MyReceipt extends React.PureComponent {

  state = {
    orderId: null
  }

  getEmail = () => {
    if (this.props.i18n.language === 'sv_SE') {
      return 'kundservice@nextory.se'
    } else {
      return 'asiakaspalvelu@nextory.fi'
    }
  }

  getPaymentMethod = (orderDetails) => {
    let paymentmethod = this.props.t('receipt.payment_methods.credit_card')
    if (orderDetails.provider === 'TRUSTLY') {
      paymentmethod = this.props.t('receipt.payment_methods.trustly_direct')
    }
    if (orderDetails.campagincode > 0) {
      paymentmethod = this.props.t('receipt.payment_methods.campaign_code')
    } else if (orderDetails.giftcardid > 0) {
      paymentmethod = this.props.t('receipt.payment_methods.gift_cards')
    }

    return paymentmethod
  }

  getVat = (orderamount) => {
    const vat = orderamount * 0.2
    const vatRounded = Math.round(vat * 10) / 10
    return vatRounded
  }

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.orders'),
      text: this.props.t('receipt.buttons.myOrders'),
      className: 'btn--secondary',
    }
  }

  componentWillMount() {
    const query = qs.parse(this.props.location.search, {
      ignoreQueryPrefix: true
    })

    if (query.oid) {
      this.setState({ orderId: query.oid })
      this.props.receiptDetailsRequest({ orderId: query.oid })
    }
  }

  render() {
    return (
      this.state.orderId ? (
        <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
          <Section maxCols={8} spacing={false}>
            <PageHeader heading={this.props.t('receipt.heading')}
              description={this.props.t('receipt.description', { orderId: this.state.orderId })} />
            <div className="accountInfo">
              <div className="accountInfo__main">
                <BoxWrapper maxCols={6}>
                  {(this.props.orders.fetching || !this.props.orders.orderDetails) ? (
                    <Loader />
                  ) : (
                      <div className="metaTable">
                        <div className="metaTable__group">
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.date')}</div>
                            <div className="metaTable__value">{this.props.orders.orderDetails.orderdate}</div>
                          </div>
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.paymentMethod')}</div>
                            <div className="metaTable__value">{this.getPaymentMethod(this.props.orders.orderDetails)}</div>
                          </div>
                        </div>

                        <div className="metaTable__group">
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.receiver')}</div>
                            <div className="metaTable__value">Nextory</div>
                          </div>
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.vatNumber')}</div>
                            <div className="metaTable__value">SE556708-4149</div>
                          </div>
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.email')}</div>
                            <div className="metaTable__value">{this.getEmail()}</div>
                          </div>
                        </div>

                        <div className="metaTable__group">
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.vat')}</div>
                            <div className="metaTable__value">
                              {this.getVat(this.props.orders.orderDetails.orderamount)} {this.props.orders.orderDetails.currency}
                            </div>
                          </div>
                          <div className="metaTable__row">
                            <div className="metaTable__label">{this.props.t('receipt.details.total')}</div>
                            <div className="metaTable__value">{this.props.orders.orderDetails.orderamountwithmoms} {this.props.orders.orderDetails.currency}</div>
                          </div>
                        </div>
                      </div>
                    )}
                </BoxWrapper>
              </div>
            </div>
            <StickyButton stickyPhone={true}>
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </StickyButton>
          </Section>
        </MainApp >
      ) : (
          <Redirect to={this.props.t('lang_route:mypages.orders')} />
        )

    )
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { receiptDetailsRequest: UserModule.actions.receiptDetailsRequest },
    dispatch
  )
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, matchDispatchToProps)(MyReceipt))