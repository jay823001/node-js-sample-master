import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import Loader from 'newui/components/Loader'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import UserModule from 'newui/domain/Modules/User'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'

class MyOrders extends React.PureComponent {

  componentWillMount() {
    this.props.orderDetailsRequest()
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false}>
          <PageHeader heading={this.props.t('orders.heading')} />
          <div className="accountInfo">
            <div className="accountInfo__main">
              <BoxWrapper maxCols={6}>
                {this.props.orders.fetching ? (
                  <Loader />
                ) : (
                    <div className="metaTable">
                      {this.props.orders.allorders.map((order, index) => (
                        <div className="metaTable__group metaTable__group--border" key={index}>
                          <div className="metaTable__heading">{order.orderdate}</div>
                          <div className="metaTable__row">
                            <div className="metaTable__label">Cost</div>
                            <div className="metaTable__value">{order.orderamt} {order.currency}</div>
                          </div>
                          <div className="metaTable__row">
                            <div className="metaTable__label">ID</div>
                            <Link className="metaTable__value metaTable__value--link" to={`${this.props.t('lang_route:mypages.receipt')}?oid=${order.orderid}`} >{order.fakturano}</Link>
                          </div>
                        </div>
                      ))}
                    </div>
                  )}
              </BoxWrapper>
            </div>
          </div>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { orderDetailsRequest: UserModule.actions.orderDetailsRequest },
    dispatch
  )
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, matchDispatchToProps)(MyOrders))