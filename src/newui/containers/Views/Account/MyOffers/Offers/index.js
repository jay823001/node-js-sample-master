import React from 'react'
import { Link } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'
import IconsCaretRight from 'newui/components/Icons/CaretRight'

class MyOffers extends React.PureComponent {

  render() {
    return (
      <Section maxCols={8} spacing={false}>
        <PageHeader heading={this.props.t('offers.heading')} description={this.props.t('offers.description')} />
        <BoxWrapper maxCols={6}>
          <div className="offerList">
            {this.props.offers.collabs.map((offer, index) => (
              <div className="offerList__item" key={index}>
                <Link className="offerList__link" to={{ pathname: this.props.t('lang_route:mypages.singleOffer'), state: { offer: offer } }} >
                  <div className="offerList__text" >{offer.title}</div>
                  <div className="offerList__icon">
                    <IconsCaretRight />
                  </div>
                </Link>
              </div>
            ))}
          </div>
        </BoxWrapper>
      </Section>
    )
  }
}


export default withNamespaces(['lang_myPages'])(MyOffers)