import React from 'react'
import Media from 'newui/components/Media'
import Button from 'newui/components/Button'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import BoxWrapper from 'newui/components/BoxWrapper'
import PageHeader from 'newui/components/PageHeader'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'

class SingleOffer extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.offers'),
      text: this.props.t('offers.singleOffer.buttons.myOffers'),
      className: 'btn--secondary',
    }
  }

  render() {
    return (
      <Section maxCols={8} spacing={false}>
        <PageHeader heading={this.props.offer.title} />
        <BoxWrapper maxCols={6}>
          <div className="offerPost">
            <div className="offerPost__image">
              <Media src={this.props.offer.picture ? this.props.offer.picture : "http://placehold.it/300x120"} external={true} width={300} height={120} />
            </div>
            <div className="offerPost__heading">{this.props.offer.subtitle}</div>
            <div className="offerPost__description">{this.props.offer.description}</div>
            <div className="offerPost__button">
              <a className="btn btn--primary" href={this.props.offer.link}>{this.props.t('offers.singleOffer.buttons.takePartbtn')}</a>
            </div>
          </div>
        </BoxWrapper>
        <StickyButton stickyPhone={true}>
          <Button items={setButtonProperties(this.getButtonProperties())} />
        </StickyButton>
      </Section>
    )
  }
}

export default withNamespaces(['lang_myPages'])(SingleOffer)