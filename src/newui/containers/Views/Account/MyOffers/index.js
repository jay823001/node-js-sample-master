import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Loader from 'newui/components/Loader'
import MainApp from 'newui/containers/Views'
import UserModule from 'newui/domain/Modules/User'
import MyOffers from 'newui/containers/Views/Account/MyOffers/Offers'
import SingleOffer from 'newui/containers/Views/Account/MyOffers/SingleOffer'

class Offers extends React.PureComponent {

  getComponent = () => {
    if (this.props.location.state) {
      return <SingleOffer offer={this.props.location.state.offer} />
    } else {
      return <MyOffers offers={this.props.offers.alloffers} />
    }
  }

  componentWillMount() {
    this.props.offerDetailsRequest()
  }

  render() {
    return (
      this.props.offers.fetching ? (
        <Loader />
      ) : (
          <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
            {this.getComponent()}
          </MainApp>
        )
    )
  }
}

function mapStateToProps(state) {
  return {
    offers: state.offers
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { offerDetailsRequest: UserModule.actions.offerDetailsRequest },
    dispatch
  )
}

export default (connect(mapStateToProps, matchDispatchToProps)(Offers))