import React from 'react'
import BEMHelper from 'react-bem-helper'
import ErrorBoundary from 'ErrorBoundary'
import DataLayer from 'containers/App/datalayer'
import CookieBar from 'newui/components/CookieBar'
import HeaderClean from 'newui/components/Layout/HeaderClean'
import HeaderDefault from 'newui/components/Layout/HeaderDefault'
import FooterDefault from 'newui/components/Layout/FooterDefault'

import { withNamespaces } from 'react-i18next'
import { connect } from 'react-redux'
import { uppercaseFirst } from 'newui/utils/helperFunctions'

class MainApp extends React.PureComponent {
  /*
   **@event: set background color for body
   **@return: background color
   **@parameter description: none
   **@parameter type: none
   */
  setBGcolor() {
    if (this.props.bgColor) {
      return `bg${uppercaseFirst(this.props.bgColor)}`
    }
    return 'bgWhite'
  }

  /*
   **@event: change header depending on trial flow
   **@return: none
   **@parameter description: none
   **@parameter type: none
   */
  getHeader() {
    let isPaymentRequired = this.props.subscriptionData && this.props.subscriptionData.data.paymentrequired
    if (this.props.header === 'clean') {
      return <HeaderClean />
    } else if (this.props.header === 'default') {
      return <HeaderDefault auth={this.props.isLoggedIn} user={this.props.userDetails} isPaymentRequired={isPaymentRequired} />
    } else if (this.props.header === 'none') {
      return null
    }
  }

  getFooter = () => {
    if (this.props.footer && this.props.footer === 'none') {
      return null
    } else {
      return <footer className="app__footer">
        <FooterDefault loggedIn={this.props.isLoggedIn} />
      </footer>
    }
  }

  render() {
    const classes = new BEMHelper('app')
    const bgColor = this.setBGcolor()
    const header = this.getHeader()
    let mustOverlap = window.location.pathname === '/' ? 'overlaping' : ''
    return (
      <ErrorBoundary>
        <div id='newUI'>
          <DataLayer />
          <div {...classes('', bgColor)} id='app' >
            {header &&
              <header {...classes('header', mustOverlap)}>{header}</header>
            }
            <main {...classes('main')}>{this.props.children}</main>
            {this.getFooter()}
            <CookieBar />
          </div>
        </div>
      </ErrorBoundary>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.userDetails.isLoggedIn,
    userDetails: state.userDetails,
    subscriptionData: state.subscriptionInfo.subscriptionInfo
  }
}

export default withNamespaces(['lang_register'])(connect(mapStateToProps)(MainApp))
