export const formConfig = () => {
  return {
    "email": {
      "component": "input",
      "name": "email",
      "type": "email",
      "maxLength": "50"
    },
    "password": {
      "component": "input",
      "name": "password",
      "type": "password",
      "maxLength": "50"
    }
  }
}