import React from 'react'
import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import UserModule from 'newui/domain/Modules/User'
import FormInput from 'newui/components/FormInput'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import FormOption from 'newui/components/FormOption'
import { formConfig } from './formConfig'
import { reduxForm } from 'redux-form'
import { Link } from 'react-router-dom'
import StickyButton from 'newui/components/StickyButton'
import Button from 'newui/components/Button'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import validate from './validate'
import { Base64 } from 'js-base64'

const loginRequest = UserModule.actions.loginRequest

class Login extends React.Component {

  getButtonProperties = () => {
    let buttons = []
    this.props.t('login.buttons').map((btn) => (
      buttons.push({
        url: this.props.t(btn.url),
        text: btn.text,
        className: btn.className,
        purpose: this.props.t(btn.url) === this.props.t('lang_route:registration.trialFlow.subscriptionMessage') ? 'link' : 'submit',
        formName: 'LoginForm'
      })
    ))
    return buttons
  }

  submit = (values, loginRequest) => {
    const path = this.props.location.state || { from: { pathname: this.props.t('lang_route:mypages.account') } }
    const trialFlowUrl = this.props.t('lang_route:registration.trialFlow.subscriptionMessage')
    const email = values.email
    const password = Base64.encode(values.password)
    const data = {
      formData: {
        password,
        email,
      },
      formName: 'LoginForm',
      navigate: path.from.pathname,
      trialFlowUrl: trialFlowUrl
    }
    loginRequest(data)
  }

  render() {
    const { loginRequest, handleSubmit } = this.props
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <PageHeader heading={this.props.t('login.heading')} />
          <form onSubmit={handleSubmit((values) => this.submit(values, loginRequest))}>
            <BoxWrapper maxCols={5}>
              <div className="formLayout">
                <div className="formLayout__item">
                  <FormOption label={this.props.t('login.form.email.label')}>
                    <FormInput items={formConfig().email} />
                  </FormOption>
                </div>
                <div className="formLayout__item">
                  <FormOption label={this.props.t('login.form.password.label')}>
                    <FormInput items={formConfig().password} />
                  </FormOption>
                </div>
                <div className="formLayout__item">
                  <Link to={this.props.t('lang_route:mypages.forgotPasswordEmail')}>{this.props.t('login.forgotPassword')}</Link>
                </div>
              </div>

              {this.props.login.error &&
                <p className='text-has-error card_error'>{this.props.t(this.props.login.error)}</p>
              }

            </BoxWrapper>
            <StickyButton>
              <Button items={setButtonProperties(this.getButtonProperties())} fetching={this.props.login.fetching} />
            </StickyButton>
          </form>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    login: state.loginState
  }
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, { loginRequest })(reduxForm({
  form: 'LoginForm',
  validate
})(Login)))