import React from 'react'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import PageHeader from 'newui/components/PageHeader'

class ConfirmMessage extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.login'),
      text: this.props.t('forgotPasswordMessage.buttons.login'),
      className: 'btn--secondary',
    }
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <PageHeader heading={this.props.t('forgotPasswordMessage.heading')} description={this.props.t('forgotPasswordMessage.description')}>
            <Button items={setButtonProperties(this.getButtonProperties())} fetching={false} />
          </PageHeader>
        </Section>
      </MainApp>
    )
  }
}

export default withNamespaces(['lang_myPages'])(ConfirmMessage)