const validate = values => {
  const errors = {}

  if (!values.password) {
    errors.password = 'registerForm.email'
  }

  if (!values.confirmpassword) {
    errors.confirmpassword = 'registerForm.email'
  } else if (values.confirmpassword !== values.password) {
    errors.confirmpassword = 'registerForm.repeat_email'
  }

  return errors
}

export default validate