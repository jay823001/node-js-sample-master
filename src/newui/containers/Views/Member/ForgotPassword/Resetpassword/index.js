import React from 'react'
import { connect } from 'react-redux'
import validate from './validate'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import { bindActionCreators } from 'redux'
import UserModule from 'newui/domain/Modules/User'
import FormInput from 'newui/components/FormInput'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import FormOption from 'newui/components/FormOption'
import { formConfig } from './formConfig'
import { reduxForm } from 'redux-form'
import { Base64 } from 'js-base64'

class ResetPassword extends React.PureComponent {

  getButtonProperties = () => {
    let buttons = []
    this.props.t('resetPassword.buttons').map((btn) => (
      buttons.push({
        url: this.props.t(btn.url),
        text: btn.text,
        className: btn.className,
        purpose: btn.className === "btn--primary" ? 'submit' : 'link',
        formName: 'ResetPasswordForm'
      })
    ))
    return buttons
  }

  submit = (values) => {
    const authkey = new URLSearchParams(this.props.location.search).get('authkey')
    const id = new URLSearchParams(this.props.location.search).get('id')
    const password = Base64.encode(values.confirmpassword)
    const navigate = this.props.t('lang_route:mypages.login')
    const data = {
      authkey,
      id,
      password,
      navigate
    }

    this.props.resetPasswordRequest(data)
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <PageHeader heading={this.props.t('resetPassword.heading')} />
          <form onSubmit={handleSubmit(values => this.submit(values))}>
            <BoxWrapper maxCols={5}>
              <div className="formLayout">
                <div className="formLayout__item">
                  <FormOption label={this.props.t('resetPassword.form.password.label')}>
                    <FormInput items={formConfig().password} />
                  </FormOption>
                </div>
                <div className="formLayout__item">
                  <FormOption label={this.props.t('resetPassword.form.confirmpassword.label')}>
                    <FormInput items={formConfig().confirmpassword} />
                  </FormOption>
                </div>
              </div>

              {this.props.forgotPassword.error &&
                <p className='text-has-error card_error'>{this.props.t(this.props.forgotPassword.error)}</p>
              }

            </BoxWrapper>
            <StickyButton>
              <Button items={setButtonProperties(this.getButtonProperties())} fetching={this.props.forgotPassword.fetching} />
            </StickyButton>
          </form>
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    forgotPassword: state.forgotPassword
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { resetPasswordRequest: UserModule.actions.resetPasswordRequest },
    dispatch
  )
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, matchDispatchToProps)(reduxForm({
  form: 'ResetPasswordForm',
  validate
})(ResetPassword)))