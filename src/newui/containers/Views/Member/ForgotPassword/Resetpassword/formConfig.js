export const formConfig = () => {
  return {
    "password": {
      "component": "input",
      "name": "password",
      "type": "password",
      "maxLength": "50"
    },
    "confirmpassword": {
      "component": "input",
      "name": "confirmpassword",
      "type": "password",
      "maxLength": "50"
    }
  }
}