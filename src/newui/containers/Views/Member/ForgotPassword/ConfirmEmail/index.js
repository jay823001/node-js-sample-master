import React from 'react'
import { connect } from 'react-redux'
import validate from './validate'
import Button from 'newui/components/Button'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import Section from 'newui/components/Section'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties } from 'newui/utils/helperFunctions'
import { bindActionCreators } from 'redux'
import UserModule from 'newui/domain/Modules/User'
import FormInput from 'newui/components/FormInput'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import FormOption from 'newui/components/FormOption'
import { formConfig } from './formConfig'
import { reduxForm } from 'redux-form'

class ConfirmEmail extends React.PureComponent {

  getButtonProperties = () => {
    let buttons = []
    this.props.t('forgotPassword.buttons').map((btn) => (
      buttons.push({
        url: this.props.t(btn.url),
        text: btn.text,
        className: btn.className,
        purpose: this.props.t(btn.url) === this.props.t('lang_route:mypages.login') ? 'link' : 'submit',
        formName: 'ForgotPasswordForm'
      })
    ))
    return buttons
  }

  submit = (values) => {
    const data = {
      email: values.email,
      navigate: this.props.t('lang_route:mypages.forgotPasswordMessage')
    }
    this.props.forgotPasswordRequest(data)
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing={false} fullScreen={true}>
          <PageHeader heading={this.props.t('forgotPassword.heading')} description={this.props.t('forgotPassword.description')} />
          <form onSubmit={handleSubmit(values => this.submit(values))}>
            <BoxWrapper maxCols={5}>
              <div className="formLayout">
                <div className="formLayout__item">
                  <FormOption label={this.props.t('forgotPassword.form.email.label')}>
                    <FormInput items={formConfig().email} />
                  </FormOption>
                </div>
              </div>
            </BoxWrapper>
            <StickyButton>
              <Button items={setButtonProperties(this.getButtonProperties())} fetching={this.props.forgotPassword.fetching} />
            </StickyButton>
          </form>
        </Section>
      </MainApp >
    )
  }

}

function mapStateToProps(state) {
  return {
    forgotPassword: state.forgotPassword
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { forgotPasswordRequest: UserModule.actions.forgotPasswordRequest },
    dispatch
  )
}

export default withNamespaces(['lang_myPages'])(connect(mapStateToProps, matchDispatchToProps)(reduxForm({
  form: 'ForgotPasswordForm',
  validate
})(ConfirmEmail)))