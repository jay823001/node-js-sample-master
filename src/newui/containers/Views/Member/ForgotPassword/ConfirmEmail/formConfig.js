export const formConfig = () => {
  return {
    "email": {
      "component": "input",
      "name": "email",
      "type": "email",
      "maxLength": "50"
    }
  }
}