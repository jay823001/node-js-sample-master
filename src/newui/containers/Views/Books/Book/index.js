import React from 'react'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import BookGroup from 'newui/components/BookGroup'
import BoxWrapper from 'newui/components/BoxWrapper'
import BooksModule from 'newui/domain/Modules/Books'
import BookDetails from 'newui/components/BookDetails'
import StickyButton from 'newui/components/StickyButton'
import TwoUpBenefits from 'newui/components/TwoUpBenefits'
import PromotedChecklist from 'newui/components/PromotedChecklist'
import SubscriptionFeatures from 'newui/components/SubscriptionFeatures'

import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { withNamespaces } from 'react-i18next'

class Book extends React.Component {
  componentWillMount() {
    this.props.getBookInfo({ id: this.props.slug.match(/\d+$/)[0] })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.slug !== this.props.slug) {
      this.props.getBookInfo({ id: nextProps.slug.match(/\d+$/)[0] })
    }
  }

  checkAuth() {
    if (this.props.userDetails && !this.props.userDetails.isLoggedIn) {
      return true
    } else if (this.props.location.state && this.props.location.state.user && !this.props.location.state.user.isLoggedIn) {
      return true
    } else {
      return false
    }
  }

  showStickyButton = () => {
    const isbn = window.location.pathname.split('-').pop()
    let showStickyButton = true
    // harry potter books cant have free cta button
    if (
      [
        '9781781102381',
        '9781781102411',
        '9781781102367',
        '9781781102398',
        '9781781102428',
        '9781781102374',
        '9781781102404',
        '9781781108925',
        '9781781109908',
      ].indexOf(isbn) > -1
    ) {
      showStickyButton = false
    }
    return showStickyButton
  }


  render() {
    const { books, loaded, related } = this.props
    const showSickyButton = this.showStickyButton()

    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        {!loaded ? (
          <div className="loader loader--global" />
        ) : (
            <React.Fragment>
              <Helmet>
                <title>{books[0].title}</title>
                <meta name="description" content={books[0].descriptionfull} />
                <meta property="og:url" content={window.location.href} />
                <meta property="og:title" content={books[0].title} />
                <meta property="og:image" content={books[0].imageurl} />
                <meta property="og:description" content={books[0].descriptionfull} />
                <meta name="twitter:title" content={books[0].title} />
                <meta name="twitter:description" content={books[0].descriptionfull} />
                <meta itemprop="productID" content={books[0].isbn} />
              </Helmet>
              <Section maxCols={8}>
                <BookDetails {...this.props} user={this.props.userDetails} />
              </Section>
              <Section spacing={['normalBottom']}>
                <BoxWrapper maxCols={8}>
                  <PromotedChecklist />
                </BoxWrapper>
              </Section>
              {related[0] && related[0].data.length > 0 ? (
                <Section maxCols={8} spacing={['normalBottom']}>
                  <BookGroup
                    heading={this.props.t('bookDetails.bookGroups.series', {
                      series: books[0].series ? books[0].series : '',
                    })}
                    books={related[0].data.slice(0, 4)}
                    link={related[0].id}
                    type={books[0].type}
                  />

                </Section>
              ) : null}
              <Section maxCols={8} spacing={['normalBottom']}>
                <TwoUpBenefits />
              </Section>
              {related[2] && related[2].data.length > 0 ? (
                <Section maxCols={8} spacing={['normalBottom']}>
                  <BookGroup
                    heading={this.props.t('bookDetails.bookGroups.authors', {
                      authors: books[0].authors.join(' & '),
                    })}
                    books={related[2].data.slice(0, 4)}
                    link={related[2].id}
                    type={books[0].type}
                  />
                </Section>
              ) : null}
              {related[1] && related[1].data.length > 0 ? (
                <Section bg="white">
                  <BookGroup
                    heading={this.props.t('bookDetails.bookGroups.related')}
                    books={related[1].data}
                  />
                </Section>
              ) : null}
              <Section maxCols={10} spacing='normalTop'>
                <SubscriptionFeatures books={books} />
              </Section>
            </React.Fragment>
          )}
        {
          (this.checkAuth() && showSickyButton) &&
          <StickyButton stickyPhone={true} stickyDesktop={true}>
            <Link className="btn btn--primary" to={this.props.t('lang_route:registration.trialFlow.subscriptionMessage')}>{this.props.t('lang_common:cta')}</Link>
          </StickyButton>
        }
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    related: state.bookInfo.related,
    loaded: state.bookInfo.loaded,
    books: state.bookInfo.books,
    userDetails: state.userDetails
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ getBookInfo: BooksModule.actions.getBookInfo }, dispatch)
}

export default withNamespaces(['lang_books'])(
  connect(mapStateToProps, matchDispatchToProps)(Book)
)
