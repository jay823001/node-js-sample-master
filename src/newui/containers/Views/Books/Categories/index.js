import React from 'react'
import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import { bindActionCreators } from 'redux'

import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import BookList from 'newui/components/BookList'

import BooksModule from 'newui/domain/Modules/Books'

class Categories extends React.Component {
  componentWillMount() {
    const page = new URLSearchParams(this.props.location.search).get('page') || 1
    const type = this.props.location.pathname.includes(this.props.t('books.categoriesAudioBooks'))
      ? 2
      : 1
    this.props.getBookGroup({ id: this.props.slug, type: type, page: page ? page : 1 })
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.slug !== this.props.slug ||
      nextProps.location.search !== this.props.location.search ||
      nextProps.location.pathname !== this.props.location.pathname
    ) {
      const page = new URLSearchParams(nextProps.location.search).get('page')
      const type = nextProps.location.pathname.includes(this.props.t('books.categoriesAudioBooks'))
        ? 2
        : 1
      this.props.getBookGroup({ id: nextProps.slug, type: type, page: page ? page : 1 })
    }
  }

  render() {
    const {
      loaded,
      heading,
      location: { pathname },
    } = this.props

    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        {!loaded ? (
          <div className="loader loader--global" />
        ) : (
            <Section maxCols={10} fullScreen={false}>
              <BookList
                heading={heading}
                type={pathname.includes(this.props.t('books.categoriesAudioBooks')) ? 2 : 1}
                {...this.props}
                user={this.props.userDetails}
              />
            </Section>
          )}
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    categories: state.bookGroup.categories,
    allGroups: state.bookGroup.allGroups,
    loaded: state.bookGroup.loaded,
    groups: state.bookGroup.groups,
    books: state.bookGroup.books,
    userDetails: state.userDetails
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ getBookGroup: BooksModule.actions.getBookGroup }, dispatch)
}

export default withNamespaces(['lang_route'])(
  connect(mapStateToProps, matchDispatchToProps)(Categories)
)
