import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'
import { bindActionCreators } from 'redux'

import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'

import BookList from 'newui/components/BookList'

import BooksModule from 'newui/domain/Modules/Books'

class Search extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showAutoComplete: false,
      type: this.getStateType()
    }

    this.onSearch = this.onSearch.bind(this)
  }

  getStateType = () => {
    if (this.props.location.pathname.includes(this.props.t('books.categoriesEbooks'))) {
      return 1
    }

    if (this.props.location.pathname.includes(this.props.t('books.categoriesAudioBooks'))) {
      return 2
    }

    return 0
  }

  componentWillMount() {
    const query = new URLSearchParams(this.props.location.search).get('query')
    const page = new URLSearchParams(this.props.location.search).get('page')

    this.props.history.replace({
      search: query ? `?query=${query}${page ? `&page=${page}` : ''}` : '',
    })
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.location.key !== this.props.location.key) {
      const query = new URLSearchParams(nextProps.location.search).get('query')
      const page = new URLSearchParams(nextProps.location.search).get('page')

      this.props.getBookSearch({
        query: query,
        page: page,
        search: !this.state.showAutoComplete,
        type: nextProps.location.state,
      })
      
      if (nextProps.location.state === 0 || nextProps.location.state === 1 || nextProps.location.state === 2) {
        this.setState({type: nextProps.location.state })
      }
      
    }
  }

  onSearch(query, submit) {
    if (submit) {
      this.setState({ showAutoComplete: false })
    } else if (!this.state.showAutoComplete) {
      this.setState({ showAutoComplete: true })
    }

    this.props.history.replace({ search: query ? `?query=${query}` : '' })
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={10} fullScreen={false}>
          <BookList
            {...this.props}
            listLoaded={this.props.listLoaded}
            heading={this.props.t('bookSearch.search')}
            onSearch={_.debounce(this.onSearch, 600)}
            hideMenu
            user={this.props.userDetails}
            type={this.state.type}
          />
        </Section>
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    books: state.bookSearch.books,
    groups: state.bookSearch.groups,
    facets: state.bookSearch.facets,
    loaded: state.bookSearch.loaded,
    allGroups: state.bookSearch.allGroups,
    listLoaded: state.bookSearch.listLoaded,
    userDetails: state.userDetails
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ getBookSearch: BooksModule.actions.getBookSearch }, dispatch)
}

export default withNamespaces(['lang_books'])(
  connect(mapStateToProps, matchDispatchToProps)(Search)
)
