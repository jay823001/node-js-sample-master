import React from 'react'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import validate from './Registration.Validate'
import FormInput from 'newui/components/FormInput'
import UserModule from 'newui/domain/Modules/User'
import PageHeader from 'newui/components/PageHeader'
import BoxWrapper from 'newui/components/BoxWrapper'
import FormOption from 'newui/components/FormOption'
import FormCheckbox from 'newui/components/FormCheckbox'
import StickyButton from 'newui/components/StickyButton'
import SubmitButton from 'newui/components/Button/SubmitButton'

import { Base64 } from 'js-base64'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import { RegistrationFormConfig } from './Registration.Form.Config'
import {
  encryptPassword,
  getSubscriptionId,
  setButtonProperties
} from 'newui/utils/helperFunctions'

const userRegisterRequest = UserModule.actions.userRegisterRequest
const subscriptionDetailsRequest = UserModule.actions.subscriptionDetailsRequest

class RegistrationForm extends React.PureComponent {

  state = {
    accepterms: false,
    showCheckError: false,
    componentUpdated: false
  }

  /*
  **@event: Retrieve subscription details and set it
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */
  getSubscriptionDetails() {
    const subscriptionId = getSubscriptionId(this.props.selectedSubscription)
    const subscriptionDetails = {
      authkey: this.props.registrationKeys.authkey,
      subscriptionid: subscriptionId,
      navigate: this.props.t('lang_route:registration.trialFlow.checkoutOptions')
    }

    this.props.dispatch(subscriptionDetailsRequest(subscriptionDetails))
  }

  /*
  **@event: Set accept terms checkbox value
  **@return description: none
  **@return type: none
  **@parameter description: checked or not
  **@parameter type: Boolean
  */
  setAcceptTerms = (bool) => {
    this.setState({ accepterms: bool })
  }

  /*
  **@event: Submit register form values
  **@return description: none
  **@return type: none
  **@parameter description: form values
  **@parameter type: Object
  */
  submit = (values, userRegisterRequest) => {
    if (this.state.accepterms) {
      values.accepterms = this.state.accepterms
      const subscriptionId = getSubscriptionId(this.props.selectedSubscription)
      const data = {
        data: {
          email: values.email,
          password: encryptPassword(values.password),
          base64Password: Base64.encode(values.password)
        },
        subscriptionId: subscriptionId,
        formName: 'RegistrationForm',
        loggedIn: false,
        navigate: this.props.t('lang_route:registration.trialFlow.checkoutOptions'),
        myAccoutUrl: this.props.t('lang_route:mypages.myaccount')
      }
      // trigger action to local saga
      userRegisterRequest(data)

    } else {
      this.setState({ showCheckError: true })
    }
  }

  getButtonProperties = () => {
    return {
      url: null,
      text: this.props.t('register.trialFlow.createAccount.buttons.text'),
      className: 'btn--primary btn--arrow',
    }
  }

  /*
  **@event: Show loading icon on button until data comes through API
  **@return description: true false depending on fetching data or not
  **@return type: Boolean
  **@parameter description: none
  **@parameter type: none
  */
  getFetchingInfo = () => {
    if (this.props.registerDetails.fetching || this.props.subscriptionInfo.fetching) {
      return true
    } else {
      return false
    }
  }

  render() {
    const formConfig = RegistrationFormConfig()
    const { userRegisterRequest, handleSubmit, registerDetails } = this.props

    if (this.props.registrationKeys.authkey !== '' && this.props.subscriptionInfo.subscriptionInfo === null && !this.props.registerDetails.registerError
      && !this.state.componentUpdated) {
      this.getSubscriptionDetails()
      this.setState({ componentUpdated: true })
    }

    let termsAndPrivacy = this.props.t('register.trialFlow.createAccount.form.terms',
      {
        openUrlTag1: `<a href='${this.props.t('lang_route:termsofuse')}' target='_blank'>`, closeUrlTag1: '</a>',
        openUrlTag2: `<a href='${this.props.t('lang_route:privacypolicy')}' target='_blank'>`, closeUrlTag2: '</a>'
      }
    )
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>

        <Section maxCols='8' spacing='false'>
          <PageHeader
            label={this.props.t('register.trialFlow.createAccount.label')}
            heading={this.props.t('register.trialFlow.createAccount.heading')}
            description={this.props.t('register.trialFlow.createAccount.description')}
          />
          <form onSubmit={handleSubmit(values => this.submit(values, userRegisterRequest))}>
            <BoxWrapper maxCols={5} heading={this.props.t('register.trialFlow.createAccount.form.heading')}>

              <div className='formLayout'>
                <div className='formLayout__item'>
                  <FormOption label={this.props.t('register.trialFlow.createAccount.form.email')}>
                    <FormInput items={formConfig.forminput.email} />
                  </FormOption>
                </div>
                <div className='formLayout__item'>
                  <FormOption label={this.props.t('register.trialFlow.createAccount.form.confirmEmail')}>
                    <FormInput items={formConfig.forminput.confirmemail} />
                  </FormOption>
                </div>
                <div className='formLayout__item'>
                  <FormOption label={this.props.t('register.trialFlow.createAccount.form.password')}>
                    <FormInput items={formConfig.forminput.password} />
                  </FormOption>
                </div >
                <div className='formLayout__item'>
                  <FormCheckbox
                    label={termsAndPrivacy}
                    showcheckerror={this.state.showCheckError}
                    setacceptterms={(bool) => this.setAcceptTerms(bool)} />
                </div>
              </div >

              {registerDetails.registerError &&
                <p className='text-has-error card_error'>{this.props.t(registerDetails.registerError)}</p>
              }

            </BoxWrapper >
            <StickyButton bgColor={this.props.bgColor} stickyPhone={true} stickyDesktop={true}>
              <SubmitButton items={setButtonProperties(this.getButtonProperties())} fetching={this.getFetchingInfo()} formName='RegistrationForm' />
            </StickyButton>
          </form>
        </Section >
      </MainApp >

    )
  }
}

function mapStateToProps(state) {
  return {
    registerDetails: state.registrationKeys,
    registrationKeys: state.authKeys,
    subscriptionInfo: state.subscriptionInfo,
    selectedSubscription: state.selectedSubscription
  }
}

export default withNamespaces(['lang_register'])(withRouter(
  connect(mapStateToProps, { userRegisterRequest })(
    reduxForm({
      form: 'RegistrationForm',
      validate,
    })(RegistrationForm)
  ))
)
