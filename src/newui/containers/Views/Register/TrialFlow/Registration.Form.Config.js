export const RegistrationFormConfig = () => {
  return {
    "forminput": {
      "email": {
        "component": "input",
        "name": "email",
        "type": "email",
        "maxLength": "50"
      },
      "confirmemail": {
        "component": "input",
        "name": "confirmemail",
        "type": "email",
        "maxLength": "50"
      },
      "password": {
        "component": "input",
        "name": "password",
        "type": "password",
        "maxLength": "50"
      }
    }
  }
}