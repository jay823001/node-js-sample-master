import React from 'react'
import MainApp from 'newui/containers/Views'
import 'newui/assets/styles/main.scss'
import Section from 'newui/components/Section'
import Button from 'newui/components/Button'

import { withNamespaces } from 'react-i18next'
import { setButtonProperties } from 'newui/utils/helperFunctions'

class RegistrationMessage extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:registration.trialFlow.createAccount'),
      text: this.props.t('register.trialFlow.registrationMessage.buttons.text'),
      className: 'btn--primary btn--arrow'
    }
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <main className='app__main'>
          <Section maxCols={8} spacing={false} fullScreen={true}>
            <div className='registerStep'>
              <p className='registerStep__label'>{this.props.t('register.trialFlow.registrationMessage.label')}</p>
              <h1 className='registerStep__heading'>{this.props.t('register.trialFlow.registrationMessage.heading')}</h1>
              <p className='registerStep__description'>{this.props.t('register.trialFlow.registrationMessage.description')}</p>
              <div className='registerStep__actions'>
                <Button items={setButtonProperties(this.getButtonProperties())} />
              </div>
            </div>
          </Section>
        </main>
      </MainApp>
    )
  }
}

export default withNamespaces(['lang_register'])(RegistrationMessage)