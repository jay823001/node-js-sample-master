import React from 'react'
import RegistrationForm from './Registration.Form'
import PreRegistrationMessage from './Registration.Message'

import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'

class RegistrationPage extends React.PureComponent {

  render() {
    return (
      <React.Fragment>
        {
          this.props.isLoggedIn &&
          (this.props.isMember || this.props.isoldmember) &&
          <Redirect to={this.props.t('lang_route:mypages.myaccount')} />}

        {`${this.props.location.pathname}${this.props.location.hash}` === this.props.t('lang_route:registration.trialFlow.createAccountMessage') ? (
          <PreRegistrationMessage bgColor={this.props.bgColor} language={this.props.t('register.trialFlow.registrationMessage')} header={this.props.header} footer={this.props.footer} />
        ) : (
            <RegistrationForm bgColor={this.props.bgColor} language={this.props.t('register.trialFlow.createAccount')} header={this.props.header} footer={this.props.footer} />
          )
        }
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.account.loggedIn,
    isMember: state.account.userData.ismember,
    activeRegistration: state.signup.activeReg
  }
}

export default withNamespaces(['lang_register', 'lang_route'])(connect(mapStateToProps)(RegistrationPage))