export const FormConfig = () => {
  return {
    "forminput": {
      "vouchercode": {
        "component": "input",
        "name": "code",
        "type": "text",
        "normalize": "vouchercode"
      },
      "email": {
        "component": "input",
        "name": "email",
        "type": "email",
        "maxLength": "50"
      },
      "confirmemail": {
        "component": "input",
        "name": "confirmemail",
        "type": "email",
        "maxLength": "50"
      },
      "password": {
        "component": "input",
        "name": "password",
        "type": "password",
        "maxLength": "50"
      }
    }
  }
}