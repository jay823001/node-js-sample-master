import React from 'react'
import { reduxForm, Field } from 'redux-form'
import MainApp from 'newui/containers/Views'
import { withNamespaces } from 'react-i18next'
import FormInput from 'newui/components/FormInput'
import BoxWrapper from 'newui/components/BoxWrapper'
import FormOption from 'newui/components/FormOption'
import BulletList from 'newui/components/BulletList'
import Brand from 'newui/components/Layout/Brand/index'
import { imageHelper } from "newui/assets/mixins/ImageHelper"
import { FormConfig } from './CampaignForm.Config'
import validate from './CampaignFlow.Validate'
import { connect } from 'react-redux'
import { gup } from 'utils/helpFunctions'
import FormCheckbox from 'newui/components/FormCheckbox'
import SubmitButton from 'newui/components/Button/SubmitButton'
import StickyButton from 'newui/components/StickyButton'
import { setButtonProperties, encryptPassword } from 'newui/utils/helperFunctions'
import UserModule from 'newui/domain/Modules/User'
import { Base64 } from 'js-base64'
import renderField from 'newui/components/RenderField'
import { Redirect } from 'react-router-dom'

const userRegisterRequest = UserModule.actions.userRegisterRequest
const campaignDetailRequest = UserModule.actions.campaignDetailRequest

class CreateAccountPage extends React.PureComponent {

  state = {
    accepterms: false,
    showCheckError: false
  }

  getButtonProperties = () => {
    return {
      url: null,
      text: this.props.t('register.campaignFlow.createAccount.form.buttons.text'),
      className: 'btn--primary btn--arrow',
    }
  }

  /*
  **@event: Submit form values to backend
  **@return description: none
  **@return type: none
  **@parameter description: form values
  **@parameter type: Object
  */
  submit = (values, userRegisterRequest) => {

    const data = {
      data: {
        email: this.props.isLoggedIn ? this.props.userData.email : values.email,
        password: encryptPassword(values.password),
        base64Password: this.props.isLoggedIn ? this.props.userData.password : Base64.encode(values.password)
      },
      formName: 'CampaignRegistrationForm',
      navigate: this.props.t('lang_route:registration.campaignFlow.subscriptionMessage'),
      campaignCode: values.code,
      loggedIn: this.props.isLoggedIn
    }

    if (this.props.isLoggedIn) {
      return userRegisterRequest(data)
    } else {
      if (this.state.accepterms) {
        return userRegisterRequest(data)
      } else {
        this.setState({ showCheckError: true })
      }
    }

  }

  /*
  **@event: Set accept terms checkbox value
  **@return description: none
  **@return type: none
  **@parameter description: checked or not
  **@parameter type: Boolean
  */
  setAcceptTerms = (bool) => {
    this.setState({ accepterms: bool })
  }

  /*
  **@event: Show loading icon on button until data comes through API
  **@return description: true false depending on fetching data or not
  **@return type: Boolean
  **@parameter description: none
  **@parameter type: none
  */
  getFetchingInfo = () => {
    if (this.props.registerDetails.fetching) {
      return true
    } else {
      return false
    }
  }

  componentDidMount() {
    this.props.initialize({ loggedInStatus: this.props.isLoggedIn })

    // pre fill campaign code if provided as a url parameter
    let vouchercode = ''
    const voucher = gup('vouchercode', this.props.location.search)
    if (voucher) {
      vouchercode = decodeURIComponent(voucher)
    }
    this.props.initialize({ code: vouchercode })

    if (this.props.type === 'custom') {
      this.props.dispatch(campaignDetailRequest(this.props.location.pathname.split('/')[2]))
    }
  }

  /*
  **@event: Set terms & condition dynamically depending on url slug
  **@return description: html elements
  **@return type: html element
  **@parameter description: t&s text
  **@parameter type: String
  */
  getTermsForCustomCampaign(termsAndPrivacy) {
    if (this.props.isLoggedIn) {
      if (this.props.campaignDetails.campaignInfo) {
        return <span>
          <div className='formCheckbox__label' dangerouslySetInnerHTML={{ __html: termsAndPrivacy }} />
          <div className='formCheckbox__label' dangerouslySetInnerHTML={{ __html: this.props.campaignDetails.campaignInfo.termsText }} />
        </span>
      } else {
        return <div className='formCheckbox__label' dangerouslySetInnerHTML={{ __html: termsAndPrivacy }} />
      }
    } else {
      if (this.props.campaignDetails.campaignInfo) {
        return <span>
          <div className="formLayout__item">
            <FormCheckbox label={termsAndPrivacy}
              showcheckerror={this.state.showCheckError}
              setacceptterms={(bool) => this.setAcceptTerms(bool)} />
          </div>
          <div className='formCheckbox__label' dangerouslySetInnerHTML={{ __html: this.props.campaignDetails.campaignInfo.termsText }} />
        </span>
      } else {
        return <div className="formLayout__item">
          <FormCheckbox label={termsAndPrivacy}
            showcheckerror={this.state.showCheckError}
            setacceptterms={(bool) => this.setAcceptTerms(bool)} />
        </div>
      }
    }
  }

  /*
  **@event: Identufy background url for campaign landing page
  **@return description: css property with background image url
  **@return type: object
  **@parameter description: none
  **@parameter type: none
  */
  getBackgroundUrl() {
    if (!this.props.campaignDetails.fetching) {
      if (this.props.campaignDetails.campaignInfo && this.props.campaignDetails.campaignInfo.imageurl) {
        return {
          backgroundImage: `url(${this.props.campaignDetails.campaignInfo.imageurl})`
        }
      } else {
        return {
          backgroundImage: `url(${imageHelper('campaign/default-bg.jpg')})`
        }
      }
    }
  }

  render() {
    const { handleSubmit, userRegisterRequest, registerDetails } = this.props
    let termsAndPrivacy = this.props.t('register.trialFlow.createAccount.form.terms',
      {
        openUrlTag1: `<a href='${this.props.t('lang_route:termsofuse')}' target='_blank'>`, closeUrlTag1: '</a>',
        openUrlTag2: `<a href='${this.props.t('lang_route:privacypolicy')}' target='_blank'>`, closeUrlTag2: '</a>'
      }
    )
    return (
      this.props.location.pathname === this.props.t('lang_route:registration.campaignFlow.customCampaign') ? (
        <Redirect to={this.props.t('lang_route:registration.campaignFlow.createAccount')} />
      ) : (
          this.props.campaignDetails.error ? (
            <Redirect to={this.props.t('lang_route:registration.campaignFlow.createAccount')} />
          ) : (
              <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
                <div className="campaignPage">
                  <div className="campaignPage__main">
                    <div className="campaignPage__partner">
                      <div className="campaignPagePartner">
                        <div className="campaignPagePartner__backdrop" style={this.getBackgroundUrl()} />
                        <div className="campaignPagePartner__content">
                          <div className="campaignPagePartner__info">
                            <h2 className="campaignPagePartner__heading">
                              {this.props.isLoggedIn ? (
                                this.props.campaignDetails.campaignInfo ? (
                                  this.props.campaignDetails.campaignInfo.headertext
                                ) : (
                                    this.props.t('register.campaignFlow.createAccount.headingLoggedIn')
                                  )
                              ) : (
                                  this.props.campaignDetails.campaignInfo ? (
                                    this.props.campaignDetails.campaignInfo.headertext
                                  ) : (
                                      this.props.t('register.campaignFlow.createAccount.heading')
                                    )
                                )}
                            </h2>
                            {this.props.isLoggedIn ? (
                              this.props.campaignDetails.campaignInfo &&
                              <div className="campaignPagePartner__description">{this.props.campaignDetails.campaignInfo.tagtext}</div>
                            ) : (
                                this.props.campaignDetails.campaignInfo ? (
                                  <div className="campaignPagePartner__description">{this.props.campaignDetails.campaignInfo.tagtext}</div>
                                ) : (
                                    <div className="campaignPagePartner__description">{this.props.t('register.campaignFlow.createAccount.description')}</div>
                                  )
                              )
                            }
                          </div>
                          {this.props.campaignDetails.campaignInfo && this.props.campaignDetails.campaignInfo.partner_logo &&
                            <div className="campaignPagePartner__brand">
                              <img className="campaignPagePartner__logo" src={this.props.campaignDetails.campaignInfo.partner_logo} alt="partner-logo" />
                            </div>
                          }
                        </div>
                      </div>
                    </div>
                    <div className="campaignPage__register">
                      <div className="campaignPageRegister">
                        <div className="campaignPageRegister__logo">
                          <Brand color="blue" />
                        </div>
                        <div className="campaignPageRegister__checklist">
                          <BulletList items={this.props.t('register.campaignFlow.createAccount.bulletList')} />
                        </div>
                        <div className="campaignPageRegister__form">
                          <form onSubmit={handleSubmit(values => this.submit(values, userRegisterRequest))}>
                            <div className="hidden_form_field">
                              <Field name="loggedInStatus" type="text" component={renderField} />
                            </div>

                            <BoxWrapper maxCols={5} disclaimer={this.props.t('register.campaignFlow.createAccount.heading')}>
                              <div className="formLayout">
                                <div className="formLayout__item">
                                  <FormOption label={this.props.t('register.campaignFlow.createAccount.form.vouchercode')}>
                                    <FormInput items={FormConfig().forminput.vouchercode} />
                                  </FormOption>
                                </div>
                                {!this.props.isLoggedIn &&
                                  <React.Fragment>
                                    <div className="formLayout__item">
                                      <FormOption label={this.props.t('register.campaignFlow.createAccount.form.email')}>
                                        <FormInput items={FormConfig().forminput.email} />
                                      </FormOption>
                                    </div>
                                    <div className="formLayout__item">
                                      <FormOption label={this.props.t('register.campaignFlow.createAccount.form.confirmEmail')}>
                                        <FormInput items={FormConfig().forminput.confirmemail} />
                                      </FormOption>
                                    </div>
                                    <div className="formLayout__item">
                                      <FormOption label={this.props.t('register.campaignFlow.createAccount.form.password')}>
                                        <FormInput items={FormConfig().forminput.password} />
                                      </FormOption>
                                    </div>
                                  </React.Fragment>
                                }

                                {this.getTermsForCustomCampaign(termsAndPrivacy)}

                              </div>

                              {registerDetails.registerError &&
                                <p className='text-has-error card_error'>{this.props.t(registerDetails.registerError)}</p>
                              }

                            </BoxWrapper>
                            <StickyButton stickyPhone={true} stickyDesktop={true}>
                              <div className="campaignPageButton">
                                <SubmitButton items={setButtonProperties(this.getButtonProperties())} fetching={this.getFetchingInfo()} formName='CampaignRegistrationForm' />
                              </div>
                            </StickyButton>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </MainApp>
            )
        )
    )
  }
}

function mapStateToProps(state) {
  return {
    registerDetails: state.registrationKeys,
    isLoggedIn: state.userDetails.isLoggedIn,
    userData: state.userDetails.userInfo,
    campaignDetails: state.campaignDetails
  }
}

export default withNamespaces(['lang_register'])(
  connect(mapStateToProps, { userRegisterRequest })(
    reduxForm({
      form: 'CampaignRegistrationForm',
      validate,
    })(CreateAccountPage)))