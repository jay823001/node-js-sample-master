const formatVoucherCode = (value) => {
  if (!value) {
    return value
  }
  return value.trim()
}

export default formatVoucherCode
