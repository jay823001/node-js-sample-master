const validate = values => {
  const errors = {}

  if (!values.code) {
    errors.code = 'registerForm.campaigncode'
  }

  if (!values.loggedInStatus) {
    if (!values.email) {
      errors.email = 'registerForm.email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(values.email)) {
      errors.email = 'registerForm.email'
    }

    if (!values.confirmemail) {
      errors.confirmemail = 'registerForm.email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(values.confirmemail)) {
      errors.confirmemail = 'registerForm.email'
    } else if (values.confirmemail !== values.email) {
      errors.confirmemail = 'registerForm.repeat_email'
    }

    if (!values.password) {
      errors.password = 'registerForm.password'
    } else if (values.password.length < 4 || values.password.length > 25) {
      errors.password = 'registerForm.password_length'
    } else if (!/^[a-zA-Z0-9-@#$%_]+$/.test(values.password)) {
      errors.password = 'registerForm.password_chars'
    }
  }

  return errors
}

export default validate
