export const GetStartedConfig = (props) => {
  return {
    "getStarted": {
      "heading": props.t('getStarted.heading'),
      "description": props.t('getStarted.description'),
      "disclaimer": props.t('getStarted.disclaimer'),
      "buttons": {
        "text": props.t('getStarted.buttons.text')
      },
      "downloadItems": [
        {
          "src": "views/logo-download-app-store.png",
          "link": "https://itunes.apple.com/us/app/nextory/id993578896?l=sv&ls=1&mt=8",
          "target": "_blank"
        },
        {
          "src": "views/logo-download-google-play.png",
          "link": "https://play.google.com/store/apps/details?id=com.gtl.nextory",
          "target": "_blank"
        }
      ]
    }
  }
}