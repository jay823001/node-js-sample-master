import React from 'react'
import Media from 'newui/components/Media'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import Button from 'newui/components/Button'
import PageHeader from 'newui/components/PageHeader'
import DownloadButtons from 'newui/components/DownloadButtons'

import { withNamespaces } from 'react-i18next'
import { GetStartedConfig } from './GetStarted.Config'
import { setButtonProperties } from 'newui/utils/helperFunctions'

class GetStarted extends React.PureComponent {

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.account'),
      text: this.props.t('getStarted.buttons.text'),
      className: 'btn--secondary'
    }
  }

  render() {
    const getStartedConfig = GetStartedConfig(this.props).getStarted
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <Section maxCols={8} spacing='smallBottom' fullScreen={true}>
          <div className='getStarted'>
            <div className='getStarted__image'>
              <Media
                src='views/get-started-phones.png'
                width={600}
                height={540}
              />
            </div>
            <div className='getStarted__info'>
              <PageHeader
                heading={getStartedConfig.heading}
                description={getStartedConfig.description}
              />
            </div>
            <div className='getStarted__download'>
              <DownloadButtons items={getStartedConfig.downloadItems} />
            </div>
            <div className='getStarted__buttons'>
              <Button items={setButtonProperties(this.getButtonProperties())} />
            </div>
            <p className='getStarted__disclaimer'>{getStartedConfig.disclaimer}</p>
          </div>
        </Section>
      </MainApp>
    )
  }
}

export default withNamespaces(['lang_register'])(GetStarted)
