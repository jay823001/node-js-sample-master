export const UserDetailsConfig = () => {
  return {
    "firstName": {
      "component": 'input',
      "name": 'firstName',
      "type": 'text',
      "maxLength": 10,
      "placeholder": '',
      "validations": {
        "required": true,
        "maxlength": 5
      }
    },
    "lastName": {
      "component": 'input',
      "name": 'lastName',
      "type": 'text',
      "maxLength": 10,
      "placeholder": ''
    },
    "phoneNumber": {
      "component": 'input',
      "name": 'phoneNumber',
      "type": 'tel',
      "minLength": 7,
      "maxLength": 12,
      "placeholder": '',
      "normalize": "phoneNumber"
    }
  }
}