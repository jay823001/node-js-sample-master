import React from 'react'
import Config from 'newui/config'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import DataLayer from 'containers/App/datalayer'
import AdTraction from './UserDetails.Adtraction'
import PageHeader from 'newui/components/PageHeader'
import UserDetailsForm from './UserDetails.Form'

import { withNamespaces } from 'react-i18next'
import { cookies, getUrlSearchParameter } from 'newui/utils/helperFunctions'

class UserDetails extends React.PureComponent {

  state = {
    sendTrackingScripts: false,
    bgColor: 'secondaryTint1'
  }

  componentDidMount() {
    const orderid = getUrlSearchParameter('orderid', window.location.search)
    this.orderCheck(orderid)
    if (getUrlSearchParameter('campaignname', window.location.search)) {
      this.setState({ bgColor: 'secondaryTint2' })
    }
  }

  /*
  **@event: Check whether already gone to the page or have a cookie regarding that
  **@return description: none
  **@return type: none
  **@parameter description: orderID
  **@parameter type: Integer
  */
  orderCheck = orderid => {
    //send tracking code only once
    if (!cookies.get(`transresp${orderid}`)) {
      cookies.set(`transresp${orderid}`, orderid, { path: '/', maxAge: 86400 })
      this.setState({ sendTrackingScripts: true })
    }
  }

  render() {
    return (
      <MainApp bgColor={this.state.bgColor} header={this.props.header} footer={this.props.footer}>
        {this.state.sendTrackingScripts && <DataLayer />}
        {this.state.sendTrackingScripts && <AdTraction Language={this.props.lng} />}
        <Section maxCols={8} spacing={false}>
          <PageHeader
            heading={this.props.t('userDetails.heading')}
            description={this.props.t('userDetails.description', { free_trial_days: Config.free_trial_days })}
          />
          <UserDetailsForm bgColor={this.props.bgColor} header={this.props.header}/>
        </Section>
      </MainApp>
    )
  }
}

export default withNamespaces(['lang_register'])(UserDetails)
