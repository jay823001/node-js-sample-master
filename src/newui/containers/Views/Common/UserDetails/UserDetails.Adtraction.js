import md5 from 'js-md5'
import React from 'react'
import Config from 'newui/config'
import PropTypes from 'prop-types'

import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { getUrlSearchParameter } from 'newui/utils/helperFunctions'

const AdTraction = props => {
  let hashedemail = ''
  if (props.UserDetails && (Object.keys(props.UserDetails).length > 1)) {
    hashedemail = md5(props.UserDetails.email)
  }

  const ordernumber = getUrlSearchParameter('orderid', window.location.search)
  const cpn = getUrlSearchParameter('campaignname', window.location.search)
  const language = Config.adtraction[props.Language]

  let updateAddtraction = () => {
    window.ADT = window.ADT || {}
    window.ADT.Tag = window.ADT.Tag || {}
    window.ADT.Tag.t = 3
    window.ADT.Tag.c = language.currency
    window.ADT.Tag.tp = language.key
    window.ADT.Tag.am = 0 // this is set to zero since price isn't used for nextory adtraction
    window.ADT.Tag.ti = '' + ordernumber + ''
    window.ADT.Tag.xd = '' + hashedemail + ''
    window.ADT.Tag.cpn = cpn
    if (window.ADT.Tag.doEvent) {
      window.ADT.Tag.doEvent()
    }

  }
  return (
    <Helmet>
      {updateAddtraction()}
    </Helmet>
  )
}

AdTraction.propTypes = {
  routeSearch: PropTypes.string,
}

function mapStateToProps(state) {
  return {
    UserDetails: state.userDetails.userInfo,
    routeSearch: state.route.location.search,
  }
}

export default connect(mapStateToProps)(AdTraction)
