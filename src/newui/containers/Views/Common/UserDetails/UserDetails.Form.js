import React from 'react'
import FormInput from 'newui/components/FormInput'
import UserModule from 'newui/domain/Modules/User'
import FormOption from 'newui/components/FormOption'
import BoxWrapper from 'newui/components/BoxWrapper'
import StickyButton from 'newui/components/StickyButton'
import SubmitButton from 'newui/components/Button/SubmitButton'

import { Base64 } from 'js-base64'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { withNamespaces } from 'react-i18next'
import { history } from "store"
import { UserDetailsConfig } from './UserDetails.Config'
import { setButtonProperties } from 'newui/utils/helperFunctions'

const userUpdateRequest = UserModule.actions.userUpdateRequest

class UserDetailsForm extends React.PureComponent {

  identifyDefaultValue = (attr) => {
    if (attr) {
      return attr
    } else {
      return ''
    }
  }

  submit = (values, userUpdateRequest) => {
    let details = {}
    details.email = this.props.userDetails.email
    details.firstname = values.firstName ? values.firstName : this.identifyDefaultValue(this.props.userDetails.firstname)
    details.lastname = values.lastName ? values.lastName : this.identifyDefaultValue(this.props.userDetails.lastname)
    details.address = values.address ? values.address : this.identifyDefaultValue(this.props.userDetails.address)
    details.postcode = values.zipCode ? values.zipCode : this.identifyDefaultValue(this.props.userDetails.postcode)
    details.city = values.city ? values.city : this.identifyDefaultValue(this.props.userDetails.city)
    details.cellphone = values.phoneNumber ? values.phoneNumber : this.identifyDefaultValue(this.props.userDetails.cellphone)
    details.newsletter = ''
    details.authkey = this.props.registrationKeys.authkey
    details.password = values.password ? Base64.encode(values.password) : ''

    const nonFormData ={
      navigate: this.props.t('lang_route:mypages.getStarted'),
      formName: 'UserDetailsForm'
    }

    let sendData = {
      formData: details,
      nonFormData: nonFormData
    }
    
    userUpdateRequest(sendData)
  }

  getButtonProperties = () => {
    return {
      url: this.props.t('lang_route:mypages.getStarted'),
      text: this.props.t('userDetails.buttons.text'),
      className: 'btn--primary btn--arrow'
    }
  }

  componentDidMount() {
    window.onpopstate = function () {
      history.go(1);
    };
  }

  render() {
    const userDetailsConfig = UserDetailsConfig()

    const { userUpdateRequest, handleSubmit, userError, fetching } = this.props
    return (
      <form onSubmit={handleSubmit(values => this.submit(values, userUpdateRequest))}>
        <BoxWrapper maxCols={5}>
          <div className='formLayout'>
            <div className='formLayout__item'>
              <FormOption label={this.props.t('userDetails.form.firstName.label')}>
                <FormInput items={userDetailsConfig.firstName} />
              </FormOption>
            </div>
            <div className='formLayout__item'>
              <FormOption label={this.props.t('userDetails.form.lastName.label')}>
                <FormInput items={userDetailsConfig.lastName} />
              </FormOption>
            </div>
            <div className='formLayout__item'>
              <FormOption label={this.props.t('userDetails.form.phone.label')}>
                <FormInput items={userDetailsConfig.phoneNumber} />
              </FormOption>
            </div>
          </div>
          {userError &&
            <p className='text-has-error'>{this.props.t(userError)}</p>
          }
        </BoxWrapper>
        <StickyButton bgColor={this.props.bgColor} stickyPhone={true} stickyDesktop={true}>
          <SubmitButton items={setButtonProperties(this.getButtonProperties())} fetching={fetching} formName='UserDetailsForm' />
        </StickyButton>
      </form>
    )
  }
}

function mapStateToProps(state) {
  return {
    registrationKeys: state.authKeys,
    fetching: state.updateUser.fetching,
    userInfo: state.updateUser.userInfo,
    userError: state.updateUser.userError,
    LoggedInData: state.account.userData,
    userDetails: state.userDetails.userInfo
  }
}

export default withNamespaces(['lang_register'])(connect(mapStateToProps, { userUpdateRequest })(reduxForm({
  form: 'UserDetailsForm',
})(UserDetailsForm))
)
