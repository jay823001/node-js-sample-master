import React from 'react'
import Loader from 'newui/components/Loader'
import MainApp from 'newui/containers/Views'
import UserModule from 'newui/domain/Modules/User'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withNamespaces } from 'react-i18next'

class PaymentResponse extends React.PureComponent {
  processPaymentResponse = () => {
    let responseObject = {}
    let searchParams = new URLSearchParams(window.location.search)
    searchParams.forEach(function (value, key) {
      responseObject[key] = value
    })
    responseObject['successURL'] = this.props.userDetails.isLoggedIn ? this.props.t('lang_route:mypages.paymentSuccess') : this.props.t('lang_route:mypages.userDetails')
    this.props.createPaymentResponseRequest(responseObject)
  }
  componentDidMount() {
    this.processPaymentResponse()
  }
  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header}>
        <Loader />
      </MainApp>
    )
  }
}

function mapStateToProps(state) {
  return {
    authKeys: state.authKeys,
    userDetails: state.userDetails
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    { createPaymentResponseRequest: UserModule.actions.createPaymentResponseRequest },
    dispatch
  )
}

export default withNamespaces('lang_route')(connect(mapStateToProps, matchDispatchToProps)(PaymentResponse))