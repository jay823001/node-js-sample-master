import React from 'react'
import CheckoutOptions from './CheckoutOptions'
import PaymentOption from 'newui/components/PaymentOption'

import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'

class CheckoutPage extends React.PureComponent {

  setComponent() {
    let flow = 'trialFlow'
    let currentUrl = `${this.props.location.pathname}${this.props.location.hash}`
    let routeUrl = this.props.t(`lang_route:registration.${flow}.checkoutOptions`)
    if (this.props.flow === 'campaign') {
      flow = 'campaignFlow'
      routeUrl = this.props.t(`lang_route:registration.${flow}.checkoutOptions`)
    } else if (this.props.flow === 're-activate') {
      flow = 'reActivationFlow'
      routeUrl = this.props.t(`lang_route:mypages.${flow}.checkoutOptions`)
    }
    currentUrl = currentUrl.split('?')[0] //for cases where payments like trustly faily and the url contains 'card#steg-3?paymentstatus=failed'
    return currentUrl === routeUrl ? (
      <CheckoutOptions
        bgColor={this.props.bgColor}
        header={this.props.header}
        footer={this.props.footer}
        flow={this.props.flow}
        campaignInfo={this.props.campaignInfo} />
    ) : (
        <PaymentOption
          bgColor={this.props.bgColor}
          option={this.props.location}
          header={this.props.header}
          footer={this.props.footer}
          flow={this.props.flow}
          campaignInfo={this.props.campaignInfo} />
      )
  }


  render() {
    return (
      this.setComponent()
    )
  }
}

function mapStateToProps(state) {
  return {
    campaignInfo: state.campaignInfo,
  }
}

export default withNamespaces(['lang_route'])(connect(mapStateToProps)(CheckoutPage))


