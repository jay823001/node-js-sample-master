import React from 'react'
import BEMHelper from 'react-bem-helper'
import MainApp from 'newui/containers/Views'
import Section from 'newui/components/Section'
import UserModule from 'newui/domain/Modules/User'
import Loader from 'newui/components/Loader'
import IconsCaretRight from 'newui/components/Icons/CaretRight'

import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import { withNamespaces } from 'react-i18next'
import { imageHelper } from 'newui/assets/mixins/ImageHelper'
import { getDynamicText } from 'newui/utils/translationHelper'
import { checkoutOptionsConfig } from './CheckoutOptions.Config'
import {
  IdentifyFlow,
  getSubscriptionId
} from 'newui/utils/helperFunctions'

const subscriptionDetailsRequest = UserModule.actions.subscriptionDetailsRequest

class checkoutOptions extends React.PureComponent {

  /*
  **@event: Retrieve subscription details and set it
  **@return description: none
  **@return type: none
  **@parameter description: none
  **@parameter type: none
  */
  getSubscriptionDetails = () => {
    const subscriptionId = this.props.flow === 'change-payment' ? this.props.userDetails.subscriptiondetails.subscriptionid : getSubscriptionId(this.props.selectedSubscription)
    const subscriptionDetails = {
      authkey: this.props.registrationKeys.authkey,
      subscriptionid: subscriptionId,
      navigate: this.getNavigationURL(),
      flow: this.props.flow,
      ccode: this.props.flow === 'campaign' ? this.props.campaignInfo.code : null
    }
    this.props.dispatch(subscriptionDetailsRequest(subscriptionDetails))
  }

  getNavigationURL() {
    if (this.props.flow === 're-activate') {
      return this.props.t('lang_route:mypages.reActivationFlow.checkoutOptions')
    } else if (this.props.flow === 'change-payment') {
      return this.props.t('lang_route:mypages.checkoutOptions')
    } else {
      return this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.checkoutOptions`)
    }
  }

  checkRequirements() {
    let isActiveMember = (this.props.isLoggedIn && this.props.isMember) ? true : false
    if (isActiveMember && this.props.flow !== 'çampaign' && this.props.flow !== 're-activate'
      && this.props.flow !== 'change-payment' && this.props.isLoggedIn) {
      return <Redirect to={this.props.t('lang_route:mypages.myaccount')} />
    }
  }

  componentDidMount() {
    if (!this.props.subscriptionInfo.subscriptionInfo) {
      this.getSubscriptionDetails()
    }
  }

  render() {
    const classes = new BEMHelper('btn')
    const paymentTypes = checkoutOptionsConfig(this.props).paymenttypes
    return (
      <React.Fragment>
        {/** Redirect Logged in members who try to browse card paged to my account page */}
        {this.checkRequirements()}

        <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
          <Section maxCols={8} spacing='normalBottom' fullScreen={true}>
            <div className='registerStep'>
              {this.props.flow !== 'change-payment' &&
                <p className='registerStep__label'>{this.props.t(`register.${IdentifyFlow(this.props.flow)}.checkoutOptions.label`)}</p>
              }
              <h1 className='registerStep__heading'>{this.props.t('register.trialFlow.checkoutOptions.heading')}</h1>

              <div className='registerStep__content'>
                {getDynamicText(this.props, 'payment_reassurance')}
              </div>
              <div className='registerStep__content registerStep__content--maxCols4'>
                {!this.props.subscriptionInfo.subscriptionInfo || this.props.subscriptionInfo.fetching ? (
                  <Loader />
                ) : (
                    <div className='formLayout'>
                      {this.props.subscriptionInfo.subscriptionInfo.data.gateways.map((gatewayItem) => (
                        this.props.t('checkoutOptions.buttons').map((item, index) => (
                          paymentTypes[index].paymenttype === gatewayItem.gateway && (
                            <div className='formLayout__item' key={index}>
                              <Link
                                {...classes('', ['secondary', 'wide'])}
                                to={{
                                  pathname: paymentTypes[index].name,
                                  state: { paymentType: paymentTypes[index].paymenttype, }
                                }}
                              >
                                <div className='paymentBtn'>
                                  <div className='paymentBtn__label'>{item.text}</div>
                                  <div className='paymentBtn__providerList'>
                                    {paymentTypes[index].providers.map((provider, i) => (
                                      <img
                                        className='paymentBtn__providerItem'
                                        alt='paymentlogo'
                                        src={imageHelper(
                                          `payment/logo-${provider}.png`
                                        )}
                                        key={i}
                                      />
                                    ))}
                                  </div>
                                  <div className='paymentBtn__icon'>
                                    <IconsCaretRight />
                                  </div>

                                </div>
                              </Link>
                            </div>
                          )
                        ))
                      ))}
                    </div>
                  )}
              </div>
            </div>
          </Section>
        </MainApp>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    registrationKeys: state.authKeys,
    subscriptionInfo: state.subscriptionInfo,
    isLoggedIn: state.account.loggedIn,
    isMember: state.account.userData.ismember,
    activeRegistration: state.signup.activeReg,
    userDetails: state.userDetails.userInfo,
    selectedSubscription: state.selectedSubscription
  }
}

export default withNamespaces(['lang_register', 'lang_route'])(connect(mapStateToProps)(checkoutOptions))