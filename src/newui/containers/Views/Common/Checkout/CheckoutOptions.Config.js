import { IdentifyFlow } from 'newui/utils/helperFunctions'

export const checkoutOptionsConfig = (props) => {
  let checkoutUrl = props ? props.t(`lang_route:${getTranslationPath(props.flow)}.checkout`) : ''

  /**
   * NOTE: in order to add new payment types
   * 1. you meed to add them below
   * 2. Add them to all language translation files
   * 3. add the payment type images to assets
   */
  return {
    "paymenttypes": [
      {
        "name": checkoutUrl,
        "providers": [
          "visa",
          "mastercard",
          "amex"
        ],
        "id": 1,
        "paymenttype": "ADYEN"
      },
      {
        "name": checkoutUrl,
        "providers": [
          "trustly",
          "bankid"
        ],
        "id": 2,
        "paymenttype": "TRUSTLY"
      },
      {
        "name": checkoutUrl,
        "providers": [
          "sofort"
        ],
        "id": 3,
        "paymenttype": "SOFORT"
      },
      {
        "name": checkoutUrl,
        "providers": [
          "giropay"
        ],
        "id": 4,
        "paymenttype": "GIROPAY"
      },

    ]
  }
}

export const getTranslationPath = (flow) => {
  if (flow === 'change-payment') {
    return 'mypages'
  } else if (flow === 're-activate') {
    return `mypages.${IdentifyFlow(flow)}`
  } else {
    return `registration.${IdentifyFlow(flow)}`
  }
}