import React from 'react'
import PreSubscriptionMessage from './Subscription.Message'
import SubscriptionOptions from 'newui/components/SubscriptionTable'

import { connect } from 'react-redux'
import { withNamespaces } from 'react-i18next'

class Subscription extends React.PureComponent {

   setComponent() {
    const currentUrl = `${this.props.location.pathname}${this.props.location.hash}`
    let flowName = 'trialFlow'
    let routeUrl = this.props.t(`registration.${flowName}.subscriptionMessage`)
    if (this.props.flow === 'campaign') {
      flowName = 'campaignFlow'
      routeUrl = this.props.t(`registration.${flowName}.subscriptionMessage`)
    } else if (this.props.flow === 're-activate') {
      flowName = 'reActivationFlow'
      routeUrl = this.props.t(`mypages.${flowName}.subscriptionMessage`)
    }
    return currentUrl === routeUrl ? true : false
  }

  render() {
    const showSubscriptionMessage = this.setComponent()
    return (
      showSubscriptionMessage ? (
        <PreSubscriptionMessage bgColor={this.props.bgColor} header={this.props.header} flow={this.props.flow} campaignInfo={this.props.campaignInfo} footer={this.props.footer}/>
      ) : (
          <SubscriptionOptions bgColor={this.props.bgColor} header={this.props.header} flow={this.props.flow} campaignInfo={this.props.campaignInfo} footer={this.props.footer}/>
        )
    )
  }
}

function mapStateToProps(state) {
  return {
    campaignInfo: state.campaignInfo,
  }
}

export default withNamespaces(['lang_route'])(connect(mapStateToProps)(Subscription))