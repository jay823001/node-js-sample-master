import React from 'react'
import MainApp from 'newui/containers/Views'
import 'newui/assets/styles/main.scss'
import Section from 'newui/components/Section'
import Button from 'newui/components/Button'

import { withNamespaces } from 'react-i18next'
import { setButtonProperties, IdentifyFlow, IdentifyCampaign } from 'newui/utils/helperFunctions'
import { getCampaignPluralsToTranslate } from 'newui/utils/translationHelper'
import { getDynamicText } from 'newui/utils/translationHelper'

class SubscriptionMessage extends React.PureComponent {

  getButtonProperties = () => {
    let url = null
    if (this.props.flow === 'trial' || this.props.flow === 'campaign') {
      url = this.props.t(`lang_route:registration.${IdentifyFlow(this.props.flow)}.subscriptionBasic`)
    } else {
      url = this.props.t(`lang_route:mypages.${IdentifyFlow(this.props.flow)}.subscriptionBasic`)
    }
    return {
      url: url,
      text: this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionMessage.buttonItems.text`),
      className: 'btn--primary btn--arrow'
    }
  }

  getBulletPoints = () => {
    if (this.props.flow === 'trial') {
      return this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionMessage.bulletItems`)
    } else if (this.props.flow === 'campaign') {
      return this.props.t(`register.${IdentifyFlow(this.props.flow)}.${IdentifyCampaign(this.props.campaignInfo)}.subscriptionMessage.bulletItems`,
        getCampaignPluralsToTranslate(this.props.campaignInfo, 'choosePlan', this.props))
    } else {
      return this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionMessage.bulletItems`)
    }
  }

  render() {
    return (
      <MainApp bgColor={this.props.bgColor} header={this.props.header} footer={this.props.footer}>
        <main className='app__main'>
          <Section maxCols={8} spacing={false} fullScreen={true}>
            <div className='registerStep'>
              <p className='registerStep__label'>{this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionMessage.stepText`)}</p>
              <h1 className='registerStep__heading'>{this.props.t(`register.${IdentifyFlow(this.props.flow)}.subscriptionMessage.h1Text`)}</h1>
              <div className='registerStep__content'>
                {/* <BulletList items={this.getBulletPoints()}></BulletList>{getDynamicText(this.props, 'choosePlan')} */}
                {getDynamicText(this.props, 'choosePlan')}
              </div>
              <div className='registerStep__actions'>
                <Button items={setButtonProperties(this.getButtonProperties())}></Button>
              </div>
            </div>
          </Section>
        </main>
      </MainApp>
    )
  }
}
export default withNamespaces(['lang_register'])(SubscriptionMessage)
