import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import i18next from 'newui/i18n'

const PrivateRouteNew = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      rest.auth ? (
        <Component {...props} {...rest} />
      ) : (
          <Redirect
            to={{
              pathname: i18next.t('lang_route:mypages.login'),
              state: { from: props.location }
            }}
          />
        )
    }
  />
)

export default PrivateRouteNew