import moment from 'moment'
import i18next from 'i18next'
import captureException from 'raven-js'
import Languages from 'newui/translations'
import { reactI18nextModule } from 'react-i18next'
import { getLocaleBasedOnHost } from 'newui/utils/helperFunctions'

i18next
  .use(reactI18nextModule)
  .init({
    lng: getLocaleBasedOnHost(),
    fallbackLng: 'sv_SE',
    debug: false,
    resources: Languages,
    keySeperator: true,
    interpolation: {
      escapeValue: false,
      format: function (value, format) {
        // date formatting for locales
        if (format) {
          return moment(value).format(format)
        }
        return value
      }
    },
    returnObjects: true,
    react: {
      wait: true,
    },
  }, (error, t) => {
    if (error) {
      // Sending error to sentry
      captureException(error)
    }
  })

export default i18next
