import {
  getTrialExpiryDate,
  IdentifyFlow,
  IdentifyCampaign
} from "newui/utils/helperFunctions"
import React from "react"
import BulletList from "newui/components/BulletList"
import Config from "newui/config"

/*
 **@event: Identify pluarls for choose plan dynamic text
 **@return description: plural values to i18n
 **@return type: Object
 **@parameter description: campaignInformation, labelname, props
 **@parameter type: Object, String, Object
 */
export const getCampaignPluralsToTranslate = (
  campaignInfo,
  textName,
  props
) => {
  if (campaignInfo.type === "DISCOUNT") {
    if (textName === "choosePlan") {
      if (campaignInfo.free_days > 0) {
        return {
          value: campaignInfo.campaign.value,
          value_unit: campaignInfo.campaign.value_unit,
          period: campaignInfo.campaign.period,
          period_unit: getPluralUnitPeriod(
            campaignInfo.campaign.period,
            campaignInfo.campaign.period_unit,
            props
          ),
          days: campaignInfo.free_days
        }
      } else {
        return {
          value: campaignInfo.campaign.value,
          value_unit: campaignInfo.campaign.value_unit,
          period: campaignInfo.campaign.period,
          period_unit: getPluralUnitPeriod(
            campaignInfo.campaign.period,
            campaignInfo.campaign.period_unit,
            props
          )
        }
      }
    }
  } else if (campaignInfo.type === "FIXED") {
    if (textName === "choosePlan") {
      return {
        value: campaignInfo.campaign.value,
        value_unit: campaignInfo.campaign.value_unit,
        period: campaignInfo.campaign.period,
        period_unit: getPluralUnitPeriod(
          campaignInfo.campaign.period,
          campaignInfo.campaign.period_unit,
          props
        )
      }
    }
  } else if (campaignInfo.type === "FREE") {
    if (textName === "choosePlan") {
      return null
    }
  }
}

/*
 **@event: Convert Strings sent by API to i18n
 **@return description: converted i18n values
 **@return type: String
 **@parameter description: campaignPeriod, campaignUnit, props
 **@parameter type: Integer, String, Object
 */
export const getPluralUnitPeriod = (period, period_unit, props) => {
  if (period > 1) {
    if (period_unit === "MONTH") {
      return props.t("common.month_plural", { count: period })
    } else if (period_unit === "DAY") {
      return props.t("common.day_plural", { count: period })
    } else {
      return props.t("common.year_plural", { count: period })
    }
  } else {
    if (period_unit === "MONTH") {
      return props.t("common.month", { count: period })
    } else if (period_unit === "DAY") {
      return props.t("common.day", { count: period })
    } else {
      return props.t("common.year", { count: period })
    }
  }
}

/*
 **@event: Dynamic text function identify which part should get translated dynamically
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props, identifier to identify the part
 **@parameter type: Object, String
 */
export const getDynamicText = (props, textIdentifier, optionalParam = null) => {
  if (textIdentifier === "choosePlan") {
    return <BulletList items={getChoosePlanText(props)} />
  } else if (textIdentifier === "subscription_date_offer") {
    return getSubscription_date_offer(props)
  } else if (textIdentifier === "subscription_bullet_offer") {
    return getSubscription_bullet_offer(props)
  } else if (textIdentifier === "family_offer_clarification") {
    return getFamily_offer_clarification(props)
  } else if (textIdentifier === "payment_reassurance") {
    return getPayment_reassurance(props)
  } else if (textIdentifier === "payment_reassurance_step2") {
    return getPayment_reassurance_step2(props, optionalParam)
  } else if (textIdentifier === "full_offer_step1") {
    return getFull_offer_step1(props)
  } else if (textIdentifier === "full_offer_step2") {
    return getFull_offer_step2(props)
  } else {
    return null
  }
}

/*
 **@event: Dynamically change choosePlan text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getChoosePlanText = props => {
  if (props.flow === "trial") {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.subscriptionMessage.bulletItems`
    )
  } else if (props.flow === "campaign") {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
        props.campaignInfo
      )}.subscriptionMessage.bulletItems`,
      getCampaignPluralsToTranslate(props.campaignInfo, "choosePlan", props)
    )
  } else if (props.flow === "re-activate") {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.subscriptionMessage.bulletItems`
    )
  } else {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.subscriptionMessage.bulletItems`
    )
  }
}

/*
 **@event: Dynamically change subscription offer text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getSubscription_date_offer = props => {
  const subscriptionPeriod = getTrialExpiryDate()
  if (props.flow === "trial") {
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsBasic.price`,
      { date: subscriptionPeriod }
    )
  } else if (props.flow === "campaign") {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
        props.campaignInfo
      )}.subscriptionOptions.subscriptionOptionsBasic.price`,
      {
        date: props.campaignInfo.next_payment_date,
        value: props.campaignInfo.type === 'FREE' ? null : props.campaignInfo.campaign.value
      }
    )
  } else {
    // return trial flow header as default
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsBasic.price`,
      { date: subscriptionPeriod }
    )
  }
}

/*
 **@event: Dynamically change subscription offer bullet point text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getSubscription_bullet_offer = props => {
  if (props.flow === "trial") {
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsBasic.benefits`, { free_days: Config.free_trial_days }
    )[0].label
  } else if (props.flow === "campaign") {
    if (props.campaignInfo.type === "FREE") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.subscriptionOptions.subscriptionOptionsBasic.benefits`,
        { days: props.campaignInfo.free_days }
      )[0].label
    } else if (props.campaignInfo.type === "FIXED") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.subscriptionOptions.subscriptionOptionsBasic.benefits`,
        { value: props.campaignInfo.campaign.value }
      )[0].label
    } else if (props.campaignInfo.type === "DISCOUNT") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.subscriptionOptions.subscriptionOptionsBasic.benefits`,
        {
          value: props.campaignInfo.campaign.value,
          unitValue: props.campaignInfo.campaign.value_unit,
          period: props.campaignInfo.campaign.period,
          plural: getPluralUnitPeriod(
            props.campaignInfo.campaign.period,
            props.campaignInfo.campaign.period_unit,
            props
          ),
          days: props.campaignInfo.free_days
        }
      )[0].label
    }
  } else if (props.flow === 're-activate') {
    return props.flow
  } else {
    // return trial flow header as default
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsBasic.benefits`
    )[0].label
  }
}

/*
 **@event: Dynamically change Family offer header clarification text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getFamily_offer_clarification = props => {
  if (props.flow === "trial") {
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsFamily.subheading`
    )
  } else if (props.flow === "campaign") {
    if (props.campaignInfo.type === 'DISCOUNT') {
      if (props.campaignInfo.free_days > 0) {
        return props.t(
          `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
            props.campaignInfo
          )}.subscriptionOptions.subscriptionOptionsFamily.subheading`, { days: props.campaignInfo.free_days, value: props.campaignInfo.campaign.value }
        )
      } else {
        return props.t(
          `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
            props.campaignInfo
          )}.subscriptionOptions.subscriptionOptionsFamily.subheading`
        )
      }
    } else {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.subscriptionOptions.subscriptionOptionsFamily.subheading`
      )
    }
  } else if (props.flow === "re-activate") {
    // this return text from trial flow because there are no text changes in re active flow
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsFamily.subheading`
    )
  } else {
    return props.t(
      `register.${IdentifyFlow(
        props.flow
      )}.subscriptionOptions.subscriptionOptionsFamily.subheading`
    )
  }
}

/*
 **@event: Dynamically change payment page reassuarance text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getPayment_reassurance = props => {
  if (props.flow === "trial") {
    return (
      <BulletList
        items={props.t("register.trialFlow.checkoutOptions.bulletItems", {
          date: getTrialExpiryDate()
        })}
      />
    )
  } else if (props.flow === "change-payment") {
    return

  } else if (props.flow === "campaign") {
    if (props.campaignInfo.type === "FREE") {
      return (
        <p className="pageHeader__description">
          {props.t(
            `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
              props.campaignInfo
            )}.checkoutOptions.description`,
            { date: props.campaignInfo.next_payment_date }
          )}
        </p>
      )
    } else if (props.campaignInfo.type === "FIXED") {
      return (
        <p className="pageHeader__description">
          {props.t(
            `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
              props.campaignInfo
            )}.checkoutOptions.description`,
            {
              date: props.campaignInfo.next_payment_date,
              value: props.campaignInfo.campaign.value
            }
          )}
        </p>
      )
    } else if (props.campaignInfo.type === "DISCOUNT") {
      return (
        <p className="pageHeader__description">
          {props.t(
            `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
              props.campaignInfo
            )}.checkoutOptions.description`,
            {
              date: props.campaignInfo.next_payment_date,
              value: props.campaignInfo.campaign.value,
              valueUnit: props.campaignInfo.campaign.value_unit
            }
          )}
        </p>
      )
    }
  } else if (props.flow === "re-activate") {
    return (
      <BulletList
        items={props.t(`register.${IdentifyFlow(props.flow)}.checkoutOptions.bulletItems`)}
      />
    )
  } else {
    return (
      <BulletList
        items={this.props.t("register.trialFlow.checkoutOptions.bulletItems", {
          date: getTrialExpiryDate()
        })}
      />
    )
  }
}

/*
 **@event: Dynamically change payment page reassuarance text page top
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getPayment_reassurance_step2 = (props, params) => {
  if (props.flow === "trial") {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.checkoutOptions.description`,
      { date: getTrialExpiryDate() }
    )
  } else if (props.flow === "campaign") {
    if (props.campaignInfo.type === "FREE") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.checkoutOptions.description`,
        { date: props.campaignInfo.next_payment_date }
      )
    } else if (props.campaignInfo.type === "FIXED") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.checkoutOptions.description`,
        {
          date: props.campaignInfo.next_payment_date,
          value: props.campaignInfo.campaign.value
        }
      )
    } else if (props.campaignInfo.type === "DISCOUNT") {
      return props.t(
        `register.${IdentifyFlow(props.flow)}.${IdentifyCampaign(
          props.campaignInfo
        )}.checkoutOptions.description`,
        {
          date: props.campaignInfo.next_payment_date,
          value: props.campaignInfo.campaign.value,
          valueUnit: props.campaignInfo.campaign.value_unit
        }
      )
    }
  } else if (props.flow === "re-activate") {
    return null
  } else if (props.flow === "change-payment") {
    return props.t('lang_myPages:paymentDetails.checkout.description', { date: params })
  } else {
    return props.t(
      `register.${IdentifyFlow(props.flow)}.checkoutOptions.description`,
      { date: getTrialExpiryDate() }
    )
  }
}

/*
 **@event: Dynamically change full offer text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getFull_offer_step1 = details => {
  if (details.props.content.flow === "trial") {
    return details.props
      .t(
        `register.${IdentifyFlow(
          details.props.content.flow
        )}.paymentOption.summary.summaryDetails`,
        { free_trial_days: Config.free_trial_days }
      )
      .map((item, index) => <span key={index}>{item.value}</span>)
      
  } else if (details.props.content.flow === "campaign") {
    if (details.props.content.campaignInfo.type === "FREE") {
      return details.props
        .t(
          `register.${IdentifyFlow(details.props.content.flow)}.${IdentifyCampaign(
            details.props.content.campaignInfo
          )}.paymentOption.summary.summaryDetails`,
          { free_trial_days: details.props.content.campaignInfo.free_days }
        )
        .map((item, index) => <span key={index}>{item.value}</span>)
    } else if (details.props.content.campaignInfo.type === "FIXED") {
      return details.props
        .t(
          `register.${IdentifyFlow(details.props.content.flow)}.${IdentifyCampaign(
            details.props.content.campaignInfo
          )}.paymentOption.summary.summaryDetails`,
          { value: details.props.content.campaignInfo.campaign.value }
        )
        .map((item, index) => <span key={index}>{item.value}</span>)
    } else if (details.props.content.campaignInfo.type === "DISCOUNT") {
      return details.props
        .t(
          `register.${IdentifyFlow(details.props.content.flow)}.${IdentifyCampaign(
            details.props.content.campaignInfo
          )}.paymentOption.summary.summaryDetails`,
          {
            value: details.props.content.campaignInfo.campaign.value,
            valueUnit: details.props.content.campaignInfo.campaign.value_unit,
            period: details.props.content.campaignInfo.campaign.period,
            plural: getPluralUnitPeriod(
              details.props.content.campaignInfo.campaign.period,
              details.props.content.campaignInfo.campaign.period_unit,
              details.props
            ),
            days: details.props.content.campaignInfo.free_days,
            price: details.subscriptionPrice,
            date: details.props.content.campaignInfo.next_payment_date
          }
        )
        .map((item, index) => <span key={index}>{item.value}</span>)
    }
  } else if (details.props.content.flow === "re-activate") {

  } else {
    return details.props
      .t(
        `register.${IdentifyFlow(
          details.props.content.flow
        )}.paymentOption.summary.summaryDetails`,
        { free_trial_days: Config.free_trial_days }
      )
      .map((item, index) => <span key={index}>{item.value}</span>)
  }
}

/*
 **@event: Dynamically change full offer date and price text
 **@return description: dynamically changed values
 **@return type: Object
 **@parameter description: props
 **@parameter type: Object
 */
export const getFull_offer_step2 = details => {
  if (details.props.content.flow === "trial") {
    return details.props.t(
      `register.${IdentifyFlow(
        details.props.content.flow
      )}.paymentOption.summary.subscriptionPrice.date`,
      {
        date: details.props.content.trialexpirydate,
        price: details.subscriptionPrice
      }
    )
  } else if (details.props.content.flow === "campaign") {
    if (details.props.content.campaignInfo.type === "FREE") {
      return details.props.t(
        `register.${IdentifyFlow(
          details.props.content.flow
        )}.${IdentifyCampaign(
          details.props.content.campaignInfo
        )}.paymentOption.summary.subscriptionPrice.date`,
        {
          date: details.props.content.trialexpirydate,
          subscription_price: details.subscriptionPrice,
          end_date: details.props.content.campaignInfo.next_payment_date
        }
      )
    } else if (details.props.content.campaignInfo.type === "FIXED") {
      return details.props.t(
        `register.${IdentifyFlow(
          details.props.content.flow
        )}.${IdentifyCampaign(
          details.props.content.campaignInfo
        )}.paymentOption.summary.subscriptionPrice.date`,
        {
          date: details.props.content.trialexpirydate,
          subscription_price: details.subscriptionPrice,
          end_date: details.props.content.campaignInfo.next_payment_date
        }
      )
    } else if (details.props.content.campaignInfo.type === "DISCOUNT") {
      return details.props.t(
        `register.${IdentifyFlow(
          details.props.content.flow
        )}.${IdentifyCampaign(
          details.props.content.campaignInfo
        )}.paymentOption.summary.subscriptionPrice.date`,
        {
          subscription_price: details.subscriptionPrice,
          end_date: details.props.content.campaignInfo.next_payment_date
        }
      )
    }
  } else if (details.props.content.flow === "re-activate") {
    return details.props.t(
      `register.${IdentifyFlow(
        details.props.content.flow
      )}.paymentOption.summary.subscriptionPrice.date`,
      {
        price: details.subscriptionPrice
      }
    )
  } else {
    return details.props.t(
      `register.${IdentifyFlow(
        details.props.content.flow
      )}.paymentOption.summary.subscriptionPrice.date`,
      { date: details.props.content.trialexpirydate }
    )
  }
}
