import CryptoJS from 'crypto-js'
import i18next from 'newui/i18n'
import Config from 'newui/config'
import Cookies from 'universal-cookie'

import { store } from 'store'
import { Base64 } from 'js-base64'

export const getApiDataFromConfig = () => {
  localStorage.setItem('env', 'dev')
  if (Config.current_api === 0) {
    // Selects API Automatically

    if (window.location.hostname.includes('nextory')) {
      localStorage.setItem('env', 'live')
      return Config.api.production
    } else {
      return Config.api.development
    }
  } else {
    // Selects API manually
    if (Config.current_api === 1) {
      return Config.api.development
    } else {
      localStorage.setItem('env', 'live')
      return Config.api.production
    }
  }
}

export const getLocaleBasedOnHost = () => {
  const url = window.location.host
  const isAcceptance = url.includes('104.155.208.232')
  const tld = window.location.origin.split('.').pop()
  let locale
  if (tld === 'se' || (isAcceptance && (url.includes('82')))) {
    locale = 'sv_SE' // swedish locale
  } else if (tld === 'fi' || (isAcceptance && (url.includes('83')))) {
    locale = 'fi_FI' // finnish locale
  } else if (tld === 'de' || isAcceptance) {
    locale = 'de_DE' // german locale
  } else {
    locale = Config.locale // affects localhost only
  }
  localStorage.setItem('locale', locale)
  return locale
}

export const cookies = new Cookies()

// Encrypt given password
export const encryptPassword = (rawPass) => {
  let base64Key = 'FresTechKey12345'
  let key = CryptoJS.enc.Utf8.parse(base64Key)
  let password = CryptoJS.AES.encrypt(rawPass, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  return password.toString()
}

// Filter URL Search String >> Params (searchparamName , url)
export function getUrlSearchParameter(name, url) {
  if (!url) url = window.location.href
  name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]')
  const regexS = '[\\?&]' + name + '=([^&#]*)'
  const regex = new RegExp(regexS)
  const results = regex.exec(url)
  return results == null ? null : results[1]
}

/*
**@event: get subscription details selected by user
**@return description: none
**@return type: none
**@parameter description: none
**@parameter type: none
*/

export const getSubscriptionId = (selectedSubscription) => {
  const index = selectedSubscription.index
  if (selectedSubscription.isfamily) {
    switch (index) {
      case 0:
        return Config.subscriptionIds.family2
      case 1:
        return Config.subscriptionIds.family3
      case 2:
        return Config.subscriptionIds.family4
      default:
        return Config.subscriptionIds.family4
    }
  } else {
    switch (index) {
      case 0:
        return Config.subscriptionIds.silver
      case 1:
        return Config.subscriptionIds.gold
      default:
        return Config.subscriptionIds.gold
    }
  }
}

/*
**@event: set button properties
**@return description: none
**@return type: json
**@parameter description: json object parameters : url, text, className
**@parameter type: string
*/
export const setButtonProperties = (properties) => {
  if (properties.length > 1) {
    let buttons = []
    properties.forEach(function (btn) {
      buttons.push({
        "name": btn.url ? btn.url : '',
        "text": btn.text ? btn.text : '',
        "type": btn.className ? btn.className : '',
        "purpose": btn.purpose ? btn.purpose : null,
        "formName": btn.formName ? btn.formName : null,
        "buttonType": btn.buttonType ? btn.buttonType : 'submit'
      })
    })
    return buttons
  } else {
    return {
      "name": properties.url ? properties.url : '',
      "text": properties.text ? properties.text : '',
      "type": properties.className ? properties.className : '',
      "purpose": properties.purpose ? properties.purpose : null,
      "formName": properties.formName ? properties.formName : null,
      "buttonType": properties.buttonType ? properties.buttonType : 'submit'
    }
  }
}

/*
**@event: Calculate trialflow validity period
**@return description: Date that the trialflow expires
**@return type: Date
**@parameter description: none
**@parameter type: none
*/
export const getTrialExpiryDate = () => {
  const trialInterval = new Date(new Date().setDate(new Date().getDate() + Config.free_trial_days))
  return trialInterval.toISOString().substring(0, 10)
}

/*
**@event: Set cookies to the application
**@return description: none
**@return type: none
**@parameter description: cookie name, data to store as the cookie, cookie path
**@parameter type: String, Object, Object
*/
export const setCookies = (cookieName, cookieData, cookiePath) => {
  const cookies = new Cookies()
  cookies.set(cookieName, cookieData, cookiePath)
}

/*
**@event: Remove cookies from the application
**@return description: none
**@return type: none
**@parameter description: cookie name, cookie path
**@parameter type: String, object
*/
export const removeCookies = (cookieName, cookiePath) => {
  const cookies = new Cookies()
  cookies.remove(cookieName, cookiePath)
}

/*
**@event: Redirect to another url
**@return description: none
**@return type: none
**@parameter description: redirect href
**@parameter type: String
*/
export const redirectTo = (path) => {
  window.location.href = path
}

/*
**@event: Identify current flow of registration and return formatted string to help i18n
**@return description: current flow
**@return type: String
**@parameter description: flowname
**@parameter type: String
*/
export const IdentifyFlow = (flowname) => {
  if (flowname === 'trial') {
    return 'trialFlow'
  } else if (flowname === 'campaign') {
    return 'campaignFlow'
  } else if (flowname === 're-activate') {
    return 'reActivationFlow'
  } else {
    return 'trialFlow'
  }
}

export const IdentifyCampaign = (campaignName) => {
  if (campaignName.type === 'DISCOUNT') {
    if (campaignName.free_days > 0) {
      return 'discountFreeDays'
    } else {
      return 'discount'
    }
  } else if (campaignName.type === 'FIXED') {
    return 'fixed'
  } else if (campaignName.type === 'FREE') {
    return 'free'
  } else {
    return 'discount'
  }
}

export const GetFlowColor = () => {
  if (store.getState().campaignInfo) {
    return 'secondaryTint2'
  } else {
    return 'secondaryTint1'
  }
}

export const getbFlowUrls = () => {
  return [
    i18next.t('lang_route:registration.trialFlow.subscriptionMessage'),
    i18next.t('lang_route:registration.trialFlow.subscriptionBasic'),
    i18next.t('lang_route:registration.trialFlow.subscriptionFamily'),
    i18next.t('lang_route:registration.trialFlow.createAccountMessage'),
    i18next.t('lang_route:registration.trialFlow.createAccount'),
    i18next.t('lang_route:registration.trialFlow.checkoutOptions'),
    i18next.t('lang_route:registration.trialFlow.checkout'),
    i18next.t('lang_route:registration.campaignFlow.subscriptionMessage'),
    i18next.t('lang_route:registration.campaignFlow.subscriptionBasic'),
    i18next.t('lang_route:registration.campaignFlow.subscriptionFamily'),
    i18next.t('lang_route:registration.campaignFlow.createAccountMessage'),
    i18next.t('lang_route:registration.campaignFlow.createAccount'),
    i18next.t('lang_route:registration.campaignFlow.checkoutOptions'),
    i18next.t('lang_route:registration.campaignFlow.checkout'),
    i18next.t('lang_route:mypages.userDetails'),
    i18next.t('lang_route:mypages.getStarted'),
    i18next.t('lang_route:registration.campaignFlow.customCampaign'),
    i18next.t('lang_route:registration.campaignFlow.customCampaignGiven'),
    i18next.t('lang_route:mypages.reActivationFlow.subscriptionBasic'),
    i18next.t('lang_route:mypages.reActivationFlow.checkout'),
    i18next.t('lang_route:mypages.reActivationFlow.checkoutOptions'),
    i18next.t('lang_route:mypages.account'),
    i18next.t('lang_route:mypages.myDetails'),
    i18next.t('lang_route:mypages.cancelSubscriptionMessage'),
    i18next.t('lang_route:mypages.cancelSubscriptionReason'),
    i18next.t('lang_route:mypages.cancelSubscriptionCompleted'),
    i18next.t('lang_route:mypages.orders'),
    i18next.t('lang_route:mypages.receipt'),
    i18next.t('lang_route:mypages.mySubscription'),
    i18next.t('lang_route:mypages.changeMySubscriptionBasic'),
    i18next.t('lang_route:mypages.changeMySubscriptionFamily'),
    i18next.t('lang_route:mypages.subscriptionChangedMessage'),
    i18next.t('lang_route:mypages.offers'),
    i18next.t('lang_route:mypages.singleOffer'),
    i18next.t('lang_route:mypages.paymentDetails'),
    i18next.t('lang_route:mypages.checkoutOptions'),
    i18next.t('lang_route:mypages.checkout'),
    i18next.t('lang_route:mypages.login'),
    i18next.t('lang_route:mypages.forgotPasswordEmail'),
    i18next.t('lang_route:mypages.forgotPasswordMessage'),
    i18next.t('lang_route:paymentresponse'),
    i18next.t('lang_route:mypages.paymentSuccess'),
    i18next.t('lang_route:books.categoriesAudioBooks'),
    i18next.t('lang_route:books.categoriesEbooks'),
    i18next.t('lang_route:books.book'),
    i18next.t('lang_route:books.search'),
    i18next.t('lang_route:CMS.about'),
    i18next.t('lang_route:CMS.cookies'),
    i18next.t('lang_route:CMS.app'),
    i18next.t('lang_route:CMS.privacy_policy'),
    i18next.t('lang_route:CMS.terms'),
    "/",
    "/404"
  ]
}
/*
**@parameter type: String
**@return formated string
*/
export function ucFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export const processAdyenError = (errorObject, props) => {
  let errorDetails = {}
  if (errorObject.error) {
    switch (errorObject.fieldType) {
      case 'encryptedCardNumber':
        let cardError = null
        if (errorObject.error === 'luhn check failed') {
          cardError = props.t('lang_error:PaymentForm.cardnumber.unreal')
        } else if ('incomplete field') {
          cardError = props.t('lang_error:PaymentForm.cardnumber.invalid')
        }
        errorDetails['card'] = cardError
        break
      case 'encryptedExpiryMonth':
        errorDetails['expiryDate'] = props.t('lang_error:PaymentForm.expiryDate')
        break
      case 'encryptedExpiryYear':
        errorDetails['expiryDate'] = props.t('lang_error:PaymentForm.expiryDate')
        break
      case 'encryptedExpiryDate':
        errorDetails['expiryDate'] = props.t('lang_error:PaymentForm.expiryDate')
        break
      case 'encryptedSecurityCode':
        errorDetails['cvcNumber'] = props.t('lang_error:PaymentForm.cvvNumber')
        break
      default:
        errorDetails = null
    }
  }
  return errorDetails
}

export const getAppEnvironment = () => {
  return window.location.hostname.includes('nextory') ? 'live' : 'test'
}

export const getOriginKey = (subscriptionInfo) => {
  let originkey = 'pub.v2.8714314236569170.aHR0cDovL2xvY2FsaG9zdA.LWMb6RZp_EgVKZ_FCjJIpVWkvOMCtvJ39wgvTczs2e0' // hardcoded for localhost
  if (!window.location.origin.includes('localhost')) {
    const gateways = subscriptionInfo.subscriptionInfo.data.gateways
    gateways.forEach(function (item) {
      if (item.gateway === 'ADYEN') {
        originkey = item.originkey
      }
    })
  }
  return originkey
}

export const createUserCookieData = (UserDetails, authkey, refreshkey) => {
  const userdata = {}
  userdata.email = UserDetails.email
  userdata.p = UserDetails.password
  userdata.authkey = authkey
  userdata.refreshkey = refreshkey
  userdata.UserDetails = UserDetails
  return Base64.encode(JSON.stringify(userdata))
}

/*
 **@event: set BEM (scss)
 **@return: background color BEM
 **@parameter description: colorname
 **@parameter type: none
 */
export const uppercaseFirst = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1)
}
