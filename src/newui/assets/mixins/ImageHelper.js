export const imageHelper = (src, external) => {
  if (external) {
    return src
  }

  let images = require.context('../images', true)
  return images('./' + src)
}
