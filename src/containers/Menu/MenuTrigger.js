import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import { toggleMenu } from 'containers/App/actions/app-actions';

const TriggerWrapper = styled.div`
  position: relative;
  top: .3rem;
  float: right;
  width: 2.5rem;
  height: 2.0rem;
  margin-top: 1.3rem;
  text-align: left;
  ${media.large`
    display:none;
  `}
`;

const TriggerBars = styled.span`
  &,
  &:before,
  &:after {
    position: absolute;
    width: 2.5rem;
    height: .3rem;
    border-radius: 0;
    background-color: ${props => props.theme.colorBlue};
  }
  &:before,
  &:after {
    content: '';
  }
  & {
    top: 50%;
    margin-top: -.2rem;
    transition: transform 75ms cubic-bezier(.55, .055, .675, .19);
  }
  &:before {
    top: -.7rem;
    transition: top 75ms ease .12s, opacity 75ms ease;
  }
  &:after {
    bottom: -.7rem;
    transition: bottom 75ms ease .12s, transform 75ms cubic-bezier(.55, .055, .675, .19);
  }

  ${props => props.open && css`
    & {
      transition-delay: 120ms;
      transition-timing-function: cubic-bezier(.215, .61, .355, 1);
      -ms-transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
      transform: rotate(45deg);
    }
    &:before {
      top: 0;
      transition: top 75ms ease, opacity 75ms ease 120ms;
      opacity: 0;
    }
    &:after {
      bottom: 0;
      transition: bottom 75ms ease, transform 75ms cubic-bezier(.215, .61, .355, 1) .12s;
      -ms-transform: rotate(-90deg);
      -webkit-transform: rotate(-90deg);
      transform: rotate(-90deg);
    }
  `}
`;


class Menutrigger extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    toggleMenu: PropTypes.func,
    MenuOpen: PropTypes.bool,
  };

  render() {
    const { toggleMenu, MenuOpen } = this.props;

    return (
      <TriggerWrapper onClick={toggleMenu}>
        <TriggerBars open={MenuOpen} />
      </TriggerWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    MenuOpen: state.appstate.MenuOpen,
  };
}

export default connect(mapStateToProps, { toggleMenu })(Menutrigger);
