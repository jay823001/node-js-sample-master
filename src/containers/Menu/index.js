import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { closeMenu } from 'containers/App/actions/app-actions';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import ButtonLink from 'components/Buttons';
//import { BlueLogo } from './logo-blue';
import BlueLogo from 'containers/Menu/logo-rgb-blue.svg';
import Nav from './Nav';
import Menutrigger from './MenuTrigger';
import Translation from 'translation/nextory-web-se';

const Header = styled.header`
  width: 100%;
  box-shadow: ${props => (window.location.pathname === '/register/email' ||
    (window.location.pathname === '/register/subscription' && window.location.hash === '#steg-1') ||
    (window.location.pathname === '/register/subscription' && window.location.hash === '#familj') ||
    (window.location.pathname === '/register/subscription') ||
    (window.location.pathname === '/register-campaign/subscription') ||
    (window.location.pathname === '/register-campaign/card') ||
    (window.location.pathname === '/register/card')) ? props.theme.noBoxShadow : props.theme.navBoxShadow};

  position: relative;
  z-index: 666;
  display: block;
  width: 100%;
  height: 5rem;
  transition: 0.3s ease-in-out;
  background-color: ${props =>
    // (window.location.pathname === '/register/email') ? props.theme.colorMenuPink : props.theme.colorMenu
    (window.location.pathname === '/register/email') ? props.theme.colorMenuPink :
      window.location.pathname === '/register/subscription' && window.location.hash === '#familj' ? props.theme.greenBg :
        window.location.pathname === '/register/subscription' && window.location.hash === '#steg-1' ? props.theme.greenBg :
          window.location.pathname === '/register/subscription' ? props.theme.greenBg :
            window.location.pathname === '/register-campaign/subscription' ? props.theme.greenBg :
              window.location.pathname === '/register-campaign/card' ? props.theme.blueBg :
                window.location.pathname === '/register/card' ? props.theme.blueBg
                  : props.theme.colorMenu
  };

  ${media.large`
    height: 6.5rem;
  `};
`;

const HeaderInnerwrap = styled.div`
  max-width: 1200px;
  padding: 0 1.5rem;
  margin: auto;
  height: inherit;
  text-align: center;
  ${media.large`
    text-align: left;
  `};
`;

const StyledNav = styled.nav`
  height: 100vh;
  position: absolute;
  top: 4.9rem;
  right: 0;
  left: 0;
  display: ${props => (props.open ? 'block' : 'none')};
  width: 100%;
  z-index: 1;
  background-color: ${props => props.theme.colorMenu};
  padding: 2.7rem 0 0.7rem 0;
  ${media.large`
    position: relative;
    height: auto;
    top: 1.4rem;
    display: block;
    float: right;
    width: auto;
    padding: 0;
  `};
`;

const CTA = ButtonLink.extend`
  font-size: 1.3rem;
  padding: 0.5rem 0.9rem 0.6rem;
  position: relative;
  top: 0.9rem;
  ${media.xsmall`
  padding: 0.5rem 1.3rem 0.6rem;
  `} ${media.large`
    display:none;
  `};
`;

const BlueLogoDiv = styled.img`
  width: 9rem;
  height: 2.3rem;
  margin-top: 1.7rem;
  float:left;
  ${media.large`
    width: 16rem;
    height: 4.2rem;
    margin-top: 1.5rem;
    float:none;
  `}
`;

const BlueLogoDivWrapper = styled.div`
  text-align: center;
`;

const BlueLogoDivCentered = styled.img`
  width: 9rem;
  height: 2.3rem;
  margin: 2.5rem auto;

  ${media.large`
    width: 16rem;
    height: 4.2rem;
    margin-top: 3.5rem;
  `}
`;

class NavBar extends React.PureComponent {
  static propTypes = {
    Route: PropTypes.string,
    MenuOpen: PropTypes.bool,
    closeMenu: PropTypes.func,
    LoggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    GiftReg: PropTypes.string,
    activateAcc: PropTypes.object,
  };
  componentDidMount() {
    var elementExists = document.getElementById("allSearch");
    if (elementExists) {
      elementExists.focus();
    }

  }
  componentDidUpdate() {
    var elementExists = document.getElementById("allSearch");
    if (elementExists) {
      elementExists.focus();
    }

  }
  render() {
    const { Route, LoggedIn, ActiveReg, GiftReg } = this.props;

    let canActivate = false;
    if (typeof this.props.UserData.UserDetails !== 'undefined') {
      if (Object.keys(this.props.UserData.UserDetails).length > 1) {
        if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
          canActivate = true;
        }
      }
    }

    const isbn = this.props.Route.split('/').pop();
    let tryfreecta = true;
    // harry potter books cant have free cta button
    if (
      [
        '9781781102381',
        '9781781102411',
        '9781781102367',
        '9781781102398',
        '9781781102428',
        '9781781102374',
        '9781781102404',
        '9781781108925',
        '9781781109908',
      ].indexOf(isbn) > -1
    ) {
      tryfreecta = false;
    }
    //console.log(Route)

    return (
      !Route.includes('/app/') &&
      !Route.includes('kampanj/') &&
      !Route.includes('summer') &&
      Route !== '/b' &&
      Route !== '/b/' &&
      Route !== '/' && (
        <Header>
          <HeaderInnerwrap>
            {(Route.includes('register/subscription') ||
              Route.includes('register/email') ||
              Route.includes('register/card') ||
              Route.includes('register-campaign/subscription') ||
              Route.includes('register-campaign/email') ||
              Route.includes('register-campaign/card')) ?
              <BlueLogoDivWrapper>
                <Link to="/" onClick={this.props.closeMenu}>
                  <BlueLogoDivCentered src={BlueLogo} alt="Logo in Menu" />
                </Link>
              </BlueLogoDivWrapper>
              :
              <Link to="/" onClick={this.props.closeMenu}>
                <BlueLogoDiv src={BlueLogo} alt="Logo in Menu" />
              </Link>
            }

            {!Route.includes('register') &&
              !Route.includes('kampanj') &&
              !Route.includes('presentkort/card') &&
              !Route.includes('fail') &&
              !Route.includes('trustly') &&
              !Route.includes('logga-in') &&
              !Route.includes('glomt-losenord') &&
              !Route.includes('andra-losenord') &&
              !Route.includes('showConfirmationFrCampaign') &&
              !Route.includes('presentkort/subscription') && (
                <span>
                  {!LoggedIn &&
                    Route !== '/' &&
                    !ActiveReg &&
                    (tryfreecta ? (
                      <CTA small to="/register/subscription#steg-1">
                        {Translation.menu.buttons.try_free}
                      </CTA>
                    ) : (
                        <CTA small to="/register/subscription#steg-1">
                          {Translation.menu.buttons.listen_to_the_phone}
                        </CTA>
                      ))}

                  {canActivate &&
                    Route !== '/' &&
                    Route !== '/konto/aktivera-betalning' &&
                    Route !== '/konto/aktivera-abonnemang' &&
                    Route !== '/konto' &&
                    Route !== '/transresp' &&
                    Route !== '/konto/avslutat' &&
                    LoggedIn &&
                    GiftReg !== 'REDEEM_CAMPAIGN' &&
                    GiftReg !== 'REDEEM_GIFTCARD' && (
                      <CTA small to="/konto/aktivera-abonnemang">
                        {Translation.menu.buttons.enable_nextory_now}
                      </CTA>
                    )}

                  <Menutrigger />
                  <StyledNav open={this.props.MenuOpen}>
                    <Nav />
                  </StyledNav>
                </span>
              )}
          </HeaderInnerwrap>
        </Header>
      )
    );
  }
}

function mapStateToProps(state) {
  return {
    Route: state.route.location.pathname,
    MenuOpen: state.appstate.MenuOpen,
    activateAcc: state.account.userData.UserDetails,
    UserData: state.account.userData,
    ActiveReg: state.signup.activeReg,
    GiftReg: state.signup.userRegPayment,
    LoggedIn: state.account.loggedIn,
  };
}

export default connect(mapStateToProps, { closeMenu }, null, { pure: false })(NavBar);
