import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import styled, { css } from 'styled-components';
import { closeMenu } from 'containers/App/actions/app-actions';
import { createUserLogout } from 'containers/Views/Registration/actions';
import media from 'theme/styled-utils';
// import Search from 'containers/Search/searchform';
// import ButtonLink from 'components/Buttons';
import MenuItem from './MenuItem';
import Translation from 'translation/nextory-web-se';
import { internationalizationLanguage } from '../App/api';

const StyledMenu = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;

const StyledLink = styled(({ primary, framed, ...rest }) => <NavLink {...rest} />)`
  color: ${props => props.theme.colorBlack};
  font-size: 1.5rem;

  ${props =>
    props.primary &&
    css`
      color: ${props => props.theme.colorBlue};
    `} &:after {
    position: absolute;
    right: 0;
    bottom: -0.2rem;
    left: 0;
    display: inline-block;
    width: 0;
    height: 0.2rem;
    margin: auto;
    content: '';
    transition: all 0.15s ease;
    background-color: transparent;
  }
  &.active {
    color: ${props => props.theme.colorBlue};
    text-decoration: underline;
  }
  ${media.large`
    &.active {
      text-decoration:none;
    }
    &.active:after,
    &:hover:after {
      width: 100%;
      background-color: ${props => props.theme.colorBlue};
    }
  `};
`;

const StyledA = StyledLink.withComponent('a');

class Nav extends React.PureComponent {
  static propTypes = {
    closeMenu: PropTypes.func,
    Route: PropTypes.string,
    LoggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    createUserLogout: PropTypes.func,
    GiftReg: PropTypes.string,
    activateAcc: PropTypes.object,
  };

  state = {
    activeGiftReg: false,
  };

  clickHandler = () => {
    this.props.createUserLogout();
  };

  render() {
    // const { closeMenu, Route, LoggedIn, ActiveReg, GiftReg } = this.props;
    const { closeMenu } = this.props;

    // let canActivate = false;
    // if (typeof this.props.UserData.UserDetails !== 'undefined') {
    //   if (Object.keys(this.props.UserData.UserDetails).length > 1) {
    //     if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
    //       canActivate = true;
    //     }
    //   }
    // }

    // const isbn = this.props.Route.split('/').pop();
    // let tryfreecta = true;
    // // harry potter books cant have free cta button
    // if (
    //   [
    //     '9781781102381',
    //     '9781781102411',
    //     '9781781102367',
    //     '9781781102398',
    //     '9781781102428',
    //     '9781781102374',
    //     '9781781102404',
    //     '9781781108925',
    //     '9781781109908',
    //   ].indexOf(isbn) > -1
    // ) {
    //   tryfreecta = false;
    // }

    return (
      <StyledMenu>
        {/* <MenuItem onClick={closeMenu}>
          <StyledLink to="/e-bocker">{Translation.menu.navigation.ebooks}</StyledLink>
        </MenuItem> */}

        {/* <MenuItem onClick={closeMenu}>
          <StyledLink to="/ljudbocker">{Translation.menu.navigation.soundbooks}</StyledLink>
        </MenuItem> */}

        <MenuItem onClick={closeMenu}>
          {internationalizationLanguage === "SW" ? <StyledA rel="noopener noreferrer" target="_blank" href="https://support.nextory.se">
            {Translation.menu.navigation.help}
          </StyledA> : <StyledA rel="noopener noreferrer" target="_blank" href="https://support.nextory.se/hc/fi">
              {Translation.menu.navigation.help}
            </StyledA>}
        </MenuItem>

        {/* {LoggedIn && (
          <MenuItem accountbuttons onClick={closeMenu}>
            <ButtonLink framed small to="/konto">
              {Translation.menu.buttons.my_account}
            </ButtonLink>
          </MenuItem>
        )} */}

        {/* {ActiveReg &&
          !LoggedIn && (
            <MenuItem accountbuttons onClick={closeMenu}>
              <ButtonLink onClick={this.clickHandler} framed small to="/">
                {Translation.menu.buttons.logout}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          !ActiveReg && (
            <MenuItem accountbuttons onClick={closeMenu}>
              <ButtonLink framed small to="/logga-in">
                {Translation.menu.buttons.login}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          Route !== '/b' &&
          !ActiveReg &&
          (tryfreecta ? (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/register/subscription#steg-1">
                {Translation.menu.buttons.try_free}
              </ButtonLink>
            </MenuItem>
          ) : (

              <MenuItem calltoaction onClick={closeMenu}>
                <ButtonLink small to="/register/subscription#steg-1">
                  {Translation.menu.buttons.listen_to_the_phone}
                </ButtonLink>
              </MenuItem>
            ))} */}

        {/* {!LoggedIn &&
          Route !== '/b' &&
          GiftReg === 'REDEEM_GIFTCARD' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/presentkort/subscription">
                {Translation.menu.buttons.complete_registration}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          Route !== '/b' &&
          GiftReg === 'REDEEM_CAMPAIGN' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/register-campaign/subscription">
                {Translation.menu.buttons.complete_registration}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          Route !== '/b' &&
          ActiveReg &&
          GiftReg !== 'REDEEM_GIFTCARD' &&
          GiftReg !== 'REDEEM_CAMPAIGN' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/register/card">
                {Translation.menu.buttons.complete_registration}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {canActivate &&
          Route !== '/b' &&
          Route !== '/konto/aktivera-betalning' &&
          Route !== '/konto/aktivera-abonnemang' &&
          Route !== '/konto' &&
          Route !== '/transresp' &&
          Route !== '/konto/avslutat' &&
          LoggedIn &&
          GiftReg !== 'REDEEM_CAMPAIGN' &&
          GiftReg !== 'REDEEM_GIFTCARD' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/konto/aktivera-abonnemang">
                {Translation.menu.buttons.enable_nextory_now}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!Route.includes('bocker') &&
          !Route.includes('/kategori') &&
          !Route.includes('/search') &&
          !Route.includes('/series') &&
          !Route.includes('/contributor') &&
          !Route.includes('/alla') && (
            <MenuItem>
              <Search />
            </MenuItem>
          )} */}
      </StyledMenu>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    GiftReg: state.signup.userRegPayment,
    LoggedIn: state.account.loggedIn,
    Route: state.route.location.pathname,
    activateAcc: state.account.userData.UserDetails,
    UserData: state.account.userData,
  };
}

export default connect(mapStateToProps, { closeMenu, createUserLogout }, null, {
  pure: false,
})(Nav);
