import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { requestApiDataCategories } from 'containers/Views/Books/actions';
import Search from 'containers/Search/searchform';
import Type from 'containers/Filter/Dropdown/Type';
import Category from 'containers/Filter/Dropdown/Category';
import Sort from 'containers/Filter/Dropdown/Sort';

const FilterWrapper = styled.section`
  background-color: ${props => props.theme.colorWhite};
  position: relative;
  z-index: 511;
  padding: 1.3rem 1.5rem 0.5rem;
  ${media.large`
    padding: 2rem 1.5rem;
  `};
`;
const FilterInnerWrapper = styled.div`
  position: relative;
  max-width: 90rem;
  margin-right: auto;
  margin-left: auto;
`;

const DropdownGroup = styled.div`
  display: inline-block;
  width: 60%;
  user-select: none;
  ${media.large`
    margin-top: .9rem;
    float:right;
    width:auto;
  `};
`;

class Filter extends React.PureComponent {
  componentDidMount() {
    this.props.requestApiDataCategories();
  }

  render() {
    const {
      categories,
      category,
      activeCategory,
      sort,
      route,
      routehash,
      routesearch,
      type,
    } = this.props;

    return (
      <FilterWrapper>
        <FilterInnerWrapper>
          <Search inFilter />
          <DropdownGroup>
            {categories &&
              category && <Category activeCategory={activeCategory} categorylist={categories} />}

            {sort && <Sort route={route} routehash={routehash} routesearch={routesearch} />}

            {type && <Type route={route} routehash={routehash} routesearch={routesearch} />}
          </DropdownGroup>
        </FilterInnerWrapper>
      </FilterWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    categories: state.bookdata.categories,
    route: state.route.location.pathname,
    routesearch: state.route.location.search,
    routehash: state.route.location.hash,
  };
}

export default connect(mapStateToProps, { requestApiDataCategories })(Filter);
