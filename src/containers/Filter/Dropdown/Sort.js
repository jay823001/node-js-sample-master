import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { openDropdownSort } from 'containers/App/actions/app-actions';
import { SortIcon, ArrowDownIcon, ArrowUpIcon } from './Icons';
import Wrapper from './Wrapper';
import Title from './Title';
import Value from './Value';
import Translation from 'translation/nextory-web-se';

const DropDown = styled.ul`
  font-weight: 400;
  text-align: left;
  position: absolute;
  z-index: 510;
  top: 100%;
  right: 0;
  width: 160px;
  padding: 2.3rem 1.5rem 1.5rem;
  list-style: none;
  text-transform: none;
  border: .1rem solid ${props => props.theme.colorGrey};
  border-radius: .5rem;
  background-color: ${props => props.theme.colorWhite};
  box-shadow: 0 .1rem 1.0rem rgba(0,0,0,.2);
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  ${media.large`
    right: 13rem;
  `}

  li {
    line-height: 1.2;
    display: block;
    margin: 0 2% .9rem;
    cursor: initial;
    a {
      display: block;
      padding-bottom: .8rem;
      border-bottom: .1rem solid #d7d7d7;
      color: ${props => props.theme.colorBlack};
      &:hover {
        color: ${props => props.theme.colorBlue};
        border-bottom: .1rem solid ${props => props.theme.colorBlue};
      }
    }
  }
`;

class Sort extends React.PureComponent {
  static propTypes = {
    DropDownSort: PropTypes.bool,
    openDropdownSort: PropTypes.func,
    route: PropTypes.string,
    routehash: PropTypes.string,
    routesearch: PropTypes.string,
  };

  render() {
    const { DropDownSort, openDropdownSort, route, routesearch, routehash } = this.props;

    return (
      <Wrapper
        open={DropDownSort}
        onClick={openDropdownSort}
      >
        <SortIcon /><Title>{Translation.filter.sort.sort_by}:</Title>
        <Value>
          {!( routehash.includes('nyheter') || routehash.includes('titel')) &&
            <span>{Translation.filter.sort.hotness}</span>
          }
          {routehash.includes('nyheter') &&
            <span>{Translation.filter.sort.published}</span>
          }
          { routehash.includes('titel') &&
            <span>{Translation.filter.sort.title}</span>
          }
        </Value>
        {DropDownSort ? (
          <span>
            <ArrowUpIcon />
            <DropDown >
              <li><Link
                style={routehash === '' ? { color: '#2226db', borderBottom: '.1rem solid #2226db', pointerEvents: 'none' } : null}
                to={{ pathname: route, hash: '', search: routesearch }}
              >
              {Translation.filter.sort.hotness}
              </Link></li>
              <li><Link
                style={routehash === '#nyheter' ? { color: '#2226db', borderBottom: '.1rem solid #2226db', pointerEvents: 'none' } : null}
                to={{ pathname: route, hash: 'nyheter', search: routesearch }}
              >
              {Translation.filter.sort.published}
              </Link></li>
              <li><Link
                style={routehash === '#titel' ? { color: '#2226db', borderBottom: '.1rem solid #2226db', pointerEvents: 'none' } : null}
                to={{ pathname: route, hash: 'titel', search: routesearch }}
              >
              {Translation.filter.sort.title}
              </Link></li>
            </DropDown>

          </span>
        ) : (
          <ArrowDownIcon />
        )}
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    DropDownSort: state.appstate.DropDownSort,
  };
}

export default connect(mapStateToProps, { openDropdownSort })(Sort);

