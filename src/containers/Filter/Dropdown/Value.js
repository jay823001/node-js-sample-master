import styled from 'styled-components';
import media from 'theme/styled-utils';

const Value = styled.div`
  max-width: 9.6rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  display: inline-block;
  position: relative;
  ${media.xsmall`
    max-width: 6.8rem;
  `}
  ${media.medium`
    max-width: 25rem;
  `}
  ${media.large`
    max-width: 20rem;
  `}
  span {
    margin-left: .4rem;
    font-size: 1.4rem;
    bottom: 0.1rem;
    position: relative;
    ${media.large`
      font-size: 1.6rem;
      margin-left: .3rem;
    `}
  }
`;

export default Value;
