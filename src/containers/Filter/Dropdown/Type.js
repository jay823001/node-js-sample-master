import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { openDropdownType } from 'containers/App/actions/app-actions';
import { TypeIcon, ArrowDownIcon, ArrowUpIcon, BookIcon, SoundBookIcon } from './Icons';
import Wrapper from './Wrapper';
import Title from './Title';
import Value from './Value';
import Translation from 'translation/nextory-web-se';

const DropDown = styled.ul`
  font-weight: 400;
  text-align: left;
  position: absolute;
  z-index: 510;
  top: 100%;
  right: 0;
  width: 160px;
  padding: 2.3rem 1.5rem 1.5rem;
  list-style: none;
  text-transform: none;
  border: 0.1rem solid ${props => props.theme.colorGrey};
  border-radius: 0.5rem;
  background-color: ${props => props.theme.colorWhite};
  box-shadow: 0 0.1rem 1rem rgba(0, 0, 0, 0.2);
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;

  li {
    line-height: 1.2;
    display: block;
    margin: 0 2% 0.9rem;
    cursor: initial;
    a {
      display: block;
      padding-bottom: 0.8rem;
      border-bottom: 0.1rem solid #d7d7d7;
      color: ${props => props.theme.colorBlack};
      &:hover {
        color: ${props => props.theme.colorBlue};
        border-bottom: 0.1rem solid ${props => props.theme.colorBlue};
      }
    }
  }
`;

class Type extends React.PureComponent {
  static propTypes = {
    DropDownType: PropTypes.bool,
    openDropdownType: PropTypes.func,
    route: PropTypes.string,
    routesearch: PropTypes.string,
    routehash: PropTypes.string,
  };

  render() {
    const { DropDownType, openDropdownType, route, routesearch, routehash } = this.props;

    return (
      <Wrapper open={DropDownType} onClick={openDropdownType}>
        {!(
          route.includes('ljudbocker') ||
          route.includes('e-bocker') ||
          routesearch.includes('ljudbocker') ||
          routesearch.includes('e-bocker')
        ) && (
          <span>
            <SoundBookIcon />
            <Title>{Translation.filter.type.type}:</Title>
            <Value>
              <span>{Translation.filter.all}:</span>
            </Value>
          </span>
        )}
        {(route.includes('e-bocker') || routesearch.includes('e-bocker')) && (
          <span>
            <BookIcon />
            <Title>{Translation.filter.type.type}:</Title>
            <Value>
              <span>{Translation.filter.type.ebook}</span>
            </Value>
          </span>
        )}
        {(route.includes('ljudbocker') || routesearch.includes('ljudbocker')) && (
          <span>
            <TypeIcon />
            <Title>{Translation.filter.type.type}:</Title>
            <Value>
              <span>{Translation.filter.type.audiobook}</span>
            </Value>
          </span>
        )}

        {DropDownType ? (
          <span>
            <ArrowUpIcon />
            {route.includes('/kategori') || route.includes('/search') ? (
              <DropDown>
                <li>
                  <Link
                    style={
                      routesearch === ''
                        ? {
                            color: '#2226db',
                            borderBottom: '.1rem solid #2226db',
                            pointerEvents: 'none',
                          }
                        : null
                    }
                    to={{ pathname: route, search: '', hash: routehash }}
                  >
                    {Translation.filter.all}
                  </Link>
                </li>
                <li>
                  <Link
                    style={
                      routesearch === '?e-bocker'
                        ? {
                            color: '#2226db',
                            borderBottom: '.1rem solid #2226db',
                            pointerEvents: 'none',
                          }
                        : null
                    }
                    to={{ pathname: route, search: 'e-bocker', hash: routehash }}
                  >
                    {Translation.filter.type.ebook}
                  </Link>
                </li>
                <li>
                  <Link
                    style={
                      routesearch === '?ljudbocker'
                        ? {
                            color: '#2226db',
                            borderBottom: '.1rem solid #2226db',
                            pointerEvents: 'none',
                          }
                        : null
                    }
                    to={{ pathname: route, search: 'ljudbocker', hash: routehash }}
                  >
                    {Translation.filter.type.audiobook}
                  </Link>
                </li>
              </DropDown>
            ) : (
              <DropDown>
                <li>
                  <Link to="/bocker">{Translation.filter.all}</Link>
                </li>
                <li>
                  <Link to="/e-bocker">{Translation.filter.type.ebook}</Link>
                </li>
                <li>
                  <Link to="/ljudbocker">{Translation.filter.type.audiobook}</Link>
                </li>
              </DropDown>
            )}
          </span>
        ) : (
          <ArrowDownIcon />
        )}
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    DropDownType: state.appstate.DropDownType,
  };
}

export default connect(mapStateToProps, { openDropdownType })(Type);
