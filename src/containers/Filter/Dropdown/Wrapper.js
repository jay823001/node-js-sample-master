import styled from 'styled-components';
import media from 'theme/styled-utils';

const Wrapper = styled.div`
  font-size: 1.3rem;
  line-height: 1.5;
  margin-bottom: .8rem;
  cursor: pointer;
  text-align: right;
  color: ${props => props.open ? '#373ae0' : '#2226db'};
  * {
    vertical-align:top;
  }
  ${media.large`
    font-size: 1.5rem;
    display: inline-block;
    margin-bottom: 0;
    text-align: left;
  `}
`;

export default Wrapper;
