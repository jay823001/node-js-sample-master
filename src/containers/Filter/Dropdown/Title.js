import styled from 'styled-components';
import media from 'theme/styled-utils';

const Title = styled.span`
  display: none;
  ${media.xsmall`
    display:inline-block;
  `}
`;

export default Title;
