/* eslint prefer-template:0 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { openDropdownCat } from 'containers/App/actions/app-actions';
import { CategoryIcon, ArrowDownIcon, ArrowUpIcon } from './Icons';
import Wrapper from './Wrapper';
import Title from './Title';
import Value from './Value';
import Translation from 'translation/nextory-web-se';

const DropDown = styled.ul`
  font-weight: 400;
  position: absolute;
  z-index: 510;
  top: 100%;
  right: 0;
  width: 100%;
  padding: 2.3rem 1.5rem 1.5rem;
  list-style: none;
  text-transform: none;
  border: .1rem solid ${props => props.theme.colorGrey};
  border-radius: .5rem;
  background-color: ${props => props.theme.colorWhite};
  box-shadow: 0 .1rem 1rem rgba(0,0,0,.2);
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  text-align: left;
  ${media.medium`
    width: 56rem;
  `}
  ${media.large`
    width: 64rem;
  `}
  li {
    line-height: 1.2;
    width: 46%;
    display: inline-block;
    margin: 0 2% .9rem;
    cursor: initial;
    a {
      display: block;
      padding-bottom: .8rem;
      border-bottom: .1rem solid #d7d7d7;
      color: ${props => props.theme.colorBlack};
      &:hover {
        color: ${props => props.theme.colorBlue};
        border-bottom: .1rem solid ${props => props.theme.colorBlue};
      }
    }
  }
`;

const Foldout = styled.span`
  display: ${props => props.active ? 'inline-block' : 'none'};
`;

const Trigger = styled.button`
  padding: 0;
  font-family: "Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
  font-size: 1.3rem;
  line-height: 1.5;
  border: none;
  &:active,&:focus,&:visited {
    border: none;
    outline: none;
  }
  &:hover {
    cursor:pointer;
  }
  ${media.large`
    font-size: 1.5rem;
    display: inline-block;
    text-align: left;
  `}
`;

class Category extends React.PureComponent {
  static propTypes = {
    DropDownCat: PropTypes.bool,
    openDropdownCat: PropTypes.func,
    categorylist: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
    ]),
    activeCategory: PropTypes.string,
  };

  render() {
    const { DropDownCat, openDropdownCat, activeCategory, categorylist } = this.props;

    return (
      <Wrapper>
        <Trigger onClick={openDropdownCat}>
          <CategoryIcon /><Title>{Translation.filter.category}:</Title>
          <Value><span>
            {activeCategory || (Translation.filter.all)}
          </span></Value>
          {!DropDownCat && <ArrowDownIcon /> }
        </Trigger>
        <Foldout active={DropDownCat} onClick={openDropdownCat}>
          {DropDownCat && <ArrowUpIcon /> }
          <DropDown>
            <li><Link to="/bocker">{Translation.filter.all}</Link></li>
            {categorylist.filter(props => (
                props.parentcatid === 0
              )).map(({ categoryid, categoryname, slugname }) => (
                <li key={categoryid}>
                  <Link
                    style={activeCategory === categoryname ? { color: '#2226db', borderBottom: '.1rem solid #2226db' } : null}
                    to={'/kategori/' + slugname}
                  >{categoryname}</Link>
                </li>
              ))}
          </DropDown>
        </Foldout>


      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    DropDownCat: state.appstate.DropDownCat,
  };
}

export default connect(mapStateToProps, { openDropdownCat })(Category);
