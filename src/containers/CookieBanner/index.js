import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Translation from 'translation/nextory-web-se';
import { getbFlowUrls } from 'newui/utils/helperFunctions'

const Wrapper = styled.aside`
  background-color: ${props => props.theme.colorLightGrey};
  font-size: 1.2rem;
  position: fixed;
  z-index: 9999;
  bottom: 0;
  left: 0;
  right: 0;
  top: auto;
  ${media.medium`
    font-size: 1.4rem;
  `};
`;

const InnerWrapper = styled.div`
  max-width: 120rem;
  padding: 0.7rem 1.5rem .9rem 1.5rem;
  margin: auto;
  ${media.medium`
    padding: 1.5rem 1.5rem 1.1rem 1.5rem;
  `};
  ${media.xlarge`
  padding: 1.3rem 1.5rem 1.1rem 1.5rem;
  `};
`;

const TextWrap = styled.div`
  width: 100%;
  display: block;
  ${media.medium`
    width: 85%;
    display: inline-block;
    margin-right: 1%;
  `};
  ${media.xlarge`
    width: 90%;
  `};
`;

const StyledLink = styled(Link)`
  color: ${props => props.theme.colorBlue};
`;

const ButtonWrapper = styled.div`
  margin: .6rem 0 0 0;
  text-align: center;
  ${media.medium`
    float:right;
    margin: 0;
    height: 2rem;
  `};
`;

const Button = styled.button`
  border: none;
  padding: 0.9rem 1.2rem 1rem;
  line-height: 1;
  background-color: ${props => props.theme.colorGreen};
  border-radius: 1.5rem;
  color: white;
  cursor: pointer;
  box-shadow: 0 0.1rem 0.1rem rgba(0, 0, 0, 0.2);
  position: relative;
  
  &:focus {
    outline: none;
  }
  &:hover {
    background-color: #08af1e;
  }
  ${media.medium`
    top: 0.3rem;
  `};
  ${media.xlarge`
    top: -.8rem;
  `};
`;

class CookieBanner extends React.PureComponent {
  state = {
    cookiebanner: true,
    route: null,
  };

  componentWillMount() {
    this.checkCookie();
    this.setState({ route: window.location.pathname })
  }

  componentDidMount() {
    this.setState({ route: window.location.pathname })
    this.checkCookie();
  }

  acceptCookie = () => {
    const cookies = new Cookies();
    cookies.set('nextory-terms', 'accepted', { path: '/', maxAge: 12096000 });
    this.setState({ cookiebanner: false });
  };

  checkCookie = () => {
    const cookies = new Cookies();
    if (cookies.get('nextory-terms') || window.location.pathname.includes('/app/') || window.location.pathname.includes('/register/') || window.location.pathname.includes('/b')) {
      this.setState({ cookiebanner: false });
    } else {
      if (getbFlowUrls().includes(window.location.pathname)) {
        this.setState({ cookiebanner: false });
      } else {
        let displayBanner = true
        getbFlowUrls().forEach(function (path) {
          // TODO: This has to be removed once all new ui components imported
          const modifiedURL = (window.location.pathname).replace((window.location.pathname).split('/')[2], ':value')

          if (modifiedURL === path) {
            displayBanner = false
          }
        });
        this.setState({ cookiebanner: displayBanner });
      }
    }
  };

  render() {
    // Hide cookie banner for subscription pages
    if (this.state.route.includes('/register/')) {
      this.setState({ cookiebanner: false });
    } else {
      this.checkCookie()
    }
    return (
      this.state.cookiebanner && (
        <Wrapper>
          <InnerWrapper>
            <TextWrap>{Translation.cookiebanner.p1}
              <StyledLink to="/om-cookies"> {Translation.cookiebanner.link}</StyledLink>
            </TextWrap>
            <ButtonWrapper>
              <Button onClick={this.acceptCookie}>{Translation.cookiebanner.button}</Button>
            </ButtonWrapper>
          </InnerWrapper>
        </Wrapper>
      )
    );
  }
}


function mapStateToProps(state) {
  return {
    Route: state.route.location.pathname,
  };
}

export default connect(mapStateToProps)(CookieBanner);
