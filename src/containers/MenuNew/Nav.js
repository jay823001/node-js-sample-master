import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
// import { NavLink, Link } from 'react-router-dom';
import styled from 'styled-components';
// import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
// import mediaCustom from 'theme/styled-utils-custom';
import { internationalizationLanguage } from '../App/api'
import { closeMenu } from 'containers/App/actions/app-actions';
import { createUserLogout } from 'containers/Views/Registration/actions';
// import IconSearch from 'containers/Views/HomePageB/Icons/icon-search';
// import ButtonLink from 'components/Buttons';
import MenuItem from './MenuItem';
import Translation from 'translation/nextory-web-se';

const StyledMenu = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;

const StyledLink = styled(({ primary, framed, ...rest }) => <NavLink {...rest} />)`
  color: ${props => props.theme.black};
  font-size: 1.8rem;

  &:after {
    position: absolute;
    right: 0;
    bottom: -0.2rem;
    left: 0;
    display: inline-block;
    width: 0;
    height: 0.2rem;
    margin: auto;
    content: '';
    transition: all 0.15s ease;
    background-color: transparent;
  }
  &.active {
    text-decoration: none;
  }
  ${media.large`
    font-size: 1.7rem;
    &.active {
      text-decoration:none;
    }
    &:hover:after {
      width: 100%;
      background-color: ${props => props.theme.black};
    }
  `};
`;

const StyledA = StyledLink.withComponent('a');

// const SearchWrap = styled.span`
//   position: relative;
//   top: -0.2rem;
//   margin-left: 2rem;
//   transition: all 0.15s ease;
//   &:hover {
//     top: -0.4rem;
//   }
// `;
// const SubmenuWrapper = styled.div`
// background: #fff;
// position: absolute;
// width: 182px;
// height: 103px;
// color: #333333;
// padding: 5px 15px;
// border-radius: 8px;
// top: 35px;
// left: -48px;
// -webkit-transition: all 0.15s ease;
// -webkit-transition: all 0.15s ease;
// transition: all 0.15s ease;
// outline:none;
//   ${props => props.active && css`
//       display:block;
//     `}
//   ${mediaCustom.largemid`
//     display:block;
//     position: initial;
//     width: auto;
//     background: transparent;
//   `}
//   hr{
//     border: 1px solid #f1f1f1;
//     margin-top: 8px;
//     margin-bottom: 8px;
//   }
// `;
// const ArrowUp = styled.div`
//   width: 0px;
//   height: 0px;
//   border-left: 10px solid transparent;
//   border-right: 10px solid transparent;
//   border-bottom: 10px solid white;
//   position: relative;
//   top: -13px;
//   margin: auto;
//   ${mediaCustom.largemid`
//     display:none;
//   `}
// `;

// const SubLink = styled(({ primary, framed, ...rest }) => <NavLink {...rest} />)`
//   display: block;
//   padding: 3px 0px;
//   text-align: center;
//   font-size: 14.5px;
//   font-family: 'Maison';
//   color:${props => props.theme.black};
//   &:hover{
//     font-weight: bold;
//   }
// `;
// const DesktopMenuItems = styled.span`
//     ${mediaCustom.largemid`
//         display:none;
//     `}
// `;
// const MobileMenuItems = styled.span`
//   ${media.large`
//     display:none;
//   `}
// `;

class Nav extends React.PureComponent {
  static propTypes = {
    closeMenu: PropTypes.func,
    Route: PropTypes.string,
    LoggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    createUserLogout: PropTypes.func,
    GiftReg: PropTypes.string,
    activateAcc: PropTypes.object,
  };
  constructor() {
    super();
    //Main menu Dropdown handle
    this.handleClick = this.handleClick.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    //Dropdown State
    this.state = {
      popupVisible: false
    };
  }

  state = {
    activeGiftReg: false,
  };

  clickHandler = () => {
    this.props.createUserLogout();
  };

  //Click handle of the Menu dropdown
  handleClick() {

    if (!this.state.popupVisible) {
      // attach/remove event handler
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }

    this.setState(prevState => ({
      popupVisible: !prevState.popupVisible,
    }));
  }

  //Handle out side click on the component
  handleOutsideClick(e) {
    this.handleClick();
  }


  render() {
    // const { closeMenu, Route, LoggedIn, ActiveReg, GiftReg } = this.props;
    const { closeMenu } = this.props;

    // let canActivate = false;
    // if (typeof this.props.UserData.UserDetails !== 'undefined') {
    //   if (Object.keys(this.props.UserData.UserDetails).length > 1) {
    //     if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
    //       // canActivate = true;
    //     }
    //   }
    // }

    // const isbn = this.props.Route.split('/').pop();
    // let tryfreecta = true;
    // // hrry potter books cant have free cta button
    // if (
    //   [
    //     '9781781102381',
    //     '9781781102411',
    //     '9781781102367',
    //     '9781781102398',
    //     '9781781102428',
    //     '9781781102374',
    //     '9781781102404',
    //     '9781781108925',
    //     '9781781109908',
    //   ].indexOf(isbn) > -1
    // ) {
    //   tryfreecta = false;
    // }

    return (
      <StyledMenu>

        {/* <MenuItem onClick={closeMenu}>
          <StyledLink to="/e-bocker">{Translation.newhomepage.menu.navigation.ebooks}</StyledLink>
        </MenuItem> */}

        {/* <MenuItem onClick={closeMenu}>
          <StyledLink to="/ljudbocker">
            {Translation.newhomepage.menu.navigation.soundbooks}
          </StyledLink>
        </MenuItem> */}

        {/* <DesktopMenuItems> 
        <MenuItem >
          <StyledLink onClick={this.handleClick} to="#" >Presentkort</StyledLink>
          {this.state.popupVisible && (
          <SubmenuWrapper active={this.state.dropDown} ref='titleRef'>
          <ArrowUp/>
            <SubLink to="/presentkort/inlosen" onClick={closeMenu}> Lös in presentkort </SubLink>
            <hr/>
            <SubLink to="/presentkort/purchase" onClick={closeMenu}> Köp presentkort</SubLink>
          </SubmenuWrapper>
          )}
        </MenuItem>
        </DesktopMenuItems> */}

        {/* <MobileMenuItems> 
          <MenuItem>
            <StyledLink to="/presentkort/inlosen" onClick={closeMenu}>Lös in presentkort</StyledLink>
          </MenuItem>
          <MenuItem>
            <StyledLink to="/presentkort/purchase" onClick={closeMenu}>Köp presentkort</StyledLink>
          </MenuItem>
        </MobileMenuItems> */}


        <MenuItem onClick={closeMenu}>
          {internationalizationLanguage === "SW" ? <StyledA rel="noopener noreferrer" target="_blank" href="https://support.nextory.se">
            {Translation.menu.navigation.help}
          </StyledA> : <StyledA rel="noopener noreferrer" target="_blank" href="https://support.nextory.se/hc/fi">
              {Translation.menu.navigation.help}
            </StyledA>}
        </MenuItem>

        {/* {LoggedIn &&
        Route !== '/presentkort/card' && // Temp workaround for giftcard purchase 
        Route !== '/presentkort/thankyou' && // Temp workaround for giftcard purchase
        (
          <MenuItem onClick={closeMenu}>
            <StyledLink to="/konto">
              {Translation.newhomepage.menu.navigation.my_account}
            </StyledLink>
          </MenuItem>
        )} */}

        {/* {ActiveReg &&
          !LoggedIn && (
            <MenuItem onClick={closeMenu}>
              <StyledLink onClick={this.clickHandler} to="/">
                {Translation.newhomepage.menu.navigation.logout}
              </StyledLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          !ActiveReg && (
            <MenuItem onClick={closeMenu}>
              <StyledLink to="/logga-in">
                {Translation.newhomepage.menu.navigation.login}
              </StyledLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          Route !== '/' &&
          Route !== '/b' &&
          Route !== '/presentkort/inlosen' &&
          Route !== '/presentkort/subscription' &&
          Route !== '/presentkort/subscription#familj' &&
          Route !== '/presentkort/purchase' &&
          Route !== '/presentkort/yourorder' &&
          Route !== '/presentkort/thankyou' &&
          Route !== '/presentkort/card' &&
          !ActiveReg &&
          (tryfreecta ? (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/register/subscription#steg-1">
                {Translation.newhomepage.menu.buttons.try_free}
              </ButtonLink>
            </MenuItem>
          ) : (
              <MenuItem calltoaction onClick={closeMenu}>
                <ButtonLink small to="/register/subscription#steg-1">
                  {Translation.newhomepage.menu.buttons.listen_to_the_phone}
                </ButtonLink>
              </MenuItem>
            ))} */}


        {/* {!LoggedIn &&
          Route !== '/' &&
          Route !== '/b' &&
          Route !== '/presentkort/inlosen' &&
          Route !== '/presentkort/subscription' &&
          Route !== '/presentkort/subscription#familj' &&
          Route !== '/presentkort/purchase' &&
          Route !== '/presentkort/yourorder' &&
          Route !== '/presentkort/thankyou' &&
          Route !== '/presentkort/card' &&
          GiftReg === 'REDEEM_GIFTCARD' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/presentkort/subscription">
                {Translation.newhomepage.menu.buttons.complete_registration}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          Route !== '/' &&
          Route !== '/b' &&
          
          GiftReg === 'REDEEM_CAMPAIGN' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/register-campaign/subscription">
                {Translation.newhomepage.menu.buttons.complete_registration}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!LoggedIn &&
          Route !== '/' &&
          Route !== '/b' &&
          ActiveReg &&
          GiftReg !== 'REDEEM_GIFTCARD' &&
         
          GiftReg !== 'REDEEM_CAMPAIGN' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/register/card">
                {Translation.newhomepage.menu.buttons.complete_registration}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {canActivate &&
          Route !== '/' &&
          Route !== '/b' &&
          Route !== '/konto/aktivera-betalning' &&
          Route !== '/konto/aktivera-abonnemang' &&
          Route !== '/konto' &&
          Route !== '/transresp' &&
          Route !== '/konto/avslutat' &&
          Route !== '/presentkort/inlosen' &&
          LoggedIn &&
          GiftReg !== 'REDEEM_CAMPAIGN' &&
          GiftReg !== 'REDEEM_GIFTCARD' && (
            <MenuItem calltoaction onClick={closeMenu}>
              <ButtonLink small to="/konto/aktivera-abonnemang">
                {Translation.newhomepage.menu.buttons.enable_nextory_now}
              </ButtonLink>
            </MenuItem>
          )} */}

        {/* {!Route.includes('bocker') &&
          !Route.includes('/kategori') &&
          !Route.includes('/search') &&
          !Route.includes('/alla') && (
            <SearchWrap>
              <Link to="/search">
                <IconSearch />
              </Link>
            </SearchWrap>
          )} */}
      </StyledMenu>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    GiftReg: state.signup.userRegPayment,
    LoggedIn: state.account.loggedIn,
    Route: state.route.location.pathname,
    activateAcc: state.account.userData.UserDetails,
    UserData: state.account.userData,
  };
}

export default connect(mapStateToProps, { closeMenu, createUserLogout }, null, {
  pure: false,
})(Nav);
