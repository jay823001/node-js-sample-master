/* eslint consistent-return:0 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { REQUEST_API_DATA, REQUEST_API_DATA_EBOOK, REQUEST_API_DATA_SBOOK, REQUEST_API_DATA_CATEGORIES } from './constants';
import { receiveApiData, receiveApiDataEbook, receiveApiDataSbook, receiveApiDataCategories } from './actions';
import { fetchData, fetchDataEbook, fetchDataSbook, fetchDataCategories } from '../../App/api';


// Book Saga: will be fired on REQUEST_API_DATA actions
function* getApiData(authkey) {
  try {
    // do api call
    const books = yield call(fetchData, authkey);
    yield put(receiveApiData(books.data.bookgroups));
  } catch (e) {
    return e;
  }
}

export function* bookSaga() {
  yield takeLatest(REQUEST_API_DATA, getApiData);
}


// EbookSaga: will be fired on REQUEST_API_DATA_EBOOK actions
function* getApiDataEbook(authkey) {
  try {
    // do api call
    const ebooks = yield call(fetchDataEbook, authkey);
    yield put(receiveApiDataEbook(ebooks.data.bookgroups));
  } catch (e) {
    return e;
  }
}

export function* ebookSaga() {
  yield takeLatest(REQUEST_API_DATA_EBOOK, getApiDataEbook);
}


// SbookSaga: will be fired on REQUEST_API_DATA_EBOOK actions
function* getApiDataSbook(authkey) {
  try {
    // do api call
    const sbooks = yield call(fetchDataSbook, authkey);
    yield put(receiveApiDataSbook(sbooks.data.bookgroups));
  } catch (e) {
    return e;
  }
}

export function* sbookSaga() {
  yield takeLatest(REQUEST_API_DATA_SBOOK, getApiDataSbook);
}


// CategorySaga: will be fired on REQUEST_API_DATA_CATEGORIES actions
function* getApiDataCategories() {
  try {
    // do api call
	 
    const categories = yield call(fetchDataCategories);
    yield put(receiveApiDataCategories(categories.data.categories));
  } catch (e) {
    return e;
  }
}
export function* categorySaga() {
  yield takeLatest(REQUEST_API_DATA_CATEGORIES, getApiDataCategories);
}
