/*
 * Action creators
 */

import {
  REQUEST_API_DATA,
  RECEIVE_API_DATA,
  REQUEST_API_DATA_EBOOK,
  RECEIVE_API_DATA_EBOOK,
  REQUEST_API_DATA_SBOOK,
  RECEIVE_API_DATA_SBOOK,
  REQUEST_API_DATA_CATEGORIES,
  RECEIVE_API_DATA_CATEGORIES,
} from './constants';

// Book actions
export const requestApiData = (authkey) => ({ type: REQUEST_API_DATA, authkey });
export const receiveApiData = books => ({ type: RECEIVE_API_DATA, books });
export const requestApiDataEbook = (authkey) => ({ type: REQUEST_API_DATA_EBOOK, authkey });
export const receiveApiDataEbook = ebooks => ({ type: RECEIVE_API_DATA_EBOOK, ebooks });
export const requestApiDataSbook = (authkey) => ({ type: REQUEST_API_DATA_SBOOK, authkey });
export const receiveApiDataSbook = sbooks => ({ type: RECEIVE_API_DATA_SBOOK, sbooks });
export const requestApiDataCategories = () => ({ type: REQUEST_API_DATA_CATEGORIES });
export const receiveApiDataCategories = categories => ({ type: RECEIVE_API_DATA_CATEGORIES, categories });
