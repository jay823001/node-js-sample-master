import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Hero from 'components/Hero';
import HeroText from 'components/Hero/HeroText';
import H1 from 'components/Typography/H1';
import ImgHeroPhone from './e-bocker_phone-min.jpg';
import ImgHeroDesktop from './e-bocker-min.jpg';
import Translation from 'translation/nextory-web-se';

const StyledHero = styled(Hero)`
  background-image: url(${ImgHeroPhone});
  ${media.medium`
    background-image: url(${ImgHeroDesktop});
  `}
`;

const HeroBocker = () => (
  <StyledHero>
    <HeroText>
      <H1 huge>{Translation.ebooks.books.heading}</H1>
    </HeroText>
  </StyledHero>
);


export default HeroBocker;
