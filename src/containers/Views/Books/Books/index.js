import React from 'react';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import BookListWrapper from 'components/Books/BookList';
import Filter from 'containers/Filter';
import HeroBocker from './Hero/Hero';
import BookList from './BookList';
import Loading from 'components/LoadingIndicator/page';

class Books extends React.PureComponent {
  state = {
    seotitle: '',
    seodesc: '',
    loadingseo: true,
  };

  componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}page-meta?pageUrl=/bocker/${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          seotitle: data.data.pagemeta.metaList[0].value,
          seodesc: data.data.pagemeta.metaList[1].value,
          loadingseo: false,
        });
      }
    } catch (error) {
      this.setState({ loadingseo: false });
    }
  };

  render() {
    let dataFetched = false;

    if (!this.state.loadingseo) {
      dataFetched = true;
    }

    return dataFetched ? (
      <div>
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />
        <HeroBocker />
        <Filter category={true} type={true} />
        <BookListWrapper>
          <BookList />
        </BookListWrapper>
      </div>
    ) : (
        <Loading />
      );
  }
}

export default Books;
