import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import { apiUrl, internationalization_b, getHeaders } from 'containers/App/api';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';
import BookListWrapper from 'components/Books/BookList';
import BookSlider from 'components/Slider';
import SliderHead from 'components/Slider/SliderHead';
import BookGroup from 'components/Books/BookList/BookGroup';
import H1 from 'components/Typography/H1';
import Filter from 'containers/Filter';
import LoadingIndicator from 'components/LoadingIndicator';
import Book from 'components/Books/BookList/BookCover';
import CategoryIcon from './CatIcon';
import Translation from 'translation/nextory-web-se';

const CatH1 = styled(H1)`
  color: ${props => props.theme.colorDarkBlue};
  display: inline-block;
  margin-bottom: 2.8rem;
  ${media.medium`
    margin-bottom: 4rem;
  `};
`;

class Category extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
    match: PropTypes.object,
  };

  state = {
    error: '',
    booklist: [],
    loadmore: false,
    loading: true,
    page: 3,
    done: false,
    title: '',
  };

  componentDidMount() {
    this.checkAuth();

    const {
      match: { params },
    } = this.props;
    let format = -1;
    if (this.props.location.search === '?e-bocker') {
      format = 1;
    } else if (this.props.location.search === '?ljudbocker') {
      format = 2;
    }
    this.fetchFirstBooksliders(params.catslug, format);
  }

  componentWillReceiveProps(nextProps) {
    this.checkAuth();

    const prevPath = this.props.location.pathname.split('/');
    const prev = prevPath[prevPath.length - 1];
    const nextPath = nextProps.location.pathname.split('/');
    const next = nextPath[nextPath.length - 1];

    // check if we are changing format, not routing
    if (prev !== next || this.props.location.search !== nextProps.location.search) {
      const {
        match: { params },
      } = nextProps;
      let format = -1;
      if (nextProps.location.search === '?e-bocker') {
        format = 1;
      } else if (nextProps.location.search === '?ljudbocker') {
        format = 2;
      }
      this.setState({
        error: '',
        booklist: [],
        loadmore: false,
        loading: true,
        page: 3,
        done: false,
        title: '',
      });
      this.fetchFirstBooksliders(params.catslug, format);
    }
  }

  checkAuth = () => {
    if (new Cookies().get('user')) {
      const data = JSON.parse(Base64.decode(new Cookies().get('user')));
      const jwt = parseJwt(data.authkey);
      const current_time = Date.now() / 1000;
      if (jwt.exp < current_time) {
        return this.props.refreshAuth();
      }
    }
  };

  // get more booksliders via infinite loading
  getPosts = () => {
    const {
      match: { params },
    } = this.props;
    let format = -1;
    if (this.props.location.search === '?e-bocker') {
      format = 1;
    } else if (this.props.location.search === '?ljudbocker') {
      format = 2;
    }
    //setTimeout(() => {
    this.setState({ loadmore: true });
    //}, 2000);

    this.fetchBooksliders(this.state.page, params.catslug, format);
  };

  // fetch first two booksliders
  fetchFirstBooksliders = async (params, format) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    };
    try {
      const response = await fetch(
        `${apiUrl}categorySpecificLoadStep?pagenumber=1&format=${format}&windowsize=18&categoryslug=${params}${internationalization_b}`,
        {
          headers: getHeaders(authkey),
          credentials: 'same-origin',
        }
      );
      const data = await response.json();
      if ('bookgroups' in data.data) {
        this.setState({
          booklist: [...this.state.booklist, data.data.bookgroups],
          loading: false,
          title: data.data.title,
        });
        try {
          const response2 = await fetch(
            `${apiUrl}categorySpecificLoadStep?pagenumber=2&format=${format}&windowsize=18&categoryslug=${params}${internationalization_b}`,
            {
              headers: getHeaders(authkey),
              credentials: 'same-origin',
            }
          );
          const data2 = await response2.json();
          if ('bookgroups' in data2.data) {
            this.setState({
              booklist: [...this.state.booklist, data2.data.bookgroups],
              loading: false,
            });
          } else {
            this.setState({
              loadmore: false,
              done: true,
              loading: false,
              error: '',
            });
          }
        } catch (error) {
          this.setState({ loading: false, loadmore: false });
        }
      } else {
        this.setState({
          loadmore: false,
          done: true,
          loading: false,
          error: '',
        });
      }
    } catch (error) {
      this.setState({
        error: (Translation.ebooks.category.messages.error),
        loading: false,
        loadmore: false,
      });
    }
  };

  // fetch more books via infinite loading
  fetchBooksliders = async (page, params, format) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    };
    try {
      const response = await fetch(
        `${apiUrl}categorySpecificLoadStep?pagenumber=${page}&format=${format}&windowsize=18&categoryslug=${params}${internationalization_b}`,
        {
          headers: getHeaders(authkey),
          credentials: 'same-origin',
        }
      );
      const data = await response.json();

      if ('bookgroups' in data.data) {
        this.setState({
          booklist: [...this.state.booklist, data.data.bookgroups],
          loadmore: false,
          page: (this.state.page += 1),
        });
      } else {
        this.setState({
          loadmore: false,
          done: true,
          loading: false,
          error: '',
        });
      }
    } catch (error) {
      this.setState({
        error: (Translation.ebooks.category.messages.error),
        loading: false,
        loadmore: false,
      });
    }
  };

  // render initial book sliders
  renderBookGroups() {
    if (typeof this.state.booklist[0] === 'undefined') return false;
    const flatList = [].concat(...this.state.booklist);
    const catslug = this.props.location.pathname.replace('/kategori/', '');
    // eslint-disable-next-line
    flatList.map(c => (c.catslug = catslug));

    return flatList.map(({ name, slug, books, formattype, iscategory, catslug }) => (
      <BookGroup key={Math.random()}>
        {formattype.format === 1 &&
          (iscategory ? (
            <SliderHead heading={name} link={`/kategori/${catslug}/${slug}?e-bocker`} />
          ) : (
              <SliderHead heading={name} link={`/kategori/${catslug}/${slug}?e-bocker#nyheter`} />
            ))}

        {formattype.format === 2 &&
          (iscategory ? (
            <SliderHead heading={name} link={`/kategori/${catslug}/${slug}?ljudbocker`} />
          ) : (
              <SliderHead heading={name} link={`/kategori/${catslug}/${slug}?ljudbocker#nyheter`} />
            ))}

        {formattype.format === -1 &&
          (iscategory ? (
            <SliderHead heading={name} link={`/kategori/${catslug}/${slug}`} />
          ) : (
              <SliderHead heading={name} link={`/kategori/${catslug}/${slug}#nyheter`} />
            ))}

        <BookSlider>
          {books.map(({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
            const cleanurl = weburl.substring(0, weburl.indexOf('?'));
            return (
              <div key={bookid}>
                <Book
                  bookCover={coverimg}
                  booktitle={title}
                  url={cleanurl}
                  formattype={formattype.format}
                  relatedbook={relatedbookid}
                />
              </div>
            );
          })}
        </BookSlider>
      </BookGroup>
    ));
  }

  render() {
    const { error, loading, done, loadmore, booklist, title } = this.state;

    let SEOdescription = '';
    let SEOtitle = '';
    if (this.state.title) {
      SEOtitle = `${this.state.title} - ${Translation.ebooks.category.seotitle}`;
      SEOdescription = `${
        this.state.title
        } - ${Translation.ebooks.category.seodescription}`;
    }

    return (
      <div>
        <Helmet>
          <title>{SEOtitle}</title>
          <meta name="description" content={SEOdescription} />
          <meta property="og:title" content={SEOtitle} />
          <meta property="og:description" content={SEOdescription} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={SEOdescription} />
          <meta name="twitter:title" content={SEOtitle} />
        </Helmet>

        {booklist.length > 0 ? (
          <Filter category={true} type={true} activeCategory={title} />
        ) : (
            <Filter category={true} type={true} />
          )}
        <BookListWrapper>
          {error && (
            <span>
              <CatH1>{Translation.ebooks.category.messages.error}</CatH1>
            </span>
          )}

          {loading && (
            <span>
              <CatH1>{Translation.ebooks.category.messages.loading}</CatH1>
              <LoadingIndicator />
            </span>
          )}

          {!loading &&
            !error && (
              <div>
                <CategoryIcon />
                <CatH1>{title}</CatH1>
                {this.renderBookGroups()}
                {!done &&
                  !loadmore &&
                  booklist.length >= 2 && (
                    <Waypoint onEnter={this.getPosts} bottomOffset={'-150px'} />
                  )}
                {loadmore && <LoadingIndicator />}
                <DataLayer />
              </div>
            )}
        </BookListWrapper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    UserAuthkey: state.account.userData.authkey,
  };
}

export default connect(mapStateToProps, { refreshAuth })(Category);
