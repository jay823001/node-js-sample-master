import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import { connect } from 'react-redux';
import { apiUrl, internationalization_b, getHeaders } from 'containers/App/api';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import Waypoint from 'react-waypoint';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Filter from 'containers/Filter';
import H1 from 'components/Typography/H1';
import BookListWrapper from 'components/Books/BookList';
import Book from 'components/Books/BookList/BookCover';
import LoadingIndicator from 'components/LoadingIndicator';
import CategoryIcon from '../CatIcon';
import Translation from 'translation/nextory-web-se';

const CatH1 = styled(H1)`
  color: ${props => props.theme.colorDarkBlue};
  display: inline-block;
  margin-bottom: 2.8rem;
  ${media.medium`
    margin-bottom: 4rem;
  `};
`;

const CatpreHead = styled.h2`
  margin: 0 0 0.2rem 0;
  font-weight: 400;
  color: ${props => props.theme.colorDarkGrey};
  font-size: 1.5rem;
  font-style: italic;
  ${media.medium`
    font-size: 1.8rem;
    margin: 0 0 .5rem 0;
  `};
`;

const BookWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  div {
    margin: 0.5rem;
    ${media.medium`
      margin: 1rem;
    `};
  }
`;

class ListAllFromCategory extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
    match: PropTypes.object,
  };

  state = {
    booklist: [],
    listinfo: null,
    error: '',
    loading: true,
    loadmore: false,
    page: 1,
  };

  componentDidMount() {
    this.checkAuth();
    this.getPosts();
  }

  componentWillReceiveProps(nextProps) {
    this.checkAuth();
    const {
      match: { params },
    } = nextProps;
    const catslug = params.catslug;
    const subcatslug = params.subcatslug;

    let format = '';
    if (nextProps.location.search === '?e-bocker') {
      format = 1;
    } else if (nextProps.location.search === '?ljudbocker') {
      format = 2;
    }

    let sort = 'relevance';
    if (nextProps.location.hash === '#nyheter') {
      sort = 'publisheddate';
    } else if (nextProps.location.hash === '#titel') {
      sort = 'title';
    }

    this.setState({ page: 2 });
    this.fetchNewPosts(catslug, subcatslug, format, sort);
  }

  checkAuth = () => {
    if (new Cookies().get('user')) {
      const data = JSON.parse(Base64.decode(new Cookies().get('user')));
      const jwt = parseJwt(data.authkey);
      const current_time = Date.now() / 1000;
      if (jwt.exp < current_time) {
        return this.props.refreshAuth();
      }
    }
  };

  getPosts = () => {
    const {
      match: { params },
    } = this.props;
    const catslug = params.catslug;
    const subcatslug = params.subcatslug;

    let format = '';
    if (this.props.location.search === '?e-bocker') {
      format = 1;
    } else if (this.props.location.search === '?ljudbocker') {
      format = 2;
    }

    let sort = 'relevance';
    if (this.props.location.hash === '#nyheter') {
      sort = 'publisheddate';
    } else if (this.props.location.hash === '#titel') {
      sort = 'title';
    }
    this.fetchPosts(this.state.page, catslug, subcatslug, format, sort);
    this.setState({ loadmore: true });
  };

  fetchPosts = async (page, catslug, subcatslug, format, sort) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    };
    try {
      const response = await fetch(
        `${apiUrl}categoryViewAll?format=${format}&pagenumber=${page}&windowsize=50&sorton=${sort}&subcatslug=${subcatslug}&catslug=${catslug}${internationalization_b}`,
        {
          headers: await getHeaders(authkey),
          credentials: 'same-origin',
        }
      );
      const data = await response.json();
      if (data.data.books.length > 0) {
        this.setState({
          booklist: [...this.state.booklist, data.data.books],
          listinfo: data.data,
          error: null,
          loading: false,
          loadmore: false,
          page: (this.state.page += 1),
        });
      }
    } catch (error) {
      this.setState({
        loading: false,
        loadmore: false,
      });
    }
  };

  fetchNewPosts = async (catslug, subcatslug, format, sort) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    };
    try {
      const response = await fetch(
        `${apiUrl}categoryViewAll?format=${format}&pagenumber=1&windowsize=50&sorton=${sort}&subcatslug=${subcatslug}&catslug=${catslug}${internationalization_b}`,
        {
          headers: await getHeaders(authkey),
          credentials: 'same-origin',
        }
      );
      const data = await response.json();
      this.setState({
        booklist: [data.data.books],
        listinfo: data.data,
        error: null,
        loading: false,
        loadmore: false,
      });
    } catch (error) {
      this.setState({
        loading: false,
        loadmore: false,
      });
    }
  };

  renderBooks() {
    if (this.state.booklist === 0) return false;
    return this.state.booklist.map(bookgroup =>
      bookgroup.map(({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
        const cleanurl = weburl.substring(0, weburl.indexOf('?'));
        return (
          <div key={bookid}>
            <Book
              bookCover={coverimg}
              booktitle={title}
              url={cleanurl}
              formattype={formattype.format}
              relatedbook={relatedbookid}
            />
          </div>
        );
      })
    );
  }

  render() {
    const { loading, listinfo, error, loadmore } = this.state;

    let SEOtitle = '';
    let SEOdescription = '';
    if (this.state.listinfo && this.state.listinfo.parentcategoryname) {
      SEOtitle = `${this.state.listinfo.name} ${
        this.state.listinfo.parentcategoryname
        } - ${Translation.ebooks.category.listall.seotitle}`;
      SEOdescription = `${this.state.listinfo.name} ${
        this.state.listinfo.parentcategoryname
        } - ${Translation.ebooks.category.listall.seodescription}`;
    } else if (this.state.listinfo && !this.state.listinfo.parentcategoryname) {
      SEOtitle = `${this.state.listinfo.name} - ${Translation.ebooks.category.listall.seotitle}`;
      SEOdescription = `${
        this.state.listinfo.name
        } -${Translation.ebooks.category.listall.seodescription}`;
    }

    return (
      <div>
        <Helmet>
          <title>{SEOtitle}</title>
          <meta name="description" content={SEOdescription} />
          <meta property="og:title" content={SEOtitle} />
          <meta property="og:description" content={SEOdescription} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={SEOdescription} />
          <meta name="twitter:title" content={SEOtitle} />
        </Helmet>

        {!loading &&
          !error &&
          listinfo.parentcategoryname && (
            <Filter
              category={true}
              sort={true}
              type={true}
              activeCategory={listinfo.parentcategoryname}
            />
          )}

        {!loading &&
          !error &&
          !listinfo.parentcategoryname && (
            <Filter category={true} sort={true} type={true} activeCategory={listinfo.name} />
          )}

        <BookListWrapper>
          {error && (
            <span>
              <CatH1>{Translation.ebooks.category.messages.error}</CatH1>
            </span>
          )}

          {loading && (
            <span>
              <CatH1>{Translation.ebooks.category.messages.loading}</CatH1>
              <LoadingIndicator />
            </span>
          )}

          {!loading &&
            !error && (
              <div>
                <CatpreHead>{listinfo.parentcategoryname}</CatpreHead>
                <CategoryIcon />
                <CatH1>{listinfo.name}</CatH1>
                <BookWrapper>{this.renderBooks()}</BookWrapper>
                <Waypoint onEnter={this.getPosts} bottomOffset={'-50px'} />
                {loadmore && <LoadingIndicator />}
                <DataLayer />
              </div>
            )}
        </BookListWrapper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    UserAuthkey: state.account.userData.authkey,
  };
}

export default connect(mapStateToProps, { refreshAuth })(ListAllFromCategory);
