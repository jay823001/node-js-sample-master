import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

const StyledSvg = styled.svg`
  position: relative;
  margin-right: .5rem;
  width: 2.1rem;
  height: 2.1rem;
  vertical-align: top;
  top: .4rem;
    path {
      fill: ${props => props.theme.colorDarkBlue};
    }
  ${media.medium`
    margin-right: 1rem;
    width: 2.7rem;
    height: 2.7rem;
  `}
`;

export default () => (
  <StyledSvg viewBox="0 0 20 20">
    <path d="M4.7,5.4c0-0.4-0.1-0.7-0.4-1S3.7,4,3.4,4s-0.7,0.1-1,0.4S2,5,2,5.4c0,0.4,0.1,0.7,0.4,1c0.3,0.3,0.6,0.4,1,0.4s0.7-0.1,1-0.4C4.6,6.1,4.7,5.8,4.7,5.4z M16,11.5c0,0.4-0.1,0.7-0.4,0.9l-5.2,5.2c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.7-0.1-0.9-0.4L1,10C0.7,9.8,0.5,9.4,0.3,9C0.1,8.5,0,8.1,0,7.8V3.4C0,3,0.1,2.7,0.4,2.4S1,2,1.3,2h4.4C6.1,2,6.5,2.1,7,2.3C7.4,2.5,7.8,2.7,8,3l7.5,7.5C15.8,10.8,16,11.1,16,11.5z M20,11.5c0,0.4-0.1,0.7-0.4,0.9l-5.2,5.2c-0.3,0.3-0.6,0.4-1,0.4c-0.3,0-0.5,0-0.6-0.1c-0.2-0.1-0.3-0.3-0.6-0.5l4.9-4.9c0.3-0.3,0.4-0.6,0.4-0.9c0-0.4-0.1-0.7-0.4-1L9.7,3C9.5,2.7,9.1,2.5,8.6,2.3C8.2,2.1,7.8,2,7.4,2h2.4c0.4,0,0.8,0.1,1.2,0.3c0.4,0.2,0.8,0.4,1.1,0.7l7.5,7.5C19.9,10.8,20,11.1,20,11.5z"></path>
  </StyledSvg>
);
