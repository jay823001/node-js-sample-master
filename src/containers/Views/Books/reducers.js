import {
  RECEIVE_API_DATA,
  RECEIVE_API_DATA_EBOOK,
  RECEIVE_API_DATA_SBOOK,
  RECEIVE_API_DATA_CATEGORIES,
} from './constants';

// book reducer
const initialBookState = {
  books: [],
  ebooks: [],
  sbooks: [],
  categories: [],
};

export function BookReducers(state = initialBookState, { type, books, ebooks, sbooks, categories }) {
  switch (type) {

    case RECEIVE_API_DATA:
      return { ...state, books };

    case RECEIVE_API_DATA_EBOOK:
      return { ...state, ebooks };

    case RECEIVE_API_DATA_SBOOK:
      return { ...state, sbooks };

    case RECEIVE_API_DATA_CATEGORIES:
      return { ...state, categories };

    default:
      return state;
  }
}
