import React from 'react';
import styled, { css } from 'styled-components';

const StyledSvg = styled.svg`
  width: 1.7rem;
  height: 1.7rem;
  margin: 0 0.2rem;
  path {
    fill: #eaad0d;
  }
  ${props =>
    props.empty &&
    css`
      path {
        fill: #ccc;
      }
    `};
`;

export const IconStar = props => (
  <StyledSvg
    empty={props.empty}
    role="img"
    width="30"
    height="28"
    viewBox="0 0 30 28"
    aria-labelledby="star"
  >
    <title id="star">Star</title>
    <path d="M15 22.252L5.73 28l2.705-10.429L0 10.695l10.943-.697L15 0l4.057 9.998L30 10.695l-8.435 6.876L24.27 28z" />
  </StyledSvg>
);
