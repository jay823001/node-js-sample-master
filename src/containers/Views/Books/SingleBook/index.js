import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { apiUrl, internationalization_b, fetchAuth, largeCover } from 'containers/App/api';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import LoadingIndicatorPage from 'components/LoadingIndicator/page';
import BookListWrapper from 'components/Books/BookList';
import Book from 'components/Books/BookList/BookCover';
import H3 from 'components/Typography/H3';
import BookDetails from './BookDetails';
import Translation from 'translation/nextory-web-se';

const BookWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: initial;
  div {
    margin: 0.5rem;
    ${media.medium`
      margin: 1rem;
    `};
  }
`;

const StyledH3 = styled(H3)`
  color: ${props => props.theme.colorDarkGrey};
  ${media.medium`
    font-size: 2.3rem;
  `};
`;

class SingleBook extends Component {
  static propTypes = {
    location: PropTypes.object,
    history: PropTypes.object,
    LoggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    UserDetails: PropTypes.object,
    GiftReg: PropTypes.string,
  };

  state = {
    bookdetails: null,
    error: '',
    fetchstatus: '',
  };

  // when component mounts, fetch books depending on route location
  componentDidMount() {
    const isbn = this.props.location.pathname.split('/').pop();
    this.fetchPosts(isbn);
  }

  // when component reciews new props, fetch books depending on next route location
  componentWillReceiveProps(nextProps) {
    const isbn = nextProps.location.pathname.split('/').pop();
    this.fetchPosts(isbn);
  }

  // fetch correct bookdetails from isbn
  fetchPosts = async isbn => {
    try {
      const response = await fetch(`${apiUrl}book?isbn=${isbn}${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      // console.log(data);
      this.setState({
        bookdetails: data.data,
        error: null,
        fetchstatus: data.status,
      });
    } catch (error) {
      this.setState({ error: Translation.ebooks.singlebook.messages.fetchposts });
    }
  };

  // if current book is part of series, render those series
  renderSeries() {
    if (this.state.bookdetails === 0) return false;
    return this.state.bookdetails.seriesbooks.map(
      ({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
        const cleanurl = weburl.substring(0, weburl.indexOf('?'));
        return (
          <div key={bookid}>
            <Book
              bookCover={coverimg}
              booktitle={title}
              url={cleanurl}
              formattype={formattype.format}
              relatedbook={relatedbookid}
            />
          </div>
        );
      }
    );
  }

  render() {
    const isbn = this.props.location.pathname.split('/').pop();
    let tryfreecta = true;
    // harry potter books are not allowed the "try free" cta button
    if (
      [
        '9781781102381',
        '9781781102411',
        '9781781102367',
        '9781781102398',
        '9781781102428',
        '9781781102374',
        '9781781102404',
        '9781781108925',
        '9781781109908',
      ].indexOf(isbn) > -1
    ) {
      tryfreecta = false;
    }

    return (
      <div>
        {this.state.bookdetails && (
          <span>
            {this.state.bookdetails.bookdetails.authors.length > 1 && (
              <Helmet>
                <title>
                  {this.state.bookdetails.bookdetails.title} -{' '}
                  {this.state.bookdetails.bookdetails.authors[0]} och{' '}
                  {this.state.bookdetails.bookdetails.authors[1]} -{' '}
                  {this.state.bookdetails.bookdetails.formattype.title}
                </title>
                <meta
                  name="description"
                  content={`Ladda ner ${this.state.bookdetails.bookdetails.title} som ${
                    this.state.bookdetails.bookdetails.formattype.title
                    } till din mobil 100% gratis i 14 dagar!`}
                />

                <meta
                  property="og:title"
                  content={`${this.state.bookdetails.bookdetails.title} - ${
                    this.state.bookdetails.bookdetails.authors[0]
                    } och${' '}
              ${this.state.bookdetails.bookdetails.authors[1]} -${' '}${
                    this.state.bookdetails.bookdetails.formattype.title
                    }`}
                />
                <meta
                  property="og:description"
                  content={`Ladda ner ${this.state.bookdetails.bookdetails.title} som ${
                    this.state.bookdetails.bookdetails.formattype.title
                    } till din mobil 100% gratis i 14 dagar!`}
                />
                <meta
                  property="og:image"
                  content={largeCover + this.state.bookdetails.bookdetails.coverimg}
                />
                <meta property="og:url" content={window.location.href} />
                <meta
                  name="twitter:description"
                  content={`Ladda ner ${this.state.bookdetails.bookdetails.title} som ${
                    this.state.bookdetails.bookdetails.formattype.title
                    } till din mobil 100% gratis i 14 dagar!`}
                />
                <meta
                  name="twitter:title"
                  content={`${this.state.bookdetails.bookdetails.title} - ${
                    this.state.bookdetails.bookdetails.authors[0]
                    } och${' '}
              ${this.state.bookdetails.bookdetails.authors[1]} -${' '}${
                    this.state.bookdetails.bookdetails.formattype.title
                    }`}
                />
                <meta
                  name="twitter:image:src"
                  content={largeCover + this.state.bookdetails.bookdetails.coverimg}
                />
              </Helmet>
            )}
            {this.state.bookdetails.bookdetails.authors.length === 1 && (
              <Helmet>
                <title>
                  {this.state.bookdetails.bookdetails.title} -{' '}
                  {this.state.bookdetails.bookdetails.authors[0]} -{' '}
                  {this.state.bookdetails.bookdetails.formattype.title}
                </title>
                <meta
                  name="description"
                  content={`Ladda ner ${this.state.bookdetails.bookdetails.title} som ${
                    this.state.bookdetails.bookdetails.formattype.title
                    } till din mobil 100% gratis i 14 dagar!`}
                />

                <meta
                  property="og:title"
                  content={`${this.state.bookdetails.bookdetails.title} - ${
                    this.state.bookdetails.bookdetails.authors[0]
                    } -${' '}
              ${this.state.bookdetails.bookdetails.formattype.title}`}
                />
                <meta
                  property="og:description"
                  content={`Ladda ner ${this.state.bookdetails.bookdetails.title} som ${
                    this.state.bookdetails.bookdetails.formattype.title
                    } till din mobil 100% gratis i 14 dagar!`}
                />
                <meta
                  property="og:image"
                  content={largeCover + this.state.bookdetails.bookdetails.coverimg}
                />
                <meta property="og:url" content={window.location.href} />
                <meta
                  name="twitter:description"
                  content={`Ladda ner ${this.state.bookdetails.bookdetails.title} som ${
                    this.state.bookdetails.bookdetails.formattype.title
                    } till din mobil 100% gratis i 14 dagar!`}
                />
                <meta
                  name="twitter:title"
                  content={`${this.state.bookdetails.bookdetails.title} - ${
                    this.state.bookdetails.bookdetails.authors[0]
                    } -${' '}
              ${this.state.bookdetails.bookdetails.formattype.title}`}
                />
                <meta
                  name="twitter:image:src"
                  content={largeCover + this.state.bookdetails.bookdetails.coverimg}
                />
              </Helmet>
            )}
          </span>
        )}

        {!this.state.bookdetails && this.state.fetchstatus !== 400 && <LoadingIndicatorPage />}

        {this.state.fetchstatus === 400 && this.props.history.push('/hoppsan')}

        {this.state.bookdetails && (
          <div>
            <BookDetails
              tryfreecta={tryfreecta}
              activateAcc={this.props.UserDetails}
              GiftReg={this.props.GiftReg}
              ActiveReg={this.props.ActiveReg}
              loggedIn={this.props.LoggedIn}
              bookdetails={this.state.bookdetails}
            />

            {this.state.bookdetails.seriesbooks &&
              this.state.bookdetails.seriesbooks.length > 0 && (
                <BookListWrapper>
                  <StyledH3>{Translation.ebooks.singlebook.all_books_in_the_series}</StyledH3>
                  <BookWrapper>{this.renderSeries()}</BookWrapper>
                </BookListWrapper>
              )}

            <DataLayer />
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    LoggedIn: state.account.loggedIn,
    UserDetails: state.account.userData.UserDetails,
    GiftReg: state.signup.userRegPayment,
  };
}

export default connect(mapStateToProps)(SingleBook);
