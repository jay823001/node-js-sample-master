import React from 'react';
import PropTypes from 'prop-types';
import { internationalizationLanguage } from '../../../App/api';
import { StyledLink } from './StyledSinglebook';

// used in old flow
// import { largeCover } from 'containers/App/api';
// import H1 from 'components/Typography/H1';
// import H4 from 'components/Typography/H4';
// import { fancyTimeFormat } from '../../../../utils/helpFunctions';
// import Ratings from './Ratings';
// import Translation from 'translation/nextory-web-se';
// import {
//   BookHeading,
//   BookHeadingBG,
//   BookHeadingInner,
//   BookWrapper,
//   BookInfoWrapper,
//   BookInfoInner,
//   CallToAction,
//   StyledLink,
//   AuthorList,
//   MetaHeader,
//   Description,
//   BookInfoList,
//   LiHead,
// } from './StyledSinglebook';

// newui wanted parts to redirect user from old book pages to new
import _ from 'lodash';
import { withNamespaces } from 'react-i18next';
import { Redirect } from 'react-router-dom'

class BookDetails extends React.PureComponent {
  static propTypes = {
    bookdetails: PropTypes.object,
    loggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    GiftReg: PropTypes.string,
    activateAcc: PropTypes.object,
  };

  mapNarattor = () => {
    const array = this.props.bookdetails.bookdetails.narattors;
    const arrayLength = array.length;
    let i = 0;
    const narattors = array.map((value, key) => {
      i = i + 1;
      if (arrayLength === i) {
        return (
          <StyledLink key={key} to={`/contributor?q=${value}`} title={`${value}`}>
            {value}
          </StyledLink>
        );
      } else {
        return (
          <StyledLink key={key} to={`/contributor?q=${value}`} title={`${value}`}>
            {value}
            {', '}
          </StyledLink>
        );
      }
    });
    return narattors;
  };


  BookLanguage = () => {


    if (internationalizationLanguage === "FI") {

      if (this.props.bookdetails.bookdetails.language === "en") {
        return 'English';
      } else if (this.props.bookdetails.bookdetails.language === "sv") {
        return 'Svenska';
      } else {
        return 'suomi';
      }
    } else {
      if (this.props.bookdetails.bookdetails.language === "en") {
        return 'Engelska';
      } else if (this.props.bookdetails.bookdetails.language === "sv") {
        return 'Svenska';
      } else {
        return 'Finska';
      }
    }
  }

  convertURL(title, authors, isbn) {
    return _.kebabCase(_.deburr(`${title}-${authors}-${isbn}`));
  }

  render() {
    const bookdetails = this.props.bookdetails.bookdetails;
    return <Redirect to={`${this.props.t('books.book')}/${this.convertURL(bookdetails.title, bookdetails.authors, bookdetails.isbn)}`} />
  }


  // render() {
  //   const bookdetails = this.props.bookdetails.bookdetails;
  //   console.log("sdfsdf", bookdetails)
  //   let format = '';
  //   let metahead = '';
  //   if (bookdetails.formattype.format === 1 && !bookdetails.relatedbook.formattype) {
  //     metahead = (
  //       <MetaHeader ebook>
  //         <li>{Translation.ebooks.singlebook.bookdetails.read_as_ebook}</li>
  //       </MetaHeader>
  //     );
  //     format = 1;
  //   } else if (bookdetails.formattype.format === 2 && !bookdetails.relatedbook.formattype) {
  //     metahead = (
  //       <MetaHeader soundbook>
  //         <li>{Translation.ebooks.singlebook.bookdetails.listen_as_audio}</li>
  //       </MetaHeader>
  //     );
  //     format = 2;
  //   } else {
  //     metahead = (
  //       <MetaHeader>
  //         <li>{Translation.ebooks.singlebook.bookdetails.listen_as_audio}</li>
  //         <li>{Translation.ebooks.singlebook.bookdetails.read_as_ebook}</li>
  //       </MetaHeader>
  //     );
  //     format = -1;
  //   }

  //   let currentformat = '';
  //   if (bookdetails.formattype.format === 1) {
  //     currentformat = 1;
  //   } else {
  //     currentformat = 2;
  //   }

  //   let activateAcc = false;
  //   if ('allowedactions' in this.props.activateAcc) {
  //     if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
  //       activateAcc = true;
  //     }
  //   }

  //   return (
  //     <div itemScope itemType="http://schema.org/Product">
  //       <BookHeading>
  //         <BookHeadingInner>
  //           <BookWrapper>
  //             <img
  //               itemProp="image"
  //               alt={bookdetails.title}
  //               src={largeCover + bookdetails.coverimg}
  //             />
  //           </BookWrapper>
  //         </BookHeadingInner>
  //         <BookHeadingBG
  //           style={{ backgroundImage: `url('${largeCover}${bookdetails.coverimg}')` }}
  //         />
  //       </BookHeading>
  //       <BookInfoWrapper>
  //         {!this.props.loggedIn &&
  //           !this.props.ActiveReg &&
  //           (this.props.tryfreecta ? (
  //             <CallToAction primary to="/register/subscription#steg-1">
  //               {Translation.ebooks.singlebook.bookdetails.buttons.try_free}
  //             </CallToAction>
  //           ) : (
  //               <CallToAction primary to="/register/subscription#steg-1">
  //                 {Translation.ebooks.singlebook.bookdetails.buttons.listen_on_mobile}
  //               </CallToAction>
  //             ))}


  //         {!this.props.loggedIn &&
  //           this.props.GiftReg === 'REDEEM_GIFTCARD' && (
  //             <CallToAction primary to="/presentkort/subscription">
  //               {Translation.menu.buttons.complete_registration}
  //             </CallToAction>
  //           )}

  //         {!this.props.loggedIn &&
  //           this.props.GiftReg === 'REDEEM_CAMPAIGN' && (
  //             <CallToAction primary to="/register-campaign/subscription">
  //               {Translation.menu.buttons.complete_registration}
  //             </CallToAction>
  //           )}

  //         {!this.props.loggedIn &&
  //           this.props.ActiveReg &&
  //           this.props.GiftReg !== 'REDEEM_GIFTCARD' &&
  //           this.props.GiftReg !== 'REDEEM_CAMPAIGN' && (
  //             <CallToAction primary to="/register/card">
  //               {Translation.menu.buttons.complete_registration}
  //             </CallToAction>
  //           )}

  //         {activateAcc &&
  //           this.props.loggedIn &&
  //           this.props.GiftReg !== 'REDEEM_CAMPAIGN' &&
  //           this.props.GiftReg !== 'REDEEM_GIFTCARD' && (
  //             <CallToAction primary to="/konto/aktivera-abonnemang">
  //               {Translation.menu.buttons.enable_nextory_now}
  //             </CallToAction>
  //           )}

  //         <BookInfoInner>
  //           <article>
  //             <H1 itemProp="name">{bookdetails.title}</H1>
  //             <Ratings ratings={bookdetails.ratings} />
  //             <H4>
  //               {Translation.ebooks.singlebook.bookdetails._Of}
  //               {bookdetails.authors.length > 2 ?
  //                 <AuthorList>
  //                   {bookdetails.authors.map((author, index) => (
  //                     < li key={author} >
  //                       <StyledLink to={`/contributor?q=${author}`} title={author}>
  //                         {author}
  //                       </StyledLink>
  //                       {bookdetails.authors.length === index + 2 ? <span>&nbsp;{Translation.ebooks.singlebook.bookdetails.and} &nbsp;</span> :
  //                         bookdetails.authors.length === index + 1 ? null : <span>,&nbsp;</span>
  //                       }
  //                     </li>
  //                   ))}
  //                 </AuthorList>
  //                 : <AuthorList>
  //                   {bookdetails.authors.map((author, index) => (
  //                     < li key={author} >
  //                       <StyledLink to={`/contributor?q=${author}`} title={author}>
  //                         {author}
  //                       </StyledLink>
  //                       {bookdetails.authors.length === index + 1 ? null : <span>&nbsp;{Translation.ebooks.singlebook.bookdetails.and} </span>}
  //                     </li>
  //                   ))}
  //                 </AuthorList>
  //               }
  //             </H4>
  //             {metahead}
  //             <Description dangerouslySetInnerHTML={{ __html: bookdetails.description }} />
  //           </article>
  //           <aside>
  //             <H4>{Translation.ebooks.singlebook.bookdetails.more_info}</H4>
  //             <BookInfoList>
  //               {bookdetails.seriesname &&
  //                 (bookdetails.sequence !== '0' ? (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.series}</span>
  //                     <span>
  //                       <StyledLink
  //                         to={`/series?q=${bookdetails.relatedbook.seriesname ||
  //                           bookdetails.seriesname}`}
  //                         title={bookdetails.seriesname}
  //                       >
  //                         {bookdetails.seriesname}
  //                       </StyledLink>{' '}
  //                       ({Translation.ebooks.singlebook.bookdetails.of_the} {bookdetails.sequence})
  //                     </span>
  //                   </li>
  //                 ) : (
  //                     <li>
  //                       <span>{Translation.ebooks.singlebook.bookdetails.series}</span>
  //                       <span>
  //                         <StyledLink
  //                           to={`/series?q=${bookdetails.relatedbook.seriesname ||
  //                             bookdetails.seriesname}`}
  //                           title={bookdetails.seriesname}
  //                         >
  //                           {bookdetails.seriesname}
  //                         </StyledLink>
  //                       </span>
  //                     </li>
  //                   ))}

  //               <li>
  //                 <span>{Translation.ebooks.singlebook.bookdetails.type}</span>
  //                 {format === 1 && (
  //                   <span>{Translation.ebooks.singlebook.bookdetails.format.ebook}</span>
  //                 )}
  //                 {format === 2 && (
  //                   <span>{Translation.ebooks.singlebook.bookdetails.format.audiobook}</span>
  //                 )}
  //                 {format === -1 && (
  //                   <span>
  //                     {Translation.ebooks.singlebook.bookdetails.format.ebook_and_audiobook}
  //                   </span>
  //                 )}
  //               </li>
  //               {format === -1 && (
  //                 <LiHead>{Translation.ebooks.singlebook.bookdetails.format.audiobook}</LiHead>
  //               )}
  //               {((bookdetails.narattors && bookdetails.narattors.length > 0) ||
  //                 (bookdetails.relatedbook.narattors &&
  //                   bookdetails.relatedbook.narattors.length > 0)) &&
  //                 (bookdetails.relatedbook.narattors ? (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.narrator}</span>
  //                     <span>
  //                       <StyledLink
  //                         to={`/search?q=${bookdetails.relatedbook.narattors[0] || bookdetails.narattors}`}
  //                         title={bookdetails.narattors}
  //                       >
  //                         {bookdetails.relatedbook.narattors[0] || bookdetails.narattors}
  //                       </StyledLink>
  //                     </span>
  //                   </li>
  //                 ) : (
  //                     <li>
  //                       <span>{Translation.ebooks.singlebook.bookdetails.narrator}</span>
  //                       <span>{this.mapNarattor()}</span>
  //                     </li>
  //                   ))}

  //               {bookdetails.bookLengthValue && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.length}</span>
  //                   <span>{fancyTimeFormat(bookdetails.bookLengthValue)}</span>
  //                 </li>
  //               )}
  //               {bookdetails.relatedbook.bookLengthValue && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.length}</span>
  //                   <span>{fancyTimeFormat(bookdetails.relatedbook.bookLengthValue)}</span>
  //                 </li>
  //               )}



  //               {currentformat === 2 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.included_in}</span>
  //                   <span>{bookdetails.bookallowedfor}</span>
  //                 </li>
  //               )}
  //               {currentformat === 1 &&
  //                 bookdetails.relatedbook.bookallowedfor && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.included_in}</span>
  //                     <span>{bookdetails.relatedbook.bookallowedfor}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 2 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.published}</span>
  //                   <span>{bookdetails.publishdate}</span>
  //                 </li>
  //               )}
  //               {currentformat === 1 &&
  //                 bookdetails.relatedbook.publishdate && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.published}</span>
  //                     <span>{bookdetails.relatedbook.publishdate}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 2 &&
  //                 (bookdetails.translators && bookdetails.translators.length > 0) && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.translator}</span>
  //                     <span>{bookdetails.translators}</span>
  //                   </li>
  //                 )}
  //               {currentformat === 1 &&
  //                 (bookdetails.relatedbook.translators &&
  //                   bookdetails.relatedbook.translators.length > 0) && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.translator}</span>
  //                     <span>{bookdetails.relatedbook.translators}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 2 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.language}</span>
  //                   <span>
  //                     {this.BookLanguage()}
  //                   </span>
  //                 </li>
  //               )}
  //               {currentformat === 1 &&
  //                 bookdetails.relatedbook.language && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.language}</span>
  //                     <span>
  //                       {this.BookLanguage()}
  //                     </span>
  //                   </li>
  //                 )}

  //               {currentformat === 2 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.isbn}</span>
  //                   <span>{bookdetails.isbn}</span>
  //                 </li>
  //               )}
  //               {currentformat === 1 &&
  //                 bookdetails.relatedbook.isbn && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.isbn}</span>
  //                     <span>{bookdetails.relatedbook.isbn}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 2 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.publishers}</span>
  //                   <span>{bookdetails.publisher}</span>
  //                 </li>
  //               )}
  //               {currentformat === 1 &&
  //                 bookdetails.relatedbook.publisher && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.publishers}</span>
  //                     <span>{bookdetails.relatedbook.publisher}</span>
  //                   </li>
  //                 )}

  //               {format === -1 && (
  //                 <LiHead>{Translation.ebooks.singlebook.bookdetails.format.ebook}</LiHead>
  //               )}

  //               {currentformat === 1 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.included_in}</span>
  //                   <span>{bookdetails.bookallowedfor}</span>
  //                 </li>
  //               )}
  //               {currentformat === 2 &&
  //                 bookdetails.relatedbook.bookallowedfor && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.included_in}</span>
  //                     <span>{bookdetails.relatedbook.bookallowedfor}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 1 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.published}</span>
  //                   <span>{bookdetails.publishdate}</span>
  //                 </li>
  //               )}
  //               {currentformat === 2 &&
  //                 bookdetails.relatedbook.publishdate && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.published}</span>
  //                     <span>{bookdetails.relatedbook.publishdate}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 1 &&
  //                 (bookdetails.translators && bookdetails.translators.length > 0) && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.translator}</span>
  //                     <span>{bookdetails.translators}</span>
  //                   </li>
  //                 )}
  //               {currentformat === 2 &&
  //                 (bookdetails.relatedbook.translators &&
  //                   bookdetails.relatedbook.translators.length > 0) && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.translator}</span>
  //                     <span>{bookdetails.relatedbook.translators}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 1 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.language}</span>
  //                   <span>
  //                     {this.BookLanguage()}
  //                   </span>
  //                 </li>
  //               )}
  //               {currentformat === 2 &&
  //                 bookdetails.relatedbook.language && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.language}</span>
  //                     <span>
  //                       {this.BookLanguage()}
  //                     </span>
  //                   </li>
  //                 )}

  //               {currentformat === 1 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.isbn}</span>
  //                   <span>{bookdetails.isbn}</span>
  //                 </li>
  //               )}
  //               {currentformat === 2 &&
  //                 bookdetails.relatedbook.isbn && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.isbn}</span>
  //                     <span>{bookdetails.relatedbook.isbn}</span>
  //                   </li>
  //                 )}

  //               {currentformat === 1 && (
  //                 <li>
  //                   <span>{Translation.ebooks.singlebook.bookdetails.publishers}</span>
  //                   <span>{bookdetails.publisher}</span>
  //                 </li>
  //               )}
  //               {currentformat === 2 &&
  //                 bookdetails.relatedbook.publisher && (
  //                   <li>
  //                     <span>{Translation.ebooks.singlebook.bookdetails.publishers}</span>
  //                     <span>{bookdetails.relatedbook.publisher}</span>
  //                   </li>
  //                 )}
  //             </BookInfoList>
  //           </aside>
  //         </BookInfoInner>
  //       </BookInfoWrapper>
  //     </div>
  //   );
  // }
}

export default withNamespaces(['lang_route'])(BookDetails);
