import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import { Link } from 'react-router-dom';

import A from 'components/Typography/A';
import Button from 'components/Buttons';
import HeadPhonesIcon from './phones.svg';
import BookIcon from './book.svg';

export const BookHeading = styled.section`
  position: relative;
  overflow: hidden;
  margin: 0;
  padding: 5rem 0 6rem;
  width: 100%;
  background-color: #0e143a;
  ${media.medium`
    padding: 7rem 0 8rem;
  `};
`;

export const BookHeadingBG = styled.aside`
  position: absolute;
  z-index: 1;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: inherit;
  content: '';
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  filter: blur(2rem);
  &:after {
    position: absolute;
    z-index: 1;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    content: '';
    background-color: rgba(0, 0, 0, 0.4);
  }
`;

export const BookHeadingInner = styled.div`
  position: relative;
  z-index: 2;
  max-width: 80rem;
  margin: auto;
  text-align: center;
`;

export const BookWrapper = styled.div`
  position: relative;
  width: 18rem;
  height: auto;
  margin: auto;
  vertical-align: top;
  box-shadow: none;
  ${media.medium`
    width: 28rem;
  `} img {
    width: 100%;
    box-shadow: 0 0.1rem 1rem rgba(0, 0, 0, 0.5);
    max-width: 100%;
    height: auto;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 0;
    user-drag: none;
    -webkit-user-drag: none;
  }
`;

export const BookInfoWrapper = styled.section`
  position: relative;
  padding: 5rem 1.5rem 4.5rem;
  background-color: white;
  ${media.medium`
    padding: 8rem 2.5rem 7rem;
  `};
`;

export const BookInfoInner = styled.div`
  max-width: 80rem;
  margin-right: auto;
  margin-left: auto;
  article {
    margin-bottom: 4rem;
    ${media.medium`
      margin-bottom: 0;
      display: inline-block;
      width: 60%;
      margin-right: 3%;
      padding-right: 3%;
      vertical-align: top;
      border-right: .1rem solid ${props => props.theme.colorGrey};
    `} ${media.large`
      margin-right: 5%;
      padding-right: 5%;
    `};
  }
  aside {
    ${media.medium`
      display: inline-block;
      width: 34%;
      vertical-align: top;
    `} ${media.large`
      width: 30%;
    `};
  }
`;

export const CallToAction = styled(Button)`
  position: absolute;
  z-index: 3;
  top: -3.5rem;
  text-decoration: none;
  left: 0;
  right: 0;
  max-width: 28rem;
  text-align: center;
  margin: auto;
  ${media.medium`
    left: 50%;
    right: auto;
    max-width: 100%;
    transform: translateX(-50%);
  `};
`;

export const StyledLink = A.withComponent(Link);

export const AuthorList = styled.ul`
  display: inline-block;
  li {
    display: inline-block;
    &:nth-child(2):before {
      content: '';
      margin: 0 0.5rem;
    }
    &:nth-child(3):before {
      content: '';
      margin: 0 0.5rem;
    }
    &:nth-child(4):before {
      content: '';
      margin: 0 0.5rem;
    }
  }
`;

export const MetaHeader = styled.ul`
  margin: 1rem 0 1.7rem;
  ${media.medium`
    margin: 1.5rem 0 2.3rem;
  `} ${props =>
    props.ebook &&
    css`
      li:nth-child(1):before {
        background-image: url(${BookIcon}) !important;
      }
    `}
  li {
    font-size: 1.5rem;
    font-weight: 600;
    margin-right: 2.5rem;
    color: ${props => props.theme.colorDarkGrey};
    display: inline-block;
    margin-bottom: 0.5rem;
    &:before {
      margin-right: 0.8rem;
      position: relative;
      top: 0.5rem;
      background-repeat: no-repeat;
      background-position: center;
      background-size: 2rem 2rem;
      height: 2rem;
      width: 2rem;
      content: '';
      display: inline-block;
    }
    &:nth-child(1):before {
      background-image: url(${HeadPhonesIcon});
    }
    &:nth-child(2):before {
      background-image: url(${BookIcon});
    }
  }
`;

export const Description = styled.p`
  font-family: Georgia, serif;
  font-style: italic;
`;

export const BookInfoList = styled.ul`
  margin-top: 1rem;
  li {
    margin-bottom: 0.6rem;
    line-height: 1.2;
  }
  span {
    font-size: 1.1rem;
    font-weight: 600;
    display: inline-block;
    width: 11rem;
    text-transform: uppercase;
    vertical-align: top;
    & + span {
      font-weight: 400;
      text-transform: none;
      font-size: 1.2rem;
      margin-left: 5%;
      width: 20rem;
    }
  }
  ${media.medium`
    span, span+span {
      width: 45%;
    }
  `};
`;

export const LiHead = styled.li`
  font-size: 1.1rem;
  font-weight: 600;
  border-bottom: 0.1rem solid ${props => props.theme.colorGrey};
  color: ${props => props.theme.colorDarkGrey};
  padding-bottom: 0.5rem;
  margin-top: 1.5rem;
  text-transform: uppercase;
`;
