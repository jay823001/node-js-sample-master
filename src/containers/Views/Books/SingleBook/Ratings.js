import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import { IconStar } from './icon-star';

const Wrapper = styled.div`
  margin: -0.3rem 0 1.2rem 0;
  ${media.medium`
    margin: -0.5rem 0 1.2rem 0;
  `};
`;

class Ratings extends React.PureComponent {
  static propTypes = {
    ratings: PropTypes.object,
  };

  render() {
    let stars = [];
    let emptystars = [];

    /* if book has ratings loop through 
    them and render star and empty star */
    if (this.props.ratings) {
      const rating = Math.round(this.props.ratings.avgrating);
      for (let i = 0; i < rating; i++) {
        stars.push(<IconStar key={Math.random()} />);
      }
      for (let e = 0; e < 5 - rating; e++) {
        emptystars.push(<IconStar empty key={Math.random()} />);
      }
    }

    return this.props.ratings ? (
      <Wrapper itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
        <meta itemProp="ratingValue" content={this.props.ratings.avgrating} />
        <meta itemProp="bestRating" content="5" />
        <meta itemProp="ratingCount" content={this.props.ratings.totalratings} />
        {stars}
        {emptystars}
      </Wrapper>
    ) : null;
  }
}

export default Ratings;
