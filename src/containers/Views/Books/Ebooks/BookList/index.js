import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Waypoint from 'react-waypoint';
import { apiUrl, internationalization_b, getHeaders } from 'containers/App/api';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import LoadingIndicator from 'components/LoadingIndicator';
import BookSlider from 'components/Slider';
import SliderHead from 'components/Slider/SliderHead';
import BookGroup from 'components/Books/BookList/BookGroup';
import { requestApiDataEbook } from 'containers/Views/Books/actions';
import Book from 'components/Books/BookList/BookCover';

class BookList extends React.PureComponent {
  static propTypes = {
    requestApiDataEbook: PropTypes.func,
    books: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
    ]),
  };

  state = {
    booklist: [],
    loadmore: false,
    page: 2,
    done: false,
  };

  componentDidMount() {
    this.checkAuth();
  }

  checkAuth = () => {
    if (new Cookies().get('user')) {
      const data = JSON.parse(Base64.decode(new Cookies().get('user')));
      const jwt = parseJwt(data.authkey);
      const current_time = Date.now() / 1000;
      if (jwt.exp < current_time) {
        return this.props.refreshAuth();
      } else {
        return this.props.requestApiDataEbook(this.props.UserAuthkey);
      }
    }
    else {
      return this.props.requestApiDataEbook(this.props.UserAuthkey);
    }
  }

  // get more booksliders via infinite loading
  getPosts = () => {
    this.fetchBooksliders(this.state.page);
    this.setState({
      page: this.state.page += 1, // eslint-disable-line
      loadmore: true,
    });
  }

  // fetch more books via infinite loading
  fetchBooksliders = async (page) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    }
    try {
      const response = await fetch(`${apiUrl}MaincategorySpecificLoadStep?format=1&windowsize=18&pagenumber=${page}${internationalization_b}`, {
        headers: getHeaders(authkey),
        credentials: 'same-origin',
      });
      const data = await response.json();

      if ('bookgroups' in data.data) {
        this.setState({
          booklist: [...this.state.booklist, data.data.bookgroups],
          loadmore: false,
        });
      } else {
        this.setState({ loadmore: false, done: true });
      }
    } catch (error) {
      this.setState({ loadmore: false });
    }
  }


  // render initial book sliders
  renderBookGroups() {
    if (this.props.books.length < 1) return false;
    return this.props.books.map(({ iscategory, name, slug, books }) => (
      <BookGroup key={Math.random()}>
        {iscategory
          ? <SliderHead
            heading={name}
            link={`/kategori/${slug}?e-bocker`}
          />
          : <SliderHead
            heading={name}
            link={`/bocker/${slug}?e-bocker`}
          />
        }
        <BookSlider>
          {books.map(({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
            const cleanurl = weburl.substring(0, weburl.indexOf('?'));
            return (
              <div key={bookid}>
                <Book
                  bookCover={coverimg}
                  booktitle={title}
                  url={cleanurl}
                  formattype={formattype.format}
                  relatedbook={relatedbookid}
                />
              </div>
            );
          })}
        </BookSlider>
      </BookGroup>
    ));
  }

  // render lazy loaded book sliders
  renderBookGroupsLazy() {
    if (this.state.booklist === undefined || this.state.booklist.length === 0) {
      return false;
    } else {
      const flatList = [].concat(...this.state.booklist);
      return flatList.map(({ iscategory, name, slug, books }) => (
        <BookGroup key={Math.random()}>
          {iscategory
            ? <SliderHead
              heading={name}
              link={`/kategori/${slug}?e-bocker`}
            />
            : <SliderHead
              heading={name}
              link={`/bocker/${slug}?e-bocker`}
            />
          }
          <BookSlider>
            {books.map(({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
              const cleanurl = weburl.substring(0, weburl.indexOf('?'));
              return (
                <div key={bookid}>
                  <Book
                    bookCover={coverimg}
                    booktitle={title}
                    url={cleanurl}
                    formattype={formattype.format}
                    relatedbook={relatedbookid}
                  />
                </div>
              );
            })}
          </BookSlider>
        </BookGroup>
      ));
    }
  }

  render() {
    const { loadmore, done } = this.state;

    return (
      <div>
        {this.props.books.length > 0
          ?
          <div>
            {this.renderBookGroups()}
            {this.renderBookGroupsLazy()}
            {!done && <Waypoint onEnter={this.getPosts} bottomOffset={'-50px'} />}
            {loadmore && <LoadingIndicator />}
          </div>
          :
          <LoadingIndicator />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    books: state.bookdata.ebooks,
    UserAuthkey: state.account.userData.authkey,
  };
}

export default connect(mapStateToProps, { requestApiDataEbook, refreshAuth })(BookList);
