import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import Waypoint from 'react-waypoint';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { parseJwt } from 'utils/helpFunctions';
import DataLayer from 'containers/App/datalayer';
import { apiUrl, internationalization_b, getHeaders } from 'containers/App/api';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import Filter from 'containers/Filter';
import H1 from 'components/Typography/H1';
import BookListWrapper from 'components/Books/BookList';
import Book from 'components/Books/BookList/BookCover';
import LoadingIndicator from 'components/LoadingIndicator';
import Translation from 'translation/nextory-web-se';

const CatH1 = styled(H1)`
  color: ${props => props.theme.colorDarkBlue};
  display: inline-block;
  margin-bottom: 2.8rem;
  ${media.medium`
    margin-bottom: 4rem;
  `};
`;


const BookWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  div {
    margin: 0.5rem;
    ${media.medium`
      margin: 1rem;
    `};
  }
`;

class TopList extends Component {
  static propTypes = {
    location: PropTypes.object,
    match: PropTypes.object,
  };

  state = {
    booklist: [],
    listinfo: {},
    error: '',
    loading: true,
    loadmore: false,
    page: 1,
    toptitles: [],
  };

  componentDidMount() {
    this.checkAuth();
  }

  checkAuth = () => {
    if (new Cookies().get('user')) {
      const data = JSON.parse(Base64.decode(new Cookies().get('user')));
      const jwt = parseJwt(data.authkey);
      const current_time = Date.now() / 1000;
      if (jwt.exp < current_time) {
        return this.props.refreshAuth();
      } else {
        return this.getPosts();
      }
    } else {
      return this.getPosts();
    }
  };

  componentWillReceiveProps(nextProps) {
    const {
      match: { params },
    } = nextProps;
    let format = -1;
    let catslug = 'bocker';
    if (nextProps.location.search === '?e-bocker') {
      format = 1;
      catslug = 'e-bocker';
    } else if (nextProps.location.search === '?ljudbocker') {
      format = 2;
      catslug = 'ljudbocker';
    }

    let sort = '';
    if (nextProps.location.hash === '#nyheter') {
      sort = 'pubdate';
    } else if (nextProps.location.hash === '#titel') {
      sort = 'title';
    }
    this.setState({ page: 1 });
    this.fetchNewPosts(params.slug, format, sort, catslug);
  }

  getPosts = () => {
    const {
      match: { params },
    } = this.props;
    const slug = params.slug;

    let format = -1;
    let catslug = 'bocker';
    if (this.props.location.search === '?e-bocker') {
      format = 1;
      catslug = 'e-bocker';
    } else if (this.props.location.search === '?ljudbocker') {
      format = 2;
      catslug = 'ljudbocker';
    }

    let sort = '';
    if (this.props.location.hash === '#nyheter') {
      sort = 'pubdate';
    } else if (this.props.location.hash === '#titel') {
      sort = 'title';
    }

    this.fetchPosts(this.state.page, slug, format, sort, catslug);
    this.setState({ loadmore: true });
  };

  fetchPosts = async (page, slug, format, sort, catslug) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    };
    try {
      const response = await fetch(
        `${apiUrl}categoryViewAll?subcatslug=${slug}&format=${format}&pagenumber=${page}&catslug=${catslug}&subscription=PREMIUM&sorton=${sort}${internationalization_b}`,
        {
          headers: await getHeaders(authkey),
          credentials: 'same-origin',
        }
      );
      const data = await response.json();
      if (data.status === 200) {
        this.setState({ loadmore: false });
        if ('books' in data.data) {
          this.setState({
            booklist: [...this.state.booklist, data.data.books],
            listinfo: data.data,
            error: null,
            loading: false,
            toptitles: data.data.toptitles,
            page: (this.state.page += 1),
          });
        }
      } else {
        this.setState({
          loading: false,
          loadmore: false,
        });
      }
    } catch (error) {
      this.setState({
        loading: false,
        loadmore: false,
        error: (Translation.ebooks.error),
      });
    }
  };

  fetchNewPosts = async (slug, format, sort, catslug) => {
    const authkey = {
      authkey: this.props.UserAuthkey,
    };
    try {
      const response = await fetch(
        `${apiUrl}categoryViewAll?subcatslug=${slug}&format=${format}&pagenumber=1&catslug=${catslug}&subscription=PREMIUM&sorton=${sort}${internationalization_b}`,
        {
          headers: await getHeaders(authkey),
          credentials: 'same-origin',
        }
      );
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          loadmore: false,
          booklist: [data.data.books],
          listinfo: data.data,
          error: null,
          loading: false,
        });
      } else {
        this.setState({
          loading: false,
          loadmore: false,
        });
      }
    } catch (error) {
      this.setState({
        error: (Translation.ebooks.error),
        loading: false,
        loadmore: false,
      });
    }
  };

  renderBooks() {
    if (this.state.booklist === 0) return false;
    return this.state.booklist.map(bookgroup =>
      bookgroup.map(({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
        const cleanurl = weburl.substring(0, weburl.indexOf('?'));
        return (
          <div key={bookid}>
            <Book
              bookCover={coverimg}
              booktitle={title}
              url={cleanurl}
              formattype={formattype.format}
              relatedbook={relatedbookid}
            />
          </div>
        );
      })
    );
  }

  render() {
    const { loading, listinfo, error, loadmore } = this.state;

    let SEOdescription = '';
    let SEOtitle = '';
    if (this.state.listinfo) {
      SEOtitle = `${this.state.listinfo.name} - ${Translation.ebooks.toplist.seotitle}`;
      SEOdescription = `${
        this.state.listinfo.name
        } -  ${Translation.ebooks.toplist.seodescription}`;
    }
    return (
      <div>
        <Helmet>
          <title>{SEOtitle}</title>
          <meta name="description" content={SEOdescription} />
          <meta property="og:title" content={SEOtitle} />
          <meta property="og:description" content={SEOdescription} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={SEOdescription} />
          <meta name="twitter:title" content={SEOtitle} />
        </Helmet>

        {!loading && <Filter category={true} type={false} sort={false} />}

        <BookListWrapper>
          {error && (
            <span>
              <CatH1>{Translation.ebooks.toplist.messages.error}</CatH1>
            </span>
          )}

          {loading && (
            <span>
              <CatH1>{Translation.ebooks.toplist.messages.loading}</CatH1>
              <LoadingIndicator />
            </span>
          )}

          {!loading &&
            !error && (
              <div>
                {/* {(listinfo.formattype.format === 1 || listinfo.formattype.format === 2) && (
                  <CatpreHead>{listinfo.formattype.title}</CatpreHead>
                )} */}
                <CatH1>{listinfo.name}</CatH1>
                <BookWrapper>{this.renderBooks()}</BookWrapper>
                <Waypoint onEnter={this.getPosts} bottomOffset={'-50px'} />
                {loadmore && <LoadingIndicator />}
                <DataLayer />
              </div>
            )}
        </BookListWrapper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    UserAuthkey: state.account.userData.authkey,
  };
}

export default connect(mapStateToProps, { refreshAuth })(TopList);
