import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import H1 from 'components/Typography/H1';
import P from 'components/Typography/P';
import A from 'components/Typography/A';
import Translation from 'translation/nextory-web-se';
import { internationalizationLanguage } from '../../../App/api';

const seotitle = Translation.saljapage.seotitle;
const seodesc = Translation.saljapage.seodescription;

const SaljaPage = () => (
  <ReactCSSTransitionGroup
    transitionAppear={true}
    transitionAppearTimeout={600}
    transitionEnterTimeout={600}
    transitionLeaveTimeout={200}
    transitionName="SlideUp"
  >
    <Helmet>
      <title>{seotitle}</title>
      <meta name="description" content={seodesc} />
      <meta property="og:title" content={seotitle} />
      <meta property="og:description" content={seodesc} />
      <meta property="og:url" content={window.location.href} />
      <meta name="twitter:description" content={seodesc} />
      <meta name="twitter:title" content={seotitle} />
    </Helmet>
    <DataLayer />
    <StaticpageWrapper>
      <H1>{Translation.saljapage.heading}</H1>
      <P>{Translation.saljapage.content.p1}</P>

      {internationalizationLanguage === "FI" ? <P>
        {Translation.saljapage.content.p2_p1}{' '}
        <A href="mailto:content@nextory.se">content@nextory.fi</A>{' '}
        {Translation.saljapage.content.p2_p2}{' '}
      </P> : <P>
          {Translation.saljapage.content.p2_p1}{' '}
          <A href="mailto:content@nextory.se">content@nextory.se</A>{' '}
          {Translation.saljapage.content.p2_p2}{' '}
        </P>}
    </StaticpageWrapper>
  </ReactCSSTransitionGroup>
);

SaljaPage.propTypes = {
  location: PropTypes.object,
};

export default SaljaPage;
