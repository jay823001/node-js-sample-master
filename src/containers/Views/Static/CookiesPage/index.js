import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { apiUrl, internationalizationLanguage, internationalization_b, fetchAuth } from 'containers/App/api';
import DataLayer from 'containers/App/datalayer';
import Loading from 'components/LoadingIndicator/page';
import StaticpageWrapper from 'components/Staticpage';

class CookiesPage extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
  };
  state = {
    seotitle: '',
    seodesc: '',
    loadingseo: true,
    content: '',
    loadingcontent: true,
  };

  componentDidMount() {
    this.fetchMetaData();
    this.fetchCMSData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}page-meta?pageUrl=/om-cookies/${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          seotitle: data.data.pagemeta.metaList[0].value,
          seodesc: data.data.pagemeta.metaList[1].value,
          loadingseo: false,
        });
      }
    } catch (error) {
      this.setState({ loadingseo: false });
    }
  };

  fetchCMSData = async () => {
    try {
      const response = await fetch(`${apiUrl}cms-page?pageUrl=/om-cookies${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          content: data.data.cmsData.pageContent,
          loadingcontent: false,
        });
      }
    } catch (error) {
      this.setState({ loadingcontent: false });
    }
  };

  render() {
    let dataFetched = false;

    if (!this.state.loadingcontent && !this.state.loadingseo) {
      dataFetched = true;
    }

    return dataFetched ? (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />
        <StaticpageWrapper>
          {internationalizationLanguage !== "FI" ? <div dangerouslySetInnerHTML={{ __html: this.state.content }} />
            :
            <div class="whitebg" id="subPage-wrap">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <h1>Tietoja evästeistä</h1>
                  </div>
                  <div class="col-md-8 col-sm-10 col-xs-12">
                    <p>
                      Nextory käyttää evästeitä, kun vierailet verkkosivuillamme. Eväste on tietokonetiedosto, joka on tallennettu käyttäjän tietokoneeseen. Eväste ei tunnista sinua henkilökohtaisesti, vaan ainoastaan selaimesi, joka on asennettu tietokoneellesi ja jota käytät vierailun aikana. Jos et halua sallia evästeiden tallentamista tietokoneellesi, voit estää sen selaimesi asetusten avulla. Eväste tallennetaan siihen aikaan, kun olet yhteydessä palveluun, tai niin kauan kuin tiedot ovat velvollisia täyttämään palvelun velvoitteet tai oikeudet.</p>
                  </div>
                </div>
              </div>
            </div>}

        </StaticpageWrapper>
      </ReactCSSTransitionGroup>
    ) : (
        <Loading />
      );
  }
}

export default CookiesPage;
