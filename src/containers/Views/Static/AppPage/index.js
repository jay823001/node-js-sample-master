import React from 'react';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import H1 from 'components/Typography/H1';
import P from 'components/Typography/P';
import AppBadge from 'components/AppBadge';
import Translation from 'translation/nextory-web-se';

const AppPage = () => (
  <ReactCSSTransitionGroup
    transitionAppear={true}
    transitionAppearTimeout={600}
    transitionEnterTimeout={600}
    transitionLeaveTimeout={200}
    transitionName="SlideUp"
  >
    <StaticpageWrapper>
      <H1>{Translation.app_page.heading}</H1>
      <P>{Translation.app_page.content}</P>
      <AppBadge />
    </StaticpageWrapper>
  </ReactCSSTransitionGroup>
);

export default AppPage;
