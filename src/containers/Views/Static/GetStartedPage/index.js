import React from 'react';
import PropTypes from 'prop-types';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import H1 from 'components/Typography/H1';
import OL from 'components/Typography/OL';
import P from 'components/Typography/P';
import AppBadge from 'components/AppBadge';
import Translation from 'translation/nextory-web-se';

class GetstartedPage extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
  };

  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        {this.props.location.search.includes('reactivated') ? (
          <StaticpageWrapper>
            <H1>{Translation.getstarted.reactivated.heading}</H1>
            <OL>
              <li>{Translation.getstarted.reactivated.content.p1}</li>
              <li>{Translation.getstarted.reactivated.content.p2}</li>
            </OL>
            <P>{Translation.getstarted.reactivated.tips}</P>
            <P>{Translation.getstarted.reactivated.app_removed}</P>
            <AppBadge />
          </StaticpageWrapper>
        ) : (
          <StaticpageWrapper>
            <H1>{Translation.getstarted.new.heading}</H1>
            <OL>
              <li>{Translation.getstarted.new.content.p1}</li>
              <li>{Translation.getstarted.new.content.p2}</li>
              <li>{Translation.getstarted.new.content.p3}</li>
            </OL>
            <AppBadge />
          </StaticpageWrapper>
        )}
      </ReactCSSTransitionGroup>
    );
  }
}

export default GetstartedPage;
