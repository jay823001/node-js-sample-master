import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import StaticpageWrapper from 'components/Staticpage';
import Loading from 'components/LoadingIndicator/page';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import P from 'components/Typography/P';
import A from 'components/Typography/A';
import Translation from 'translation/nextory-web-se';

const H2Extramargin = H2.extend`
  margin-bottom: 2.7rem;
`;

const StyledLink = A.withComponent(Link);

class NoMatch404 extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
  };
  state = {
    validcode: false,
    code: '',
    loading: true,
  };

  componentDidMount() {
    const slug = this.props.location.pathname.replace('/', '');
    this.checkIfCampaign(slug);
  }

  checkIfCampaign = async slug => {
    try {
      const response = await fetch(`${apiUrl}campaigndetails?campaignslug=${slug}${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({ validcode: true, code: data.data.campaignslug });
      } else {
        this.setState({ validcode: false, loading: false });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { loading, validcode, code } = this.state;

    return (
      <StaticpageWrapper>
        {loading ? (
          <Loading />
        ) : (
            <div>
              <H1 huge>{Translation.not_found.heading}</H1>
              <H2Extramargin>{Translation.not_found.subheading}</H2Extramargin>
              <P ingress>
                {Translation.not_found.go_back}
                <StyledLink to="/">{Translation.not_found.links.click_here}</StyledLink>
                <br />
                {Translation.not_found.content}
                <StyledLink to="/bocker">{Translation.not_found.links.nice_book}</StyledLink>
              </P>
            </div>
          )}

        {validcode && <Redirect to={`/kampanj/${code}`} />}
      </StaticpageWrapper>
    );
  }
}

export default NoMatch404;
