import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Hero from 'components/Hero';
import HeroText from 'components/Hero/HeroText';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import ImgHeroPhone from './e-bocker_phone-min.jpg';
import ImgHeroDesktop from './e-bocker-min.jpg';
import Translation from 'translation/nextory-web-se';

const StyledHero = styled(Hero)`
  background-image: url(${ImgHeroPhone});
  margin-bottom: -2rem;
  ${media.medium`
    background-image: url(${ImgHeroDesktop});
    margin-bottom: -3.5rem;
  `};
`;

const StyledHeroLarge = styled(Hero)`
  background-image: url(${ImgHeroPhone});
  margin-bottom: -2rem;
  height: 26.5rem;
  ${media.medium`
    background-image: url(${ImgHeroDesktop});
    margin-bottom: -3.5rem;
    height: 32rem;
  `};
`;

export const HeroProcess = () => (
  <StyledHero>
    <HeroText>
      <H1 huge>{Translation.stipendum_pages.heroprocess}</H1>
    </HeroText>
  </StyledHero>
);

export const HeroStipendie = () => (
  <StyledHeroLarge>
    <HeroText centered>
      <H1 huge>{Translation.stipendum_pages.herostipendie.heading}</H1>
      <H2 huge>{Translation.stipendum_pages.herostipendie.subheading}</H2>
    </HeroText>
  </StyledHeroLarge>
);
