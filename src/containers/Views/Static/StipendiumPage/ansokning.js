import React from 'react';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Link } from 'react-router-dom';
import H3 from 'components/Typography/H3';
import P from 'components/Typography/P';
import Quote from 'components/Typography/Quote';
import A from 'components/Typography/A';
import Button from 'components/Buttons';
import Center from 'components/Center';
import { HeroProcess } from './Hero/Hero';
import Translation from 'translation/nextory-web-se';

const seotitle = (Translation.stipendum_pages.application.seotitle);
const seodesc = (Translation.stipendum_pages.application.seodescription);

const StyledLink = A.withComponent(Link);

const C = Center.extend`
  margin-top: 4rem;
`;

class StipendiumPageAnsokning extends React.PureComponent {
  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="FadeIn"
      >
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <DataLayer />

        <HeroProcess />

        <StaticpageWrapper>
          <H3>{Translation.stipendum_pages.application.content1.heading}</H3>
          <P>{Translation.stipendum_pages.application.content1.p1}</P>
          <Quote>{Translation.stipendum_pages.application.content1.quote}</Quote>
          <P>{Translation.stipendum_pages.application.content1.p2}</P>
          <P>{Translation.stipendum_pages.application.content1.p3}</P>

          <H3>{Translation.stipendum_pages.application.content2.heading}</H3>
          <P>{Translation.stipendum_pages.application.content2.p1_p1}{' '} 
          <a href="mailto:scholarships@nextory.se">scholarships@nextory.se</a>
          {Translation.stipendum_pages.application.content2.p1_p2}
          
          </P>

          <H3>{Translation.stipendum_pages.application.content3.heading}</H3>
          <P>{Translation.stipendum_pages.application.content3.p1}</P>
          <P>{Translation.stipendum_pages.application.content3.p2_p1}{' '}<StyledLink to="/nextory-stipendiet-allmanna-villkor">
          {Translation.stipendum_pages.application.content3.p2_p2}
            </StyledLink>
          </P>

          <C>
            <Button to="/nextory-stipendiet">{Translation.stipendum_pages.application.content3.buttons.back_to_nextory_scholarship}</Button>
          </C>
        </StaticpageWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

export default StipendiumPageAnsokning;
