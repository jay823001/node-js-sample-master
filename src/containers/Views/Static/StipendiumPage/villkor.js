import React from 'react';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Link } from 'react-router-dom';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import H4 from 'components/Typography/H4';
import P from 'components/Typography/P';
import OL from 'components/Typography/OL';
import A from 'components/Typography/A';
import Translation from 'translation/nextory-web-se';

const seotitle = Translation.stipendum_pages.terms.seotitle;
const seodesc = Translation.stipendum_pages.terms.seodescription;

const StyledLink = A.withComponent(Link);

class StipendiumPageVillkor extends React.PureComponent {
  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <DataLayer />
        <StaticpageWrapper>
          <H1>{Translation.stipendum_pages.terms.heading}</H1>
          <P>
            {Translation.stipendum_pages.terms.p1_p1}
            <StyledLink to="/nextory-stipendiet">
              {' '}
              {Translation.stipendum_pages.terms.links.nextory_scholarship}
            </StyledLink>
            {Translation.stipendum_pages.terms.p1_p2}
          </P>

          <H2>{Translation.stipendum_pages.terms.h2}</H2>

          <H4>{Translation.stipendum_pages.terms.list1.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list1.p1}</li>
            <li>{Translation.stipendum_pages.terms.list1.p2}</li>
            <li>{Translation.stipendum_pages.terms.list1.p3}</li>
            <li>{Translation.stipendum_pages.terms.list1.p4}</li>
            <li>{Translation.stipendum_pages.terms.list1.p5}</li>
            <li>{Translation.stipendum_pages.terms.list1.p6}</li>
          </OL>

          <H4>{Translation.stipendum_pages.terms.list2.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list2.p1}</li>
            <li>{Translation.stipendum_pages.terms.list2.p2}</li>
            <li>{Translation.stipendum_pages.terms.list2.p3}</li>
            <li>{Translation.stipendum_pages.terms.list2.p4}</li>
          </OL>

          <H4>{Translation.stipendum_pages.terms.list3.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list3.p1}</li>
            <li>
              {Translation.stipendum_pages.terms.list3.p2_p1}{' '}
              <A mailto="scholarships@nextory.se">scholarships@nextory.se </A> {
                Translation.stipendum_pages.terms.list3.p2_p2
              }
            </li>
            <li>{Translation.stipendum_pages.terms.list3.p3}</li>
            <li>{Translation.stipendum_pages.terms.list3.p4}</li>
            <li>{Translation.stipendum_pages.terms.list3.p5}</li>
            <li>{Translation.stipendum_pages.terms.list3.p6}</li>
            <li>
              {Translation.stipendum_pages.terms.list3.p7} <A mailto="scholarships@nextory.se">
                scholarships@nextory.se
              </A>.
            </li>
          </OL>

          <H4>{Translation.stipendum_pages.terms.list4.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list4.p1}</li>
          </OL>

          <H4>{Translation.stipendum_pages.terms.list5.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list5.p1}</li>
            <li>{Translation.stipendum_pages.terms.list5.p2}</li>
            <li>
              {Translation.stipendum_pages.terms.list5.p3_p1} <A mailto="scholarships@nextory.se">
                scholarships@nextory.se
              </A>
              {Translation.stipendum_pages.terms.list5.p3_p2}
            </li>
          </OL>

          <H4>{Translation.stipendum_pages.terms.list6.h4}</H4>
          <P>{Translation.stipendum_pages.terms.list6.p1}</P>

          <H4>{Translation.stipendum_pages.terms.list7.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list7.p1}</li>
            <li>{Translation.stipendum_pages.terms.list7.p2}</li>
          </OL>

          <H4>{Translation.stipendum_pages.terms.list8.h4}</H4>
          <OL>
            <li>{Translation.stipendum_pages.terms.list8.p1}</li>
            <li>
              {Translation.stipendum_pages.terms.list8.p2_p1} <A mailto="scholarships@nextory.se">
                scholarships@nextory.se
              </A>
              {Translation.stipendum_pages.terms.list8.p2_p2}
            </li>
            <li>{Translation.stipendum_pages.terms.list8.p3}</li>
            <li>{Translation.stipendum_pages.terms.list8.p4}</li>
          </OL>
        </StaticpageWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

export default StipendiumPageVillkor;
