import React from 'react';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import H3 from 'components/Typography/H3';
import P from 'components/Typography/P';
import Quote from 'components/Typography/Quote';
import Button from 'components/Buttons';
import Center from 'components/Center';
import { HeroStipendie } from './Hero/Hero';
import Translation from 'translation/nextory-web-se';

const seotitle = Translation.stipendum_pages.index.seotitle;
const seodesc = Translation.stipendum_pages.index.seodescription;

const C = Center.extend`
  margin-top: 4rem;
`;

class StipendiumPage extends React.PureComponent {
  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="FadeIn"
      >
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <DataLayer />

        <HeroStipendie />

        <StaticpageWrapper>
          <H3>{Translation.stipendum_pages.index.heading}</H3>

          <P>{Translation.stipendum_pages.index.p1}</P>
          <P>{Translation.stipendum_pages.index.p2}</P>
          <Quote>{Translation.stipendum_pages.index.quote}</Quote>

          <P>{Translation.stipendum_pages.index.p3}</P>

          <C>
            <Button to="/nextory-stipendiet-ansokan">
              {Translation.stipendum_pages.index.buttons.read_more}
            </Button>
          </C>
        </StaticpageWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

export default StipendiumPage;
