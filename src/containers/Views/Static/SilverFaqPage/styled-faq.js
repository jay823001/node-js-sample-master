import styled from 'styled-components';
import media_b from './styled-utilsb';

export const FaqContainer = styled.div`
  max-width: 630px;
  margin: 0 auto;
`;

export const PageBox = styled.div`
  width: 100%;
  height: auto;
`;
export const Title = styled.div`
  text-align: left;
  width: 100%;
  display: inline-block;
  margin-bottom: 30px;
`;
export const H1 = styled.h1`
  font-family: 'MaisonDemi';
  font-size: 28px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: normal;
  text-align: left;
  color: #333333;
  margin-bottom: 0px;
  margin-top: 0px;
`;

export const Contents = styled.div`
  padding-top: 20px;
  padding-bottom: 20px;
  height: auto;
  font-size: 18px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.39;
  letter-spacing: -0.2px;
  -webkit-text-align: left;
  text-align: left;
  color: #333333;
  border-bottom: solid 1px #eeeeee;
  border-top: solid 1px #eeeeee;
  position: relative;
`;

export const SubContents = styled.div`
  width: 100%;
  height: auto;
  font-size: 21px;
  padding-top: 13px;
  ${media_b.small`
    font-size: 18px;
    max-width: 260px;
  `};
`;

export const PageBoxAnswer = styled.div`
  width: 100%;
  min-height: 553px;
  height: auto;
  padding-bottom: 20px;
  font-size: 21px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.39;
  letter-spacing: -0.2px;
  text-align: left;
  color: #333333;
`;

export const Answerblock = styled.div`
  padding-top: 30px;
  height: auto;
  padding-bottom: 20px;
  font-size: 19px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.39;
  letter-spacing: -0.2px;
  -webkit-text-align: left;
  text-align: left;
  color: #333333;
  font-family: 'MaisonDemi';
  ${media_b.small`
        font-size: 18px;
  `};
`;

export const GreyHeader = styled.div`
  width: auto;
  height: 76px;
  background-color: #666666;
  padding-top: 15px;
  padding-left: 21px;
`;

export const GreyHeaderA = styled.div`
  font-size: 17px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.35;
  letter-spacing: -0.2px;
  text-align: left;
  color: #ffffff;
  text-decoration: none;
`;

export const Arrow = styled.div`
  position: absolute;
  right: 0;
  color: #dddddd;
  font-size: 35px;
  top: 30px;
`;
