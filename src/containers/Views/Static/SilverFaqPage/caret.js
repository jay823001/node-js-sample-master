import React from 'react';
import styled from 'styled-components';
import media_b from './styled-utilsb';

const StyledSvg = styled.svg`
  width: 29px;
  height: 48px;
  margin-top: -42px;
  margin-right: -12px;
  float: right;
  polygon,
  path {
    fill: #dddddd;
  }
  ${media_b.small`
  margin-top: -57px;
  width: 24px;
  margin-right: -8px;

`};
`;

export const ArrowIcon = () => (
  <StyledSvg role="img" viewBox="0 0 476.8 125.2" aria-labelledby="nextory-logo">
    <g>
      <path d="M239.087 142.427L101.259 4.597c-6.133-6.129-16.073-6.129-22.203 0L67.955 15.698c-6.129 6.133-6.129 16.076 0 22.201l115.621 115.626-115.621 115.61c-6.129 6.136-6.129 16.086 0 22.209l11.101 11.101c6.13 6.136 16.07 6.136 22.203 0l137.828-137.831c6.135-6.127 6.135-16.058 0-22.187z" />
    </g>
  </StyledSvg>
);
