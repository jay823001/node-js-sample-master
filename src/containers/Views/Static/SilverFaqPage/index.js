import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Link } from 'react-router-dom';
import { ArrowIcon } from './caret';
import { FaqContainer, PageBox, Title, H1, Contents, SubContents } from './styled-faq';
import Translation from 'translation/nextory-web-se';

const seotitle = Translation.silverfaq.seotitle;
const seodesc = Translation.silverfaq.seodescription;

const maincontent = (
  <FaqContainer>
    <PageBox>
      <Title>
        <H1>{Translation.silverfaq.links.h1}</H1>
      </Title>

      <Link to="/app/silver_faq/varfor-kan-jag-inte-lyssna-på-de-senaste-bockerna-i-silver">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h1}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>

      <Link to="/app/silver_faq/varfor-ar-vissa-aldre-bocker-inte-tillgangliga-i-silver">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h2}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>

      <Link to="/app/silver_faq/hur-skiljer-sig-utbudet-i-silver-mot-guld-och-familj">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h3}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>

      <Link to="/app/silver_faq/hur-ofta-uppdateras-utbudet-i-silver">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h4}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>

      <Link to="/app/silver_faq/varfor-saknas-en-av-bockerna-i-en-serie-i-silver">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h5}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>

      <Link to="/app/silver_faq/kan-jag-uppgradera-abonnemanget-tillfalligt">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h6}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>

      <Link to="/app/silver_faq/vara-olika-abonnemang">
        <Contents>
          <SubContents>{Translation.silverfaq.links.h7}</SubContents>
          <ArrowIcon />
        </Contents>
      </Link>
    </PageBox>
  </FaqContainer>
);

class SilverFaqPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      content: maincontent,
    };
  }

  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <StaticpageWrapper white={this.props.location.pathname.includes('/app/')}>
          {this.state.content}
        </StaticpageWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}
SilverFaqPage.propTypes = {
  location: PropTypes.object,
};

export default SilverFaqPage;
