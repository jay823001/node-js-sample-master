import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { FaqContainer, Title, H1, PageBoxAnswer } from '../styled-faq';
import Translation from 'translation/nextory-web-se';

const seotitle = Translation.silverfaq.seotitle;
const seodesc = Translation.silverfaq.seodescription;

const Link2 = props => (
  <ReactCSSTransitionGroup
    transitionAppear={true}
    transitionAppearTimeout={600}
    transitionEnterTimeout={600}
    transitionLeaveTimeout={200}
    transitionName="SlideUp"
  >
    <Helmet>
      <title>{seotitle}</title>
      <meta name="description" content={seodesc} />
      <meta property="og:title" content={seotitle} />
      <meta property="og:description" content={seodesc} />
      <meta property="og:url" content={window.location.href} />
      <meta name="twitter:description" content={seodesc} />
      <meta name="twitter:title" content={seotitle} />
    </Helmet>
    <StaticpageWrapper white={props.location.pathname.includes('/app/')}>
      <FaqContainer>
        <PageBoxAnswer>
          <Title>
            <H1>{Translation.silverfaq.contents.page2.heading}</H1>
          </Title>
          <div>
            {Translation.silverfaq.contents.page2.content.p1}
            <br />
            <br /> {Translation.silverfaq.contents.page2.content.p2}
          </div>
        </PageBoxAnswer>
      </FaqContainer>
    </StaticpageWrapper>
  </ReactCSSTransitionGroup>
);

Link2.propTypes = {
  location: PropTypes.object,
};

export default Link2;
