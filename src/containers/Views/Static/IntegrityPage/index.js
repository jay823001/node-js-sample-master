import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {
  apiUrl,
  internationalization_b,
  fetchAuth,
} from '../../../App/api';
import DataLayer from 'containers/App/datalayer';
import StaticpageWrapper from 'components/Staticpage';
import Translation from 'translation/nextory-web-se';

class IntegrityPage extends React.PureComponent {
  state = {
    pageContent: null,
  }

  async componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(
        `${apiUrl}cms-page?pageUrl=/integritetspolicy${internationalization_b}`, {
          headers: {
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
        });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({ pageContent: data.data.cmsData.pageContent })
      }
    } catch (error) {
      console.log(error);
    }
  };


  render() {
    let contents = this.state.pageContent ? this.state.pageContent : Translation.integrity_pages.content;
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <StaticpageWrapper white={this.props.location.pathname.includes('/app/')}>
          <div dangerouslySetInnerHTML={{ __html: contents }} />
        </StaticpageWrapper>
      </ReactCSSTransitionGroup>
    );
  }

};

export default IntegrityPage;
