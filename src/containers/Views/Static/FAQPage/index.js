import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import StaticpageWrapper from 'components/Staticpage';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import H3 from 'components/Typography/H3';
import P from 'components/Typography/P';
import UL from 'components/Typography/UL';
import Translation from 'translation/nextory-web-se';

const seotitle = Translation.faq.seotitle;
const seodesc = Translation.faq.seodescription;

const FaqPage = props => (
  <ReactCSSTransitionGroup
    transitionAppear={true}
    transitionAppearTimeout={600}
    transitionEnterTimeout={600}
    transitionLeaveTimeout={200}
    transitionName="SlideUp"
  >
    <Helmet>
      <title>{seotitle}</title>
      <meta name="description" content={seodesc} />
      <meta property="og:title" content={seotitle} />
      <meta property="og:description" content={seodesc} />
      <meta property="og:url" content={window.location.href} />
      <meta name="twitter:description" content={seodesc} />
      <meta name="twitter:title" content={seotitle} />
    </Helmet>
    <DataLayer />
    <StaticpageWrapper white={props.location.pathname.includes('/app/')}>
      <H3>{Translation.faq.faq1.heading}</H3>
      <P>{Translation.faq.faq1.content.p1}</P>
      <P>{Translation.faq.faq1.content.p2}</P>

      <H3>{Translation.faq.faq2.heading}</H3>
      <P>{Translation.faq.faq2.content.p1} </P>
      <P>{Translation.faq.faq2.content.p2}</P>

      <H3>{Translation.faq.faq3.heading}</H3>
      <P>{Translation.faq.faq3.content.p1}</P>
      <P>{Translation.faq.faq3.content.p2}</P>

      <H3>{Translation.faq.faq4.heading}</H3>
      <P>{Translation.faq.faq4.content.p1} </P>
      <P>{Translation.faq.faq4.content.p2}</P>

      <H3>{Translation.faq.faq5.heading}</H3>
      <P>{Translation.faq.faq5.content.p1} </P>
      <P>{Translation.faq.faq5.content.p2}</P>

      <H3>{Translation.faq.faq6.heading}</H3>
      <P>{Translation.faq.faq6.content.p1} </P>
      <P>{Translation.faq.faq6.content.p2}</P>
      <P>{Translation.faq.faq6.content.p3}</P>
      <P>{Translation.faq.faq6.content.p4}</P>

      <H3>{Translation.faq.faq7.heading}</H3>
      <P>{Translation.faq.faq7.content.p1} </P>
      <P>{Translation.faq.faq7.content.p2}</P>

      <H3>{Translation.faq.faq8.heading}</H3>
      <P>{Translation.faq.faq8.content.p1} </P>
      <P>{Translation.faq.faq8.content.p2}</P>

      <H3>{Translation.faq.faq9.heading}</H3>
      <P>{Translation.faq.faq9.content.p1} </P>
      <P>{Translation.faq.faq9.content.p2} </P>
      <P>{Translation.faq.faq9.content.p3}</P>

      <H3>{Translation.faq.faq10.heading}</H3>
      <P>{Translation.faq.faq10.content.p1} </P>
      <P>{Translation.faq.faq10.content.p2} </P>
      <P>{Translation.faq.faq10.content.p3}</P>

      <H3>{Translation.faq.faq11.heading}</H3>
      <P>{Translation.faq.faq11.content.p1} </P>
      <P>{Translation.faq.faq11.content.p2}</P>

      <H3>{Translation.faq.faq12.heading}</H3>
      <P>{Translation.faq.faq12.content.p1} </P>
      <P>
        <strong>{Translation.faq.faq12.content.family} </strong> {Translation.faq.faq12.content.p2}{' '}
      </P>
      <P>
        <strong>{Translation.faq.faq12.content.gold} </strong> {Translation.faq.faq12.content.p3}{' '}
      </P>
      <P>
        <strong>{Translation.faq.faq12.content.silver} </strong> {Translation.faq.faq12.content.p4}
      </P>

      <H3>{Translation.faq.faq13.heading}</H3>
      <P>{Translation.faq.faq13.content.p1}</P>

      <H3>{Translation.faq.faq14.heading}</H3>
      <P>{Translation.faq.faq14.content.p1} </P>
      <P>{Translation.faq.faq14.content.p2}</P>

      <H3>{Translation.faq.faq15.heading}</H3>
      <P>{Translation.faq.faq15.content.p1}</P>
      <P>{Translation.faq.faq15.content.p2} </P>
      <P>{Translation.faq.faq15.content.p3}</P>

      <H3>{Translation.faq.faq16.heading}</H3>
      <P>{Translation.faq.faq16.content.p1}</P>
      <P>{Translation.faq.faq16.content.p2}</P>
      <P>{Translation.faq.faq16.content.p3} </P>
      <P>{Translation.faq.faq16.content.p4}</P>
      <P>{Translation.faq.faq16.content.p5} </P>
      <P>
        {Translation.faq.faq16.content.p6_p1}{' '}
        <a href="mailto:kundservice@nextory.se">kundservice@nextory.se</a>
        {Translation.faq.faq16.content.p6_p2}{' '}
      </P>
      <P>{Translation.faq.faq16.content.p7}</P>

      <H3>{Translation.faq.faq17.heading}</H3>
      <P>{Translation.faq.faq17.content.p1} </P>
      <UL>
        <li>{Translation.faq.faq17.content.p2} </li>
        <li>{Translation.faq.faq17.content.p3} </li>
        <li>
          {Translation.faq.faq17.content.p4_p1}{' '}
          <a href="mailto:kundservice@nextory.se">kundservice@nextory.se</a>{' '}
          {Translation.faq.faq17.content.p4_p2}{' '}
        </li>
      </UL>
      <P>{Translation.faq.faq17.content.p5} </P>
    </StaticpageWrapper>
  </ReactCSSTransitionGroup>
);

FaqPage.propTypes = {
  location: PropTypes.object,
};

export default FaqPage;
