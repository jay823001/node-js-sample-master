import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { internationalizationLanguage } from '../../../App/api';
import Img from 'components/Img';
import FalletSkattkartan from 'containers/Views/HomePageB/Images/book-covers/fallet-med-skattkartan-min.jpg';
import SuperCharlie from 'containers/Views/HomePageB/Images/book-covers/super-charlie-gosedjurstjuven-min.jpg';
import FiffigaKroppen from 'containers/Views/HomePageB/Images/book-covers/fiffiga-kroppen-min.jpg';
import SlottsMysteriet from 'containers/Views/HomePageB/Images/book-covers/slottsmysteriet-min.jpg';
import Elefanten from 'containers/Views/HomePageB/Images/book-covers/elefanten-som-sa-garna-ville-somna-min.jpg';
import MammaMu from 'containers/Views/HomePageB/Images/book-covers/mamma-mu-aker-bobb-min.jpg';
import PelleSvanslos from 'containers/Views/HomePageB/Images/book-covers/valkommen-pelle-svanslos-min.jpg';
import PaxMaran from 'containers/Views/HomePageB/Images/book-covers/pax-maran-min.jpg';
import LasseMaja from 'containers/Views/HomePageB/Images/book-covers/lasse-maja-bigrafmysteriet-min.jpg';
import SamSigge from 'containers/Views/HomePageB/Images/book-covers/sam-och-sigge-min.jpg';
import LetaSpoken from 'containers/Views/HomePageB/Images/book-covers/letar-spoken-min.jpg';
import HandbokSuperhjaltar from 'containers/Views/HomePageB/Images/book-covers/handbok-for-superhjaltar-min.jpg';
import Rum123 from 'containers/Views/HomePageB/Images/book-covers/rum-213-min.jpg';
import Bajsboken from 'containers/Views/HomePageB/Images/book-covers/bajsboken-min.jpg';
import Vemsbyxor from 'containers/Views/HomePageB/Images/book-covers/vems-byxor-min.jpg';
import HarBrandbild from 'containers/Views/HomePageB/Images/book-covers/har-kommer-brandbilen-min.jpg';
import Translation from 'translation/nextory-web-se';

//Finnish book cover for Home page children
import small_9789510425138 from '../Images/FI-book-covers/Childrens_books/small_9789510425138.jpg';
import small_9789510430453 from '../Images/FI-book-covers/Childrens_books/small_9789510430453.jpg';
import small_9789510432983 from '../Images/FI-book-covers/Childrens_books/small_9789510432983.jpg';
import small_9789510434147 from '../Images/FI-book-covers/Childrens_books/small_9789510434147.jpg';
import small_9789510434406 from '../Images/FI-book-covers/Childrens_books/small_9789510434406.jpg';
import small_9789510435823 from '../Images/FI-book-covers/Childrens_books/small_9789510435823.jpg';
import small_9789511288756 from '../Images/FI-book-covers/Childrens_books/small_9789511288756.jpg';
import small_9789511326236 from '../Images/FI-book-covers/Childrens_books/small_9789511326236.jpg';
import small_9789511331506 from '../Images/FI-book-covers/Childrens_books/small_9789511331506.jpg';
import small_9789512408306 from '../Images/FI-book-covers/Childrens_books/small_9789512408306.jpg';
import small_9789513197384 from '../Images/FI-book-covers/Childrens_books/small_9789513197384.jpg';
import small_9789513197421 from '../Images/FI-book-covers/Childrens_books/small_9789513197421.jpg';
import small_9789513199814 from '../Images/FI-book-covers/Childrens_books/small_9789513199814.jpg';
import small_9789513199876 from '../Images/FI-book-covers/Childrens_books/small_9789513199876.jpg';
import small_9789520400835 from '../Images/FI-book-covers/Childrens_books/small_9789520400835.jpg';
import small_9789520402952 from '../Images/FI-book-covers/Childrens_books/small_9789520402952.jpg';

const GridWrap = styled.ul`
  display: inline-block;
  position: absolute;
  max-width: 55rem;
  margin: 10rem auto auto auto;
  width: 130%;
  left: -15%;
  right: -15%;
  ${media.medium`
    margin: 13rem auto auto auto;
    max-width: 155rem;
    width: 110%;
    left: -5%;
    right: -5%;
  `};
`;

const ImgContainer = styled.li`
  display: ${props => (props.desktoponly ? 'none' : 'inline-block')};
  position: relative;
  width: 23%;
  margin: 0 1%;
  img {
    box-shadow: 0 0.2rem 1rem -0.1rem rgba(0, 0, 0, 0.3);
    margin-bottom: 0.6rem;
  }
  &:nth-child(odd) {
    top: -4rem;
  }
  &:nth-child(3) {
    top: -7rem;
  }
  &:nth-child(2),
  &:nth-child(6) {
    top: -10rem;
  }
  &:nth-child(4),
  &:nth-child(8) {
    top: 0rem;
  }

  ${media.medium`
    display: ${props => props.desktoponly && 'inline-block'};
    width: 11.5%;
    margin: 0 0.4% 0 0.4%;
    img {
      box-shadow: 0 0.5rem 1.5rem -0.3rem rgba(0, 0, 0, 0.3);
      margin-bottom: 1.2rem;
    }
    &:nth-child(odd) {
      top: 0;
    }
    &:nth-child(2),
    &:nth-child(6) {
      top: -10rem;
    }
    &:nth-child(4),
    &:nth-child(8) {
      top: 5rem;
    }

  `};
`;

class Grid extends React.PureComponent {
  static propTypes = {
    imagesLoaded: PropTypes.bool,
  };

  render() {

    const swedishCover = (<div>
      <GridWrap className={`visible${this.props.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={FalletSkattkartan} alt={Translation.newhomepage.children.images_alt_text.falletskattkartan} />
          <Img src={SuperCharlie} alt={Translation.newhomepage.children.images_alt_text.supercharlie} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={FiffigaKroppen} alt={Translation.newhomepage.children.images_alt_text.fiffigakroppen} />
          <Img src={SlottsMysteriet} alt={Translation.newhomepage.children.images_alt_text.slottsmysteriet} />
        </ImgContainer>
        <ImgContainer>
          <Img src={Elefanten} alt={Translation.newhomepage.children.images_alt_text.elefanten} />
          <Img src={MammaMu} alt={Translation.newhomepage.children.images_alt_text.mammamu} />
        </ImgContainer>
        <ImgContainer>
          <Img src={PelleSvanslos} alt={Translation.newhomepage.children.images_alt_text.pellesvanslos} />
          <Img src={PaxMaran} alt={Translation.newhomepage.children.images_alt_text.paxmaran} />
        </ImgContainer>
        <ImgContainer>
          <Img src={LasseMaja} alt={Translation.newhomepage.children.images_alt_text.lassemaja} />
          <Img src={SamSigge} alt={Translation.newhomepage.children.images_alt_text.samsigge} />
        </ImgContainer>
        <ImgContainer>
          <Img src={LetaSpoken} alt={Translation.newhomepage.children.images_alt_text.letaspoken} />
          <Img src={HandbokSuperhjaltar} alt={Translation.newhomepage.children.images_alt_text.handboksuperhjaltar} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Rum123} alt={Translation.newhomepage.children.images_alt_text.rum123} />
          <Img src={Bajsboken} alt={Translation.newhomepage.children.images_alt_text.bajsboken} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Vemsbyxor} alt={Translation.newhomepage.children.images_alt_text.vemsbyxor} />
          <Img src={HarBrandbild} alt={Translation.newhomepage.children.images_alt_text.harbrandbild} />
        </ImgContainer>
      </GridWrap>
    </div>);

    const finnishCover = (<div>
      <GridWrap className={`visible${this.props.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={small_9789510425138} alt={Translation.newhomepage.children.images_alt_text.falletskattkartan} />
          <Img src={small_9789510430453} alt={Translation.newhomepage.children.images_alt_text.supercharlie} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={small_9789510432983} alt={Translation.newhomepage.children.images_alt_text.fiffigakroppen} />
          <Img src={small_9789510434147} alt={Translation.newhomepage.children.images_alt_text.slottsmysteriet} />
        </ImgContainer>
        <ImgContainer>
          <Img src={small_9789510434406} alt={Translation.newhomepage.children.images_alt_text.elefanten} />
          <Img src={small_9789510435823} alt={Translation.newhomepage.children.images_alt_text.mammamu} />
        </ImgContainer>
        <ImgContainer>
          <Img src={small_9789511288756} alt={Translation.newhomepage.children.images_alt_text.pellesvanslos} />
          <Img src={small_9789511326236} alt={Translation.newhomepage.children.images_alt_text.paxmaran} />
        </ImgContainer>
        <ImgContainer>
          <Img src={small_9789511331506} alt={Translation.newhomepage.children.images_alt_text.lassemaja} />
          <Img src={small_9789512408306} alt={Translation.newhomepage.children.images_alt_text.samsigge} />
        </ImgContainer>
        <ImgContainer>
          <Img src={small_9789513197384} alt={Translation.newhomepage.children.images_alt_text.letaspoken} />
          <Img src={small_9789513197421} alt={Translation.newhomepage.children.images_alt_text.handboksuperhjaltar} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={small_9789513199814} alt={Translation.newhomepage.children.images_alt_text.rum123} />
          <Img src={small_9789513199876} alt={Translation.newhomepage.children.images_alt_text.bajsboken} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={small_9789520400835} alt={Translation.newhomepage.children.images_alt_text.vemsbyxor} />
          <Img src={small_9789520402952} alt={Translation.newhomepage.children.images_alt_text.harbrandbild} />
        </ImgContainer>
      </GridWrap>
    </div>);

    if (internationalizationLanguage === "FI") {
      return finnishCover;
    } else {
      return swedishCover;
    }
  }
}

export default Grid;
