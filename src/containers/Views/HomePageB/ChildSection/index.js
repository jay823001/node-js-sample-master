import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';

import Grid from './Grid';
import { P } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';

const Section = styled.section`
  position: relative;
  padding: 6.5rem 1.5rem;
  height: 70rem;
  background-color: ${props => props.theme.beige};
  ${media.medium`
    padding: 8rem 1.5rem;
    height: 73rem;
  `};
  ${media.large`
    height: 98rem;
  `};
`;

const Wrapper = styled.div`
  max-width: 120rem;
  padding: 0 1.5rem;
  margin: auto;
`;

const Pcustom = styled(P)`
  max-width: 52rem;
`;

class ChildSection extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };
  state = {
    imagesLoaded: false,
  };

  loadImages = () => {
    this.setState({ imagesLoaded: true });
  };

  render() {
    return (
      <Waypoint onEnter={this.loadImages} bottomOffset={'50px'}>
        <Section>
          <Wrapper>
            <Pcustom>{Translation.newhomepage.children.text}</Pcustom>
            <Grid imagesLoaded={this.state.imagesLoaded} />
          </Wrapper>
        </Section>
      </Waypoint>
    );
  }
}

export default ChildSection;
