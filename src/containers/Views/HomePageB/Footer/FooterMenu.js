import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Translation from 'translation/nextory-web-se';
import { internationalizationLanguage } from '../../../App/api'
const StyledUL = styled.ul`
  display: inline-block;
  width: 100%;
  max-width: 64rem;
  margin: 2.5rem 0 0 0;
  ${media.medium`
    margin:2.5rem 0 0 0;
  `};
`;

const Li = styled.li`
  display: inline-block;
  margin-bottom: 1rem;
  font-size: 1.5rem;
  font-weight: bold;
  max-width: 14.8rem;
  width: 47%;
  min-width: 200px;
  a {
    font-family: 'Maison';
    font-weight: normal;
  }
  ${media.medium`
    width: 32%;
  `} a:hover {
    text-decoration: underline;
  }
`;

const FooterMenu = () => (
  <StyledUL>
    <Li>
      {internationalizationLanguage === "SW" ? <a target="_blank" rel="noopener noreferrer" href="https://support.nextory.se/hc/sv">
        {Translation.newhomepage.footer.links.questions_and_answers}
      </a> :
        <a target="_blank" rel="noopener noreferrer" href="https://support.nextory.se/hc/fi">
          {Translation.newhomepage.footer.links.questions_and_answers}
        </a>}
    </Li>
    {internationalizationLanguage === "SW" ? <Li>
      <a target="_blank" rel="noopener noreferrer" href="https://www.mynewsdesk.com/se/nextory">
        {Translation.newhomepage.footer.links.press}
      </a>
    </Li> : null}
    <Li>
      <Link to="/appar">{Translation.newhomepage.footer.links.apps}</Link>
    </Li>
    <Li>
      <Link to="/om-nextory-ab">{Translation.newhomepage.footer.links.om_nextory}</Link>
    </Li>
    <Li>
      {internationalizationLanguage === "SW" ? <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://support.nextory.se/hc/sv/articles/207176175-Lediga-tjanster?_ga=1.137687289.897587104.1456923298"
      >
        {Translation.newhomepage.footer.links.job}
      </a> : <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://support.nextory.se/hc/fi/articles/207176175-Lediga-tjanster?_ga=1.137687289.897587104.1456923298"
      >
          {Translation.newhomepage.footer.links.job}
        </a>}
    </Li>
    <Li>
      <Link to="/kampanjkod">{Translation.newhomepage.footer.links.campaign_code}</Link>
    </Li>
    <Li>
      <Link to="/salja-pa-nextory">{Translation.newhomepage.footer.links.sell_via_nextory}</Link>
    </Li>
    {/* {internationalizationLanguage === "SW" ?
      <Li>
        <Link to="/presentkort">{Translation.newhomepage.footer.links.giftcards}</Link>
      </Li> :
      null} */}
  </StyledUL>
);

export default FooterMenu;
