import styled from 'styled-components';

const InnerWrapper = styled.div`
  max-width: 117rem;
  margin: auto;
`;

export default InnerWrapper;
