import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Translation from 'translation/nextory-web-se';

const Copy = styled.ul`
  margin-top: 0.5rem;
  font-size: 1.4rem;
  display: block;
  li {
    display: inline-block;
    margin: 0 1.5rem 0 0;
    &:nth-child(1) {
      display: block;
      margin: 0 0 0.7rem 0;
    }
    &:nth-child(5) {
      display: none;
    }
  }
  a:hover {
    text-decoration: underline;
  }
  ${media.medium`
    li {
      margin: 0 0 0 2rem;
      &:nth-child(5) {
        display:inline-block;
      }
      &:nth-child(1) {
        display:none;
      }
    }
  `};
`;

const year = new Date().getFullYear();

const Copyright = () => (
  <Copy>
    <li>
      {Translation.newhomepage.footer.copyrights.copyright_text} {year}{' '}
      {Translation.newhomepage.footer.copyrights.nextory_ab}
    </li>
    <li>
      <Link to="/integritetspolicy">{Translation.footer.links.integrity_terms}</Link>
    </li>
    <li>
      <Link to="/medlemsvillkor">{Translation.newhomepage.footer.copyrights.terms}</Link>
    </li>
    <li>
      <Link to="/om-cookies">{Translation.newhomepage.footer.copyrights.cookies}</Link>
    </li>
    <li>
      {Translation.newhomepage.footer.copyrights.copyright_text} {year}{' '}
      {Translation.newhomepage.footer.copyrights.nextory_ab}
    </li>
  </Copy>
);

export default Copyright;
