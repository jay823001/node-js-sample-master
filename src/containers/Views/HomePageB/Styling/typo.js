import styled from 'styled-components';
import media from 'theme/styled-utils';

export const H1 = styled.h1`
  font-family: 'MaisonDemi';
  font-size: 4.2rem;
  margin: 0 auto 2rem auto;
  line-height: 0.8;
  letter-spacing: -1.5px;
  ${media.medium`
    font-size: 7.5rem;
    margin: 0 auto 1.7rem auto;
    line-height: 1;
  `};
`;

export const H2 = styled.h2`
  font-family: 'MaisonDemi';
  line-height: 1.1;
  font-size: 4rem;
  margin: 0 auto 0 auto;
  letter-spacing: -1px;
  & + h3 {
    margin: 3.5rem 0 1rem 0;
  }
  ${media.medium`
    font-size: 5.5rem;
  `};
`;

export const H3 = styled.h3`
  font-size: 2.3rem;
  font-family: 'MaisonDemi';
  line-height: 1.1;
  margin: 0 0 0.8rem 0;
  letter-spacing: normal;
  & + p {
    margin: 0 0 2.7rem 0;
  }
  ${media.small`
    font-size: 2.3rem;
  `};
`;

export const P = styled.p`
  font-family: 'Maison';
  margin: 0 0 1.7rem 0;
  line-height: 1.39;
  font-size: 1.8rem;
`;
