import React from 'react';
import PropTypes from 'prop-types';
import media from 'theme/styled-utils';
import styled, { css } from 'styled-components';
import Button from 'components/Buttons';
import Translation from 'translation/nextory-web-se';

const Wrapper = styled.span``;

const CallToAction = styled(Button)`
  background-color: ${props => props.theme.blue};
  font-family: 'MaisonDemi';
  border-radius: 2.6rem;
  border: none;
  box-shadow: none;
  padding: 1.9rem 3rem;
  line-height: 1;
  letter-spacing: 0.9px;
  font-size: 1.3rem;
  margin-top: 4.5rem;
  margin: 4.5rem auto auto auto;
  left: 0;
  right: 0;
  position: absolute;
  width: 24rem;
  z-index: 999;
  &:hover {
    background-color: ${props => props.theme.bluehover};
  }
  ${media.medium`
    font-size: 1.5rem;
    margin-top: 6rem;
    padding: 1.9rem 4.5rem;
    width: 29rem;
  `};
  ${props =>
    props.sticky &&
    css`
      top: 2rem;
      left: 0;
      right: 0;
      margin: auto;
      position: fixed;
    `};

    ${props =>
      props.ctaborder ==='true' &&
      css`
        border: solid 1px rgba(243, 243, 243, 0.54);
      `};
`;

class HeroCta extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };

  render() {
    let activateAcc = false;

    if ('allowedactions' in this.props.activateAcc) {
      if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
        activateAcc = true;
      }
    }

    return (
      <Wrapper>
        {!this.props.loggedIn &&
          !this.props.activeReg && (
            <CallToAction sticky={this.props.sticky} primary to="/register/subscription#steg-1" ctaborder={this.props.ctaborder}>
              {Translation.newhomepage.menu.buttons.try_free}
            </CallToAction>
          )}

        {this.props.activeReg &&
          this.props.activeGiftReg !== 'REDEEM_GIFTCARD' &&
          this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' && (
            <CallToAction sticky={this.props.sticky} primary to="/register/card" ctaborder={this.props.ctaborder}>
              {Translation.newhomepage.menu.buttons.complete_registration}
            </CallToAction>
          )}

        {this.props.activeGiftReg === 'REDEEM_CAMPAIGN' && (
          <CallToAction sticky={this.props.sticky} primary to="/register-campaign/subscription" ctaborder={this.props.ctaborder}>
            {Translation.newhomepage.menu.buttons.complete_registration}
          </CallToAction>
        )}

        {this.props.activeGiftReg === 'REDEEM_GIFTCARD' && (
          <CallToAction sticky={this.props.sticky} primary to="/presentkort/subscription" ctaborder={this.props.ctaborder}>
            {Translation.newhomepage.menu.buttons.complete_registration}
          </CallToAction>
        )}

        {activateAcc &&
          this.props.loggedIn &&
          this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' &&
          this.props.activeGiftReg !== 'REDEEM_GIFTCARD' && (
            <CallToAction sticky={this.props.sticky} primary to="/konto/aktivera-abonnemang" ctaborder={this.props.ctaborder}>
              {Translation.newhomepage.menu.buttons.enable_nextory_now}
            </CallToAction>
          )}
      </Wrapper>
    );
  }
}

export default HeroCta;
