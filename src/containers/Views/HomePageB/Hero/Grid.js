import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { internationalizationLanguage } from '../../../App/api';
import Img from 'components/Img';
import ScrollDown from './ScrollDown';
import HejdaSaker from 'containers/Views/HomePageB/Images/book-covers/hej-da-saker-min.jpg';
import PaxMaran from 'containers/Views/HomePageB/Images/book-covers/pax-maran-min.jpg';
import HalsoRev from 'containers/Views/HomePageB/Images/book-covers/halsorevolutionen-min.jpg';
import HandbokSuperhjaltar from 'containers/Views/HomePageB/Images/book-covers/handbok-for-superhjaltar-min.jpg';
import OmgivenAvIdioter from 'containers/Views/HomePageB/Images/book-covers/omgiven-av-idioter-min.jpg';
import TraskkungensDotter from 'containers/Views/HomePageB/Images/book-covers/traskkungens-dotter-min.jpg';
import Glommig from 'containers/Views/HomePageB/Images/book-covers/glom-mig-min.jpg';
import DenSistaParrish from 'containers/Views/HomePageB/Images/book-covers/den-sista-mrs-parrish-min.jpg';
import SveasSon from 'containers/Views/HomePageB/Images/book-covers/sveas-son-min.jpg';
import StorstAvAllt from 'containers/Views/HomePageB/Images/book-covers/storst-av-allt-min.jpg';
import FactFullnes from 'containers/Views/HomePageB/Images/book-covers/factfullnes-min.jpg';
import Cover1793 from 'containers/Views/HomePageB/Images/book-covers/1793-min.jpg';
import FoodPharmacy from 'containers/Views/HomePageB/Images/book-covers/food-pharmacy-min.jpg';
import VildPlockat from 'containers/Views/HomePageB/Images/book-covers/vildplockat-min.jpg';
import Hjarnstark from 'containers/Views/HomePageB/Images/book-covers/hjarnstark-min.jpg';
import SiggeSally from 'containers/Views/HomePageB/Images/book-covers/sally-och-sigge-min.jpg';
import Translation from 'translation/nextory-web-se';

//Finnish book cover for Home page Adult
import small_9789510426500 from '../Images/FI-book-covers/Adult_books/small_9789510426500.jpg';
import small_9789510426791 from '../Images/FI-book-covers/Adult_books/small_9789510426791.jpg';
import small_9789510432143 from '../Images/FI-book-covers/Adult_books/small_9789510432143.jpg';
import small_9789510435823 from '../Images/FI-book-covers/Adult_books/small_9789510435823.jpg';
import small_9789511317753 from '../Images/FI-book-covers/Adult_books/small_9789511317753.jpg';
import small_9789511317838 from '../Images/FI-book-covers/Adult_books/small_9789511317838.jpg';
import small_9789511322504 from '../Images/FI-book-covers/Adult_books/small_9789511322504.jpg';
import small_9789511326236 from '../Images/FI-book-covers/Adult_books/small_9789511326236.jpg';
import small_9789511327134 from '../Images/FI-book-covers/Adult_books/small_9789511327134.jpg';
import small_9789511327141 from '../Images/FI-book-covers/Adult_books/small_9789511327141.jpg';
import small_9789512412075 from '../Images/FI-book-covers/Adult_books/small_9789512412075.jpg';
import small_9789513199708 from '../Images/FI-book-covers/Adult_books/small_9789513199708.jpg';
import small_9789513199876 from '../Images/FI-book-covers/Adult_books/small_9789513199876.jpg';
import small_9789520118051 from '../Images/FI-book-covers/Adult_books/small_9789520118051.jpg';
import small_9789520400002 from '../Images/FI-book-covers/Adult_books/small_9789520400002.jpg';
import small_9789520400101 from '../Images/FI-book-covers/Adult_books/small_9789520400101.jpg';

const GridWrap = styled.ul`
  display: inline-block;
  position: absolute;
  max-width: 55rem;
  margin: 7rem auto auto auto;
  width: 130%;
  left: -15%;
  right: -15%;
  ${media.medium`
    max-width: 155rem;
    width: 110%;
    left: -5%;
    right: -5%;
  `};
`;

const ImgContainer = styled.li`
  display: ${props => (props.desktoponly ? 'none' : 'inline-block')};
  position: relative;
  width: 23%;
  margin: 0 1%;
  img {
    box-shadow: 0 0.2rem 1rem -0.1rem rgba(0, 0, 0, 0.3);
    margin-bottom: 0.6rem;
  }
  &:nth-child(odd) {
    top: -4rem;
  }
  &:nth-child(3) {
    top: -7rem;
  }
  &:nth-child(2),
  &:nth-child(6) {
    top: -10rem;
  }
  &:nth-child(4),
  &:nth-child(8) {
    top: 0rem;
  }

  ${media.medium`
    display: ${props => props.desktoponly && 'inline-block'};
    width: 11.5%;
    margin: 0 0.4% 0 0.4%;
    img {
      box-shadow: 0 0.5rem 1.5rem -0.3rem rgba(0, 0, 0, 0.3);
      margin-bottom: 1.2rem;
    }
    &:nth-child(odd) {
      top: 0;
    }
    &:nth-child(2),
    &:nth-child(6) {s
      top: -10rem;
    }
    &:nth-child(4),
    &:nth-child(8) {
      top: 5rem;
    }

  `};
`;

class Grid extends React.PureComponent {
  state = {
    imagesLoaded: false,
  };

  componentDidMount() {
    /* this handles animation of bookcovers,
    css is available in globalstyle.js */
    this.timer = setTimeout(() => {
      this.setState({
        imagesLoaded: true,
      });
    }, 300);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {

    const swedishCover = (<div>
      <ScrollDown />
      <GridWrap className={`visible${this.state.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={HejdaSaker} alt={Translation.newhomepage.hero.images_alt_text.hejdasaker} />
          <Img src={PaxMaran} alt={Translation.newhomepage.hero.images_alt_text.paxmaran} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={VildPlockat} alt={Translation.newhomepage.hero.images_alt_text.vildplockat} />
          <Img src={HalsoRev} alt={Translation.newhomepage.hero.images_alt_text.halsorev} />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={HandbokSuperhjaltar}
            alt={Translation.newhomepage.hero.images_alt_text.handboksuperhjaltar}
          />
          <Img
            src={OmgivenAvIdioter}
            alt={Translation.newhomepage.hero.images_alt_text.omgivenavidioter}
          />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={TraskkungensDotter}
            alt={Translation.newhomepage.hero.images_alt_text.traskkungensdotter}
          />
          <Img src={Glommig} alt={Translation.newhomepage.hero.images_alt_text.glommig} />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={DenSistaParrish}
            alt={Translation.newhomepage.hero.images_alt_text.densistaparrish}
          />
          <Img
            src={FoodPharmacy}
            alt={Translation.newhomepage.hero.images_alt_text.foodpharmacy}
          />
        </ImgContainer>
        <ImgContainer>
          <Img src={Hjarnstark} alt={Translation.newhomepage.hero.images_alt_text.hjarnstark} />
          <Img src={SveasSon} alt={Translation.newhomepage.hero.images_alt_text.sveasson} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img
            src={StorstAvAllt}
            alt={Translation.newhomepage.hero.images_alt_text.storstavallt}
          />
          <Img src={FactFullnes} alt={Translation.newhomepage.hero.images_alt_text.factfullnes} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Cover1793} alt={Translation.newhomepage.hero.images_alt_text.cover1793} />
          <Img src={SiggeSally} alt={Translation.newhomepage.hero.images_alt_text.siggesally} />
        </ImgContainer>
      </GridWrap>
    </div>);

    const finnishCover = (<div>
      <ScrollDown />
      <GridWrap className={`visible${this.state.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={small_9789510426500} alt={Translation.newhomepage.hero.images_alt_text.small_9789510426500} />
          <Img src={small_9789510426791} alt={Translation.newhomepage.hero.images_alt_text.small_9789510426791} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={small_9789510432143} alt={Translation.newhomepage.hero.images_alt_text.small_9789510432143} />
          <Img src={small_9789510435823} alt={Translation.newhomepage.hero.images_alt_text.small_9789510435823} />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={small_9789511317753}
            alt={Translation.newhomepage.hero.images_alt_text.small_9789511317753}
          />
          <Img
            src={small_9789511317838}
            alt={Translation.newhomepage.hero.images_alt_text.small_9789511317838}
          />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={small_9789511322504}
            alt={Translation.newhomepage.hero.images_alt_text.small_9789511322504}
          />
          <Img src={small_9789511326236} alt={Translation.newhomepage.hero.images_alt_text.small_9789511326236} />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={small_9789511327134}
            alt={Translation.newhomepage.hero.images_alt_text.small_9789511327134}
          />
          <Img
            src={small_9789511327141}
            alt={Translation.newhomepage.hero.images_alt_text.small_9789511327141}
          />
        </ImgContainer>
        <ImgContainer>
          <Img src={small_9789512412075} alt={Translation.newhomepage.hero.images_alt_text.small_9789512412075} />
          <Img src={small_9789513199708} alt={Translation.newhomepage.hero.images_alt_text.small_9789513199708} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img
            src={small_9789513199876}
            alt={Translation.newhomepage.hero.images_alt_text.small_9789513199876}
          />
          <Img src={small_9789520118051} alt={Translation.newhomepage.hero.images_alt_text.small_9789520118051} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={small_9789520400002} alt={Translation.newhomepage.hero.images_alt_text.small_9789520400002} />
          <Img src={small_9789520400101} alt={Translation.newhomepage.hero.images_alt_text.small_9789520400101} />
        </ImgContainer>
      </GridWrap>
    </div>);



    if (internationalizationLanguage === "FI") {
      return finnishCover;
    } else {
      return swedishCover;
    }
  }
}

export default Grid;
