import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { scroller } from 'react-scroll';
import Translation from 'translation/nextory-web-se';

import { IconCaret } from './icon-caret';

const Wrapper = styled.div`
  margin: 13rem auto auto auto;
  ${media.medium`
    margin: 18rem auto auto auto;
  `};
`;

const Title = styled.span`
  display: block;
  margin: 0 auto;
  font-family: 'Maison';
  font-size: 1.8rem;
  font-weight: normal;
  letter-spacing: normal;
  ${media.medium`
    font-size: 2rem;
    max-width: 43rem;
  `};
`;

const ScrollTrigger = styled.div`
  display: inline-block;
  cursor: pointer;
`;

class ScrollDown extends React.PureComponent {
  scrollTo() {
    scroller.scrollTo('scroll-to-phone', {
      duration: 800,
      delay: 0,
      smooth: 'easeInOutQuart',
      offset: 20,
    });
  }

  render() {
    return (
      <Wrapper>
        <ScrollTrigger onClick={() => this.scrollTo()}>
          <Title>{Translation.newhomepage.menu.buttons.therefore_nextory}</Title>
          <IconCaret />
        </ScrollTrigger>
      </Wrapper>
    );
  }
}

export default ScrollDown;
