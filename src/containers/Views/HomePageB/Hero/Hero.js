import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';

import HeroCta from './HeroCta';
import Grid from './Grid';
import { H1, H2 } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';

const Hero = styled.section`
  // height: 70rem;
  height: 78rem;
  position: relative;
  padding: 1.5rem;
  text-align: center;
  background-color: ${props => props.theme.teal};
  z-index: 600;
  ${media.medium`
    padding: 1.5rem 0;
    // height: 87rem;
    height: 89rem;
  `};
  ${media.large`
    // height: 100rem;
    height: 105rem;
  `};
`;

const HeroText = styled.div`
  position: relative;
  z-index: 10;
  margin: 8rem auto auto auto;
  color: ${props => props.theme.black};
  ${media.medium`
    margin: 14rem auto auto auto;
  `};
`;

const H1custom = styled(H1)`
  font-family: 'MaisonDemi';
  font-size: 4.2rem;
  margin: 0 auto 2rem auto;
  line-height: 1.1;
  letter-spacing: -1.5px;
  max-width: 920px;

  br {
    display: none;
  }

  span {
    white-space: nowrap;
  }

  ${media.medium`
    font-size: 6rem;
    margin: 0 auto 1.7rem auto;
    line-height: 1;

    br {
      display: block;
    }
  `};

  ${media.large`
    font-size: 7.5rem;
  `};
`;

const H2custom = styled(H2)`
  max-width: 27rem;
  font-family: 'Maison';
  line-height: 1.39;
  font-size: 1.8rem;
  font-weight: normal;
  letter-spacing: normal;

  ${media.medium`
    font-size: 2rem;
    max-width: 43rem;
  `};
`;

class HeroHome extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };
  state = {
    sticky: false,
  };

  render() {
    return (
      <Hero>
        <HeroText>
          <H1custom dangerouslySetInnerHTML={{ __html: Translation.newhomepage.hero.text }} />
          <Waypoint
            onLeave={() => this.setState({ sticky: true })}
            onEnter={() => this.setState({ sticky: false })}
            topOffset={'-25px'}
          >
            <H2custom>{Translation.newhomepage.hero.subheading}</H2custom>
          </Waypoint>

          <HeroCta
            activeGiftReg={this.props.activeGiftReg}
            activeReg={this.props.activeReg}
            loggedIn={this.props.loggedIn}
            activateAcc={this.props.activateAcc}
            sticky={this.state.sticky}
            ctaborder={this.props.ctaborder}
          />
          <Grid />
        </HeroText>
      </Hero>
    );
  }
}

export default HeroHome;
