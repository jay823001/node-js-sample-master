import React from 'react';
import styled, { keyframes } from 'styled-components';

const bounce = keyframes`
 0%, 20%, 50%, 80%, 100% {
    transform: rotate(90deg) translateX(0);
  }
  40% {
    transform: rotate(90deg) translateX(-.8rem);
  }
  60% {
    transform: rotate(90deg) translateX(-.3rem);
  }
  
`;

const StyledSvg = styled.svg`
  width: 1rem;
  height: 1rem;
  transform: rotate(90deg);
  animation: ${bounce} 2s infinite;
  path {
    fill: #000000;
  }
`;

export const IconCaret = () => (
  <StyledSvg
    role="img"
    width="307.046"
    height="307.046"
    viewBox="0 0 307.046 307.046"
    aria-labelledby="caret"
  >
    <title id="caret">Caret</title>
    <path d="M239.087 142.427L101.259 4.597c-6.133-6.129-16.073-6.129-22.203 0L67.955 15.698c-6.129 6.133-6.129 16.076 0 22.201l115.621 115.626-115.621 115.61c-6.129 6.136-6.129 16.086 0 22.209l11.101 11.101c6.13 6.136 16.07 6.136 22.203 0l137.828-137.831c6.135-6.127 6.135-16.058 0-22.187z" />
  </StyledSvg>
);
