import React from 'react';
import styled from 'styled-components';

const StyledSvg = styled.svg`
  width: 2.7rem;
  height: 2.7rem;
  margin: 0 .2rem;
  path {
    fill: black;
  }
`;

export const IconStar = () => (
  <StyledSvg role="img" viewBox="0 0 30 28" aria-labelledby="star">
    <title id="star">Star</title>
    <defs>
    <linearGradient x1="50%" y1="0%" x2="50%" y2="97.3792251%" id="linearGradient-1">
      <stop stopColor="#EFBC00" offset="0%"></stop>
      <stop stopColor="#DA8F00" offset="100%"></stop>
    </linearGradient>
    </defs>
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-775.000000, -4521.000000)" fill="url(#linearGradient-1)">
            <g transform="translate(635.000000, 4521.000000)">
                <polygon points="155 22.2520394 145.72949 28 148.435187 17.5713066 140 10.6950483 150.942722 9.99772197 155 0 159.057278 9.99772197 170 10.6950483 161.564813 17.5713066 164.27051 28"></polygon>
            </g>
        </g>
    </g>
  </StyledSvg>
);
