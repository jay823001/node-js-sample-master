import React from 'react';
import styled from 'styled-components';

const StyledSvg = styled.svg`
  width: 4.5rem;
  height: 4.5rem;
  path {
    fill: ${props => props.theme.black};
  }
`;

export const IconDevices = () => (
  <StyledSvg role="img" viewBox="0 0 40 40" aria-labelledby="devices">
    <title id="devices">Devices</title>
    <path d="M18.688 14.625h-4.875a.813.813 0 0 1 0-1.625h4.874a.813.813 0 0 1 0 1.625zM16.25 32.5a1.625 1.625 0 1 1 0 3.25 1.625 1.625 0 0 1 0-3.25zm8.125 1.625a3.253 3.253 0 0 1-3.25 3.25h-9.75a3.253 3.253 0 0 1-3.25-3.25v-19.5a3.253 3.253 0 0 1 3.25-3.25h9.75a3.253 3.253 0 0 1 3.25 3.25v19.5zm-22.75 0V4.875a3.253 3.253 0 0 1 3.25-3.25h16.25a3.253 3.253 0 0 1 3.25 3.25v6.147c-.866-.781-1.999-1.272-3.25-1.272h-9.75A4.89 4.89 0 0 0 6.5 14.625v19.5c0 1.251.49 2.385 1.272 3.25H4.875a3.253 3.253 0 0 1-3.25-3.25zM21.125 0H4.875A4.89 4.89 0 0 0 0 4.875v29.25A4.89 4.89 0 0 0 4.875 39h16.25A4.89 4.89 0 0 0 26 34.125V4.875A4.89 4.89 0 0 0 21.125 0z"/>
  </StyledSvg>
);

