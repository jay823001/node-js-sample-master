import React from 'react';
import styled from 'styled-components';

const StyledSvg = styled.svg`
  width: 4.5rem;
  height: 4.5rem;
  path {
    fill: ${props => props.theme.black};
  }
`;

export const IconHeart = () => (
  <StyledSvg role="img" viewBox="0 0 24 24" aria-labelledby="heart">
    <title id="heart">Heart</title>
    <path d="M12,21l-.26-.16C11.34,20.6,2,14.89,2,8.66A5.74,5.74,0,0,1,5.64,3.29c2.14-.77,4.43,0,6.36,2,1.93-2.06,4.22-2.81,6.36-2A5.74,5.74,0,0,1,22,8.66c0,6.23-9.34,11.94-9.74,12.18ZM7.31,4A3.84,3.84,0,0,0,6,4.23,4.72,4.72,0,0,0,3,8.66c0,5.13,7.5,10.2,9,11.16,1.5-1,9-6,9-11.16a4.72,4.72,0,0,0-3-4.43c-1.87-.68-3.92.12-5.64,2.17L12,6.86l-.38-.46A5.84,5.84,0,0,0,7.31,4Z"/>
  </StyledSvg>
);

