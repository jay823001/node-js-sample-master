import React from 'react';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';
import Slider from 'react-slick';

import { H2, P } from '../Styling/typo';
import { IconStar } from 'containers/Views/HomePageB/Icons/icon-star';
import Translation from 'translation/nextory-web-se';

const Section = styled.section`
  position: relative;
  padding: 4.5rem 1.5rem 4rem;
  background-color: ${props => props.theme.teal};
  text-align: center;
  ${media.medium`
    padding: 4.5rem 1.5rem 6.5rem;
  `};
`;

const Wrapper = styled.div`
  max-width: 70rem;
  margin: auto auto 1.2rem auto;
  opacity: 0;
  transform: translateY(5rem);
  transition: all 1s ease-out;
  -webkit-touch-callout: none;
  ${props =>
    props.visible &&
    css`
      opacity: 1;
      transform: translateY(0);
    `};
`;

const H2custom = styled(H2)`
  margin: 1.8rem auto 2rem auto;
  font-size: 2rem;
  letter-spacing: normal;
  line-height: 1.27;
  ${media.medium`
    font-size: 2.4rem;
  `};
`;

const TimeLine = styled.ul`
  margin: 0 0 4rem 0;
`;

const TimeLineLi = styled.li`
  height: 0.2rem;
  position: relative;
  width: 5rem;
  background-color: ${props => props.theme.grey};
  content: '';
  display: inline-block;
  margin: 0 1rem;
  &:after {
    height: 0.2rem;
    position: absolute;
    width: 0;
    left: 0;
    background-color: #888888;
    content: '';
    display: inline-block;
  }
  ${props =>
    props.active &&
    css`
      &::after {
        width: 100%;
        transition: all 3s linear;
      }
    `};
`;

const Disclaimer = styled(P)`
  max-width: 21rem;
  margin: 2rem auto 2rem;
  ${media.medium`
    max-width: 100%;
  `};
`;

class RatingSection extends React.PureComponent {
  state = {
    activerate: 1,
    visible: false,
  };

  fadeIn = () => {
    this.setState({ visible: true });
  };

  componentDidMount() {
    this.interval = setInterval(() => {
      if (this.state.activerate === 1) {
        this.setState({ activerate: 2 });
      } else if (this.state.activerate === 2) {
        this.setState({ activerate: 3 });
      } else if (this.state.activerate === 3) {
        this.setState({ activerate: 4 });
      } else {
        this.setState({ activerate: 1 });
      }
    }, 2600);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    var settings = {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2500,
      pauseOnHover: false,
      arrows: false,
      draggable: false,
      swipe: false,
    };

    return (
      <Waypoint onEnter={this.fadeIn} bottomOffset={'100px'}>
        <Section>
          <Wrapper visible={this.state.visible}>
            <Slider {...settings}>
              <div>
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <H2custom>
                {Translation.newhomepage.ratings.rating1.text}
                </H2custom>
                <P>{Translation.newhomepage.ratings.rating1.user}</P>
              </div>
              <div>
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <H2custom>
                {Translation.newhomepage.ratings.rating2.text}
                </H2custom>
                <P>{Translation.newhomepage.ratings.rating2.user}</P>
              </div>

              <div>
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <H2custom>{Translation.newhomepage.ratings.rating3.text}</H2custom>
                <P>{Translation.newhomepage.ratings.rating3.user}</P>
              </div>
            </Slider>
          </Wrapper>

          <TimeLine>
            {this.state.activerate === 1 ? <TimeLineLi active /> : <TimeLineLi />}
            {this.state.activerate === 2 ? <TimeLineLi active /> : <TimeLineLi />}
            {this.state.activerate === 3 ? <TimeLineLi active /> : <TimeLineLi />}
            {this.state.activerate === 4 ? <TimeLineLi active /> : <TimeLineLi />}
          </TimeLine>

          <Disclaimer>{Translation.newhomepage.ratings.disclaimer}</Disclaimer>
        </Section>
      </Waypoint>
    );
  }
}

export default RatingSection;
