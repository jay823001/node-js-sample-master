import React from 'react';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';

import { H2, H3, P } from '../Styling/typo';
import Iphone from 'containers/Views/HomePageB/Images/iphone.jpg';
import IphoneSmall from 'containers/Views/HomePageB/Images/iphone-small.jpg';
import Translation from 'translation/nextory-web-se';

const Section = styled.section`
  position: relative;
  padding: 15rem 1.5rem 4.5rem;
  max-width: 110rem;
  margin: auto;
  background-color: ${props => props.theme.beige};
  opacity: 0;
  transform: translateY(5rem);
  transition: all 1s ease-out;
  @media (min-width: 500px) {
    padding: 20rem 1.5rem 4.5rem;
  }
  ${props =>
    props.visible &&
    css`
      opacity: 1;
      transform: translateY(0);
    `};
  ${media.medium`
    padding: 22rem 1.5rem 6.5rem;
  `};
  ${media.large`
    padding: 30rem 1.5rem 6.5rem;
  `};
`;

const Left = styled.div`
  display: none;
  width: 45%;
  z-index: 1;
  ${media.medium`
    display: inline-block;
  `};
`;

const Img = styled.img`
  max-width: 100%;
  width: 50rem;
  height: auto;
  user-select: none;
  border: 0;
  position: absolute;
  bottom: 0;
  ${media.medium`
    width: 40rem;
  `};
  ${media.large`
    width: 50rem;
  `};
`;

const PhoneContent = styled.div`
  margin: 0 0 2rem 0;
  height: 38rem;
  position: relative;
  img {
    height: 38rem;
    position: absolute;
    left: -1.5rem;
  }
  ${media.medium`
    display: none;
  `};
`;

const Right = styled.div`
  position: relative;
  z-index: 10;
  ${media.medium`
    display: inline-block;
    width: 45%;
    margin-left: 9.5%;
    max-width: 36rem;
  `};
`;

class PhoneSection extends React.PureComponent {
  state = {
    visible: false,
  };

  fadeIn = () => {
    this.setState({ visible: true });
  };

  render() {
    return (
      <Waypoint onEnter={this.fadeIn} bottomOffset={'300px'}>
        <Section visible={this.state.visible} name="scroll-to-phone">
          <Left>
            <Img src={Iphone} alt="Iphone" />
          </Left>
          <Right>
            <H2>{Translation.newhomepage.iphone.heading}</H2>
            <H3>{Translation.newhomepage.iphone.paragraph1.heading}</H3>
            <P>{Translation.newhomepage.iphone.paragraph1.content}</P>
            <PhoneContent>
              <img src={IphoneSmall} alt="Iphone" />
            </PhoneContent>
            <H3>{Translation.newhomepage.iphone.paragraph2.heading}</H3>
            <P>{Translation.newhomepage.iphone.paragraph2.content}</P>
            <H3>{Translation.newhomepage.iphone.paragraph3.heading}</H3>
            <P>{Translation.newhomepage.iphone.paragraph3.content}</P>
          </Right>
        </Section>
      </Waypoint>
    );
  }
}

export default PhoneSection;
