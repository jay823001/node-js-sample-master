import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SubCrossNew2, SubTickNew2 } from 'components/Subscription/SubIcons';
import { chooseSubscription } from 'containers/Views/Registration/actions';
import {
  TableWrapper,
  ButtonStyled,
  TableHeading,
  TableContent,
  TableRow,
  LI,
  TableContainer,
  TableTitle,
  TableRowContent,
  TableNo,
  PriceFrom
} from 'containers/Views/Registration/SubscriptionPage/StyledSubTable';
import Translation from 'translation/nextory-web-se';
import {
  apiUrl,
  internationalization_a,
  fetchAuth,
} from 'containers/App/api';

class SubscriptionTable extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-functions
  static propTypes = {
    chooseSubscription: PropTypes.func,
    ChosenSubscription: PropTypes.string,
  };

  state = {
    sticky: false,
    subscriptionData: null,
  };

  async componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      //console.log(data);
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package1: {
              packagename: subscriptions[0].subname,
              packageprice: subscriptions[0].subprice,
            }, package2: {
              packagename: subscriptions[1].subname,
              packageprice: subscriptions[1].subprice,
            }, package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  setSubscriptionChoice = (subscription) => {
    this.props.chooseSubscription(subscription);
    this.props.getChosenSubscription(subscription);
  }

  render() {
    localStorage.removeItem('sub-state') // Clear the localstorage to allow room for new values to be set
    this.props.getDefaultSubscription(this.props.ChosenSubscription)
    localStorage.setItem('sub-state', this.props.ChosenSubscription)
    localStorage.setItem('origin-path', window.location.hash)
    if (this.props.ChosenSubscription === Translation.app.common.family3 ||
      this.props.ChosenSubscription === Translation.app.common.family4){
      this.props.chooseSubscription(Translation.app.common.family2);
    }
    const twoWeeks = new Date(new Date().setDate(new Date().getDate() + 13));
    const date = twoWeeks.toISOString().substring(0, 10);

  // NOTE : Set pre-chosen or Default subscription in src/containers/App/auth/auth.js >> choosenSubsciption()

    return (
      <div>
        <TableWrapper>

          <TableContainer>
            <TableHeading sticky={this.props.sticky}>
              <li>
                {/*<TableTitle>{Translation.registration.stage1.tabletitle} </TableTitle>*/}
                <TableTitle>{Translation.registration.buttons.for_free_14_days}</TableTitle>
              </li>
              <li>
                <ButtonStyled
                  active={this.props.ChosenSubscription === Translation.app.common.silver}
                  onClick={() => this.setSubscriptionChoice(Translation.app.common.silver)}
                >
                  {!this.state.subscriptionData ? '' : this.state.subscriptionData.package1.packagename}
                </ButtonStyled>
              </li>
              <li>
                <ButtonStyled
                  active={this.props.ChosenSubscription === Translation.app.common.gold}
                  onClick={() => this.setSubscriptionChoice(Translation.app.common.gold)}
                >
                  {!this.state.subscriptionData ? '' : this.state.subscriptionData.package2.packagename}
                </ButtonStyled>
              </li>
              <li>
                <ButtonStyled
                  active={this.props.ChosenSubscription === Translation.app.common.family2}
                  onClick={() => this.setSubscriptionChoice(Translation.app.common.family2)}
                >
                  {/* {!this.state.subscriptionData ? '' : this.state.subscriptionData.package3.packagename} */}
                  {!this.state.subscriptionData ? '' : Translation.app.common.family}
                </ButtonStyled>
              </li>
            </TableHeading>

            <TableContent sticky={this.props.sticky}>
              <TableRowContent>
                <TableRow hiddenMobile={true}>
                  <ul>
                    <li>
                      {Translation.registration.buttons.for_free_14_days}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>

                <TableRow isPrice={true}>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item1f} {' '} {date}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <span>
                        {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package1.packageprice}
                        {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency}
                      </span>
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <span>
                        {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package2.packageprice}
                        {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency}
                      </span>
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <PriceFrom>
                        {Translation.subscription_table.family.price_from}
                      </PriceFrom>
                      <span>
                        {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package3.packageprice}
                        {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency}
                      </span>
                    </LI>
                  </ul>
                </TableRow>

                <TableRow>

                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item2}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <TableNo>
                        1
                    </TableNo>
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <TableNo>
                        1
                   </TableNo>
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <TableNo>
                        2-4
                   </TableNo>
                    </LI>
                  </ul>
                </TableRow>

                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item3}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}><SubCrossNew2 /></LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}><SubTickNew2 /></LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}><SubTickNew2 /></LI>
                  </ul>
                </TableRow>

                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item4}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>

                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item5}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>

                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item6}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>
                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item7}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>
                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item8}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>
                <TableRow>
                  <ul>
                    <li>
                      {Translation.subscription_table.listitems.item9}
                    </li>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.silver}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.gold}>
                      <SubTickNew2 />
                    </LI>
                    <LI active={this.props.ChosenSubscription === Translation.app.common.family2}>
                      <SubTickNew2 />
                    </LI>
                  </ul>
                </TableRow>
              </TableRowContent>
            </TableContent>
          </TableContainer>
        </TableWrapper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription: state.signup.ChosenSubscription,
  };
}

export default connect(mapStateToProps, { chooseSubscription })(SubscriptionTable);
