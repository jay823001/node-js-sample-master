import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import DataLayer from 'containers/App/datalayer';
import Loading from 'components/LoadingIndicator/page';
import Center from 'components/Center';
import SubscriptionTable from './SubscriptionTable';
import Translation from 'translation/nextory-web-se';
import Book from './images/book.svg';
import Waypoint from 'react-waypoint';
import FamilySubscriptionTable from './SubscriptionTable.Family';
// import { isFamilyPackage } from 'containers/App/common';
import {
  RegisterWrapper,
  H1Main,
  ButtonMain,
  ButtonPrimary,
  InnerWrapper,
  Heading,
  Span,
  InnerWrapperc,
  SubcriptionC,
  SubcriptionCInfo,
  ButtonWhiteWrapper,
  SubcriptionStage2,
  SubcriptionStage2Heading,
  SubcriptionStage2Bottom,
  BoxButtonPrimary,
  InnerWrapperd,
  SubcriptionStage2BoxBottom,
  PkgBoxContainer
} from './StyledSubTable';

class SubscriptionPage extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
  };

  state = {
    seotitle: '',
    seodesc: '',
    loadingseo: true,
    sticky: false,
    subscription: null,
    showFamilyPage: false,
  };

  componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}page-meta?pageUrl=/register/subscription/${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          seotitle: data.data.pagemeta.metaList[0].value,
          seodesc: data.data.pagemeta.metaList[1].value,
          loadingseo: false,
        });
      }
    } catch (error) {
      this.setState({ loadingseo: false });
    }
  };

  getChosenSubscription = (subscription) => {
    if (subscription) { //undefined check
      this.setState({ subscription: subscription })
    }
  }

  getDefaultSubscription = (subscription) => {
    this.setState({ subscription: subscription })
  }

  render() {
    let dataFetched = false;
    if (!this.state.loadingseo) {
      dataFetched = true;
    }

    const familyPackage2 = Translation.subscription_table.packages.family2;
    let subscriptionFamily = (this.state.subscription === familyPackage2 || localStorage.getItem('sub-state') === familyPackage2)
    let showFamilyPage = (subscriptionFamily) ? '/register/subscription#familj' : '/register/email#steg-2';
    this.setState({ showFamilyPage });
    let urlToSubscriptionPage = '/register/subscription'
    // let buttonText = subscriptionFamily ? Translation.app.common.family : this.state.subscription
    const preSubscriptionPageContent = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <SubcriptionC>
          <RegisterWrapper>
            <InnerWrapperc>
              <img src={Book} alt="" />
              <Span>
                {Translation.registration.stage} 1 {' '}{Translation.registration.of} {' '} 3
              </Span>
              <H1Main>
                {Translation.registration.stage1.heading_a}
              </H1Main>
              <SubcriptionCInfo>
                {Translation.registration.stage1.subheading_a}
              </SubcriptionCInfo>
            </InnerWrapperc>
          </RegisterWrapper>
        </SubcriptionC>
        <ButtonWhiteWrapper>
          <ButtonMain large to={urlToSubscriptionPage}>
            {Translation.registration.buttons.viewsub}
          </ButtonMain>
        </ButtonWhiteWrapper>
      </ReactCSSTransitionGroup>
    );

    const subscriptionPageContent = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />

        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>{Translation.registration.stage1.heading}</H1Main>
                  <SubcriptionCInfo>{Translation.registration.stage1.subheading}</SubcriptionCInfo>
                </SubcriptionStage2Heading>
              </Heading>
              <Waypoint
                onLeave={() => this.setState({ sticky: true })}
                onEnter={() => this.setState({ sticky: false })}
                topOffset={'0px'}
              />
            </InnerWrapper>
            <SubscriptionTable sticky={this.state.sticky} getChosenSubscription={this.getChosenSubscription} getDefaultSubscription={this.getDefaultSubscription} />

          </RegisterWrapper>
        </SubcriptionStage2>


        {/*
        ----------------------------------------------  PLEASE READ! --------------------------------------------------
        NOTE : eslint throws warnings for the below line saying "No duplicate props allowed  react/jsx-no-duplicate-props:
               But when you try to remove the 2nd sticky2 which equals props, the app will freeze at some point when the
               /register/subscription page is accessed. It is odd but for now I am ignoring this lint warning.

        The FIX : You Encouraged to fix this if you can find the root cause but make sure you dont just remove it and assume
              it will be fixed. If you remove it, please continue to test the /register/subscription page multiple times
              until you are pretty sure the problem doesn't exist
                                                                                                  - 29/09/2018 - Irshad
        Affected line : <SubcriptionStage2Bottom sticky2={this.state.sticky2} sticky2={this.props.sticky2} >

        ................................................ This is known issue.......................................
        As I have tryied to stick button of footer. made this to the testing purpose.
                                                                              - 10/09/2018 - Sagara
        */}
        <SubcriptionStage2Bottom>
          <InnerWrapper>
            <Center>
              <ButtonPrimary large to={showFamilyPage} onClick={() => this.setState({ showFamilyPage: !showFamilyPage })}>
                {/*
                {Translation.registration.buttons.choose} {' '}
                {buttonText} {' '}
                {Translation.registration.buttons.for_free_14_days}
                */}
                {/*
                {`${Translation.registration.buttons.choose} ${buttonText}`}
                */}
                {Translation.registration.buttons.continue}
              </ButtonPrimary>
            </Center>
          </InnerWrapper>

        </SubcriptionStage2Bottom>

        <Waypoint
          onLeave={() => this.setState({ sticky2: true })}
          onEnter={() => this.setState({ sticky2: false })}
        />

        {this.props.LoggedIn && <Redirect to={'/konto'} />}
      </ReactCSSTransitionGroup>
    );

    const familysubscriptionPageContent = (
      <span>
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />

        <ReactCSSTransitionGroup
          transitionAppear={true}
          transitionAppearTimeout={600}
          transitionEnterTimeout={600}
          transitionLeaveTimeout={200}
          transitionName="SlideUp" >

          <SubcriptionStage2>
            <RegisterWrapper>
              <InnerWrapper>
                <Heading>
                  <SubcriptionStage2Heading>
                    <H1Main>{Translation.registration.stage1.heading_family}</H1Main>
                    <SubcriptionCInfo>{Translation.registration.stage1.subheading_family}</SubcriptionCInfo>
                  </SubcriptionStage2Heading>
                  <PkgBoxContainer />
                </Heading>
              </InnerWrapper>
              <FamilySubscriptionTable sticky={this.state.sticky} getChosenSubscription={this.getChosenSubscription} />
            </RegisterWrapper>
          </SubcriptionStage2>

          <SubcriptionStage2BoxBottom>
            <InnerWrapperd>
              <Center>
                <BoxButtonPrimary large to={urlToSubscriptionPage} onClick={() => this.setState({ showFamilyPage: !showFamilyPage })}>
                  {Translation.registration.buttons.go_back}
                </BoxButtonPrimary>
              </Center>
            </InnerWrapperd>
          </SubcriptionStage2BoxBottom>

          {this.props.LoggedIn && <Redirect to={'/konto'} />}
        </ReactCSSTransitionGroup>
      </span>
    );

    let pageContents;
    if (this.props.location.hash === '#steg-1') {
      pageContents = preSubscriptionPageContent
    } else if (this.state.showFamilyPage === '/register/subscription#familj' && this.props.location.hash === '#familj') {
      pageContents = familysubscriptionPageContent
    } else {
      pageContents = subscriptionPageContent
    }

    return dataFetched ? pageContents : <Loading />;
  }
}
function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    LoggedIn: state.account.loggedIn,
  };
}

export default connect(mapStateToProps)(SubscriptionPage);
