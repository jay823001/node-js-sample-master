import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { chooseSubscription } from 'containers/Views/Registration/actions';
import Translation from 'translation/nextory-web-se';
import {
  apiUrl,
  internationalization_a,
  fetchAuth,
} from 'containers/App/api';
import {
  PkgBoxWrapperParent,
  PkgBoxWrapper,
  PkgBox,
  PriceContainer,
  PriceInfo,
  PriceTitle,
  PriceInfoTitle,
  PriceButton,
  PriceButtonBtn,
  Clear
} from './StyledSubTable';

class SubscriptionTable extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-functions
  static propTypes = {
    chooseSubscription: PropTypes.func,
    ChosenSubscription: PropTypes.string,
  };

  state = {
    sticky: false,
    subscriptionData: null,
  };

  async componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      // console.log(data);
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }, package4: {
              packagename: subscriptions[3].subname,
              packageprice: subscriptions[3].subprice,
            }, package5: {
              packagename: subscriptions[4].subname,
              packageprice: subscriptions[4].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const path = window.location.pathname;
    const buttonUrl = (path.substring(path.lastIndexOf('/') + 1) === 'b') ? '/register/email/b#steg-2' : '/register/email#steg-2'
    localStorage.setItem('origin-path', window.location.hash)
    return (
      <span>
        <PkgBoxWrapperParent>
          <PkgBoxWrapper>
            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 2
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                    {' ' + Translation.subscription_table.family.days_free_then}
                    <b>
                      {!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package3.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b>
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  <PriceButtonBtn onClick={() => this.props.chooseSubscription(Translation.app.common.family2)} to={buttonUrl} >
                    {Translation.subscription_table.family.choose} {' '} <span> 2 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn>
                </PriceButton>
              </PriceContainer>
            </PkgBox>

            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 3
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>{' ' + Translation.subscription_table.family.days_free_then}
                    <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package4.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b>
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  <PriceButtonBtn onClick={() => this.props.chooseSubscription(Translation.app.common.family3)} to={buttonUrl} >
                    {Translation.subscription_table.family.choose} {' '} <span> 3 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn>
                </PriceButton>
              </PriceContainer>
            </PkgBox>

            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 4
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>{' ' + Translation.subscription_table.family.days_free_then}
                    <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package5.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b>
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  <PriceButtonBtn onClick={() => this.props.chooseSubscription(Translation.app.common.family4)} to={buttonUrl} >
                    {Translation.subscription_table.family.choose} {' '} <span> 4 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn>
                </PriceButton>
              </PriceContainer>
            </PkgBox>
            <Clear />
          </PkgBoxWrapper>
          <Clear />
        </PkgBoxWrapperParent>
      </span>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription: state.signup.ChosenSubscription,
  };
}

export default connect(mapStateToProps, { chooseSubscription })(SubscriptionTable);
