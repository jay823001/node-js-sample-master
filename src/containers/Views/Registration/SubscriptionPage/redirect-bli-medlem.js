import React from 'react';
import { Redirect } from 'react-router';

class RedirectSubscriptionPage extends React.PureComponent {
  render() {
    return <Redirect to={'/register/subscription'} />;
  }
}

export default RedirectSubscriptionPage;
