import {
  CHOOSE_SUBSCRIPTION,
  CREATEUSER_REQUEST,
  CREATEUSER_LOGOUT,
  REGPAYMENT_REQUEST,
  CHOOSE_MONTH,
  CHOOSE_AMOUNT,
  USER_REDEEM_GIFT,
  CHECK_USER_ACTIVE,
} from './constants';

// Signup acctions
export const chooseSubscription = subscription => ({ type: CHOOSE_SUBSCRIPTION, subscription });

export const createUserRequest = data => ({ type: CREATEUSER_REQUEST, data });
export const createUserLogout = error => ({ type: CREATEUSER_LOGOUT, error });

export const regPaymentRequest = data => ({ type: REGPAYMENT_REQUEST, data });

export const chooseMonth = month => ({ type: CHOOSE_MONTH, month });

export const chooseAmount = amount => ({ type: CHOOSE_AMOUNT, amount });

export const userRedeemGiftvoucer = (data) => ({ type: USER_REDEEM_GIFT, data });
export const checkUserActive = (data) => ({ type: CHECK_USER_ACTIVE, data });
