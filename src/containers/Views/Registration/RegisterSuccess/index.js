import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { gup } from 'utils/helpFunctions';

import DataLayer from 'containers/App/datalayer';
import AdTraction from './adtraction';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import Center from 'components/Center';
import RegSuccessForm from './RegSuccessForm';
import Translation from 'translation/nextory-web-se';

const RegisterWrapper = styled.section`
  padding: 3rem 0 8rem;
  ${media.medium`
    padding: 7rem 0 12rem;
  `};
`;

const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 45rem;
`;

const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
    margin-bottom: 2.5rem;
  `};
`;

class RegisterSuccessPage extends React.PureComponent {
  static propTypes = {
    routeSearch: PropTypes.string,
    UserDetails: PropTypes.object,
  };

  state = {
    sendlayer: false,
  };

  componentDidMount() {
    const orderid = gup('orderid', this.props.routeSearch);
    this.orderCheck(orderid);
  }

  orderCheck = orderid => {
    const cookie = new Cookies();
    if (!cookie.get(`transresp${orderid}`)) {
      cookie.set(`transresp${orderid}`, orderid, { path: '/', maxAge: 86400 });
      this.setState({ sendlayer: true });
    }
  };

  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        {this.state.sendlayer && <DataLayer />}
        {this.state.sendlayer && <AdTraction />}

        <RegisterWrapper>
          <InnerWrapper>
            {this.props.routeSearch.includes('reactivated') ? (
              <Heading>
                <H1>{Translation.registration.register_success.reactivated.heading}</H1>
                <H2>
                {Translation.registration.register_success.reactivated.subheading}
                </H2>
              </Heading>
            ) : (
              <Heading>
                <H1>{Translation.registration.register_success.new.heading}</H1>
                <H2>{Translation.registration.register_success.new.subheading}</H2>
              </Heading>
            )}

            <Center>
              <RegSuccessForm UserDetails={this.props.UserDetails} />
            </Center>
          </InnerWrapper>
        </RegisterWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    routeSearch: state.route.location.search,
    UserDetails: state.account.userData.UserDetails,
  };
}

export default connect(mapStateToProps)(RegisterSuccessPage);
