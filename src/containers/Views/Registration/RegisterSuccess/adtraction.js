import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { gup } from 'utils/helpFunctions';
import md5 from 'js-md5';
import { internationalizationLanguage } from 'containers/App/api';

const AdTraction = props => {
  let hashedemail = '';
  if (Object.keys(props.UserDetails).length > 1) {
    hashedemail = md5(props.UserDetails.email);
  }
  const ordernumber = gup('orderid', props.routeSearch);
  const cpn = gup('campaignname', props.routeSearch);

  let updateAddtraction = function () {
    window.ADT = window.ADT || {};
    window.ADT.Tag = window.ADT.Tag || {};
    window.ADT.Tag.t = 3;
    window.ADT.Tag.c = internationalizationLanguage === 'FI' ? "EUR" : "SEK";
    window.ADT.Tag.tp = internationalizationLanguage === 'FI' ? 1248237634 : 1105175412;;
    window.ADT.Tag.am = 0; //this is set to zero since price isn't required for now | //props.UserDetails.subscriptiondetails.price;
    window.ADT.Tag.ti = "" + ordernumber + "";
    window.ADT.Tag.xd = "" + hashedemail + "";
    window.ADT.Tag.cpn = cpn;
    if (window.ADT.Tag.doEvent) {
      window.ADT.Tag.doEvent()
    }
  }
  return (
    <Helmet>
      {updateAddtraction()}
    </Helmet>
  );
};

AdTraction.propTypes = {
  UserDetails: PropTypes.object,
  routeSearch: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    UserDetails: state.account.userData.UserDetails,
    routeSearch: state.route.location.search,
  };
}

export default connect(mapStateToProps)(AdTraction);
