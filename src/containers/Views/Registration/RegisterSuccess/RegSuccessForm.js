import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import { updateProfile } from 'containers/Views/Account/actions';
import styled from 'styled-components';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import { validate } from './validate';
import Phonenumber from '../../Account/MyAccount/phonevalidate';
import Translation from 'translation/nextory-web-se';

const Submit = styled.button`
  margin-top: 1rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;
const SubmitSC = Button.withComponent(Submit);

class RegSuccessForm extends React.PureComponent {
  static propTypes = {
    LoggedInData: PropTypes.object,
  };
  submit = (values, updateProfile) => {
    const authtoken = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(authtoken.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    }

    let firstname = '';
    if (values.firstname !== undefined) {
      firstname = values.firstname;
    }
    let lastname = '';
    if (values.lastname !== undefined) {
      lastname = values.lastname;
    }
    let cellphone = '';
    if (values.cellphone !== undefined) {
      cellphone = values.cellphone;
    }

    const auth = this.props.LoggedInData.authkey;
    const password = '';
    const email = this.props.LoggedInData.email;
    const address = '';
    const postcode = '';
    const city = '';
    const newsletter = '';
    const data = {
      firstname,
      lastname,
      cellphone,
      auth,
      password,
      email,
      address,
      postcode,
      city,
      newsletter,
    };
    updateProfile(data);
  };

  render() {
    const { handleSubmit, updateProfile, submitSucceeded, submitting } = this.props;
    let reactivate = false;
    if (this.props.routeSearch.includes('reactivated')) {
      reactivate = true;
    }

    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, updateProfile))}>
        <Field
          name="firstname"
          type="text"
          component={renderField}
          label={Translation.forms.labels.firstname}
        />
        <Field
          name="lastname"
          type="text"
          component={renderField}
          label={Translation.forms.labels.lastname}
        />
        <Field
          name="cellphone"
          type="text"
          component={renderField}
          label={Translation.forms.labels.phone}
          normalize={Phonenumber}
        />

        <SubmitSC large type="submit" disabled={submitting}>
          {Translation.registration.buttons.continue}
        </SubmitSC>

        {submitSucceeded ? (
          reactivate ? (
            <Redirect to={'/kom-igang?reactivated'} />
          ) : (
            <Redirect to={'/kom-igang'} />
          )
        ) : null}
      </Form>
    );
  }
}

RegSuccessForm.propTypes = {
  handleSubmit: PropTypes.func,
  updateProfile: PropTypes.func,
  submitting: PropTypes.bool,
  submitSucceeded: PropTypes.bool,
  routeSearch: PropTypes.string,
};

function mapStateToProps(state, ownProps) {
  return {
    LoggedInData: state.account.userData,
    routeSearch: state.route.location.search,
    initialValues: {
      firstname: ownProps.UserDetails.firstname,
      lastname: ownProps.UserDetails.lastname,
      cellphone: ownProps.UserDetails.cellphone,
    },
  };
}

export default connect(mapStateToProps, { updateProfile, refreshAuth })(
  reduxForm({
    form: 'regsuccess',
    validate,
    enableReinitialize: true,
  })(RegSuccessForm)
);
