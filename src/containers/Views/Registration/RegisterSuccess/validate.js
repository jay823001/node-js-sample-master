import Translation from 'translation/nextory-web-se';

export const validate = values => {
  const errors = {};

  if (values.cellphone !== null) {
    if (
      values.cellphone.length !== 0 &&
      (values.cellphone.length < 7 || values.cellphone.length > 12)
    ) {
      errors.cellphone = Translation.forms.validation.phone;
    }
  }

  return errors;
};
