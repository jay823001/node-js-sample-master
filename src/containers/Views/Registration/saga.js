import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { stopSubmit, startSubmit } from 'redux-form';
import { Base64 } from 'js-base64';
import Cookies from 'universal-cookie';
import { parseJwt } from 'utils/helpFunctions';
import {
  fetchLogin,
  postUserCreate,
  postRegPayment,
  retryPayment,
  fetchUserDetails,
  refreshAuthkey,
} from 'containers/App/api';
import { REFRESH_STORE, LOGOUT } from '../../Views/Account/MyAccount/constants';
import {
  CREATEUSER_REQUEST,
  CREATEUSER_LOGOUT,
  CREATEUSER_ERROR,
  CREATEUSER_CLEAR_ERROR,
  CREATEUSER_SETAUTH,
  REGPAYMENT_REQUEST,
  REGPAYMENT_ERROR,
  USER_REDEEM_GIFT,
  CHECK_USER_ACTIVE,
} from './constants';
import { SET_AUTH } from '../../Views/Account/LoginPage/constants';
import Translation from 'translation/nextory-web-se';
import { getSubscriptionIdByName } from '../../App/common';

// new ui actiontypes
import { REGISTRATION_SUCCESS } from 'newui/domain/Modules/User/actionTypes'

const cookies = new Cookies();
/*
* Register user function
*/
function* createUser(data) {
  let errors = {};
  const campC = 'campaigncode' in data.data;
  const giftV = 'giftvoucher' in data.data;

  /*
  * IF USER IS CREATED VIA CAMPAIGN
  */
  if (campC) {
    yield put(startSubmit('campaignregisteruser'));
    const userdata = yield call(postUserCreate, data);
    // if user creation works, we temporarly log in
    // if not we show some errors
    if (userdata.status === 200) {
      // set authkeys for the use of new flow
      const registrationData = {
        authkey: userdata.authkey,
        refreshkey: userdata.refreshauthkey
      }
      yield put({ type: REGISTRATION_SUCCESS, registrationData })
      //=====================================

      // if this is a nonmember redirect to choose sub page,
      // otherwise login user and redirect to transresp
      if (userdata.data.paymentrequired === true) {
        const user = {
          campaigncode: data.data.campaigncode,
          campaignname: userdata.data.campaignname,
          campaignprice: userdata.data.campaigndetails.campaignprice,
          campaignintervel: userdata.data.campaigndetails.intervel,
          campaignintervelindays: userdata.data.campaigndetails.intervelindays,
          campaignperiod: userdata.data.campaigndetails.period,
          campaigndays: userdata.data.campaigndetails.trialdays,
          campaigndiscounted: userdata.data.campaigndetails.discounted,
          campaigntrialdays: userdata.data.campaigndetails.trialdays,
          campaignfixed: userdata.data.campaigndetails.fixedcamp,
          newuser: userdata.data.newuser,
          paymentrequired: userdata.data.paymentrequired,
          paymenttype: userdata.data.paymenttype,
          authkey: userdata.authkey,
          userid: userdata.data.userinfo.userid,
          orderid: userdata.data.orderid,
          email: data.data.email,
          password: data.data.password,
        };
        const userEncode = Base64.encode(JSON.stringify(user));
        cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

        yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
        yield put({ type: CREATEUSER_CLEAR_ERROR });

        yield put(push('/register-campaign/subscription#steg-1'));
      } else if (userdata.data.paymentrequired !== true) {
        const logindata = {
          data: {
            email: data.data.email,
            password: data.data.password,
            campaigncode: data.data.campaigncode,
          },
        };

        // api call for login
        const userdata = yield call(fetchLogin, logindata);
        // user DETAILS fetch
        const UserDetails = yield call(fetchUserDetails, userdata.authkey);

        // set logged in auth state
        userdata.data.email = data.data.email;
        userdata.data.p = data.data.password;
        userdata.data.authkey = userdata.authkey;
        userdata.data.refreshkey = userdata.refreshkey;
        userdata.data.UserDetails = UserDetails;
        const userEncode = Base64.encode(JSON.stringify(userdata.data));
        cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

        yield put({ type: SET_AUTH, newAuthState: true });

        // logout possible registration user
        cookies.remove('reg', { path: '/' });
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });

        // clear errors and stop sending
        yield put({ type: CREATEUSER_CLEAR_ERROR });
        yield put(push('/konto?campaigncode-added'));
      }
    } else if (userdata.status === 401) {
      // auth fail
      yield put({ type: CREATEUSER_ERROR, error: userdata.error });
      errors = { password: Translation.api_messages.error.password_error };
    } else if (userdata.status === 400) {
      // code validation fail
      yield put({ type: CREATEUSER_ERROR, error: userdata.error });
      errors = { campcode: Translation.api_messages.error.campaigncode_error };
    }
    yield put(stopSubmit('campaignregisteruser', errors));

    /*
  * IF USER IS CREATED VIA GIFTVOUCHER
  */
  } else if (giftV) {
    yield put(startSubmit('giftregisteruser'));

    // create a registration temp user
    const response = yield call(postUserCreate, data);

    // if user creation works, start user creation
    if (response.status === 200) {

      // set authkeys for the use of new flow
      const registrationData = {
        authkey: response.authkey,
        refreshkey: response.refreshauthkey
      }
      yield put({ type: REGISTRATION_SUCCESS, registrationData })
      //=====================================
      // if regstatus is completed, log in user
      if (response.data.registrationstatus === 'COMPLETED') {
        // login active user via cookie data
        const decoded = JSON.parse(Base64.decode(cookies.get('reg')));
        localStorage.setItem('order-id', response.data.orderid)

        // api call for login
        const logindata = {
          data: {
            email: decoded.email,
            password: decoded.password,
          },
        };

        //call /login
        const userdata = yield call(fetchLogin, logindata);

        // call /userdetails
        const UserDetails = yield call(fetchUserDetails, userdata.authkey);

        // set userdetails as a cookie "user"
        userdata.data.email = decoded.email;
        userdata.data.p = decoded.password;
        userdata.data.authkey = userdata.authkey;
        userdata.data.refreshkey = userdata.refreshkey;
        userdata.data.UserDetails = UserDetails;
        const userEncode = Base64.encode(JSON.stringify(userdata.data));
        cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

        yield put({ type: SET_AUTH, newAuthState: true });

        // logout registration user
        cookies.remove('reg', { path: '/' });
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });

        // clear errors and stop sending
        yield put({ type: CREATEUSER_CLEAR_ERROR });

        // redirect to success page
        yield put(push(`/completed?orderid=${response.data.orderid}`));


        // if regstatus is not yet start, start it
      } else {
        const user = {
          campaigncode: data.data.giftvoucher,
          email: data.data.email,
          password: data.data.password,
          cleanpass: data.data.cleanpass,
          paymenttype: response.data.paymenttype,
          paymentdetails: response.data.paymentdetails,
        };
        const userEncode = Base64.encode(JSON.stringify(user));
        cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

        yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });

        yield put({ type: CREATEUSER_CLEAR_ERROR });

        // redirect to choose sub page
        if (response.data.newuser === true) {
          //yield put(push('/presentkort/subscription'));
          yield put(push('/presentkort/subscription'));
        }
      }

      // if user creation doesnt work not we show some errors
    } else if (response.error.code === 6000) {
      // auth fail
      yield put({ type: CREATEUSER_ERROR, error: response.error });
      errors = {
        email: Translation.api_messages.error.email1_error,
        repeatemail: Translation.api_messages.error.email1_error,
      };
    } else if (response.error.code === 101) {
      // code validation fail
      yield put({ type: CREATEUSER_ERROR, error: response.error });
      if (response.error.details.email === 'USER_NOT_FOUND') {
        errors = {
          email: Translation.api_messages.error.email2_error,
        };
      } else if (response.error.details.giftvoucher === 'INVALID_GIFTCARD_CODE') {
        errors = {
          giftvoucher: Translation.api_messages.error.giftvoucher_error,
        };
      } else {
        errors = {
          giftvoucher: Translation.api_messages.error.giftvoucher_error,
        };
      }
    } else {
      yield put({ type: CREATEUSER_ERROR, error: response.error });
    }
    yield put(stopSubmit('giftregisteruser', errors));

    /*
  * IF USER IS CREATED VIA TRIAL SIGNUP
  */
  } else {
    yield put(startSubmit('registeruser'));

    // clear any existing regtoken
    cookies.remove('reg', { path: '/' });
    cookies.remove('gift', { path: '/' });

    // api call
    const userdata = yield call(postUserCreate, data);

    // if user creation works, we log in user and send them to cardregistration
    if (userdata.status === 200) {
      const reg = {
        campaignname: userdata.data.campaignname,
        paymenttype: userdata.data.paymenttype,
        authkey: userdata.authkey,
        userid: userdata.data.userinfo.userid,
        email: data.data.email,
        password: data.data.password,
        cleanpass: data.data.cleanpass,
      };
      const userEncode = Base64.encode(JSON.stringify(reg));
      cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

      yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });

      // redirect to success page
      // const path = window.location.pathname;
      let nextStep = '/register/card';
      // if (path.substring(path.lastIndexOf('/') + 1) === 'b') {
      //   nextStep = '/register/card/b';
      // }
      yield put(push(nextStep));

      yield put({ type: CREATEUSER_CLEAR_ERROR });

      // set authkeys for the use of new flow
      const registrationData = {
        authkey: userdata.authkey,
        refreshkey: userdata.refreshauthkey
      }
      yield put({ type: REGISTRATION_SUCCESS, registrationData })
      //=====================================

      // else if user creation fails,
    } else if (userdata.status === 400) {
      if (userdata.error.code === 6000) {
        const logindata = {
          data: {
            email: data.data.email,
            password: data.data.password,
          },
        };
        //call /login
        const userdata = yield call(fetchLogin, logindata);
        // if user has added valid login details, log them in and send to a "welcome page"
        if (userdata.status === 200) {
          // call /userdetails
          const UserDetails = yield call(fetchUserDetails, userdata.authkey);
          // set userdetails as a cookie "user"
          userdata.data.email = data.data.email;
          userdata.data.p = data.data.password;
          userdata.data.authkey = userdata.authkey;
          userdata.data.refreshkey = userdata.refreshkey;
          userdata.data.UserDetails = UserDetails;
          const userEncode = Base64.encode(JSON.stringify(userdata.data));
          cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });
          // set auth to true
          yield put({ type: SET_AUTH, newAuthState: true });

          yield put(push('/konto'));

          yield put({ type: CREATEUSER_CLEAR_ERROR });

          // if user has entered an existing email but wrong password, show error
        } else if (userdata.status === 401) {
          yield put({ type: CREATEUSER_ERROR, error: userdata.error });
          errors = {
            email: Translation.api_messages.error.email1_error,
            repeatemail: Translation.api_messages.error.email1_error,
            password: Translation.api_messages.error.password_error,
          };
        } else {
          console.log('something is wrong with api');
          yield put({ type: CREATEUSER_ERROR, error: userdata.error });
          errors = {
            email: Translation.api_messages.error.connection_error,
            repeatemail: Translation.api_messages.error.connection_error,
            password: Translation.api_messages.error.connection_error,
          };
        }
      } else if (userdata.error.code === 3004) {
        yield put({ type: CREATEUSER_ERROR, error: userdata.error });
        errors = {
          email: Translation.api_messages.error.other_region_error,
        };
      }
    } else if (userdata.status !== 200 || userdata.status !== 400) {
      console.log('something is wrong with api');
      yield put({ type: CREATEUSER_ERROR, error: userdata.error });
      errors = {
        email: Translation.api_messages.error.connection_error,
        repeatemail: Translation.api_messages.error.connection_error,
        password: Translation.api_messages.error.connection_error,
      };
    }

    yield put(stopSubmit('registeruser', errors));
  }
}
// Register user saga
export function* createUserSaga() {
  yield takeLatest(CREATEUSER_REQUEST, createUser);
}

/*
* Register payment function
*/
function* regPayment(data) {
  cookies.remove('retry', { path: '/' });

  // check validity of authtoken if needed
  if (cookies.get('user')) {
    const data = JSON.parse(Base64.decode(cookies.get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;

    // if key expired, fetch a new one
    if (jwt.exp < current_time) {
      const response = yield call(refreshAuthkey, data);
      if (response.status === 200) {
        // refresh user auth
        data.authkey = response.newauthkey;
        cookies.set('user', Base64.encode(JSON.stringify(data)), { path: '/', maxAge: 1209600 });
        // set auth to true
        yield put({ type: SET_AUTH, newAuthState: true });
        //reload browser to get new values based on new auth
        window.location.reload(true);
      } else {
        yield put({ type: LOGOUT });
      }
    }
  }

  /* if paymentmethod is TRUSTLY */
  if (data.data.paymentgateway === 'TRUSTLY') {
    cookies.set('trustly', Base64.encode(JSON.stringify(data)), { path: '/', maxAge: 1209600 });

    // api call
    const trustlydata = yield call(postRegPayment, data);

    if (trustlydata.status === 200) {
      window.location.href = trustlydata.data.iframeURL;
    } else {
      // refresh browser
      window.location.reload(true);
    }

    /* if paymentmethod is ADYEN */
  } else if (data.data.paymentgateway === 'ADYEN') {
    /*
    * UPDATE CARD INFO FOR LOGGED IN USER
    */
    if (data.data.paymenttype === 'CARD_UPDATE') {
      yield put(startSubmit('registercard'));

      // api call
      const regdata = yield call(postRegPayment, data);

      if (regdata.status === 200) {
        // refresh store saga
        yield put({ type: REFRESH_STORE, authkey: data.data.authkey });

        // clear errors and stop sending
        yield put({ type: CREATEUSER_CLEAR_ERROR });

        // redirect to success page
        yield put(push('/konto?updated-card'));
      } else if (regdata.status !== 200) {
        // create retry payment
        const newdata = {
          paymenttype: data.data.paymenttype,
          authkey: data.data.authkey,
          subscriptionid: data.data.subscriptionid,
          orderid: regdata.data.orderid,
        };
        //call retry to get new orderid
        const neworderrespons = yield call(retryPayment, newdata);
        //set cookie to keep the new orderid
        cookies.set('retry', neworderrespons.data.orderid, { path: '/', maxAge: 1209600 });
        // payment error
        yield put({ type: REGPAYMENT_ERROR, error: regdata });
      }
      yield put(stopSubmit('registercard'));

      /*
    * BUYING GIFTCARD FOR VISITORS
    */
    } else if (data.data.paymenttype === 'PURCHASE_GIFTCARD') {

      yield put(startSubmit('registercard'));
      // api call
      const regdata = yield call(postRegPayment, data);

      if (regdata.status === 200) {
        // refresh store saga if user is logged in
        if (data.data.loggedin) {
          yield put({ type: REFRESH_STORE, authkey: data.authkey });
        }

        // redirect to success page
        yield put(push(`/presentkort/thankyou?orderid=${regdata.data.orderid}`));

      } else if (regdata.status !== 200) {

        // create retry payment
        const newdata = {
          paymenttype: data.data.paymenttype,
          authkey: data.data.authkey,
          subscriptionid: data.data.subscriptionid,
          orderid: regdata.data.orderid,
        };
        //call retry to get new orderid
        const neworderrespons = yield call(retryPayment, newdata);
        //set cookie to keep the new orderid
        cookies.set('retry', neworderrespons.data.orderid, { path: '/', maxAge: 1209600 });
        // payment error
        yield put({ type: REGPAYMENT_ERROR, error: regdata });
      }
      yield put(stopSubmit('registercard'));

      /*
    * REACTIVEATE PAYMENT
    */
    } else if (data.data.paymenttype === 'PURCHASE_SUBSCRIPTION') {
      yield put(startSubmit('registercard'));

      // api call
      const regdata = yield call(postRegPayment, data);

      if (regdata.status === 200) {
        // refresh store saga
        yield put({ type: REFRESH_STORE, authkey: data.data.authkey });

        // clear errors and stop sending
        yield put({ type: CREATEUSER_CLEAR_ERROR });

        // redirect to success page
        yield put(push(`/transresp?reactivated&orderid=${regdata.data.orderid}`));
      } else if (regdata.status !== 200) {
        // create retry payment
        const newdata = {
          paymenttype: data.data.paymenttype,
          authkey: data.data.authkey,
          subscriptionid: data.data.subscriptionid,
          orderid: regdata.data.orderid,
        };
        //call retry to get new orderid
        const neworderrespons = yield call(retryPayment, newdata);
        //set cookie to keep the new orderid
        cookies.set('retry', neworderrespons.data.orderid, { path: '/', maxAge: 1209600 });
        // payment error
        yield put({ type: REGPAYMENT_ERROR, error: regdata });
      }
      yield put(stopSubmit('registercard'));

      /*
    * PAYMENT FOR REDEEMING CAMPAIGN
    */
    } else if (data.data.paymenttype === 'REDEEM_CAMPAIGN') {
      yield put(startSubmit('registercard'));

      const regdata = yield call(postRegPayment, data);

      if (regdata.status === 200) {
        // login active user via cookie data
        const decoded = JSON.parse(Base64.decode(cookies.get('reg')));

        // api call for login
        const logindata = {
          data: {
            email: decoded.email,
            password: decoded.password,
          },
        };

        //call /login
        const userdata = yield call(fetchLogin, logindata);

        // call /userdetails
        const UserDetails = yield call(fetchUserDetails, userdata.authkey);

        // set userdetails as a cookie "user"
        userdata.data.email = decoded.email;
        userdata.data.p = decoded.password;
        userdata.data.authkey = userdata.authkey;
        userdata.data.refreshkey = userdata.refreshkey;
        userdata.data.UserDetails = UserDetails;
        const userEncode = Base64.encode(JSON.stringify(userdata.data));
        cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

        // set auth to true
        yield put({ type: SET_AUTH, newAuthState: true });

        // redirect to success page
        let id = '';
        if (regdata.data.orderid) {
          id = regdata.data.orderid;
        } else {
          id = data.data.orderid;
        }
        // console.log(id);

        if (decoded.newuser) {
          yield put(push(`/transresp?orderid=${id}&campaignname=${data.data.campaigncode}`));
        } else {
          yield put(
            push(`/transresp?reactivated&orderid=${id}&campaignname=${data.data.campaigncode}`)
          );
        }
        // logout registration user
        cookies.remove('reg', { path: '/' });
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });
        yield put({ type: CREATEUSER_CLEAR_ERROR });
      } else if (regdata.status !== 200) {
        // create retry payment
        const newdata = {
          paymenttype: data.data.paymenttype,
          authkey: data.data.authkey,
          subscriptionid: data.data.subscriptionid,
          orderid: regdata.data.orderid,
        };
        //call retry to get new orderid
        const neworderrespons = yield call(retryPayment, newdata);
        //set cookie to keep the new orderid
        cookies.set('retry', neworderrespons.data.orderid, { path: '/', maxAge: 1209600 });
        // payment error
        yield put({ type: REGPAYMENT_ERROR, error: regdata });
      }
      yield put(stopSubmit('registercard'));

      /*
    * PAYMENT FOR TRIAL
    */
    } else if (data.data.paymenttype === 'TRAIL') {


      if (localStorage.getItem('SwitchBoardSetSub')) {
        let subscriptionname = localStorage.getItem('SwitchBoardSetSub');
        data.data.subscriptionid = getSubscriptionIdByName(subscriptionname)
      }

      yield put(startSubmit('registercard'));

      // call /payment api
      const regdata = yield call(postRegPayment, data);

      if (regdata.status === 200) {
        // login active user via cookie data
        const decoded = JSON.parse(Base64.decode(cookies.get('reg')));

        // api call for login
        const logindata = {
          data: {
            email: decoded.email,
            password: decoded.password,
          },
        };

        //Registered user logout and then make the payment, this will be used
        if (!logindata.data.password) {
          logindata.data.password = localStorage.getItem('pword')
        }

        //call /login
        const userdata = yield call(fetchLogin, logindata);

        // call /userdetails
        const UserDetails = yield call(fetchUserDetails, userdata.authkey);

        // set userdetails as a cookie "user"
        userdata.data.email = decoded.email;
        userdata.data.p = decoded.password;
        userdata.data.authkey = userdata.authkey;
        userdata.data.refreshkey = userdata.refreshkey;
        userdata.data.UserDetails = UserDetails;
        const userEncode = Base64.encode(JSON.stringify(userdata.data));
        cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

        // set auth to true
        yield put({ type: SET_AUTH, newAuthState: true });

        // redirect to success page
        yield put(push(`/transresp?orderid=${regdata.data.orderid}`));

        // logout registration user
        cookies.remove('reg', { path: '/' });
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });
        yield put({ type: CREATEUSER_CLEAR_ERROR });
      } else if (regdata.status !== 200) {
        yield put({ type: REGPAYMENT_ERROR, error: regdata });
      }

      yield put(stopSubmit('registercard'));
    }
  }
}
// Register payment saga
export function* regPaymentSaga() {
  yield takeLatest(REGPAYMENT_REQUEST, regPayment);
}

/*
* SignupLogout
*/
function* logout() {
  // remove reg cookie
  cookies.remove('reg', { path: '/' });
  cookies.remove('retry', { path: '/' });
  cookies.remove('trustly', { path: '/' });
  cookies.remove('gift', { path: '/' });

  yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });
}
export function* createUserLogoutSaga() {
  yield takeLatest(CREATEUSER_LOGOUT, logout);
}

export function* redeemGiftvoucherUser() {
  yield takeLatest(USER_REDEEM_GIFT, redeemGiftVUser);
}

function* checkUserActive(data) {
  yield put(startSubmit('giftregisteruser'));
  const response = yield call(postUserCreate, data);
  return response
}

export function* redeemGiftVUser() {
  yield takeLatest(CHECK_USER_ACTIVE, checkUserActive);
}
