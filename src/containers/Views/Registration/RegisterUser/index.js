import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import DataLayer from 'containers/App/datalayer';
import { connect } from 'react-redux';

import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import Button from 'components/Buttons';
import Center from 'components/Center';
import RegUserForm from './RegUserForm';
import Translation from 'translation/nextory-web-se';
import Phone from './phone.svg';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  Span,
  SubcriptionC,
  SubcriptionCInfo,
  H1Main,
  ButtonMain,
  SeccondWrapper,
  ButtonWrapper,
  ButtonMain2,
  RegisterWrapperForm,
  InnerWrapperForm,
  RegUserInfo,
  SeccondTitle,
  RegUserFormRow,
  Clear,
  H3Styled
} from './StyledRegisterUser';


class RegisterUser extends React.PureComponent {
  static propTypes = {
    ActiveRegEmail: PropTypes.string,
  };

  render() {
    let prevButtonUrl = localStorage.getItem('origin-path') === '#familj' ? '/register/subscription#familj' : '/register/subscription'
    const preRegisterPageContent = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <SubcriptionC>
          <RegisterWrapper>
            <InnerWrapper>
              <img src={Phone} alt="Phone" />
              <Span>
                {Translation.registration.stage} 2 {' '}{Translation.registration.of} {' '} 3
              </Span>
              <H1Main>{Translation.registration.h1}</H1Main>
              <SubcriptionCInfo>
                {Translation.registration.subinfo}
              </SubcriptionCInfo>
            </InnerWrapper>
          </RegisterWrapper>
        </SubcriptionC>
        <ButtonWrapper>
          <ButtonMain large to="/register/email">
            {Translation.registration.buttons.create_account}
          </ButtonMain>
          <br />
          <ButtonMain2 large to={prevButtonUrl}>
            {Translation.registration.buttons.go_back}
          </ButtonMain2>
        </ButtonWrapper>

      </ReactCSSTransitionGroup>
    );

    // if user is in the b-flow, next step is also b-flow
    // const path = this.props.location.pathname;
    let nextStep = '/register/card';
    // if (path.substring(path.lastIndexOf('/') + 1) === 'b') {
    //   nextStep = '/register/card/b';
    // }

    let registerPageContent = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <RegisterWrapperForm>
          <InnerWrapperForm>
            {this.props.ActiveRegEmail ? (
              <div>
                <Heading>
                  <Span>
                    {Translation.registration.stage} <strong>2</strong>/<strong>3</strong>
                  </Span>
                  <H1>{Translation.registration.stage2.registered_user.heading}</H1>
                  <H2>{Translation.registration.stage2.registered_user.subheading}</H2>
                </Heading>
                <Center>
                  <H3Styled>{this.props.ActiveRegEmail}</H3Styled>
                  <Button large to={nextStep}>
                    {Translation.registration.buttons.continue}
                  </Button>
                </Center>
              </div>
            ) : (
                <div>
                  <RegUserInfo>
                    <Heading>
                      <Span>
                        {Translation.registration.stage} 2 {' '}{Translation.registration.of} {' '} 3
                    </Span>
                      <H1Main>{Translation.registration.stage2.new_user.heading}</H1Main>
                      <SeccondTitle>
                        {Translation.registration.stage2.new_user.subheading}
                      </SeccondTitle>
                    </Heading>
                  </RegUserInfo>

                  <RegUserFormRow>
                    <RegUserForm pathname={this.props.location.pathname} />
                  </RegUserFormRow>
                  <Clear />
                </div>
              )}
          </InnerWrapperForm>
          <SeccondWrapper />
        </RegisterWrapperForm>
      </ReactCSSTransitionGroup>
    );

    const pageContents =
      this.props.location.hash === '#steg-2' ? preRegisterPageContent : registerPageContent;

    return pageContents;
  }
}

function mapStateToProps(state) {
  return {
    ActiveRegEmail: state.signup.email,
  };
}

export default connect(mapStateToProps)(RegisterUser);
