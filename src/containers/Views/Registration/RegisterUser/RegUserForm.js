import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Base64 } from 'js-base64';

import { createUserRequest } from 'containers/Views/Registration/actions';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import { renderCheck } from 'components/Form/Checkbox';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import validate from './validate';
import Translation from 'translation/nextory-web-se';
import { getSubscriptionIdByName } from 'containers/App/common';

import {
  StyledLink,
  SubmitSC,
  FieldCustom,
  FieldCustomSection,
  FieldCustomCheckBox,
  EmailCustom,
  PreSubmit
} from './StyledRegisterUser';

class RegUserForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool,
    valid: PropTypes.bool,
    createUserRequest: PropTypes.func,
    SendingRequest: PropTypes.bool,
    LoggedIn: PropTypes.bool,
    ChoosenSub: PropTypes.string,
  };

  // take inputed values and pass them to our redux saga loginRequest
  submit = (values, createUserRequest) => {
    const email = values.email;
    const repeatemail = values.repeatemail;
    const password = Base64.encode(values.password);
    let subid = getSubscriptionIdByName(this.props.ChoosenSub);
    const data = {
      password,
      email,
      repeatemail,
      subid,
    };
    createUserRequest(data);
    //console.log(data)
  };

  render() {
    const {
      handleSubmit,
      createUserRequest,
      SendingRequest,
      // pathname
    } = this.props;

    // if user is in the b-flow, prev step is also b-flow
    // const path = pathname;
    // let prevStep = '/register/subscription';
    // if (path.substring(path.lastIndexOf('/') + 1) === 'b') {
    //   prevStep = '/register/subscription/b';
    // }

    const labelTerms = (
      <span>
        {Translation.forms.privaceTerm_a} <br />
        <StyledLink to="/medlemsvillkor" target="_blank">{Translation.forms.privaceTerm_b}</StyledLink> {Translation.forms.and}{' '}
        <StyledLink to="/integritetspolicy" target="_blank">{Translation.forms.privaceTerm_c}</StyledLink>
      </span>
    );
    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, createUserRequest))}>
        <FieldCustom>
          <Field
            name="email"
            type="email"
            component={renderField}
            label={Translation.forms.labels.email}
            placeholder={Translation.forms.labels.email_placeholder}
          />

          <EmailCustom>
            <Field
              name="repeatemail"
              type="email"
              component={renderField}
              // label={Translation.forms.labels.repeat_email}
              autoComplete="off"
              autoCorrect="off"
              spellCheck="off"
              placeholder={Translation.forms.labels.repeat_email}
            />
          </EmailCustom>

          <FieldCustomSection>
            <Field
              name="password"
              type="password"
              component={renderField}
              // label={Translation.forms.labels.password}
              label={Translation.forms.labels.password_label}
              placeholder={Translation.forms.labels.password}
            />
          </FieldCustomSection>

          <FieldCustomCheckBox>
            <Field name="acceptterms" component={renderCheck} type="checkbox" label={labelTerms} />
          </FieldCustomCheckBox>
          {this.props.submitFailed && this.props.ActiveReg ? (
            <PreSubmit to="/glomt-losenord">
              {Translation.registration.stage2.new_user.links.forgot_pass}
            </PreSubmit>
          ) : null}
          <Center>
            {SendingRequest ? (
              <SubmitSC large type="submit" disabled>
                <LoadingIndicatorSmall />
              </SubmitSC>
            ) : (
                <SubmitSC large type="submit">
                  {Translation.registration.buttons.continue}
                </SubmitSC>
              )}
            {/* <Button large framed to={prevStep}>
              {Translation.registration.buttons.go_back}
            </Button> */}
          </Center>

          {this.props.LoggedIn && <Redirect to={'/konto'} />}
        </FieldCustom>
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    ChoosenSub: state.signup.ChosenSubscription,
    SendingRequest: state.signup.currentlySending,
    LoggedIn: state.account.loggedIn,
  };
}

export default withRouter(
  connect(mapStateToProps, { createUserRequest })(
    reduxForm({
      form: 'registeruser',
      validate,
    })(RegUserForm)
  )
);
