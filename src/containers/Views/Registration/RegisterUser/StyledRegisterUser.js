import styled from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import H1 from 'components/Typography/H1';
import Button from 'components/Buttons';
import H3 from 'components/Typography/H3';
import A from 'components/Typography/A';
import { Link } from 'react-router-dom';

export const H1Main = styled(H1)`
font-size: 5.1rem;
max-width: 685px;
margin: 0 auto;
margin-bottom: 20px;
${mediaCustom.medium`
    font-size: 3.1rem;
    word-break: break-word;
`};
`;
export const ButtonMain = styled(Button)`
width: 25%;
height: 54px;
border-radius: 27px;
text-transform: uppercase;
font-size: 14px;
padding: 15px 0px;
font-weight: bold;
max-width:175px;
${mediaCustom.medium`
    width: 290px;
    max-width:290px;
`};
`;
export const ButtonMain2 = styled(Button)`
width: 25%;
height: 54px;
border-radius: 27px;
border: solid 2px #363bec;
text-transform: uppercase;
font-size: 14px;
background-color: #ffffff;
padding: 15px 0px;
font-weight: bold;
color:#363bec;
max-width:175px;
margin-top: 15px;
box-shadow: none;
&:hover{
  color:#ffff;
}
${mediaCustom.medium`
    width: 290px;
    max-width:290px;
`};
`;
export const RegisterWrapper = styled.section`
padding: 3rem 0 8rem;
background:${props => props.theme.colorMenuPink};
${media.medium`
  padding: 7rem 0 12rem;
  padding-bottom:0px;
`};
${mediaCustom.medium`
  padding-bottom: 0px;
  padding-top:0px;
`};
`;
export const InnerWrapper = styled.div`
padding: 0 1.5rem;
margin: auto;
max-width: 102.2rem;
text-align: center;
${mediaCustom.medium`
padding-left: 40px;
padding-right: 40px;
  img{
    width:110px;
  }
`};
`;
export const Heading = styled.div`
margin-bottom: 1.8rem;
${media.medium`
  margin-bottom: 2.5rem;
`};
`;
export const Span = styled.span`
display: block;
font-size: 1.4rem;
margin: 15px 0px;
${media.medium`
  font-size: 1.6rem;
  margin: 15px 0px;
`};
`;
export const H3Styled = H3.extend`
margin: 3rem 0 4rem 0;
${media.medium`
  margin: 4rem 0 5rem 0;
`};
`;
export const SubcriptionC = styled.div`
padding-top: 8rem;
background-color: #f6edec;
${media.medium`
  padding-top: 1rem;
`};
${mediaCustom.medium`
padding-top: 1rem;
padding-right: 0rem;
padding-left: 0rem;

`};
img {
  margin-bottom: 0.8rem;
}
`;

export const SubcriptionCInfo = styled.div`
font-size: 18px;
margin-bottom: 1.8rem;
max-width: 53.5rem;
margin: 0 auto;
color: #555555;
${mediaCustom.medium`
  font-size:16px;
`};
`;
export const ButtonWrapper = styled.section`
background:${props => props.theme.colorMenuPink};
-webkit-text-align: center;
text-align: center;
min-height: 200px;
padding-top: 2em;
${mediaCustom.medium`
  padding-top: 10px;
  padding-bottom: 3em;
  min-height: 145px;
`};
`;

/// register/email

export const RegUserInfo = styled.div`
float:left;
width:50%;
padding: 30px 0px;
h1{
  max-width: 397px;
  ${mediaCustom.medium`
    font-size: 27px;
  `};
}
${mediaCustom.medium`
  float:none;
  width:100%;
  padding-top:0px;
`};
`;

export const RegUserFormRow = styled.div`
float:right;
width:50%;
form{
  background: ${props => props.theme.summerWhiteLight};
  padding: 40px;
  max-width: 382px;
  border-radius: 6px;
  box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.08), 0 12px 24px 2px rgba(234, 243, 245, 0.25);
  ${mediaCustom.medium`
    position: absolute;
    width: 100%;
    margin: auto;
    left: 0;
    top: 250px;
    right: 0;
    padding: 20px !important;
  `};
  ${mediaCustom.xsmall`
    width:94%;
    top: 260px;
  `};
}
${mediaCustom.medium`
  float:none;
  width:100%;
`};
`;
export const InnerWrapperForm = styled.div`
padding: 0 1.5rem;
margin: auto;
max-width: 102.2rem;
text-align: center;
${mediaCustom.medium`
padding-left: 40px;
padding-right: 40px;
`};
`;

export const Clear = styled.div`
clear:both !important;
`;
export const SeccondTitle = styled.p`
font-size: 18px;
font-weight: normal;
font-style: normal;
font-stretch: normal;
line-height: 1.33;
letter-spacing: normal;
text-align: center;
color:${props => props.theme.seccondGrey};
${mediaCustom.medium`
  font-size:16px;
`};
`;

export const SeccondWrapper = styled.div`
min-height:200px;
${mediaCustom.medium`
min-height: 460px;
background: ${props => props.theme.summerWhiteLight};
`};
`;
export const RegisterWrapperForm = styled.section`
padding: 3rem 0 8rem;
background:${props => props.theme.colorMenuPink};
${media.medium`
  padding: 7rem 0 12rem;
  padding-bottom:0px;
`};
${mediaCustom.medium`
  padding-bottom: 0px;
  padding-top: 5px;
`};
`;

//RegUserForm Styles
export const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 1.5rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
`;

export const StyledLink = A.withComponent(Link);

export const PreSubmit = StyledLink.extend`
  font-size: 1.2rem;
  margin: 1rem 0 1.5rem 0;
  text-align: center;
  display: block;
  ${media.medium`
    font-size: 1.4rem;
  `};
`;
export const SubmitSC = Button.withComponent(Submit);

export const FieldCustom = styled.div`
  input{
    border-radius: 27px;
    margin-top: 5px;
  }
  label{
    font-size: 13px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.36;
    letter-spacing: normal;
    color: #585858;
  }
  button{
    border-radius: 27px;
    background-color: ${props => props.theme.buttonSecconBlue};
  }
`;

export const FieldCustomSection = styled.div`
    margin-top: 15px;
`;
export const FieldCustomCheckBox = styled.div`
  div{
    left:0px;
    margin-bottom: 5px !important;
    ${mediaCustom.medium`
        left: -9% !important;
        margin: 0 auto !important;
        width: 230px !important
    `};
    ${mediaCustom.xsmall`
        left:-10px !important;
    `};
    ${mediaCustom.usmall`
        left: 12px !important;
    `};
  }
  label span{
    font-weight: 400;
    font-size: 13px;
  }
  small{
    background: #fff;
    border-radius: 6px;
    border: solid 1px rgba(68, 68, 68, 0.5);
  }
  span{
    top: -17px;
    font-size: 12px;
    position: initial !important;
  }
    img{
      position: initial !important;
    }

`;
export const EmailCustom = styled.div`
  img{
    top: 18px;
  }
`;