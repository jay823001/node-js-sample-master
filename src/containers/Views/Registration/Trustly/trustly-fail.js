import React from 'react';
import PropTypes from 'prop-types';
import Loading from 'components/LoadingIndicator/page';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import { Base64 } from 'js-base64';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import { withNamespaces } from 'react-i18next';

class TrustlyFail extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
  }
  state = {
    redirect: false,
    orderid: '',
    customerid: '',
    neworderid: '',
  }
  componentDidMount() {
    const decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));
    if (decodedTrustly.data.route === this.props.t('registration.trialFlow.checkout') || decodedTrustly.data.route === this.props.t('registration.campaignFlow.checkout') ||
      decodedTrustly.data.route === this.props.t('mypages.reActivationFlow.checkout')) {
      return this.setState({ redirect: true });
    } else {
      this.setID();
    }
  }

  setID = () => {
    const customerid = this.props.location.pathname.split('/');
    this.setState({
      orderid: customerid[4],
      customerid: customerid[3],
    }, () => {
      this.trustlyPaymentStatus();
    });
  }

  trustlyPaymentStatus = async () => {
    try {
      const response = await fetch(`${apiUrl}trustlypaymentstatus?orderno=${this.state.orderid}&customerid=${this.state.customerid}&status=FAILED${internationalization_b}`, {
        method: 'POST',
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data) {
        this.setState({
          neworderid: data.data.orderid
        }, () => {
          this.retryPayment();
        });
      }
    } catch (error) {
      return error;
    }
  }

  retryPayment = async (newdata) => {
    const decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));
    try {
      const response = await fetch(`${apiUrl}order/recreate?paymenttype=${decodedTrustly.data.paymenttype}&subscriptionid=${decodedTrustly.data.subscriptionid}&orderid=${this.state.neworderid}${internationalization_b}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'nx-at': decodedTrustly.data.authkey,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(newdata),
      });
      const neworderid = await response.json();
      if (neworderid) {
        new Cookies().set('retry', neworderid.data.orderid, { path: '/', maxAge: 1209600 });
        return this.setState({ redirect: true });
      }
    } catch (error) {
      return error;
    }
  };

  changeLocation(url) {
    if (url === this.props.t('registration.trialFlow.checkoutOptions') || url === this.props.t('registration.campaignFlow.checkoutOptions') ||
      url === this.props.t('mypages.reActivationFlow.checkoutOptions')) {
      window.location.href = url;
    } else {
      return <Redirect to={url} />
    }
  }

  render() {
    let redirect = null;
    let decodedTrustly = '';

    decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));
    if (decodedTrustly) {
      if (decodedTrustly.data.route === this.props.t('registration.trialFlow.checkout')) {
        redirect = this.props.t('registration.trialFlow.checkoutOptions');
      } else if (decodedTrustly.data.route === this.props.t('registration.campaignFlow.checkout')) {
        redirect = this.props.t('registration.campaignFlow.checkoutOptions');
      } else if (decodedTrustly.data.route === this.props.t('mypages.reActivationFlow.checkout')) {
        redirect = this.props.t('mypages.reActivationFlow.checkoutOptions');
      } else {
        if (decodedTrustly.data.route === '/register/card') {
          redirect = '/register/card?trustlyfail';
        } else if (decodedTrustly.data.route === this.props.t('registration.trialFlow.checkout')) { // NEWUI
          redirect = '/register/card?trustlyfail';
        } else if (decodedTrustly.data.route === '/konto/andra-betalning') {
          redirect = '/konto/andra-betalning?trustlyfail';
        } else if (decodedTrustly.data.route === '/register-campaign/card') {
          redirect = '/register-campaign/card?trustlyfail';
        } else if (decodedTrustly.data.route === '/konto/aktivera-betalning') {
          redirect = '/konto/aktivera-betalning?trustlyfail';
        } else if (decodedTrustly.data.route === '/presentkort/card') {
          redirect = '/presentkort/card?trustlyfail';
        }
      }
    } else {
      if (new Cookies().get('trustly')) {
        decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));

        if (decodedTrustly.data.route === '/register/card') {
          redirect = '/register/card?trustlyfail';
        } else if (decodedTrustly.data.route === this.props.t('registration.trialFlow.checkout')) { // NEWUI
          redirect = '/register/card?trustlyfail';
        } else if (decodedTrustly.data.route === '/konto/andra-betalning') {
          redirect = '/konto/andra-betalning?trustlyfail';
        } else if (decodedTrustly.data.route === '/register-campaign/card') {
          redirect = '/register-campaign/card?trustlyfail';
        } else if (decodedTrustly.data.route === '/konto/aktivera-betalning') {
          redirect = '/konto/aktivera-betalning?trustlyfail';
        } else if (decodedTrustly.data.route === '/presentkort/card') {
          redirect = '/presentkort/card?trustlyfail';
        }
      }
    }

    return (
      <div>
        <Loading title={'Avbryter din betalning'} />
        {(this.state.redirect && decodedTrustly) &&
          // <Redirect to={redirect} />
          this.changeLocation(redirect)
        }
      </div>
    );
  }
}

export default withNamespaces(['lang_route'])(TrustlyFail);
