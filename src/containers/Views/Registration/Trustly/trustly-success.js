import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import Loading from 'components/LoadingIndicator/page';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import Cookies from 'universal-cookie';
import { dispatchTrustlySuccess } from './actions';
import { Base64 } from 'js-base64';
import { withNamespaces } from 'react-i18next';

class TrustlySuccess extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
    dispatchTrustlySuccess: PropTypes.func,
  };
  state = {
    redirect: false,
    orderid: '',
    customerid: '',
  };

  componentDidMount() {
    if (new Cookies().get('trustly')) {
      return this.setID();
    }
    return this.redirect();
  }

  setID = () => {
    const customerid = this.props.location.pathname.split('/');

    this.setState(
      {
        orderid: customerid[4],
        customerid: customerid[3],
      },
      () => {
        this.trustlyPaymentStatus();
      }
    );
  };

  redirect = () => {
    this.setState({ redirect: true });
  };

  trustlyPaymentStatus = async () => {
    try {
      const response = await fetch(
        `${apiUrl}trustlypaymentstatus?orderno=${this.state.orderid}&customerid=${
        this.state.customerid
        }&status=SUCCESS${internationalization_b}`,
        {
          method: 'POST',
          headers: {
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
        }
      );
      const data = await response.json();

      if (data.status === 200) {
        //let decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));
        // if (decodedTrustly) {
        //   if (decodedTrustly.data.route === '/register/card/b') {
        //     // redirect = `/transresp/b?orderid=${this.state.orderid}`;
        //     this.setState({ redirect: true });
        //   } else {
        //     return this.props.dispatchTrustlySuccess(data.data.orderid);
        //   }
        // } else {
        return this.props.dispatchTrustlySuccess(data.data.orderid);
        // }

      } else if (data.status !== 200) {
        this.setState({ redirect: true });
      }

      return console.log('hej pårej');
    } catch (error) {
      return error;
    }
  };

  render() {
    let redirect = '/';
    let decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));
    if (decodedTrustly) {
      if (decodedTrustly.data.route === this.props.t('registration.trialFlow.checkout')) {
        redirect = `${this.props.t('mypages.userDetails')}?orderid=${this.state.orderid}`;
      } else if (decodedTrustly.data.route === this.props.t('registration.campaignFlow.checkout')) {
        redirect = `${this.props.t('mypages.userDetails')}?orderid=${this.state.orderid}&campaignname=${decodedTrustly.data.campaign.name}`;
      } else if (decodedTrustly.data.route === this.props.t('mypages.reActivationFlow.checkout')) {
        redirect = `${this.props.t('mypages.getStarted')}`
      }
    }

    return (
      <div>
        <Loading title={'Behandlar din betalning'} />
        {this.state.redirect && <Redirect to={redirect} />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
  };
}

export default withNamespaces(['lang_route'])(connect(mapStateToProps, { dispatchTrustlySuccess })(TrustlySuccess));
