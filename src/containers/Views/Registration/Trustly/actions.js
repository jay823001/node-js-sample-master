import { TRUSTLY_SUCCESS } from './constants';

export const dispatchTrustlySuccess = (data) => ({ type: TRUSTLY_SUCCESS, data });
