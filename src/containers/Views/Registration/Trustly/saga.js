import { call, put, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { push } from 'react-router-redux';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { fetchLogin, fetchUserDetails } from 'containers/App/api';
import { SET_AUTH } from 'containers/Views/Account/LoginPage/constants';
import { REFRESH_STORE } from 'containers/Views/Account/MyAccount/constants';
import { CREATEUSER_CLEAR_ERROR, CREATEUSER_SETAUTH } from '../constants';
import { TRUSTLY_SUCCESS } from './constants';
import i18next from 'newui/i18n';
import { REGISTRATION_SUCCESS, UPDATE_USER_DETAILS_STORE } from 'newui/domain/Modules/User/actionTypes'

const cookies = new Cookies();

/*
* Login user if payment success with Trustly
*/
function* loginTrustly(orderid) {
  const decodedTrustly = JSON.parse(Base64.decode(cookies.get('trustly')));
  if (decodedTrustly.data.paymenttype === 'CARD_UPDATE') {
    // refresh store
    yield put({ type: REFRESH_STORE, authkey: decodedTrustly.data.authkey });

    // redirect to success page
    yield put(push('/konto?updated-card'));
    cookies.remove('trustly', { path: '/' });
  } else if (decodedTrustly.data.paymenttype === 'PURCHASE_GIFTCARD') {
    // refresh store
    if (cookies.get('user')) {
      yield put({ type: REFRESH_STORE, authkey: decodedTrustly.data.authkey });
    }
    // redirect to success page
    yield put(push(`/lospresentkort-purchase-success?${orderid.data}`));
    yield put({ type: CREATEUSER_CLEAR_ERROR });
    cookies.remove('gift', { path: '/' });
    cookies.remove('trustly', { path: '/' });
  } else if (decodedTrustly.data.paymenttype === 'PURCHASE_SUBSCRIPTION') {

    // refresh store
    yield put({ type: REFRESH_STORE, authkey: decodedTrustly.data.authkey });

    // redirect to success page
    if (decodedTrustly.data.route === i18next.t('lang_route:mypages.reActivationFlow.checkout')) {
      yield delay(3000)
      // yield put(push(`${i18next.t('lang_route:mypages.userDetails')}?reactivated&orderid=${orderid.data}`));
      yield put(push(`${i18next.t('lang_route:mypages.getStarted')}`))
      window.location.reload() // this is because new css should load when redirecting from 3rd party url
    } else {
      yield put(push(`/transresp?reactivated&orderid=${orderid.data}`));
    }

    cookies.remove('trustly', { path: '/' });
  } else if (decodedTrustly.data.paymenttype === 'REDEEM_CAMPAIGN') {
    const decoded = JSON.parse(Base64.decode(cookies.get('reg')));

    // api call for login
    const logindata = {
      data: {
        email: decoded.email,
        password: decoded.password,
      },
    };
    const userdata = yield call(fetchLogin, logindata);

    // user DETAILS fetch
    const UserDetails = yield call(fetchUserDetails, userdata.authkey);
    // set a cookie token to preserve logged in user on page refresh
    userdata.data.email = decoded.email;
    userdata.data.p = decoded.password;
    userdata.data.authkey = userdata.authkey;
    userdata.data.refreshkey = userdata.refreshkey;
    userdata.data.UserDetails = UserDetails;
    const userEncode = Base64.encode(JSON.stringify(userdata.data));
    cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

    // ==============set new flow authkeys ================//
    const registrationData = {
      authkey: userdata.authkey,
      refreshkey: userdata.refreshkey,
    }


    yield put({ type: REGISTRATION_SUCCESS, registrationData })
    yield put({ type: UPDATE_USER_DETAILS_STORE })
    //=====================================================//

    // set auth to true
    yield put({ type: SET_AUTH, newAuthState: true });

    // logout registration user
    cookies.remove('reg', { path: '/' });

    yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });

    // clear errors and stop sending
    yield put({ type: CREATEUSER_CLEAR_ERROR });

    if (decodedTrustly.data.route === i18next.t('lang_route:registration.campaignFlow.checkout')) {
      yield put(push(`${i18next.t('lang_route:mypages.userDetails')}?reactivated&orderid=${orderid.data}&campaignname=${decodedTrustly.data.campaign.name}`
      ));
      window.location.reload() // this is because new css should load when redirecting from 3rd party url
    } else {
      // redirect to success page
      if (decoded.newuser) {
        yield put(
          push(`/transresp?orderid=${orderid.data}&campaignname=${decodedTrustly.data.campaigncode}`)
        );
      } else {
        yield put(
          push(
            `/transresp?reactivated&orderid=${orderid.data}&campaignname=${
            decodedTrustly.data.campaigncode
            }`
          )
        );
      }
    }
    cookies.remove('trustly', { path: '/' });


  } else {
    // TRIAL
    // login active user via cookie data
    let decoded = JSON.parse(Base64.decode(cookies.get('reg')));
    let ps = decoded.password

    let decodedTrustly = JSON.parse(Base64.decode(new Cookies().get('trustly')));
    if (decodedTrustly) {
      if (decodedTrustly.data.route === i18next.t('lang_route:registration.trialFlow.checkout') || decodedTrustly.data.route === i18next.t('lang_route:registration.campaignFlow.checkout')) {
        const usercase = JSON.parse(Base64.decode(cookies.get('visitor')));
        decoded = usercase.data;
        ps = decoded.base64Password
      }
    }

    // api call for login
    const logindata = {
      data: {
        email: decoded.email,
        password: ps,
      },
    };

    //call /login
    const userdata = yield call(fetchLogin, logindata);

    // call /userdetails
    const UserDetails = yield call(fetchUserDetails, userdata.authkey);

    // set userdetails as a cookie "user"
    userdata.data.email = decoded.email;
    userdata.data.p = decoded.password;
    userdata.data.authkey = userdata.authkey;
    userdata.data.refreshkey = userdata.refreshkey;
    userdata.data.UserDetails = UserDetails;
    const userEncode = Base64.encode(JSON.stringify(userdata.data));
    cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

    // ==============set new flow authkeys ================//
    const registrationData = {
      authkey: userdata.authkey,
      refreshkey: userdata.refreshkey,
    }
    yield put({ type: REGISTRATION_SUCCESS, registrationData })
    yield put({ type: UPDATE_USER_DETAILS_STORE })
    //=====================================================//

    // set auth to true
    yield put({ type: SET_AUTH, newAuthState: true });

    // logout registration user
    cookies.remove('reg', { path: '/' });

    yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });

    // clear errors and stop sending
    yield put({ type: CREATEUSER_CLEAR_ERROR });

    let decodedTrustly2 = JSON.parse(Base64.decode(new Cookies().get('trustly')));
    if (decodedTrustly2) {
      if (decodedTrustly2.data.route === i18next.t('lang_route:registration.trialFlow.checkout')) {
        cookies.set('member', cookies.get('visitor'), { path: '/', maxAge: 12096000 })
        yield put(push(`${i18next.t('lang_route:mypages.userDetails')}?orderid=${orderid.data}`));
        window.location.reload() // this is because new css should load when redirecting from 3rd party url
      } else if (decodedTrustly2.data.route === i18next.t('lang_route:registration.campaignFlow.checkout')) {
        yield put(push(`${i18next.t('lang_route:mypages.userDetails')}?orderid==${orderid.data}&campaignname=${decodedTrustly2.data.campaign.name}`))
        window.location.reload() // this is because new css should load when redirecting from 3rd party url
      } else {
        yield put(push(`/transresp?orderid=${orderid.data}`));

      }
    } else {
      // redirect to success page
      yield put(push(`/transresp?orderid=${orderid.data}`));
    }
    cookies.remove('trustly', { path: '/' });

  }
}

// Register Trustly success saga
export function* loginTrustlySaga() {
  yield takeLatest(TRUSTLY_SUCCESS, loginTrustly);
}
