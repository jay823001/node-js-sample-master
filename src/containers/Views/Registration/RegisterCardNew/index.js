import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import DataLayer from 'containers/App/datalayer';
import Cookies from 'universal-cookie';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { ToastContainer, toast, style } from 'react-toastify';
import { scroller } from 'react-scroll';

import { connect } from 'react-redux';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  Payment,
  SafePayment,
  FoldTrigger,
  Cards,
  Mobile,
  Foldout,
  Span
} from './StyledPayment';
import H1 from 'components/Typography/H1';
import UL from 'components/Typography/UL';
import Lock from './IconLock';
import Visa from './visa.png';
import Master from './master.png';
import BankID from './bankid.svg';
import Trustly from './trustly.png';
import RegCardForm from './RegCardForm';
import RegTrustly from './RegTrustly';
import Translation from 'translation/nextory-web-se';


class RegisterCardPage extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
    RouteSearch: PropTypes.string,
  };

  state = {
    card: false,
    bankid: false,
  };

  componentDidMount() {
    if (this.props.RouteSearch.includes('trustlyfail')) {
      this.notifyTrustlyFail();
    }
  }

  notifyTrustlyFail = () => {
    toast.success(Translation.registration.stage3.messages.trustyfail_success, {
      position: toast.POSITION.BOTTOM_CENTER,
      style: style({
        colorSuccess: '#ff3a54',
        width: '380px',
      }),
    });
  };

  scrollTo() {
    if (window.innerWidth < 768) {
      scroller.scrollTo('scroll-to-payment', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart',
        offset: -20,
      });
    }
  };

  handleCardClick = () => {
    this.setState({ card: !this.state.card, bankid: false });
    this.scrollTo();
  };

  handleBankidClick = () => {
    this.setState({ bankid: !this.state.bankid, card: false });
    this.scrollTo();
  };

  render() {
    const twoWeeks = new Date(new Date().setDate(new Date().getDate() + 13));
    const date = twoWeeks.toISOString().substring(0, 10);

    let regActive = false;
    if (new Cookies().get('reg')) {
      regActive = true;
    }

    const bulletPointforC =
      this.props.location.pathname === '/register/card/c' ? (
        <li>Vi skickar en påminnelse via e-post 3 dagar innan.</li>
      ) : null;

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <Span>
                {Translation.registration.stage} <strong>3</strong> {Translation.registration.of}{' '}
                <strong>3</strong>
              </Span>
              <H1>{Translation.registration.stage3.heading}</H1>
              <UL>
                <li>
                  {Translation.registration.stage3.bullets.point1_p1} {date}{' '}
                  {Translation.registration.stage3.bullets.point1_p2}
                </li>
                {bulletPointforC}
                <li>{Translation.registration.stage3.bullets.point2}</li>
              </UL>
            </Heading>

            <Payment name="scroll-to-payment">
              <SafePayment>
                {Translation.registration.stage3.secure_server} <Lock />
              </SafePayment>
              <FoldTrigger active={this.state.card} onClick={this.handleCardClick}>
                {Translation.registration.stage3.credit_or_debit_card}
                <Cards visa alt="Visa" src={Visa} />
                <Cards master alt="Mastercard" src={Master} />
              </FoldTrigger>
              <Foldout active={this.state.card}>
                <article>
                  <RegCardForm />
                </article>
              </Foldout>
              <FoldTrigger active={this.state.bankid} onClick={this.handleBankidClick}>
                {Translation.registration.stage3.mobile_bankid}
                <Mobile bankid alt="BankId" src={BankID} />
                <Mobile trustly alt="Trustly" src={Trustly} />
              </FoldTrigger>
              <Foldout active={this.state.bankid}>
                <RegTrustly />
              </Foldout>
            </Payment>
          </InnerWrapper>
        </RegisterWrapper>

        {this.props.LoggedIn && !regActive && <Redirect to={'/konto'} />}

        <ToastContainer autoClose={5000} />
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    RouteSearch: state.route.location.search,
  };
}

export default connect(mapStateToProps)(RegisterCardPage);
