import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import H3 from 'components/Typography/H3';
import Img from 'components/Img';
import Caret from 'containers/Views/Registration/RegisterCard/images/caret.svg';
import Button from 'components/Buttons';

export const RegisterWrapper = styled.section`
  padding: 3rem 0 8rem;
  ${media.medium`
    padding: 7rem 0 12rem;
  `}
`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 57.5rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
    margin-bottom: 2.5rem;
  `}
`;


export const Payment = styled.div`
  position: relative;
`;

export const SafePayment = styled.span`
  position: relative;
  text-align: right;
  display: block;
  top: 0;
  font-size: 1.2rem;
  color: ${props => props.theme.colorDarkGrey};
  vertical-align: bottom;
  ${media.medium`
    font-size: 1.4rem;
  `}
`;

export const FoldTrigger = styled(H3)`
  font-size: 1.6rem;
  font-weight: 400;
  display: block;
  padding: 1.5rem 0 1.5rem 1.0rem;
  text-decoration: none;
  color: ${props => props.theme.colorBlack};
  border: .1rem solid ${props => props.theme.colorGrey};
  border-radius: .5rem;
  position: relative;
  background-color: white;
  cursor:pointer;
  z-index:5;
  ${media.medium`
    font-size: 1.8rem;
  `}
  &:before {
    position: relative;
    top: .2rem;
    display: inline-block;
    width: 1.4rem;
    height: 1.4rem;
    margin-right: .7rem;
    content: '';
    transition: all .15s ease;
    background-image: url(${Caret});
    background-repeat: no-repeat;
    background-position: center;
    background-size: 1.4rem 1.4rem;
  }
  ${props => props.active && css`
    border-radius: .5rem .5rem 0 0;
    &:before {
      transform: rotate(90deg);
    }
  `}
`;


export const Cards = styled(Img)`
  width: 2.8rem;
  display: inline-block;
  height: auto;
  position: absolute;
  top: 1.4rem;
  border: .1rem solid ${props => props.theme.colorWhite};
  ${media.medium`
    width: 3.6rem;
    top: 1.3rem;
  `}
  ${props => props.visa && css`
    margin-left: 1.5rem;
  `}
  ${props => props.master && css`
    margin-left: 4.8rem;
    ${media.medium`
      margin-left: 5.8rem;
    `}
  `}
`;

export const Mobile = styled(Img)`
  display: inline-block;
  height: auto;
  position: absolute;
  ${props => props.bankid && css`
    width: 2.0rem;
    height: 1.8rem;
    top: 1.3rem;
    margin-left: 1.3rem;
    ${media.medium`
      width: 2.8rem;
      height: 2.6rem;
      top: 1.0rem;
    `}
  `}
  ${props => props.trustly && css`
    width: 7.0rem;
    top: 1.0rem;
    margin-left: 4.2rem;
    ${media.medium`
      width: 8.5rem;
      top: .8rem;
      margin-left: 4.9rem;
    `}
  `}
`;

export const Foldout = styled.div`
  background: white;
  border-radius: 0 0 .5rem .5rem;
  border: .1rem solid ${props => props.theme.colorGrey};
  border-top: none;
  overflow-y: hidden;
  max-height: 0;
  transition: all .15s ease;
  position: relative;
  top: -1.3rem;
  z-index:3;
  ${props => props.active && css`
    max-height:100rem;
    transition: all .15s ease;
  `}
  article {
    padding: 2.0rem;
    ${media.medium`
      padding: 2.5rem;
    `}
  }
  a,button {
    margin-top: 1.5rem;
  }
`;

export const Span = styled.span`
  display: block;
  font-size: 1.4rem;
  margin: 0 0 0.8rem;
  ${media.medium`
    font-size: 1.6rem;
    margin: 0 0 1.3rem;
  `};
`;

//RegCardFrom Styles
export const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 0rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.6;
      &:hover {
        background-color: ${props => props.theme.colorBlue};
      }
    `};
`;
export const SubmitSC = Button.withComponent(Submit);
export const ThreeColumn = styled.div`
  position: relative;
  ${media.large`
    display: inline-block;
    float: left;
    width: 30%;
    margin-right: 5%;
  `} ${props =>
    props.last &&
    css`
      margin-right: 0;
    `};
`;
export const ImgCvvQ = styled.img`
  display: none;
  ${media.medium`
    display: inline-block;
    width: 1.8rem;
    height: 1.8rem;
    position: absolute;
    top: 3.4rem;
    right: 1.0rem;
  `};
`;
export const Tooltip = styled.aside`
  position: absolute;
  background: white;
  padding: 0.7rem 0.5rem 0.6rem;
  box-shadow: 0.1rem 0.1rem 1.3rem rgba(0, 0, 0, 0.25);
  border-radius: 0.7rem;
  z-index: 666;
  bottom: 6.9rem;
  left: -2.1rem;
  display: block;
  img {
    max-width: 15rem;
  }
  &:after {
    content: '';
    position: absolute;
    width: 0;
    height: 0;
    bottom: -17px;
    right: 22px;
    box-sizing: border-box;
    border: 9px solid #b1b0b0;
    border-color: transparent transparent #fff #fff;
    transform-origin: 0 0;
    transform: rotate(-45deg);
    box-shadow: -2px 2px 5px 0 rgba(0, 0, 0, 0.16);
  }
`;
export const PreSubmit = styled.span`
  font-size: 1.2rem;
  margin: 3rem 0 0 0;
  display: block;
  color: ${props => props.theme.colorRed};
  ${media.medium`
    font-size: 1.4rem;
  `} ul {
    padding: 0 0 1rem 1.5rem;
    list-style: disc;
  }
`;

export const RegCardFormInfo = styled.div`
  padding: 0px 3px 0px 2px;
  padding-bottom: 10px;
`;
export const RegCardFormInfoTitle = styled.div`
  font-weight: bold;
  font-size: 14px;
  color:#737373;
`;
export const RegCardFormInfoDetails = styled.span`
  width: 250px;
  display: inline-block;
  float: left;
  font-size: 13px;
  max-width: 285px;
  color: #737373;

  ${media.medium`
    font-size: 13px;
    width: 285px;
  `}
  @media (max-width:400px){
    width:75%;
  }
`;
export const RegCardFormInfoLink = styled.a`
  float: right;
  color:#737373;
  margin-top: 0px !important;
  left: 0;
  position: relative;
  font-weight: bold;
  font-size: 14px;

  &:hover{
    text-decoration: underline;
  }
`;
export const Clear = styled.div`
  clear:both !important;
`;
export const RegCardFormInfoPara = styled.p`
  font-size: 12px;
  color:#737373;
`;