import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

const StyledSvg = styled.svg`
  width: 2.5rem;
  height: 2.5rem;
  position: relative;
  top: -0.4rem;
    path {
      fill: #FFA500;
    }
  ${media.medium`
    width: 3.0rem;
    top: -0.6rem;
    margin-left: 0.5rem;
    height: 3.0rem;
  `}
`;

export default () => (
  <StyledSvg viewBox="0 0 1792 1792">
    <path d="M640 768h512V576q0-106-75-181t-181-75-181 75-75 181v192zm832 96v576q0 40-28 68t-68 28H416q-40 0-68-28t-28-68V864q0-40 28-68t68-28h32V576q0-184 132-316t316-132 316 132 132 316v192h32q40 0 68 28t28 68z" />
  </StyledSvg>
);
