function CVV(value) {
  if (!value) {
    return value;
  }
  const onlyNums = value.replace(/[^\d]/g, '');

  if (onlyNums.length <= 4) {
    return onlyNums;
  }
}

export default CVV;
