import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { regPaymentRequest } from 'containers/Views/Registration/actions';
import P from 'components/Typography/P';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import Center from 'components/Center';
import Button from 'components/Buttons';
import Cookies from 'universal-cookie';
import Translation from 'translation/nextory-web-se';
import { getSubscriptionIdByName } from '../../../App/common';
import styled from 'styled-components';
import mediaCustom from 'theme/styled-utils-custom';

const StyledLink = Button.withComponent('button');

const Submit = StyledLink.extend`
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;
const Wrapper = styled.div`
  button{
    border-radius: 27px;
    text-transform: uppercase;
    font-size: 16px;
    font-weight: 600;
      ${mediaCustom.medium`
      font-size: 12px !important;
      `};
  }

`;

class RegTrustly extends React.PureComponent {
  static propTypes = {
    activateUser: PropTypes.object,
    ChoosenSub: PropTypes.string,
    Route: PropTypes.string,
    orderidUpdatePayment: PropTypes.string,
    Campaign: PropTypes.string,
    UserId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    Authkey: PropTypes.string,
    Payment: PropTypes.string,
    regPaymentRequest: PropTypes.func,
    LoggedInData: PropTypes.object,
    giftCardAuthkey: PropTypes.string,
    RouteSearch: PropTypes.string,
    daOrderid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    daAuthkey: PropTypes.string,
    orderid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    authkey: PropTypes.string,
    giftCardOrderid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  };

  state = {
    loading: false,
    orderid: '',
  };

  handleSubmit = () => {
    const route = this.props.Route;
    const cookies = new Cookies();

    /* trial trustly registration */
    if (this.props.Route === '/register/card') {
      let subscriptionid = getSubscriptionIdByName(this.props.ChoosenSub)
      const orderid = '';
      const paymentgateway = 'TRUSTLY';
      const campaignname = this.props.Campaign;
      const customerid = this.props.UserId;
      const authkey = this.props.Authkey;
      const paymenttype = this.props.Payment;

      const data = {
        route,
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        orderid,
        campaignname,
        customerid,
      };

      this.setState({ loading: true });
      this.props.regPaymentRequest(data);

      /* update trustly details for logged in user */
    } else if (this.props.Route === '/konto/andra-betalning') {
      let orderid = this.props.orderidUpdatePayment;
      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      const authkey = this.props.LoggedInData.authkey;
      const subscriptionid = this.props.LoggedInData.subscriptionid;
      const paymenttype = 'CARD_UPDATE';
      const paymentgateway = 'TRUSTLY';

      const data = {
        route,
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        orderid,
      };
      if (data.orderid) {
        this.setState({ loading: true });
        this.props.regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }

      /* trustly for creating campaign user */
    } else if (this.props.Route === '/register-campaign/card') {
      let orderid = '';
      let authkey = '';

      if (this.props.RouteSearch === '?da') {
        orderid = this.props.daOrderid;
        authkey = this.props.daAuthkey;
      } else {
        orderid = this.props.orderid;
        authkey = this.props.authkey;
      }
      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      let subscriptionid = getSubscriptionIdByName(this.props.ChoosenSub)
      const paymentgateway = 'TRUSTLY';
      const paymenttype = 'REDEEM_CAMPAIGN';
      const campaigncode = this.props.Campaign;

      const data = {
        route,
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        orderid,
        campaigncode,
      };
      if (data.orderid) {
        this.setState({ loading: true });
        this.props.regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }

      /* reactivate trustly details nonmembers */
    } else if (this.props.Route === '/konto/aktivera-betalning') {
      let subscriptionid = getSubscriptionIdByName(this.props.ChoosenSub)
      let orderid = this.props.activateUser.orderid;
      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }

      const authkey = this.props.LoggedInData.authkey;
      const paymentgateway = 'TRUSTLY';
      const paymenttype = 'PURCHASE_SUBSCRIPTION';

      const data = {
        route,
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        orderid,
      };
      if (data.orderid) {
        this.setState({ loading: true });
        this.props.regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }

      /* buy a new giftcard */
    } else if (this.props.Route === '/presentkort/card') {
      const subscriptionid = 6;
      let orderid = this.props.giftCardOrderid;
      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      const authkey = this.props.giftCardAuthkey;
      const paymentgateway = 'TRUSTLY';
      const paymenttype = 'PURCHASE_GIFTCARD';

      const data = {
        route,
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        orderid,
      };
      if (data.orderid) {
        this.setState({ loading: true });
        this.props.regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }
    }
  };

  render() {
    const { Route } = this.props;

    let copy = Translation.registration.stage3.regtrustly.mobile_bankid_option.initial_text;
    if (Route === '/konto/andra-betalning') {
      copy = Translation.registration.stage3.regtrustly.mobile_bankid_option.second_to_payment;
    } else if (Route === '/konto/aktivera-betalning') {
      copy = Translation.registration.stage3.regtrustly.mobile_bankid_option.enable_payment;
    } else if (Route === '/presentkort/card') {
      copy = Translation.registration.stage3.regtrustly.mobile_bankid_option.register_giftcard;
    } else if (Route === '/register-campaign/card') {
      copy = Translation.registration.stage3.regtrustly.mobile_bankid_option.register_campaign;
    }

    let button = null;

    if (this.state.loading && Route !== '/konto/andra-betalning') {
      button = (
        <Submit onClick={this.handleSubmit} large disabled>
          <LoadingIndicatorSmall />
        </Submit>
      );
    } else if (!this.state.loading && Route !== '/konto/andra-betalning') {
      button = (
        <Submit onClick={this.handleSubmit} large>
          {Translation.registration.stage3.regtrustly.buttons.use_mobile_bankid}
        </Submit>
      );
    } else if (this.state.loading && Route === '/konto/andra-betalning') {
      button = (
        <Submit onClick={this.handleSubmit} large disabled>
          <LoadingIndicatorSmall />
        </Submit>
      );
    } else if (!this.state.loading && Route === '/konto/andra-betalning') {
      button = (
        <Submit onClick={this.handleSubmit} large>
          {Translation.registration.stage3.regtrustly.buttons.use_mobile_bankid}
        </Submit>
      );
    } else if (!this.state.loading && Route === '/konto/andra-betalning' && !this.state.orderid) {
      button = (
        <Submit onClick={this.handleSubmit} large disabled>
          {Translation.registration.stage3.regtrustly.buttons.use_mobile_bankid}
        </Submit>
      );
    }

    return (
      <Wrapper>
        <article>
          <P>{copy}</P>
          <Center>{button}</Center>
        </article>
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    ActiveRegData: state.signup,
    ChoosenSub: state.signup.ChosenSubscription,
    Campaign: state.signup.userRegCampaign,
    Payment: state.signup.userRegPayment,
    Authkey: state.signup.userRegAuthkey,
    UserId: state.signup.userRegId,
    LoggedIn: state.account.loggedIn,
    Route: state.route.location.pathname,
    RouteSearch: state.route.location.search,
    LoggedInData: state.account.userData,
  };
}

export default connect(mapStateToProps, { regPaymentRequest })(RegTrustly);
