import React from 'react';
import PropTypes from 'prop-types';
import DataLayer from 'containers/App/datalayer';
import Cookies from 'universal-cookie';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Lock from './images/IconLock';
import Visa from './images/visa.png';
import Master from './images/master.png';
import BankID from './images/bankid.svg';
import Trustly from './images/trustly.png';
import RegCardForm from './RegCardForm';
import RegTrustly from './RegTrustly';
import Translation from 'translation/nextory-web-se';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { ToastContainer, toast, style } from 'react-toastify';
import { internationalizationLanguage } from 'containers/App/api';
import { scroller } from 'react-scroll';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  Payment,
  SafePayment,
  FoldTrigger,
  Cards,
  Mobile,
  Foldout,
  HeadingText,
  SeccondHeading,
  PaymentHead,
  Clear,
  PaymentTitle,
  CardImg,
  CardImg2,
  WhiteBg,
  Span
} from './StyledPayment';

class RegisterCardPage extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
    RouteSearch: PropTypes.string,
  };

  state = {
    card: false,
    bankid: false,
  };

  componentDidMount() {
    if (this.props.RouteSearch.includes('trustlyfail')) {
      this.notifyTrustlyFail();
    }
  }

  notifyTrustlyFail = () => {
    toast.success(Translation.registration.stage3.messages.trustyfail_success, {
      position: toast.POSITION.BOTTOM_CENTER,
      style: style({
        colorSuccess: '#ff3a54',
        width: '380px',
      }),
    });
  };

  scrollTo() {
    if (window.innerWidth < 768) {
      scroller.scrollTo('scroll-to-payment', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart',
        offset: -20,
      });
    }
  };

  handleCardClick = () => {
    this.setState({ card: !this.state.card, bankid: false });
    this.scrollTo();
  };

  handleBankidClick = () => {
    this.setState({ bankid: !this.state.bankid, card: false });
    this.scrollTo();
  };

  render() {
    const subscriptionPeriod = JSON.parse(localStorage.getItem('sub-period'))
    const twoWeeks = new Date(new Date().setDate(new Date().getDate() + subscriptionPeriod));
    const date = twoWeeks.toISOString().substring(0, 10);
    // console.log(date)

    let regActive = false;
    if (new Cookies().get('reg')) {
      regActive = true;
    }

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <Span>
                {Translation.registration.stage} 3 {' '}{Translation.registration.of} {' '} 3
              </Span>
              <HeadingText>{Translation.registration.stage3.heading}</HeadingText>
              <SeccondHeading>
                {Translation.registration.stage3.bullets.point1_p1} {date}{' '}
                {Translation.registration.stage3.bullets.point1_p2}
              </SeccondHeading>
            </Heading>

            <Payment name="scroll-to-payment">
              <PaymentHead>
                <PaymentTitle>
                  {Translation.registration.stage3.regcardform.paymenttitle}
                </PaymentTitle>
                <SafePayment>
                  <Lock />  {Translation.registration.stage3.secure_server}
                </SafePayment>
                <Clear />
              </PaymentHead>
              <FoldTrigger active={this.state.card} onClick={this.handleCardClick}>
                {Translation.registration.stage3.credit_or_debit_card}
                <CardImg>
                  <Cards visa alt="Visa" src={Visa} />
                  <Cards master alt="Mastercard" src={Master} />
                </CardImg>
              </FoldTrigger>
              <Foldout active={this.state.card}>
                <article>
                  <RegCardForm />
                </article>
              </Foldout>
              {
                internationalizationLanguage !== "FI" ?
                  <FoldTrigger active={this.state.bankid} onClick={this.handleBankidClick}>
                    {Translation.registration.stage3.mobile_bankid}
                    <CardImg2>
                      <Mobile bankid alt="BankId" src={BankID} />
                      <Mobile trustly alt="Trustly" src={Trustly} />
                    </CardImg2>
                  </FoldTrigger>
                  : null
              }

              <Foldout active={this.state.bankid}>
                <RegTrustly />
              </Foldout>
            </Payment>
          </InnerWrapper>
        </RegisterWrapper>
        <WhiteBg active={this.state.card} active2={this.state.bankid} />

        {this.props.LoggedIn && !regActive && <Redirect to={'/konto'} />}

        <ToastContainer autoClose={5000} />
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    RouteSearch: state.route.location.search,
  };
}

export default connect(mapStateToProps)(RegisterCardPage);
