import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { adyenKey } from 'containers/App/api';
import { Field, reduxForm } from 'redux-form';
import { regPaymentRequest } from 'containers/Views/Registration/actions';
import adyenEncrypt from 'adyen-cse-js';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import { renderFieldSelect } from 'components/Form/FieldSelect';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import Cookies from 'universal-cookie';
import Card from './images/card.png';
import Question from './images/question-circle.svg';
import validate from './validate';
import cardNumber from './cardnumber';
import CVV from './cvv';
import Translation from 'translation/nextory-web-se';
import { getSubscriptionIdByName } from 'containers/App/common';
import {
  FormWrapper,
  CustomField,
  CardField,
  ThreeColumn,
  Month,
  Cvv,
  CvvHelp,
  ImgCvvQ,
  Tooltip,
  SubmitSC,
  PreSubmit
} from './StyledRegCardForm';

class RegCardForm extends Component {
  static propTypes = {
    activateUser: PropTypes.object,
    RouteSearch: PropTypes.string,
    daOrderid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    daAuthkey: PropTypes.string,
    handleSubmit: PropTypes.func,
    orderid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    authkey: PropTypes.string,
    ActiveReg: PropTypes.bool,
    LoggedIn: PropTypes.bool,
    ChoosenSub: PropTypes.string,
    Campaign: PropTypes.string,
    UserId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    Authkey: PropTypes.string,
    Payment: PropTypes.string,
    regPaymentRequest: PropTypes.func,
    SendingRequest: PropTypes.bool,
    Error: PropTypes.object,
    LoggedInData: PropTypes.object,
    Route: PropTypes.string,
    giftCardAuthkey: PropTypes.string,
    orderidUpdatePayment: PropTypes.string,
    giftCardOrderid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  };

  state = {
    showCVV: false,
    orderid: '',
  };

  showCVV = () => {
    this.setState({ showCVV: true });
  };
  hideCVV = () => {
    this.setState({ showCVV: false });
  };

  // take inputed values and pass them to our redux saga loginRequest
  submit = (values, regPaymentRequest) => {
    const cookies = new Cookies();
    const options = {
      enableValidations: false,
      name: 'adyen',
    };

    const d = new Date();
    const n = d.toISOString();
    const cseInstance = adyenEncrypt.createEncryption(adyenKey, options);

    function encryptMyData() {
      const postData = {};
      const cardData = {
        number: values.cardnumber,
        holderName: 'CUSTOMER',
        cvc: values.cvv,
        expiryMonth: values.month,
        expiryYear: values.year,
        generationtime: n,
      };
      postData['adyen-encrypted-data'] = cseInstance.encrypt(cardData);
      return postData;
    }
    const ad = encryptMyData();
    const adyenancrypteddata = ad['adyen-encrypted-data'];

    /* trial card registration */
    if (this.props.Route === '/register/card') {
      let subscriptionid = getSubscriptionIdByName(this.props.ChoosenSub)
      const orderid = '';
      const paymentgateway = 'ADYEN';

      const campaignname = this.props.Campaign;
      const customerid = this.props.UserId;
      const authkey = this.props.Authkey;
      const paymenttype = this.props.Payment;

      const data = {
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        adyenancrypteddata,
        orderid,
        campaignname,
        customerid,
      };

      return regPaymentRequest(data);

      /* update card details for logged in user */
    } else if (this.props.Route === '/konto/andra-betalning') {
      let orderid = this.props.orderidUpdatePayment;
      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      const authkey = this.props.LoggedInData.authkey;
      const subscriptionid = this.props.LoggedInData.subscriptionid;
      const paymentgateway = 'ADYEN';
      const paymenttype = 'CARD_UPDATE';
      const data = {
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        adyenancrypteddata,
        orderid,
      };
      if (data.orderid) {
        return regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }

      /* card details for creating campaign user */
    } else if (this.props.Route === '/register-campaign/card') {
      let orderid = '';
      let authkey = '';

      if (this.props.RouteSearch === '?da') {
        orderid = this.props.daOrderid;
        authkey = this.props.daAuthkey;
      } else {
        orderid = this.props.orderid;
        authkey = this.props.authkey;
      }

      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      let subscriptionid = getSubscriptionIdByName(this.props.ChoosenSub)
      const paymentgateway = 'ADYEN';
      const paymenttype = 'REDEEM_CAMPAIGN';
      const campaigncode = this.props.Campaign;
      const data = {
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        adyenancrypteddata,
        orderid,
        campaigncode,
      };
      if (data.orderid) {
        return regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }

      /* reactivate card details nonmembers */
    } else if (this.props.Route === '/konto/aktivera-betalning') {
      let subscriptionid = getSubscriptionIdByName(this.props.ChoosenSub)
      let orderid = this.props.activateUser.orderid;
      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      const authkey = this.props.LoggedInData.authkey;
      const paymentgateway = 'ADYEN';
      const paymenttype = 'PURCHASE_SUBSCRIPTION';

      const data = {
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        adyenancrypteddata,
        orderid,
      };
      if (data.orderid) {
        return regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }

      /* buy a new giftcard */
    } else if (this.props.Route === '/presentkort/card') {
      const subscriptionid = 6;
      let orderid = this.props.giftCardOrderid;
      let AmountCount = localStorage.getItem('choosen-amount');
      const authkey = this.props.giftCardAuthkey;

      localStorage.setItem('auth-key', authkey);
      localStorage.setItem('order-id', orderid);

      if (cookies.get('retry')) {
        orderid = cookies.get('retry');
      }
      const paymentgateway = 'ADYEN';
      const paymenttype = 'PURCHASE_GIFTCARD';
      let loggedin = false;
      if (this.props.LoggedIn) {
        loggedin = true;
      }
      const data = {
        subscriptionid,
        authkey,
        paymenttype,
        paymentgateway,
        adyenancrypteddata,
        orderid,
        loggedin,
        AmountCount,
      };
      if (data.orderid) {
        return regPaymentRequest(data);
      } else {
        window.location.reload(true);
      }
    }
  };

  render() {
    const { handleSubmit, regPaymentRequest, SendingRequest, Route } = this.props;
    let button = '';
    if (Route === '/konto/andra-betalning') {
      button = Translation.registration.stage3.regcardform.buttons.text1;
    } else if (
      Route === '/register/card' ||
      Route === '/register/card/'
    ) {
      button = Translation.registration.stage3.regcardform.buttons.text2;
    } else if (Route === '/konto/aktivera-betalning') {
      button = Translation.registration.stage3.regcardform.buttons.text3;
    } else if (Route === '/register-campaign/card') {
      button = Translation.registration.stage3.regcardform.buttons.text4;
    } else if (Route === '/presentkort/card') {
      button = Translation.registration.stage3.regcardform.buttons.text5;
    }

    return (
      <FormWrapper>
        <Form onSubmit={handleSubmit(values => this.submit(values, regPaymentRequest))}>
          <CustomField>
            <CardField>
              <Field
                name="cardnumber"
                type="tel"
                component={renderField}
                label={Translation.forms.labels.cardnumber}
                placeholder={Translation.forms.labels.cardnumber_placeholder}
                pattern="(\d{4} *\d{4} *\d{4} *\d{4})"
                normalize={cardNumber}
                disabled={SendingRequest}
              />
            </CardField>
            <ThreeColumn first>
              <Month>
                <Field
                  name="month"
                  component={renderFieldSelect}
                  label={Translation.forms.labels.cardmonth}
                  disabled={SendingRequest}
                  placeholder="Månad"
                >
                  <option value="0">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.month}
                  </option>
                  <option value="1">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.january} (01)
            </option>
                  <option value="2">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.february} (02)
            </option>
                  <option value="3">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.march} (03)
            </option>
                  <option value="4">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.april} (04)
            </option>
                  <option value="5">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.may} (05)
            </option>
                  <option value="6">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.june} (06)
            </option>
                  <option value="7">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.july} (07)
            </option>
                  <option value="8">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.august} (08)
            </option>
                  <option value="9">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.september}{' '}
                    (09)
            </option>
                  <option value="10">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.october} (10)
            </option>
                  <option value="11">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.november} (11)
            </option>
                  <option value="12">
                    {Translation.registration.stage3.regcardform.credit_debit_option.months.december} (12)
            </option>
                </Field>
              </Month>
            </ThreeColumn>
            <ThreeColumn seccond>
              <Field
                name="year"
                component={renderFieldSelect}
                label={Translation.forms.labels.cardyear}
                disabled={SendingRequest}
                placeholder="Månad"
              >
                <option value="0">
                  {Translation.registration.stage3.regcardform.credit_debit_option.year}
                </option>
                <option value="2018">2018 (18)</option>
                <option value="2019">2019 (19)</option>
                <option value="2020">2020 (20)</option>
                <option value="2021">2021 (21)</option>
                <option value="2022">2022 (22)</option>
                <option value="2023">2023 (23)</option>
                <option value="2024">2024 (24)</option>
                <option value="2025">2025 (25)</option>
                <option value="2026">2026 (26)</option>
              </Field>
            </ThreeColumn>
            <ThreeColumn last>
              <Cvv>
                <Field
                  name="cvv"
                  type="tel"
                  component={renderField}
                  label={Translation.forms.labels.cardcvv}
                  normalize={CVV}
                  pattern="[0-9]*"
                  disabled={SendingRequest}
                  placeholder="CVV"
                />
                <CvvHelp>
                  <ImgCvvQ
                    onMouseEnter={this.showCVV}
                    onMouseLeave={this.hideCVV}
                    src={Question}
                    alt="question mark"
                  />
                </CvvHelp>
                {this.state.showCVV && (
                  <Tooltip>
                    <img src={Card} alt="cvv card" />
                  </Tooltip>
                )}
              </Cvv>
            </ThreeColumn>

            <Center>
              {SendingRequest ? (
                <SubmitSC disabled large type="submit">
                  <LoadingIndicatorSmall />
                </SubmitSC>
              ) : (
                  <SubmitSC large type="submit">
                    {button}
                  </SubmitSC>
                )}
            </Center>
            {(this.props.Error && (this.props.Error.status || this.props.Error.paymentstatus)) && (
              <PreSubmit>
                {Translation.registration.stage3.regcardform.credit_debit_option.error.presubmit}
                <ul>
                  <li>
                    {
                      Translation.registration.stage3.regcardform.credit_debit_option.error.bullets
                        .point1
                    }
                  </li>
                  <li>
                    {
                      Translation.registration.stage3.regcardform.credit_debit_option.error.bullets
                        .point2
                    }
                  </li>
                  <li>
                    {
                      Translation.registration.stage3.regcardform.credit_debit_option.error.bullets
                        .point3
                    }
                  </li>
                  <li>
                    {
                      Translation.registration.stage3.regcardform.credit_debit_option.error.bullets
                        .point4
                    }
                  </li>
                </ul>
                {Translation.registration.stage3.regcardform.credit_debit_option.error.please_try_again}
              </PreSubmit>
            )}

            {
              Route !== '/register/card/b' && // NEWUI
              !this.props.ActiveReg &&
              !this.props.LoggedIn &&
              Route !== '/presentkort/card' &&
              Route !== '/lospresentkort-purchase-success' &&
              Route !== '/presentkort/card' &&
              Route !== '/presentkort/thankyou' &&
              Route !== '/register-campaign/card' && <Redirect to={'/'} />}
          </CustomField>
        </Form>
      </FormWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    RouteSearch: state.route.location.search,
    Route: state.route.location.pathname,
    ActiveReg: state.signup.activeReg,
    ActiveRegData: state.signup,
    ChoosenSub: state.signup.ChosenSubscription,
    SendingRequest: state.signup.currentlySending,
    Error: state.signup.error,
    Campaign: state.signup.userRegCampaign,
    Payment: state.signup.userRegPayment,
    Authkey: state.signup.userRegAuthkey,
    UserId: state.signup.userRegId,
    LoggedIn: state.account.loggedIn,
    LoggedInData: state.account.userData,
  };
}

export default withRouter(
  connect(mapStateToProps, { regPaymentRequest })(
    reduxForm({
      form: 'registercard',
      validate,
    })(RegCardForm)
  )
);
