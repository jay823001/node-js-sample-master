import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Img from 'components/Img';
import ScrollDown from './ScrollDown';
import Book1 from 'containers/Views/HomePageSummer/Images/book-covers/husmoderns-död.jpg';
import Book2 from 'containers/Views/HomePageSummer/Images/book-covers/hinsides-väktare.jpg';
import Book3 from 'containers/Views/HomePageSummer/Images/book-covers/supergott-och-supersnabbt.jpg';
import Book4 from 'containers/Views/HomePageSummer/Images/book-covers/hälsorevolutionen.jpg';
import Book5 from 'containers/Views/HomePageSummer/Images/book-covers/de-vises-sten.jpg';
import Book6 from 'containers/Views/HomePageSummer/Images/book-covers/alltid-din-dotter.jpg';
import Book7 from 'containers/Views/HomePageSummer/Images/book-covers/ett-mörker-mitt-ibland-oss.jpg';
import Book8 from 'containers/Views/HomePageSummer/Images/book-covers/omgiven-av-idioter.jpg';
import Book9 from 'containers/Views/HomePageSummer/Images/book-covers/syndabocken.jpg';
import Book10 from 'containers/Views/HomePageSummer/Images/book-covers/factfullnes.jpg';
import Book11 from 'containers/Views/HomePageSummer/Images/book-covers/vildplockat.jpg';
import Book12 from 'containers/Views/HomePageSummer/Images/book-covers/den-frusna-elden.jpg';
import Book13 from 'containers/Views/HomePageSummer/Images/book-covers/konsten-att-hålla-sig-flytande.jpg';
import Book14 from 'containers/Views/HomePageSummer/Images/book-covers/12-livsregler.jpg';
import Book15 from 'containers/Views/HomePageSummer/Images/book-covers/hunger-alma-katsu.jpg';
import Book16 from 'containers/Views/HomePageSummer/Images/book-covers/ödesryttarna.jpg';
import Translation from 'translation/nextory-web-se';
import { internationalizationLanguage } from '../../../App/api';

//summer page cover
import Ett_mörker_mitt_ibland_oss from '../Images/FI-book-covers/Adult/Ett_mörker_mitt_ibland_oss.jpg'
import Halsorevolutionen from '../Images/FI-book-covers/Adult/Halsorevolutionen.jpg'
import Hinsides_väktare from '../Images/FI-book-covers/Adult/Hinsides_väktare.jpg'
import Husmoderns_dod from '../Images/FI-book-covers/Adult/Husmoderns_dod.jpg'
import Konsten_all_halla_sig_flytande from '../Images/FI-book-covers/Adult/Konsten_all_halla_sig_flytande.jpg'
import Odesryttarna from '../Images/FI-book-covers/Adult/Odesryttarna.jpg'

const GridWrap = styled.ul`
  display: inline-block;
  position: absolute;
  max-width: 55rem;
  margin: 7rem auto auto auto;
  width: 130%;
  left: -15%;
  right: -15%;

  .book-col-3{
    top: -5em !important;
  }

  ${media.medium`
    max-width: 155rem;
    width: 110%;
    left: -5%;
    right: -5%;
  `};
`;

const ImgContainer = styled.li`
  display: ${props => (props.desktoponly ? 'none' : 'inline-block')};
  position: relative;
  width: 23%;
  margin: 0 1%;
  img {
    box-shadow: 0 0.2rem 1rem -0.1rem rgba(0, 0, 0, 0.3);
    margin-bottom: 0.6rem;
  }
  &:nth-child(odd) {
    top: -4rem;
  }
  &:nth-child(3) {
    top: -7rem;
  }
  &:nth-child(2),
  &:nth-child(6) {
    top: -10rem;
  }
  &:nth-child(4),
  &:nth-child(8) {
    top: 0rem;
  }


  ${media.medium`
    display: ${props => props.desktoponly && 'inline-block'};
    width: 11.5%;
    margin: 0 0.4% 0 0.4%;
    img {
      box-shadow: 0 0.5rem 1.5rem -0.3rem rgba(0, 0, 0, 0.3);
      margin-bottom: 1.2rem;
    }
    &:nth-child(odd) {
      top: 0;
    }
    &:nth-child(2),
    &:nth-child(6) {
      top: -10rem;
    }
    &:nth-child(4),
    &:nth-child(8) {
      top: 5rem;
    }

  `};
`;

class Grid extends React.PureComponent {
  state = {
    imagesLoaded: false,
  };

  componentDidMount() {
    /* this handles animation of bookcovers,
    css is available in globalstyle.js */
    this.timer = setTimeout(() => {
      this.setState({
        imagesLoaded: true,
      });
    }, 300);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {

    const FinnishCoverPage = (<div>
      <ScrollDown />
      <GridWrap className={`visible${this.state.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={Ett_mörker_mitt_ibland_oss} alt={'Ett_mörker_mitt_ibland_oss'} />
          <Img src={Halsorevolutionen} alt={'Halsorevolutionen'} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Hinsides_väktare} alt={'Hinsides_väktare'} />
          <Img src={Husmoderns_dod} alt={'Husmoderns_dod'} />
        </ImgContainer>
        <ImgContainer className="book-col-3">
          <Img
            src={Konsten_all_halla_sig_flytande}
            alt={'Konsten_all_halla_sig_flytande'}
          />
          <Img
            src={Odesryttarna}
            alt={'Odesryttarna'}
          />
        </ImgContainer>
      </GridWrap>
    </div>);

    const SwedishCoverPage = (<div>
      <ScrollDown />
      <GridWrap className={`visible${this.state.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={Book1} alt={Translation.newhomepage.summerHero.images_alt_text.Book1} />
          <Img src={Book2} alt={Translation.newhomepage.summerHero.images_alt_text.Book2} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Book3} alt={Translation.newhomepage.summerHero.images_alt_text.Book3} />
          <Img src={Book4} alt={Translation.newhomepage.summerHero.images_alt_text.Book4} />
        </ImgContainer>
        <ImgContainer className="book-col-3">
          <Img
            src={Book5}
            alt={Translation.newhomepage.summerHero.images_alt_text.Book5}
          />
          <Img
            src={Book6}
            alt={Translation.newhomepage.summerHero.images_alt_text.Book6}
          />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={Book7}
            alt={Translation.newhomepage.summerHero.images_alt_text.Book7}
          />
          <Img src={Book8} alt={Translation.newhomepage.summerHero.images_alt_text.Book8} />
        </ImgContainer>
        <ImgContainer>
          <Img
            src={Book9}
            alt={Translation.newhomepage.summerHero.images_alt_text.Book9}
          />
          <Img
            src={Book10}
            alt={Translation.newhomepage.summerHero.images_alt_text.Book10}
          />
        </ImgContainer>
        <ImgContainer>
          <Img src={Book11} alt={Translation.newhomepage.summerHero.images_alt_text.Book11} />
          <Img src={Book12} alt={Translation.newhomepage.summerHero.images_alt_text.Book12} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img
            src={Book13}
            alt={Translation.newhomepage.summerHero.images_alt_text.Book13}
          />
          <Img src={Book14} alt={Translation.newhomepage.summerHero.images_alt_text.Book14} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Book15} alt={Translation.newhomepage.summerHero.images_alt_text.Book15} />
          <Img src={Book16} alt={Translation.newhomepage.summerHero.images_alt_text.Book16} />
        </ImgContainer>
      </GridWrap>
    </div>);

    if (internationalizationLanguage === "FI") {
      return FinnishCoverPage;
    } else {
      return SwedishCoverPage;
    }

  }
}

export default Grid;
