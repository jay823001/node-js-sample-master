import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';

import HeroCta from './HeroCta';
import Grid from './Grid';
import { H1, H2 } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';

const Hero = styled.section`
  height: 70rem;
  position: relative;
  padding: 1.5rem;
  text-align: center;
  background-color: ${props => props.theme.summerWhite};
  z-index: 600;
  ${media.medium`
    padding: 1.5rem 0;
    height: 87rem;
  `};
  ${media.large`
    height: 100rem;
  `};
`;

const HeroText = styled.div`
  position: relative;
  z-index: 10;
  margin: 8rem auto auto auto;
  color: ${props => props.theme.black};
  ${media.medium`
    margin: 14rem auto auto auto;
  `};
`;

const H1Custom = H1.extend`
  line-height: 1.1;
  ${media.medium`
    font-size: 60px;
    max-width: 665px;
  `}
`;

const H2custom = styled(H2)`
  max-width: 27rem;
  font-family: 'Maison';
  line-height: 1.39;
  font-size: 1.8rem;
  font-weight: normal;
  letter-spacing: normal;
  ${media.medium`
    font-size: 2rem;
    max-width: 43rem;
  `};
`;




class HeroHome extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };
  state = {
    sticky: false,
  };

  render() {
    return (
      <Hero>
        <HeroText>
          <H1Custom>{Translation.newhomepage.summerHero.text}</H1Custom>
          <Waypoint
            onLeave={() => this.setState({ sticky: true })}
            onEnter={() => this.setState({ sticky: false })}
            topOffset={'-25px'}
          >
            <H2custom>{Translation.newhomepage.summerHero.subheading}</H2custom>
          </Waypoint>

          <HeroCta
            activeGiftReg={this.props.activeGiftReg}
            activeReg={this.props.activeReg}
            loggedIn={this.props.loggedIn}
            activateAcc={this.props.activateAcc}
            sticky={this.state.sticky}
          />
          <Grid />
        </HeroText>
      </Hero>
    );
  }
}

export default HeroHome;
