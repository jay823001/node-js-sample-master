import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';

import Grid from './Grid';
import { P, H3 } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';

const Section = styled.section`
  position: relative;
  padding: 6.5rem 1.5rem;
  height: 108rem;
  background-color: ${props => props.theme.summerPink};
  ${media.medium`
    padding: 8rem 1.5rem;
    height: 108rem;
  `};
  ${media.large`
    height: 98rem;
  `};
`;

const Wrapper = styled.div`
  max-width: 120rem;
  padding: 0 1.5rem;
  margin: auto;
`;

const Pcustom = styled(P)`
  max-width: 52rem;
`;
const PcustomContaienr = styled.div`

${media.large`
    width:50%;
    float:left;
  `};
`;
const PcustomSingle = styled.div`


  ${media.large`
    min-height: 108px;
    width: 435px;
    margin: 0 auto;
  `};

`;
const Clear = styled.div`
 clear:both;
`;

class ChildSection extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };
  state = {
    imagesLoaded: false,
  };

  loadImages = () => {
    this.setState({ imagesLoaded: true });
  };

  render() {
    return (
      <Waypoint onEnter={this.loadImages} bottomOffset={'50px'}>
        <Section>
          <Wrapper>

            <PcustomContaienr>
              <PcustomSingle>

                <H3>{Translation.newhomepage.summerChildren.heading1} </H3>
                <Pcustom>
                  {Translation.newhomepage.summerChildren.text1}
                </Pcustom>
              </PcustomSingle>

              <PcustomSingle>
                <H3>{Translation.newhomepage.summerChildren.heading2}  </H3>
                <Pcustom>
                  {Translation.newhomepage.summerChildren.text2}
                </Pcustom>
              </PcustomSingle>

            </PcustomContaienr>

            <PcustomContaienr>
              <PcustomSingle>
                <H3>{Translation.newhomepage.summerChildren.heading3}  </H3>
                <Pcustom>
                  {Translation.newhomepage.summerChildren.text3}
                </Pcustom>
              </PcustomSingle>
              <PcustomSingle>
                <H3>{Translation.newhomepage.summerChildren.heading4}  </H3>
                <Pcustom>
                  {Translation.newhomepage.summerChildren.text4}
                </Pcustom>
              </PcustomSingle>

            </PcustomContaienr>


            <Clear />
            <Grid imagesLoaded={this.state.imagesLoaded} />
          </Wrapper>
        </Section>
      </Waypoint>
    );
  }
}

export default ChildSection;
