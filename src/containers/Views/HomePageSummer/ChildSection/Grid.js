import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Img from 'components/Img';
import Book1 from 'containers/Views/HomePageSummer/Images/book-covers/sallader-året-runt.jpg';
import Book2 from 'containers/Views/HomePageSummer/Images/book-covers/hinsides-väktare.jpg';
import Book3 from 'containers/Views/HomePageSummer/Images/book-covers/vego-på-30-min.jpg';
import Book4 from 'containers/Views/HomePageSummer/Images/book-covers/pirater.jpg';
import Book5 from 'containers/Views/HomePageSummer/Images/book-covers/supergott-och-supersnabbt.jpg';
import Book6 from 'containers/Views/HomePageSummer/Images/book-covers/min-bästis-målvakten.jpg';
import Book7 from 'containers/Views/HomePageSummer/Images/book-covers/frukost-hela-dagen.jpg';
import Book8 from 'containers/Views/HomePageSummer/Images/book-covers/de-vises-sten.jpg';
import Book9 from 'containers/Views/HomePageSummer/Images/book-covers/japansk-grillning.jpg';
import Book10 from 'containers/Views/HomePageSummer/Images/book-covers/ödesryttarna.jpg';
import Book11 from 'containers/Views/HomePageSummer/Images/book-covers/det-gröna-skafferiet.jpg';
import Book12 from 'containers/Views/HomePageSummer/Images/book-covers/handbok-för-superhjältar-3.jpg';
import Book13 from 'containers/Views/HomePageSummer/Images/book-covers/baka-med-godis.jpg';
import Book14 from 'containers/Views/HomePageSummer/Images/book-covers/till-alla-killar.jpg';
import Book15 from 'containers/Views/HomePageSummer/Images/book-covers/chokladbollar.jpg';
import Book16 from 'containers/Views/HomePageSummer/Images/book-covers/bajsboken.jpg';
import Translation from 'translation/nextory-web-se';
import { internationalizationLanguage } from '../../../App/api';
import Det_grona_skafferiet from '../Images/FI-book-covers/Children/Det_grona_skafferiet.jpg';
import Handbok_for_superhjaltar_3 from '../Images/FI-book-covers/Children/Handbok_for_superhjaltar_3.jpg';
import Hinsides_väktare from '../Images/FI-book-covers/Children/Hinsides_väktare.jpg';
import Min_bastis_malvakten from '../Images/FI-book-covers/Children/Min_bastis_malvakten.jpg';
import Odesryttarna from '../Images/FI-book-covers/Children/Odesryttarna.jpg';
import salladeraretrunt from '../Images/FI-book-covers/Children/salladeraretrunt.jpg';
import Vego_pa_30_minuter from '../Images/FI-book-covers/Children/Vego_pa_30_minuter.jpg';


const GridWrap = styled.ul`
  display: inline-block;
  position: absolute;
  max-width: 55rem;
  margin: 7rem auto auto auto;
  width: 130%;
  left: -15%;
  right: -15%;

  .book-col-3{
    top: -5em !important;
  }

  ${media.medium`
    max-width: 155rem;
    width: 110%;
    left: -5%;
    right: -5%;
  `};
`;

const ImgContainer = styled.li`
  display: ${props => (props.desktoponly ? 'none' : 'inline-block')};
  position: relative;
  width: 23%;
  margin: 0 1%;
  img {
    box-shadow: 0 0.2rem 1rem -0.1rem rgba(0, 0, 0, 0.3);
    margin-bottom: 0.6rem;
  }
  &:nth-child(odd) {
    top: -4rem;
  }
  &:nth-child(3) {
    top: -7rem;
  }
  &:nth-child(2),
  &:nth-child(6) {
    top: -10rem;
  }
  &:nth-child(4),
  &:nth-child(8) {
    top: 0rem;
  }


  ${media.medium`
    display: ${props => props.desktoponly && 'inline-block'};
    width: 11.5%;
    margin: 0 0.4% 0 0.4%;
    img {
      box-shadow: 0 0.5rem 1.5rem -0.3rem rgba(0, 0, 0, 0.3);
      margin-bottom: 1.2rem;
    }
    &:nth-child(odd) {
      top: 0;
    }
    &:nth-child(2),
    &:nth-child(6) {
      top: -10rem;
    }
    &:nth-child(4),
    &:nth-child(8) {
      top: 5rem;
    }

  `};
`;


class Grid extends React.PureComponent {
  static propTypes = {
    imagesLoaded: PropTypes.bool,
  };

  render() {

    const FinnishCoverPage = (<div>
      <GridWrap className={`visible${this.props.imagesLoaded}`}>
        <ImgContainer desktoponly>

        </ImgContainer>
        <ImgContainer desktoponly>

        </ImgContainer>
        <ImgContainer>
          <Img src={Odesryttarna} alt={'Odesryttarna'} />
          <Img src={salladeraretrunt} alt={'salladeraretrunt'} />
        </ImgContainer>
        <ImgContainer>
          <Img src={Vego_pa_30_minuter} alt={'Vego_pa_30_minuter'} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Det_grona_skafferiet} alt={'Det_grona_skafferiet'} />
          <Img src={Handbok_for_superhjaltar_3} alt={'Handbok_for_superhjaltar_3'} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Hinsides_väktare} alt={'Hinsides_väktare'} />
          <Img src={Min_bastis_malvakten} alt={'Min_bastis_malvakten'} />
        </ImgContainer>
        <ImgContainer>
        </ImgContainer>
        <ImgContainer>

        </ImgContainer>
      </GridWrap>
    </div>);

    const SwedishCoverPage = (<div>
      <GridWrap className={`visible${this.props.imagesLoaded}`}>
        <ImgContainer desktoponly>
          <Img src={Book1} alt={Translation.newhomepage.summerChildren.images_alt_text.Book1} />
          <Img src={Book2} alt={Translation.newhomepage.summerChildren.images_alt_text.Book2} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Book3} alt={Translation.newhomepage.summerChildren.images_alt_text.Book3} />
          <Img src={Book4} alt={Translation.newhomepage.summerChildren.images_alt_text.Book4} />
        </ImgContainer>
        <ImgContainer>
          <Img src={Book5} alt={Translation.newhomepage.summerChildren.images_alt_text.Book5} />
          <Img src={Book6} alt={Translation.newhomepage.summerChildren.images_alt_text.Book6} />
        </ImgContainer>
        <ImgContainer>
          <Img src={Book7} alt={Translation.newhomepage.summerChildren.images_alt_text.Book7} />
          <Img src={Book8} alt={Translation.newhomepage.summerChildren.images_alt_text.Book8} />
        </ImgContainer>
        <ImgContainer>
          <Img src={Book9} alt={Translation.newhomepage.summerChildren.images_alt_text.Book9} />
          <Img src={Book10} alt={Translation.newhomepage.summerChildren.images_alt_text.Book10} />
        </ImgContainer>
        <ImgContainer>
          <Img src={Book11} alt={Translation.newhomepage.summerChildren.images_alt_text.Book11} />
          <Img src={Book12} alt={Translation.newhomepage.summerChildren.images_alt_text.Book12} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Book13} alt={Translation.newhomepage.summerChildren.images_alt_text.Book13} />
          <Img src={Book14} alt={Translation.newhomepage.summerChildren.images_alt_text.Book14} />
        </ImgContainer>
        <ImgContainer desktoponly>
          <Img src={Book15} alt={Translation.newhomepage.summerChildren.images_alt_text.Book15} />
          <Img src={Book16} alt={Translation.newhomepage.summerChildren.images_alt_text.Book16} />
        </ImgContainer>
      </GridWrap>
    </div>);

    if (internationalizationLanguage === "FI") {
      return FinnishCoverPage;
    } else {
      return SwedishCoverPage;
    }


  }
}

export default Grid;
