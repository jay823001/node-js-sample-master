import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

const MenuItem = styled.li`
  position: relative;
  margin: 0 0 2rem;
  text-align: center;
  color: white;

  ${props =>
    props.calltoaction &&
    css`
      display: none;
    `};

  ${media.large`
    display: inline-block;
    margin: 0 0 0 2.5rem;
    text-align: left;
    ${props =>
      props.accountbuttons &&
      css`
        margin: 0 0 0 6.4rem;
        position: relative;
        top: -0.1rem;
      `}
    ${props =>
      props.calltoaction &&
      css`
        margin: 0 0 0 1.8rem;
        display: inline-block;
        position: relative;
        top: -0.1rem;
      `}
  `};
`;

export default MenuItem;
