import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { closeMenu } from 'containers/App/actions/app-actions';
import styled from 'styled-components';
import media from 'theme/styled-utils';
//import { Logo } from './logo';
import Logo from './logo-rgb-blue.svg';
import Nav from './Nav';
import Menutrigger from './MenuTrigger';

const Header = styled.header`
  font-family: 'Maison';
  width: 100%;
  position: absolute;
  z-index: 666;
  display: block;
  width: 100%;
  height: 5rem;
  transition: 0.3s ease-in-out;
  background-color: transparent;
  ${media.large`
    height: 6.5rem;
    margin-top: 1.5rem;
  `};
`;

const HeaderInnerwrap = styled.div`
  max-width: 1200px;
  padding: 0 1.5rem;
  margin: auto;
  height: inherit;
  text-align: center;
  ${media.large`
    text-align: left;
  `};
`;

const StyledNav = styled.nav`
  height: 100vh;
  position: absolute;
  top: 4.9rem;
  right: 0;
  left: 0;
  display: ${props => (props.open ? 'block' : 'none')};
  width: 100%;
  z-index: 1;
  background-color: ${props => props.theme.summerWhite};
  padding: 2.7rem 0 0.7rem 0;
  ${media.large`
    position: relative;
    height: auto;
    top: 1.9rem;
    display: block;
    float: right;
    width: auto;
    padding: 0;
  `};
`;
const LogoDiv = styled.img`
  width: 9rem;
  height: 3.5rem;
  margin-top: 1.7rem;
  float: left;
  polygon,
  path {
    fill: ${props => props.theme.blue};
  }
  ${media.large`
    width: 16rem;
    height: 4.9rem;
    margin-top: 1.5rem;
    float:none;
  `};
`;

class NavBar extends React.PureComponent {
  static propTypes = {
    MenuOpen: PropTypes.bool,
  };

  render() {
    return (
      <Header>
        <HeaderInnerwrap>
          <Link to="/" onClick={this.props.closeMenu}>
            <LogoDiv src={Logo} alt="Logo on Summer-Homepage Menu" />
          </Link>
          <Menutrigger />
          <StyledNav open={this.props.MenuOpen}>
            <Nav />
          </StyledNav>
        </HeaderInnerwrap>
      </Header>
      //Logo need to change
    );
  }
}

function mapStateToProps(state) {
  return {
    MenuOpen: state.appstate.MenuOpen,
  };
}

export default connect(mapStateToProps, { closeMenu }, null, { pure: false })(NavBar);
