import React from 'react';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';
import { H2, H3, P } from '../Styling/typo';
import IphoneSummer from 'containers/Views/HomePageSummer/Images/iphone-summer.png';
import IphoneSmallSummer from 'containers/Views/HomePageSummer/Images/iphone-small-summer.png';
import Translation from 'translation/nextory-web-se';


const PhoneWrapper = styled.div`
  background-color: ${props => props.theme.summerWhite};
`;
const Section = styled.section`
  position: relative;
  padding: 25rem 1.5rem 4.5rem;
  max-width: 110rem;
  margin: auto;
  background-color: ${props => props.theme.summerWhite};
  opacity: 0;
  transform: translateY(5rem);
  transition: all 1s ease-out;
  @media (min-width: 500px) {
    padding: 25rem 1.5rem 4.5rem;
  }
  ${props =>
    props.visible &&
    css`
      opacity: 1;
      transform: translateY(0);
    `};
  ${media.medium`
    padding: 22rem 1.5rem 6.5rem;
  `};
  ${media.large`
    padding: 39rem 1.5rem 6.5rem;
  `};
`;

const Left = styled.div`
  display: none;
  width: 45%;
  z-index: 1;
  ${media.medium`
    display: inline-block;
  `};
`;

const Img = styled.img`
  max-width: 100%;
  width: 50rem;
  height: auto;
  user-select: none;
  border: 0;
  position: absolute;
  bottom: 0;
  ${media.medium`
    width: 40rem;
  `};
  ${media.large`
    width: 50rem;
  `};
`;

const PhoneContent = styled.div`
  margin: 0 0 2rem 0;
  height: 38rem;
  position: relative;
  img {
    height: 38rem;
    position: absolute;
    left: -1.5rem;
  }
  ${media.medium`
    display: none;
  `};
`;

const Right = styled.div`
  position: relative;
  z-index: 10;
  ${media.medium`
    display: inline-block;
    width: 45%;
    margin-left: 9.5%;
    max-width: 39rem;
  `};
`;

class PhoneSection extends React.PureComponent {
  state = {
    visible: false,
  };

  fadeIn = () => {
    this.setState({ visible: true });
  };

  render() {
    return (
      <Waypoint onEnter={this.fadeIn} bottomOffset={'300px'}>
        <PhoneWrapper>
          <Section visible={this.state.visible} name="scroll-to-phone">
            <Left>
              <Img src={IphoneSummer} alt="Iphone" />
            </Left>
            <Right>
              <H2>{Translation.newhomepage.summerIphone.heading}</H2>
              <H3>{Translation.newhomepage.summerIphone.paragraph1.heading}</H3>
              <P>{Translation.newhomepage.summerIphone.paragraph1.content}</P>
              <PhoneContent>
                <img src={IphoneSmallSummer} alt="Iphone" />
              </PhoneContent>
              <H3>{Translation.newhomepage.summerIphone.paragraph2.heading}</H3>
              <P>{Translation.newhomepage.summerIphone.paragraph2.content}</P>
              <H3>{Translation.newhomepage.summerIphone.paragraph3.heading}</H3>
              <P>{Translation.newhomepage.summerIphone.paragraph3.content}</P>
            </Right>
          </Section>
        </PhoneWrapper>
      </Waypoint>
    );
  }
}

export default PhoneSection;
