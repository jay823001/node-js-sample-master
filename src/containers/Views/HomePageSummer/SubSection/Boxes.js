import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { H3, P } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';
import {
  apiUrl,
  internationalization_a,
  fetchAuth,
} from '../../../App/api';

const BoxWrapper = styled.div`
  max-width: 120rem;
  padding: 0 1.5rem;
  margin: auto;
  text-align: center;
  ${media.medium`
    margin: 2.5rem auto 3.3rem auto;
    justify-content: space-between;
    flex-wrap: wrap;
    display: flex;
  `};
`;

const SubBox = styled.div`
  background-color: white;
  border-radius: 0.5rem;
  display: block;
  padding: 3.5rem 3rem 3.2rem 3rem;
  text-align: left;
  margin: 0 0 2rem 0;
  ${media.medium`
    padding: 3.5rem 2rem 3.2rem 2rem;
    display: inline-block;
    margin: 0 1.5%;
    width: 30%;
  `};
  ${media.large`
    padding: 4.5rem 4rem 4.2rem 4rem;
  `};
`;

const SubBoxHeading = styled.div``;

const H3custom = styled(H3)`
  margin-bottom: 0.4rem;
`;

const SubBoxList = styled.ul`
  margin: 0 0 2rem 0;
  padding-left: 2rem;
  ${media.medium`
    margin: 2.5rem 0 3.3rem 0;
  `};
`;

const SubBoxListItem = styled.li`
  margin: 0 0 0.5rem 0;
  font-size: 1.8rem;
  list-style: disc;
`;

const SubBoxListItemStrike = styled(SubBoxListItem)`
  text-decoration: line-through;
`;

const SubBoxFooter = styled.div`
  text-align: right;
  font-size: 1.5rem;
`;

class SubSectionBoxes extends React.PureComponent {

  state = {
    ObjSub: null,
  };

  async componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          ObjSub: {
            countrycode: data.data.subscriptions[0].countrycode,
            sub1: {
              subid: data.data.subscriptions[0].subid,
              subname: data.data.subscriptions[0].subname,
              subprice: data.data.subscriptions[0].subprice,
            }, sub2: {
              subid: data.data.subscriptions[1].subid,
              subname: data.data.subscriptions[1].subname,
              subprice: data.data.subscriptions[1].subprice,
            }, sub3: {
              subid: data.data.subscriptions[2].subid,
              subname: data.data.subscriptions[2].subname,
              subprice: data.data.subscriptions[2].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <BoxWrapper>
        <SubBox>
          <SubBoxHeading>
            <H3custom>{this.state.ObjSub === null ? '' : this.state.ObjSub.sub1.subname}</H3custom>
            <P>
            {/* {this.state.ObjSub === null ? ''
              : this.state.ObjSub.countrycode === 'FI' ? '14 päivää ilmaiseksi, sitten ' + this.state.ObjSub.sub1.subprice + ' Euro/kuukausi' :
                '14 dagar fritt, sedan ' + this.state.ObjSub.sub1.subprice + ' kr/mån'} */}
                {this.state.ObjSub === null ? '' :
                Translation.newhomepage.subsection.package1.content.p1 + ' ' +
                this.state.ObjSub.sub1.subprice + ' ' +
                Translation.newhomepage.subsection.currency
                }
            </P>
          </SubBoxHeading>
          <SubBoxList>
            <SubBoxListItemStrike>{Translation.newhomepage.subsection.package1.content.p2}</SubBoxListItemStrike>
            <SubBoxListItem>{Translation.newhomepage.subsection.package1.content.p3}</SubBoxListItem>
          </SubBoxList>
        </SubBox>

        <SubBox>
          <SubBoxHeading>
            <H3custom>{this.state.ObjSub === null ? '' : this.state.ObjSub.sub2.subname}</H3custom>
            <P>
            {/* {this.state.ObjSub === null ? ''
              : this.state.ObjSub.countrycode === 'FI' ? '14 päivää ilmaiseksi, sitten ' + this.state.ObjSub.sub2.subprice + ' Euro/kuukausi' :
                '14 dagar fritt, sedan ' + this.state.ObjSub.sub2.subprice + ' kr/mån'} */}
                {this.state.ObjSub === null ? '' :
                Translation.newhomepage.subsection.package2.content.p1 + ' ' +
                this.state.ObjSub.sub2.subprice + ' ' +
                Translation.newhomepage.subsection.currency
                }
            </P>
          </SubBoxHeading>
          <SubBoxList>
            <SubBoxListItem>{Translation.newhomepage.subsection.package2.content.p2}</SubBoxListItem>
            <SubBoxListItem>{Translation.newhomepage.subsection.package2.content.p3}</SubBoxListItem>
          </SubBoxList>
          <SubBoxFooter>{Translation.newhomepage.subsection.package2.content.p4}</SubBoxFooter>
        </SubBox>

        <SubBox>
          <SubBoxHeading>
            <H3custom>
            {/* {this.state.ObjSub === null ? '' : this.state.ObjSub.sub3.subname} */}
            {this.state.ObjSub === null ? '' : Translation.app.common.family}
            </H3custom>
            <P>
            {/* {this.state.ObjSub === null ? ''
              : this.state.ObjSub.countrycode === 'FI' ? '14 päivää ilmaiseksi, sitten ' + this.state.ObjSub.sub3.subprice + ' Euro/kuukausi' :
                '14 dagar fritt, sedan ' + this.state.ObjSub.sub3.subprice + ' kr/mån'} */}
                {this.state.ObjSub === null ? '' :
                Translation.newhomepage.subsection.package3.content.p1 + ' ' +
                this.state.ObjSub.sub3.subprice + ' ' +
                Translation.newhomepage.subsection.currency
                }
            </P>
          </SubBoxHeading>
          <SubBoxList>
            <SubBoxListItem>{Translation.newhomepage.subsection.package3.content.p2}</SubBoxListItem>
            <SubBoxListItem>{Translation.newhomepage.subsection.package3.content.p3}</SubBoxListItem>
          </SubBoxList>
          <SubBoxFooter>{Translation.newhomepage.subsection.package3.content.p4}</SubBoxFooter>
        </SubBox>
      </BoxWrapper>
    );
  }
}

export default SubSectionBoxes;
