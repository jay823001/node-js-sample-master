import React from 'react';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';

import SubSectionBoxes from './Boxes';
import { H2, P } from '../Styling/typo';
import { IconOwl } from 'containers/Views/HomePageB/Icons/icon-owl';
import { IconClock } from 'containers/Views/HomePageB/Icons/icon-clock';
import { IconDevices } from 'containers/Views/HomePageB/Icons/icon-devices';
import Translation from 'translation/nextory-web-se';

const Section = styled.section`
  position: relative;
  padding: 6rem 1.5rem 0.5rem;
  background-color: ${props => props.theme.summerPink};
  ${media.medium`
    padding: 7rem 1.5rem 2rem 1.5rem;
  `};
  ${media.large`
    padding: 14rem 1.5rem 4.5rem;
  `};
`;

const Wrapper = styled.div`
  opacity: 0;
  transform: translateY(5rem);
  transition: all 1s ease-out;
  ${props =>
    props.visible &&
    css`
      opacity: 1;
      transform: translateY(0);
    `};
`;

const H2custom = styled(H2)`
  ${media.medium`
    text-align: center;
  `};
`;

const UspList = styled.ul`
  margin: 4rem auto 5rem auto;
  li {
    display: block;
    margin-bottom: 3rem;
    @media (min-width: 500px) {
      margin-bottom: 4rem;
    }
  }
  svg {
    float: left;
    margin-right: 1.5rem;
  }

  ${media.medium`
    max-width: 56rem;
    svg {
      position: relative;
      top: -1rem;
    }
  `};

  ${media.large`
    margin: 4rem auto 3rem auto;
    text-align: center;
    max-width: 100rem;
    li {
      display: inline-block;
      width: 30%;
      margin-bottom: 0;
      text-align: left;
      vertical-align: top;
      &:nth-child(2) {
        margin: 0 5%;
      }
      &:nth-child(3) svg {
        margin-right: 0.9rem;
      }
    }
    svg {
      top: 0;
    }
  `};
`;

class SubSection extends React.PureComponent {
  state = {
    visible: false,
  };

  fadeIn = () => {
    this.setState({ visible: true });
  };

  render() {
    return (
      <Waypoint onEnter={this.fadeIn} bottomOffset={'100px'}>
        <Section>
          <Wrapper visible={this.state.visible}>
            <H2custom>{Translation.newhomepage.subsection.heading}</H2custom>
            <UspList>
              <li>
                <IconOwl />
                <P>{Translation.newhomepage.subsection.features.owl}</P>
              </li>
              <li>
                <IconClock />
                <P>{Translation.newhomepage.subsection.features.clock}</P>
              </li>
              <li>
                <IconDevices />
                <P>{Translation.newhomepage.subsection.features.devices}</P>
              </li>
            </UspList>

            <SubSectionBoxes />
          </Wrapper>
        </Section>
      </Waypoint>
    );
  }
}

export default SubSection;
