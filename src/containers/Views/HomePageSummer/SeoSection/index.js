import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import { P, H2 } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';

const InfoWrapper = styled.section`
  padding: 5rem 0;
  background-color: ${props => props.theme.summerWhiteLight};
  ${media.medium`
    padding: 7.5rem 0;
  `};
`;

const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  ${media.medium`
    display: flex;
    justify-content: space-between;
  `};
  ${media.large`
    max-width: 95rem;
    padding: 0;
  `};
`;

const Column = styled.div`
  margin-bottom: 2.3rem;
  ${media.medium`
    margin-bottom: 0;
    width:45%;
    text-align:justify;
  `};
`;

const StyledH2 = styled(H2)`
  font-size: 2.1rem;
  letter-spacing: 0;
  margin: 0 0 0.4rem 0;
`;

const StyledP = styled(P)`
  font-size: 1.5rem;
  & + h2 {
    margin: 2.5rem 0 0.4rem 0;
  }
`;

const SeoText = () => (
  <InfoWrapper>
    <InnerWrapper>
      <Column>
        <StyledH2>{Translation.newhomepage.seo.find_audio_books.heading}</StyledH2>
        <StyledP>
          {Translation.newhomepage.seo.find_audio_books.content}
        </StyledP>

        <StyledH2>{Translation.newhomepage.seo.listen_to_audio.heading}</StyledH2>
        <StyledP>
          {Translation.newhomepage.seo.listen_to_audio.content.p1}
        </StyledP>
        <StyledP>
          {Translation.newhomepage.seo.listen_to_audio.content.p2}
        </StyledP>
        <StyledP>
          {Translation.newhomepage.seo.listen_to_audio.content.p3}
        </StyledP>
      </Column>

      <Column>
        <StyledH2>{Translation.newhomepage.seo.ebooks_for_all.heading}</StyledH2>
        <StyledP>
          {Translation.newhomepage.seo.ebooks_for_all.content}
        </StyledP>
        <StyledH2>{Translation.newhomepage.seo.information_about_ebooks.heading}</StyledH2>
        <StyledP>
          {Translation.newhomepage.seo.information_about_ebooks.content.p1}
        </StyledP>
        <StyledP>
          {Translation.newhomepage.seo.information_about_ebooks.content.p2}
        </StyledP>
      </Column>
    </InnerWrapper>
  </InfoWrapper>
);

export default SeoText;
