import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import media from 'theme/styled-utils';

//import { Logo } from './logo';
import Logo from './logo-white.svg';
import { H3 } from '../Styling/typo';
import FooterMenu from './FooterMenu';
import InnerWrapper from './InnerWrapper';
import SocialIcons from './Social';
import Copyright from './Copyright';
import Translation from 'translation/nextory-web-se';

const FooterStyled = styled.footer`
  background-color: ${props => props.theme.blue};
  color: white;
  padding: 5rem 1.5rem 5rem 1.5rem;
  height: 60rem;
  position: relative;
  z-index: 10;
  ${media.medium`
    height: 37.5rem;
    padding: 6.8rem 1.5rem 6.3rem 1.5rem;
  `};
  ${media.large`
    height: 34.5rem;
  `};
  * {
    color: white;
  }
`;

const Wrapper = styled.div`
  margin: 4rem 0 3.5rem 0;
  ${media.medium`
    margin: -3rem 0 0 0;
    float: right;
    text-align: right;
  `};
`;

const LogoImg = styled.img`
    width: 130px;
    height: 34px;
    display: inline-block;
    margin-bottom: 1rem;
`;

class Footer extends React.PureComponent {
  static propTypes = {
    Route: PropTypes.string,
  };

  render() {
    return (
      !this.props.Route.includes('/app/') && (
        <FooterStyled>
          <InnerWrapper>
            <H3>{Translation.newhomepage.footer.heading}</H3>
            <FooterMenu />
            <Wrapper>
              <SocialIcons />
              <LogoImg src={Logo} alt="Logo at Summer-Homepage Footer" />
              <Copyright />
            </Wrapper>
          </InnerWrapper>
        </FooterStyled>
      )
    );
  }
}

function mapStateToProps(state) {
  return {
    Route: state.route.location.pathname,
  };
}

export default connect(mapStateToProps)(Footer);
