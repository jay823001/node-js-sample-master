import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import { IconInstagram } from 'containers/Views/HomePageB/Icons/icon-instagram';
import { IconFacebook } from 'containers/Views/HomePageB/Icons/icon-facebook';

const StyledUL = styled.ul`
  display: block;
  margin: 0 0 2rem 0;
  ${media.medium`
    margin:0 0 4.6rem 0;
  `};
`;

const Li = styled.li`
  display: inline-block;
  margin: 0 1.5rem 0 0;
  transition: all 0.1s ease;
  &:hover {
    transform: scale(1.07);
  }
  ${media.medium`
    margin: 0 0 0 1.5rem;
  `};
`;

const SocialIcons = () => (
  <StyledUL>
    <Li>
      <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/nextory.se/">
        <IconInstagram />
      </a>
    </Li>
    <Li>
      <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/Nextory.se/">
        <IconFacebook />
      </a>
    </Li>
  </StyledUL>
);

export default SocialIcons;
