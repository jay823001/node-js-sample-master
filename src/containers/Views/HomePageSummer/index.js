import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import DataLayer from 'containers/App/datalayer';
import HeroHome from './Hero/Hero';
import NavBar from './Menu';
import PhoneSection from './PhoneSection';
import IpadSection from './IpadSection';
import ChildSection from './ChildSection';
import SubSection from './SubSection';
import RatingSection from './RatingSection';
import Footer from './Footer';
import SeoSection from './SeoSection';
import Translation from 'translation/nextory-web-se';

class HomePageSummer extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    GiftReg: PropTypes.string,
    UserDetails: PropTypes.object,
  };

  componentDidMount() {

    removeElementsByClass('gQHgRL');
    function removeElementsByClass(className) {
      var elements = document.getElementsByClassName(className);
      while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
      }
    }
  }

  render() {
    const seotitle = (Translation.homepage.seotitle);
    const seodesc = (Translation.homepage.seodescription);
    return (
      <div>
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <DataLayer />
        <NavBar />
        <HeroHome
          activeGiftReg={this.props.GiftReg}
          activeReg={this.props.ActiveReg}
          loggedIn={this.props.LoggedIn}
          activateAcc={this.props.UserDetails}
        />
        <PhoneSection />
        <IpadSection />
        <ChildSection />
        <SubSection />
        <RatingSection />
        <Footer />
        <SeoSection />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    LoggedIn: state.account.loggedIn,
    UserDetails: state.account.userData.UserDetails,
    UserAuthkey: state.account.userData.authkey,
    GiftReg: state.signup.userRegPayment,
  };
}

export default connect(mapStateToProps)(HomePageSummer);
