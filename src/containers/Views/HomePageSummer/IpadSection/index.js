import React from 'react';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Waypoint from 'react-waypoint';
import BackgroundImageSummer from 'containers/Views/HomePageSummer/Images/relax-listen.jpg';
import BackgroundImageSummerSmall from 'containers/Views/HomePageSummer/Images/relax-listen-mobile.jpg';
import { H2 } from '../Styling/typo';
import Translation from 'translation/nextory-web-se';

const Section = styled.section`
  position: relative;
  height: 45rem;
  background-image: url(${BackgroundImageSummerSmall});
  background-position: left;
  background-repeat: no-repeat;
  background-size: cover;
  color: white;
  &:after {
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: 100%;
    width: 100%;
    content: '';
    background-color: rgba(0, 0, 0, 0.15);
  }
  ${media.medium`
    height: 75rem;
  `};

  ${media.small`
    background-image: url(${BackgroundImageSummer});
    background-position: center;
  `};
`;

const VerticalAlign = styled.div`
  position: absolute;
  z-index: 5;
  top: 50%;
  right: 0;
  bottom: auto;
  left: 0;
  max-width: 120rem;
  padding: 0 1.5rem;
  margin: auto;
  transform: translateY(-25%);
  opacity: 0;
  transition: all 1s ease-out;
  ${props =>
    props.visible &&
    css`
      opacity: 1;
      transform: translateY(-55%);
    `};
`;

const H2custom = styled(H2)`
  font-size: 2.4rem;
  text-align:center;
  ${media.medium`
    font-size: 2.4rem;
  `};
`;

class IpadSection extends React.PureComponent {
  state = {
    visible: false,
  };

  fadeIn = () => {
    this.setState({ visible: true });
  };

  render() {
    return (
      <Section>
        <VerticalAlign visible={this.state.visible}>
          <Waypoint onEnter={this.fadeIn} bottomOffset={'100px'} />
          <H2custom>{Translation.newhomepage.summerIpad.text}</H2custom>
        </VerticalAlign>
      </Section>
    );
  }
}

export default IpadSection;
