import React from 'react';
import styled from 'styled-components';

const StyledSvg = styled.svg`
  width: 2.7rem;
  height: 2.7rem;
  path {
    fill: ${props => props.theme.black};
  }
`;

export const IconSearch = () => (
  <StyledSvg role="img" viewBox="0 0 24 24" aria-labelledby="magnifyglass">
    <title id="magnifyglass">Magnify glass</title>
    <path d="M20.85,20.15,17,16.28A7.92,7.92,0,0,0,19,11a8,8,0,1,0-8,8,7.92,7.92,0,0,0,5.28-2l3.87,3.86a.48.48,0,0,0,.7,0A.48.48,0,0,0,20.85,20.15ZM11,18a7,7,0,1,1,7-7A7,7,0,0,1,11,18Z" />
  </StyledSvg>
);
