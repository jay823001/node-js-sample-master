/* eslint consistent-return:0 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { stopSubmit, startSubmit } from 'redux-form';
import { fetchLogin, postRedeemGiftvoucer, postUserCreate, fetchUserDetails } from 'containers/App/api';
import { SET_AUTH, CLEAR_ERROR } from 'containers/Views/Account/LoginPage/constants';
import { REFRESH_STORE } from 'containers/Views/Account/MyAccount/constants';
import {
  CREATEUSER_CLEAR_ERROR,
  CREATEUSER_SETAUTH,
} from 'containers/Views/Registration/constants';
import { USER_REDEEM_GIFT } from './constants';
import Translation from 'translation/nextory-web-se';

const cookies = new Cookies();
/*
* Redeem giftvoucer for existing user
*/
function* checkUserActive(data) {
  const response = yield call(postUserCreate, data);
}



function* redeemGiftVUser(data) {
  let errors = {};
  yield put(startSubmit('giftloggedinruser'));

  // api call
  const response = yield call(fetchLogin, data);

  // do something with the api call response
  if (response.status === 200) {
    const redeem = {
      authkey: response.authkey,
      email: data.data.email,
      giftvoucher: data.data.giftvoucher,
      password: data.data.password,
    };
    const validation = yield call(postRedeemGiftvoucer, redeem);

    if (validation.status === 200) {
      // if regstatus is completed, log in user
      if (validation.data.registrationstatus === 'COMPLETED') {
        // if user is not currently logged in, do so
        if (!cookies.get('user')) {
          // user DETAILS fetch
          const UserDetails = yield call(fetchUserDetails, response.authkey);
          response.data.email = data.data.email;
          response.data.p = data.data.password;
          response.data.authkey = response.authkey;
          response.data.UserDetails = UserDetails;
          const userEncode = Base64.encode(JSON.stringify(response.data));
          new Cookies().set('user', userEncode, { path: '/', maxAge: 1209600 });

          // set auth to true
          yield put({ type: SET_AUTH, newAuthState: true });
        } else {
          // if user is logged in, just refresh store
          yield put({ type: REFRESH_STORE, authkey: response.authkey });
        }

        // redirect to success page
        //yield put(push(`/giftcard-success?${validation.data.paymentdetails.subscriptionrundate}`));
        yield put(push('/presentkort/subscription'));
        // if regstatus is not yet start, start it
      } else {
        const user = {
          campaigncode: data.data.giftvoucher,
          email: data.data.email,
          password: data.data.password,
          paymenttype: validation.data.paymenttype,
          paymentdetails: validation.data.paymentdetails,
          authkey: validation.authkey,
        };

        cookies.set('reg', Base64.encode(JSON.stringify(user)), { path: '/', maxAge: 1209600 });
        yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });

        yield put({ type: CREATEUSER_CLEAR_ERROR });

        // redirect to choose sub page
        yield put(push('/presentkort/subscription'));
      }
    } else {
      errors = {
        giftvoucher: Translation.api_messsages.giftvoucher_error,
      };
      yield put(stopSubmit('giftloggedinruser', errors));
    }
  } else if (response.error.details.includes('password')) {
    errors = {
      password: Translation.api_messsages.password_error,
    };
  } else if (response.error.details.includes('email')) {
    errors = {
      email: Translation.api_messsages.email2_error,
    };
  }
  yield put({ type: CLEAR_ERROR });
  yield put(stopSubmit('giftloggedinruser', errors));
}
export function* redeemGiftvoucherUser() {
  yield takeLatest(USER_REDEEM_GIFT, redeemGiftVUser);
}
