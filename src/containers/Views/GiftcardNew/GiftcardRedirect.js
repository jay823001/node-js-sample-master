import React from 'react';
import { Redirect } from 'react-router';

class RedirectGiftCard extends React.PureComponent {
  render() {
    return <Redirect to={'/presentkort/inlosen'} />;
  }
}

export default RedirectGiftCard;
