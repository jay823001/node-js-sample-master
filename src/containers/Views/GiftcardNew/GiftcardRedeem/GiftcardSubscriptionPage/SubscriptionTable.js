import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BlackDot } from 'components/Subscription/SubIcons';
import { chooseSubscription } from 'containers/Views/Registration/actions';
import {
  apiUrl,
  internationalization_a,
  fetchAuth,
} from 'containers/App/api';
import {
  TableWrapper,
  TableHeading,
  TableContent,
  TableRow,
  LI,
  TableContainer,
  TableTitle,
  FirstRow,
  TableRowHeadText,
  TableLI,
  TableRowSpText,
  TableRowContent,
  TableNo,
  StyledButtonTwo,
  SubscriptionTableHeadBtn,
  Clear
} from './StyledSubTable';
import Translation from 'translation/nextory-web-se';

class SubscriptionTable extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-functions
  static propTypes = {
    chooseSubscription: PropTypes.func,
    ChosenSubscription: PropTypes.string,
    GiftCardAmount: PropTypes.number,
    GCPeriod: PropTypes.number,
  };

  state = {
    sticky: false,
    subscriptionData: null,
    redirect: false
  };

  async componentWillMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package1: {
              packagename: subscriptions[0].subname,
              packageprice: subscriptions[0].subprice,
            }, package2: {
              packagename: subscriptions[1].subname,
              packageprice: subscriptions[1].subprice,
            }, package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  setSubscriptionChoice = (subscription) => {
    this.props.chooseSubscription(subscription);
    this.props.getChosenSubscription(subscription);
    if(subscription === Translation.app.common.silver || subscription === Translation.app.common.gold){
      this.props.handleClick(subscription);
    }
  }

  render() {
    const Silver = Translation.app.common.silver
    const Gold = Translation.app.common.gold
    const Family2 = Translation.app.common.family2

    let amount;
    let roundDays;
    let days;
    if (this.props.GCPeriod) amount = this.props.GiftCardAmount * this.props.GCPeriod;
     function GetDays(packagePrice){
      if (Silver || Gold || Family2)  {
          days = amount / (packagePrice / 30);
          roundDays = Number(days.toFixed(0));
        return roundDays
      }
    };
  
    if (this.props.ChosenSubscription === Translation.app.common.family3 ||
      this.props.ChosenSubscription === Translation.app.common.family4) {
      this.props.chooseSubscription(Translation.app.common.family2);
    }

    let urlToFamilyPage = '/presentkort/subscription#familj'

    return (
      <TableWrapper>
        <TableContainer>
          <TableHeading sticky={this.props.sticky}>
            <li>
              <TableTitle>Förbetalt med presentkort
                {/* {Translation.registration.stage1.tabletitle}  */}
              </TableTitle>
            </li>
            <li>
            <SubscriptionTableHeadBtn
               
                onClick={() => this.setSubscriptionChoice(Translation.app.common.silver)}
              >
                {!this.state.subscriptionData ? '' : this.state.subscriptionData.package1.packagename}
              </SubscriptionTableHeadBtn>
            </li>
            <li>
              <SubscriptionTableHeadBtn
               
                onClick={() => this.setSubscriptionChoice(Translation.app.common.gold)}
              >
                {!this.state.subscriptionData ? '' : this.state.subscriptionData.package2.packagename}
              </SubscriptionTableHeadBtn>
            </li>
            <li>
              <StyledButtonTwo to={urlToFamilyPage} 
                
                onClick={() => this.setSubscriptionChoice(Translation.app.common.family2)}
              >
                {/* {!this.state.subscriptionData ? '' : this.state.subscriptionData.package3.packagename} */}
                {!this.state.subscriptionData ? '' : Translation.app.common.family}
                </StyledButtonTwo>
            </li>
          </TableHeading>

          <TableContent sticky={this.props.sticky}>
            <FirstRow>
              <TableRow>
                <ul>
                  <li>
                  {!this.props.GCPeriod ? (
                    <TableRowHeadText>
                      
                    </TableRowHeadText>
                    ):(
                      <TableRowHeadText>
                      Så här länge räcker ditt presentkort
                      
                      {/* {Translation.subscription_table.listitems.item1d} {roundDays}{' '} */}
                      {/* {Translation.subscription_table.listitems.item1e} {date} */}
                    </TableRowHeadText>
                    )}
                  </li>

                  <TableLI>
                    <TableRowSpText>
                      {!this.props.GCPeriod? (
                        <span>
                          {/* {this.props.GCPeriod === null ? '' : GetDays(139) || !GetDays(139)} {''}
                          {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date} */}
                        </span>
                      ):(
                        <span>
                          {this.props.GCPeriod === null ? '' : GetDays(139)} {''}
                          {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date}
                        </span>
                      )}
                        {/* {GetDays(139)} {''} dagar */}
                        {/* {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package1.packageprice}
                        {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency} */}
                    </TableRowSpText>
                  </TableLI>
                  <TableLI>
                    <TableRowSpText>
                    {!this.props.GCPeriod? (
                        <span>
                         
                        </span>
                      ):(
                        <span>
                          {this.props.GCPeriod === null ? '' : GetDays(169)} {''}
                          {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date}
                        </span>
                      )}
                        {/* {GetDays(169)} {''} dagar */}
                        {/* {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package2.packageprice}
                        {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency} */}
                    </TableRowSpText>
                  </TableLI>
                  <TableLI>
                    <TableRowSpText>
                      {!this.props.GCPeriod ? (
                        <span>
                          {/* {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date1} {''}
                          {this.props.GCPeriod === null ? '' : GetDays(199) || !GetDays(199)} {''}
                          {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date} */}
                        </span>
                      ):(
                        <span>
                          {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date1} {''}
                          {this.props.GCPeriod === null ? '' : GetDays(199)} {''}
                          {this.props.GCPeriod === null ? '' : ' ' + Translation.subscription_table.dates.date}
                        </span>
                      )}
                        {/* Upp till {GetDays(199)} {''} dagar */}
                      {/* {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package3.packageprice}
                      {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency} */}
                    </TableRowSpText>
                    {/* <Infotext>{Translation.subscription_table.listitems.infotext} </Infotext> */}
                  </TableLI>
                  <Clear/>
                </ul>
              </TableRow>
            </FirstRow>
            <TableRowContent>
              <TableRow>

                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item2}
                  </li>
                  <LI >
                    <TableNo>
                      1
                   </TableNo>
                  </LI>
                  <LI >
                    <TableNo>
                      1
                   </TableNo>
                  </LI>
                  <LI >
                    <TableNo>
                      2-4
                   </TableNo>
                  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item3}
                  </li>
                  <LI > </LI>
                  <LI >   <BlackDot /> </LI>
                  <LI >  <BlackDot />  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item4}
                  </li>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI>
                  <BlackDot />
                  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item5}
                  </li>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item6}
                  </li>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                </ul>
              </TableRow>
              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item7}
                  </li>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                </ul>
              </TableRow>
              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item8}
                  </li>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                  <LI>
                  <BlackDot />
                  </LI>
                </ul>
              </TableRow>
              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item9}
                  </li>
                  <LI>
                  <BlackDot />
                  </LI>
                  <LI>
                  <BlackDot />
                  </LI>
                  <LI >
                  <BlackDot />
                  </LI>
                </ul>
              </TableRow>
            </TableRowContent>
          </TableContent>
        </TableContainer>
      </TableWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription: state.signup.ChosenSubscription,
    SignupDetails: state.signup,
    GiftCardAmount: state.signup.paymentdetails.amount,
    GCPeriod: state.signup.paymentdetails.period,
  };
}

export default connect(mapStateToProps, { chooseSubscription })(SubscriptionTable);
