import auth from 'containers/App/auth/auth';
import {
  CHOOSE_SUBSCRIPTION,
  CREATEUSER_REQUEST,
  CREATEUSER_LOGOUT,
  CREATEUSER_ERROR,
  CREATEUSER_CLEAR_ERROR,
  CREATEUSER_SETAUTH,
  REGPAYMENT_REQUEST,
  REGPAYMENT_ERROR,
  CHOOSE_MONTH,
  CHOOSE_AMOUNT,
} from './constants';

// app reducer
const initialSignupState = {
  ChosenSubscription: auth.choosenSubsciption(),
  ChooseMonth: auth.chooseMonth(),
  ChooseAmount: auth.chooseAmount(),
  email: auth.userRegistration().email,
  activeReg: auth.activeRegistration(),
  campaigncode: auth.userRegistration().campaigncode,
  campaignname: auth.userRegistration().campaignname,
  campaignprice: auth.userRegistration().campaignprice,
  campaignintervel: auth.userRegistration().campaignintervel,
  campaignintervelindays: auth.userRegistration().campaignintervelindays,
  campaignperiod: auth.userRegistration().campaignperiod,
  campaigndays: auth.userRegistration().campaigndays,
  campaigndiscounted: auth.userRegistration().campaigndiscounted,
  campaigntrialdays: auth.userRegistration().campaigntrialdays,
  campaignfixed: auth.userRegistration().campaignfixed,
  paymentrequired: auth.userRegistration().paymentrequired,
  paymentdetails: auth.userRegistration().paymentdetails,
  newuser: auth.userRegistration().newuser,
  orderid: auth.userRegistration().orderid,
  userRegCampaign: auth.userRegistration().campaignname,
  userRegPayment: auth.userRegistration().paymenttype,
  userRegAuthkey: auth.userRegistration().authkey,
  userRegId: auth.userRegistration().userid,
  currentlySending: false,
  error: {
    status: '',
    data: {},
  },
};

export function SignupReducers(
  state = initialSignupState,
  { type, subscription, month, amount, newAuthState, error }
) {
  switch (type) {
    case CHOOSE_SUBSCRIPTION:
      const serializedState = JSON.stringify(subscription);
      localStorage.setItem('sub-state', serializedState);
      return { ...state, ChosenSubscription: subscription };

    case CHOOSE_MONTH:
      const serializedMonth = JSON.stringify(month);
      localStorage.setItem('choosen-month', serializedMonth);
      return { ...state, ChooseMonth: month };
      
    case CHOOSE_AMOUNT:
      const serializedAmount = JSON.stringify(amount);
      localStorage.setItem('choosen-amount', serializedAmount);
      return { ...state, ChooseAmount: amount };

    case CREATEUSER_REQUEST:
      return { ...state, currentlySending: true };

    case CREATEUSER_SETAUTH:
      return {
        ...state,
        activeReg: newAuthState,
        email: auth.userRegistration().email,
        campaigncode: auth.userRegistration().campaigncode,
        campaignname: auth.userRegistration().campaignname,
        campaignprice: auth.userRegistration().campaignprice,
        campaignintervel: auth.userRegistration().campaignintervel,
        campaignintervelindays: auth.userRegistration().campaignintervelindays,
        campaignperiod: auth.userRegistration().campaignperiod,
        campaigndays: auth.userRegistration().campaigndays,
        campaigndiscounted: auth.userRegistration().campaigndiscounted,
        campaigntrialdays: auth.userRegistration().campaigntrialdays,
        campaignfixed: auth.userRegistration().campaignfixed,
        paymentdetails: auth.userRegistration().paymentdetails,
        paymentrequired: auth.userRegistration().paymentrequired,
        newuser: auth.userRegistration().newuser,
        orderid: auth.userRegistration().orderid,
        userRegCampaign: auth.userRegistration().campaignname,
        userRegPayment: auth.userRegistration().paymenttype,
        userRegAuthkey: auth.userRegistration().authkey,
        userRegId: auth.userRegistration().userid,
      };

    case CREATEUSER_ERROR:
      return { ...state, currentlySending: false, error };

    case CREATEUSER_CLEAR_ERROR:
      return { ...state, error: {}, currentlySending: false };

    case REGPAYMENT_REQUEST:
      return { ...state, currentlySending: true };

    case REGPAYMENT_ERROR:
      return { ...state, currentlySending: false, error };

    case CREATEUSER_LOGOUT:
      localStorage.clear();
      return {
        ...state,
        ChosenSubscription: auth.choosenSubsciption(),
        //ChosenMonth: auth.chosenMonth(),
        email: '',
        activeReg: false,
        campaigncode: '',
        campaignname: '',
        campaignprice: '',
        campaignintervel: '',
        campaignintervelindays: '',
        campaignperiod: '',
        campaigndays: '',
        campaigndiscounted: '',
        campaigntrialdays: '',
        campaignfixed: '',
        paymentrequired: '',
        newuser: '',
        orderid: '',
        userRegCampaign: '',
        userRegPayment: '',
        userRegAuthkey: '',
        userRegId: '',
      };

    default:
      return state;
  }
}
