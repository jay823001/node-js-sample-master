import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import Button from 'components/Buttons';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';

export const TableWrapper = styled.article`
  position: relative;
  min-height: 465px;
`;

export const TableWrapperFix = styled.article`
  padding-top: 10px;
  display: table-row;
  -webkit-overflow-scrolling: touch;
  position: fixed;
  top: 0;
  z-index: 99999999;
  width: auto;
  background: #eaf0e2;
  width: auto;
  right: 15px;
  left: 15px;
  height: 70px;
  visibility:hidden;

  ${media.medium`
    display:none;
  `}

  ${props =>
    props.sticky &&
    css`
      visibility: visible;
    `};
`;

export const StyledButton = styled(Button)`
  width: 80%;
  height: 50px;
  border-radius: 26px;
  border: solid 2px #363bec;
  background-color: #fff;
  box-shadow: none;
  -webkit-text-align: center;
  -webkit-text-align: center;
  text-align: center;
  padding: 0px;
  margin: 0px;
  text-transform: uppercase;
  font-weight: bold;
  color:${props => props.theme.colorBlue};

  ${media.medium`
    font-size: 1.6rem;
  `}
  ${mediaCustom.medium`
    width: 80%;
  `}
  ${mediaCustom.small`
    width: 80%;
  `}
  &:hover {
    background-color:${props => props.theme.colorBlue};
    color:#fff;
  }

`;

export const StyledButtonTwo = styled(Button)`
  width: 80%;
  height: 50px;
  border-radius: 26px;
  border: solid 2px #363bec;
  background-color: ${props => props.theme.colorBlue};
  box-shadow: none;
  -webkit-text-align: center;
  -webkit-text-align: center;
  text-align: center;
  padding: 0px;
  margin: 0px;
  text-transform: uppercase;
  font-weight: bold;
  color:#fff;
  padding-top: 12px;
  font-family: 'MaisonDemi';
  ${media.medium`
    font-size: 1.6rem;
  `}
  ${mediaCustom.medium`
    width: 80%;
  `}
  ${mediaCustom.small`
    width: 80%;
  `}
  &:hover {
    background-color:#fff ;
    color:${props => props.theme.colorBlue};
    border: solid 2px #363bec;
  }
`;

export const ButtonExtend = Button.withComponent('button');

export const ButtonStyled = styled(ButtonExtend)`
  width: 80%;
  height: 50px;
  border-radius: 26px;
  border: solid 2px #363bec;
  background-color: #fff;
  box-shadow: none;
  -webkit-text-align: center;
  -webkit-text-align: center;
  text-align: center;
  padding: 0px;
  margin: 0px;
  text-transform: uppercase;
  font-weight: bold;
  color:${props => props.theme.colorBlue};

  ${media.medium`
    font-size: 1.6rem;
  `}
  ${mediaCustom.medium`
    width: 80%;
  `}
  ${mediaCustom.small`
    width: 80%;
  `}
  &:hover {
    background-color:${props => props.theme.colorBlue};
    color:#fff;
  }

`;

export const TableHeading = styled.ul`
  margin: 3rem 0 0;
  font-weight: 600;
  margin-bottom: 10px;
  margin-top:0px;
  ${media.medium`
    margin: 4.5rem 0 0;
  `}
  li {
    position: relative;
    display: inline-block;
    &:first-child {
      width: 54%;
      display: none;
      ${media.medium`
        display:inline-block;
      `}
    }
    &:nth-child(2), &:nth-child(3), &:nth-child(4) {
      float: left;
      width: 33.3%;
      text-align: center;
      ${media.medium`
        float: none;
        width: 15%;
      `}
    }
  }

  ${props =>
    props.sticky &&
    css`
    padding-top: 10px;
    display: table-row;
    -webkit-overflow-scrolling: touch;
    position: fixed;
    top: 0;
    z-index: 99999999;
    width: auto;
    background: #fff;
    width: auto;
    right: 5px;
    left: 5px;
    height: 70px;
    `};

    ${media.medium`
    height: auto;
    position: relative;
    display: block;
    left: 0;
    right: 0;
    top: 0;
    padding-top: 28px;
    margin-top: 0px;
    padding-bottom: 15px;
    padding-left: 45px;
    padding-right:45px;
  `}
`;

export const TableHeadingFix = styled.ul`

  ${media.medium`
    margin: 4.5rem 0 0;
  `}
  li {
    position: relative;
    display: inline-block;
    &:first-child {
      width: 54%;
      display: none;
      ${media.medium`
        display:inline-block;
      `}
    }
    &:nth-child(2), &:nth-child(3), &:nth-child(4) {
      float: left;
      width: 33.3%;
      text-align: center;
      ${media.medium`
        float: none;
        width: 15%;
      `}
    }
  }


`;

export const TableContent = styled.ul`
    margin: 3.0rem 0 4.0rem;
  ${media.medium`
    font-size:1.6rem;
  `}
  ${props =>
    props.sticky &&
    css`
    margin-top: 87px;

    `};
    ${media.medium`
    margin: 0rem 0 4.0rem;
  `}

`;

export const TableRow = styled.li`
  display: block;
  margin: 0 0 1.6rem;
  :nth-of-type(odd){
    background-color:#fafafa;
  }
  color:${props => props.theme.tableFont};
  ${media.medium`
    margin: 0;
    padding: 1.2rem 45px;
    padding-left: 15x;
    //padding-right: 0px;
  `}
  &:last-child {
    border-bottom:none;
  }
  ul li {
    line-height: 1.3;
    display: inline-block;
    margin: 0;
    padding: 0;
    word-break: keep-all;
    &:first-child {
      font-size: 1.4rem;
      width: 100%;
      margin: 0 0 1.1rem;
      text-align: center;
      padding: 10px 5px;
      ${media.medium`
        font-size: 1.4rem;
        width: 54%;
        margin: 0;
        text-align: left;
        padding:0px;
      `}

    }
    &:nth-child(2), &:nth-child(3), &:nth-child(4) {
      font-size: 1.4rem;
      float: left;
      width: 33.3%;
      padding: .5rem 0;
      text-align: center;
      // border-right: .1rem solid ${props => props.theme.colorGrey};
      color: ${props => props.theme.colorGrey};
      ${media.medium`
        float: none;
        width: 15%;
        padding: 0;
        border-right: none;
      `}
    }
    &:last-child {
      border-right: none;
    }
  }
`;

export const LI = styled.li`
 
    color: ${props => props.theme.colorBlue} !important;
  
`;

export const TableLI = styled(LI)`
  span{
    color:#585858;
    //font-weight: bold;
    font-size:14px;
  }
`;
export const TableTitle = styled.h2`
  font-size: 25px;
  font-weight: 600;
  max-width: 200px;
  line-height: 29px;
  position: absolute;
  bottom: -47px;
  font-family: 'MaisonDemi';
`;
export const TableContainer = styled.div`
  border-radius: 6px;
  background-color: #ffffff;
  padding: 20px 0px;
  margin: auto;
  max-width: 93rem;
  position: initial;
  left: 0;
  right: 0;
  //box-shadow: 3px 3px 12px 4px #b4b4b42e;

${mediaCustom.medium`
  padding:20px 5px;
  position:relative;
  box-shadow:none;

`}
`;
export const Infotext = styled.p`
  font-size:11px;
  line-height: 1.18;
  letter-spacing: normal;
  text-align: center;
  color: rgba(88, 88, 88, 0.5)
`;
export const TableRowSpText = styled.div`
  position:relative;
  bottom: 18px;
  font-weight: bold;
    font-family: 'Maison';
    font-size: 14px;
    color: #585858;
  
  ${mediaCustom.medium`
  bottom:0px;
`};
`;
export const TableRowHeadText = styled.div`
  max-width: 215px;
  ${mediaCustom.medium`
    bottom: 0px;
    top: 12px;
    max-width:initial;
  `};
`;
export const TableNo = styled.span`
  color: #585858;
  font-size: 15px;
  font-weight: bold;
`;
export const TableRowContent = styled.div`
  li{
    font-family: 'Maison';
    font-size: 14px;
    color: #585858;
    
  }
  ${mediaCustom.medium`
     li{
     margin-bottom: 3px !important;
    }
  `};
`;
export const FirstRow = styled.div`
   li{
    background-color: #ffff !important;
  }
`;

// SubscriptionTable family Styles

export const PkgBoxWrapperParent = styled.section`
position: relative;
${mediaCustom.medium`
  //height:300px;
`};
`;
export const PkgBoxWrapper = styled.section`
  position: initial;
  left: 0;
  right: 0;
  max-width: 93rem;
  margin: auto;
  margin-bottom: 32px;

  ${mediaCustom.medium`
  position: relative;
  //bottom: 90px;
  `};
`;

export const PkgBox = styled.div`
  width: 30%;
  height: 220px;
  border-radius: 6px;
  background-color: #ffffff;
  float: left;
  margin: 10px;
  //box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.08), 0 12px 24px 2px rgba(234, 243, 245, 0.25);

  ${mediaCustom.medium`
      float: none;
      width: 96%;
      margin: 0 auto;
      margin-bottom: 7px;
      height: 118px;
  `};
`;
export const PriceContainer = styled.div`
  padding: 40px;
  ${mediaCustom.mlarge`
    padding: 40px 25px;
  `};
  ${mediaCustom.medium`
    padding: 20px !important;
  `};
`;
export const PriceInfo = styled.div`
    ${mediaCustom.medium`
      float:left;
    `};
  `;
export const PriceButton = styled.div`
  ${mediaCustom.medium`
    float:right;
  `};
  button{
    padding: 12px 5px;
    width: 100%;
    height: 45px;
    border-radius: 27px;
    color: #fff;
    background-color: #353bec;
    border: none;
    margin-top: 15px;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: bold;
    cursor:pointer;
    font-family: 'MaisonDemi';
    letter-spacing: 0.9px;
    &:hover {
      background-color: #373ae0;
    }
    ${mediaCustom.medium`
      width:94px !important;
      span{
        display:none;
      }
    `};
  }
`;
export const PriceTitle = styled.h2`
  font-size: 25px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.2;
  letter-spacing: -0.2px;
  color: #444444;
  margin: 0px;
  font-weight: bold;
  margin-bottom: 5px;
  font-family: 'MaisonDemi';
  ${mediaCustom.medium`
    font-size: 21px !important;
  `};
`;
export const PriceInfoTitle = styled.p`
  font-size: 17px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: #555555;
  margin:0px;
  max-width: 185px;
  font-family: 'Maison';
  ${mediaCustom.medium`
    max-width: 155px !important;
    font-size: 14px !important;
  `};
`;
export const PriceButtonBtn = styled(StyledButton)`
  padding: 14px 5px;
  width: 100%;
  height: 45px;
  border-radius: 27px;
  color: #fff;
  background-color: #353bec;
  border: none;
  margin-top: 15px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: bold;
  cursor:pointer;
  &:hover {
    background-color: #373ae0;
  }
  ${mediaCustom.medium`
    width:94px !important;
    span{
      display:none;
    }
  `};
`;

export const Clear = styled.div`
  clear:both;
 `;

//  Index page style

export const RegisterWrapper = styled.section`
  padding: 3rem 0 1rem;

  ${media.medium`
    padding: 14rem 0 1rem;
    padding-bottom: 10rem;
  `};
  ${mediaCustom.medium`
    padding: 8rem 0 0rem;
    padding-bottom: 25px;
  `};
`;
export const H1Main = styled(H1)`
  font-size: 5.1rem;
  max-width: 685px;
  margin: 0 auto;
  margin-bottom: 20px;
  ${mediaCustom.medium`
      font-size: 3.1rem;
  `};
`;
export const H2Main = styled(H2)`
      font-size: 1.8rem;
      font-family: 'Maison';
      font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: center;
  color: #555555;
`;
export const ButtonMain = styled(Button)`
  width: 245px;
  height: 54px;
  border-radius: 27px;
  text-transform: uppercase;
  font-size: 14px;
  padding: 15px 0px;
  font-weight: bold;
  ${mediaCustom.medium`
      width: 290px;
  `};
`;

export const ButtonPrimary = styled(Button)`
  border-radius: 27px;
  max-width: 30rem;
  text-transform: uppercase;
  font-size: 15px;
  font-weight: bold;
  ${mediaCustom.medium`
  font-size: 12px;
`};
`;
export const TableButtons = styled.div`
    button{
      border-radius: 27px;
      max-width: 30rem;
      text-transform: uppercase;
      font-size: 15px;
      font-weight: bold;
      ${mediaCustom.medium`
      font-size: 12px;
    `};
    }

`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 93rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
      margin-bottom: 3.9rem;
  `};
`;

export const Span = styled.span`
  display: block;
  padding-top: 10px;
  font-size: 1.4rem;
  margin: 0 0 0.8rem;
  ${media.medium`
    font-size: 1.6rem;
    margin: 20px 0 1.4rem;
  `};
`;

export const InnerWrapperc = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;

`;

export const SubcriptionC = styled.div`
  padding-top: 8rem;
  padding-right: 3rem;
  padding-left: 3rem;
  padding-bottom: 0rem;
  background:#eaf0e2;
  text-align:center;

  ${media.medium`
    padding-top: 1rem;
  `};

  img {
    margin-bottom: 0.8rem;
  }
`;

export const SubcriptionCInfo = styled.div`
  font-size: 1.6rem;
  margin-bottom: 0rem;
  padding-bottom: 15px;
  ${media.medium`
    margin-bottom: 2.5rem;
    margin-top: 2.5rem;
    padding-bottom: 0px;
  `};
`;
export const ButtonWhiteWrapper = styled.section`
    text-align:center;
    background:${props => props.theme.greenBg};
    padding-bottom:70px;
    ${mediaCustom.medium`
    padding-bottom: 0px;
    height: 120px;
    padding-top: 35px;

    `};
`;

export const SubcriptionStage2 = styled.section`
      background: #eaf0e2;
`;

export const SubcriptionStage2Heading = styled.div`
      text-align:center;
`;

export const SubcriptionStage2Bottom = styled.section`
      background: #fff;
      padding: 20px;
      min-height: 375px;
      padding-top: 250px;

      ${props =>
    props.sticky2 &&
    css`
        position: fixed;
        bottom: 0px;
        width: 100%;
      `};

      ${mediaCustom.medium`
        padding: 20px;
        min-height: auto;
        padding-top: 25px
    `};
  `;

export const H2MainBox = styled(H2)`
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    text-align: center;
    color: #555555;
    margin-top: 25px;
    font-family: 'Maison';
`;

export const BoxButtonPrimary = styled(Button)`
    width: 160px;
    height: 54px;
    border-radius: 27px;
    border: solid 2px #e0e0e0;
    -webkit-text-transform: uppercase;
    text-transform: uppercase;
    background-color:${props => props.theme.summerWhite};
    font-size: 15px;
    box-shadow: none;
    color: ${props => props.theme.seccondGrey};
    font-weight: bold;
    &:hover{
      color:#fff;
    }
    ${mediaCustom.medium`
    font-size: 13px;
    width: 95%;
    height: 54px
    `};
`;
export const InnerWrapperd = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;
  ${mediaCustom.medium`
    padding:0px;
    width: 65%;
  `};
  ${mediaCustom.small`
    padding:0px;
    width: 100%;
  `};
`;

export const SubcriptionStage2BoxBottom = styled.section`
    background: #fff;
    padding: 20px;
    min-height: 250px;
    padding-top: 150px;

    ${mediaCustom.medium`
      padding: 20px;
      min-height: auto;
      padding-top: 25px
  `};
  `;

export const PkgBoxContainer = styled.section`
  position: relative;
  height: 100px;
  margin-top: 20px;
  ${mediaCustom.medium`
  position: unset;
`};
`;
export const SubscriptionTableHeadBtn = styled.button`

width: 80%;
height: 50px;
border-radius: 26px;
border: none;
background-color: ${props => props.theme.colorBlue};
box-shadow: none;
-webkit-text-align: center;
-webkit-text-align: center;
text-align: center;
padding: 0px;
margin: 0px;
text-transform: uppercase;
font-weight: bold;
color:#fff;
padding-top: 0px;
cursor:pointer;
outline:none;
font-family: 'MaisonDemi';
letter-spacing: 0.9px;
${media.medium`
  font-size: 1.6rem;
`}
${mediaCustom.medium`
  width: 80%;
`}
${mediaCustom.small`
  width: 80%;
`}
&:hover {
  background-color:#fff ;
  color:${props => props.theme.colorBlue};
  border: solid 2px #363bec;
}
`;