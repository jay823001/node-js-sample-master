import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { chooseSubscription } from 'containers/Views/Registration/actions';
import Translation from 'translation/nextory-web-se';
import styled from 'styled-components';
import Button from 'components/Buttons';
import { Redirect } from 'react-router-dom';
import { createUserRequest } from 'containers/Views/Registration/actions';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import {
  apiUrl,
  internationalization_a,
  fetchAuth,
} from 'containers/App/api';
import {
  PkgBoxWrapperParent,
  PkgBoxWrapper,
  PkgBox,
  PriceContainer,
  PriceInfo,
  PriceTitle,
  PriceInfoTitle,
  PriceButton,
  Clear,
  ButtonStyled
} from './StyledSubTable';

const b = styled.button``;
const StyledButton = Button.withComponent(b);
const StyledButtonLoading = StyledButton.extend`
  cursor: not-allowed;
  opacity: 0.6;
`;

class SubscriptionTable extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-functions
  static propTypes = {
    chooseSubscription: PropTypes.func,
    ChosenSubscription: PropTypes.string,
    createUserRequest: PropTypes.func,
    SendingRequest: PropTypes.bool,
    GiftCardAmount: PropTypes.number,
    GCPeriod: PropTypes.number,
  };

  state = {
    sticky: false,
    subscriptionData: null,
    subscription: null,
    redirectToKonto: false,
  };

  async componentDidMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }, package4: {
              packagename: subscriptions[3].subname,
              packageprice: subscriptions[3].subprice,
            }, package5: {
              packagename: subscriptions[4].subname,
              packageprice: subscriptions[4].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  setSubscriptionChoice = (subscription) => {
    this.props.chooseSubscription(subscription);
    this.props.getChosenSubscription(subscription);
  }

  render() {
    const Family2 = Translation.app.common.family2
    const Family3 = Translation.app.common.family3
    const Family4 = Translation.app.common.family4

    let amount;
    let roundDays;
    let days;

    if (this.props.GCPeriod) amount = this.props.GiftCardAmount * this.props.GCPeriod;
    function GetDays(packagePrice){
      if (Family2 || Family3 || Family4)  {
          days = amount / (packagePrice / 30);
         
          roundDays = Number(days.toFixed(0));
         
          return roundDays
      }
    };

    localStorage.setItem('roundDays', roundDays)
    localStorage.setItem('sub-state', this.props.ChosenSubscription)
    localStorage.setItem('origin-path', window.location.hash)
    localStorage.setItem('amount', amount)

    localStorage.setItem('origin-path', window.location.hash)

    return (
      <span>
        <PkgBoxWrapperParent>
          <PkgBoxWrapper>
            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 2
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                  {GetDays(199)} dagar förbetalt med presentkort
                  {/* {Translation.subscription_table.listitems.item1d} {roundDays}{' '} */}
                      {/* {Translation.subscription_table.listitems.item1e} {date} */}
                    {/* {' ' + Translation.subscription_table.family.days_free_then} */}
                    {/* <b>
                      {!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package3.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b> */}
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  {/* <PriceButtonBtn onClick={() => this.props.handleClick(Translation.app.common.family2)} to={buttonUrl} >
                    {Translation.subscription_table.family.choose} {' '} <span> 2 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn> */}


                  {this.props.SendingRequest && this.state.subscription === Translation.app.common.family2 ? (
                    <StyledButtonLoading large >
                      <LoadingIndicatorSmall />
                    </StyledButtonLoading>
                  ) : (
                    <ButtonStyled 
                        onClick={() => this.props.handleClick(Translation.app.common.family2)} >
                        {Translation.subscription_table.family.choose} {' '} <span> 2 {' ' + Translation.subscription_table.family.users} </span>
                    </ButtonStyled>
                  )}
                  
                </PriceButton>
              </PriceContainer>
            </PkgBox>

            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 3
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                  {GetDays(239)} {''} dagar förbetalt med presentkort

                    {/* {' ' + Translation.subscription_table.family.days_free_then}
                    <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package4.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b> */}
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  {/* <PriceButtonBtn onClick={() => this.props.handleClick(Translation.app.common.family3)} to={buttonUrl} >
                    {Translation.subscription_table.family.choose} {' '} <span> 3 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn> */}


                  {this.props.SendingRequest && this.state.subscription === Translation.app.common.family3? (
                    <StyledButtonLoading large>
                      <LoadingIndicatorSmall />
                    </StyledButtonLoading>
                  ) : (
                    <ButtonStyled 
                    onClick={() => this.props.handleClick(Translation.app.common.family3)} >
                    {Translation.subscription_table.family.choose} {' '} <span> 3 {' ' + Translation.subscription_table.family.users} </span>
                  </ButtonStyled>
                  )}

                </PriceButton>
              </PriceContainer>
            </PkgBox>

            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 4
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                  {GetDays(279)} dagar förbetalt med presentkort

                    {/* {' ' + Translation.subscription_table.family.days_free_then}
                    <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package5.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b> */}
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  {/* <PriceButtonBtn onClick={() => this.props.handleClick(Translation.app.common.family4)} to={buttonUrl} >
                    {Translation.subscription_table.family.choose} {' '} <span> 4 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn> */}


                  {this.props.SendingRequest && this.state.subscription === Translation.app.common.family4 ? (
                    <StyledButtonLoading large>
                      <LoadingIndicatorSmall />
                    </StyledButtonLoading>
                  ) : (
                    <ButtonStyled 
                      onClick={() => this.props.handleClick(Translation.app.common.family4)} >
                      {Translation.subscription_table.family.choose} {' '} <span> 4 {' ' + Translation.subscription_table.family.users} </span>
                    </ButtonStyled>
                   )}

                </PriceButton>
              </PriceContainer>
            </PkgBox>
            <Clear />
          </PkgBoxWrapper>
          <Clear />
          {this.state.redirectToKonto && <Redirect to={'/konto'} />}
        </PkgBoxWrapperParent>
      </span>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription: state.signup.ChosenSubscription,
    SendingRequest: state.signup.currentlySending,
    GiftCardAmount: state.signup.paymentdetails.amount,
    GCPeriod: state.signup.paymentdetails.period,
  };
}

export default connect(mapStateToProps, { chooseSubscription, createUserRequest })(SubscriptionTable);
