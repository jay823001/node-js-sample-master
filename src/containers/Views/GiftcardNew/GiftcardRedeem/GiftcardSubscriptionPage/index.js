import React from 'react';
import PropTypes from 'prop-types';
import Waypoint from 'react-waypoint';
import Cookies from 'universal-cookie';
import Center from 'components/Center';
import DataLayer from 'containers/App/datalayer';
import SubscriptionTable from './SubscriptionTable';
import Translation from 'translation/nextory-web-se';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Base64 } from 'js-base64';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getSubscriptionIdByName } from 'containers/App/common';
import FamilySubscriptionTable from './SubscriptionTable.Family';
import { createUserRequest } from './actions';
import {
  RegisterWrapper,
  H1Main,
  H2Main,
  InnerWrapper,
  Heading,
  SubcriptionStage2,
  SubcriptionStage2Heading,
  H2MainBox,
  BoxButtonPrimary,
} from './StyledSubTable';

class GiftcardCardSub extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    ChoosenSub: PropTypes.string,
    createUserRequest: PropTypes.func,
    userRedeemGiftvoucer: PropTypes.func,
    SendingRequest: PropTypes.bool,
  };

  state = {
    sticky: false,
    sticky2: false,
    subscription: null,
    showFamilyPage: false,
    datalayer: null,
    redirectToKonto: false,
  };

  handleClick = (subscription) => {
    const cookies = new Cookies()
    if (cookies.get('user') && !cookies.get('reg')) { // If the user exists and and isnt a full member yet
      this.setState({redirectToKonto: true})
    } else { // New user
      const decodedReg = JSON.parse(Base64.decode(cookies.get('reg')));
      const giftvoucher = decodedReg.campaigncode;
      const email = decodedReg.email;
      const confirmemail = decodedReg.email;
      const password = decodedReg.password;
      //const signuptype = decodedReg.signuptype;
      let subscriptionid = getSubscriptionIdByName(subscription)
      const data = {
        giftvoucher,
        password,
        email,
        confirmemail,
        subscriptionid,
      };
      this.props.createUserRequest(data);
    }
  };

  getChosenSubscription = (subscription) => {
    this.setState({ subscription })
  }

  componentDidMount () {
    this.setState({datalayer : <DataLayer />})
  }

  render() {
    const familyPackage2 = Translation.subscription_table.packages.family2;
    let subscription = (this.state.subscription === familyPackage2 || localStorage.getItem('sub-state') === familyPackage2)
    let showFamilyPage = (subscription) ? '/presentkort/subscription#familj' : '/presentkort/subscription';
    this.setState({ showFamilyPage});
    let urlToSubscriptionPage = '/presentkort/subscription'

    const SubscriptionPage = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        {/* <DataLayer /> */}
        {this.state.datalayer}
        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>Välj det abonnemang som passar dig bäst
                    {/* {Translation.giftcard.subscriptionpage.heading} */}
                  </H1Main>
                  <H2Main>Antal förbetalda dagar styrs av ditt abonnemangsval.
                    {/* {Translation.giftcard.subscriptionpage.subheading} */}
                  </H2Main>
                </SubcriptionStage2Heading>
              </Heading>

              <Waypoint
                onLeave={() => this.setState({ sticky: true })}
                onEnter={() => this.setState({ sticky: false })}
                topOffset={'0px'}
              />
            </InnerWrapper>
            <SubscriptionTable getChosenSubscription={this.getChosenSubscription} sticky={this.state.sticky} handleClick={this.handleClick}/>

          </RegisterWrapper>
        </SubcriptionStage2>

      </ReactCSSTransitionGroup>
    )

    const FamilySubscriptionPage = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>Välj det abonnemang som passar dig bäst
                    {/* {Translation.giftcard.subscriptionpage.heading} */}
                  </H1Main>
                  <H2MainBox>Du kan uppgradera och nedgradera när du vill.
                    {/* {Translation.giftcard.subscriptionpage.subheading} */}
                  </H2MainBox>
                </SubcriptionStage2Heading>
               
              </Heading>

            </InnerWrapper>
            <FamilySubscriptionTable handleClick={this.handleClick}/>
            <Center>
            <BoxButtonPrimary large to={urlToSubscriptionPage} onClick={() => this.setState({ showFamilyPage: !showFamilyPage })}>
              {Translation.registration.buttons.go_back}
            </BoxButtonPrimary>
          </Center>
          </RegisterWrapper>
        </SubcriptionStage2>

      </ReactCSSTransitionGroup>
    )

    let pageContents;
    if (this.state.showFamilyPage === '/presentkort/subscription#familj' && this.props.location.hash === '#familj') {
      pageContents = FamilySubscriptionPage
    } else {
      pageContents = SubscriptionPage
    }

    return (
      <span>
        {this.state.redirectToKonto && <Redirect to={'/konto'} />}
        {pageContents}
      </span>
    )
  }
}

function mapStateToProps(state) {
  return {
    ChoosenSub: state.signup.ChosenSubscription,
    SendingRequest: state.signup.currentlySending,
    ActiveReg: state.signup,
  };
}

export default connect(mapStateToProps, { createUserRequest })(GiftcardCardSub);
