import React from 'react';
import validate from './validate';
import PropTypes from 'prop-types';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import Button from 'components/Buttons';
// import Translation from 'translation/nextory-web-se';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { renderField } from 'components/Form/Field';
import { renderCheck } from 'components/Form/Checkbox';
import { processGiftVoucher } from './actions';
import { Submit, FormCustom, FieldCustomCheckBox, CheckText } from './StyledGiftRegForm';

const SubmitSC = Button.withComponent(Submit);

class GiftRegUserForm extends React.PureComponent {

  submit = (values, processGiftVoucher) => {
    const cookies = new Cookies()
    let email;
    let authkey;
    let subscriptionid;
    if (cookies.get('user')) {
      // const data = JSON.parse(Base64.decode(cookies.get('user')));
      email = this.props.userDetails.email
      authkey = this.props.authKey
      subscriptionid = this.props.userDetails.subscriptiondetails.subscriptionid
    }

    const data = {
      email: email ? email : values.email,
      confirmemail: email ? email : values.repeatemail,
      password: Base64.encode(values.password),
      giftvoucher: values.giftvoucher,
      subscriptionid: subscriptionid ? subscriptionid : '',
      isloggedin: this.props.userDetails.isloggedin,
      authkey: authkey ? authkey : null,
      giftcardsubmitted: true,
    };

    processGiftVoucher(data);
  }

  render() {
    const {
      handleSubmit,
      submitting,
      pristine,
      valid,
      SendingRequest,
      LoggedIn,
      processGiftVoucher
    } = this.props;

    return (
      <FormCustom>
        <Form onSubmit={handleSubmit(values => this.submit(values, processGiftVoucher))}>

          <Field
            name="giftvoucher"
            type="text"
            component={renderField}
            placeholder="Presentkod"
          //label={Translation.forms.labels.giftvoucher}
          />
          {!LoggedIn && (
            <Field
              name="email"
              type="email"
              component={renderField}
              placeholder="E-postadress"
            //label={Translation.forms.labels.email}
            />
          )}
          {!LoggedIn && (
            <Field
              name="repeatemail"
              type="text"
              component={renderField}
              placeholder="Upprepa e-postadress"
              //label={Translation.forms.labels.repeat_email}
              autoComplete="off"
              autoCorrect="off"
              spellCheck="off"
            />
          )}
          {!LoggedIn && (
            <Field
              name="password"
              type="password"
              component={renderField}
              placeholder="Lösenord"
            //label={Translation.forms.labels.password_label}
            //  label={Translation.forms.labels.password}
            />
          )}
          <FieldCustomCheckBox>
            <Field name="acceptterms" component={renderCheck} type="checkbox" />
            <CheckText> Jag godkänner Nextorys
            <a href="/medlemsvillkor"> användarvillkor </a> och
            <a href="/integritetspolicy"> integritespolicy </a>
            </CheckText>
          </FieldCustomCheckBox>

          <Center>
            {SendingRequest ? (
              <SubmitSC large type="submit" disabled={!valid || pristine || submitting}>
                <LoadingIndicatorSmall />
              </SubmitSC>
            ) : (
                <SubmitSC large type="submit" disabled={!valid || pristine || submitting}>
                  Fortsätt
            </SubmitSC>
              )}

          </Center>

        </Form>
      </FormCustom>
    );
  }
}


GiftRegUserForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  pristine: PropTypes.bool,
  valid: PropTypes.bool,
  createUserSaga: PropTypes.func,
  SendingRequest: PropTypes.bool,
  LoggedIn: PropTypes.bool,
  UserData: PropTypes.object,
  checkUserActive: PropTypes.func,
  postUserCreate: PropTypes.func,
  fetchLogin: PropTypes.func,
  fetchUserDetails: PropTypes.func,
  processGiftVoucher: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ChoosenSub: state.signup.ChosenSubscription,
    SendingRequest: state.signup.currentlySending,
    ActiveReg: state.signup,
    LoggedIn: state.account.loggedIn,
    UserData: state.account.userData,
    authKey: state.authKeys.authkey,
    userDetails: state.userDetails.userInfo
  };
}

export default withRouter(
  connect(mapStateToProps, { processGiftVoucher })(
    reduxForm({
      form: 'giftregisteruser',
      validate,
    })(GiftRegUserForm)
  )
);
