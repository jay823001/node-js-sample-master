/* eslint consistent-return:0 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { stopSubmit, startSubmit } from 'redux-form';
import { fetchLogin, postRedeemGiftvoucer, fetchUserDetails, postUserCreate } from 'containers/App/api';
import {
  LOGIN_REQUEST,
  SET_AUTH,
  REQUEST_ERROR,
  CLEAR_ERROR,
  CREATEUSER_CLEAR_ERROR,
  CREATEUSER_SETAUTH,
  REFRESH_STORE,
  CREATEUSER_ERROR
} from './constants';
import { loginRecieve } from './actions';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import Translation from 'translation/nextory-web-se';

const cookies = new Cookies();

/*
* processGiftRedeemSaga
*/

function* processGiftRedeem(data) {
  console.log(data)
  if (data.data.giftcardsubmitted) { // execute only if giftcard submissions happen
    // clear all errors to start fresh
    let errors = {};

    // start submitting form
    yield put(startSubmit('giftregisteruser'));

    // construct data to send user login request
    const userDetails = {
      data: {
        email: data.data.email,
        password: data.data.password,
        repeatemail: data.data.email,
        confirmemail: data.data.confirmemail,
        giftvoucher: data.data.giftvoucher,
        subid: data.data.subscriptionid,
        subscriptionid: data.data.subscriptionid,
        authkey: data.data.authkey,
      },
    };
    
    // call login api and check if user is new or already exists
    const userdata = yield call(fetchLogin, userDetails);

    // set giftcard redeem to true so that login saga will not rewrite its cookies
    localStorage.setItem('GiftCardRedeem', true)

    // if status is 200 the user exists so we log in user
    if (userdata.status === 200 || data.data.isloggedin) { // Existing User

      let authkey = data.data.authkey ? data.data.authkey : userdata.authkey

      // construct data to redeem giftvoucher
      const redeem = {
        authkey: authkey,
        email: data.data.email,
        giftvoucher: data.data.giftvoucher,
        password: data.data.password,
        subscriptionid: data.data.subscriptionid,
      };

      // Handle Temp user who gets created with membertypecode 100002
      if (userdata.data && userdata.data.membertypecode && userdata.data.membertypecode === 100002) {

        // re-create user by calling signup api | membertypecode 100002 can be considered as no user found
        const response = yield call(postUserCreate, data);
        if (response.status === 200) {

          // construct data for reg cookie
          const user = {
            campaigncode: data.data.giftvoucher,
            email: data.data.email,
            password: data.data.password,
            cleanpass: data.data.cleanpass,
            paymenttype: response.data.paymenttype,
            paymentdetails: response.data.paymentdetails,
          };
          const userEncode = Base64.encode(JSON.stringify(user));
          cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

          yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
          yield put({ type: CREATEUSER_CLEAR_ERROR });

          // redirect to choose sub page
          if (response.data.newuser === true) {
            yield put(push('/presentkort/subscription'));
          }
          // if user creation doesnt work not we show some errors
        } else if (response.error.code === 6000) {
          // auth fail
          yield put({ type: CREATEUSER_ERROR, error: response.error });
          errors = {
            email: Translation.api_messages.error.email1_error,
            repeatemail: Translation.api_messages.error.email1_error,
          };
        } else if (response.error.code === 101) {
          // code validation fail
          yield put({ type: CREATEUSER_ERROR, error: response.error });
          if (response.error.details.email === 'USER_NOT_FOUND') {
            errors = {
              email: Translation.api_messages.error.email2_error,
            };
          } else if (response.error.details.giftvoucher === 'INVALID_GIFTCARD_CODE') {
            errors = {
              giftvoucher: Translation.api_messages.error.giftvoucher_error,
            };
          } else {
            errors = {
              giftvoucher: Translation.api_messages.error.giftvoucher_error,
            };
          }
        } else {
          yield put({ type: CREATEUSER_ERROR, error: response.error });
        }
        yield put(stopSubmit('giftregisteruser', errors));
      } else // User exists but doesnt have membertypecode 1000002
        if ((userdata.data && !userdata.data.ismember)) { // user is a Visitor and not yet a member

          // check if giftcard is valid and redeem it if valid
          const validation = yield call(postRedeemGiftvoucer, redeem);

          if (validation.status === 200) { // Giftcard is Valid

            // Set redeem type for new user
            data.data.signuptype = 'REDEEM_GIFTCARD_NEW_USER'

            // new users or visiors do not have a payment type yet. so create mock data
            let paymentdetails = {
              amount: 0,
            }
            if (validation.data && validation.data.paymentdetails) {
              paymentdetails = validation.data.paymentdetails
            }

            // construct data for reg cookie
            const user = {
              campaigncode: data.data.giftvoucher,
              giftvoucher: data.data.giftvoucher,
              email: data.data.email,
              password: data.data.password,
              cleanpass: data.data.cleanpass,
              paymenttype: 'REDEEM_GIFTCARD',
              paymentdetails: paymentdetails,
            };
            const userEncode = Base64.encode(JSON.stringify(user));

            // set reg cookie
            cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

            yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
            //If giftvoucher is validated

            yield put({ type: CREATEUSER_CLEAR_ERROR });

            // redirect to choose sub page
            yield put(push('/presentkort/subscription'));

          } else { // Invalid Giftcard so handle errors

            if (validation.error.code === 101) {
              // code validation fail
              yield put({ type: CREATEUSER_ERROR, error: validation.error });
              if (validation.error.details.email === 'USER_NOT_FOUND') {
                errors = {
                  email: Translation.api_messages.error.email2_error,
                };
              } else if (validation.error.details.giftvoucher === 'INVALID_GIFTCARD_CODE') {
                errors = {
                  giftvoucher: Translation.api_messages.error.giftvoucher_error,
                };
              } else {
                errors = {
                  giftvoucher: Translation.api_messages.error.giftvoucher_error,
                };
              }
            }

            yield put(stopSubmit('giftregisteruser', errors));
          }

        } else { // User is and Existing member or Non Visitor

          // Set Redeem type for Existing user
          data.data.signuptype = 'REDEEM_GIFTCARD'

          // Refresh the Auth Token
          yield call(refreshAuth);

          // fetch user details
          const UserDetails = yield call(fetchUserDetails, authkey);

          // If the user is not a visitor
          if (!UserDetails.allowedactions.includes('TRAIL')) {

            // login done stop sending and send userdata to
            yield put(loginRecieve());

            // set a cookie token to preserve logged in user on page refresh
            if (!data.data.isloggedin) {
              userdata.data.email = data.data.email;
              userdata.data.p = data.data.password;
              userdata.data.authkey = authkey;
              userdata.data.refreshkey = userdata.refreshkey;
              userdata.data.UserDetails = UserDetails;
              const userEncode = Base64.encode(JSON.stringify(userdata.data));
              new Cookies().set('user', userEncode, { path: '/', maxAge: 1209600 });
            }

            // set auth to true
            yield put({ type: SET_AUTH, newAuthState: true });

            // clear all errors
            yield put({ type: CLEAR_ERROR });

            // Proceed to redeem existing users giftvoucher
            const validation = yield call(postRedeemGiftvoucer, redeem);

            // existing user
            if (validation.status === 200) {

              // if regstatus is completed, log in user
              if (validation.data.registrationstatus === 'COMPLETED') {
                // if user is not currently logged in, do so
                if (!cookies.get('user')) {
                  // user DETAILS fetch
                  const UserDetails = yield call(fetchUserDetails, authkey);
                  userdata.data.giftvoucher = data.data.giftvoucher;
                  userdata.data.email = data.data.email;
                  userdata.data.p = data.data.password;
                  userdata.data.authkey = authkey;
                  userdata.data.UserDetails = UserDetails;
                  const userEncode = Base64.encode(JSON.stringify(userdata.data));
                  new Cookies().set('user', userEncode, { path: '/', maxAge: 1209600 });

                  // set auth to true
                  yield put({ type: SET_AUTH, newAuthState: true });
                } else {
                  // if user is logged in, just refresh store
                  yield put({ type: REFRESH_STORE, authkey: authkey });
                }
                // redirect to success page
                yield put(push(`/completed?orderid=${validation.data.orderid}`));

                // if regstatus is not yet start, start it
              } else {

                const user = {
                  campaigncode: data.data.giftvoucher,
                  email: data.data.email,
                  password: data.data.password,
                  paymenttype: validation.data.paymenttype,
                  paymentdetails: validation.data.paymentdetails,
                  authkey: validation.authkey,
                };

                cookies.set('reg', Base64.encode(JSON.stringify(user)), { path: '/', maxAge: 1209600 });
                yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
                yield put({ type: CREATEUSER_CLEAR_ERROR });
                // redirect to choose sub page
                yield put(push('/presentkort/subscription'));
              }
            } else {
              if (validation.error.code === 101) {
                // code validation fail
                yield put({ type: CREATEUSER_ERROR, error: validation.error });
                if (validation.error.details.email === 'USER_NOT_FOUND') {
                  errors = {
                    email: Translation.api_messages.error.email2_error,
                  };
                } else if (validation.error.details.giftvoucher === 'INVALID_GIFTCARD_CODE') {
                  errors = {
                    giftvoucher: Translation.api_messages.error.giftvoucher_error,
                  };
                } else {
                  errors = {
                    giftvoucher: Translation.api_messages.error.giftvoucher_error,
                  };
                }
              }
              yield put(stopSubmit('giftregisteruser', errors));
            }
          } else {
            yield put({ type: REQUEST_ERROR, error: 'api error' });
            errors = { email: Translation.api_messages.error.email1_error };
          }
        }

      localStorage.setItem('GiftCardRedeem', true)
    }

    else { // User login failed
      // handle login failure

      if (userdata.error.code === 3004) {

        yield put({ type: REQUEST_ERROR, error: userdata.error.details });
        errors = { password: Translation.api_messages.error.other_region_error };

        // Handle New user
      } else if (userdata.error.code === 3001) {

        // Set Redeem type for New user
        data.data.signuptype = 'REDEEM_GIFTCARD_NEW_USER'

        yield put(startSubmit('giftregisteruser'));

        // create a registration temp user
        const response = yield call(postUserCreate, data);
        // if user creation works, start user creation
        if (response.status === 200) {
          // if regstatus is completed, log in user
          if (response.data.registrationstatus === 'COMPLETED') {
            // login active user via cookie data
            const decoded = JSON.parse(Base64.decode(cookies.get('reg')));
            localStorage.setItem('order-id', response.data.orderid)

            // api call for login
            const logindata = {
              data: {
                email: decoded.email,
                password: decoded.password,
              },
            };

            //call /login
            const userdata = yield call(fetchLogin, logindata);

            // call /userdetails
            const UserDetails = yield call(fetchUserDetails, userdata.authkey);

            // set userdetails as a cookie "user"
            userdata.data.giftvoucher = decoded.giftvoucher;
            userdata.data.email = decoded.email;
            userdata.data.p = decoded.password;
            userdata.data.authkey = userdata.authkey;
            userdata.data.refreshkey = userdata.refreshkey;
            userdata.data.UserDetails = UserDetails;
            const userEncode = Base64.encode(JSON.stringify(userdata.data));
            cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

            yield put({ type: SET_AUTH, newAuthState: true });

            // logout registration user
            cookies.remove('reg', { path: '/' });
            yield put({ type: CREATEUSER_SETAUTH, newAuthState: false });

            // clear errors and stop sending
            yield put({ type: CREATEUSER_CLEAR_ERROR });

            // redirect to success page
            yield put(push(`/completed?orderid=${response.data.orderid}`));

            // if regstatus is not yet start, start it
          } else {
            const user = {
              campaigncode: data.data.giftvoucher,
              email: data.data.email,
              password: data.data.password,
              cleanpass: data.data.cleanpass,
              paymenttype: response.data.paymenttype,
              paymentdetails: response.data.paymentdetails,
            };
            const userEncode = Base64.encode(JSON.stringify(user));
            cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

            yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
            yield put({ type: CREATEUSER_CLEAR_ERROR });

            // redirect to choose sub page
            if (response.data.newuser === true) {
              yield put(push('/presentkort/subscription'));
            }
          }

          // if user creation doesnt work not we show some errors
        } else if (response.error.code === 6000) {
          // auth fail
          yield put({ type: CREATEUSER_ERROR, error: response.error });
          errors = {
            email: Translation.api_messages.error.email1_error,
            repeatemail: Translation.api_messages.error.email1_error,
          };
        } else if (response.error.code === 101) {
          // code validation fail
          yield put({ type: CREATEUSER_ERROR, error: response.error });
          if (response.error.details.email === 'USER_NOT_FOUND') {
            errors = {
              email: Translation.api_messages.error.email2_error,
            };
          } else if (response.error.details.giftvoucher === 'INVALID_GIFTCARD_CODE') {
            errors = {
              giftvoucher: Translation.api_messages.error.giftvoucher_error,
            };
          } else {
            errors = {
              giftvoucher: Translation.api_messages.error.giftvoucher_error,
            };
          }
        } else {
          yield put({ type: CREATEUSER_ERROR, error: response.error });
        }
        localStorage.setItem('GiftCardRedeem', true)
        yield put(stopSubmit('giftregisteruser', errors));

      } else {
        // if user login fails
        yield put({ type: REQUEST_ERROR, error: userdata.error.details });

        // display errors depending on what api tells us
        if (userdata.error.details.includes('password not matching')) {
          errors = { password: Translation.api_messages.error.password_error };
        } else if (!userdata.error.details.includes('password not matching')) {
          errors = { email: Translation.api_messages.error.email2_error };
        }
      }
    }
    // stop submitting the form
    yield put(stopSubmit('login', errors));
  }
}
export function* giftloginUserSaga() {
  // export saga
  yield takeLatest(LOGIN_REQUEST, processGiftRedeem);
}