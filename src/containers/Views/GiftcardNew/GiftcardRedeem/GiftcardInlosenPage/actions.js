import { 
    USER_REDEEM_GIFT,
    CHECK_USER_ACTIVE,
    SET_AUTH,
    CREATEUSER_REQUEST,
    LOGIN_REQUEST,
    LOGIN_REQUEST_SUCCESS,
    REQUEST_ERROR,
    CLEAR_ERROR,
    REFRESH_STORE,
    CHOOSE_SUBSCRIPTION,
} from './constants';

export const userRedeemGiftvoucer = (data) => ({ type: USER_REDEEM_GIFT, data });
export const checkUserActive = (data) => ({ type: CHECK_USER_ACTIVE, data });

// account
export const setAuthState = newAuthState => ({ type: SET_AUTH, newAuthState });
export const processGiftVoucher = data => ({ type: LOGIN_REQUEST, data });
export const loginRecieve = () => ({ type: LOGIN_REQUEST_SUCCESS });
export const requestError = error => ({ type: REQUEST_ERROR, error });
export const clearError = () => ({ type: CLEAR_ERROR });
export const refreshStore = authkey => ({ type: REFRESH_STORE, authkey });

// registration
export const createUserRequest = data => ({ type: CREATEUSER_REQUEST, data });
export const chooseSubscription = subscription => ({ type: CHOOSE_SUBSCRIPTION, subscription });

