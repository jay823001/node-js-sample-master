import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import styled from 'styled-components';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import RadioList from 'components/Form/Radio';
import Button from 'components/Buttons';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import validate from './validate';
import Translation from 'translation/nextory-web-se';

const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 1.5rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
`;
const SubmitSC = Button.withComponent(Submit);

class GiftBuyForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    valid: PropTypes.bool,
    LoggedIn: PropTypes.bool,
    LoggedinData: PropTypes.object,
  };

  state = {
    loading: false,
    redirect: false,
    error: false,
  };

  componentDidMount() {
    new Cookies().remove('gift', { path: '/' });
  }

  submit = values => {
    let firstname = '';
    let lastname = '';
    let email = '';
    let signuptype = '';
    if (this.props.LoggedIn) {
      firstname = this.props.LoggedinData.firstname;
      lastname = this.props.LoggedinData.lastname;
      email = values.email;
      signuptype = 'PURCHASE_GIFTCARD';
    } else {
      firstname = values.firstname;
      lastname = values.lastname;
      email = values.email;
      signuptype = 'PURCHASE_GIFTCARD_NEW_USER';
    }

    const presentkortqty = values.giftcard;
    const serverdetails = '127.0.0.1';

    const data = {
      firstname,
      lastname,
      email,
      presentkortqty,
      serverdetails,
      signuptype,
    };
    this.setState({ loading: true });
    // this.checkAuth(data);
    console.log(data)
  };

  checkAuth = data => {
    if (!new Cookies().get('user')) {
      return this.reqGiftcard(data);
    }
    const auth = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(auth.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      this.reqGiftcard(data);
    }
  };

  reqGiftcard = async data => {
    let firstname = '';
    if (data.firstname) {
      firstname = `&firstname=${data.firstname}`;
    }
    let lastname = '';
    if (data.lastname) {
      lastname = `&lastname=${data.lastname}`;
    }
    try {
      const response = await fetch(
        `${apiUrl}usersignup?email=${data.email}${firstname}${lastname}&presentkortqty=${
        data.presentkortqty
        }&serverdetails=${data.serverdetails}&signuptype=${
        data.signuptype
        }&newsletter=true&giftvoucher=GIFTCARD_PURCHASE${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data),
        }
      );
      const regdata = await response.json();
      const authkey = await response.headers.get('nx-at');

      if (regdata.status === 200) {
        const giftcard = {
          authkey: authkey,
          orderid: regdata.data.orderid,
          value: regdata.data.paymentdetails.period,
        };
        new Cookies().set('gift', Base64.encode(JSON.stringify(giftcard)), {
          path: '/',
          maxAge: 1209600,
        });
        this.setState({ redirect: true });
      } else {
        this.setState({ error: true });
      }
    } catch (e) {
      this.setState({ error: true });
    }
    return console.log('');
  };

  render() {
    const { handleSubmit, valid } = this.props;

    return (
      <Form onSubmit={handleSubmit(values => this.submit(values))}>
        <RadioList>
          <li>
            <label htmlFor="1">
              <Field name="giftcard" component="input" type="radio" value="1" id="1" />
              <span>
                <strong>{Translation.giftcard.gift_buyform.one_month}</strong>{' '}
                {Translation.giftcard.gift_buyform.price1}
              </span>
              <div />
            </label>
          </li>
          <li>
            <label htmlFor="3">
              <Field name="giftcard" component="input" type="radio" value="3" id="3" />
              <span>
                <strong>{Translation.giftcard.gift_buyform.three_month}</strong>{' '}
                {Translation.giftcard.gift_buyform.price2}
              </span>
              <div />
            </label>
          </li>
          <li>
            <label htmlFor="6">
              <Field name="giftcard" component="input" type="radio" value="6" id="6" />
              <span>
                <strong>{Translation.giftcard.gift_buyform.six_month}</strong>{' '}
                {Translation.giftcard.gift_buyform.price3}
              </span>
              <div />
            </label>
          </li>
        </RadioList>

        {!this.props.LoggedIn && (
          <Field
            name="firstname"
            type="text"
            component={renderField}
            label={Translation.forms.labels.firstname}
          />
        )}
        {!this.props.LoggedIn && (
          <Field
            name="lastname"
            type="text"
            component={renderField}
            label={Translation.forms.labels.lastname}
          />
        )}
        {this.props.LoggedIn ? (
          <Field
            disabled
            name="email"
            type="email"
            component={renderField}
            label={Translation.forms.labels.email}
          />
        ) : (
            <Field
              name="email"
              type="email"
              component={renderField}
              label={Translation.forms.labels.email}
            />
          )}

        <Center>
          {this.state.loading ? (
            <SubmitSC large type="submit" disabled>
              <LoadingIndicatorSmall />
            </SubmitSC>
          ) : (
              <SubmitSC large type="submit" disabled={!valid}>
                {Translation.giftcard.gift_buyform.buttons.continue}
              </SubmitSC>
            )}
        </Center>

        {this.state.redirect && <Redirect to={'/presentkort/card'} />}

        {this.state.error && <Redirect to={'/hoppsan'} />}
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.userDetails.isLoggedIn,
    LoggedinData: state.userDetails.userInfo,
    ActiveReg: state.signup.activeReg,
    initialValues: {
      giftcard: '3',
      email: state.userDetails.userInfo.email,
    },
  };
}

export default withRouter(
  connect(mapStateToProps, { refreshAuth })(
    reduxForm({
      form: 'buygiftcard',
      initialValues: { enabled: true },
      validate,
    })(GiftBuyForm)
  )
);
