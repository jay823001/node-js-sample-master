import React from 'react';
import PropTypes from 'prop-types';
import GiftRegUserForm from './GiftRegUserForm';
import DataLayer from 'containers/App/datalayer';
//import Translation from 'translation/nextory-web-se';
import Loading from 'components/LoadingIndicator/page';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router-dom';
import {
  apiUrl,
  internationalization_b,
  fetchAuth
} from 'containers/App/api';
import {
  RedeemWrapper,
  FormWrapper,
  H1Main,
  H2Main
} from './StyledGiftRegForm';

class GiftcardPage extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
    RegGiftcard: PropTypes.string,
    Newuser: PropTypes.bool,
  };
  state = {
    activeRedeemForm: 0,
    seotitle: '',
    seodesc: '',
    loadingseo: true,
  };

  componentDidMount() {
    this.handleSwitchForm(1);
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}page-meta?pageUrl=/presentkort/${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          seotitle: data.data.pagemeta.metaList[0].value,
          seodesc: data.data.pagemeta.metaList[1].value,
          loadingseo: false,
        });
      }
    } catch (error) {
      this.setState({ loadingseo: false });
    }
  };

  handleSwitchForm = index => {
    this.setState({
      activeRedeemForm: index,
    });
  };

  render() {
    let dataFetched = false;
    if (!this.state.loadingseo) {
      dataFetched = true;
    }

    return dataFetched ? (
      <div>
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />
        <RedeemWrapper>
          <H1Main>Lös in din presentkod</H1Main>
          <H2Main>Fyll i dina uppgifter för att registrera dig eller logga in.</H2Main>
            <FormWrapper>
                <GiftRegUserForm  />
            </FormWrapper>
        </RedeemWrapper>

        {(this.props.RegGiftcard === 'REDEEM_GIFTCARD'  || this.props.RegGiftcard === 'REDEEM_GIFTCARD_NEW_USER') &&(
          <Redirect to={'/presentkort/subscription'} />
        )}
      </div>
    ) : (
        <Loading />
      );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    newuser: state.signup.newuser,
    RegGiftcard: state.signup.userRegPayment,
  };
}

export default connect(mapStateToProps)(GiftcardPage);
