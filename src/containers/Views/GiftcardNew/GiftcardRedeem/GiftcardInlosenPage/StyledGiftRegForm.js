import styled from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import Button from 'components/Buttons';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import A from 'components/Typography/A';
import { Link } from 'react-router-dom';

export const GiftcardWrapper = styled.section`
  padding: 3rem 1.5rem;
  ${media.medium`
    padding: 7rem;
  `};
`;

export const GiftcardInnerWrapper = styled.div`
  margin: auto;
  z-index: 10;
  max-width: 60rem;
  text-align: center;
`;

export const Heading = styled.div`
  margin-bottom: 2.4rem;
  ${media.medium`
    margin-bottom: 3.3rem;
  `};
`;

export const FormGift = styled.div`
  max-width: 50rem;
  margin: auto;
`;

export const RedeemWrapper = styled.section`
  background-color: ${props => props.theme.summerPink};
  color: white;
  padding: 9rem 1.5rem;
  text-align: center;
  ${media.medium`
    padding: 14rem 1.5rem;
  `};
`;

export const SpanButton = Button.withComponent('span');

export const RedeemButton = SpanButton.extend`
  color: white;
  margin: 2rem 1rem 0 1rem;
  &:hover {
    color: white;
  }
`;

export const FormWrapper = styled.div`
  max-width: 594px;
  margin: 30px auto 0;
  background-color: ${props => props.theme.themeWhite};
  color: ${props => props.theme.colorBlack};
  padding: 40px 59px;
  border-radius: 5px;
  padding-bottom: 20px;
    ${mediaCustom.medium`
      padding: 40px 30px;
    `};

  h2 {
    color: ${props => props.theme.colorBlack};
    font-weight: 600;
    text-align: left;
    margin: 0 0 1rem 0;
  }
`;

export const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 1.5rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
`;
export const H1Main = styled(H1)`
  font-size: 5.1rem;
  max-width: 685px;
  margin: 0 auto;
  margin-bottom: 20px;
  color:${props => props.theme.black2};
  ${mediaCustom.medium`
      font-size: 3.1rem;
      word-break: break-word;
  `};
`;
export const H2Main = styled(H2)`
  font-size: 18px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: center;
  color:${props => props.theme.black3};
`;

export const FormCustom = styled.div`

  input[type="text"], input[type="email"], input[type="password"] {
    background-color: #f6f6f6;
    border-radius: 5px;
    margin: 0px;
    margin-bottom: 18px;
    margin-top: 5px;
    font-size:18px;
    border:none;
    font-family: 'Maison';
  }
  img{
    display:none;
  }
  span{
    ${mediaCustom.medium`
      font-size:11px !important;
    `};
  }
  button{
    border-radius: 27px;
    max-width: 99%;
    padding: 1rem 2.0rem;
    font-family: 'Maison';
    text-transform: uppercase;
    font-size: 17px;
    letter-spacing: 1px;
  }
  small{
    background-color: #f6f6f6;
  }

`;

export const FieldCustomCheckBox = styled.div`
  position:relative;
  margin-bottom: 12px;
  top: -30px;
  font-family: 'Maison';
  font-size: 14px;
  ${mediaCustom.medium`
  margin-bottom: 22px;
  top: -20px;
    `};

  div{
    position: relative;
    margin: 0 auto;
    width: 100%;
    top: 8px;
    ${mediaCustom.medium`
      position: relative;
    `};
    span{
      font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif !important;
      top: 24px;
      ${mediaCustom.medium`
      top: 34px !important;
    `};
    }
  }
  label{
    position:absolute;
    left:0;
  }


`;
export const StyledLink = A.withComponent(Link);

export const textBlue = styled.span`
  color: #363bec; 
`;

export const CheckText = styled.span`
  font-size: 14px !important;
  position: relative;
  top: 15px;
  left: -17px;
  color:${props => props.theme.tableFont};

  ${mediaCustom.medium`
    top: 8px;
    left: 30px;
    max-width: 200px;
    display: block;
    text-align: left;
    padding-left: 5px;
  `};

  span{
    font-size: 12px !important;
    position: initial;
    color: #363bec; 
  }
`;