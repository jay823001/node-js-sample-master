import auth from 'containers/App/auth/auth';
import {
  CREATEUSER_CLEAR_ERROR,
  CREATEUSER_SETAUTH,
} from './constants';

// app reducer
const initialSignupState = {
  ChosenSubscription: auth.choosenSubsciption(),
  ChooseMonth: auth.chooseMonth(),
  ChooseAmount: auth.chooseAmount(),
  email: auth.userRegistration().email,
  activeReg: auth.activeRegistration(),
  paymentrequired: auth.userRegistration().paymentrequired,
  paymentdetails: auth.userRegistration().paymentdetails,
  newuser: auth.userRegistration().newuser,
  orderid: auth.userRegistration().orderid,
  userRegPayment: auth.userRegistration().paymenttype,
  userRegAuthkey: auth.userRegistration().authkey,
  userRegId: auth.userRegistration().userid,
  currentlySending: false,
  error: {
    status: '',
    data: {},
  },
};

export function SignupReducers(
  state = initialSignupState,
  { type, subscription, month, amount, newAuthState, error }
) {
  switch (type) {

    case CREATEUSER_REQUEST:
      return { ...state, currentlySending: true };

    case CREATEUSER_SETAUTH:
      return {
        ...state,
        activeReg: newAuthState,
        email: auth.userRegistration().email,
        paymentdetails: auth.userRegistration().paymentdetails,
        paymentrequired: auth.userRegistration().paymentrequired,
        newuser: auth.userRegistration().newuser,
        orderid: auth.userRegistration().orderid,
        userRegPayment: auth.userRegistration().paymenttype,
        userRegAuthkey: auth.userRegistration().authkey,
        userRegId: auth.userRegistration().userid,
      };

    case CREATEUSER_ERROR:
      return { ...state, currentlySending: false, error };

    case CREATEUSER_CLEAR_ERROR:
      return { ...state, error: {}, currentlySending: false };

    case REGPAYMENT_REQUEST:
      return { ...state, currentlySending: true };

    case REGPAYMENT_ERROR:
      return { ...state, currentlySending: false, error };

    case CREATEUSER_LOGOUT:
      localStorage.clear();
      return {
        ...state,
        ChosenSubscription: auth.choosenSubsciption(),
        //ChosenMonth: auth.chosenMonth(),
        email: '',
        activeReg: false,
        paymentrequired: '',
        newuser: '',
        orderid: '',
        userRegPayment: '',
        userRegAuthkey: '',
        userRegId: '',
      };

    default:
      return state;
  }
}
