import React from 'react';
import PropTypes from 'prop-types';
import GiftBuyForm from './GiftBuyForm';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import GiftRegUserForm from './GiftRegUserForm';
import DataLayer from 'containers/App/datalayer';
import GiftLoggedinForm from './GiftLoggedInForm';
import Translation from 'translation/nextory-web-se';
import Loading from 'components/LoadingIndicator/page';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Redirect } from 'react-router-dom';
import {
  apiUrl,
  internationalization_b,
  fetchAuth
} from 'containers/App/api';
import {
  RedeemWrapper,
  RedeemButton,
  FormWrapper,
  GiftcardWrapper,
  GiftcardInnerWrapper,
  Heading,
  FormGift
} from './StyledGiftRegForm';

class GiftcardPage extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
    RegGiftcard: PropTypes.string,
  };
  state = {
    activeRedeemForm: 0,
    seotitle: '',
    seodesc: '',
    loadingseo: true,
  };

  componentDidMount() {
    this.handleSwitchForm(1);
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}page-meta?pageUrl=/presentkort/${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      // console.log(data)
      if (data.status === 200) {
        this.setState({
          seotitle: data.data.pagemeta.metaList[0].value,
          seodesc: data.data.pagemeta.metaList[1].value,
          loadingseo: false,
        });
      }
    } catch (error) {
      this.setState({ loadingseo: false });
    }
  };

  handleSwitchForm = index => {
    this.setState({
      activeRedeemForm: index,
    });
  };

  render() {
    let dataFetched = false;
    if (!this.state.loadingseo) {
      dataFetched = true;
    }

    return dataFetched ? (
      <div>
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />
        <RedeemWrapper>
          <H1>{Translation.giftcard.redeem_wrapper.heading}</H1>

          {!this.props.LoggedIn && (
            <RedeemButton onClick={() => this.handleSwitchForm(1)} small framed>
              {Translation.giftcard.redeem_wrapper.button.not_logged_in}
            </RedeemButton>
          )}
          <RedeemButton onClick={() => this.handleSwitchForm(2)} small framed>
            {Translation.giftcard.redeem_wrapper.button.logged_in}
          </RedeemButton>

          {this.state.activeRedeemForm === 1 ? (
            <FormWrapper>
              <H2>{Translation.giftcard.redeem_wrapper.active_redeem_form.heading1}</H2>
              <GiftRegUserForm />
            </FormWrapper>
          ) : this.state.activeRedeemForm === 2 ? (
            <FormWrapper>
              <H2>{Translation.giftcard.redeem_wrapper.active_redeem_form.heading2}</H2>
              <GiftLoggedinForm />
            </FormWrapper>
          ) : null}
        </RedeemWrapper>
        <GiftcardWrapper>
          <GiftcardInnerWrapper>
            <ReactCSSTransitionGroup
              transitionAppear={true}
              transitionAppearTimeout={600}
              transitionEnterTimeout={600}
              transitionLeaveTimeout={200}
              transitionName="SlideUp"
            >
              <Heading>
                <H1>{Translation.giftcard.heading}</H1>
                <H2>{Translation.giftcard.subheading}</H2>
              </Heading>
              <FormGift>
                <GiftBuyForm />
              </FormGift>
            </ReactCSSTransitionGroup>
          </GiftcardInnerWrapper>
        </GiftcardWrapper>

        {this.props.RegGiftcard === 'REDEEM_GIFTCARD' && (
          <Redirect to={'/presentkort/subscription'} />
        )}
      </div>
    ) : (
        <Loading />
      );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    RegGiftcard: state.signup.userRegPayment,
  };
}

export default connect(mapStateToProps)(GiftcardPage);
