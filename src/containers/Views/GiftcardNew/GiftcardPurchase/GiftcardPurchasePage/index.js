import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { chooseMonth } from 'containers/Views/Registration/actions';
import { chooseAmount } from 'containers/Views/Registration/actions';
import { connect } from 'react-redux';
import Translation from 'translation/nextory-web-se';
import CheckIcon from './checkIcon.png';

import {
    PkgBoxWrapperParent,
    PkgBoxWrapper,
    PkgBox,
    PriceContainer,
    PriceInfo,
    PriceTitle,
    PriceInfoTitle,
    PriceButton,
    Clear,
    RegisterWrapper,
    H1Main,
    InnerWrapper,
    Heading,
    SubcriptionStage2,
    SubcriptionStage2Heading,
    H2MainBox,
    CheckTitle,
    CheckTitleContainer,
    PurchaseText,
    InnerWrapperd,
    BoxButtonPrimary,
    StyledButtonPurchaseContainer,

  } from './StyledSubTable';
 
    const chooseOneUser = "1 månad"
    const chooseThreeUsers = "3 månader"
    const chooseSixUsers = "6 månader"
    const chooseOneUserAmount = 199 
    const chooseThreeUsersAmount = 597 
    const chooseSixUsersAmount = 1194 

class GiftcardPurchase extends Component{
  static propTypes = {
    chooseMonth: PropTypes.func,
    ChooseMonth: PropTypes.string,
    chooseAmount: PropTypes.func,
    ChooseAmount: PropTypes.number,
    month: PropTypes.number,
    monthselect: PropTypes.number,
    order: PropTypes.number,
  };

  state = {
    sticky: false,
    subscriptionData: null,
    redirect: false,
    month: null,
    amount:  null,
  };

  getChosenMonth = (month) => {
    this.setState({ month })
  }

  getChosenAmount = (amount) => {
    this.setState({ amount })
  }

  setSubscriptionChoice = (month, amount) => {
    this.props.chooseMonth(month);
    this.getChosenMonth(month);
    this.props.chooseAmount(amount);
    this.getChosenAmount(amount);
  }
  
   render(){
    let amount;
    let urlToOrderPage = '/presentkort/yourorder'
    
    if (this.state.month || this.state.amount){
      if (this.props.ChooseMonth === chooseOneUser)  {
          amount = chooseOneUserAmount;
          } else if (this.props.ChooseMonth === chooseThreeUsers) {
            amount =  chooseThreeUsersAmount;
          } else if (this.props.ChooseMonth === chooseSixUsers) {
            amount =  chooseSixUsersAmount;
          }
          return amount
      }

    localStorage.setItem('choosen-month', this.props.ChooseMonth)

    localStorage.setItem('choosen-amount', this.props.ChooseAmount)

      return(
        <div>
        <SubcriptionStage2>
        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <SubcriptionStage2Heading>
                <H1Main>Tiotusentals presenter i en</H1Main>
                <H2MainBox>Med ett presentkort från Nextory kan du ge någon du tycker om obegränsad tillgång till läsning och lyssning bland våra tiotusentals e-böcker och ljudböcker.</H2MainBox>
              </SubcriptionStage2Heading>
            </Heading>
            <CheckTitleContainer>
                <CheckTitle> <img src={CheckIcon} alt="Check Mark"/> Läs och lyssna i offlineläge </CheckTitle>
                <CheckTitle> <img src={CheckIcon} alt="Check Mark"/> Tiotusentals e- och ljudböcker </CheckTitle>
                <CheckTitle> <img src={CheckIcon} alt="Check Mark"/> Obegränsad lyssning </CheckTitle>
            </CheckTitleContainer>
          </InnerWrapper>

          <span>
          <PkgBoxWrapperParent>
            <PkgBoxWrapper>

              <PkgBox>
                <PriceContainer>
                  <PriceInfo>
                    <PriceTitle> {chooseOneUser}</PriceTitle>
                    <PriceInfoTitle>{chooseOneUserAmount} {''} kr</PriceInfoTitle>
                  </PriceInfo>
                  <PriceButton>
                    <BoxButtonPrimary to = {urlToOrderPage}
                      onClick={() => this.setSubscriptionChoice(chooseOneUser, chooseOneUserAmount)}
                      >
                      {Translation.subscription_table.family.choose} {' '} <span> 1 {' '} månad </span>
                    </BoxButtonPrimary>
  
                  </PriceButton>
                </PriceContainer>
              </PkgBox>

              <PkgBox>
                <PriceContainer>
                  <PriceInfo>
                    <PriceTitle> {chooseThreeUsers}</PriceTitle>
                    <PriceInfoTitle>{chooseThreeUsersAmount} {''} kr</PriceInfoTitle>
                  </PriceInfo>
                  <PriceButton>
                    <BoxButtonPrimary to = {urlToOrderPage}
                      onClick={() => this.setSubscriptionChoice(chooseThreeUsers, chooseThreeUsersAmount)}
                      >
                      {Translation.subscription_table.family.choose} {' '} <span> 3 {' '} månader </span>
                    </BoxButtonPrimary>
  
                 {/*   )} */}  
                  </PriceButton>
                </PriceContainer>
              </PkgBox>

            <PkgBox>
                <PriceContainer>
                  <PriceInfo>
                    <PriceTitle> {chooseSixUsers}</PriceTitle>
                    <PriceInfoTitle>{chooseSixUsersAmount} {''} kr</PriceInfoTitle>
                  </PriceInfo>
                  <PriceButton>
                    <BoxButtonPrimary to = {urlToOrderPage}
                      onClick={() => this.setSubscriptionChoice(chooseSixUsers, chooseSixUsersAmount)}
                      >
                      {Translation.subscription_table.family.choose} {' '} <span> 6 {' '} månader </span>
                    </BoxButtonPrimary>
  
                 {/*   )} */}  
                  </PriceButton>
                </PriceContainer>
              </PkgBox>
  
            <Clear />
        </PkgBoxWrapper>
            <Clear />
          </PkgBoxWrapperParent>
        </span>

        <PurchaseText>
        Är du från ett företag och vill köpa fler än 10 presentkort? Maila <a href="mailto:partner@nextory.se">partner@nextory.se</a> så ordnar vi en bra deal.
        </PurchaseText>

         <InnerWrapperd>
           <StyledButtonPurchaseContainer />
        </InnerWrapperd>

        </RegisterWrapper>
      </SubcriptionStage2>

    </div>
          
    );
   }
}

function mapStateToProps(state) {
  return {
    ChooseMonth: state.signup.ChooseMonth,
    ChooseAmount: state.signup.ChooseAmount
  };
}

export default connect(mapStateToProps, { chooseMonth, chooseAmount })(GiftcardPurchase);
