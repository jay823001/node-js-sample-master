import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import Button from 'components/Buttons';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';


//  Index page style

export const RegisterWrapper = styled.section`
  padding: 3rem 0 1rem;

  ${media.medium`
    padding: 15rem 0 1rem;
  `};
  ${mediaCustom.medium`
    padding: 10rem 0 0rem;
  `};
`;
export const H1Main = styled(H1)`
  font-size: 5.1rem;
  max-width: 685px;
  margin: 0 auto;
  margin-bottom: 20px;
  ${mediaCustom.medium`
      font-size: 3.1rem;
  `};
`;
export const H2Main = styled(H2)`
      font-size: 1.9rem;
`;
export const ButtonMain = styled(Button)`
  width: 245px;
  height: 54px;
  border-radius: 27px;
  text-transform: uppercase;
  font-size: 14px;
  padding: 15px 0px;
  font-weight: bold;
  ${mediaCustom.medium`
      width: 290px;
  `};
`;

export const ButtonPrimary = styled(Button)`
  border-radius: 27px;
  max-width: 30rem;
  text-transform: uppercase;
  font-size: 15px;
  font-weight: bold;
  ${mediaCustom.medium`
  font-size: 12px;
`};
`;
export const TableButtons = styled.div`
    button{
      border-radius: 27px;
      max-width: 30rem;
      text-transform: uppercase;
      font-size: 15px;
      font-weight: bold;
      ${mediaCustom.medium`
      font-size: 12px;
    `};
    }

`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 93rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
      margin-bottom: 2.9rem;
  `};
`;

export const Span = styled.span`
  display: block;
  padding-top: 10px;
  font-size: 1.4rem;
  margin: 0 0 0.8rem;
  ${media.medium`
    font-size: 1.6rem;
    margin: 20px 0 1.4rem;
  `};
`;

export const InnerWrapperc = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;

`;

export const SubcriptionC = styled.div`
  padding-top: 8rem;
  padding-right: 3rem;
  padding-left: 3rem;
  padding-bottom: 0rem;
  background:#eaf0e2;
  text-align:center;

  ${media.medium`
    padding-top: 1rem;
  `};

  img {
    margin-bottom: 0.8rem;
  }
`;

export const SubcriptionCInfo = styled.div`
  font-size: 1.6rem;
  margin-bottom: 0rem;
  padding-bottom: 15px;
  ${media.medium`
    margin-bottom: 2.5rem;
    margin-top: 2.5rem;
    padding-bottom: 0px;
  `};
`;
export const ButtonWhiteWrapper = styled.section`
    text-align:center;
    background:${props => props.theme.greenBg};
    padding-bottom:70px;
    ${mediaCustom.medium`
    padding-bottom: 0px;
    height: 120px;
    padding-top: 35px;

    `};
`;

export const SubcriptionStage2 = styled.section`
      background: ${props => props.theme.summerPink};
`;

export const SubcriptionStage2Heading = styled.div`
      text-align:center;
`;

export const SubcriptionStage2Bottom = styled.section`
      background: #fff;
      padding: 20px;
      min-height: 375px;
      padding-top: 250px;

      ${props =>
    props.sticky2 &&
    css`
        position: fixed;
        bottom: 0px;
        width: 100%;
      `};

      ${mediaCustom.medium`
        padding: 20px;
        min-height: auto;
        padding-top: 25px
    `};
  `;

export const H2MainBox = styled(H2)`
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    text-align: center;
    color: #555555;
    margin-top: 25px;
    max-width: 660px;
    margin: 0 auto;
    font-family: 'Maison';
`;

export const BoxButtonPrimary = styled(Button)`
    width: 160px;
    height: 54px;
    border-radius: 27px;
    border: solid 2px #363bec;
    -webkit-text-transform: uppercase;
    text-transform: uppercase;
    background-color: #ffffff;
    font-size: 15px;
    box-shadow: none;
    color: #363bec;
    font-weight: bold;
    font-family: 'MaisonDemi';
    letter-spacing: 1px;
    &:hover{
      color:#fff;
    }
    ${mediaCustom.medium`
    font-size: 13px;
    width: 95%;
    height: 54px
    `};
`;
export const InnerWrapperd = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;
  ${mediaCustom.medium`
    padding:0px;
    width: 65%;
  `};
  ${mediaCustom.small`
    padding:0px;
    width: 100%;
  `};
`;

export const SubcriptionStage2BoxBottom = styled.section`
    background: #fff;
    padding: 20px;
    min-height: 250px;
    padding-top: 150px;

    ${mediaCustom.medium`
      padding: 20px;
      min-height: auto;
      padding-top: 25px
  `};
  `;

export const PkgBoxContainer = styled.section`
  position: relative;
  height: 100px;
  margin-top: 20px;
  ${mediaCustom.medium`
  position: unset;
`};
`;

// SubscriptionTable family Styles
export const StyledButton = styled(Button)`
  width: 80%;
  height: 50px;
  border-radius: 26px;
  border: solid 2px #363bec;
  background-color: #fff;
  box-shadow: none;
  -webkit-text-align: center;
  -webkit-text-align: center;
  text-align: center;
  padding: 0px;
  margin: 0px;
  text-transform: uppercase;
  font-weight: bold;
  color:${props => props.theme.colorBlue};

  ${media.medium`
    font-size: 1.6rem;
  `}
  ${mediaCustom.medium`
    width: 80%;
  `}
  ${mediaCustom.small`
    width: 80%;
  `}
  &:hover {
    background-color:${props => props.theme.colorBlue};
    color:#fff;
  }
  ${props => props.active && css`
    background-color:${props => props.theme.colorBlue};
    color:#fff;
    &:after {
      box-shadow: none;
      position: absolute;
      bottom: -1.5rem;
      left: 50%;
      display: inline-block;
      box-sizing: border-box;
      width: 0;
      height: 0;
      margin-left: -1.1rem;
      content: "";
      transform: rotate(-45deg);
      transform-origin: 0 0;
      opacity: 1;
      border: .8rem solid ${props => props.theme.colorBlue};
      border-color: transparent transparent ${props => props.theme.colorBlue} ${props => props.theme.colorBlue};
      ${media.medium`
        display:none;
      `}
    }
  `}
`;

export const PkgBoxWrapperParent = styled.section`
//background-color: #ffffff;
position: relative;
${mediaCustom.medium`
 // height:300px;
`};
`;
export const PkgBoxWrapper = styled.section`
  position: initial;
  left:0;
  right:0;
  max-width: 93rem;
  margin: auto;
  bottom: -6em;

  ${mediaCustom.medium`
  //position: relative;
  //bottom: 90px;
  `};
`;

export const PkgBox = styled.div`
  width: 30%;
  height: 205px;
  border-radius: 5px;
  background-color: #ffffff;
  float: left;
  margin: 10px;
  //box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.08), 0 12px 24px 2px rgba(234, 243, 245, 0.25);

  ${mediaCustom.medium`
      float: none;
      width: 96%;
      margin: 0 auto;
      margin-bottom: 7px;
      height: 118px;
  `};
`;
export const PriceContainer = styled.div`
  padding: 40px;
  ${mediaCustom.medium`
    padding: 20px !important;
  `};
`;
export const PriceInfo = styled.div`
    ${mediaCustom.medium`
      float:left;
    `};
  `;
export const PriceButton = styled.div`
  ${mediaCustom.medium`
    float:right;
  `};
  a{
    padding: 12px 5px;
    width: 100%;
    height: 45px;
    border-radius: 27px;
    color: #fff;
    background-color: #353bec;
    border: none;
    margin-top: 28px;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: bold;
    cursor:pointer;
    text-align: center;
    &:hover {
      background-color: #373ae0;
    }
    ${mediaCustom.medium`
      width:94px !important;
      margin-top: 5px !important;
      span{
        display:none;
      }
    `};
  }
`;
export const PriceTitle = styled.h2`
  font-size: 25px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.2;
  letter-spacing: -0.2px;
  color: #444444;
  margin: 0px;
  font-weight: bold;
  margin-bottom: 5px;
  font-family: 'MaisonDemi';
  ${mediaCustom.medium`
    font-size: 21px !important;
  `};
`;
export const PriceInfoTitle = styled.p`
  font-size: 18px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: #555555;
  margin:0px;
  max-width: 185px;
  font-family: 'Maison';
  ${mediaCustom.medium`
    max-width: 155px !important;
    font-size: 14px !important;
  `};
`;
export const PriceButtonBtn = styled(StyledButton)`
  padding: 14px 5px;
  width: 100%;
  height: 45px;
  border-radius: 27px;
  color: #fff;
  background-color: #353bec;
  border: none;
  margin-top: 15px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: bold;
  cursor:pointer;
  &:hover {
    background-color: #373ae0;
  }
  ${mediaCustom.medium`
    width:94px !important;
    span{
      display:none;
    }
  `};
`;

export const Clear = styled.div`
  clear:both;
 `;
export const CheckTitleContainer = styled.div`
  text-align: center;
  padding-bottom: 15px;
  font-family: 'Maison';
  ${mediaCustom.medium`
    text-align: left;
  `};
`;

export const CheckTitle = styled.span`
  margin: 0px 15px;
  font-size: 18px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color:${props => props.theme.seccondGrey};

  ${mediaCustom.medium`
  display: block;
  margin: 0px 0px 10px 0px;
  `};
`;
export const PurchaseText = styled.div`
  font-size: 14px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.71;
  letter-spacing: normal;
  text-align: center;
  color: ${props => props.theme.seccondGrey};
  margin-top: 16px;
  padding: 0px 15px;
  span{
    color: ${props => props.theme.buttonSecconBlue};
   }
`;

export const StyledButtonPurchase = styled.button`
width: 165px;
height: 54px;
border-radius: 27px;
border: solid 2px #e0e0e0;
&:hover{
  color: #fff;
  background-color: #353bec;
}
cursor:pointer;
outline:none;
text-transform: uppercase;
font-size: 15px;
box-shadow: none;
color: #555555;
font-weight: bold;
`;
export const BoxButtonPrimaryBack = styled(Button)`
width: 165px;
height: 54px;
border-radius: 27px;
border: solid 2px #e0e0e0;
background: transparent;
padding-top: 14px;
&:hover{
  color: #fff;
  background-color: #353bec;
}
cursor:pointer;
outline:none;
text-transform: uppercase;
font-size: 15px;
box-shadow: none;
color: #555555;
font-weight: bold;
font-family: 'MaisonDemi';
letter-spacing: 1px;
`;


export const StyledButtonPurchaseContainer = styled.div`
  text-align:center;
  padding: 30px 10px;
`;