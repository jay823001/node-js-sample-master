import React from 'react';
import PropTypes from 'prop-types';
import validate from './validate';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import Button from 'components/Buttons';
// import Translation from 'translation/nextory-web-se';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { parseJwt } from 'utils/helpFunctions';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import { connect } from 'react-redux';
import { Redirect, withRouter } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { renderField } from 'components/Form/Field';
import { Submit, FormCustom, CustomField, CustomField2, Clear} from './StyledGiftRegForm';

const SubmitSC = Button.withComponent(Submit);

const cookies = new Cookies()

class GiftRegUserForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool,
    valid: PropTypes.bool,
    createUserRequest: PropTypes.func,
    SendingRequest: PropTypes.bool,
    LoggedIn: PropTypes.bool,
    ChoosenSub: PropTypes.string,
    amountSelect: PropTypes.string,
    monthSelect: PropTypes.string,
    LoggedinData: PropTypes.object,
  };

  state = {
    loading: false,
    redirect: false,
    error: false,
  };

  componentDidMount() {
    cookies.remove('gift', { path: '/' });
  }

  submit = values => {
    let firstname = '';
    let lastname = '';
    let email = '';
    let signuptype = '';
    if (this.props.LoggedIn) {
      // temporarily log out user to proceed with allowing any email to be entered here, and log them in after thank you page
      // above is a dirty fix and proper way is to use redux, but given the tight timeframe, product agreed for a worakaround as such
      cookies.set('tempuser', cookies.get('user'), { path: '/', maxAge: 1209600 });

      cookies.remove('user', { path: '/' });
      signuptype = 'PURCHASE_GIFTCARD';
    } else {
      signuptype = 'PURCHASE_GIFTCARD_NEW_USER';
    }
      firstname = values.firstname;
      lastname = values.lastname;
      email = values.email;

    let chosenMonth = JSON.parse(localStorage.getItem('choosen-month'));
    let presentkortqty;
    if (chosenMonth === "1 månad") {
      presentkortqty = 1
    }  if (chosenMonth === "3 månader") {
      presentkortqty = 3
    } else if (chosenMonth === "6 månader") {
      presentkortqty = 6
    }

    localStorage.setItem('email', email)
    const serverdetails = '127.0.0.1';

    const data = {
      firstname,
      lastname,
      email,
      serverdetails,
      presentkortqty,
      signuptype,
    };
    this.setState({ loading: true });
    this.checkAuth(data);
  };

  checkAuth = data => {
    if (!cookies.get('user')) {
      return this.reqGiftcard(data);
    }
    const auth = JSON.parse(Base64.decode(cookies.get('user')));
    const jwt = parseJwt(auth.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      this.reqGiftcard(data);
    }
  };

  reqGiftcard = async data => {
    let firstname = '';
    if (data.firstname) {
      firstname = `&firstname=${data.firstname}`;
    }
    let lastname = '';
    if (data.lastname) {
      lastname = `&lastname=${data.lastname}`;
    }

    try {
      const response = await fetch(
        `${apiUrl}usersignup?email=${data.email}${firstname}${lastname}&presentkortqty=${
        data.presentkortqty
        }&serverdetails=${data.serverdetails}&signuptype=${
        data.signuptype
        }&newsletter=true&giftvoucher=GIFTCARD_PURCHASE${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data),
        }
      );
      
      const regdata = await response.json();
      const authkey = await response.headers.get('nx-at');

      if (regdata.status === 200) {
        const AmountCount = {
          authkey: authkey,
          orderid: regdata.data.orderid,
          value: regdata.data.paymentdetails.period,
        };
        cookies.set('gift', Base64.encode(JSON.stringify(AmountCount)), {
          path: '/',
          maxAge: 1209600,
        });
        this.setState({ redirect: true });
      } else {
        this.setState({ error: true });
      }
    } catch (e) {
      this.setState({ error: true });
    }
    return console.log('');
  };

  render() {
    const {
      handleSubmit,
      valid
    } = this.props;

    return (
      <FormCustom> 
      <Form onSubmit={handleSubmit(values => this.submit(values))}>
      
        <CustomField> 
          <Field
            name="firstname"
            type="text"
            component={renderField}
            placeholder="Förnamn"
          />
        </CustomField>
        
        
        <CustomField2> 
          <Field
            name="lastname"
            type="text"
            component={renderField}
            placeholder="Efternamn"
          />
        </CustomField2> 
        
        <Clear/>
        <Field
          name="email"
          type="email"
          component={renderField}
          placeholder="E-postadress"
          autoComplete="off"
          autoCorrect="off"
          spellCheck="off"
        />

         <Center>
          {this.state.loading ? (
            <SubmitSC large type="submit" disabled>
              <LoadingIndicatorSmall />
            </SubmitSC>
          ) : (
            <SubmitSC large type="submit" disabled={!valid}>
                GÄ VIDARE
            </SubmitSC>
              )}
        </Center>

        {this.state.redirect && <Redirect to={'/presentkort/card'} />}

          {this.state.error && <Redirect to={'/hoppsan'} />}
      </Form>
      </FormCustom>
    );
  }
}

function mapStateToProps(state) {
  return {
    //ChoosenSub: state.signup.ChosenSubscription,
    LoggedinData: state.account.userData.UserDetails,
    //SendingRequest: state.signup.currentlySending,
    ActiveReg: state.signup.activeReg,
    LoggedIn: state.account.loggedIn,
  };
}

export default withRouter(
  connect(mapStateToProps, { refreshAuth })(
    reduxForm({
      form: 'giftregisteruser',
      validate,
    })(GiftRegUserForm)
  )
);
