import styled from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import Button from 'components/Buttons';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import A from 'components/Typography/A';
import { Link } from 'react-router-dom';

export const GiftcardWrapper = styled.section`
  padding: 3rem 1.5rem;
  ${media.medium`
    padding: 7rem;
  `};
`;

export const GiftcardInnerWrapper = styled.div`
  margin: auto;
  z-index: 10;
  max-width: 60rem;
  text-align: center;
`;

export const Heading = styled.div`
  margin-bottom: 2.4rem;
  ${media.medium`
    margin-bottom: 3.3rem;
  `};
`;

export const FormGift = styled.div`
  max-width: 50rem;
  margin: auto;
`;

export const RedeemWrapper = styled.section`
  background-color: ${props => props.theme.greenBg};
  color: white;
  padding: 9rem 1.5rem;
  text-align: center;
  ${media.medium`
    padding: 14rem 1.5rem;
  `};
`;

export const SpanButton = Button.withComponent('span');

export const RedeemButton = SpanButton.extend`
  color: white;
  margin: 2rem 1rem 0 1rem;
  &:hover {
    color: white;
  }
`;

export const FormWrapper = styled.div`
  max-width: 594px;
  margin: 30px auto 0;
  background-color: ${props => props.theme.themeWhite};
  color: ${props => props.theme.colorBlack};
  padding: 20px 59px;
  border-radius: 5px;
    ${mediaCustom.medium`
      padding: 40px 30px;
    `};

  h2 {
    color: ${props => props.theme.colorBlack};
    font-weight: 600;
    text-align: left;
    margin: 0 0 1rem 0;
    font-family: 'Maison';
  }
  text-align: left;
`;

export const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 1.5rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
`;
export const H1Main = styled(H1)`
  font-size: 5.1rem;
  max-width: 685px;
  margin: 0 auto;
  margin-bottom: 20px;
  color:${props => props.theme.black2};
  ${mediaCustom.medium`
      font-size: 3.1rem;
      word-break: break-word;
  `};
`;
export const H2Main = styled(H2)`
  font-size: 18px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: center;
  color:${props => props.theme.black3};
`;

export const FormCustom = styled.div`

  input[type="text"], input[type="email"], input[type="password"] {
    background-color: #f6f6f6;
    border-radius: 5px;
    margin: 0px;
    margin-bottom: 18px;
    margin-top: 5px;
    font-size:16px;
    border:none;
    font-family: 'Maison';
    font-size: 18px;
  }
  img{
    display:none;
  }
  span{
    font-size:11px;
  }
  button{
    border-radius: 27px;
    max-width: 99%;
    padding: 1rem 2.0rem;
    font-family: 'Maison';
    font-size: 16px;
    font-weight: bold;
    letter-spacing: 3px;
  }
  small{
    background-color: #f6f6f6;
  }

`;

export const FieldCustomCheckBox = styled.div`
  position:relative;
  margin-bottom: 25px;
  div{
    position:absolute;
    top: -7px;
    ${mediaCustom.medium`
      position: relative;
    `};
  }

`;
export const StyledLink = A.withComponent(Link);

export const textBlue = styled.span`
  color: #363bec; 
`;

export const CheckText = styled.span`
  font-size: 12px !important;
  position: relative;
  top: 15px;
  left: -44px;

  ${mediaCustom.medium`
    top: 8px;
    left: 30px;
    max-width: 200px;
    display: block;
    text-align: left;
    padding-left: 5px;
  `};

  span{
    font-size: 12px !important;
    position: initial;
    color: #363bec; 
  }
`;

export const Clear = styled.div`
  clear:both !important;
`;

export const CustomField = styled.div`

width: 50%;
float: left;
padding-right: 5px;
${mediaCustom.medium`
  width: 100%;
  float: none;
  padding-left: 0px;
`};
`;

export const CustomField2 = styled.div`
width: 50%;
float: right;
padding-left: 5px;
${mediaCustom.medium`
  width: 100%;
  float: none;
  padding-left: 0px;
`};
`;

export const PriceWrapper = styled.div`
 p{
  text-align: left;
  font-size: 19px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.06;
  letter-spacing: normal;
  color: #585858;
  margin-bottom: 7px;
  font-family: 'Maison';
 }
 h2{
  font-size: 25px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.2;
  letter-spacing: -0.2px;
  color: #444444;
  font-weight: bold;
  font-family: 'Maison';
 }
`;

export const InfoBox1 = styled.div`
width: 50%;
float: left;
padding-right: 5px;
${mediaCustom.medium`
  width: 100%;
  float: none;
  padding-left: 0px;
`};
`;

export const InfoBox2 = styled.div`
width: 50%;
float: right;
padding-left: 5px;
${mediaCustom.medium`
  width: 100%;
  float: none;
  padding-left: 0px;
`};
`;

export const ChangeLink = styled(Button)`
font-size: 18px;
color: #353bec;
font-weight: bold;
margin: 6px 0px 10px 0px;
width: 100px;
display: block;
font-family: 'Maison';
background: none;
border: none;
box-shadow: none;
padding: 0;
&:hover{
  background: none;
}
`;