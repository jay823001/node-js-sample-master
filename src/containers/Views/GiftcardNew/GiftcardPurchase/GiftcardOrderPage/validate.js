import Translation from 'translation/nextory-web-se';

const validate = values => {
  const errors = {};

  if (!values.firstname) {
    errors.firstname = Translation.forms.validation.firstname;
  }

  if (!values.lastname) {
    errors.lastname = Translation.forms.validation.lastname;
  }

  if (!values.email) {
    errors.email = Translation.forms.validation.email;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(values.email)) {
    errors.email = Translation.forms.validation.email;
  }

  if (!values.acceptterms) {
    errors.acceptterms = Translation.forms.validation.termserror;
  }

  return errors;
};

export default validate;
