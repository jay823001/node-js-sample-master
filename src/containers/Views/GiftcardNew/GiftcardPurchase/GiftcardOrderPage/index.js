import React from 'react';
import PropTypes from 'prop-types';
import YourOrderForm from './GiftRegUserForm';
import {
  RedeemWrapper,
  FormWrapper,
  H1Main,
  H2Main,
  PriceWrapper,
  InfoBox1,
  InfoBox2,
  ChangeLink,
  Clear
} from './StyledGiftRegForm';

class GiftcardYourorder extends React.PureComponent {
  static propTypes = {
    amountSelect: PropTypes.string,
    monthSelect: PropTypes.string,
  };

  state = {
    sticky: false,
    subscriptionData: null,
    redirect: false
  };

  render() {
    let AmountCount = JSON.parse(localStorage.getItem('choosen-month'));
    let monthName = localStorage.getItem('choosen-amount');
    
    return  (
      <div>
        <RedeemWrapper>
          <H1Main>Din order</H1Main>
          <H2Main>Välj antal presentkort och fyll i dina uppgifter.</H2Main>
            <FormWrapper>
              <PriceWrapper>
                <InfoBox1>
                  <p> Presentkort </p>
                  <h2> {AmountCount}</h2>
                </InfoBox1>
                <InfoBox2>
                  <p>Pris </p>
                  <h2>{monthName}  kr</h2>
                </InfoBox2>

              </PriceWrapper>
              <Clear/>
                <ChangeLink to="/presentkort/purchase">ÄNDRA</ChangeLink>
              <YourOrderForm />
            </FormWrapper>
        </RedeemWrapper>
       
      </div>
    )
  }
}

export default GiftcardYourorder;
