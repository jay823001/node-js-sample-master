import React, { Component } from 'react';
import Cookies from 'universal-cookie';

import {
    RegisterWrapper,
    H1Main,
    InnerWrapper,
    Heading,
    SubcriptionStage2,
    SubcriptionStage2Heading,
    H2MainBox,
    BoxButtonPrimaryBack
  } from './StyledThankyou';

const cookies = new Cookies()

class App extends Component{
  submit () {
    if (cookies.get('tempuser')) {
      cookies.set('user', cookies.get('tempuser'), { path: '/', maxAge: 1209600 })
      cookies.remove('tempuser', { path: '/' });
      // temporary hack to force the application to read the user cookie values. This should be done using redux
      window.location.reload()
    }
  }
   render(){
    let urlTOHome = '/'
    let email = localStorage.getItem('email');
      return(
        <SubcriptionStage2>
        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <SubcriptionStage2Heading>
                <H1Main>Skickat & klart! </H1Main>
                <H2MainBox> Din beställning är nu skickad till {email} </H2MainBox>
              </SubcriptionStage2Heading>
            </Heading> 
          </InnerWrapper>

          <BoxButtonPrimaryBack to={urlTOHome} onClick={() => this.submit ()}> tillbaka till hemsidan </BoxButtonPrimaryBack>

        </RegisterWrapper>
      </SubcriptionStage2>
      );
   }
}
export default App;