import styled from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import Button from 'components/Buttons';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';


export const RegisterWrapper = styled.section`
  padding: 3rem 0 1rem;

  ${media.medium`
    padding: 15rem 0 1rem;
  `};
  ${mediaCustom.medium`
    padding: 10rem 0 0rem;
  `};
`;
export const H1Main = styled(H1)`
  font-size: 5.1rem;
  max-width: 685px;
  margin: 0 auto;
  margin-bottom: 20px;
  ${mediaCustom.medium`
      font-size: 3.1rem;
  `};
`;
export const H2Main = styled(H2)`
    font-size: 1.9rem;
`;

export const ButtonPrimary = styled.a`
display: block;
width: 384px;
max-width:384px;
height: 54px;
padding: 15px 0px;
border-radius: 27px;
color: ${props => props.theme.themeWhite}; 
background-color: ${props => props.theme.buttonSecconBlue};
border: none;
margin-top: 28px;
text-transform: uppercase;
font-size: 16px;
font-weight: bold;
cursor: pointer;
text-align: center;
font-family: 'MaisonDemi';
letter-spacing: 1px;
margin: 0 auto;
${mediaCustom.medium`
font-size: 13px;
width: 85%;
height: 48px;
padding-top: 14px;
`};
`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 93rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
      margin-bottom: 2.9rem;
  `};
`;

export const Span = styled.span`
  display: block;
  padding-top: 10px;
  font-size: 1.4rem;
  margin: 0 0 0.8rem;
  ${media.medium`
    font-size: 1.6rem;
    margin: 20px 0 1.4rem;
  `};
`;

export const InnerWrapperc = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;

`;

export const SubcriptionStage2 = styled.section`
    background: ${props => props.theme.summerWhite};
    &:after{
    content: '';
    display: block;
    height: 38rem;
    background: ${props => props.theme.summerWhite};
    }
    ${media.medium`
    height: 38.5rem;
    `};
`;

export const SubcriptionStage2Heading = styled.div`
      text-align:center;
`;
 
export const H2MainBox = styled(H2)`
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    text-align: center;
    color: ${props => props.theme.black3};
    margin-top: 25px;
    max-width: 660px;
    margin: 0 auto;
    font-family: 'Maison';
`;

export const BoxButtonPrimaryBack = styled(Button)`
display: block;
width: 384px;
max-width:384px;
height: 54px;
padding: 15px 0px;
border-radius: 27px;
color: ${props => props.theme.themeWhite}; 
background-color: ${props => props.theme.buttonSecconBlue};
border: none;
margin-top: 28px;
text-transform: uppercase;
font-size: 16px;
font-weight: bold;
cursor: pointer;
text-align: center;
font-family: 'MaisonDemi';
letter-spacing: 1px;
margin: 0 auto;
${mediaCustom.medium`
font-size: 13px;
width: 85%;
height: 48px;
padding-top: 14px;
`};
`;

