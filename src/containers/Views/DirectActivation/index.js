import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import { Base64 } from 'js-base64';
import Cookies from 'universal-cookie';
import { gup } from 'utils/helpFunctions';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import LoadingIndicator from 'components/LoadingIndicator';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import P from 'components/Typography/P';
import Button from 'components/Buttons';
import Center from 'components/Center';
import A from 'components/Typography/A';
import Disclaimer from 'components/Form/Disclaimer';
import Translation from 'translation/nextory-web-se';

const RegisterWrapper = styled.section`
  padding: 3rem 0 8rem;
  ${media.medium`
    padding: 7rem 0 12rem;
  `};
`;

const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 50rem;
`;

const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
    margin-bottom: 2.5rem;
  `};
`;

const SButton = styled.button`
  margin-top: 3rem;
  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed !important;
      opacity: 0.6;
    `};
`;
const StyledButton = Button.withComponent(SButton);

export const StyledLink = A.withComponent(Link);

class DirectActivation extends React.PureComponent {
  static propTypes = {
    routeSearch: PropTypes.string,
  };

  state = {
    paymentdetails: {},
    campaigndetails: {},
    validlink: true,
    process: false,
    subname: '',
    submitting: false,
    error: false,
    loading: true,
    validCode: true,
  };

  componentDidMount() {
    const uid = gup('uid', this.props.routeSearch);
    this.fetchActivationDetails(uid);
  }

  fetchActivationDetails = async uid => {
    try {
      const response = await fetch(`${apiUrl}userdirectactivation?uuid=${uid}&processType=INFO${internationalization_b}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(data), // eslint-disable-line
      });
      const data = await response.json();
      // console.log(data);

      if (data.status === 200) {
        this.setState({
          campaigndetails: data.data.campaigndetails,
          paymentdetails: data.data.paymentdetails,
          loading: false,
        });
        const store = {
          email: data.data.useremail,
          password: data.data.password,
          campaignname: data.data.campaigndetails.campaignname,
          campaigncode: data.data.campaigndetails.vouchercode,
          amount: data.data.paymentdetails.amount,
          discounted: data.data.campaigndetails.discounted,
          sub: data.data.paymentdetails.subscriptionname,
          campaignprice: data.data.campaigndetails.campaignprice,
          campaignintervel: data.data.campaigndetails.intervel,
          campaignintervelindays: data.data.campaigndetails.intervelindays,
          campaignperiod: data.data.campaigndetails.period,
          campaigndays: data.data.campaigndetails.trialdays,
          newuser: data.data.newuser,
          paymentrequired: data.data.paymentrequired,
          paymenttype: data.data.paymenttype,
        };
        new Cookies().set('reg', Base64.encode(JSON.stringify(store)), {
          path: '/',
          maxAge: 1209600,
        });
      } else if (data.status === 400) {
        this.setState({ validlink: false, loading: false });
        if (data.error.code === 4000 || data.error.code === 101)
          this.setState({ validCode: false, loading: false });
      } else if (data.status === 500) {
        this.setState({ error: true, loading: false });
      }
    } catch (error) {
      console.log(error);
    }
  };

  submit = async () => {
    this.setState({ submitting: true });
    const uid = gup('uid', this.props.routeSearch);
    try {
      const response = await fetch(
        `${apiUrl}userdirectactivation?uuid=${uid}&processType=PROCESS${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data), // eslint-disable-line
        }
      );
      const data = await response.json();

      if (data.status === 200) {
        this.setState({
          submitting: false,
          process: true,
          subname: data.data.paymentdetails.subscriptionname,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    let amount = '';
    let period = '';
    let intervel = '';
    let discounted = '% rabatt';
    if (Object.keys(this.state.campaigndetails).length > 1) {
      if (!this.state.campaigndetails.discounted && this.state.campaigndetails.period <= 1) {
        discounted = 'kr';
      } else if (!this.state.campaigndetails.discounted && this.state.campaigndetails.period > 1) {
        discounted = 'kr/månad';
      }
      if (this.state.campaigndetails.period > 1) {
        amount = `${this.state.campaigndetails.campaignprice}${discounted}`;
      } else {
        amount = `${this.state.campaigndetails.campaignprice}${discounted}`;
      }
      intervel = this.state.campaigndetails.intervel;
      period = this.state.campaigndetails.period;
      if (period > 1 && intervel === 'månad') {
        intervel = 'månader';
      }
    }

    let submitbutton = (
      <StyledButton large onClick={this.submit}>
        Starta kampanjperioden
      </StyledButton>
    );
    if (this.state.submitting) {
      submitbutton = (
        <StyledButton large disabled>
          <LoadingIndicatorSmall />
        </StyledButton>
      );
    }

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        {!this.state.loading ? (
          <RegisterWrapper>
            {!this.state.error &&
              (!this.state.validlink ? (!this.state.validCode ? (
                <InnerWrapper>
                  <Heading>
                    <H1>{Translation.direct_activation.invalid_code.heading}</H1>
                    <H2>{Translation.direct_activation.invalid_code.subheading}</H2>
                  </Heading>
                  <P>
                    {Translation.direct_activation.existing_member.p1}{' '}
                    <StyledLink to="/bocker/nyheter/">
                      {Translation.direct_activation.links.latest_titles}
                    </StyledLink>{' '}
                    {Translation.direct_activation.existing_member.p2}
                  </P>
                </InnerWrapper>
              ) : (
                  <InnerWrapper>
                    <Heading>
                      <H1>{Translation.direct_activation.existing_member.heading}</H1>
                      <H2>{Translation.direct_activation.existing_member.subheading}</H2>
                    </Heading>
                    <P>
                      {Translation.direct_activation.existing_member.p1}{' '}
                      <StyledLink to="/bocker/nyheter/">
                        {Translation.direct_activation.links.latest_titles}
                      </StyledLink>{' '}
                      {Translation.direct_activation.existing_member.p2}
                    </P>
                  </InnerWrapper>
                )) : (
                  <InnerWrapper>
                    <Heading>
                      <H1>
                        {Translation.direct_activation.new_member.heading_p1} {amount} i {period}{' '}
                        {intervel}
                      </H1>
                      <H2>
                        {Translation.direct_activation.new_member.subheading_p1} {'"'}
                        {Translation.direct_activation.new_member.subheading_p2}
                        {'"'} {Translation.direct_activation.new_member.subheading_p3}
                      </H2>
                    </Heading>
                    <Center>
                      {submitbutton}
                      <Disclaimer>
                        {Translation.direct_activation.new_member.disclaimer.p1} {'"'}
                        {Translation.direct_activation.new_member.disclaimer.p2}
                        {'"'} {Translation.direct_activation.new_member.disclaimer.p3}{' '}
                        <StyledLink to="/medlemsvillkor">
                          {Translation.direct_activation.links.terms}
                        </StyledLink>{' '}
                        {Translation.direct_activation.new_member.disclaimer.p4}
                      </Disclaimer>
                    </Center>
                  </InnerWrapper>
                ))}

            {this.state.error && (
              <InnerWrapper>
                <Heading>
                  <H1>{Translation.direct_activation.error} </H1>
                </Heading>
              </InnerWrapper>
            )}
          </RegisterWrapper>
        ) : (
            <LoadingIndicator />
          )}

        {this.state.process && <Redirect to={`/register-campaign/card?da#${this.state.subname}`} />}
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    routeSearch: state.route.location.search,
  };
}

export default connect(mapStateToProps)(DirectActivation);
