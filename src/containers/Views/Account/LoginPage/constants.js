// login/logout constants
export const SET_AUTH = 'SET_AUTH';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_REQUEST_SUCCESS = 'LOGIN_REQUEST_SUCCESS';
export const REQUEST_ERROR = 'REQUEST_ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const CREATEUSER_LOGOUT = 'CREATEUSER_LOGOUT';
export const LOGIN_CHECK = 'LOGIN_CHECK';
