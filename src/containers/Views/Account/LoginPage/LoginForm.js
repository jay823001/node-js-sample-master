import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Base64 } from 'js-base64';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import Center from 'components/Center';
import A from 'components/Typography/A';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import validate from './validate';
import { loginRequest } from './actions';
import Translation from 'translation/nextory-web-se';

const StyledLink = A.withComponent(Link);
const Submit = styled.button`
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;
const SubmitSC = Button.withComponent(Submit);
const PreSubmit = StyledLink.extend`
  font-size: 1.2rem;
  margin: 1rem 0 1.5rem 0;
  display: block;
  ${media.medium`
    font-size: 1.4rem;
  `};
`;

class LoginForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    loginRequest: PropTypes.func,
    SendingRequest: PropTypes.bool,
    valid: PropTypes.bool,
    LoggedIn: PropTypes.bool,
  };

  // take inputed values and pass them to our redux saga loginRequest
  submit = (values, loginRequest) => {
    const email = values.email;
    const password = Base64.encode(values.password);
    const data = {
      password,
      email,
    };
    loginRequest(data);
  };

  render() {
    const { handleSubmit, loginRequest, valid, SendingRequest } = this.props;
    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, loginRequest))}>
        <Field
          name="email"
          type="text"
          component={renderField}
          label={Translation.forms.labels.email}
        />
        <Field
          name="password"
          type="password"
          component={renderField}
          label={Translation.forms.labels.password}
        />
        <Center>
          <PreSubmit to="/glomt-losenord">
            {Translation.account.loginpage.loginform.presubmit}
          </PreSubmit>

          {SendingRequest ? (
            <SubmitSC large type="submit" disabled={true}>
              <LoadingIndicatorSmall />
            </SubmitSC>
          ) : (
            <SubmitSC large type="submit" disabled={!valid}>
              {Translation.account.loginpage.loginform.login}
            </SubmitSC>
          )}
        </Center>

        {this.props.LoggedIn && <Redirect to={'/konto'} />}
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    SendingRequest: state.account.currentlySending,
  };
}

export default withRouter(
  connect(mapStateToProps, { loginRequest })(
    reduxForm({
      form: 'login',
      validate,
    })(LoginForm)
  )
);
