import Translation from 'translation/nextory-web-se';

const validate = values => {
  const errors = {};

  if (!values.email) {
    errors.email = Translation.forms.validation.email;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(values.email)) {
    errors.email = Translation.forms.validation.email;
  }

  if (!values.password) {
    errors.password = Translation.forms.validation.password;
  } else if (values.password.length < 4 || values.password.length > 25) {
    errors.password = Translation.forms.validation.password_length;
  } else if (!/^[a-zA-Z0-9-@#$%_]+$/.test(values.password)) {
    errors.password = Translation.forms.validation.password_chars;
  }

  return errors;
};

export default validate;
