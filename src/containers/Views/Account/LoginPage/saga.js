/* eslint consistent-return:0 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { stopSubmit, startSubmit } from 'redux-form';
import { push } from 'react-router-redux';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { fetchLogin, fetchUserDetails } from 'containers/App/api';
import {
  LOGIN_REQUEST,
  SET_AUTH,
  REQUEST_ERROR,
  CLEAR_ERROR,
  CREATEUSER_LOGOUT,
} from './constants';
import { CREATEUSER_SETAUTH } from 'containers/Views/Registration/constants';
import { loginRecieve } from './actions';
import Translation from 'translation/nextory-web-se';

// new ui actiontypes
import { REGISTRATION_SUCCESS } from 'newui/domain/Modules/User/actionTypes'

const cookies = new Cookies();

/*
* LoginSaga: will be fired on LOGIN_REQUEST actions
*/
function* loginUser(data) {
  // clear all errors to start fresh
  let errors = {};

  if (!localStorage.getItem('GiftCardRedeem')) {
    // make sure we dont have a current registration active
    yield put({ type: CREATEUSER_LOGOUT });
    cookies.remove('reg', { path: '/' });
    cookies.remove('trustly', { path: '/' });
    cookies.remove('retry', { path: '/' });
    cookies.remove('gift', { path: '/' });
  }

  // start submitting form
  yield put(startSubmit('login'));

  // api call for login
  const logindata = {
    data: {
      authkey: (data.data.authkey ? data.data.authkey : null),
      email: data.data.email,
      password: data.data.password,
    },
  };

  const userdata = yield call(fetchLogin, logindata);
  // set authkeys for the use of new flow
  const registrationData = {
    authkey: userdata.authkey,
    refreshkey: userdata.refreshkey
  }
  yield put({ type: REGISTRATION_SUCCESS, registrationData })
  //=====================================
  // if status is 200, we log in user, if not its falsy and we return error
  if (userdata.status === 200) {

    // user DETAILS fetch
    const UserDetails = yield call(fetchUserDetails, userdata.authkey);
    // if user is visitor we start a trial registration process
    if (UserDetails.allowedactions.includes('TRAIL') && !localStorage.getItem('GiftCardRedeem')) {
      const reg = {
        campaignname: 'Trail',
        paymenttype: 'TRAIL',
        authkey: userdata.authkey,
        userid: data.data.userid,
        email: data.data.email,
        password: data.data.hashedpass,
      };
      const userEncode = Base64.encode(JSON.stringify(reg));
      cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });
      yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
      // redirect to choose sub

      if (!data.data.giftvoucher) { // Do not redirect if user was logged in from giftcard
        // if the user is not from the b flow
        if (!window.location.pathname.includes('/b')) {
          yield put(push('/register/subscription#steg-1'));
        }
      } else {
        yield put({ type: SET_AUTH, newAuthState: true });
      }
    } else if (!UserDetails.allowedactions.includes('TRAIL')) {
      // login done stop sending and send userdata to
      yield put(loginRecieve());
      // set a cookie token to preserve logged in user on page refresh
      userdata.data.email = data.data.email;
      userdata.data.p = data.data.password;
      userdata.data.authkey = userdata.authkey;
      userdata.data.refreshkey = userdata.refreshkey;
      userdata.data.UserDetails = UserDetails;
      const userEncode = Base64.encode(JSON.stringify(userdata.data));
      new Cookies().set('user', userEncode, { path: '/', maxAge: 1209600 });

      // set auth to true
      yield put({ type: SET_AUTH, newAuthState: true });

      // clear all errors
      yield put({ type: CLEAR_ERROR });

      // refresh browser in order to clear network tab
    } else {
      yield put({ type: REQUEST_ERROR, error: 'api error' });
      errors = { email: Translation.api_messages.error.email1_error };
    }
  } else {
    if (userdata.error.code === 3004) {

      yield put({ type: REQUEST_ERROR, error: userdata.error.details });
      errors = { password: Translation.api_messages.error.other_region_error };

    } else {
      // if user login fails
      yield put({ type: REQUEST_ERROR, error: userdata.error.details });

      // display errors depending on what api tells us
      if (userdata.error.details.includes('password not matching')) {
        errors = { password: Translation.api_messages.error.password_error };
      } else if (!userdata.error.details.includes('password not matching')) {
        errors = { email: Translation.api_messages.error.email2_error };
      }

    }

  }

  // stop submitting the form
  yield put(stopSubmit('login', errors));
}
export function* loginUserSaga() {
  // export saga
  yield takeLatest(LOGIN_REQUEST, loginUser);
}
