import React from 'react';
import { Link } from 'react-router-dom';
import DataLayer from 'containers/App/datalayer';
import media from 'theme/styled-utils';
import H1 from 'components/Typography/H1';
import A from 'components/Typography/A';
import Disclaimer from 'components/Form/Disclaimer';
import AccountWrapper from '../AccountWrapper';
import LoginForm from './LoginForm';
import Translation from 'translation/nextory-web-se';

const H1Styled = H1.extend`
  margin-bottom: 1.5rem;
  ${media.medium`
    margin-bottom: 2rem;
  `};
`;
const StyledLink = A.withComponent(Link);

const LoginPage = () => (
  <AccountWrapper>
    <DataLayer />
    <H1Styled>{Translation.account.loginpage.heading}</H1Styled>
    <LoginForm />
    <Disclaimer>
      {Translation.account.loginpage.disclaimer}{' '}
      <StyledLink to="/register/subscription#steg-1">
        {Translation.account.loginpage.register_now}
      </StyledLink>
    </Disclaimer>
  </AccountWrapper>
);

export default LoginPage;
