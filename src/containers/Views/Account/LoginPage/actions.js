// login/logout actions
import {
  SET_AUTH,
  LOGIN_REQUEST,
  LOGIN_REQUEST_SUCCESS,
  REQUEST_ERROR,
  CLEAR_ERROR,
  LOGIN_CHECK,
} from './constants';

// Account actions
export const setAuthState = newAuthState => ({ type: SET_AUTH, newAuthState });

export const loginRequest = data => ({ type: LOGIN_REQUEST, data });
export const loginRecieve = () => ({ type: LOGIN_REQUEST_SUCCESS });

export const requestError = error => ({ type: REQUEST_ERROR, error });
export const clearError = () => ({ type: CLEAR_ERROR });


export const loginCheck = userdata => ({ type: LOGIN_CHECK, userdata });

