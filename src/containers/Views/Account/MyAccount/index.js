import React from 'react';
import Orders from './orders';
import Collabs from './collabs';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import P from 'components/Typography/P';
import AccountForm from './AccountForm';
import DataLayer from 'containers/App/datalayer';
import Translation from 'translation/nextory-web-se';
import AfterUncleaned from '../ChangeSubscription/ThankyouPage';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Base64 } from 'js-base64';
import { connect } from 'react-redux';
import { parseJwt } from 'utils/helpFunctions';
import { ToastContainer } from 'react-toastify';
import { isFamilyPackage } from 'containers/App/common';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import {
  refreshStore,
  logoutRequest
} from './actions';
import {
  apiUrl,
  fetchAuth,
  internationalization_a,
  internationalization_b
} from 'containers/App/api';
import {
  notifySubChanged,
  notifyCardUpdated,
  notifySubActivated,
  notifyCampaignCode,
  notifycancelDowngrade
} from 'containers/App/notifications';
import {
  Box,
  H2styled,
  ButtonStyl,
  BoxWrapper,
  ButtonStyled,
  InnerWrapper,
  LogoutButton,
  LogoutWrapper,
  AccountWrapper,
  CancelButtonStyl
} from './StyledMyAccount';

class MyAccount extends React.PureComponent {
  static propTypes = {
    logoutRequest: PropTypes.func,
    userData: PropTypes.object,
    routeSearch: PropTypes.string,
  };

  state = {
    loading: false,
    uncancelled: false,
  };

  componentDidMount() {
    localStorage.removeItem('SwitchBoardSetSub');
    this.notifications();
  }

  clickLogout = () => {
    this.props.logoutRequest(this.props.userData.authkey);
  };

  notifications = () => {
    if (this.props.routeSearch.includes('updated-card')) {
      notifyCardUpdated();
    } else if (this.props.routeSearch.includes('subscription-changed')) {
      notifySubChanged();
    } else if (this.props.routeSearch.includes('subscription-activated')) {
      notifySubActivated();
    } else if (this.props.routeSearch.includes('campaigncode-added')) {
      notifyCampaignCode();
    }
  };

  clickActivateAccount = () => {
    this.checkAuthActivate();
    this.setState({ loading: true });
  };

  checkAuthActivate = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.activateAccount();
    }
  };

  activateAccount = async () => {
    try {
      const response = await fetch(
        `${apiUrl}usersignup?email=${this.props.userData.UserDetails.email}&confirmemail=${
        this.props.userData.UserDetails.email
        }&password=${this.props.userData.p}&subscriptionid=${
        this.props.userData.subscriptionid
        }&serverdetails=127.0.0.1&signuptype=PURCHASE_SUBSCRIPTION${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'nx-at': this.props.userData.authkey,
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data), // eslint-disable-line
        }
      );
      const data = await response.json();
      if (data.status === 200) {
        this.props.refreshStore(this.props.userData.authkey);
        //notifySubActivated();
        this.setState({
          loading: false,
          uncancelled: true,
        });
      }
    } catch (error) {
      console.log(error);
    }
    // return console.log('success');
  };

  clickCancelDowngrade = () => {
    this.checkAuthCancel();
    this.setState({ loading: true });
  };

  checkAuthCancel = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.cancelDowngrade();
    }
  };

  cancelDowngrade = async () => {
    try {
      const response = await fetch(`${apiUrl}cancelsubscriptiondowngrade${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          'nx-at': this.props.userData.authkey,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.props.refreshStore(this.props.userData.authkey);
        notifycancelDowngrade();
        this.setState({ loading: false });
      }
      // return console.log('success');
    } catch (error) {
      return error;
    }
  };

  render() {
    const {
      email,
      subscriptiondetails,
      allowedactions,
      canceldowngrade,
      nexsubscriptiondetails,
    } = this.props.userData.UserDetails;

    let userStatus = 'payingMember';
    if (typeof allowedactions !== 'undefined') {
      if (allowedactions.includes('REVERT_CANCEL_MEMBERSHIP')) {
        userStatus = 'canceledMember';
      } else if (canceldowngrade === true) {
        userStatus = 'downgradingMember';
      } else if (allowedactions.includes('ACTIVATE_AGAIN')) {
        userStatus = 'oldMember';
      }
    }

    const isTele2User = this.props.userData.account && this.props.userData.account === 'tele2' // check if the user is a tele2 user

    let subscription = subscriptiondetails.subscriptionplanename
    let peopleText = isFamilyPackage(subscription) ? Translation.account.my_account.headings.people : null

    let nextSubscription = nexsubscriptiondetails && nexsubscriptiondetails.subscriptionplanename ? nexsubscriptiondetails.subscriptionplanename : null
    let peopleText_Next = nextSubscription && isFamilyPackage(nextSubscription) ? Translation.account.my_account.headings.people : null

    /*eslint ignore no-extend-native: "error"*/
    Number.prototype.countDecimals = function () {
      if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
      return this.toString().split(".")[1].length || 0;
    }

    let price = parseFloat(subscriptiondetails.price)
    price = (price.countDecimals() > 2) ? (Math.floor(price * 100) / 100) : price // limit decimals to 2 only if they exceed 2
    let getDecimalCorrrectedPrice = Number.isInteger(price) ? price : +price.toFixed(2)

    let getDecimalCorrrectednextPrice
    let nextPrice = nexsubscriptiondetails && nexsubscriptiondetails.price ?  nexsubscriptiondetails.price : null
    if (nextPrice){
      nextPrice = (nextPrice.countDecimals() > 2) ? (Math.floor(nextPrice * 100) / 100) : nextPrice
      getDecimalCorrrectednextPrice = Number.isInteger(nextPrice) ? nextPrice : +nextPrice.toFixed(2)
    }

    const compMyAccount = (
      <AccountWrapper>
        <InnerWrapper>
          <ReactCSSTransitionGroup
            transitionAppear={true}
            transitionAppearTimeout={600}
            transitionEnterTimeout={600}
            transitionLeaveTimeout={200}
            transitionName="FadeIn"
          >
            <DataLayer />
            <BoxWrapper>
              <Box>
                <H2styled>{Translation.account.my_account.headings.my_details}</H2styled>
                <AccountForm UserDetails={this.props.userData.UserDetails} />
              </Box>
            </BoxWrapper>

            <BoxWrapper>
              {!isTele2User && userStatus === 'downgradingMember' && (
                <Box>
                  <H2styled>
                    {Translation.account.my_account.headings.you_have_subscription}{' '}
                    {subscriptiondetails.subscriptionplanename} {' '} {peopleText} {' '}
                  </H2styled>
                  <P>
                    {Translation.account.my_account.content.p1}, {subscriptiondetails.nextrundate},{' '}
                    {Translation.account.my_account.content.p2}{' '}
                    <strong>
                      {nexsubscriptiondetails.subscriptionplanename}  {' '} {peopleText_Next} {' '} {getDecimalCorrrectednextPrice}{' '}
                      {Translation.account.my_account.content.currency}
                    </strong>
                  </P>

                  {this.state.loading ? (
                    <CancelButtonStyl small>
                      <LoadingIndicatorSmall />
                    </CancelButtonStyl>
                  ) : (
                      <CancelButtonStyl onClick={this.clickCancelDowngrade} small>
                        {Translation.account.my_account.buttons.cancel_downgrade}
                      </CancelButtonStyl>
                    )}

                  <ButtonStyl small to="/konto/andra-betalning">
                    {Translation.account.my_account.buttons.change_payment_details}
                  </ButtonStyl>
                </Box>
              )}
              {!isTele2User && userStatus === 'canceledMember' && (
                <Box>
                  <H2styled>
                    {subscriptiondetails.subscriptionplanename} {' '} {peopleText} {' '}
                    {Translation.account.my_account.headings.account_has_ended}
                  </H2styled>
                  <P>
                    {Translation.account.my_account.content.p3}{' '}
                    <strong>{subscriptiondetails.nextrundate}</strong>
                  </P>
                  {this.state.loading ? (
                    <CancelButtonStyl small>
                      <LoadingIndicatorSmall />
                    </CancelButtonStyl>
                  ) : (
                      <CancelButtonStyl onClick={this.clickActivateAccount} small>
                        {Translation.account.my_account.buttons.enable_your_account_again}
                      </CancelButtonStyl>
                    )}
                  <ButtonStyl small to="/konto/andra-betalning">
                    {Translation.account.my_account.buttons.change_payment_details}
                  </ButtonStyl>
                </Box>
              )}
              {!isTele2User && userStatus === 'oldMember' && (
                <Box>
                  <H2styled>
                    {' '}
                    {Translation.account.my_account.headings.enable_your_account_again}
                  </H2styled>
                  <ButtonStyl small to="/konto/aktivera-abonnemang">
                    {Translation.account.my_account.buttons.enable_your_account_again}
                  </ButtonStyl>
                </Box>
              )}
              {!isTele2User && userStatus === 'payingMember' && (
                <Box>
                  <H2styled>
                    {Translation.account.my_account.headings.you_have_subscription} :{' '}
                    {email && (
                      <span>
                        {' '}
                        {subscriptiondetails.subscriptionplanename}{' '} {peopleText} {' '}
                        {getDecimalCorrrectedPrice} {' '}
                        {Translation.account.my_account.content.currency_per_month}
                      </span>
                    )}
                  </H2styled>
                  <P>
                    {Translation.account.my_account.content.p4}{' '}
                    {email && <strong>{subscriptiondetails.nextrundate}</strong>}
                    {this.props.userData.UserDetails.systemEvent === 'PARKED' &&
                      this.props.userData.UserDetails.userEvent === 'CANCELLED' && (
                        <span>
                          {' '}
                          {subscriptiondetails.subscriptionplanename} {' '} {peopleText} {' '}
                          {' '} {getDecimalCorrrectedPrice} {' '}
                          {Translation.account.my_account.content.currency_per_month}
                        </span>
                      )}
                  </P>
                  <ButtonStyled small to="/konto/andra-abonnemang">
                    {Translation.account.my_account.buttons.change_subscription}
                  </ButtonStyled>
                  <ButtonStyled small to="/konto/andra-betalning">
                    {Translation.account.my_account.buttons.change_payment_details}
                  </ButtonStyled>
                </Box>
              )}

              {userStatus !== 'oldMember' && <Collabs />}

              {!isTele2User && <Orders />}

              {!isTele2User && userStatus !== 'canceledMember' &&
                userStatus !== 'oldMember' && (
                  <Box>
                    <H2styled> {Translation.account.my_account.headings.end_subscriptions}</H2styled>
                    <ButtonStyl small to="/konto/avsluta">
                      {Translation.account.my_account.buttons.end_subscriptions}
                    </ButtonStyl>
                  </Box>
                )}

              <LogoutWrapper>
                <LogoutButton onClick={this.clickLogout} small red to="/">
                  {Translation.account.my_account.buttons.logout}
                </LogoutButton>
              </LogoutWrapper>
            </BoxWrapper>
          </ReactCSSTransitionGroup>
        </InnerWrapper>

        <ToastContainer autoClose={5000} />
      </AccountWrapper>
    );

    if (this.state.uncancelled === false) {
      return (compMyAccount);
    } else {
      return (<AfterUncleaned />);
    }
  }
}

function mapStateToProps(state) {
  return {
    userData: state.account.userData,
    routeSearch: state.route.location.search,
    sendingData: state.account.currentlySending,
  };
}

export default connect(mapStateToProps, { logoutRequest, refreshStore, refreshAuth })(MyAccount);
