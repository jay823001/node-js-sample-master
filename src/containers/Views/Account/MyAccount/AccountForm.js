import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Base64 } from 'js-base64';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import styled, { css } from 'styled-components';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import { validate, warn } from './validate';
import Phonenumber from './phonevalidate';
import Postalnumber from './postalvalidate';
import { updateProfile } from '../actions';
import Translation from 'translation/nextory-web-se';

const Submit = styled.button`
  margin-top: 1rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed !important;
      opacity: 0.6;
    `};
`;
const SubmitSC = Button.withComponent(Submit);

class MyaccountForm extends React.PureComponent {
  static propTypes = {
    authKey: PropTypes.string,
    handleSubmit: PropTypes.func,
    updateProfile: PropTypes.func,
    Sending: PropTypes.bool,
    Password: PropTypes.string,
  };

  // take inputed values and pass them to our redux saga updateProfile
  submit = (values, updateProfile) => {
    const authtoken = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(authtoken.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    }

    let password = '';
    if (values.password) {
      password = Base64.encode(values.password);
    }
    const email = values.email;
    const firstname = values.firstname;
    const lastname = values.lastname;
    const address = values.address;
    const postcode = values.postcode;
    const city = values.city;
    const cellphone = values.cellphone;
    const newsletter = values.newsletter;
    const auth = this.props.authKey;
    const data = {
      password,
      email,
      firstname,
      lastname,
      address,
      postcode,
      city,
      cellphone,
      newsletter,
      auth,
    };
    updateProfile(data);
  };

  render() {
    const { handleSubmit, updateProfile } = this.props;
    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, updateProfile))}>
        <Field
          name="email"
          type="text"
          component={renderField}
          label={Translation.forms.labels.email}
        />
        <Field
          name="password"
          type="password"
          component={renderField}
          label={Translation.forms.labels.password}
          placeholder="*******"
        />
        <Field
          name="firstname"
          type="text"
          component={renderField}
          label={Translation.forms.labels.firstname}
        />
        <Field
          name="lastname"
          type="text"
          component={renderField}
          label={Translation.forms.labels.lastname}
        />
        <Field
          name="address"
          type="text"
          component={renderField}
          label={Translation.forms.labels.address}
        />
        <Field
          name="postcode"
          type="text"
          component={renderField}
          label={Translation.forms.labels.postalcode}
          normalize={Postalnumber}
        />
        <Field
          name="city"
          type="text"
          component={renderField}
          label={Translation.forms.labels.city}
        />
        <Field
          name="cellphone"
          type="text"
          component={renderField}
          label={Translation.forms.labels.phone}
          normalize={Phonenumber}
        />

        {this.props.Sending ? (
          <SubmitSC small disabled type="submit">
            <LoadingIndicatorSmall />
          </SubmitSC>
        ) : (
          <SubmitSC small type="submit">
            {Translation.account.my_account.accountform.save_data}
          </SubmitSC>
        )}
      </Form>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    authKey: state.account.userData.authkey,
    Sending: state.account.currentlySending,
    Password: state.account.userData.p,
    initialValues: {
      email: ownProps.UserDetails.email,
      firstname: ownProps.UserDetails.firstname,
      lastname: ownProps.UserDetails.lastname,
      address: ownProps.UserDetails.address,
      postcode: ownProps.UserDetails.postcode,
      city: ownProps.UserDetails.city,
      cellphone: ownProps.UserDetails.cellphone,
      newsletter: ownProps.UserDetails.newsletter,
    },
  };
}

export default connect(mapStateToProps, { updateProfile, refreshAuth })(
  reduxForm({
    form: 'myaccount',
    validate,
    warn,
    enableReinitialize: true,
  })(MyaccountForm)
);
