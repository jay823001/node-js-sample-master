// logout actions
import { LOGOUT, REFRESH_STORE, REFRESH_AUTH } from './constants';

export const logoutRequest = authkey => ({ type: LOGOUT, authkey });
export const refreshStore = authkey => ({ type: REFRESH_STORE, authkey });
export const refreshAuth = () => ({ type: REFRESH_AUTH });
