import styled from 'styled-components';
import media from 'theme/styled-utils';
import H2 from 'components/Typography/H2';
import Button from 'components/Buttons';

export const AccountWrapper = styled.section`
  padding: 3rem 1.5rem 8rem;
  background-color: ${props => props.theme.colorGrey};
  ${media.medium`
    padding: 7rem 1.5rem 12rem;
  `};
`;
export const InnerWrapper = styled.div`
  max-width: 120rem;
  margin: auto;
`;
export const BoxWrapper = styled.div`
  ${media.large`
    width: 50%;
    margin: 0 2%;
    display: inline-block;
    vertical-align: top;
    &:nth-of-type(1) {
      width: 40%;
    }
  `};
`;
export const Box = styled.div`
  border: 0.1rem solid ${props => props.theme.colorGrey};
  background-color: ${props => props.theme.colorWhite};
  border-radius: 0.5rem;
  padding: 2rem;
  width: 100%;
  margin: 0 0 2rem;
  ${media.medium`
    margin: 0 0 4rem;
    padding: 2rem 2.7rem 2rem 2rem;
  `} ul {
    list-style: none;
    padding: 0 0 0.6rem;
    margin: 0 0 0.6rem;
    border-bottom: 0.1rem solid ${props => props.theme.colorGrey};
    li {
      ${media.medium`
        margin: 0 2rem 0 0;
        display: inline-block;
      `};
    }
  }
`;
export const H2styled = H2.extend`
  font-weight: 600;
  margin: 0 0 1rem 0;
`;
export const LogoutWrapper = styled.div`
  text-align: center;
  margin-top: 4rem;
`;
export const LogoutButton = Button.withComponent('span');
const CancelButton = Button.withComponent('span');
export const CancelButtonStyl = CancelButton.extend`
  margin: 1.5rem 0 0 0;
  ${media.medium`
    &+a {
      margin: 1.5rem 0 0 2rem;
    }
  `};
`;
export const ButtonStyl = Button.extend`
  margin: 1.5rem 0 0 0;
  ${media.medium`
    &+a {
      margin: 1.5rem 0 0 2rem;
    }
  `};
`;
export const ButtonStyled = Button.extend`
margin: 1.5rem 15px 0px 0px;
`;