import Translation from 'translation/nextory-web-se';

export const validate = values => {
  const errors = {};

  if (!values.email) {
    errors.email = Translation.forms.validation.email;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(values.email)) {
    errors.email = Translation.forms.validation.email;
  }

  if (values.password !== undefined) {
    if (values.password.length < 4 || values.password.length > 25) {
      errors.password = Translation.forms.validation.password_length;
    } else if (!/^[a-zA-Z0-9-@#$%_]+$/.test(values.password)) {
      errors.password = Translation.forms.validation.password_chars;
    }
  }

  if (values.cellphone) {
    if (values.cellphone.length < 7 || values.cellphone.length > 12) {
      errors.cellphone = Translation.forms.validation.phone;
    }
  }

  return errors;
};

export const warn = values => {
  const warnings = {};
  if (!values.firstname) {
    warnings.firstname = Translation.forms.validation.warnings.firstname;
  }
  if (!values.lastname) {
    warnings.lastname = Translation.forms.validation.warnings.lastname;
  }
  if (!values.address) {
    warnings.address = Translation.forms.validation.warnings.address;
  }
  if (!values.postcode) {
    warnings.postcode = Translation.forms.validation.warnings.postalcode;
  }
  if (!values.city) {
    warnings.city = Translation.forms.validation.warnings.city;
  }
  if (!values.cellphone) {
    warnings.cellphone = Translation.forms.validation.warnings.phone;
  }

  return warnings;
};
