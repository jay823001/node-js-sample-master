import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { Base64 } from 'js-base64';
import Cookies from 'universal-cookie';
import { fetchUserDetails, logoutAPI, refreshAuthkey } from 'containers/App/api';
import { SET_AUTH, CLEAR_ERROR } from '../LoginPage/constants';
import { CHOOSE_SUBSCRIPTION } from 'containers/Views/Registration/constants';
import { LOGOUT, REFRESH_STORE, REFRESH_AUTH } from './constants';
import auth from 'containers/App/auth/auth';
import { persistor } from "store";

const cookies = new Cookies();

/*
* Refresh store SAGA
*/
function* refresh(authkey) {
  // user DETAILS fetch
  const UserDetails = yield call(fetchUserDetails, authkey.authkey);

  // refresh user details
  const user = JSON.parse(Base64.decode(cookies.get('user')));
  user.UserDetails = UserDetails;
  const userEncode = Base64.encode(JSON.stringify(user));
  cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

  // set auth to true
  yield put({ type: SET_AUTH, newAuthState: true });

  // clear errors/stop sending
  yield put({ type: CLEAR_ERROR });
}
export function* refreshStoreSaga() {
  // export saga
  yield takeLatest(REFRESH_STORE, refresh);
}

/*
* Refresh auth SAGA
*/
function* refreshAuth() {
  //get refreshkey and authkey
  const data = JSON.parse(Base64.decode(new Cookies().get('user')));

  // token api
  const response = yield call(refreshAuthkey, data);

  // if refresh auth doesnt work, logout user
  if (response.status !== 200) {
    // redirect user to login page
    yield put(push('/logga-in'));
    cookies.remove('user', { path: '/' });
    cookies.remove('reg', { path: '/' });
    cookies.remove('gift', { path: '/' });
    cookies.remove('retry', { path: '/' });
    cookies.remove('trustly', { path: '/' });
    // set auth state to false
    yield put({ type: SET_AUTH, newAuthState: false });
    // call logout API
    yield call(logoutAPI, data.authkey);

    // but if response status is NOT 1000 refresh logged in users authkey
  } else {
    // refresh user auth
    data.authkey = response.newauthkey;
    cookies.set('user', Base64.encode(JSON.stringify(data)), { path: '/', maxAge: 1209600 });

    // set auth to true
    yield put({ type: SET_AUTH, newAuthState: true });

    // clear errors/stop sending
    yield put({ type: CLEAR_ERROR });

    //reload browser to get new values based on new auth
    window.location.reload(true);
  }
}
export function* refreshAuthSaga() {
  // export saga
  yield takeLatest(REFRESH_AUTH, refreshAuth);
}

/*
* LogoutSaga: will be fired on LOGOUT actions
*/
function* logout(authkey) {
  yield put(push('/'));
  cookies.remove('user', { path: '/' });
  cookies.remove('reg', { path: '/' });
  cookies.remove('gift', { path: '/' });
  cookies.remove('retry', { path: '/' });
  cookies.remove('trustly', { path: '/' });
  localStorage.clear();

  persistor.purge();

  // set auth state to false
  yield put({ type: SET_AUTH, newAuthState: false });
  const subscription = auth.choosenSubsciption();
  yield put({ type: CHOOSE_SUBSCRIPTION, subscription });

  // call logout API
  yield call(logoutAPI, authkey.authkey);
}
export function* logoutSaga() {
  yield takeLatest(LOGOUT, logout);
}
