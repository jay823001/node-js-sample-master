import React from 'react';
import PropTypes from 'prop-types';
import { apiUrl, internationalization_a, fetchAuth } from 'containers/App/api';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import { logoutRequest } from 'containers/Views/Account/MyAccount/actions';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H2 from 'components/Typography/H2';
import LoadingIndicatorSmall from 'components/LoadingIndicator';
import A from 'components/Typography/A';
import Translation from 'translation/nextory-web-se';

const Box = styled.div`
  border: .1rem solid ${props => props.theme.colorGrey};
  background-color: ${props => props.theme.colorWhite};
  border-radius: .5rem;
  padding: 2rem;
  width: 100%;
  margin: 0 0 2rem;
  ${media.medium`
    margin: 0 0 4rem;
    padding: 2rem 2.7rem 2rem 2rem;
  `}
  ul {
    list-style: none;
    padding: 0 0 .6rem;
    margin: 0 0 .6rem;
    border-bottom: .1rem solid ${props => props.theme.colorGrey};
    li {
      ${media.medium`
        margin: 0 2rem 0 0;
        display: inline-block;
      `}
    }
  }
`;

const H2styl = H2.extend`
  font-weight: 600;
  margin: 0 0 1rem 0;
`;

const StyledLink = A.withComponent(Link);

class Orders extends React.PureComponent {
  static propTypes = {
    authkey: PropTypes.string,
    refreshAuth: PropTypes.func,
  }

  state = {
    orders: [],
    loading: true,
  }

  componentDidMount() {
    this.checkAuth();
  }

  checkAuth = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.getOrders();
    }
  }

  getOrders = async () => {
    try {
      const response = await fetch(`${apiUrl}orders${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          'nx-at': this.props.authkey,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        return this.setState({ orders: data.data, loading: false });
      } else {
        return this.props.logoutRequest(this.props.authkey);
      }
    } catch (error) {
      return error;
    }
  };
  renderOrders() {
    if (this.state.orders.length < 1) return false;
    return this.state.orders.map(({ orderid, fakturano, orderdate, title, orderamt }) => (
      <ul key={orderid}>
        <li><StyledLink to={`/konto/kophistorik?${orderid}`}>{Translation.account.my_account.orders.order}: {fakturano}</StyledLink></li>
        <li>{orderdate}</li>
        <li>{title}: {Number.isInteger(orderamt) ? orderamt : +orderamt.toFixed(2)} {Translation.account.my_account.orders.currency}</li>
      </ul>
    ));
  }


  render() {
    return (
      <Box>
        <H2styl>{Translation.account.my_account.orders.order_history}</H2styl>
        {this.state.loading
          ? <LoadingIndicatorSmall />
          : this.renderOrders()
        }
      </Box>
    );
  }
}


function mapStateToProps(state) {
  return {
    authkey: state.account.userData.authkey,
  };
}


export default connect(mapStateToProps, { refreshAuth, logoutRequest })(Orders);
