// logout constants
export const LOGOUT = 'LOGOUT';
export const REFRESH_STORE = 'REFRESH_STORE';
export const REFRESH_AUTH = 'REFRESH_AUTH';
