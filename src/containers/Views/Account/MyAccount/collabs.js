import React from 'react';
import PropTypes from 'prop-types';
import { apiUrl, internationalization_a, internationalizationLanguage, fetchAuth } from 'containers/App/api';
import { connect } from 'react-redux';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions'
import { logoutRequest } from 'containers/Views/Account/MyAccount/actions';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import H2 from 'components/Typography/H2';
import H4 from 'components/Typography/H4';
import LoadingIndicatorSmall from 'components/LoadingIndicator';
import P from 'components/Typography/P';
import A from 'components/Typography/A';
import Img from 'components/Img';
import Caret from '../../Registration/RegisterCard/images/caret.svg';
import Translation from 'translation/nextory-web-se';

const Box = styled.div`
  border: .1rem solid ${props => props.theme.colorGrey};
  background-color: ${props => props.theme.colorWhite};
  border-radius: .5rem;
  padding: 2rem;
  width: 100%;
  margin: 0 0 2rem;
  ${media.medium`
    margin: 0 0 4rem;
    padding: 2rem 2.7rem 2rem 2rem;
  `}
  ul {
    list-style: none;
    padding: 0 0 .6rem;
    margin: 0 0 .6rem;
    border-bottom: .1rem solid ${props => props.theme.colorGrey};
    li {
      ${media.medium`
        margin: 0 2rem 0 0;
        display: inline-block;
      `}
    }
  }
`;

const H2styl = H2.extend`
  font-weight: 600;
  margin: 0 0 1rem 0;
`;


const Collab = styled.div`
  border-bottom: .1rem solid ${props => props.theme.colorGrey};
  margin: 0 0 1.2rem 0;
  padding: 0 0 .8rem 0;
  div {
    display: none;
  }
  h4 {
    cursor: pointer;
  }
  h4:after {
    width: 1.4rem;
    height: 1.4rem;
    top: 0.6rem;
    position: relative;
    float: right;
    content: '';
    transition: all .15s ease;
    background-image: url(${Caret});
    background-repeat: no-repeat;
    background-position: center;
    background-size: 1.4rem 1.4rem;
  }
  ${props => props.isActive && css`
    div {
      display:block;
    }
    h4:after {
      transform: rotate(90deg);
    }
  `}
`;


class Collabs extends React.PureComponent {
  static propTypes = {
    authkey: PropTypes.string,
    refreshAuth: PropTypes.func,
  }

  state = {
    activeCollab: null,
    collabs: [],
    loading: true,
  }

  componentDidMount() {
    this.checkAuth();
  }


  checkAuth = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.getCollabs();
    }
  }

  getCollabs = async () => {
    try {
      const response = await fetch(`${apiUrl}mycollabs${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          'nx-at': this.props.authkey,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        return this.setState({ collabs: data.data.collabs, loading: false });
      } else {
        return this.props.logoutRequest(this.props.authkey);
      }
    } catch (error) {
      return error;
    }
  };

  foldOutCollab = (collabid) => {
    if (collabid === this.state.activeCollab) {
      this.setState({ activeCollab: null });
    } else {
      this.setState({ activeCollab: collabid });
    }
  }


  renderCollabs() {
    if (this.state.collabs.length < 1) return false;
    return this.state.collabs.map(({ collabid, title, subtitle, description, picture, link }) => (
      <Collab key={collabid} isActive={this.state.activeCollab === collabid}>
        <H4 onClick={() => this.foldOutCollab(collabid)}>{title}</H4>
        <div>
          <P><strong>{subtitle}</strong></P>
          <P>{description}</P>
          <A target="_blank" href={link}><Img src={picture} alt={title} /></A>
        </div>
      </Collab>
    ));
  }


  render() {
    if (internationalizationLanguage === "SW") {

      return (
        <Box>
          <H2styl>{Translation.account.my_account.collabs.heading}</H2styl>
          <P>{Translation.account.my_account.collabs.p1}</P>
          {this.state.loading
            ? <LoadingIndicatorSmall />
            : this.renderCollabs()
          }
        </Box>
      );
    } else {
      return null;
    }
  }
}


function mapStateToProps(state) {
  return {
    authkey: state.account.userData.authkey,
  };
}


export default connect(mapStateToProps, { refreshAuth, logoutRequest })(Collabs);
