import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import BackgroundImage from './nextory-dark.jpg';


const PageWrapper = styled.section`
  padding: 0 1.5rem;
  background-image: url(${BackgroundImage});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  min-height: 50rem;
  height: 100vh;
  position: relative;
`;

const InnerWrapper = styled.div`
  z-index: 10;
  max-width: 90%;
  width: 41.2rem;
  background-color: ${props => props.theme.colorWhite};
  padding: 1.5rem 2.0rem 2.0rem;
  margin: 0 auto;
  position: absolute;
  top: 40%;
  left:0;
  right:0;
  bottom:auto;
  transform: translateY(-50%);
  ${media.medium`
    padding: 2.0rem 2.5rem 2.5rem;
    top: 44%;
  `}
  ${media.large`
    padding: 2.5rem 3.0rem 3.0rem;
  `}
`;

class AccountWrapper extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    return (
      <PageWrapper>
        <ReactCSSTransitionGroup
          transitionAppear={true}
          transitionAppearTimeout={600}
          transitionEnterTimeout={600}
          transitionLeaveTimeout={200}
          transitionName="FadeIn"
        >
          <InnerWrapper>
            {this.props.children}
          </InnerWrapper>
        </ReactCSSTransitionGroup>
      </PageWrapper>
    );
  }
}

export default AccountWrapper;
