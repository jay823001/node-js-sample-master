import React from 'react';
import PropTypes from 'prop-types';
import DataLayer from 'containers/App/datalayer';
import { apiUrl, internationalizationLanguage, internationalization_b, fetchAuth } from 'containers/App/api';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import { logoutRequest } from 'containers/Views/Account/MyAccount/actions';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import P from 'components/Typography/P';
import LoadingIndicator from 'components/LoadingIndicator';
import Translation from 'translation/nextory-web-se';

const OuterWrapper = styled.section`
  padding: 3rem 0 8rem;
  ${media.medium`
    padding: 7rem 0 12rem;
  `};
`;

const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 51.5rem;
`;

const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
    margin-bottom: 2.5rem;
  `};
`;

class Receipt extends React.PureComponent {
  static propTypes = {
    UserAuthkey: PropTypes.string,
  };
  state = {
    orderdata: {},
  };

  componentDidMount() {
    this.checkAuth();
  }

  checkAuth = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.fetchOrderData();
    }
  };

  fetchOrderData = async () => {
    const search = this.props.RouteSearch;
    const auth = this.props.UserAuthkey;
    const clean = search.replace('?', '');
    try {
      const response = await fetch(`${apiUrl}orderdetails?orderno=${clean}${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          'nx-at': auth,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({ orderdata: data.data });
      } else {
        return this.props.logoutRequest(auth);
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const {
      fakturano,
      orderdate,
      orderamount,
      orders,
      campagincode,
      giftcardid,
    } = this.state.orderdata;
    const moms = orderamount * 0.2;
    const momsRounded = Math.round(moms * 10) / 10;

    let paymentmethod = (Translation.account.receipt.payment_methods.credit_card);
    if (this.state.orderdata.provider === 'TRUSTLY') {
      paymentmethod = (Translation.account.receipt.payment_methods.trustly_direct);
    }
    if (campagincode > 0) {
      paymentmethod = (Translation.account.receipt.payment_methods.campaign_code);
    } else if (giftcardid > 0) {
      paymentmethod = (Translation.account.receipt.payment_methods.gift_cards);
    }
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <OuterWrapper>
          <InnerWrapper>
            <Heading>
              <H1>{Translation.account.receipt.heading}</H1>
              <H2>{Translation.account.receipt.subheading} {fakturano}</H2>
            </Heading>

            {!fakturano ? (
              <LoadingIndicator />
            ) : (
                <span>
                  <P>
                    <strong>{Translation.account.receipt.date}:</strong> {orderdate}
                    <br />
                    {orderamount > 1 &&
                      (orders[0].title.includes('Nextory i') ? (
                        <span>
                          <strong>{Translation.account.receipt.payment_methods.gift_cards} :</strong> {orderamount} {Translation.account.receipt.currency}
                        </span>
                      ) : (
                          <span>
                            <strong>{Translation.account.receipt.subsctiption}:</strong> {orders[0].title} {orderamount} {Translation.account.receipt.currency}
                          </span>
                        ))}
                  </P>

                  <P>
                    <strong>{Translation.account.receipt.payment_method}:</strong> {paymentmethod} {orderamount} {Translation.account.receipt.currency}
                  </P>

                  <P>
                    <strong>{Translation.account.receipt.receiver}:</strong> Nextory<br />
                    <strong>{Translation.account.receipt.vat_number}:</strong> SE556708-4149<br />
                    <strong>{Translation.account.receipt.email}:</strong>
                    {
                      internationalizationLanguage === 'FI' ? ' asiakaspalvelu@nextory.fi' : ' kundservice@nextory.se'
                    }
                </P>

                  <P>
                    {orderamount > 1 && (
                      <span>
                        <strong>{Translation.account.receipt.vat}:</strong> {momsRounded} {Translation.account.receipt.currency}<br />
                        <strong>{Translation.account.receipt.total}:</strong> {orderamount} {Translation.account.receipt.currency}
                      </span>
                    )}
                  </P>
                </span>
              )}
          </InnerWrapper>
        </OuterWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    RouteSearch: state.route.location.search,
    UserAuthkey: state.account.userData.authkey,
  };
}

export default connect(mapStateToProps, { refreshAuth, logoutRequest })(Receipt);
