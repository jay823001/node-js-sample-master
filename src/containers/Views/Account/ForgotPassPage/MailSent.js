import React from 'react';
import media from 'theme/styled-utils';
import DataLayer from 'containers/App/datalayer';

import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import AccountWrapper from '../AccountWrapper';
import Translation from 'translation/nextory-web-se';

const H2Styled = H2.extend`
  margin-bottom: 1.3rem;
  ${media.medium`
    margin-bottom: 1.6rem;
  `};
`;

const ForgotPassMailsent = () => (
  <AccountWrapper>
    <DataLayer />
    <H1>{Translation.account.forgotpassword.mailsent.heading}</H1>
    <H2Styled>{Translation.account.forgotpassword.mailsent.subheading}</H2Styled>
  </AccountWrapper>
);

export default ForgotPassMailsent;
