import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import styled from 'styled-components';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import validate from './validate';
import { forgotpassRequest } from '../actions';
import Translation from 'translation/nextory-web-se';

const Submit = styled.button`
  margin-top: 1.5rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;
const SubmitSC = Button.withComponent(Submit);

// take inputed values and pass them to our redux saga loginRequest
const submit = (values, forgotpassRequest) => {
  const email = values.email;
  forgotpassRequest(email);
};

class ForgotForm extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      handleSubmit,
      forgotpassRequest,
      submitting,
      pristine,
      valid,
      SendingRequest,
    } = this.props;
    return (

      <Form onSubmit={handleSubmit(values => submit(values, forgotpassRequest))}>
        <Field
          name="email"
          type="text"
          component={renderField}
          label={Translation.forms.labels.email}
        />
        <SubmitSC large type="submit" disabled={!valid || pristine || submitting}>
          {SendingRequest && <LoadingIndicatorSmall />}
          {!SendingRequest && Translation.account.forgotpassword.send}
        </SubmitSC>
      </Form>
    );
  }
}

ForgotForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  pristine: PropTypes.bool,
  SendingRequest: PropTypes.bool,
  valid: PropTypes.bool,
  forgotpassRequest: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    SendingRequest: state.account.currentlySending,
  };
}

export default connect(mapStateToProps, { forgotpassRequest })(
  reduxForm({
    form: 'forgot',
    validate,
  })(ForgotForm)
);
