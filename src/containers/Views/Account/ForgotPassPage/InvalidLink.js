import React, { Component } from 'react';
import media from 'theme/styled-utils';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import AccountWrapper from '../AccountWrapper';
import Translation from 'translation/nextory-web-se';
import { Link } from 'react-router-dom';
import A from 'components/Typography/A';
import { connect } from 'react-redux';
import { setSendingRequest } from '../../../Views/Account/actions';

const StyledLink = A.withComponent(Link);

const H2Styled = H2.extend`
  margin-bottom: 1.3rem;
  ${media.medium`
    margin-bottom: 1.6rem;
  `};
`;

const PreSubmit = StyledLink.extend`
  font-size: 1.2rem;
  margin: 1rem 0 1.5rem 0;
  display: block;
  ${media.medium`
    font-size: 1.4rem;
  `};
`;


class InvalidLinkChangPass extends Component {
    componentDidMount() {
        this.props.setSendingRequest();
    }

    render() {
        return (
            <AccountWrapper>

                <H1>{Translation.account.forgotpassword.Invlidlink}</H1>
                <H2Styled>{Translation.account.forgotpassword.InvlidlinkdESC}</H2Styled>
                <PreSubmit to="/glomt-losenord">
                    {Translation.account.forgotpassword.Resend}
                </PreSubmit>
            </AccountWrapper>
        )
    }
}

function mapStateToProps(state) {
    return {
        SendingRequest: state.account.currentlySending,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setSendingRequest: () => {
            dispatch(setSendingRequest());
        }
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(InvalidLinkChangPass);