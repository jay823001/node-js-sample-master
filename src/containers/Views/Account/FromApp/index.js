import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { gup } from 'utils/helpFunctions';
import Loading from 'components/LoadingIndicator/page';
import { loginApp } from './actions';

class AccountApp extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
  };

  componentDidMount() {
    const key = gup('websitekey', this.props.Route);
    this.props.loginApp(key);
  }

  render() {
    return (
      <div>
        <Loading />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    Route: state.route.location.path,
  };
}

export default connect(mapStateToProps, { loginApp })(AccountApp);
