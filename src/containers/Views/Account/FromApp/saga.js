/* eslint consistent-return:0 */
import { call, put, takeLatest } from 'redux-saga/effects';
import Cookies from 'universal-cookie';
import { push } from 'react-router-redux';
import { Base64 } from 'js-base64';
import { fetchWebsitekey, fetchLogin, fetchUserDetails, logoutAPI } from 'containers/App/api';
import { CREATEUSER_LOGOUT, CREATEUSER_SETAUTH } from 'containers/Views/Registration/constants';
import { SET_AUTH } from 'containers/Views/Account/LoginPage/constants';
import { LOGIN_APP } from './constants';

const cookies = new Cookies();

/*
* loginAppSaga: will be fired on LOGIN_APP actions
*/
function* loginUser(key) {
  // if user us already logged in, log him out
  if (cookies.get('user')) {
    const data = JSON.parse(Base64.decode(cookies.get('user')));
    cookies.remove('user', { path: '/' });
    yield put({ type: SET_AUTH, newAuthState: false });
    yield call(logoutAPI, data.authkey);
  }

  // make sure we dont have a current registration active or a logged in user
  cookies.remove('reg', { path: '/' });
  cookies.remove('trustly', { path: '/' });
  cookies.remove('retry', { path: '/' });
  cookies.remove('gift', { path: '/' });
  yield put({ type: CREATEUSER_LOGOUT });

  // api call for webaccesslogin
  const data = yield call(fetchWebsitekey, key.key);

  // if webaccess fails, send user to login page
  if (data.status !== 200) {
    yield put(push('/logga-in'));
  } else {
    // api call for login
    const logindata = {
      data: {
        email: data.data.email,
        password: data.data.hashedpass,
      },
    };
    const userdata = yield call(fetchLogin, logindata);

    // if status is 200, we log in user, if not its falsy and we return error
    if (userdata.status === 200) {
      // user DETAILS fetch
      const UserDetails = yield call(fetchUserDetails, userdata.authkey);

      //if customerid is present, user is valid so keep going
      if (UserDetails.customerid) {
        // if user is visitor we start a trial registration process
        if (UserDetails.allowedactions.includes('TRAIL')) {
          const reg = {
            campaignname: 'Trail',
            paymenttype: 'TRAIL',
            authkey: userdata.authkey,
            userid: data.data.userid,
            email: data.data.email,
            password: data.data.hashedpass,
          };
          const userEncode = Base64.encode(JSON.stringify(reg));
          cookies.set('reg', userEncode, { path: '/', maxAge: 1209600 });

          yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });

          // redirect to choose sub
          yield put(push('/register/subscription#steg-1'));
          // if user is nonmember
        } else if (UserDetails.allowedactions.includes('ACTIVATE_AGAIN')) {
          // set a cookie token to preserve logged in user on page refresh
          userdata.data.email = data.data.email;
          userdata.data.p = data.data.hashedpass;
          userdata.data.authkey = userdata.authkey;
          userdata.data.refreshkey = userdata.refreshkey;
          userdata.data.UserDetails = UserDetails;
          const userEncode = Base64.encode(JSON.stringify(userdata.data));
          new Cookies().set('user', userEncode, { path: '/', maxAge: 1209600 });

          // set auth to true
          yield put({ type: SET_AUTH, newAuthState: true });

          // redirect to reactivate payment
          yield put(push('/konto/aktivera-abonnemang'));

          // if user is member
        } else if (UserDetails.ismember) {
          // set a cookie token to preserve logged in user on page refresh
          userdata.data.email = data.data.email;
          userdata.data.p = data.data.hashedpass;
          userdata.data.authkey = userdata.authkey;
          userdata.data.refreshkey = userdata.refreshkey;
          userdata.data.UserDetails = UserDetails;
          const userEncode = Base64.encode(JSON.stringify(userdata.data));
          new Cookies().set('user', userEncode, { path: '/', maxAge: 1209600 });

          // set auth to true
          yield put({ type: SET_AUTH, newAuthState: true });

          // redirect to change sub
          yield put(push('/konto/andra-abonnemang'));
        }

        // if userdetails customerid is empty, fetching doesnt work so we send user to login page
      } else {
        yield put(push('/logga-in'));
      }
    } else {
      yield put(push('/logga-in'));
    }
  }
}

export function* loginAppSaga() {
  // export saga
  yield takeLatest(LOGIN_APP, loginUser);
}
