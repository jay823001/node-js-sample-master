/*
 * Logged in User reducer
 */
import auth from 'containers/App/auth/auth';
import { UPDATE_PROFILE, CHANGE_PASS_REQUEST, CHANGE_PASS_SUCCESS, FORGOT_PASS, SET_SENDING_REQUEST } from './constants';
import {
  SET_AUTH, LOGIN_REQUEST, LOGIN_REQUEST_SUCCESS, REQUEST_ERROR, CLEAR_ERROR,
} from './LoginPage/constants';
import { LOGOUT } from './MyAccount/constants';
import { USER_REDEEM_GIFT } from '../GiftcardNew/constants';

// Account reducer
const initialAccountState = {
  error: '',
  currentlySending: false,
  loggedIn: auth.loggedIn(),
  userData: auth.userData(),
};

export function AccountReducers(state = initialAccountState, { type, newAuthState, error }) {
  switch (type) {
    case SET_AUTH:
      return { ...state, userData: auth.userData(), loggedIn: newAuthState };

    case LOGIN_REQUEST:
      return { ...state, currentlySending: true };

    case LOGIN_REQUEST_SUCCESS:
      return { ...state, currentlySending: false, error: '' };

    case REQUEST_ERROR:
      return { ...state, error, currentlySending: false };

    case CLEAR_ERROR:
      return { ...state, error: '', currentlySending: false };

    case FORGOT_PASS:
      return { ...state, currentlySending: true };

    case USER_REDEEM_GIFT:
      return { ...state, currentlySending: true };

    case UPDATE_PROFILE:
      return { ...state, currentlySending: true };

    case CHANGE_PASS_REQUEST:
      return { ...state, currentlySending: true };

    case CHANGE_PASS_SUCCESS:
      return { ...state, currentlySending: false, error: '' };

    case SET_SENDING_REQUEST:
      return { ...state, currentlySending: false };

    case LOGOUT:
      localStorage.clear();
      return { ...state, userData: {} };

    default:
      return state;
  }
}
