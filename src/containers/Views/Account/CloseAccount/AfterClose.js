import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DataLayer from 'containers/App/datalayer';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H1 from 'components/Typography/H1';
import P from 'components/Typography/P';
import Button from 'components/Buttons';
import Translation from 'translation/nextory-web-se';

const RegisterWrapper = styled.section`
  padding: 3rem 0 8rem;
  ${media.medium`
    padding: 7rem 0 12rem;
  `};
`;

const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 51.5rem;
`;

const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
    margin-bottom: 2.5rem;
  `};
`;

class AfterCloseAccount extends React.PureComponent {
  static propTypes = {
    routeSearch: PropTypes.string,
  };

  render() {
    const search = this.props.routeSearch;
    const clean = search.replace('?', '');

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              {clean.includes('canceled') ? (
                <H1>{Translation.account.close_account.step2.afterclose.heading1}</H1>
              ) : (
                <H1>
                  {Translation.account.close_account.step2.afterclose.heading2}
                  <br />
                  {clean}
                </H1>
              )}
            </Heading>

            <P>{Translation.account.close_account.step2.afterclose.p1}</P>

            {clean.includes('canceled') ? (
              <Button to={'/konto/aktivera-abonnemang'}>
                {Translation.account.close_account.step2.afterclose.button1}
              </Button>
            ) : (
              <span>
                <P>
                  {Translation.account.close_account.step2.afterclose.p2} {clean} {' '}
                  {Translation.account.close_account.step2.afterclose.p2_b}
                </P>
                <br />
                <Button to={'/konto'}>
                  {Translation.account.close_account.step2.afterclose.button2}
                </Button>
              </span>
            )}
          </InnerWrapper>
        </RegisterWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    routeSearch: state.route.location.search,
  };
}

export default connect(mapStateToProps)(AfterCloseAccount);
