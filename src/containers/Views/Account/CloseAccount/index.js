// Project dependencies
import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import DataLayer from 'containers/App/datalayer';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
// Project functions
import { parseJwt } from 'utils/helpFunctions';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import {
  refreshAuth,
  refreshStore,
  logoutRequest,
} from 'containers/Views/Account/MyAccount/actions';

// Styled Components
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import P from 'components/Typography/P';
import Form from 'components/Form/Form';
import Select from 'components/Form/Select';
import Input from 'components/Form/Input';
import Label from 'components/Form/Label';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  ButtonLarge,
  ButtonFramed,
  ButtonRedStyl,
  ButtonRedSubmit,
  Step1,
  Step2,
  Error,
} from './styles-closeacc';
import Translation from 'translation/nextory-web-se';

class CloseAccount extends React.PureComponent {
  static propTypes = {
    Subscription: PropTypes.object,
    Authkey: PropTypes.string,
    refreshStore: PropTypes.func,
  };
  static defaultProps = {
    Subscription: {},
  };

  state = {
    step: 1,
    value: '',
    inputvalue: '',
    loading: false,
    redirect: false,
    enddate: '',
    reasonempty: false,
    error: false,
  };

  // get to step 2 of closing account
  handleClick = num => {
    this.setState({ step: num });
  };

  // set state when choosing options in select
  handleChange = event => {
    this.setState({ value: event.target.value, inputvalue: '', reasonempty: false });
  };

  // when writing other reason in input
  handleInput = event => {
    this.setState({ inputvalue: event.target.value });
  };

  // when form is submitted
  handleSubmit = event => {
    event.preventDefault();
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else if (!this.state.value || this.state.value === 'Vänligen välj ett alternativ') {
      this.setState({ reasonempty: true });
    } else {
      this.setState({ loading: true });
      const params = {
        reason: encodeURI(this.state.value),
        comment: encodeURI(this.state.inputvalue),
        key: this.props.Authkey,
      };
      return this.submitPost(params);
    }
  };

  /* if user has valid authkey and valid reason, we submit
  cancelation to API then refresh our redux store and redirect user */
  submitPost = async params => {
    try {
      const response = await fetch(
        `${apiUrl}cancelmembership?reason=${params.reason}&comment=${params.comment}${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'nx-at': params.key,
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(params),
        }
      );
      const status = await response.json();
      if (status.status === 200) {
        this.props.refreshStore(params.key);
        this.setState({ loading: false, enddate: status.data.nextrundate });
        this.setState({ redirect: true });
        // return console.log(status);
      } else {
        return this.setState({ loading: false, error: true });
      }
    } catch (error) {
      return this.setState({ loading: false, error: true });
    }
  };

  render() {
    let sub = {};
    let freeMember = false;

    // set type of sub current has, render different UI
    if (typeof this.props.Subscription.subscriptiondetails !== 'undefined') {
      sub = this.props.Subscription.subscriptiondetails.subscriptionplanename;
    }

    // if user can CANCEL_FREE_MEMBERSHIP they are a free trial member, render different UI
    if (typeof this.props.Subscription.allowedactions !== 'undefined') {
      if (
        this.props.Subscription.allowedactions.includes('CANCEL_FREE_MEMBERSHIP') &&
        !this.props.Subscription.giftcarduser
      ) {
        freeMember = true;
      }
    }

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <RegisterWrapper>
          <InnerWrapper>
            {sub !== 'Silver' &&
              this.state.step === 1 && (
                <Step1>
                  {freeMember ? (
                    <span>
                      <Heading>
                        <H1>{Translation.account.close_account.step1.freemember.heading}</H1>
                        <H2>{Translation.account.close_account.step1.freemember.subheading}</H2>
                      </Heading>
                      <P>{Translation.account.close_account.step1.freemember.p1}</P>
                    </span>
                  ) : (
                      <span>
                        <Heading>
                          <H1>{Translation.account.close_account.step1.member.heading}</H1>
                          <H2>{Translation.account.close_account.step1.member.subheading}</H2>
                        </Heading>
                        <P>
                          {Translation.account.close_account.step1.member.p1_p1}{' '}
                          <strong>{Translation.account.close_account.step1.member.p1_p2}</strong>
                        </P>
                      </span>
                    )}

                  <ButtonLarge to={'/konto/andra-abonnemang'}>
                    {Translation.account.close_account.buttons.change_subscription}
                  </ButtonLarge>
                  <ButtonFramed framed to={'/konto'}>
                    {Translation.account.close_account.buttons.cancel}
                  </ButtonFramed>
                  <ButtonRedStyl red onClick={() => this.handleClick(2)}>
                    {Translation.account.close_account.buttons.end}
                  </ButtonRedStyl>
                </Step1>
              )}

            {(sub === 'Silver' || this.state.step === 2) && (
              <Step2>
                <Heading>
                  <H1>{Translation.account.close_account.step2.heading}</H1>
                  <H2>{Translation.account.close_account.step2.subheading}</H2>
                </Heading>

                <Form onSubmit={this.handleSubmit}>
                  <Label for="email">{Translation.account.close_account.step2.email_label}</Label>
                  <Select value={this.state.value} onChange={this.handleChange}>
                    <option defaultValue>
                      {Translation.account.close_account.step2.list.option1}
                    </option>
                    <option value="Jag har inte tid att utnyttja tjänsten">
                      {Translation.account.close_account.step2.list.option2}
                    </option>
                    <option value="Tjänsten är inte tillräckligt prisvärd">
                      {Translation.account.close_account.step2.list.option3}
                    </option>
                    <option value="Jag hittar inte de böcker jag vill läsa">
                      {Translation.account.close_account.step2.list.option4}
                    </option>
                    <option value="Appen har inte fungerat som den ska">
                      {Translation.account.close_account.step2.list.option5}
                    </option>
                    <option value="Jag har valt en annan tjänst">
                      {Translation.account.close_account.step2.list.option6}
                    </option>
                    <option value="Annat">
                      {Translation.account.close_account.step2.list.option7}
                    </option>
                  </Select>

                  {this.state.value === 'Jag hittar inte de böcker jag vill läsa' && (
                    <span>
                      <Label for="missingbooks">
                        {Translation.account.close_account.step2.labels.label1}
                      </Label>
                      <Input
                        type="text"
                        name="missingbooks"
                        value={this.state.inputvalue}
                        onChange={this.handleInput}
                      />
                    </span>
                  )}

                  {this.state.value === 'Annat' && (
                    <span>
                      <Label for="annaninput">
                        {Translation.account.close_account.step2.labels.label2}
                      </Label>
                      <Input
                        type="text"
                        name="annaninput"
                        value={this.state.inputvalue}
                        onChange={this.handleInput}
                      />
                    </span>
                  )}

                  {this.state.reasonempty && (
                    <Error>{Translation.account.close_account.step2.errors.error1}</Error>
                  )}
                  {this.state.error && (
                    <Error>{Translation.account.close_account.step2.errors.error2}</Error>
                  )}

                  <ButtonFramed framed to={'/konto'}>
                    {Translation.account.close_account.buttons.cancel}
                  </ButtonFramed>
                  {this.state.loading ? (
                    <ButtonRedStyl disabled red>
                      <LoadingIndicatorSmall />
                    </ButtonRedStyl>
                  ) : (
                      <ButtonRedSubmit
                        type="submit"
                        red
                        value={Translation.account.close_account.step2.buttonred_value}
                      />
                    )}
                </Form>
              </Step2>
            )}

            {this.state.redirect &&
              (freeMember ? (
                <Redirect to={'/konto/avslutat?canceled'} />
              ) : (
                  <Redirect to={`/konto/avslutat?${this.state.enddate}`} />
                ))}
          </InnerWrapper>
        </RegisterWrapper>
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    Subscription: state.account.userData.UserDetails,
    Authkey: state.account.userData.authkey,
  };
}

export default connect(mapStateToProps, { refreshStore, refreshAuth, logoutRequest })(CloseAccount);
