import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

import Button from 'components/Buttons';


export const RegisterWrapper = styled.section`
  padding: 3rem 0 8rem;  
  ${media.medium`
    padding: 7rem 0 12rem;
  `}
`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 52.5rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
    margin-bottom: 2.5rem;
  `}
`;

export const ButtonLarge = Button.extend`
    width: 100%;
    max-width: 100%;
    margin: 2rem 0 1.5rem 0;
    padding: 1.3rem 2.0rem;
    text-align: center;
`;

export const ButtonFramed = Button.extend`
  text-align: center;
  padding: 1.3rem 2.0rem;
  margin: 0 0 1.5rem 0;
  width: 100%;
  ${media.small`
    width: 48%;
    float:left;  
  `}
`;

export const Red = styled.span``;
export const ButtonRed = Button.withComponent(Red);
export const ButtonRedStyl = ButtonRed.extend`
  text-align: center;
  padding: 1.3rem 2.0rem;
  width: 100%;
  ${media.small`
    width: 48%;
    float:right;
  `}
  ${props => props.disabled && css`
    opacity: .6;
    cursor: not-allowed;
  `}
`;
export const Submit = styled.input``;
export const ButtonRedSubmit = ButtonRedStyl.withComponent(Submit);

export const Step1 = styled.div``;

export const Step2 = styled.div``;

export const Error = styled.span`
  font-size: 1.2rem;
  position: relative;
  display: block;
  bottom: 0;
  left: 0;
  top: -1.4rem;
  color: ${props => props.theme.colorRed};
`;