import React from 'react';
import PropTypes from 'prop-types';
import RegTrustly from './RegTrustly';
import Lock from './images/IconLock';
import Visa from './images/visa.png';
import Cookies from 'universal-cookie';
import RegCardForm from './RegCardForm';
import Master from './images/master.png';
import BankID from './images/bankid.svg';
import Trustly from './images/trustly.png';
import DataLayer from 'containers/App/datalayer';
import Translation from 'translation/nextory-web-se';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Base64 } from 'js-base64';
import { connect } from 'react-redux';
import { scroller } from 'react-scroll';
import { Redirect } from 'react-router';
import { parseJwt } from 'utils/helpFunctions';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import { logoutRequest } from 'containers/Views/Account/MyAccount/actions';
import {
  toast,
  style,
  ToastContainer,
} from 'react-toastify';
import {
  apiUrl,
  fetchAuth,
  internationalization_a,
  internationalizationLanguage
} from 'containers/App/api';
import {
  Cards,
  Clear,
  Mobile,
  Foldout,
  Heading,
  Payment,
  WhiteBg,
  CardImg,
  CardImg2,
  PaymentHead,
  HeadingText,
  SafePayment,
  FoldTrigger,
  PaymentTitle,
  InnerWrapper,
  SeccondHeading,
  RegisterWrapper,
} from './StyledPayment';

class ChangePayment extends React.PureComponent {
  static propTypes = {
    authkey: PropTypes.string,
    RouteSearch: PropTypes.string,
  };
  state = {
    card: false,
    bankid: false,
    orderid: '',
    useraccess: true,
  };

  componentDidMount() {
    this.checkSublevel();
    this.checkAuth();
    if (this.props.RouteSearch.includes('trustlyfail')) {
      this.notifyTrustlyFail();
    }
  }

  checkSublevel = () => {
    if (this.props.UserDetails.allowedactions.includes('ACTIVATE_AGAIN')) {
      this.setState({ useraccess: false });
    }
  };

  checkAuth = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.fetchOrderid();
    }
  };

  fetchOrderid = async () => {
    try {
      const response = await fetch(`${apiUrl}updatecarddetails${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          'nx-at': this.props.authkey,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        return this.setState({ orderid: data.data.orderid });
      } else {
        return this.props.logoutRequest(this.props.authkey);
      }
    } catch (error) {
      console.log(error);
    }
  };

  notifyTrustlyFail = () => {
    toast.success(
      (Translation.account.changepayment.notifyTrustlyFail),
      {
        position: toast.POSITION.BOTTOM_CENTER,
        style: style({
          colorSuccess: '#ff3a54',
          width: '380px',
        }),
      }
    );
  };
  scrollTo() {
    if (window.innerWidth < 768) {
      scroller.scrollTo('scroll-to-payment', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart',
        offset: -20,
      });
    }
  };

  handleCardClick = () => {
    this.setState({ card: !this.state.card, bankid: false });
    this.scrollTo();
  };

  handleBankidClick = () => {
    this.setState({ bankid: !this.state.bankid, card: false });
    this.scrollTo();
  };

  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />

        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <HeadingText>{Translation.account.changepayment.heading}</HeadingText>
              <SeccondHeading>{Translation.account.changepayment.listitem}
              </SeccondHeading>
            </Heading>

            <Payment name="scroll-to-payment">
              <PaymentHead>
                <PaymentTitle>
                  {Translation.registration.stage3.regcardform.paymenttitle}
                </PaymentTitle>
                <SafePayment>
                  <Lock />  {Translation.registration.stage3.secure_server}
                </SafePayment>
                <Clear />
              </PaymentHead>
              <FoldTrigger active={this.state.card} onClick={this.handleCardClick}>
                {Translation.account.changepayment.credit_or_debit_card}
                <CardImg>
                  <Cards visa alt="Visa" src={Visa} />
                  <Cards master alt="Mastercard" src={Master} />
                </CardImg>
              </FoldTrigger>
              <Foldout active={this.state.card}>
                <article>
                  <RegCardForm orderidUpdatePayment={this.state.orderid} />
                </article>
              </Foldout>
              {
                internationalizationLanguage !== "FI" ?
                  <FoldTrigger active={this.state.bankid} onClick={this.handleBankidClick}>
                    {Translation.account.changepayment.mobile_bankid}
                    <CardImg2>
                      <Mobile bankid alt="BankId" src={BankID} />
                      <Mobile trustly alt="Trustly" src={Trustly} />
                    </CardImg2>
                  </FoldTrigger>
                  : null
              }

              <Foldout active={this.state.bankid}>
                <RegTrustly orderidUpdatePayment={this.state.orderid} />
              </Foldout>
            </Payment>
            <ToastContainer autoClose={5000} />
          </InnerWrapper>
        </RegisterWrapper>
        <WhiteBg active={this.state.card} active2={this.state.bankid} />

        {!this.state.useraccess && <Redirect to={'/konto'} />}
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    authkey: state.account.userData.authkey,
    RouteSearch: state.route.location.search,
    UserDetails: state.account.userData.UserDetails,
  };
}

export default connect(mapStateToProps, { refreshAuth, logoutRequest })(ChangePayment);
