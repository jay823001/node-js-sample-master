import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';

import H3 from 'components/Typography/H3';
import Img from 'components/Img';
import Caret from './images/icn-arrow-right.svg';

export const RegisterWrapper = styled.section`
  padding: 3rem 0 8rem;
  background:${props => props.theme.blueBg};
  ${media.medium`
    padding: 7rem 0 16rem;
  `}
`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 75.5rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  text-align: center;
  ${media.medium`
    margin-bottom: 2.5rem;
  `}
`;
export const HeadingText = styled.h1`
    font-size: 60px;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.1;
    letter-spacing: -0.6px;
    text-align: center;
    margin: 18px 0px;
    ${mediaCustom.medium`
    font-size: 30px;
  `}
`;
export const SeccondHeading = styled.p`
    font-size: 18px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    -webkit-text-align: center;
    text-align: center;
    max-width: 650px;
    margin: 0 auto;
    color: ${props => props.theme.seccondGrey};
`;

export const Payment = styled.div`
position: absolute;
max-width: 700px;
margin: auto;
  background: ${props => props.theme.themeWhite};
  padding: 35px;
  box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.08), 0 12px 24px 2px rgba(234, 243, 245, 0.25);
  border-radius: 6px;
  left: 0;
  right: 0;
  ${mediaCustom.medium`
    padding: 25px;
    width: 94%;
  `}

`;

export const PaymentHead = styled.div`
  position:relative;
  margin-bottom: 10px;
  border-bottom: .1rem solid #e9e9e9;
  padding-bottom: 15px;
`;
export const PaymentTitle = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.36;
  letter-spacing: normal;
  color: #585858;
  width: 85px;
  float: left;
`;
export const SafePayment = styled.span`
  position: relative;
  text-align: right;
  display: block;
  top: 0;
  font-size: 1.2rem;
  color: ${props => props.theme.colorDarkGrey};
  vertical-align: bottom;
  ${media.medium`
    font-size: 11px;
  `}
`;

export const FoldTrigger = styled(H3)`
  font-size: 1.6rem;
  font-weight: 400;
  display: block;
  padding: 1.5rem 0 1.5rem 1.0rem;
  text-decoration: none;
  color: ${props => props.theme.seccondGrey};
  border: .1rem solid #e9e9e9;
  border-radius: 0px;
  position: relative;
  background-color: white;
  cursor:pointer;
  z-index:5;
  border-left: 0;
  border-right: 0;
  border-top:0;
  padding-top: 5px;
  ${media.medium`
    font-size: 1.8rem;
  `}
  ${props => props.active && css`
    border:none;
  `}
  &:before {
    position: relative;
    top: 0.1rem;
    left: -7px;
    display: inline-block;
    width: 1.4rem;
    height: 1.4rem;
    margin-right: .7rem;
    content: '';
    transition: all .15s ease;
    background-image: url(${Caret});
    background-repeat: no-repeat;
    background-position: center;
    background-size: 2.5rem 2.4rem;
  }
  ${props => props.active && css`
    border-radius: .5rem .5rem 0 0;
    &:before {
      transform: rotate(90deg);
    }
  `}
`;


export const Cards = styled(Img)`
  width: 2.8rem;
  display: inline-block;
  height: auto;
  position: absolute;
  top: 1.4rem;
  border: .1rem solid ${props => props.theme.colorWhite};
  ${media.medium`
    width: 3.6rem;
    top: 1.3rem;
  `}
  ${props => props.visa && css`
    margin-left: 1.5rem;
  `}
  ${props => props.master && css`
    margin-left: 4.8rem;
    ${media.medium`
      margin-left: 5.8rem;
    `}
  `}
`;

export const CardImg = styled.div`
  float:right;
  margin-right: 6.2em;
  img{
    top: 7px;
  }
  ${mediaCustom.small`
      margin-right: 4.2em;
  `}
`;
export const CardImg2 = styled.div`
  float:right;
  margin-right: 8.2em;
  img{
    top: 7px;
  }
  ${mediaCustom.small`
  margin-right: 6.6em;
  `}
`;
export const Mobile = styled(Img)`
  display: inline-block;
  height: auto;
  position: absolute;
  ${props => props.bankid && css`
    width: 2.0rem;
    height: 1.8rem;
    top: 1.3rem;
    margin-left: 1.3rem;
    ${media.medium`
      width: 2.8rem;
      height: 2.6rem;
      top: 1.0rem;
    `}
  `}
  ${props => props.trustly && css`
    width: 7.0rem;
    top: 1.0rem;
    margin-left: 4.2rem;
    ${media.medium`
      width: 8.5rem;
      top: .8rem;
      margin-left: 4.9rem;
    `}
  `}
`;

export const Foldout = styled.div`
  background: white;
  border-radius: 0 0 .5rem .5rem;
  border: .1rem solid ${props => props.theme.colorGrey};
  border-top: none;
  overflow-y: hidden;
  max-height: 0;
  transition: all .15s ease;
  position: relative;
  top: -1.3rem;
  z-index:3;
  border-left: none;
    border-right: none;
    border-radius: 0px;
    border-bottom: 1px solid #e9e9e9;
    box-shadow: none;
  ${props => props.active && css`
    max-height: 70rem;
    transition: all .15s ease;
  `}
  article {
    padding: 2.0rem;
    padding-left: 0px;
    padding-right: 30px;
    ${media.medium`
      padding: 2.5rem;
      padding-left: 0px;
      padding-right: 30px;
    `}
    ${mediaCustom.small`
      padding-right: 0px;
    `}
  }
  a,button {
    margin-top: 1.5rem;
  }
`;

export const Clear = styled.div`
    clear:both !important;
`;
export const WhiteBg = styled.div`
  background:${props => props.theme.themeWhite};
  height: auto;
  min-height: 240px;
  transition: all .15s ease;
  ${mediaCustom.medium`
    min-height: 304px;
  `}
  ${props => props.active && css`
    min-height: 480px;
    ${mediaCustom.medium`
       min-height: 600px;
    `}
  `}
  ${props => props.active2 && css`
    min-height: 380px;
  `}
`;

export const Span = styled.span`
  display: block;
  font-size: 1.4rem;
  margin: 0 0 0.8rem;
  ${media.medium`
    font-size: 1.6rem;
    margin: 0 0 1.3rem;
  `};
`;
