import Translation from 'translation/nextory-web-se';
import { verifyValidCardNo } from './luhnCheck';

const validate = values => {
  const errors = {};
  if (!values.cardnumber) {
    errors.cardnumber = Translation.forms.validation.card_number_error;
  } else if (values.cardnumber.length !== 19) {
    errors.cardnumber = Translation.forms.validation.card_number_error;
  } else if (verifyValidCardNo(values.cardnumber) === false) {
    errors.cardnumber = Translation.forms.validation.card_real_number_error;
  }

  if (values.month === '0' || !values.month) {
    errors.month = Translation.forms.validation.month_error;
  }

  if (values.year === '0' || !values.year) {
    errors.year = Translation.forms.validation.year_error;
  }

  if (!values.cvv) {
    errors.cvv = Translation.forms.validation.cvv_error;
  } else if (values.cvv.length < 3 || values.cvv.length > 4) {
    errors.cvv = Translation.forms.validation.cvv_length_error;
  }

  return errors;
};

export default validate;
