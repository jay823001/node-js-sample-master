import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import Button from 'components/Buttons';


export const TableWrapper = styled.article`
  position: relative;
  min-height: 465px;
`;

export const TableWrapperFix = styled.article`
  padding-top: 10px;
  display: table-row;
  -webkit-overflow-scrolling: touch;
  position: fixed;
  top: 0;
  z-index: 99999999;
  width: auto;
  background: #eaf0e2;
  width: auto;
  right: 15px;
  left: 15px;
  height: 70px;
  visibility:hidden;

  ${media.medium`
    display:none;
  `}

  ${props =>
    props.sticky &&
    css`
      visibility: visible;
    `};
`;

export const StyledButton = styled(Button)`
  width: 80%;
  height: 50px;
  border-radius: 26px;
  border: solid 2px #363bec;
  background-color: #fff;
  box-shadow: none;
  -webkit-text-align: center;
  -webkit-text-align: center;
  text-align: center;
  padding: 0px;
  margin: 0px;
  text-transform: uppercase;
  font-weight: bold;
  color:${props => props.theme.colorBlue};

  ${media.medium`
    font-size: 1.6rem;
  `}
  ${mediaCustom.medium`
    width: 80%;
  `}
  ${mediaCustom.small`
    width: 80%;
  `}
  &:hover {
    background-color:${props => props.theme.colorBlue};
    color:#fff;
  }
  ${props => props.active && css`
    background-color:${props => props.theme.colorBlue};
    color:#fff;
    &:after {
      box-shadow: none;
      position: absolute;
      bottom: -1.5rem;
      left: 50%;
      display: inline-block;
      box-sizing: border-box;
      width: 0;
      height: 0;
      margin-left: -1.1rem;
      content: "";
      transform: rotate(-45deg);
      transform-origin: 0 0;
      opacity: 1;
      border: .8rem solid ${props => props.theme.colorBlue};
      border-color: transparent transparent ${props => props.theme.colorBlue} ${props => props.theme.colorBlue};
      ${media.medium`
        display:none;
      `}
    }
  `}
`;

export const ButtonExtend = Button.withComponent('button');

export const ButtonStyled = styled(ButtonExtend)`
  width: 80%;
  height: 50px;
  border-radius: 26px;
  border: solid 2px #363bec;
  background-color: #fff;
  box-shadow: none;
  -webkit-text-align: center;
  -webkit-text-align: center;
  text-align: center;
  padding: 0px;
  margin: 0px;
  text-transform: uppercase;
  font-weight: bold;
  color:${props => props.theme.colorBlue};

  ${media.medium`
    font-size: 1.6rem;
  `}
  ${mediaCustom.medium`
    width: 80%;
  `}
  ${mediaCustom.small`
    width: 80%;
  `}
  &:hover {
    background-color:${props => props.theme.colorBlue};
    color:#fff;
  }
  ${props => props.active && css`
    background-color:${props => props.theme.colorBlue};
    color:#fff;
    &:after {
      box-shadow: none;
      position: absolute;
      bottom: -1.5rem;
      left: 50%;
      display: inline-block;
      box-sizing: border-box;
      width: 0;
      height: 0;
      margin-left: -1.1rem;
      content: "";
      transform: rotate(-45deg);
      transform-origin: 0 0;
      opacity: 1;
      border: .8rem solid ${props => props.theme.colorBlue};
      border-color: transparent transparent ${props => props.theme.colorBlue} ${props => props.theme.colorBlue};
      ${media.medium`
        display:none;
      `}
    }
  `}
`;

export const TableHeading = styled.ul`
  margin: 3rem 0 0;
  font-weight: 600;
  margin-bottom: 10px;
  margin-top:0px;
  ${media.medium`
    margin: 4.5rem 0 0;
  `}
  li {
    position: relative;
    display: inline-block;
    &:first-child {
      width: 54%;
      display: none;
      ${media.medium`
        display:inline-block;
      `}
    }
    &:nth-child(2), &:nth-child(3), &:nth-child(4) {
      float: left;
      width: 33.3%;
      text-align: center;
      ${media.medium`
        float: none;
        width: 15%;
      `}
    }
  }

  ${props =>
    props.sticky &&
    css`
    padding-top: 10px;
    display: table-row;
    -webkit-overflow-scrolling: touch;
    position: fixed;
    top: 0;
    z-index: 99999999;
    width: auto;
    background: #fff;
    width: auto;
    right: 5px;
    left: 5px;
    height: 70px;
    `};

    ${media.medium`
    height: auto;
    position: relative;
    display: block;
    left: 0;
    right: 0;
    top: 0;
    padding-top: 28px;
    margin-top: 0px;
    padding-bottom: 15px;
    padding-left: 15px;
  `}
`;

export const TableHeadingFix = styled.ul`

  ${media.medium`
    margin: 4.5rem 0 0;
  `}
  li {
    position: relative;
    display: inline-block;
    &:first-child {
      width: 54%;
      display: none;
      ${media.medium`
        display:inline-block;
      `}
    }
    &:nth-child(2), &:nth-child(3), &:nth-child(4) {
      float: left;
      width: 33.3%;
      text-align: center;
      ${media.medium`
        float: none;
        width: 15%;
      `}
    }
  }


`;

export const TableContent = styled.ul`
    margin: 3.0rem 0 4.0rem;
  ${media.medium`
    font-size:1.6rem;
  `}
  ${props =>
    props.sticky &&
    css`
    margin-top: 87px;

    `};
    ${media.medium`
    margin: 2.0rem 0 4.0rem;
  `}

`;

export const TableRow = styled.li`
  display: block;
  margin: 0 0 1.6rem;
  :nth-of-type(odd){
    background-color:#fafafa;
  }
  color:${props => props.theme.tableFont};
  ${media.medium`
    margin: 0;
    padding: 1.2rem 15px;
    padding-left: 15x;
    padding-right: 0px;
  `}
  &:last-child {
    border-bottom:none;
  }
  ul li {
    line-height: 1.3;
    display: inline-block;
    margin: 0;
    padding: 0;
    word-break: keep-all;
    &:first-child {
      font-size: 1.5rem;
      width: 100%;
      margin: 0 0 1.1rem;
      text-align: center;
      padding: 10px 5px;
      ${media.medium`
        font-size: 1.6rem;
        width: 54%;
        margin: 0;
        text-align: left;
        padding:0px;
      `}

    }
    &:nth-child(2), &:nth-child(3), &:nth-child(4) {
      font-size: 1.6rem;
      float: left;
      width: 33.3%;
      padding: .5rem 0;
      text-align: center;
      // border-right: .1rem solid ${props => props.theme.colorGrey};
      color: ${props => props.theme.colorGrey};
      ${media.medium`
        float: none;
        width: 15%;
        padding: 0;
        border-right: none;
      `}
    }
    &:last-child {
      border-right: none;
    }
  }
`;

export const LI = styled.li`
  ${props => props.active && css`
    color: ${props => props.theme.colorBlue} !important;
  `}
`;
