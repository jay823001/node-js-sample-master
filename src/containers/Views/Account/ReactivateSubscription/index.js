import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import DataLayer from 'containers/App/datalayer';
import Center from 'components/Center';
import SubscriptionTable from './SubscriptionTable';
import Translation from 'translation/nextory-web-se';
import FamilySubscriptionTable from './SubscriptionTable.Family'
import Waypoint from 'react-waypoint';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { chooseSubscription } from 'containers/Views/Registration/actions';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  H1Main,
  Span,
  SubcriptionStage2,
  SubcriptionStage2Heading,
  H2Main,
  SubcriptionStage2Bottom,
  H2MainBox,
  PkgBoxContainer,
  SubcriptionStage2BoxBottom,
  BoxButtonPrimary,
  InnerWrapperd,
  ButtonPrimary
} from './StyledSubscriptionPage';

class ReactivateSubscription extends React.PureComponent {

  static propTypes = {
    chooseSubscription: PropTypes.func,
    ChosenSubscription: PropTypes.string,
  };
  state = {
    showFamilyPage: false,
    sticky: false,
    subscription: null,
  }

  getChosenSubscription = (subscription) => {
    this.setState({ subscription })
  }

  render() {

    const familyPackage2 = Translation.subscription_table.packages.family2;
    let subscription = (this.state.subscription === familyPackage2 || localStorage.getItem('sub-state') === familyPackage2)
    let showFamilyPage = (subscription) ? '/konto/aktivera-abonnemang#familj' : '/konto/aktivera-betalning';
    this.setState({ showFamilyPage, subscription: localStorage.getItem('sub-state') });
    let prevStep = '/konto/aktivera-abonnemang'

    const subscriptionTable = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />

        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>

              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>{Translation.account.reactivate_subscription.heading}</H1Main>
                  <H2Main>{Translation.account.reactivate_subscription.subheading}</H2Main>
                </SubcriptionStage2Heading>
              </Heading>

              <Waypoint
                onLeave={() => this.setState({ sticky: true })}
                onEnter={() => this.setState({ sticky: false })}
                topOffset={'0px'}
              >
                <p> </p>
              </Waypoint>

            </InnerWrapper>

            <SubscriptionTable sticky={this.state.sticky} getChosenSubscription={this.getChosenSubscription}/>

          </RegisterWrapper>
        </SubcriptionStage2>

        <SubcriptionStage2Bottom >
          <InnerWrapper>
            <Center>
              <ButtonPrimary large to={showFamilyPage} onClick={()=> this.setState({showFamilyPage: !showFamilyPage})}>
                {Translation.account.reactivate_subscription.buttons.continue}
              </ButtonPrimary>
            </Center>
          </InnerWrapper>

        </SubcriptionStage2Bottom>
      </ReactCSSTransitionGroup>
    )

    const familySubscriptionTable = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />

        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  <Span>
                    {Translation.campaign.subscriptionpage.registerwrapper.stage} 1 {' '}
                    {Translation.campaign.subscriptionpage.registerwrapper.of} {' '} 2
                  </Span>
                  <H1Main>{Translation.campaign.subscriptionpage.registerwrapper.family.heading}</H1Main>
                  <H2MainBox>{Translation.campaign.subscriptionpage.registerwrapper.family.subheading} </H2MainBox>
                </SubcriptionStage2Heading>

                <PkgBoxContainer />
              </Heading>

            </InnerWrapper>
            <FamilySubscriptionTable sticky={this.state.sticky} getChosenSubscription={this.getChosenSubscription} />
          </RegisterWrapper>
        </SubcriptionStage2>

        <SubcriptionStage2BoxBottom>
          <InnerWrapperd>
            <Center>
              <BoxButtonPrimary large to={prevStep} onClick={()=> this.setState({showFamilyPage: !showFamilyPage})}>
                {Translation.campaign.subscriptionpage.registerwrapper.button_go_back}
              </BoxButtonPrimary>
            </Center>
          </InnerWrapperd>
        </SubcriptionStage2BoxBottom>

      </ReactCSSTransitionGroup>
    )

    if (this.state.showFamilyPage === '/konto/aktivera-abonnemang#familj' && window.location.hash === '#familj') {
      return familySubscriptionTable
    } else {
      return subscriptionTable
    }
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription: state.signup.ChosenSubscription,
  };
}

export default connect(mapStateToProps, { chooseSubscription })(ReactivateSubscription);
