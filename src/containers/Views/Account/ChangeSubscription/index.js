import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import DataLayer from 'containers/App/datalayer';
import SubscriptionTable from './SubscriptionTable';
import FamilySubscriptionTable from './SubscriptionTable.Family';
import Translation from 'translation/nextory-web-se';
import Center from 'components/Center';
import Waypoint from 'react-waypoint';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  H1Main,
  SubcriptionStage2,
  SubcriptionStage2Heading,
  H2Main,
  SubcriptionStage2Bottom,
  H2MainBox,
  PkgBoxContainer,
  SubcriptionStage2BoxBottom,
  LoaderContainer,
  BoxButtonPrimary,
  InnerWrapperd
} from './StyledSubscriptionPage';

import LoadingIndicator from 'components/LoadingIndicator/';

class ChangeSubscription extends React.PureComponent {

  setLoadingState = (istrue) => {
    this.setState({ loading: istrue })
  }

  state = {
    loading: false,
    showFamilyPage: false,
    sticky: false,
    subscription: null,
  }

  render() {
    let subscription = (this.state.subscription === Translation.subscription_table.packages.silver ||
      this.state.subscription === Translation.subscription_table.packages.gold)
    let showFamilyPage = (!subscription) ? '/konto/aktivera-abonnemang#familj' : '/konto';

    this.setState({ showFamilyPage, subscription: localStorage.getItem('sub-state') });
    let previousButtonURL = '/konto/andra-abonnemang'

    const subscriptionTable = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />

        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>

              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>{Translation.account.changesubscription.heading}</H1Main>
                  <H2Main>{Translation.account.changesubscription.subheading}</H2Main>
                </SubcriptionStage2Heading>
              </Heading>

              <Waypoint
                onLeave={() => this.setState({ sticky: true })}
                onEnter={() => this.setState({ sticky: false })}
                topOffset={'0px'}
              >
                <p> </p>
              </Waypoint>

            </InnerWrapper>
            <SubscriptionTable showFamilyPage={() => this.setState({ showFamilyPage: !showFamilyPage })} sticky={this.state.sticky} />
          </RegisterWrapper>
        </SubcriptionStage2>

        <SubcriptionStage2Bottom >
          <InnerWrapper>
            <Center />
          </InnerWrapper>
        </SubcriptionStage2Bottom>
      </ReactCSSTransitionGroup>
    )

    const familySubscriptionTable = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>{Translation.campaign.subscriptionpage.registerwrapper.family.heading} </H1Main>
                  <H2MainBox>{Translation.campaign.subscriptionpage.registerwrapper.family.subheading} </H2MainBox>
                </SubcriptionStage2Heading>
                <PkgBoxContainer />
              </Heading>

            </InnerWrapper>
            <FamilySubscriptionTable setLoadingState={this.setLoadingState} />
          </RegisterWrapper>
        </SubcriptionStage2>

        <SubcriptionStage2BoxBottom>
          <InnerWrapperd>
            <Center>
              <BoxButtonPrimary large to={previousButtonURL} onClick={() => this.setState({ showFamilyPage: !showFamilyPage })}>
                {Translation.campaign.subscriptionpage.registerwrapper.button_go_back}
              </BoxButtonPrimary>
            </Center>
          </InnerWrapperd>
        </SubcriptionStage2BoxBottom>
      </ReactCSSTransitionGroup>
    )

    const Loader = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <LoaderContainer>
          <LoadingIndicator />
        </LoaderContainer>

      </ReactCSSTransitionGroup>
    )

    let PageContents = window.location.hash === '#familj' && this.state.showFamilyPage === '/konto/aktivera-abonnemang#familj' ? familySubscriptionTable : subscriptionTable;

    return this.state.loading ? Loader : PageContents
  }
}

export default ChangeSubscription;
