import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import { updateSubscription } from '../actions';
import Translation from 'translation/nextory-web-se';
import { SubTickNew, SubCrossNew } from 'components/Subscription/SubIcons';
import { isFamilyPackage } from 'containers/App/common';
import {
  apiUrl,
  internationalization_a,
  fetchAuth
} from 'containers/App/api';
import {
  TableWrapper,
  ButtonStyled,
  TableHeading,
  TableContent,
  TableRow,
  LI,
} from './StyledSubTable';
import {
  TableContainer,
  TableTitle,
  TableLI,
  TableRowContent,
  TableNo,
  ButtonPrimary,
  CenterButton,
  Change,
  ButtonSubmitStyled,
  PriceFrom
} from './StyledSubscriptionPage';

class SubscriptionTable extends React.Component {
  static propTypes = {
    ChosenSubscription: PropTypes.string,
    updateSubscription: PropTypes.func,
    authkey: PropTypes.string,
  };

  state = {
    loading: false,
    useraccess: true,
    subscriptionData: null,
    sticky: false,
  };

  async componentWillMount() {
    this.checkSublevel();
    this.setSubscription();
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      //console.log(data);
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package1: {
              packagename: subscriptions[0].subname,
              packageprice: subscriptions[0].subprice,
            }, package2: {
              packagename: subscriptions[1].subname,
              packageprice: subscriptions[1].subprice,
            }, package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.setSubscription()
  }

  checkSublevel = () => {
    if (this.props.UserDetails.allowedactions.includes('ACTIVATE_AGAIN')) {
      this.setState({ useraccess: false });
    }
  };

  setSubscription = () => { // set existing subscription at page load
    const currentSubscription = this.props.UserDetails.subscriptiondetails.subscriptionplanename
    let subscription = isFamilyPackage(currentSubscription) ? Translation.app.common.family2 : this.props.ChosenSubscription
    console.log(subscription)
    this.setState({ newSubscription: subscription });
    localStorage.setItem('sub-state', this.props.ChosenSubscription);
  };

  handleClick = (newSubscription, price) => { // set subscription when user clicks on a package
    this.setState({ newSubscription: newSubscription, packageprice: price });
    localStorage.setItem('sub-state', newSubscription);
  };

  // take inputed values and pass them to our redux saga updateSubscription
  submit = () => {
    const authtoken = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(authtoken.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    }

    this.setState({ loading: true });
    const authKey = this.props.authkey;
    const updatesub = this.state.newSubscription;
    const data = {
      authKey,
      updatesub,
    };
    //console.log(data)
    this.props.updateSubscription(data);
  };

  render() {
    const newSubscription = this.state.newSubscription;
    const currentSubscription = this.props.ChosenSubscription;
    let buttonUrl = isFamilyPackage() ? '/konto/andra-abonnemang#familj' : '/konto';

    let SaveCurrentSubButton =
    (currentSubscription === newSubscription && isFamilyPackage(currentSubscription)) ?
    (
      <ButtonPrimary large to={buttonUrl} onClick={() => this.props.showFamilyPage()}  >
        {Translation.account.changesubscription.buttons.save_current}
      </ButtonPrimary>
    ) : (
      <ButtonPrimary large to={buttonUrl} >
        {Translation.account.changesubscription.buttons.save_current}
      </ButtonPrimary>
    )

    return (

      <TableWrapper>
        <TableContainer>
          <TableHeading sticky={this.props.sticky}>
            <li>
              <TableTitle>{Translation.registration.stage1.tabletitle} </TableTitle>
            </li>
            <li>
              <ButtonStyled active={newSubscription === Translation.app.common.silver} onClick={() => this.handleClick(
                Translation.app.common.silver, (!this.state.subscriptionData ? '' : this.state.subscriptionData.package1.packageprice))}>
                {!this.state.subscriptionData ? '' : this.state.subscriptionData.package1.packagename}
              </ButtonStyled>
            </li>
            <li>
              <ButtonStyled active={newSubscription === Translation.app.common.gold} onClick={() => this.handleClick(
                Translation.app.common.gold, (!this.state.subscriptionData ? '' : this.state.subscriptionData.package2.packageprice))}>
                {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package2.packagename}
              </ButtonStyled>
            </li>
            <li>
              <ButtonStyled active={newSubscription === Translation.app.common.family2} onClick={() => this.handleClick(
                Translation.app.common.family2, (!this.state.subscriptionData ? '' : this.state.subscriptionData.package3.packageprice))}>
                {/* {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package3.packagename} */}
                {!this.state.subscriptionData ? '' : Translation.app.common.family}
              </ButtonStyled>
            </li>
          </TableHeading>

          <TableContent sticky={this.props.sticky}>
            <TableRowContent>
              <TableRow>
                <ul>
                  <li >
                    {Translation.subscription_table.listitems.monthly_cost}
                  </li>
                  <TableLI active={newSubscription === Translation.app.common.silver}>
                    <span>
                      {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package1.packageprice}
                      {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency}
                    </span>
                  </TableLI>
                  <TableLI active={newSubscription === Translation.app.common.gold}>
                    <span>
                      {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package2.packageprice}
                      {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency}
                    </span>
                  </TableLI>
                  <TableLI active={newSubscription === Translation.app.common.family2}>
                    <PriceFrom>{Translation.subscription_table.family.price_from} </PriceFrom>
                    <span>
                      {this.state.subscriptionData === null ? '' : this.state.subscriptionData.package3.packageprice}
                      {this.state.subscriptionData === null ? '' : ' ' + Translation.subscription_table.prices.currency}
                    </span>
                  </TableLI>
                </ul>
              </TableRow>
              <TableRow>

                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item2}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <TableNo>
                      1
            </TableNo>
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <TableNo>
                      1
            </TableNo>
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <TableNo>
                      2-4
            </TableNo>
                  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item3}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}><SubCrossNew /></LI>
                  <LI active={newSubscription === Translation.app.common.gold}><SubTickNew /></LI>
                  <LI active={newSubscription === Translation.app.common.family2}> <SubTickNew /></LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item4}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <SubTickNew />
                  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item5}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <SubTickNew />
                  </LI>
                </ul>
              </TableRow>

              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item6}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <SubTickNew />
                  </LI>
                </ul>
              </TableRow>
              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item7}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <SubTickNew />
                  </LI>
                </ul>
              </TableRow>
              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item8}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <SubTickNew />
                  </LI>
                </ul>
              </TableRow>
              <TableRow>
                <ul>
                  <li>
                    {Translation.subscription_table.listitems.item9}
                  </li>
                  <LI active={newSubscription === Translation.app.common.silver}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.gold}>
                    <SubTickNew />
                  </LI>
                  <LI active={newSubscription === Translation.app.common.family2}>
                    <SubTickNew />
                  </LI>
                </ul>
              </TableRow>
            </TableRowContent>
          </TableContent>

          <CenterButton>
            {currentSubscription === newSubscription && (
              SaveCurrentSubButton
            )}

            {currentSubscription !== newSubscription && (
              <Change>
                {this.state.loading ? (
                  <ButtonSubmitStyled disabled large>
                    <LoadingIndicatorSmall />
                  </ButtonSubmitStyled>
                ) :
                  newSubscription === Translation.app.common.family2 ||
                  newSubscription === Translation.app.common.family3 ||
                  newSubscription === Translation.app.common.family4 ? (
                    // <ButtonPrimary to='/konto/andra-abonnemang#familj' large></ButtonPrimary>
                    <ButtonPrimary to={buttonUrl} onClick={() => this.props.showFamilyPage()} large>
                      <span>
                        {isFamilyPackage() ?
                          (
                            Translation.account.changesubscription.buttons.change_to_b + ' ' + Translation.app.common.family
                          ) : (
                            Translation.account.changesubscription.buttons.change_to + ' ' + newSubscription
                          )
                        }
                      </span>
                    </ButtonPrimary>
                  ) : (
                      <ButtonSubmitStyled onClick={() => this.submit()} large>
                        <span>
                          {Translation.account.changesubscription.buttons.change_to}  {' '}
                          {newSubscription} {' '} {this.state.packageprice}{' '} {Translation.account.changesubscription.buttons.currency_per_month}
                        </span>
                      </ButtonSubmitStyled>
                    )
                }

                {/* TEXT CONTENT BELOW BUTTON */}
                {currentSubscription === Translation.app.common.family2 &&
                  newSubscription === Translation.app.common.gold &&

                  <p>{Translation.account.changesubscription.content.p1}</p>}

                {currentSubscription === Translation.app.common.family2 &&
                  newSubscription === Translation.app.common.silver &&

                  <p>{Translation.account.changesubscription.content.p2}</p>}

                {currentSubscription === Translation.app.common.gold &&
                  newSubscription === Translation.app.common.family2 &&

                  <p>{Translation.account.changesubscription.content.p3}</p>}

                {currentSubscription === Translation.app.common.gold &&
                  newSubscription === Translation.app.common.silver &&

                  <p>{Translation.account.changesubscription.content.p4}</p>}

                {currentSubscription === Translation.app.common.silver &&
                  newSubscription === Translation.app.common.family2 &&

                  <p>{Translation.account.changesubscription.content.p5}</p>}

                {currentSubscription === Translation.app.common.silver &&
                  newSubscription === Translation.app.common.gold &&

                  <p>{Translation.account.changesubscription.content.p6}</p>}
              </Change>
            )}
          </CenterButton>

          {!this.state.useraccess && <Redirect to={'/konto'} />}

        </TableContainer>
      </TableWrapper>

    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription:
      state.account.userData.UserDetails.subscriptiondetails.subscriptionplanename,
    authkey: state.account.userData.authkey,
    UserDetails: state.account.userData.UserDetails,
  };
}

export default connect(mapStateToProps, { updateSubscription, refreshAuth })(SubscriptionTable);
