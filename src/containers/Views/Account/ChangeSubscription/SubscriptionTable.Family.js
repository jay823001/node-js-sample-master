import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import { updateSubscription } from '../actions';
import Translation from 'translation/nextory-web-se';
import {
  apiUrl,
  internationalization_a,
  fetchAuth,
} from 'containers/App/api';
import {
  PkgBoxWrapperParent,
  PkgBoxWrapper,
  PkgBox,
  PriceContainer,
  PriceInfo,
  PriceButton,
  PriceTitle,
  PriceInfoTitle,
  PriceButtonBtn,
  Clear

} from './StyledSubscriptionPage';

class SubscriptionTable extends React.PureComponent {
  static propTypes = {
    ChosenSubscription: PropTypes.string,
    updateSubscription: PropTypes.func,
    authkey: PropTypes.string,
  };

  state = {
    loading: false,
    subscriptionData: null,
  };

  async componentWillMount() {
    this.fetchMetaData();
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      // console.log(data);
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }, package4: {
              packagename: subscriptions[3].subname,
              packageprice: subscriptions[3].subprice,
            }, package5: {
              packagename: subscriptions[4].subname,
              packageprice: subscriptions[4].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  // take inputed values and pass them to our redux saga updateSubscription
  submit = (value) => {
    const authtoken = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(authtoken.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    }

    this.setState({ loading: true });
    this.props.setLoadingState(true)
    const authKey = this.props.authkey;
    const updatesub = value
    const data = {
      authKey,
      updatesub,
    };
    //console.log(data)
    this.props.updateSubscription(data);
  };

  render() {
    const buttonUrl = '#'
    return (
      <PkgBoxWrapperParent>
        <PkgBoxWrapper>
          <PkgBox>
            <PriceContainer>
              <PriceInfo>
                <PriceTitle> 2
                  {' ' + Translation.subscription_table.family.users}
                </PriceTitle>
                <PriceInfoTitle>
                  <b>
                    {!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package3.packageprice} {' '}
                    {Translation.subscription_table.family.currency_per_month}
                  </b>
                </PriceInfoTitle>
              </PriceInfo>
              <PriceButton>
                <PriceButtonBtn onClick={() => this.submit(Translation.app.common.family2)} to={buttonUrl} >
                  {Translation.subscription_table.family.choose} {' '} <span> 2 {' ' + Translation.subscription_table.family.users} </span>
                </PriceButtonBtn>
              </PriceButton>
            </PriceContainer>
          </PkgBox>

          <PkgBox>
            <PriceContainer>
              <PriceInfo>
                <PriceTitle> 3
                  {' ' + Translation.subscription_table.family.users}
                </PriceTitle>
                <PriceInfoTitle>
                  <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package4.packageprice} {' '}
                    {Translation.subscription_table.family.currency_per_month}
                  </b>
                </PriceInfoTitle>
              </PriceInfo>
              <PriceButton>
                <PriceButtonBtn onClick={() => this.submit(Translation.app.common.family3)} to={buttonUrl} >
                  {Translation.subscription_table.family.choose} {' '} <span> 3 {' ' + Translation.subscription_table.family.users} </span>
                </PriceButtonBtn>
              </PriceButton>
            </PriceContainer>
          </PkgBox>

          <PkgBox>
            <PriceContainer>
              <PriceInfo>
                <PriceTitle> 4
                  {' ' + Translation.subscription_table.family.users}
                </PriceTitle>
                <PriceInfoTitle>
                  <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package5.packageprice} {' '}
                    {Translation.subscription_table.family.currency_per_month}
                  </b>
                </PriceInfoTitle>
              </PriceInfo>
              <PriceButton>
                <PriceButtonBtn onClick={() => this.submit(Translation.app.common.family4)} to={buttonUrl} >
                  {Translation.subscription_table.family.choose} {' '} <span> 4 {' ' + Translation.subscription_table.family.users} </span>
                </PriceButtonBtn>
              </PriceButton>
            </PriceContainer>
          </PkgBox>
          <Clear />
        </PkgBoxWrapper>
        <Clear />
      </PkgBoxWrapperParent>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription:
      state.account.userData.UserDetails.subscriptiondetails.subscriptionplanename,
    authkey: state.account.userData.authkey,
    UserDetails: state.account.userData.UserDetails,
  };
}

export default connect(mapStateToProps, { updateSubscription, refreshAuth })(SubscriptionTable);
