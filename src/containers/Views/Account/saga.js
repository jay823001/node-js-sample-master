/* eslint consistent-return:0 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { stopSubmit, startSubmit } from 'redux-form';
import { push } from 'react-router-redux';
import { Base64 } from 'js-base64';
import Cookies from 'universal-cookie';
import {
  fetchLogin,
  postUpdateProfile,
  postUpdateSubscription,
  postForgotPassword,
  postChangePassword,
  fetchUserDetails,
} from 'containers/App/api';
import { UPDATE_PROFILE, UPDATE_SUB, FORGOT_PASS, CHANGE_PASS_REQUEST } from './constants';
import { REFRESH_STORE } from './MyAccount/constants';
import { changepassRecieve } from './actions';
import { REQUEST_ERROR, CLEAR_ERROR, SET_AUTH } from '../Account/LoginPage/constants';
import { loginRecieve } from '../Account/LoginPage/actions';
import Translation from 'translation/nextory-web-se';

const cookies = new Cookies();

/*
* UpdateProfileSaga: will be fired on UPDATE_PROFILE actions
*/
function* updateProfileFlow(data) {
  let errors = {};

  // start submitting form
  yield put(startSubmit('myaccount'));
  // api call for updating profile information
  const response = yield call(postUpdateProfile, data);

  // if status is 200, we just refresh store
  if (response.status === 200) {
    // refresh store saga
    yield put({ type: REFRESH_STORE, authkey: data.data.auth });
  } else {
    errors = {
      email: Translation.api_messages.error.email1_error,
    };
    yield put({ type: CLEAR_ERROR });
  }

  // stop submitting form
  yield put(stopSubmit('myaccount', errors));
}
export function* updateProfileSaga() {
  yield takeLatest(UPDATE_PROFILE, updateProfileFlow);
}

/*
* UpdateSubscriptionSaga: will be fired on UPDATE_PROFILE actions
*/
function* updateSubscriptionFlow(data) {
  try {
    // api call
    yield call(postUpdateSubscription, data);

    // refresh store saga
    yield put({ type: REFRESH_STORE, authkey: data.data.authKey });

    // redirect user back to my account and show a notification
    yield put(push('/konto?subscription-changed'));
  } catch (error) {
    console.log(error);
  }
}
export function* updateSubscriptionSaga() {
  yield takeLatest(UPDATE_SUB, updateSubscriptionFlow);
}

/*
* ForgotPassSaga: will be fired on FORGOT_PASS actions
*/
function* forgotPassFlow(data) {
  let forgoterrors = {};

  // start submitting form
  yield put(startSubmit('forgot'));

  // api call
  const response = yield call(postForgotPassword, data);

  // if status is 200, we do nothing
  if (response.status === 200) {
    yield put(push('/glomt-losenord-skickat'));
  } else {
    forgoterrors = {
      email: response.error.details,
    };
  }

  yield put({ type: CLEAR_ERROR });
  yield put(stopSubmit('forgot', forgoterrors));
}
export function* forgotPassSaga() {
  yield takeLatest(FORGOT_PASS, forgotPassFlow);
}

/*
* ChangepassSaga: will be fired on CHANGE_PASS_REQUEST actions
*/
function* changepassFlow(data) {

  let errors = {};
  const password = data.data.password;

  yield put(startSubmit('changepassword'));

  // change password post request
  const status = yield call(postChangePassword, data);
  if (status.error) {
    yield put(push('/InvalidLinkChangPass'));
  } else {
    const email = status.data.useremail;

    // if change of pass was successful, we log in user, if not its falsy and we return error
    if (status.status === 200) {
      yield put(changepassRecieve());
      const data = {
        data: {
          email,
          password,
        },
      };

      // login post request
      const userdata = yield call(fetchLogin, data);
      if (userdata.status === 200) {
        // user DETAILS fetch
        const UserDetails = yield call(fetchUserDetails, userdata.authkey);

        // login done stop sending
        yield put(loginRecieve());

        // set a cookie token to preserve logged in user on page refresh
        userdata.data.email = data.data.email;
        userdata.data.p = data.data.password;
        userdata.data.authkey = userdata.authkey;
        userdata.data.refreshkey = userdata.refreshkey;
        userdata.data.UserDetails = UserDetails;
        const userEncode = Base64.encode(JSON.stringify(userdata.data));
        cookies.set('user', userEncode, { path: '/', maxAge: 1209600 });

        // set auth to true
        yield put({ type: SET_AUTH, newAuthState: true });

        // clear all errors
        yield put({ type: CLEAR_ERROR });
      }
    } else {
      yield put({ type: REQUEST_ERROR, error: 'false link' });
      errors = {
        newpassword: ' ',
        repeatpassword: ' ',
      };
    }

    yield put(stopSubmit('changepassword', errors));
  }

}
export function* changepassFlowSaga() {
  yield takeLatest(CHANGE_PASS_REQUEST, changepassFlow);
}
