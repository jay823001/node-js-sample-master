// account actions
import { UPDATE_PROFILE, UPDATE_SUB, FORGOT_PASS, CHANGE_PASS_REQUEST, CHANGE_PASS_SUCCESS, SET_SENDING_REQUEST } from './constants';

export const updateProfile = (data) => ({ type: UPDATE_PROFILE, data });
export const updateSubscription = (data) => ({ type: UPDATE_SUB, data });
export const forgotpassRequest = (data) => ({ type: FORGOT_PASS, data });
export const changepassRequest = (data) => ({ type: CHANGE_PASS_REQUEST, data });
export const changepassRecieve = () => ({ type: CHANGE_PASS_SUCCESS });
export const setSendingRequest = () => ({ type: SET_SENDING_REQUEST });