import React from 'react';
import styled from "styled-components";
import DataLayer from 'containers/App/datalayer';
import media from 'theme/styled-utils';
import H1 from 'components/Typography/H1';
import AccountWrapper from '../AccountWrapper';
import ChangeForm from './ChangeForm';
import {
  apiUrl,
  internationalization_b,
  fetchAuth,
} from '../../../App/api';
import InvalidLink from '../ForgotPassPage/InvalidLink'
import LoadingIndicator from '../../../../components/LoadingIndicator';

const H1Styled = H1.extend`
  margin-bottom: 1.5rem;
  ${media.medium`
    margin-bottom: 2rem;
  `};
`;

const InvalidLinkLoading = styled.div`
    position: relative;
    width: 200px;
    margin: 0 auto;
    top: 70px;
      ${media.medium`
        top: 120px;
      `};

`;

class ChangePassPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isInvalidLink: false,
      errorCodes: 1,
    };
  }

  async componentDidMount() {
    await this.PasswordChange();
  }

  async PasswordChange() {

    var ID = window.location.search.split('&')[0].split('=')[1];
    var AUTH_KEY = window.location.search.split('&')[1].split('=')[1];

    let data = {
      data: {
        authkey: AUTH_KEY,
        id: ID,
        password: "",
      },
      type: "CHANGE_PASS_REQUEST"
    };

    const response = await validateChangePassword(data);
    // console.log(response);
    if (response.error) {
      this.setState({
        isInvalidLink: true,
        errorCodes: response.error.code,
      });
    } else {
      this.setState({
        isInvalidLink: false,
        errorCodes: response.data.status,
      });
    }
  }

  showPasswordField() {
    if (this.state.isInvalidLink === false & this.state.errorCodes === 1) {
      return <InvalidLinkLoading> <LoadingIndicator /> </InvalidLinkLoading>;
    } else if (!this.state.isInvalidLink) {
      return <AccountWrapper>
        <DataLayer />
        <H1Styled>Ändra lösenord</H1Styled>
        <ChangeForm />
      </AccountWrapper>;
    } else {
      return <InvalidLink />;
    }
  }

  render() {
    return (
      <div>
        {this.showPasswordField()}
      </div>
    )
  }
}



async function validateChangePassword(data) {

  try {
    const response = await fetch(
      `${apiUrl}changepassword?userid=${data.data.id}&hash=${data.data.authkey}&newpassword=${
      data.data.password
      }&processType=VALIDATE_FORGOT_PASSWORD${internationalization_b}`,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(data),
      }
    );

    const status = await response.json();
    return status;

  } catch (error) {
    return error;
  }

};

export default ChangePassPage;