import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Base64 } from 'js-base64';
import { gup } from 'utils/helpFunctions';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import A from 'components/Typography/A';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import validate from './validate';
import { changepassRequest } from '../actions';
import Translation from 'translation/nextory-web-se';

const Submit = styled.button`
  margin-top: 1.5rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
`;
const SubmitSC = Button.withComponent(Submit);
const StyledLink = A.withComponent(Link);
const PreSubmit = StyledLink.extend`
  font-size: 1.2rem;
  margin: 1rem 0 0 0;
  display: block;
  ${media.medium`
    font-size: 1.4rem;
  `};
`;

class ChangeForm extends React.PureComponent {
  static defaultProps = {
    Route: '',
  };

  // take inputed values and pass them to our redux saga loginRequest
  submit = (values, changepassRequest) => {
    const authkey = gup('authkey', this.props.Route);
    const id = gup('id', this.props.Route);
    const password = Base64.encode(values.newpassword);
    const data = {
      authkey,
      id,
      password,
    };
    changepassRequest(data);
  };

  render() {
    const {
      handleSubmit,
      changepassRequest,
      submitting,
      pristine,
      valid,
      SendingRequest,
    } = this.props;
    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, changepassRequest))}>
        <Field
          name="newpassword"
          type="password"
          component={renderField}
          label={Translation.forms.labels.new_password}
        />
        <Field
          name="repeatpassword"
          type="password"
          component={renderField}
          label={Translation.forms.labels.repeat_password}
        />

        <Center>
          {this.props.Error && (
            <PreSubmit to="/glomt-losenord">{Translation.account.changepass.presubmit}</PreSubmit>
          )}
          {!this.props.Error && (
            <SubmitSC large type="submit" disabled={!valid || pristine || submitting}>
              {SendingRequest && <LoadingIndicatorSmall />}
              {!SendingRequest && 'Ändra lösenord'}
            </SubmitSC>
          )}
        </Center>

        {this.props.LoggedIn && <Redirect to={'/konto'} />}
      </Form>
    );
  }
}

ChangeForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  pristine: PropTypes.bool,
  valid: PropTypes.bool,
  changepassRequest: PropTypes.func,
  SendingRequest: PropTypes.bool,
  LoggedIn: PropTypes.bool,
  Route: PropTypes.string,
  Error: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
    Route: state.route.location.search,
    SendingRequest: state.account.currentlySending,
    Error: state.account.error,
  };
}

export default withRouter(
  connect(mapStateToProps, { changepassRequest })(
    reduxForm({
      form: 'changepassword',
      validate,
    })(ChangeForm)
  )
);
