import Translation from 'translation/nextory-web-se';

const validate = values => {
  const errors = {};

  if (!values.newpassword) {
    errors.newpassword = Translation.forms.validation.password;
  } else if (values.newpassword.length < 4 || values.newpassword.length > 25) {
    errors.newpassword = Translation.forms.validation.password_length;
  }

  if (!values.repeatpassword) {
    errors.repeatpassword = Translation.forms.validation.password;
  } else if (values.repeatpassword !== values.newpassword) {
    errors.repeatpassword = Translation.forms.validation.password_repeat;
  }

  return errors;
};

export default validate;
