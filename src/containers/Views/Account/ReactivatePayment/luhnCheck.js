function verifyValidCardNo(CreditOrDebitcardNumber) {
  var i, sum, digit, reverse, modified;
  // First, reverse the string and remove any non-numeric characters.
  reverse = '';

  for (i = 0; i < CreditOrDebitcardNumber.length; i++) {
    digit = parseInt(CreditOrDebitcardNumber.charAt(i), 10);
    if (digit >= 0 && digit <= 9) {
      reverse = digit + reverse;
    }
  }

  // Check for a bad string.
  if (reverse.length <= 1) {
    return false;
  }

  // Now run through each single digit to create a new string. Even digits are multiplied by two, odd digits are left alone.
  modified = '';

  for (i = 0; i < reverse.length; i++) {
    digit = parseInt(reverse.charAt(i), 10);

    if (i % 2 !== 0) {
      digit *= 2;
    }

    modified = modified + digit;
  }

  // Finally, add up all the single digits in this string.
  sum = 0;

  for (i = 0; i < modified.length; i++) {
    digit = parseInt(modified.charAt(i), 10);
    sum = sum + digit;
  }

  // If the resulting sum is an even multiple of ten (but not zero), the card number is good.
  if (sum !== 0 && sum % 10 === 0) {
    return true;
  } else {
    return false;
  }
}

export { verifyValidCardNo };
