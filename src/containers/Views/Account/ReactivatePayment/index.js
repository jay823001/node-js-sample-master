import React from 'react';
import PropTypes from 'prop-types';
import RegTrustly from './RegTrustly';
import Lock from './images/IconLock';
import Visa from './images/visa.png';
import Cookies from 'universal-cookie';
import RegCardForm from './RegCardForm';
import Master from './images/master.png';
import BankID from './images/bankid.svg';
import Trustly from './images/trustly.png';
import DataLayer from 'containers/App/datalayer';
import Translation from 'translation/nextory-web-se';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Base64 } from 'js-base64';
import { connect } from 'react-redux';
import { scroller } from 'react-scroll';
import { Redirect } from 'react-router';
import { parseJwt } from 'utils/helpFunctions';
import { getSubscriptionIdByName } from 'containers/App/common';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import { logoutRequest } from 'containers/Views/Account/MyAccount/actions';
import {
  toast,
  style,
  ToastContainer
} from 'react-toastify';
import {
  apiUrl,
  fetchAuth,
  internationalization_b,
  internationalizationLanguage
} from 'containers/App/api';
import {
  Cards,
  Clear,
  Mobile,
  Foldout,
  Heading,
  CardImg,
  CardImg2,
  WhiteBg,
  Payment,
  PaymentHead,
  SafePayment,
  FoldTrigger,
  HeadingText,
  PaymentTitle,
  InnerWrapper,
  SeccondHeading,
  RegisterWrapper
} from './StyledPayment';

class ReactivatePayment extends React.PureComponent {
  static propTypes = {
    LoggedInData: PropTypes.object,
    RouteSearch: PropTypes.string,
  };

  state = {
    card: false,
    bankid: false,
    activateUser: {},
  };

  componentDidMount() {
    this.checkAuth();
    if (this.props.RouteSearch.includes('trustlyfail')) {
      this.notifyTrustlyFail();
    }
  }

  checkAuth = () => {
    const data = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(data.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    } else {
      return this.getReactivateData();
    }
  };

  getReactivateData = async () => {
    let storedSubscription = localStorage.getItem('SwitchBoardSetSub')
    let subscriptionName = !storedSubscription ? this.props.ChoosenSub : storedSubscription;

    let subscriptionid = getSubscriptionIdByName(subscriptionName)

    try {
      const response = await fetch(
        `${apiUrl}usersignup?email=${this.props.LoggedInData.email}&confirmemail=${
        this.props.LoggedInData.email
        }&password=${
        this.props.LoggedInData.p
        }&subscriptionid=${subscriptionid}&serverdetails=127.0.0.1&&signuptype=PURCHASE_SUBSCRIPTION${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'nx-at': this.props.LoggedInData.authkey,
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data), // eslint-disable-line
        }
      );
      const data = await response.json();
      if (data.status === 200) {
        return this.setState({ activateUser: data.data });
      } else {
        return this.props.logoutRequest(this.props.LoggedInData.authkey);
      }
    } catch (error) {
      return console.log(error);
    }
  };

  notifyTrustlyFail = () => {
    toast.success(
      (Translation.account.reactivate_payment.notifytrustlyfail.success),
      {
        position: toast.POSITION.BOTTOM_CENTER,
        style: style({
          colorSuccess: '#ff3a54',
          width: '380px',
        }),
      }
    );
  };
  scrollTo() {
    if (window.innerWidth < 768) {
      scroller.scrollTo('scroll-to-payment', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart',
        offset: -20,
      });
    }
  };

  handleCardClick = () => {
    this.setState({ card: !this.state.card, bankid: false });
    this.scrollTo();
  };

  handleBankidClick = () => {
    this.setState({ bankid: !this.state.bankid, card: false });
    this.scrollTo();
  };

  render() {

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />

        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <HeadingText>{Translation.account.reactivate_payment.heading}</HeadingText>
              <SeccondHeading>{Translation.account.reactivate_payment.listitem}
              </SeccondHeading>
            </Heading>

            <Payment name="scroll-to-payment">
              <PaymentHead>
                <PaymentTitle>
                  {Translation.registration.stage3.regcardform.paymenttitle}
                </PaymentTitle>
                <SafePayment>
                  <Lock />  {Translation.registration.stage3.secure_server}
                </SafePayment>
                <Clear />
              </PaymentHead>
              <FoldTrigger active={this.state.card} onClick={this.handleCardClick}>
                {Translation.account.reactivate_payment.credit_or_debit_card}
                <CardImg>
                  <Cards visa alt="Visa" src={Visa} />
                  <Cards master alt="Mastercard" src={Master} />
                </CardImg>
              </FoldTrigger>
              <Foldout active={this.state.card}>
                <article>
                  <RegCardForm activateUser={this.state.activateUser} />
                </article>
              </Foldout>
              {
                internationalizationLanguage !== "FI" ?
                  <FoldTrigger active={this.state.bankid} onClick={this.handleBankidClick}>
                    {Translation.account.reactivate_payment.mobile_bankid}
                    <CardImg2>
                      <Mobile bankid alt="BankId" src={BankID} />
                      <Mobile trustly alt="Trustly" src={Trustly} />
                    </CardImg2>
                  </FoldTrigger>
                  : null
              }

              <Foldout active={this.state.bankid}>
                <RegTrustly activateUser={this.state.activateUser} />
              </Foldout>
            </Payment>
          </InnerWrapper>
        </RegisterWrapper>
        <WhiteBg active={this.state.card} active2={this.state.bankid} />

        {!this.props.LoggedInData.UserDetails.allowedactions.includes('ACTIVATE_AGAIN') && (
          <Redirect to={'/konto'} />
        )}

        <ToastContainer autoClose={5000} />
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChoosenSub: state.signup.ChosenSubscription,
    LoggedInData: state.account.userData,
    RouteSearch: state.route.location.search,
  };
}

export default connect(mapStateToProps, { refreshAuth, logoutRequest })(ReactivatePayment);
