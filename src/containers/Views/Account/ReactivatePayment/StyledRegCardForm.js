import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';
import styled, { css } from 'styled-components';
import Button from 'components/Buttons';
import { internationalizationLanguage } from 'containers/App/api';

const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 0rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
  ${props => props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.6;
      &:hover {
        background-color: ${props => props.theme.colorBlue};
      }
    `};
`;
export const SubmitSC = Button.withComponent(Submit);
export const ThreeColumn = styled.div`
display: inline-block;
float: left;
width: 30%;
margin-right: 5%;
  &:first-child{
    margin-right: 5%;
  }

  ${props => props.seccond &&
    css`
      img{
        display:none;
      }
    `};

${mediaCustom.medium`
  display: inline-block;
  float: left;
  width: 45%;
  ${props => props.seccond &&
      css`
      margin-right: 0;
      `};
  ${props => props.first &&
      css`
    margin-right: 10%;
  `};
`}
${props => props.last &&
    css`
    margin-right: 0;
`};


`;
export const Month = styled.div`
 // margin-right: 5%;
  img{
    display:none;
  }

`;
export const Tooltip = styled.aside`
  position: absolute;
  background: white;
  padding: 0.7rem 0.5rem 0.6rem;
  box-shadow: 0.1rem 0.1rem 1.3rem rgba(0, 0, 0, 0.25);
  border-radius: 0.7rem;
  z-index: 666;
  bottom: 6.9rem;
  left: -2.1rem;
  display: block;
  ${mediaCustom.medium`
    left: 0.9rem;
  `}
  img {
    max-width: 15rem;
    display: inline !important;
  }
  &:after {
    content: '';
    position: absolute;
    width: 0;
    height: 0;
    bottom: -17px;
    right: 22px;
    box-sizing: border-box;
    border: 9px solid #b1b0b0;
    border-color: transparent transparent #fff #fff;
    transform-origin: 0 0;
    transform: rotate(-45deg);
    box-shadow: -2px 2px 5px 0 rgba(0, 0, 0, 0.16);
  }
`;
export const PreSubmit = styled.span`
  font-size: 1.2rem;
  margin: 3rem 0 0 0;
  display: block;
  color: ${props => props.theme.colorRed};
  ${media.medium`
    font-size: 1.4rem;
  `} ul {
    padding: 0 0 1rem 1.5rem;
    list-style: disc;
  }
`;
export var FormWrapper;
export let ImgCvvQ;
export var CardField;
if (internationalizationLanguage === "FI") {
  FormWrapper = styled.div`
    label{
      min-height:40px;
    }
`;
  ImgCvvQ = styled.img`
display: none;
width: 1.8rem;
height: 1.8rem;
position: absolute;
top: 4.4rem;
right: 2rem;
  ${media.medium`
    display: inline-block;
    width: 1.8rem;
    height: 1.8rem;
    position: absolute;
    top: 55%;
    right: 2rem;
  `};
  ${mediaCustom.medium`
    top: 55%;
  `}
  ${mediaCustom.small`
    top: 55%;
  `}
  ${mediaCustom.usmall`
    top: 55%;
  `}
`;
  CardField = styled.div`
  img{
    top: 6.5rem;
  }
  span{
    position: relative;
    top: -12px;
  }
`;
}
else {
  FormWrapper = styled.div`
    label{
      min-height:0px;
    }
`;
  CardField = styled.div`
  img{
    top: 4.5rem;
  }
  span{
    position: relative;
    top: -12px;
  }
  `;
  ImgCvvQ = styled.img`

  display: inline-block;
  width: 1.8rem;
  height: 1.8rem;
  position: absolute;
  top: 4.4rem;
  right: 2rem;
  ${media.medium`
    display: inline-block;
    width: 1.8rem;
    height: 1.8rem;
    position: absolute;
    top: 46%;
    right: 2rem;
  `};
  ${mediaCustom.medium`
    top: 50%;
  `}
  ${mediaCustom.small`
    top: 46%;
  `}
  ${mediaCustom.usmall`
    top: 54%;
  `}
`;
}
export const CustomField = styled.div`
  input{
    border-radius: 27px;
   // border: solid 1px rgba(68, 68, 68, 0.5);
    height: 54px;
  }
  select{
    border-radius: 27px;
   // border: solid 1px rgba(68, 68, 68, 0.5);
    height: 54px;
  }
  label{
    font-size: 14px;
    font-weight: 500;
    line-height: 1.36;
    letter-spacing: normal;
    color: #585858;
    margin-bottom: 10px;
  }
  button {
    border-radius: 27px;
    text-transform: uppercase;
    font-size: 16px;
    font-weight: 600;
    ${mediaCustom.medium`
      font-size: 12px !important;
    `};
  }

`;
export const Cvv = styled.div`
  position:relative;
  img{
    display:none;
  }
`;
export const CvvHelp = styled.div`
  img{
    display:block !important;
  }
`;