import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H2 from '../../../components/Typography/H2';
import P from '../../../components/Typography/P';
import Translation from 'translation/nextory-web-se';

const InfoWrapper = styled.section`
  padding: 5.0rem 0;
  background-color: ${props => props.theme.colorWhite};
  ${media.medium`
    padding: 7.5rem 0;
  `}
`;

const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin:auto;
  ${media.medium`
    display: flex;
    justify-content: space-between;
  `}
  ${media.large`
    max-width: 95rem;
    padding: 0;
  `}
`;

const Column = styled.div`
  margin-bottom: 2.3rem;
  ${media.medium`
    margin-bottom: 0;
    width:45%;
    text-align:justify;
  `}
`;

const StyledH2 = styled(H2)`
  font-weight: 500;
`;

const SeoText = () => (
  <InfoWrapper>
    <InnerWrapper>

      <Column>
        <StyledH2>{Translation.seo.find_audio_books.heading}</StyledH2>
        <P>{Translation.seo.find_audio_books.content}</P>

        <StyledH2>{Translation.seo.listen_to_audio.heading}</StyledH2>
        <P>{Translation.seo.listen_to_audio.content.p1}</P>
        <P>{Translation.seo.listen_to_audio.content.p2}</P>
        <P>{Translation.seo.listen_to_audio.content.p3}</P>
      </Column>


      <Column>
        <StyledH2>{Translation.seo.ebooks_for_all.heading}</StyledH2>
        <P>{Translation.seo.ebooks_for_all.content}</P>

        <StyledH2>{Translation.seo.information_about_ebooks.heading}</StyledH2>
        <P>{Translation.seo.information_about_ebooks.content.p1}</P>
        <P>{Translation.seo.information_about_ebooks.content.p2}</P>
      </Column>
    </InnerWrapper>
  </InfoWrapper>
);

export default SeoText;
