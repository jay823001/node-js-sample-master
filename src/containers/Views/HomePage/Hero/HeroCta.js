import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Button from 'components/Buttons';
import Translation from 'translation/nextory-web-se';

const CallToAction = styled(Button)`
  margin-top: 5rem;
  ${media.medium`
    margin-top: 7rem;
  `};
`;

class HeroCta extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };

  render() {
    let activateAcc = false;

    if ('allowedactions' in this.props.activateAcc) {
      if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
        activateAcc = true;
      }
    }

    return (
      <span>
        {!this.props.loggedIn &&
          !this.props.activeReg && (
            <CallToAction primary to="/register/subscription#steg-1">
              {Translation.hero.buttons.try_free}
            </CallToAction>
          )}

        {this.props.activeReg &&
          this.props.activeGiftReg !== 'REDEEM_GIFTCARD' &&
          this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' && (
            <CallToAction primary to="/register/card">
              {Translation.hero.buttons.complete_registration}
            </CallToAction>
          )}

        {this.props.activeGiftReg === 'REDEEM_CAMPAIGN' && (
          <CallToAction primary to="/register-campaign/subscription">
            {Translation.hero.buttons.complete_registration}
          </CallToAction>
        )}

        {this.props.activeGiftReg === 'REDEEM_GIFTCARD' && (
          <CallToAction primary to="/presentkort/subscription">
            {Translation.hero.buttons.complete_registration}
          </CallToAction>
        )}

        {activateAcc &&
          this.props.loggedIn &&
          this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' &&
          this.props.activeGiftReg !== 'REDEEM_GIFTCARD' && (
            <CallToAction primary to="/konto/aktivera-abonnemang">
              {Translation.hero.buttons.enable_nextory_now}
            </CallToAction>
          )}
      </span>
    );
  }
}

export default HeroCta;
