import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Hero from 'components/Hero';
import HeroText from 'components/Hero/HeroText';
import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import HeroCta from './HeroCta';
import ImgHeroPhone from './nextory-wallpaper-phone.jpg';
import ImgHeroDesktop from './nextory-wallpaper.jpg';
import Translation from 'translation/nextory-web-se';

const StyledHero = styled(Hero)`
  height: calc(100vh - 18.5rem);
  min-height: 40.0rem;
  background-image: url(${ImgHeroPhone});
  &:after {
    background-color: rgba(0,0,0,.5);
  }
  ${media.xsmall`
    height: calc(100vh - 24rem);
    background-image: url(${ImgHeroDesktop});
  `}
  ${media.medium`
    height: calc(100vh - 22rem);
    background-image: url(${ImgHeroDesktop});
  `}
  ${media.large`
    height: calc(100vh - 18rem);
  `}
`;

const StyledHeroText = styled(HeroText)`
  ${media.large`
    text-align:left;
  `};
`;

class HeroHome extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activateAcc: PropTypes.object,
    activeGiftReg: PropTypes.string,
  };

  render() {
    return (
      <StyledHero>
        <StyledHeroText>
          <H1 huge>
            {Translation.hero.heading_part1}
            <br />
            {Translation.hero.heading_part2}
          </H1>
          <H2 huge>
            {Translation.hero.subheading_part1}
            <br /> {Translation.hero.subheading_part2}
          </H2>

          <HeroCta
            activeGiftReg={this.props.activeGiftReg}
            activeReg={this.props.activeReg}
            loggedIn={this.props.loggedIn}
            activateAcc={this.props.activateAcc}
          />
        </StyledHeroText>
      </StyledHero>
    );
  }
}

export default HeroHome;
