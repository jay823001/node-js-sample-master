import styled from 'styled-components';
import media from 'theme/styled-utils';

const BookToggle = styled.ul`
  display: block;
  margin: 0 auto 2rem;
  padding: 0;
  text-align: center;
  font-size: 1.2rem;
  ${media.medium`
    font-size: 1.5rem;
  `}
  li {
    display: inline-block;
    cursor:pointer;
    color: ${props => props.theme.colorDarkGrey};
    
    &:last-child:after {
      display: none;
    }  
    &.active {
      color:${props => props.theme.colorBlue};
    }
    &:hover {
      text-decoration:underline;
    }
    &:after {
      content: '';
      width: .1rem;
      height: 1.2rem;
      position: relative;
      top: .2rem;
      background-color: ${props => props.theme.colorGrey};
      display: inline-block;
      margin: 0 .7rem;
      ${media.medium`
        margin: 0 1.5rem;
      `}
    }
  }
`;

export default BookToggle;
