import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import LoadingIndicator from 'components/LoadingIndicator';
import BookSlide from 'components/Slider';
import Book from 'components/Books/BookList/BookCover';
import BookToggle from './BookToggle';
import Translation from 'translation/nextory-web-se';

class BookList extends React.PureComponent {
  static propTypes = {
    books: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
    ]),
  };

  state = {
    BookSlider: true,
    SoundSlider: false,
    NewsSlider: false,
  };

  activeEbook = () => {
    this.setState({
      BookSlider: true,
      SoundSlider: false,
      NewsSlider: false,
    });
  }
  activeSoundbook = () => {
    this.setState({
      BookSlider: false,
      SoundSlider: true,
      NewsSlider: false,
    });
  }
  activeNews = () => {
    this.setState({
      BookSlider: false,
      SoundSlider: false,
      NewsSlider: true,
    });
  }

  renderBookListSoundbook() {
    if (this.props.books.length < 1) return false;
    return this.props.books[1].books.map(book => {
      const cleanurl = book.weburl.substring(0, book.weburl.indexOf('?'));
      return (
        <div key={book.bookid}>
          <Book
            formattype={book.formattype.format}
            booktitle={book.title}
            relatedbook={book.relatedbookid}
            bookCover={book.coverimg}
            url={cleanurl}
          />
        </div>
      )
 ;
    });
  }
  renderBookListEbook() {
    if (this.props.books.length < 1) return false;
    return this.props.books[0].books.map(book => {
      const cleanurl = book.weburl.substring(0, book.weburl.indexOf('?'));
      return (
        <div key={book.bookid}>
          <Book
            formattype={book.formattype.format}
            booktitle={book.title}
            relatedbook={book.relatedbookid}
            bookCover={book.coverimg}
            url={cleanurl}
          />
        </div>
      );
    });
  }
  renderBookListNews() {
    if (this.props.books.length < 1) return false;
    return this.props.books[2].books.map(book => {
      const cleanurl = book.weburl.substring(0, book.weburl.indexOf('?'));
      return (
        <div key={book.bookid}>
          <Book
            formattype={book.formattype.format}
            booktitle={book.title}
            relatedbook={book.relatedbookid}
            bookCover={book.coverimg}
            url={cleanurl}
          />
        </div>
      );
    });
  }

  render() {
    const { BookSlider, SoundSlider, NewsSlider } = this.state;

    return (
      <div>
        <BookToggle>
          {BookSlider
            ? <li className="active">{Translation.tabs.books.links.popular_ebooks}</li>
            : <li onClick={this.activeEbook}>{Translation.tabs.books.links.popular_ebooks}</li>
          }
          {SoundSlider
            ? <li className="active">{Translation.tabs.books.links.popular_audiobooks}</li>
            : <li onClick={this.activeSoundbook}>{Translation.tabs.books.links.popular_audiobooks}</li>
          }
          {NewsSlider
            ? <li className="active">{Translation.tabs.books.links.news}</li>
            : <li onClick={this.activeNews}>{Translation.tabs.books.links.news}</li>
          }
        </BookToggle>

        {this.props.books.length > 0 &&
          <div>

            {BookSlider &&
              <BookSlide>
                {this.renderBookListEbook()}
              </BookSlide>
            }

            {SoundSlider &&
              <BookSlide>
                {this.renderBookListSoundbook()}
              </BookSlide>
            }

            {NewsSlider &&
              <BookSlide>
                {this.renderBookListNews()}
              </BookSlide>
            }

          </div>
        }

        {!this.props.books.length > 0 &&
          <LoadingIndicator />
        }

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    books: state.bookdata.books,
  };
}

export default connect(mapStateToProps)(BookList);
