import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import {
  requestApiData,
  requestApiDataEbook,
  requestApiDataSbook,
  requestApiDataCategories,
} from 'containers/Views/Books/actions';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import HeroHome from './Hero/Hero';
import SeoText from './SeoText';
import Tabs from './Tabs';
import Translation from 'translation/nextory-web-se';

class HomePage extends React.PureComponent {
  static propTypes = {
    LoggedIn: PropTypes.bool,
    ActiveReg: PropTypes.bool,
    GiftReg: PropTypes.string,
    requestApiData: PropTypes.func,
    requestApiDataEbook: PropTypes.func,
    requestApiDataSbook: PropTypes.func,
    requestApiDataCategories: PropTypes.func,
    UserDetails: PropTypes.object,
  };

  componentDidMount() {
    this.checkAuth();
    this.props.requestApiDataCategories();
  }

  checkAuth = () => {
    if (new Cookies().get('user')) {
      const data = JSON.parse(Base64.decode(new Cookies().get('user')));
      const jwt = parseJwt(data.authkey);
      const current_time = Date.now() / 1000;
      if (jwt.exp < current_time) {
        return this.props.refreshAuth();
      } else {
        return (
          this.props.requestApiData(this.props.UserAuthkey) &&
          this.props.requestApiDataEbook(this.props.UserAuthkey) &&
          this.props.requestApiDataSbook(this.props.UserAuthkey)
        );
      }
    } else {
      return (
        this.props.requestApiData(this.props.UserAuthkey) &&
        this.props.requestApiDataEbook(this.props.UserAuthkey) &&
        this.props.requestApiDataSbook(this.props.UserAuthkey)
      );
    }
  };

  render() {
    const seotitle = (Translation.homepage.seotitle);
    const seodesc =(Translation.homepage.seodescription);
    return (
      <div>
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <DataLayer />
        <HeroHome
          activeGiftReg={this.props.GiftReg}
          activeReg={this.props.ActiveReg}
          loggedIn={this.props.LoggedIn}
          activateAcc={this.props.UserDetails}
        />
        <Tabs
          activeGiftReg={this.props.GiftReg}
          activeReg={this.props.ActiveReg}
          loggedIn={this.props.LoggedIn}
          activateAcc={this.props.UserDetails}
        />
        <SeoText />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ActiveReg: state.signup.activeReg,
    LoggedIn: state.account.loggedIn,
    UserDetails: state.account.userData.UserDetails,
    UserAuthkey: state.account.userData.authkey,
    GiftReg: state.signup.userRegPayment,
  };
}

export default connect(mapStateToProps, {
  requestApiData,
  requestApiDataEbook,
  requestApiDataSbook,
  requestApiDataCategories,
  refreshAuth,
})(HomePage);
