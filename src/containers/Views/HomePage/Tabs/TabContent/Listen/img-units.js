import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import Img from 'components/Img';
import Image from './nextory-apps.jpg';
import Translation from 'translation/nextory-web-se';

const StyledImg = styled(Img)`
  display: none;
  ${media.medium`
    float: left;
    width: 50%;
    margin-right: 8%;
    display: inline-block;
  `}
`;

const StyledImgPhone = styled(Img)`
  display: block;
  width: 90%;
  margin: 2rem auto 0 auto;
  ${media.medium`
    display: none;
  `}
`;

export const ImgUnits = () =>
  <StyledImg src={Image} alt={Translation.tabs.listen.img.alt_text2} />
;

export const ImgUnitsPhone = () =>
  <StyledImgPhone src={Image} alt={Translation.tabs.listen.img.alt_text2} />
;
