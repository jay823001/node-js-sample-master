import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import Img from 'components/Img';
import Image from './nextory-appicon.png';
import Translation from 'translation/nextory-web-se';

const StyledImg = styled(Img)`
  display: none;
  ${media.medium`
    width: 7rem;
    margin: 0 0 2rem;
    display: inline-block;
  `}
`;

const ImgAppicon = () =>
  <StyledImg src={Image} alt={Translation.tabs.listen.img.alt_text1} />
;

export default ImgAppicon;
