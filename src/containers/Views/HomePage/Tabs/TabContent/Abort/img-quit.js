import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import Img from 'components/Img';
import Image from './quit-sub.jpg';
import Translation from 'translation/nextory-web-se';

const StyledImg = styled(Img)`
  ${media.medium`
    max-width: 31rem;
    position: relative;
    top: -3.5rem;
  `}
  ${media.xlarge`
    max-width: 40rem;
  `}
`;

const ImgUnits = () =>
  <StyledImg src={Image} alt={Translation.tabs.abort.images_alt_text} />
;

export default ImgUnits;
