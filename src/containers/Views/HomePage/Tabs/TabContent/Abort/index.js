import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H2 from 'components/Typography/H2';
import Button from 'components/Buttons';
import ImgQuit from './img-quit';
import Translation from 'translation/nextory-web-se';

const TabWrapper = styled.section`
  position: relative;
  padding: 4rem 1.5rem;
  background-color: white;
  text-align: center;
  ${media.medium`
    padding: 6rem 1.5rem;
    text-align: left;
  `} ${media.large`
    padding: 7rem 1.5rem;
  `}
  &:after {
    clear: both;
    content: ' ';
    display: table;
  }
`;
const TabInnerWrapper = styled.div`
  max-width: 81rem;
  margin: auto auto 4rem;
`;

const TextWrapper = styled.div`
  margin-bottom: 1rem;
  ${media.medium`
    display: inline-block;
    width: 50%;
    margin-bottom: 0;
    vertical-align: top;
  `} a {
    margin-top: 2rem;
    ${media.medium`
      margin-top: 2.5rem;
    `};
  }
`;

class AbortTab extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activeGiftReg: PropTypes.string,
    activateAcc: PropTypes.object,
  };

  render() {
    let activateAcc = false;
    if ('allowedactions' in this.props.activateAcc) {
      if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
        activateAcc = true;
      }
    }

    return (
      <TabWrapper>
        <TabInnerWrapper>
          <TextWrapper>
            <H2>{Translation.tabs.abort.heading}</H2>

            {!this.props.loggedIn &&
              !this.props.activeReg && (
                <Button to="/register/subscription#steg-1">
                  {Translation.tabs.abort.buttons.try_free}
                </Button>
              )}

            {this.props.activeReg &&
              this.props.activeGiftReg === 'REDEEM_GIFTCARD' && (
                <Button to="/presentkort/subscription">
                  {Translation.tabs.abort.buttons.complete_registration}
                </Button>
              )}

            {this.props.activeReg &&
              this.props.activeGiftReg === 'REDEEM_CAMPAIGN' && (
                <Button to="/register-campaign/subscription">
                  {Translation.tabs.abort.buttons.complete_registration}
                </Button>
              )}

            {this.props.activeReg &&
              this.props.activeGiftReg !== 'REDEEM_GIFTCARD' &&
              this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' && (
                <Button to="/register/card">
                  {Translation.tabs.abort.buttons.complete_registration}
                </Button>
              )}

            {activateAcc &&
              this.props.loggedIn &&
              this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' &&
              this.props.activeGiftReg !== 'REDEEM_GIFTCARD' && (
                <Button to="/konto/aktivera-abonnemang">
                  {Translation.tabs.abort.buttons.enable_nextory_now}
                </Button>
              )}

            {activateAcc &&
              this.props.loggedIn &&
              this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' &&
              this.props.activeGiftReg !== 'REDEEM_GIFTCARD' && (
                <Button to="/konto/aktivera-abonnemang">Aktivera Nextory nu</Button>
              )}
          </TextWrapper>
          <ImgQuit />
        </TabInnerWrapper>
      </TabWrapper>
    );
  }
}

export default AbortTab;
