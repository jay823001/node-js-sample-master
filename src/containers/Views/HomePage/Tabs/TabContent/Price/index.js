import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H2 from 'components/Typography/H2';
import Button from 'components/Buttons';
import SubscriptionTable from './SubscriptionTable';
import Translation from 'translation/nextory-web-se';

const TabWrapper = styled.section`
  position: relative;
  padding: 4rem 1.5rem 0;
  background-color: white;
  ${media.medium`
    padding: 6rem 1.5rem 0;
  `} ${media.large`
    padding: 7rem 1.5rem 0;
  `}
  &:after {
    clear: both;
    content: ' ';
    display: table;
  }
`;
const TabInnerWrapper = styled.div`
  max-width: 114rem;
  margin: auto;
`;

const TextWrapper = styled.div`
  margin: 0 0 3rem;
  text-align: center;
  ${media.medium`
    margin: 0 0 6rem;
  `} h2 {
    ${media.xlarge`
      display: inline-block;
      vertical-align: top;
    `};
  }
  a {
    margin-top: -1rem;
    ${media.xlarge`
      margin-left: 2rem;
    `};
  }
  h2 + a {
    margin-top: 2rem;
    ${media.medium`
      margin-top: 2.5rem;
    `} ${media.xlarge`
      margin-top: -1rem;
    `};
  }
`;

class PriceTab extends React.PureComponent {
  static propTypes = {
    loggedIn: PropTypes.bool,
    activeReg: PropTypes.bool,
    activeGiftReg: PropTypes.string,
    activateAcc: PropTypes.object,
  };

  render() {
    let activateAcc = false;
    if ('allowedactions' in this.props.activateAcc) {
      if (this.props.activateAcc.allowedactions.includes('ACTIVATE_AGAIN')) {
        activateAcc = true;
      }
    }

    return (
      <TabWrapper>
        <TabInnerWrapper>
          <TextWrapper>
            <H2>{Translation.tabs.price.heading}</H2>

            {!this.props.loggedIn &&
              !this.props.activeReg && (
                <Button to="/register/subscription#steg-1">
                  {Translation.tabs.price.buttons.try_free}
                </Button>
              )}

            {this.props.activeReg &&
              this.props.activeGiftReg === 'REDEEM_GIFTCARD' && (
                <Button to="/presentkort/subscription">
                  {Translation.tabs.price.buttons.complete_registration}
                </Button>
              )}

            {this.props.activeReg &&
              this.props.activeGiftReg === 'REDEEM_CAMPAIGN' && (
                <Button to="/register-campaign/subscription">
                  {Translation.tabs.buttons.price.complete_registration}
                </Button>
              )}

            {this.props.activeReg &&
              this.props.activeGiftReg !== 'REDEEM_GIFTCARD' &&
              this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' && (
                <Button to="/register/card">
                  {Translation.tabs.price.buttons.complete_registration}
                </Button>
              )}

            {activateAcc &&
              this.props.loggedIn &&
              this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' &&
              this.props.activeGiftReg !== 'REDEEM_GIFTCARD' && (
                <Button to="/konto/aktivera-abonnemang">
                  {Translation.tabs.price.buttons.enable_nextory_now}
                </Button>
              )}

            {activateAcc &&
              this.props.loggedIn &&
              this.props.activeGiftReg !== 'REDEEM_CAMPAIGN' &&
              this.props.activeGiftReg !== 'REDEEM_GIFTCARD' && (
                <Button to="/konto/aktivera-abonnemang">Aktivera Nextory nu</Button>
              )}
          </TextWrapper>
          <SubscriptionTable />
        </TabInnerWrapper>
      </TabWrapper>
    );
  }
}

export default PriceTab;
