import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import { SubCheck, SubCross } from './SubIcons';
import Translation from 'translation/nextory-web-se';

const TableWrapper = styled.article`
  max-width: 86.6rem;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 6rem;
`;

const TableHeading = styled.ul`
  margin: 3rem 0 0;
  font-weight: 600;
  li {
    position: relative;
    display: inline-block;
    &:first-child {
      width: 54%;
      display: none;
    }
    &:nth-child(2),
    &:nth-child(3),
    &:nth-child(4) {
      float: left;
      width: 33.3%;
      text-align: center;
    }
  }
  ${media.large`
    margin: 4.5rem 0 0;
    li:first-child {
      display:inline-block;
    }
    li:nth-child(2),
    li:nth-child(3),
    li:nth-child(4) {
      float: none;
      width: 14.5%;
    }
  `};
`;

const TableContent = styled.ul`
  margin: 0.5rem 0 4rem;
  ${media.large`
    font-size:1.6rem;
  `};
`;

const TableRow = styled.li`
  display: block;
  margin: 0 0 1.6rem;
  &:last-child {
    border-bottom: none;
  }
  ul li {
    line-height: 1.3;
    display: inline-block;
    margin: 0;
    padding: 0;
    word-break: keep-all;
    &:first-child {
      font-size: 1.3rem;
      width: 100%;
      margin: 0 0 1.1rem;
      text-align: center;
    }
    &:nth-child(2),
    &:nth-child(3),
    &:nth-child(4) {
      font-size: 1.6rem;
      float: left;
      width: 33.3%;
      padding: 0.5rem 0;
      text-align: center;
      border-right: 0.1rem solid #bdbdbd;
    }
  }
  ${media.large`
    margin: 0;
    padding: 1.2rem 0;
    border-bottom: .1rem solid ${props => props.theme.colorGrey};
    ul li:first-child {
      font-size: 1.6rem;
      width: 54%;
      margin: 0;
      text-align: left;
    }
    ul li:nth-child(2),
    ul li:nth-child(3),
    ul li:nth-child(4) {
      float: none;
      width: 14.5%;
      padding: 0;
      border-right: none;
    }
  `};
`;

const SubscriptionTable = () => (
  <TableWrapper>
    <TableHeading>
      <li />
      <li>{Translation.subscription_table.buttons.silver}</li>
      <li>{Translation.subscription_table.buttons.gold}</li>
      <li>{Translation.subscription_table.buttons.family}</li>
    </TableHeading>

    <TableContent>
      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item1b}</li>
          <li>{Translation.subscription_table.prices.silver}</li>
          <li>{Translation.subscription_table.prices.gold}</li>
          <li>{Translation.subscription_table.prices.family}</li>
        </ul>
      </TableRow>

      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item2}</li>
          <li>
            <SubCross />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>

      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item3}</li>
          <li>1</li>
          <li>1</li>
          <li>4</li>
        </ul>
      </TableRow>

      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item4}</li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>

      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item5}</li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>

      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item6}</li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>
      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item7}</li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>
      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item8}</li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>
      <TableRow>
        <ul>
          <li>{Translation.subscription_table.listitems.item9}</li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
          <li>
            <SubCheck />
          </li>
        </ul>
      </TableRow>
    </TableContent>
  </TableWrapper>
);

export default SubscriptionTable;
