import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import { TabBook, TabListen, TabPrice, TabLeave } from './TabIcons';

const TabItemStyled = styled.li`
  font-size: 1.2rem;
  font-weight: 600;
  line-height: 1.15;
  float: left;
  width: 25%;
  height: 8.0rem;
  margin: 0;
  padding: .5rem 0 0;
  cursor: pointer;
  text-align: center;
  color: ${props => props.theme.colorBlack};
  ${media.medium`
    padding: .3rem 0 0;
  `}
  ${media.large`
    font-size: 1.4rem;
    height: 11.5rem;
    padding: .8rem 0 0;
  `}

  &:nth-child(1),&:nth-child(2),&:nth-child(3) {
    border-right: 1px solid ${props => props.theme.colorGrey};
  }

  &:hover {
    color: ${props => props.theme.colorBlue};
    svg path {
      fill: ${props => props.theme.colorBlue};
    }
  }

  ${props => props.isActive && css`
    color: ${props => props.theme.colorBlue};
    border-bottom: 3px solid ${props => props.theme.colorBlue};
    svg path {
      fill: ${props => props.theme.colorBlue};
    }
  `}
`;

const Small = styled.small`
  display: block;
  font-size: 1.2rem;
  ${media.large`
    display:none;
  `}
`;

const Span = styled.span`
  display: none;
  ${media.large`
    display:block;
  `}
`;


class TabItem extends Component {
  static propTypes = {
    onClick: PropTypes.func,
    isActive: PropTypes.bool,
    tabIndex: PropTypes.number,
    tabHeadingSmall: PropTypes.string,
    tabHeading: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
    ]),
  };

  handleTabClick = () => {
    this.props.onClick(this.props.tabIndex);
  }

  render() {
    const { isActive, tabHeading, tabHeadingSmall } = this.props;
    let icon = 0;

    if (this.props.tabIndex === 0) {
      icon = <TabBook />;
    } else if (this.props.tabIndex === 1) {
      icon = <TabListen />;
    } else if (this.props.tabIndex === 2) {
      icon = <TabPrice />;
    } else {
      icon = <TabLeave />;
    }

    return (
      <TabItemStyled
        isActive={isActive}
        onClick={this.handleTabClick}
      >
        {icon}
        <Span>{tabHeading}</Span>
        <Small>{tabHeadingSmall}</Small>
      </TabItemStyled>
    );
  }
}

export default TabItem;
