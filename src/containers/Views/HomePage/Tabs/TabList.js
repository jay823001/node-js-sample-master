import React from 'react';
import media from 'theme/styled-utils';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { findPos } from 'utils/helpFunctions';

const TabInnerwrapper = styled.div`
  max-width: 120rem;
  margin: auto;
  ${media.small`
    padding: 0 1.5rem;
  `}
`;

const TabWrapper = styled.section`
  background-color: white;
  position: relative;
  z-index: 30;
  border-bottom: .1rem solid ${props => props.theme.colorGrey};
`;


class TabList extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  state = {
    activeTabIndex: 0,
  };

  handleTabClick = (tabIndex) => {
    window.scroll(0, findPos(document.getElementById('tabs')));
    this.setState({
      activeTabIndex: tabIndex,
    });
  }

  // Encapsulate <Tabs/> component API as props for <Tab/> children
  renderChildrenWithTabsApiAsProps() {
    return React.Children.map(this.props.children, (child, index) => React.cloneElement(child, {
      onClick: this.handleTabClick,
      tabIndex: index,
      isActive: index === this.state.activeTabIndex,
    }));
  }

  // Render current active tab content
  // eslint-disable-next-line consistent-return
  renderActiveTabContent() {
    const { children } = this.props;
    const { activeTabIndex } = this.state;
    if (children[activeTabIndex]) {
      return children[activeTabIndex].props.children;
    }
  }

  render() {
    return (
      <div id="tabs">
        <TabWrapper>
          <TabInnerwrapper>

            <ul>
              {this.renderChildrenWithTabsApiAsProps()}
            </ul>

          </TabInnerwrapper>
        </TabWrapper>
        <div>
          {this.renderActiveTabContent()}
        </div>
      </div>
    );
  }
}

export default TabList;
