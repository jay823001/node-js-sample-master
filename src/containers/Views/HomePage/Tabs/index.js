import React from 'react';
import PropTypes from 'prop-types';
import TabList from './TabList';
import TabItem from './TabItem';
import BookTab from './TabContent/Books';
import ListenTab from './TabContent/Listen';
import PriceTab from './TabContent/Price';
import AbortTab from './TabContent/Abort';
import Translation from 'translation/nextory-web-se';

const Tabs = props => (
  <TabList>
    <TabItem tabHeadingSmall={'Böcker'} tabHeading={Translation.tabs.books.tabname}>
      <BookTab
        loggedIn={props.loggedIn}
        activeReg={props.activeReg}
        activeGiftReg={props.activeGiftReg}
        activateAcc={props.activateAcc}
      />
    </TabItem>

    <TabItem tabHeadingSmall={'Läs/lyssna'} tabHeading={Translation.tabs.listen.tabname}>
      <ListenTab
        loggedIn={props.loggedIn}
        activeReg={props.activeReg}
        activeGiftReg={props.activeGiftReg}
        activateAcc={props.activateAcc}
      />
    </TabItem>

    <TabItem tabHeadingSmall={'Pris'} tabHeading={Translation.tabs.price.tabname}>
      <PriceTab
        loggedIn={props.loggedIn}
        activeReg={props.activeReg}
        activeGiftReg={props.activeGiftReg}
        activateAcc={props.activateAcc}
      />
    </TabItem>

    <TabItem
      tabHeadingSmall={'Avbryt'}
      tabHeading={
        <div>
          {Translation.tabs.abort.tabname.p1}
          <br />
          {Translation.tabs.abort.tabname.p2}
        </div>
      }
    >
      <AbortTab
        loggedIn={props.loggedIn}
        activeReg={props.activeReg}
        activeGiftReg={props.activeGiftReg}
        activateAcc={props.activateAcc}
      />
    </TabItem>
  </TabList>
);

Tabs.propTypes = {
  loggedIn: PropTypes.bool,
  activeGiftReg: PropTypes.string,
  activeReg: PropTypes.bool,
  activateAcc: PropTypes.object,
};

export default Tabs;
