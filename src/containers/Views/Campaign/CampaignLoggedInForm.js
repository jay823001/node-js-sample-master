import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import { refreshAuth } from 'containers/Views/Account/MyAccount/actions';
import styled from 'styled-components';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import { userCampaignRequest } from './actions';
import validate from './validate';
import Translation from 'translation/nextory-web-se';

const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 1rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
`;
const SubmitSC = Button.withComponent(Submit);

class CampaignLoggedInForm extends React.PureComponent {
  static propTypes = {
    LoggedInAuthkey: PropTypes.string,
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    valid: PropTypes.bool,
    LoggedInEmail: PropTypes.string,
    LoggedInSubid: PropTypes.number,
    userCampaignRequest: PropTypes.func,
    LoggedInPassword: PropTypes.string,
  };

  submit = values => {
    const auth = JSON.parse(Base64.decode(new Cookies().get('user')));
    const jwt = parseJwt(auth.authkey);
    const current_time = Date.now() / 1000;
    if (jwt.exp < current_time) {
      return this.props.refreshAuth();
    }
    const campaigncode = values.campcode;
    const email = this.props.LoggedInEmail;
    const subid = this.props.LoggedInSubid;
    const authkey = this.props.LoggedInAuthkey;
    const password = this.props.LoggedInPassword;
    const data = {
      campaigncode,
      password,
      email,
      subid,
      authkey,
    };
    return this.props.userCampaignRequest(data);
  };

  render() {
    const { handleSubmit, userCampaignRequest, submitting, valid } = this.props;

    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, userCampaignRequest))}>
        <Field
          name="campcode"
          type="text"
          component={renderField}
          label={Translation.forms.labels.campaigncode}
        />

        <Center>
          {submitting ? (
            <SubmitSC large type="submit" disabled>
              <LoadingIndicatorSmall />
            </SubmitSC>
          ) : (
              <SubmitSC large type="submit" disabled={!valid || submitting}>
                {Translation.campaign.campaignreguserform.buttons.continue}
              </SubmitSC>
            )}
        </Center>
      </Form>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    LoggedInSubid: state.account.userData.subscriptionid,
    LoggedInAuthkey: state.account.userData.authkey,
    LoggedInPassword: state.account.userData.p,
    LoggedInEmail: state.account.userData.email,
    initialValues: {
      campcode: ownProps.vouchercode,
    },
  };
}

export default withRouter(
  connect(mapStateToProps, { userCampaignRequest, refreshAuth })(
    reduxForm({
      form: 'campaignloggedin',
      validate,
    })(CampaignLoggedInForm)
  )
);
