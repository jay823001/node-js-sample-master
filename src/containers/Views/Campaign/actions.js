import { USER_CAMPAIGN_REQUEST } from './constants';

export const userCampaignRequest = (data) => ({ type: USER_CAMPAIGN_REQUEST, data });
