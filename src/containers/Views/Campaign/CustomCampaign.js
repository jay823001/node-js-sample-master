/* eslint react/no-danger:0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import DataLayer from 'containers/App/datalayer';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import styled from 'styled-components';
import mediaCustom from 'theme/styled-utils-custom';
import media from 'theme/styled-utils';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { gup } from 'utils/helpFunctions';
import Disclaimer from 'components/Form/Disclaimer';
import Loading from 'components/LoadingIndicator/page';
import P from 'components/Typography/P';
//import { BlueLogo } from 'containers/Menu/logo-blue';
import BlueLogo from 'containers/Menu/logo-blue-new.png';
import BackgroundImage from 'containers/Views/Account/nextory-dark.jpg';
import BackgroundImageMobile from '../Account/sweplus-mobile.jpg';
import BackgroundImageTab from '../Account/sweplus-tab.jpg';
import CampaignRegUserForm from './CampaignRegUserForm';
import CampaignLoggedInForm from './CampaignLoggedInForm';
import {
  PageWrapper,
  InnerWrapper,
  HeroWrapper,
  FormWrapper,
  H1Form,
  H1Styled,
  StyledLink,
} from './StyledCampaign';
import Translation from 'translation/nextory-web-se';

const Header = styled.header`
  width: 100%;
  box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);
  position: relative;
  z-index: 666;
  display: block;
  width: 100%;
  height: 5rem;
  transition: 0.3s ease-in-out;
  background-color: ${props => props.theme.colorMenu};
  ${media.large`
    height: 6.5rem;
  `};
`;

const HeaderInnerwrap = styled.div`
  max-width: 1200px;
  padding: 0 1.5rem;
  margin: auto;
  height: inherit;
`;

const CampaignWrapper = PageWrapper.extend`

  background-image:${props => `url(${props.background})`};

  ${media.small`
   // background-image: url(${BackgroundImageMobile});
   //background-image:${props => `url(${props.background})`};

  `}
  ${media.medium`
    //background-image: url(${BackgroundImageTab});
    //background-image:${props => `url(${props.background})`};

  `}
  ${media.large`
   //background-image:${props => `url(${props.background})`};
    height: 100vh;
   
  `}

  ${mediaCustom.medium`
      background-position-x: -87px;
  `}

`;

const H1FormStyled = H1Form.extend`
  margin: 0 0 2rem 0;
  font-size: 2rem;
  line-height: 1.1;
`;

const PartnerLogo = styled.img`
  height: 3rem;
  width: auto;
  margin-top: 1.1rem;
  float: right;
  ${media.large`
    height: 4.5rem;
    margin-top: 0.9rem;
  `};
`;
const BlueLogoDiv = styled.img`
  width: 9rem;
  height: 2.3rem;
  margin-top: 1.7rem;
  float:left;
  ${media.large`
    width: 16rem;
    height: 4.2rem;
    margin-top: 1.5rem;
    float:none;
  `}
`;

class CustomCampaignPage extends React.PureComponent {
  static propTypes = {
    LoggedInUser: PropTypes.bool,
    Route: PropTypes.string,
    location: PropTypes.object,
  };
  state = {
    validcode: true,
    campaigndata: {},
    loading: true,
    cookiebanner: true,
  };

  componentDidMount() {
    const campaignslug = this.props.Route.substr(this.props.Route.lastIndexOf('/') + 1);
    this.fetchCampaignDetails(campaignslug);
  }

  fetchCampaignDetails = async campaignslug => {
    try {
      const response = await fetch(`${apiUrl}campaigndetails?campaignslug=${campaignslug}${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({ campaigndata: data.data, loading: false });
      } else {
        this.setState({ validcode: false, loading: false });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    let vouchercode = '';
    const voucher = gup('vouchercode', this.props.location.search);
    if (voucher) {
      vouchercode = decodeURIComponent(voucher);
    }

    let backgroundImage = BackgroundImage;
    if (this.state.campaigndata.imageurl !== undefined) {
      backgroundImage = this.state.campaigndata.imageurl;
    }

    let seotitle = this.state.campaigndata.headertext;
    const seodesc = (Translation.campaign.customcampaign.seodescription);

    if (seotitle) {
      seotitle = seotitle.replace(/<(?:.|\n)*?>/gm, '');
    }

    return (
      <div>
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={seodesc} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>

        <Header>
          <HeaderInnerwrap>
            <Link to="/">
              <BlueLogoDiv src={BlueLogo} alt="Logo" />
            </Link>
            {this.state.campaigndata.partner_logo && (
              <PartnerLogo src={this.state.campaigndata.partner_logo} alt="partner logo" />
            )}
          </HeaderInnerwrap>
        </Header>

        {this.state.loading ? (
          <Loading />
        ) : (
            <CampaignWrapper background={backgroundImage}>
              <InnerWrapper>
                <ReactCSSTransitionGroup
                  transitionAppear={true}
                  transitionAppearTimeout={600}
                  transitionEnterTimeout={600}
                  transitionLeaveTimeout={200}
                  transitionName="FadeIn"
                >
                  <HeroWrapper>
                    <H1Styled
                      dangerouslySetInnerHTML={{ __html: this.state.campaigndata.headertext }}
                      huge
                    />
                    {this.state.campaigndata.description ? (
                      <P dangerouslySetInnerHTML={{ __html: this.state.campaigndata.description }} />
                    ) : (
                        <ol>
                          <li>{Translation.campaign.list.item1}</li>
                          <li>{Translation.campaign.list.item2}</li>
                          <li>{Translation.campaign.list.item3}</li>
                        </ol>
                      )}
                  </HeroWrapper>

                  <FormWrapper>
                    {!this.props.LoggedInUser && (
                      <H1FormStyled
                        dangerouslySetInnerHTML={{ __html: this.state.campaigndata.tagtext }}
                      />
                    )}

                    {this.props.LoggedInUser ? (
                      <CampaignLoggedInForm vouchercode={vouchercode} />
                    ) : (
                        <CampaignRegUserForm vouchercode={vouchercode} />
                      )}
                    <Disclaimer>
                      {Translation.campaign.disclaimer}{' '}
                      <StyledLink to="/medlemsvillkor">{Translation.campaign.link}.</StyledLink>{' '}
                      <span dangerouslySetInnerHTML={{ __html: this.state.campaigndata.termsText }} />
                    </Disclaimer>
                  </FormWrapper>
                </ReactCSSTransitionGroup>
              </InnerWrapper>

              {!this.state.validcode && <Redirect to={'/kampanjkod'} />}
              <DataLayer />
            </CampaignWrapper>
          )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    LoggedInUser: state.account.loggedIn,
    Route: state.route.location.pathname,
  };
}

export default connect(mapStateToProps)(CustomCampaignPage);
