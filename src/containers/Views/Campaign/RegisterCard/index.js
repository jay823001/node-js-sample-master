import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DataLayer from 'containers/App/datalayer';
import Cookies from 'universal-cookie';
import RegCardForm from './RegCardForm';
import RegTrustly from './RegTrustly';
import Lock from './images/IconLock';
import Visa from './images/visa.png';
import Master from './images/master.png';
import BankID from './images/bankid.svg';
import Trustly from './images/trustly.png';
import Translation from 'translation/nextory-web-se';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { Base64 } from 'js-base64';
import { getSubscriptionIdByName } from 'containers/App/common';
import { scroller } from 'react-scroll';
import {
  ToastContainer,
  toast,
  style
} from 'react-toastify';
import {
  apiUrl,
  internationalizationLanguage,
  internationalization_a,
  internationalization_b,
  fetchAuth
} from 'containers/App/api';
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  Payment,
  SafePayment,
  FoldTrigger,
  Cards,
  Mobile,
  Foldout,
  HeadingText,
  SeccondHeading,
  PaymentHead,
  Clear,
  PaymentTitle,
  CardImg,
  CardImg2,
  WhiteBg,
  Span
} from './StyledPayment';

class CampaignCard extends Component {
  static propTypes = {
    SignupDetails: PropTypes.object,
    RouteSearch: PropTypes.string,
    RouteHash: PropTypes.string,
  };

  state = {
    card: false,
    bankid: false,
    DAorderid: '',
    DAauthkey: '',
    orderid: '',
    allowaccess: true,
    ObjSub: null,
  };

  async  componentDidMount() {
    this.fetchMetaData();
    if (new Cookies().get('reg')) {
      const decoded = JSON.parse(Base64.decode(new Cookies().get('reg')));
      if (decoded) {
        this.fetchID(decoded);
      }
    }
    this.notify();
  }

  async componentWillMount() {
    this.fetchMetaData();
  }

  notify = () => {
    if (this.props.RouteSearch.includes('trustlyfail')) {
      this.notifyTrustlyFail();
    }
  };

  fetchID = decoded => {
    if (this.props.RouteSearch === '?da') {
      this.fetchCampaignOrderidDA(decoded);
    } else {
      this.fetchCampaignOrderid(decoded);
    }
  };

  fetchCampaignOrderid = async decoded => {
    let subid = getSubscriptionIdByName(this.props.SignupDetails.ChosenSubscription)
    try {
      const response = await fetch(
        `${apiUrl}usersignup?email=${decoded.email}&confirmemail=${decoded.email}&password=${
        decoded.password
        }&subscriptionid=${subid}&serverdetails=127.0.0.1&signuptype=REDEEM_CAMPAIGN&campaigncode=${
        decoded.campaigncode
        }&newsletter=true${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data), // eslint-disable-line
        }
      );
      const data = await response.json();
      if (data.status === 200) {
        return this.setState({ orderid: data.data.orderid });
      } else {
        this.setState({ allowaccess: false });
      }
    } catch (error) {
      console.log(error);
    }
  };

  fetchCampaignOrderidDA = async decoded => {
    let subid = getSubscriptionIdByName(decoded.sub)

    try {
      const response = await fetch(
        `${apiUrl}usersignup?email=${decoded.email}&confirmemail=${decoded.email}&password=${
        decoded.password}&subscriptionid=${subid}&serverdetails=127.0.0.1&signuptype=REDEEM_CAMPAIGN&campaigncode=${
        decoded.campaigncode}&newsletter=true${internationalization_b}`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            Authorization: fetchAuth,
          },
          credentials: 'same-origin',
          method: 'POST',
          body: JSON.stringify(data), // eslint-disable-line
        }
      );
      const data = await response.json();
      //console.log(data);
      if (data.status === 200) {
        const authkey = await response.headers.get('nx-at');
        this.setState({
          DAorderid: data.data.orderid,
          DAauthkey: authkey,
        });
      } else {
        this.setState({ allowaccess: false });
      }
    } catch (error) {
      console.log(error);
    }
  };

  notifyTrustlyFail = () => {
    toast.success(Translation.campaign.subscriptionpage.registercard.notifytrustlyfail, {
      position: toast.POSITION.BOTTOM_CENTER,
      style: style({
        colorSuccess: '#ff3a54',
        width: '380px',
      }),
    });
  };
  scrollTo() {
    if (window.innerWidth < 768) {
      scroller.scrollTo('scroll-to-payment', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart',
        offset: -20,
      });
    }
  };

  handleCardClick = () => {
    this.setState({ card: !this.state.card, bankid: false });
    this.scrollTo();
  };

  handleBankidClick = () => {
    this.setState({ bankid: !this.state.bankid, card: false });
    this.scrollTo();
  };

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          ObjSub: {
            countrycode: data.data.subscriptions[0].countrycode,

            sub1: {
              subid: data.data.subscriptions[0].subid,
              subname: data.data.subscriptions[0].subname,
              subprice: data.data.subscriptions[0].subprice,
            }, sub2: {
              subid: data.data.subscriptions[1].subid,
              subname: data.data.subscriptions[1].subname,
              subprice: data.data.subscriptions[1].subprice,
            }, sub3: {
              subid: data.data.subscriptions[2].subid,
              subname: data.data.subscriptions[2].subname,
              subprice: data.data.subscriptions[2].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {

    const {
      campaignintervelindays,
      campaignfixed,
      campaigndiscounted
    } = this.props.SignupDetails;
    // eslint-disable-line no-unused-vars

    const campainInterval = new Date(new Date().setDate(new Date().getDate() + campaignintervelindays));
    const campaignEndDate = campainInterval.toISOString().substring(0, 10);

    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />
        <RegisterWrapper>
          <InnerWrapper>
            <Heading>
              <Span>
                {Translation.campaign.subscriptionpage.registerwrapper.stage}{' '} 2 {' '}
                {Translation.campaign.subscriptionpage.registerwrapper.of}{' '} 2
              </Span>
              <HeadingText>
                {Translation.campaign.subscriptionpage.registercard.heading}
              </HeadingText>
              <SeccondHeading>

                {!campaigndiscounted && !campaignfixed ?
                  (
                    Translation.campaign.subscriptionpage.registercard.subheading_a_part1
                    + ' ' + campaignEndDate + ' ' +
                    Translation.campaign.subscriptionpage.registercard.subheading_a_part2
                  ) : (
                    Translation.campaign.subscriptionpage.registercard.subheading_b_part1
                    + ' ' + campaignEndDate + ' ' +
                    Translation.campaign.subscriptionpage.registercard.subheading_b_part2
                  )
                }
              </SeccondHeading>
            </Heading>

            <Payment name="scroll-to-payment">
              <PaymentHead>
                <PaymentTitle>
                  {Translation.campaign.subscriptionpage.registercard.paymenttitle}
                </PaymentTitle>
                <SafePayment>
                  <Lock />
                  {Translation.campaign.subscriptionpage.registercard.secure_server}
                </SafePayment>
                <Clear />
              </PaymentHead>
              <FoldTrigger active={this.state.card} onClick={this.handleCardClick}>
                {Translation.registration.stage3.credit_or_debit_card}
                <CardImg>
                  <Cards visa alt="Visa" src={Visa} />
                  <Cards master alt="Mastercard" src={Master} />
                </CardImg>
              </FoldTrigger>
              <Foldout active={this.state.card}>
                <article>
                  <RegCardForm
                    authkey={this.props.SignupDetails.userRegAuthkey}
                    orderid={this.state.orderid}
                    daOrderid={this.state.DAorderid}
                    daAuthkey={this.state.DAauthkey}
                  />
                </article>
              </Foldout>
              {
                internationalizationLanguage !== "FI" ?
                  <FoldTrigger active={this.state.bankid} onClick={this.handleBankidClick}>
                    {Translation.registration.stage3.mobile_bankid}
                    <CardImg2>
                      <Mobile bankid alt="BankId" src={BankID} />
                      <Mobile trustly alt="Trustly" src={Trustly} />
                    </CardImg2>
                  </FoldTrigger>
                  : null
              }

              <Foldout active={this.state.bankid}>
                <RegTrustly
                  authkey={this.props.SignupDetails.userRegAuthkey}
                  orderid={this.state.orderid}
                  daOrderid={this.state.DAorderid}
                  daAuthkey={this.state.DAauthkey}
                />
              </Foldout>
            </Payment>
          </InnerWrapper>
        </RegisterWrapper>
        <WhiteBg active={this.state.card} active2={this.state.bankid} />
        {!this.state.allowaccess && <Redirect to={'/'} />}

        <ToastContainer autoClose={5000} />
      </ReactCSSTransitionGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    SignupDetails: state.signup,
    RouteSearch: state.route.location.search,
    RouteHash: state.route.location.hash,
  };
}

export default connect(mapStateToProps)(CampaignCard);
