import styled from 'styled-components';
import media from 'theme/styled-utils';
import { Link } from 'react-router-dom';
import H1 from 'components/Typography/H1';
import A from 'components/Typography/A';
import BackgroundImage from '../Account/nextory-dark.jpg';
import BackgroundImageMobile from '../Account/nextory-dark-mobile.jpg';

export const PageWrapper = styled.section`
  padding: 3rem 1.5rem;
  background-image: url(${BackgroundImageMobile});
  background-repeat: no-repeat;
  background-position: left;
  background-size: cover;
  height: auto;
  min-height: 66rem;
  position: relative;

  ${media.xlarge`
    background-position: center;
  `}

  ${media.medium`
    height: 100vh;
  `} &:after {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: 100%;
    width: 100%;
    content: '';
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 1;
  }

${media.small`
  background-image: url(${BackgroundImageMobile});
`}

${media.medium`
  background-image: url(${BackgroundImage});
`}

${media.large`
  background-image: url(${BackgroundImage});
  height: 100vh;
`}
`;

export const InnerWrapper = styled.div`
  max-width: 150rem;
  margin: auto;
  height: inherit;
  position: relative;
`;

export const HeroWrapper = styled.div`
  z-index: 10;
  color: white;
  max-width: 39rem;
  margin: 0 auto 3.0rem;
  position: relative;
  ${media.medium`
    position:absolute;
    top: 35%;
    left: 2%;
    float: left;
    max-width: 38rem;
    transform: translateY(-50%);
  `}
  ${media.large`
    left: 4%;
    max-width: 40.0rem;
  `}
  ${media.xlarge`
    left: 7%;
    max-width: 54.5rem;
  `}
  ul,ol {
    padding-left: 2.2rem;
    li {
      font-size: 1.6rem;
      margin-bottom: .2rem;
      ${media.medium`
        font-size: 2.2rem;
        margin-bottom: .5rem;
      `}
    }
  }
`;

export const FormWrapper = styled.div`
  z-index: 10;
  background-color: ${props => props.theme.colorWhite};
  max-width: 39.2rem;
  padding: 1.5rem 2.0rem 2.0rem;
  margin: 0 auto;
  position: relative;
  ${media.medium`
    position:absolute;
    padding: 2.0rem 2.5rem 2.5rem;
    top: 46%;
    right: 0%;
    float: right;
    max-width: 34rem;
    margin: auto 0;
    transform: translateY(-50%);

  `}
  ${media.large`
    right: 7%;
    max-width: 43.6rem;
  `}
  ${media.xlarge`
    right: 10%;
  `}
`;

export const H1Form = H1.extend`
  margin-bottom: 1.5rem;
  ${media.medium`
    margin-bottom: 2rem;
  `};
`;

export const H1Styled = H1.extend`
  font-size: 2.8rem;
  ${media.medium`
    font-size:4.7rem;
  `};
`;

export const StyledLink = A.withComponent(Link);
