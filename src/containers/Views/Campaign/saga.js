import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { stopSubmit, startSubmit } from 'redux-form';
import { Base64 } from 'js-base64';
import Cookies from 'universal-cookie';
import { postUserCampaign } from 'containers/App/api';
import {
  CREATEUSER_CLEAR_ERROR,
  CREATEUSER_SETAUTH,
} from 'containers/Views/Registration/constants';
import { REFRESH_STORE } from '../Account/MyAccount/constants';
import { USER_CAMPAIGN_REQUEST } from './constants';
import Translation from 'translation/nextory-web-se';

/*
* Redeen campaign for logged in active member
*/
function* userCampaign(data) {
  let errors = {};
  yield put(startSubmit('campaignloggedin'));

  // api call
  const regdata = yield call(postUserCampaign, data);
  // do something with the api call response
  if (regdata.status === 200) {
    
    if (regdata.data.paymentrequired) {
      const user = {
        campaigncode: data.data.campaigncode,
        campaignname: regdata.data.campaignname,
        campaignprice: regdata.data.campaigndetails.campaignprice,
        campaignintervel: regdata.data.campaigndetails.intervel,
        campaignintervelindays: regdata.data.campaigndetails.intervelindays,
        campaignperiod: regdata.data.campaigndetails.period,
        campaigndays: regdata.data.campaigndetails.trialdays,
        campaigndiscounted: regdata.data.campaigndetails.discounted,
        campaigntrialdays: regdata.data.campaigndetails.trialdays,
        campaignfixed: regdata.data.campaigndetails.fixedcamp,
        newuser: regdata.data.newuser,
        paymentrequired: regdata.data.paymentrequired,
        paymenttype: regdata.data.paymenttype,
        authkey: regdata.authkey,
        userid: regdata.data.userinfo.userid,
        orderid: regdata.data.orderid,
        email: data.data.email,
        password: data.data.password,
        cleanpass: data.data.cleanpass,
      };

      new Cookies().set('reg', Base64.encode(JSON.stringify(user)), { path: '/', maxAge: 1209600 });

      yield put({ type: CREATEUSER_SETAUTH, newAuthState: true });
      yield put({ type: CREATEUSER_CLEAR_ERROR });
      yield put(push('/register-campaign/subscription#steg-1'));
    } else {
      // refresh store saga and redirect to konto page
      yield put({ type: REFRESH_STORE, authkey: data.data.authkey });
      yield put(push('/konto?campaigncode-added'));
    }
  } else {
    errors = {
      campcode: Translation.api_messages.error.campaigncode_error,
    };
  }

  yield put(stopSubmit('campaignloggedin', errors));
}
export function* userCampaignSaga() {
  yield takeLatest(USER_CAMPAIGN_REQUEST, userCampaign);
}
