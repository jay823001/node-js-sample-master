import React from 'react';
import PropTypes from 'prop-types';
import Waypoint from 'react-waypoint';
import Center from 'components/Center';
import Book from './images/book.svg';
import Cookies from 'universal-cookie';
import DataLayer from 'containers/App/datalayer';
import SubscriptionTable from './SubscriptionTable';
import Translation from 'translation/nextory-web-se';
import FamilySubscriptionTable from './SubscriptionTable.Family';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { isFamilyPackage, getMonthOrMonths } from 'containers/App/common'
import {
  RegisterWrapper,
  InnerWrapper,
  Heading,
  Span,
  InnerWrapperc,
  SubcriptionC,
  SubcriptionCInfo,
  H1Main,
  ButtonWhiteWrapper,
  ButtonMain,
  SubcriptionStage2,
  SubcriptionStage2Heading,
  SubcriptionStage2Bottom,
  ButtonPrimary,
  PkgBoxContainer,
  SubcriptionStage2BoxBottom,
  InnerWrapperd,
  BoxButtonPrimary
} from './StyledSubscriptionPage';

class CampaignSubscriptionPage extends React.PureComponent {
  static propTypes = {
    Campaignprice: PropTypes.number,
    SignupDetails: PropTypes.object,
  };

  state = {
    sticky: false,
    subscription: null,
    showFamilyPage: false
  };

  componentDidMount() {
    const cookies = new Cookies();
    if (cookies.get('retry')) {
      cookies.remove('retry', { path: '/' });
    }
  }
  componentWillReceiveProps() {
    const cookies = new Cookies();
    if (cookies.get('retry')) {
      cookies.remove('retry', { path: '/' });
    }
  }

  getChosenSubscription = (subscription) => {
    this.setState({ subscription })
  }

  getCampaignSpecificMessage = (props) => {
    const Translate = Translation.campaign.subscriptionpage.registerwrapper.campaignmessage
    let signupDetails = props.SignupDetails
    if (!signupDetails.campaigndiscounted && !signupDetails.campaignfixed) { // Free Days Campaign
      return (
        Translate.message_a
      )
    } else if (!signupDetails.campaigndiscounted) {// Fixed price Campaign
      return (
        Translate.message_b + " " + signupDetails.campaignprice + " " +
        Translate.currency + " " + Translate.in + " " + signupDetails.campaignperiod + " " +
        getMonthOrMonths(signupDetails.campaignperiod)
      )
    } else {// Discounted price Campaign
      return '';

      // return (
      //   Translate.message_c + " " + signupDetails.campaignprice + "% " +
      //   Translate.discount_in + " " + signupDetails.campaignperiod + " " +
      //   getMonthOrMonths(signupDetails.campaignperiod)
      // )
    }
  }

  getCampaignSpecificButtonText = () => {
    const Translate = Translation.campaign.subscriptionpage.registerwrapper
    let signupDetails = this.props.SignupDetails
    let packageName = isFamilyPackage(signupDetails.ChosenSubscription) ? Translation.app.common.family : signupDetails.ChosenSubscription
    if (signupDetails.campaigndiscounted) {// Discouned Price Campaign
      return Translate.choose + " " + packageName;
      /*
      return ( //'Välj guld för 50% i 3 månader'
        Translate.choose + " " + packageName + " " + Translate.when + " " +
        Translate.for + " " + signupDetails.campaignprice + "% " + Translate.off + " " + Translate.in + " " +
        signupDetails.campaignperiod + " " + getMonthOrMonths(signupDetails.campaignperiod)
      )
      */
    } else if (signupDetails.campaignfixed) {// Fixed Price Campaign
        return Translate.choose + " " + packageName;
        /*
        return (  // 'Välj guld för 9 kr/mån i 3 månader'
          Translate.choose + " " + packageName + " " + Translate.at_the_cost_of + " " +
          Translate.for + " " + signupDetails.campaignprice + " " + Translate.currency_per_month + " " +
          Translate.in + " " + signupDetails.campaignperiod + " " + getMonthOrMonths(signupDetails.campaignperiod)
        )
        */
    } else {// Free Days Campaign
        return Translate.choose + " " + packageName;
        /*
        return (
          Translate.choose + " " + packageName + " " + Translate.free_for + " " + signupDetails.campaignintervelindays + " " + Translate.for_free_days + " " + Translate.days)
        */
    }
  }

  render() {
    const familyPackage2 = Translation.subscription_table.packages.family2;
    let subscription = (this.state.subscription === familyPackage2 || localStorage.getItem('sub-state') === familyPackage2)
    let showFamilyPage = (subscription) ? "/register-campaign/subscription#familj" : "/register-campaign/card";
    this.setState({ showFamilyPage, subscription: localStorage.getItem('sub-state') });
    let urlToSubscriptionPage = '/register-campaign/subscription'
    //this is the pre campaign message before showing the subscriptions.
    const preCampaignSubscription = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <SubcriptionC>
          <RegisterWrapper>
            <InnerWrapperc>
              <img src={Book} alt="" />
              <Span>
                {Translation.campaign.subscriptionpage.registerwrapper.stage} 1 {' '}
                {Translation.campaign.subscriptionpage.registerwrapper.of} {' '} 2
              </Span>
              <H1Main>{Translation.campaign.subscriptionpage.registerwrapper.pre_stage_heading}</H1Main>
              <SubcriptionCInfo>
                {this.getCampaignSpecificMessage(this.props)}
              </SubcriptionCInfo>
            </InnerWrapperc>
          </RegisterWrapper>
        </SubcriptionC>
        <ButtonWhiteWrapper>
          <ButtonMain large to={urlToSubscriptionPage}>
            {Translation.campaign.subscriptionpage.registerwrapper.pre_stage_button}
          </ButtonMain>
        </ButtonWhiteWrapper>
      </ReactCSSTransitionGroup>
    );

    //Subscriptions list on clike the link button.
    const campaignSubscription = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <DataLayer />

        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  {this.props.Campaignprice === 0 ? (
                    <H1Main>{Translation.campaign.subscriptionpage.registerwrapper.heading1}</H1Main>
                  ) :
                    (
                      <H1Main>{Translation.campaign.subscriptionpage.registerwrapper.heading2}</H1Main>
                    )}
                  <SubcriptionCInfo>{Translation.campaign.subscriptionpage.registerwrapper.subheading}</SubcriptionCInfo>
                </SubcriptionStage2Heading>
              </Heading>

              <Waypoint
                onLeave={() => this.setState({ sticky: true })}
                onEnter={() => this.setState({ sticky: false })}
                topOffset={'0px'}
              >
                <p> </p>
              </Waypoint>
            </InnerWrapper>
            <SubscriptionTable getCampaignSpecificMessage={this.getCampaignSpecificMessage(this.props)} sticky={this.state.sticky} getChosenSubscription={this.getChosenSubscription} />

          </RegisterWrapper>
        </SubcriptionStage2>

        <SubcriptionStage2Bottom >
          <InnerWrapper>
            <Center>
              <ButtonPrimary large to={showFamilyPage} onClick={() => this.setState({ showFamilyPage: !showFamilyPage })}>
                {/*
                {this.getCampaignSpecificButtonText()}
                */}
                {Translation.campaign.subscriptionpage.registerwrapper.continue}
              </ButtonPrimary>
            </Center>
          </InnerWrapper>

        </SubcriptionStage2Bottom>


        {!this.props.SignupDetails.campaignname && <Redirect to={'/'} />}
      </ReactCSSTransitionGroup>
    );

    // familyCampaignSubscription
    const familyCampaignSubscription = (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={600}
        transitionEnterTimeout={600}
        transitionLeaveTimeout={200}
        transitionName="SlideUp"
      >
        <SubcriptionStage2>
          <RegisterWrapper>
            <InnerWrapper>
              <Heading>
                <SubcriptionStage2Heading>
                  <H1Main>{Translation.campaign.subscriptionpage.registerwrapper.family.heading}</H1Main>
                  <SubcriptionCInfo>{Translation.campaign.subscriptionpage.registerwrapper.family.subheading}</SubcriptionCInfo>
                </SubcriptionStage2Heading>

                <PkgBoxContainer />
              </Heading>

            </InnerWrapper>
            <FamilySubscriptionTable sticky={this.state.sticky} getChosenSubscription={this.getChosenSubscription} />

          </RegisterWrapper>
        </SubcriptionStage2>

        <SubcriptionStage2BoxBottom>
          <InnerWrapperd>
            <Center>
              <BoxButtonPrimary large to={urlToSubscriptionPage} onClick={() => this.setState({ showFamilyPage: !showFamilyPage })}>
                {Translation.campaign.subscriptionpage.registerwrapper.button_go_back}
              </BoxButtonPrimary>
            </Center>
          </InnerWrapperd>
        </SubcriptionStage2BoxBottom>


        {!this.props.SignupDetails.campaignname && <Redirect to={'/'} />}
      </ReactCSSTransitionGroup>
    );

    let pageContents;
    if (this.props.location.hash === '#steg-1') {
      pageContents = preCampaignSubscription
    } else if (this.state.showFamilyPage === '/register-campaign/subscription#familj' && this.props.location.hash === '#familj') {
      pageContents = familyCampaignSubscription
    } else {
      pageContents = campaignSubscription
    };

    return (
      pageContents
    );
  }
}

function mapStateToProps(state) {
  return {
    Campaignprice: state.signup.campaignprice,
    SignupDetails: state.signup,
  };
}

export default connect(mapStateToProps)(CampaignSubscriptionPage);
