import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { chooseSubscription } from 'containers/Views/Registration/actions';
import Translation from 'translation/nextory-web-se';
import {
  apiUrl,
  internationalization_a,
  internationalizationLanguage,
  fetchAuth,
} from 'containers/App/api';
import {
  PkgBoxWrapperParent,
  PkgBoxWrapper,
  PkgBox,
  PriceContainer,
  PriceInfo,
  PriceButton,
  PriceTitle,
  PriceInfoTitle,
  PriceButtonBtn,
  Clear
} from './StyledSubscriptionPage';

class SubscriptionTable extends React.PureComponent {
  static propTypes = {
    chooseSubscription: PropTypes.func,
    ChosenSubscription: PropTypes.string,
    SignupDetails: PropTypes.object,
  };

  state = {
    sticky: false,
    subscriptionData: null,
  };
  componentDidMount() {
    this.fetchMetaData();
  };

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}subscriptions${internationalization_a}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      // console.log(data);
      if (data.status === 200) {
        let subscriptions = data.data.subscriptions;
        this.setState({
          subscriptionData: {
            countrycode: subscriptions[0].countrycode,
            package3: {
              packagename: subscriptions[2].subname,
              packageprice: subscriptions[2].subprice,
            }, package4: {
              packagename: subscriptions[3].subname,
              packageprice: subscriptions[3].subprice,
            }, package5: {
              packagename: subscriptions[4].subname,
              packageprice: subscriptions[4].subprice,
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    localStorage.setItem('origin-path', window.location.hash)
    const buttonUrl = '/register-campaign/card'
    const SignupDetails = this.props.SignupDetails

    let campaignDetails;
    let monthOrMonths = SignupDetails.campaignperiod > 1 ?
      ( Translation.campaign.subscriptionpage.registerwrapper.family.months ) :
      ( Translation.campaign.subscriptionpage.registerwrapper.family.month )

    if (SignupDetails.campaignfixed) { // Fixed Price Campaign
      campaignDetails = internationalizationLanguage === 'FI' ?
      ( // Language FI
        SignupDetails.campaignprice + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.currency_per_month + ' ' +
        SignupDetails.campaignperiod  + ' ' + monthOrMonths + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.after
      ) : ( // Language SE
        SignupDetails.campaignprice + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.currency_per_month + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.in + ' ' +
        SignupDetails.campaignperiod  + ' ' + monthOrMonths + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.after
      )
    } else if (SignupDetails.campaigndiscounted) { // Discount Campaign
      campaignDetails = '';
      /*
      campaignDetails = internationalizationLanguage === 'FI' ?
      ( // Language FI
        SignupDetails.campaignprice + '% ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.discount + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.in + ' ' +
        SignupDetails.campaignperiod  + ' ' + monthOrMonths + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.after
      ) : ( // Language SE
        SignupDetails.campaignprice + '% ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.discount + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.in + ' ' +
        SignupDetails.campaignperiod  + ' ' + monthOrMonths + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.after
      )
      */
    } else { // Free Days Campaign
      campaignDetails = (
        SignupDetails.campaignintervelindays + ' ' +
        Translation.campaign.subscriptionpage.registerwrapper.family.days_free_then
      )
    }
    return (
      <PkgBoxWrapperParent>
          <PkgBoxWrapper>
            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 2
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                  {campaignDetails}
                    <b>
                    {!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package3.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b>
                </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  <PriceButtonBtn onClick={() => this.props.chooseSubscription(Translation.app.common.family2)} to={buttonUrl} >
                  {Translation.subscription_table.family.choose} {' '} <span> 2 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn>
                </PriceButton>
              </PriceContainer>
            </PkgBox>

            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 3
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                  {campaignDetails}
                    <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package4.packageprice} {' '}
                      {Translation.subscription_table.family.currency_per_month}
                    </b>
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  <PriceButtonBtn onClick={() => this.props.chooseSubscription(Translation.app.common.family3)} to={buttonUrl} >
                  {Translation.subscription_table.family.choose} {' '} <span> 3 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn>
                </PriceButton>
              </PriceContainer>
            </PkgBox>

            <PkgBox>
              <PriceContainer>
                <PriceInfo>
                  <PriceTitle> 4
                  {' ' + Translation.subscription_table.family.users}
                  </PriceTitle>
                  <PriceInfoTitle>
                  {campaignDetails}
                    <b>{!this.state.subscriptionData ? '' : ' ' + this.state.subscriptionData.package5.packageprice} {' '}
                    {Translation.subscription_table.family.currency_per_month}
                    </b>
                  </PriceInfoTitle>
                </PriceInfo>
                <PriceButton>
                  <PriceButtonBtn onClick={() => this.props.chooseSubscription(Translation.app.common.family4)} to={buttonUrl} >
                  {Translation.subscription_table.family.choose} {' '} <span> 4 {' ' + Translation.subscription_table.family.users} </span>
                  </PriceButtonBtn>
                </PriceButton>
              </PriceContainer>
            </PkgBox>
            <Clear />
          </PkgBoxWrapper>
          <Clear />
        </PkgBoxWrapperParent>
    );
  }
}

function mapStateToProps(state) {
  return {
    ChosenSubscription: state.signup.ChosenSubscription,
    SignupDetails: state.signup,
  };
}

export default connect(mapStateToProps, { chooseSubscription })(SubscriptionTable);
