
import styled from 'styled-components';
import media from 'theme/styled-utils';
import mediaCustom from 'theme/styled-utils-custom';

import H1 from 'components/Typography/H1';
import H2 from 'components/Typography/H2';
import Button from 'components/Buttons';

import { LI, StyledButton } from './StyledSubTable'

export const RegisterWrapper = styled.section`
  padding: 1rem 0 1rem;

  ${media.medium`
    padding: 7rem 0 1rem;
  `};
  ${mediaCustom.medium`
    padding: 3rem 0 0rem;
  `};
`;
export const H1Main = styled(H1)`
  font-size: 3.1rem;
  max-width: 685px;
  margin: 0 auto;
  margin-bottom: 10px;
  word-break: break-word;

  ${media.medium`
    font-size: 5.1rem;
    margin-bottom: 20px;
  `};
`;
export const H2Main = styled(H2)`
      font-size: 1.9rem;
`;
export const ButtonMain = styled(Button)`
  width: 245px;
  height: 54px;
  border-radius: 27px;
  text-transform: uppercase;
  font-size: 14px;
  padding: 15px 0px;
  font-weight: bold;
  ${mediaCustom.medium`
      width: 290px;
  `};
`;

export const ButtonPrimary = styled(Button)`
  border-radius: 27px;
  max-width: 30rem;
  text-transform: uppercase;
  font-size: 15px;
  font-weight: bold;
  ${mediaCustom.medium`
    font-size: 12px;
  `};
`;

export const InnerWrapper = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 93rem;
`;

export const Heading = styled.div`
  margin-bottom: 1.8rem;
  ${media.medium`
      margin-bottom: 4.9rem;
  `};
`;

export const Span = styled.span`
  display: block;
  padding-top: 10px;
  font-size: 1.4rem;
  margin: 0 0 0.8rem;
  ${media.medium`
    font-size: 1.6rem;
    margin: 20px 0 1.4rem;
  `};
`;

export const InnerWrapperc = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;

`;

export const SubcriptionC = styled.div`
  padding-top: 8rem;
  padding-right: 3rem;
  padding-left: 3rem;
  padding-bottom: 0rem;
  background: ${props => props.theme.greenBg};
  text-align:center;

  img {
    width: 50px;
  }

  ${media.medium`
    padding-top: 1rem;

    img {
      width: 80px;
      margin-bottom: 0.8rem;
    }
  `};

  ${mediaCustom.medium`
    padding-top: 0;
  `};
`;

export const SubcriptionCInfo = styled.div`
  font-size: 1.6rem;
  margin-bottom: 0rem;
  padding-bottom: 15px;
  ${media.medium`
    margin-bottom: 2.5rem;
    margin-top: 2.5rem;
    padding-bottom: 0px;
  `};
`;
export const ButtonWhiteWrapper = styled.section`
    text-align:center;
    background: ${props => props.theme.greenBg};
    padding-bottom:70px;
    ${mediaCustom.medium`
      padding-bottom: 0px;
      height: 120px;
      padding-top: 15px;
    `};
`;

export const SubcriptionStage2 = styled.section`
  background: ${props => props.theme.greenBg};
  margin-top: -20px;

  ${media.medium`
    margin-top: 0;
  `};
`;

export const SubcriptionStage2Heading = styled.div`
      text-align:center;
`;

export const SubcriptionStage2Bottom = styled.section`
  position: fixed;
  z-index: 10;
  bottom: 0;
  left: 0;
  width: 100%;
  background: linear-gradient(rgba(255, 255, 255, 0), #fff 70%);
  padding: 40px 0 40px;

  ${media.medium`
    position: relative;
    padding: 20px 0 60px;
    background: ${props => props.theme.greenBg};
  `};
`;



export const H2MainBox = styled(H2)`
  font-size: 18px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: center;
  color: #555555;
  margin-top: 25px;
`;


export const BoxButtonPrimary = styled(Button)`
  width: 160px;
  height: 54px;
  border-radius: 27px;
  border: solid 2px #363bec;
  -webkit-text-transform: uppercase;
  text-transform: uppercase;
  background-color: #ffffff;
  font-size: 15px;
  box-shadow: none;
  color: #363bec;
  font-weight: bold;
  &:hover{
    color:#fff;
  }
  ${mediaCustom.medium`
  font-size: 13px;
  width: 95%;
  height: 54px
  `};
`;
export const InnerWrapperd = styled.div`
  padding: 0 1.5rem;
  margin: auto;
  max-width: 65.2rem;
  ${mediaCustom.medium`
    padding:0px;
    width: 65%;
  `};
  ${mediaCustom.small`
    padding:0px;
    width: 100%;
  `};
`;

export const SubcriptionStage2BoxBottom = styled.section`
  background: ${props => props.theme.greenBg};
  padding: 20px;
  min-height: 250px;
  padding-top: 150px;

  ${mediaCustom.medium`
    padding: 20px;
    min-height: auto;
    padding-top: 25px
`};
`;

export const PkgBoxContainer = styled.section`
  position: relative;
  height: 100px;
  margin-top: 20px;
  ${mediaCustom.medium`
  position: unset;
`};

`;


//Subscription table styles
export const TableLI = styled(LI)`
  span{
    font-weight: bold;
  }
`;

export const TableTitle = styled.h2`
  font-size: 26px;
  font-weight: 600;
  line-height: 29px;
  position: absolute;
  bottom: -30px;

  ::first-letter {
    text-transform: uppercase;
  }
`;

export const TableContainer = styled.div`
  border-radius: 6px;
  background-color: #ffffff;
  padding: 20px 45px;
  margin: auto;
  max-width: 93rem;
  box-shadow: 3px 3px 12px 4px #b4b4b42e;

  ${mediaCustom.medium`
    padding:20px 5px;
    position:relative;
    box-shadow:none;
  `}
`;

export const TableRowHeadText = styled.div`
  ${mediaCustom.medium`
    margin-bottom: 0.25em;
  `}

  ${media.medium`
    // max-width: 225px;
  `};
`;
export const TableNo = styled.span`
  font-weight: bold;
`;

export const TableRowContent = styled.div`

  ${mediaCustom.medium`
    li{
      // margin-bottom: 3px !important;
    }
  `};
`;
export const PriceFrom = styled.span`
  font-size: 14px;
  font-weight: normal !important;
`;

//Subscription familj styles

export const PkgBoxWrapperParent = styled.section`
position: relative;
${mediaCustom.medium`
  height:300px;
`};
`;
export const PkgBoxWrapper = styled.section`
  position: absolute;
  left:0;
  right:0;
  max-width: 93rem;
  margin: auto;
  bottom: -6em;

  ${mediaCustom.medium`
  position: relative;
  bottom: 90px;
  `};
`;

export const PkgBox = styled.div`
  width: 30%;
  height: 220px;
  border-radius: 6px;
  background-color: #ffffff;
  float: left;
  margin: 10px;
  box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.08), 0 12px 24px 2px rgba(234, 243, 245, 0.25);

  ${mediaCustom.medium`
      float: none;
      width: 96%;
      margin: 0 auto;
      margin-bottom: 7px;
      height: 118px;
  `};
`;
export const PriceContainer = styled.div`
  padding: 40px;
  ${mediaCustom.medium`
    padding: 20px !important;
  `};
`;
export const PriceInfo = styled.div`
    ${mediaCustom.medium`
      float:left;
    `};
  `;
export const PriceButton = styled.div`
  ${mediaCustom.medium`
    float:right;
  `};
`;
export const PriceTitle = styled.h2`
  font-size: 23px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.2;
  letter-spacing: -0.2px;
  color: #444444;
  margin: 0px;
  font-weight: bold;
  margin-bottom: 5px;
  ${mediaCustom.medium`
    font-size: 21px !important;
  `};
`;
export const PriceInfoTitle = styled.p`
  font-size: 17px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: #555555;
  margin:0px;
  max-width: 185px;
  ${mediaCustom.medium`
    max-width: 155px !important;
    font-size: 14px !important;
  `};
`;
export const PriceButtonBtn = styled(StyledButton)`
  padding: 14px 5px;
  width: 100%;
  height: 45px;
  border-radius: 27px;
  color: #fff;
  background-color: #353bec;
  border: none;
  margin-top: 15px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: bold;
  cursor:pointer;
  &:hover {
    background-color: #373ae0;
  }
  ${mediaCustom.medium`
    width:94px !important;
    span{
      display:none;
    }
  `};
`;

export const Clear = styled.div`
  clear:both;
 `;
