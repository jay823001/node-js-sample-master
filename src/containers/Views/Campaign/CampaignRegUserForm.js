import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Base64 } from 'js-base64';
import { createUserRequest } from 'containers/Views/Registration/actions';
import styled from 'styled-components';
import Center from 'components/Center';
import Form from 'components/Form/Form';
import { renderField } from 'components/Form/Field';
import Button from 'components/Buttons';
import LoadingIndicatorSmall from 'components/LoadingIndicator/small';
import validate from './validate';
import Translation from 'translation/nextory-web-se';
import { getSubscriptionIdByName } from '../../App/common';

const Submit = styled.button`
  margin-top: 0.8rem;
  margin-bottom: 1rem;
  &:disabled {
    cursor: not-allowed;
    opacity: 0.6;
    &:hover {
      background-color: ${props => props.theme.colorBlue};
    }
  }
`;
const SubmitSC = Button.withComponent(Submit);

class CampaignRegUserForm extends Component {
  static propTypes = {
    ActiveReg: PropTypes.object,
    LoggedInUser: PropTypes.bool,
    LoggedInData: PropTypes.object,
  };

  submit = (values, createUserRequest) => {
    const campaigncode = values.campcode;
    let email = values.email;
    let repeatemail = values.repeatemail;
    let password = Base64.encode(values.password);
    let subid = getSubscriptionIdByName(this.props.ChoosenSub)

    if (this.props.LoggedInUser) {
      email = this.props.LoggedInData.email;
      repeatemail = this.props.LoggedInData.email;
      password = this.props.LoggedInData.p;
      subid = this.props.LoggedInData.subscriptionid;
    }

    const data = {
      campaigncode,
      password,
      email,
      repeatemail,
      subid,
    };
    createUserRequest(data);
  };

  render() {
    const {
      handleSubmit,
      createUserRequest,
      submitting,
      pristine,
      valid,
      SendingRequest,
      LoggedInUser,
      // initialValues,
    } = this.props;
    const regtype = this.props.ActiveReg.userRegPayment;

    return (
      <Form onSubmit={handleSubmit(values => this.submit(values, createUserRequest))}>
        <Field
          name="campcode"
          type="text"
          component={renderField}
          label={Translation.forms.labels.campaigncode}
        />
        {!LoggedInUser && (
          <span>
            <Field
              name="email"
              type="email"
              component={renderField}
              label={Translation.forms.labels.email}
            />
            <Field
              name="repeatemail"
              type="email"
              component={renderField}
              label={Translation.forms.labels.repeat_email}
            />
            <Field
              name="password"
              type="password"
              component={renderField}
              label={Translation.forms.labels.password}
            />
          </span>
        )}

        <Center>
          {SendingRequest ? (
            <SubmitSC large type="submit" disabled>
              <LoadingIndicatorSmall />
            </SubmitSC>
          ) : (
              <SubmitSC large type="submit" disabled={!valid || pristine || submitting}>
                {Translation.forms.submit}
              </SubmitSC>
            )}
        </Center>

        {regtype === 'REDEEM_CAMPAIGN' && <Redirect to={'/register-campaign/subscription'} />}
      </Form>
    );
  }
}

CampaignRegUserForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  pristine: PropTypes.bool,
  valid: PropTypes.bool,
  createUserRequest: PropTypes.func,
  SendingRequest: PropTypes.bool,
  ChoosenSub: PropTypes.string,
};

function mapStateToProps(state, ownProps) {
  return {
    ChoosenSub: state.signup.ChosenSubscription,
    SendingRequest: state.signup.currentlySending,
    ActiveReg: state.signup,
    LoggedInUser: state.account.loggedIn,
    LoggedInData: state.account.userData,
    initialValues: {
      campcode: ownProps.vouchercode,
    },
  };
}

export default withRouter(
  connect(mapStateToProps, { createUserRequest })(
    reduxForm({
      form: 'campaignregisteruser',
      validate,
    })(CampaignRegUserForm)
  )
);
