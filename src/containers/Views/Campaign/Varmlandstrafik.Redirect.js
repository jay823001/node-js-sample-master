import React from 'react';
import { Redirect } from 'react-router';

class RedirectCampaign extends React.PureComponent {
  render() {
    return <Redirect to={'/kampanj/varmlandstrafik?vouchercode=varmlandstrafik&utm_medium=partnership&utm_campaign=v%C3%A4rmlandstrafiken&utm_content=varmlandstrafik&utm_source=partnership'} />;
  }
}

export default RedirectCampaign;
