import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { gup } from 'utils/helpFunctions';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import DataLayer from 'containers/App/datalayer';
import Disclaimer from 'components/Form/Disclaimer';
import CampaignRegUserForm from './CampaignRegUserForm';
import CampaignLoggedInForm from './CampaignLoggedInForm';
import Loading from 'components/LoadingIndicator/page';
import {
  PageWrapper,
  InnerWrapper,
  HeroWrapper,
  FormWrapper,
  H1Form,
  H1Styled,
  StyledLink,
} from './StyledCampaign';
import Translation from 'translation/nextory-web-se';

class CampaignPage extends React.PureComponent {
  static propTypes = {
    LoggedInUser: PropTypes.bool,
    AllowedActions: PropTypes.array,
  };

  state = {
    formHeading: (Translation.campaign.formheading_a),
    seotitle: '',
    seodesc: '',
    loadingseo: true,
  };

  componentDidMount() {
    this.fetchMetaData();
    var urlParts = window.location.pathname.split('/');
    if (urlParts[urlParts.length - 1] === 'b') {
      this.setState({ formHeading: (Translation.campaign.formheading_b) });
    }
  }

  fetchMetaData = async () => {
    try {
      const response = await fetch(`${apiUrl}page-meta?pageUrl=/kampanjkod/${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (data.status === 200) {
        this.setState({
          seotitle: data.data.pagemeta.metaList[0].value,
          seodesc: data.data.pagemeta.metaList[1].value,
          loadingseo: false,
        });
      }
    } catch (error) {
      this.setState({ loadingseo: false });
    }
  };

  render() {
    let vouchercode = '';
    const voucher = gup('vouchercode', this.props.location.search);
    if (voucher) {
      vouchercode = decodeURIComponent(voucher);
    }

    let dataFetched = false;
    if (!this.state.loadingseo) {
      dataFetched = true;
    }

    return dataFetched ? (
      <PageWrapper>
        <Helmet>
          <title>{this.state.seotitle}</title>
          <meta name="description" content={this.state.seodesc} />
          <meta property="og:title" content={this.state.seotitle} />
          <meta property="og:description" content={this.state.seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta name="twitter:description" content={this.state.seodesc} />
          <meta name="twitter:title" content={this.state.seotitle} />
        </Helmet>
        <DataLayer />
        <InnerWrapper>
          <ReactCSSTransitionGroup
            transitionAppear={true}
            transitionAppearTimeout={600}
            transitionEnterTimeout={600}
            transitionLeaveTimeout={200}
            transitionName="FadeIn"
          >
            {this.props.LoggedInUser ? (
              <HeroWrapper>
                <H1Styled huge>{Translation.campaign.heading1}</H1Styled>
              </HeroWrapper>
            ) : (
                <HeroWrapper>
                  <H1Styled huge>{Translation.campaign.heading2}</H1Styled>
                  <ol>
                    <li>{Translation.campaign.list.item1}</li>
                    <li>{Translation.campaign.list.item2}</li>
                    <li>{Translation.campaign.list.item3}</li>
                  </ol>
                </HeroWrapper>
              )}

            <FormWrapper>
              {!this.props.LoggedInUser && <H1Form>{this.state.formHeading}</H1Form>}

              {this.props.LoggedInUser ? (
                this.props.AllowedActions.includes('ACTIVATE_AGAIN') ? (
                  <CampaignRegUserForm vouchercode={vouchercode} />
                ) : (
                    <CampaignLoggedInForm vouchercode={vouchercode} />
                  )
              ) : (
                  <CampaignRegUserForm vouchercode={vouchercode} />
                )}

              <Disclaimer>
                {Translation.campaign.disclaimer}{' '}
                <StyledLink to="/medlemsvillkor">{Translation.campaign.link}</StyledLink>
              </Disclaimer>
            </FormWrapper>
          </ReactCSSTransitionGroup>
        </InnerWrapper>
      </PageWrapper>
    ) : (
        <Loading />
      );
  }
}

function mapStateToProps(state) {
  return {
    LoggedInUser: state.account.loggedIn,
    AllowedActions: state.account.userData.UserDetails.allowedactions,
  };
}

export default connect(mapStateToProps)(CampaignPage);
