import React from 'react';
import { Redirect } from 'react-router';

class RedirectCampaign extends React.PureComponent {
  render() {
    return <Redirect to={'/kampanj/sjprio?vouchercode=sjprio&utm_medium=partnership&utm_campaign=SJ&utm_content=sjprio&utm_source=partnership'} />;
  }
}

export default RedirectCampaign;
