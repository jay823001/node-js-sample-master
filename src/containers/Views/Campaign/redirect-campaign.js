import React from 'react';
import { Redirect } from 'react-router';

class RedirectCampaign extends React.PureComponent {
  render() {
    return <Redirect to={'/kampanjkod'} />;
  }
}

export default RedirectCampaign;
