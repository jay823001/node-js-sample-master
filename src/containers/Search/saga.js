import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { fetchSearch } from 'containers/App/api';
import {
  SEARCH_FETCHING,
  SEARCH_ERROR,
  SEARCH_SUCCESS,
  SEARCH_LOADMORE,
  SEARCH_LOADMORE_SUCCESS,
  SEARCH_LOADMORE_END,
} from './constants';

/** Search books **/
function* search(data) {
  if (data.params.searchtype === 'search') {
    yield put(push(`search?q=${data.params.query}`));
  }

  const params = {
    searchtype: data.params.searchtype,
    query: data.params.query,
    page: data.params.page,
    format: data.params.format,
  };

  // api call
  const searchdata = yield call(fetchSearch, params);

  if (searchdata.status === 200) {
    yield put({ type: SEARCH_SUCCESS, results: searchdata.data.books });
  } else {
    yield put({ type: SEARCH_ERROR, error: searchdata.error });
  }
}

// Register search saga
export function* searchSaga() {
  yield takeLatest(SEARCH_FETCHING, search);
}

/** Search more books **/
function* searchMore(data) {
  const params = {
    searchtype: data.params.searchtype,
    query: data.params.query,
    page: data.params.page,
    format: data.params.format,
  };

  // api call
  const searchdata = yield call(fetchSearch, params);

  if (searchdata.status === 200) {
    if (searchdata.data.books.length < 20) {
      yield put({ type: SEARCH_LOADMORE_END });
    } else {
      yield put({ type: SEARCH_LOADMORE_SUCCESS, results: searchdata.data.books });
    }
  } else {
    yield put({ type: SEARCH_ERROR, error: searchdata.error });
  }
}

// Register search more saga
export function* searchMoreSaga() {
  yield takeLatest(SEARCH_LOADMORE, searchMore);
}
