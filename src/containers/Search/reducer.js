import {
  SEARCH_FETCHING,
  SEARCH_ERROR,
  SEARCH_SUCCESS,
  SEARCH_LOADMORE,
  SEARCH_LOADMORE_SUCCESS,
  SEARCH_LOADMORE_END,
} from './constants';

// search reducer
const initialState = {
  query: '',
  format: '',
  searchtype: 'search',
  searching: false,
  loadmore: false,
  morepages: true,
  searchSuccess: false,
  results: [],
  pagenumber: 1,
  error: {},
};

export function SearchReducer(
  state = initialState,
  { type, query, error, results, searchtype, format, pagenumber }
) {
  switch (type) {
    case SEARCH_FETCHING:
      return {
        ...state,
        query: query,
        format: format,
        searchtype: searchtype,
        searching: true,
        searchSuccess: false,
        results: [],
        pagenumber: 1,
        error: {},
        morepages: true,
        loadmore: false,
      };

    case SEARCH_ERROR:
      return {
        ...state,
        searching: false,
        format: '',
        searchtype: 'search',
        pagenumber: null,
        error: error,
      };

    case SEARCH_SUCCESS:
      return {
        ...state,
        query: '',
        format: '',
        searchtype: 'search',
        searching: false,
        searchSuccess: true,
        results: results,
        morepages: true,
      };

    case SEARCH_LOADMORE:
      return {
        ...state,
        pagenumber: state.pagenumber + 1,
        loadmore: true,
      };

    case SEARCH_LOADMORE_SUCCESS:
      return {
        ...state,
        results: state.results.concat(results),
        searching: false,
        loadmore: false,
      };

    case SEARCH_LOADMORE_END:
      return {
        ...state,
        morepages: false,
      };

    default:
      return state;
  }
}
