import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const StyledMagnifyGlass = styled.svg`
  position: absolute;
  z-index: 1;
  top: 0.9rem;
  right: 1.1rem;
  width: 1.9rem;
  height: 1.9rem;
  pointer-events: none;
  path {
    fill: white;
  }
  ${props =>
    props.inFilter &&
    css`
      top: 1rem;
      path {
        fill: ${props => props.theme.colorGrey};
      }
    `};
`;

const MagnifyGlass = props => (
  <StyledMagnifyGlass inFilter={props.inFilter} viewBox="0 0 20 20">
    <path d="M13.8,8.5c0-1.5-0.5-2.8-1.6-3.8S9.9,3.1,8.5,3.1S5.7,3.6,4.7,4.7S3.1,7,3.1,8.5s0.5,2.8,1.6,3.8s2.3,1.6,3.8,1.6s2.8-0.5,3.8-1.6S13.8,9.9,13.8,8.5z M20,18.5c0,0.4-0.2,0.8-0.5,1.1c-0.3,0.3-0.7,0.5-1.1,0.5c-0.4,0-0.8-0.2-1.1-0.5l-4.1-4.1c-1.4,1-3,1.5-4.8,1.5c-1.1,0-2.2-0.2-3.3-0.7s-1.9-1-2.7-1.8s-1.4-1.7-1.8-2.7S0,9.6,0,8.5s0.2-2.2,0.7-3.3s1-1.9,1.8-2.7s1.7-1.4,2.7-1.8S7.3,0,8.5,0s2.2,0.2,3.3,0.7s1.9,1,2.7,1.8s1.4,1.7,1.8,2.7s0.7,2.1,0.7,3.3c0,1.8-0.5,3.4-1.5,4.8l4.1,4.1C19.9,17.7,20,18,20,18.5z" />
  </StyledMagnifyGlass>
);

MagnifyGlass.propTypes = {
  inFilter: PropTypes.bool,
};

export default MagnifyGlass;
