import {
  SEARCH_FETCHING,
  SEARCH_ERROR,
  SEARCH_SUCCESS,
  SEARCH_LOADMORE,
  SEARCH_LOADMORE_SUCCESS,
  SEARCH_LOADMORE_END,
} from './constants';

// Search actions
export const searchRequest = params => ({ type: SEARCH_FETCHING, params });
export const searchError = error => ({ type: SEARCH_ERROR, error });
export const searchSuccess = results => ({ type: SEARCH_SUCCESS, results });
export const searchLoadmore = params => ({ type: SEARCH_LOADMORE, params });
export const searchMoreSuccess = results => ({ type: SEARCH_LOADMORE_SUCCESS, results });
export const searchMoreEnd = () => ({ type: SEARCH_LOADMORE_END });
