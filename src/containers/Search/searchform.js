/* eslint react/no-danger:0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { apiUrl, internationalization_b, fetchAuth } from 'containers/App/api';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Form from 'components/Form/Form';
import Input from 'components/Form/Input';
import { closeMenu } from 'containers/App/actions/app-actions';
import MagnifyGlass from './MagnifyGlass';
import { boldString } from '../../utils/helpFunctions';
import Translation from 'translation/nextory-web-se';

const StyledForm = styled(Form)`
  width: 85%;
  max-width: 30rem;
  top: 0;
  margin: 4rem auto auto auto;
  position: relative;
  ${media.large`
    width: 13rem;
    margin: 0 0 0 -.7rem;
  `} @media (min-width: 1100px) {
    width: 19.7rem;
  }
  ${props =>
    props.inFilter &&
    css`
      position: relative;
      top: 3px;
      display: inline-block;
      width: 40%;
      margin: 0;
      vertical-align: top;
      ${media.medium`
      top: 0;
      max-width: 22rem;
    `};
    `};
`;

const SearchInput = styled(Input)`
  margin: -0.1rem 0 0 0;
  padding-top: 0.9rem;
  padding-bottom: 0.9rem;
  height: 3.8rem;
  ${props =>
    props.inFilter &&
    css`
      height: 4.2rem;
    `};
`;

const Submit = styled(Input)`
  position: absolute;
  top: 0;
  right: 0;
  width: 4.2rem;
  height: 4.1rem;
  margin: 0;
  padding-top: 0.9rem;
  border-radius: 0 0.3rem 0.3rem 0;
  box-shadow: none;
  border: 0.1rem solid ${props => props.theme.colorBlue};
  background-color: ${props => props.theme.colorBlue};
  cursor: pointer;
  height: 3.7rem;
  &:hover {
    background-color: ${props => props.theme.colorHoverBlue};
  }
  ${props =>
    props.inFilter &&
    css`
      background-color: transparent;
      border: 0.1rem solid transparent;
      height: 3.5rem;
      right: 0.1rem;
      top: 0.1rem;
      &:hover {
        background-color: transparent;
      }
      &:focus {
        border: 0.1rem solid transparent;
      }
    `};
`;

const Suggestions = styled.div`
  position: absolute;
  z-index: 510;
  top: 54px;
  width: 85%;
  margin: auto;
  left: 0;
  right: 0;
  border: 1px solid ${props => props.theme.colorGrey};
  border-radius: 5px;
  background-color: ${props => props.theme.colorWhite};
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
  ${media.medium`
    width: 220px;
    left: auto;
    right: auto;
  `} ${media.large`
    width: 250px;
  `}
  ul {
    padding: 23px 15px;
    list-style: none;
    li {
      line-height: 1.2;
      margin: 0 0 9px;
      padding-bottom: 0;
      a {
        display: block;
        padding-bottom: 8px;
        text-transform: none;
        ${media.large`
          &:hover {
            color: ${props => props.theme.colorBlue};
            border-bottom: 1px solid ${props => props.theme.colorBlue};
          }
        `};
      }
    }
  }
`;

const StyledLink = styled(({ active, ...rest }) => <Link {...rest} />)`
  color: ${props => props.theme.colorBlack};
  border-bottom: 1px solid #d7d7d7;
  ${props =>
    props.active &&
    css`
      color: ${props => props.theme.colorBlue};
      border-bottom: 1px solid ${props => props.theme.colorBlue};
    `};
`;

class Search extends React.PureComponent {
  static propTypes = {
    history: PropTypes.object,
    closeMenu: PropTypes.func,
    inFilter: PropTypes.bool,
  };

  state = {
    query: '',
    suggestion: [],
    cursor: -1,
    activesuggestion: '',
    loading: false,
    active: false,
    error: null,
  };

  componentDidMount() {
    this.timeout = 0;
    document.getElementById('allSearch').focus();
  }
  componentDidUpdate() {
    document.getElementById('allSearch').focus();
  }

  componentWillReceiveProps() {
    this.setState({
      query: '',
      suggestion: [],
      cursor: -1,
      activesuggestion: '',
      loading: false,
      active: false,
      error: null,
    });
  }

  // on form submit do a router redirect
  handleSubmit = e => {
    e.preventDefault();
    this.props.history.push(`/search?q=${this.state.query}`);
    this.setState({ active: false, suggestion: [], query: '' });
    this.props.closeMenu();
  };

  // when user writes in search input
  handleInput = e => {
    const input = e.target.value;
    this.setState({ query: input });
    if (input.length > 2) {
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.fetchSuggestions(input);
        this.setState({ loading: true });
      }, 800);
    } else {
      this.setState({ active: false, suggestion: [], loading: false });
    }
  };

  // on form blur
  handleBlur = () => {
    setTimeout(() => {
      this.setState({ active: false, loading: false });
    }, 200);
  };
  // on form focus
  handleFocus = () => {
    if (this.state.query.length > 2 && this.state.suggestion.length > 0) {
      this.setState({ active: true });
    }
  };

  // this function is used to navigate in search suggestion
  handleKeyDown = e => {
    if (e.keyCode === 38 && this.state.cursor > 0) {
      this.setState(prevState => ({
        cursor: prevState.cursor - 1,
        activesuggestion: this.state.suggestion[this.state.cursor - 1].text,
      }));
    } else if (e.keyCode === 40 && this.state.cursor < this.state.suggestion.length - 1) {
      this.setState(prevState => ({
        cursor: prevState.cursor + 1,
        activesuggestion: this.state.suggestion[this.state.cursor + 1].text,
      }));
    } else if (e.keyCode === 13 && this.state.cursor > -1) {
      e.preventDefault();
      this.props.history.push(`/search?q=${this.state.activesuggestion}`);
    }
  };

  // fetch suggestions base on input value
  fetchSuggestions = async input => {
    try {
      const response = await fetch(`${apiUrl}suggest?keyword=${input}${internationalization_b}`, {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      });
      const data = await response.json();
      if (this.state.query) {
        this.setState({
          suggestion: data.data.completions,
          error: null,
          cursor: -1,
          loading: false,
          active: true,
        });
      }
    } catch (error) {
      this.setState({
        active: false,
        loading: false,
        error: (Translation.search.searchform_error),
        suggestion: [],
      });
    }
  };

  // after we get results, map them
  renderSuggestions() {
    if (this.state.suggestion.length > 0) {
      return this.state.suggestion.map((book, i) => (
        <li key={book.ticket}>
          <StyledLink
            onClick={this.props.closeMenu}
            active={this.state.cursor === i}
            to={`/search?q=${book.text}`}
          >
            <span
              dangerouslySetInnerHTML={{
                __html: boldString(book.text, this.state.query),
              }}
            />
          </StyledLink>
        </li>
      ));
    } else {
      return null;
    }
  }

  render() {
    return (
      <span>
        <StyledForm
          onSubmit={this.handleSubmit}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          inFilter={this.props.inFilter}
        >
          <SearchInput
            autoComplete="off"
            autoCorrect="off"
            inFilter={this.props.inFilter}
            autoCapitalize="off"
            spellCheck="false"
            type="text"
            onChange={this.handleInput}
            onKeyDown={this.handleKeyDown}
            id="allSearch"
            value={this.state.query}
            placeholder={Translation.search.all}
          />

          <MagnifyGlass inFilter={this.props.inFilter} />

          <Submit
            inFilter={this.props.inFilter}
            id="search-book-submit"
            type="submit"
            onClick={this.handleSubmit}
            value=""
          />
        </StyledForm>

        {this.state.active &&
          this.state.suggestion.length > 0 && (
            <Suggestions>
              <ul>{this.renderSuggestions()}</ul>
            </Suggestions>
          )}
      </span>
    );
  }
}

function mapStateToProps(state) {
  return {
    SearchSuggest: state.appstate.DropDownSearch,
  };
}

export default withRouter(connect(mapStateToProps, { closeMenu })(Search));
