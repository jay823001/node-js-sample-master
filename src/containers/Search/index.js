import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { gup } from 'utils/helpFunctions';
import Waypoint from 'react-waypoint';
import DataLayer from 'containers/App/datalayer';
import { searchRequest, searchLoadmore } from './actions';
import MagnifyGlass from './MagnifyGlass';
import BookListWrapper from 'components/Books/BookList';
import Book from 'components/Books/BookList/BookCover';
import LoadingIndicator from 'components/LoadingIndicator';
import SearchIcon from './SearchIcon';
import Title from 'containers/Filter/Dropdown/Title';
import Value from 'containers/Filter/Dropdown/Value';
import Wrapper from 'containers/Filter/Dropdown/Wrapper';
import Translation from 'translation/nextory-web-se';
import {
  TypeIcon,
  ArrowDownIcon,
  ArrowUpIcon,
  BookIcon,
  SoundBookIcon,
} from 'containers/Filter/Dropdown/Icons';
import {
  Head,
  StyledForm,
  SearchInput,
  Submit,
  CatH1,
  BookWrapper,
  DropdownGroup,
  DropDown,
} from './StyledSearch';

class SearchView extends React.PureComponent {
  static propTypes = {
    location: PropTypes.object,
    booksearch: PropTypes.object,
    searchRequest: PropTypes.func,
    searchLoadmore: PropTypes.func,
  };

  state = {
    query: '',
    inputvalue: '',
    searchtype: 'search',
    format: '',
    dropdown: false,
  };

  componentDidMount() {
    this.timeout = 0;
    document.getElementById('searchInput').focus();
    // get results depending on URL
    let query = '';
    if (gup('q', this.props.location.search)) {
      query = gup('q', this.props.location.search);
    }
    this.setState(
      {
        searchtype: this.props.location.pathname.replace(/^\/+/g, ''),
        query: query,
      },
      () => {
        const data = {
          query: query,
          searchtype: this.state.searchtype,
          page: 1,
          format: this.state.format,
        };
        this.props.searchRequest(data);
      }
    );
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  componentDidUpdate() {
    document.getElementById('searchInput').focus();
  }
  // when user writes in search input, add value to state
  // and dispatch search after 800ms
  handleInput = e => {
    this.setState({ inputvalue: e.target.value });
    if (e.target.value.length > 2) {
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.setState({ query: this.state.inputvalue });
        const data = {
          query: this.state.inputvalue,
          searchtype: this.state.searchtype,
          page: 1,
          format: this.state.format,
        };
        this.props.searchRequest(data);
      }, 500);
    }
  };

  // format dropdown
  handleDropdown = () => {
    this.setState({ dropdown: !this.state.dropdown });
  };

  // choose format from dropdown
  setFormat = format => {
    this.setState(
      {
        format: format,
      },
      () => {
        const data = {
          query: this.state.query,
          searchtype: this.state.searchtype,
          page: 1,
          format: this.state.format,
        };
        this.props.searchRequest(data);
      }
    );
  };

  // when user submit, dispatch searchRequest
  handleSubmit = e => {
    e.preventDefault();
    this.setState(
      {
        query: this.state.inputvalue,
        format: '',
        dropdown: false,
      },
      () => {
        const data = {
          query: this.state.query,
          searchtype: 'search',
          page: 1,
          format: this.state.format,
        };
        this.props.searchRequest(data);
      }
    );
  };

  // Load more posts of current search
  loadMore = () => {
    const data = {
      query: this.state.query,
      searchtype: this.state.searchtype,
      page: this.props.booksearch.pagenumber + 1,
      format: this.state.format,
    };
    this.props.searchLoadmore(data);
  };

  // render book covers
  renderBooks() {
    if (this.props.booksearch.results === undefined || this.props.booksearch.results.length === 0)
      return false;
    return this.props.booksearch.results.map(
      ({ bookid, coverimg, weburl, formattype, relatedbookid, title }) => {
        const cleanurl = weburl.substring(0, weburl.indexOf('?'));
        return (
          <div key={Math.random()}>
            <Book
              bookCover={coverimg}
              booktitle={title}
              url={cleanurl}
              formattype={formattype.format}
              relatedbook={relatedbookid}
            />
          </div>
        );
      }
    );
  }

  render() {
    const { error, searching, results, loadmore, morepages } = this.props.booksearch;

    let format = 'Alla';
    let formaticon = <SoundBookIcon />;
    if (this.state.format === 'E-bok') {
      format = (Translation.search.ebooks);
      formaticon = <BookIcon />;
    } else if (this.state.format === 'Ljudbok') {
      format = (Translation.search.soundbooks);
      formaticon = <TypeIcon />;
    } else {
      format = (Translation.search.all);
      formaticon = <SoundBookIcon />;
    }

    const titleClean = decodeURI(this.state.query);

    return (
      <div>
        <Head>
          <StyledForm onSubmit={this.handleSubmit}>
            <SearchInput onChange={this.handleInput} type="text" placeholder={Translation.search.placeholder} id="searchInput" />
            <MagnifyGlass inFilter />
            <Submit type="submit" onClick={this.handleSubmit} value="" />
          </StyledForm>

          <DropdownGroup>
            <Wrapper onClick={this.handleDropdown}>
              {formaticon}
              <Title>{Translation.search.type}</Title>
              <Value>
                <span>{format}</span>
              </Value>

              {this.state.dropdown ? (
                <span>
                  <ArrowUpIcon />
                  <DropDown>
                    <li
                      onClick={() => this.setFormat('')}
                      style={
                        format === 'Alla'
                          ? {
                            color: '#2226db',
                            borderBottom: '.1rem solid #2226db',
                            pointerEvents: 'none',
                          }
                          : null
                      }
                    >
                      {Translation.search.all}
                    </li>
                    <li
                      onClick={() => this.setFormat('E-bok')}
                      style={
                        format === 'E-böcker'
                          ? {
                            color: '#2226db',
                            borderBottom: '.1rem solid #2226db',
                            pointerEvents: 'none',
                          }
                          : null
                      }
                    >
                      {Translation.search.ebooks}
                    </li>
                    <li
                      onClick={() => this.setFormat('Ljudbok')}
                      style={
                        format === 'Ljudböcker'
                          ? {
                            color: '#2226db',
                            borderBottom: '.1rem solid #2226db',
                            pointerEvents: 'none',
                          }
                          : null
                      }
                    >
                      {Translation.search.soundbooks}
                    </li>
                  </DropDown>
                </span>
              ) : (
                  <ArrowDownIcon />
                )}
            </Wrapper>
          </DropdownGroup>
        </Head>

        <BookListWrapper>
          {Object.keys(error).length !== 0 && <CatH1>{Translation.search.what_are}</CatH1>}

          {searching && (
            <span>
              <CatH1>{Translation.search.loading_results}{titleClean}</CatH1>
              <LoadingIndicator />
            </span>
          )}

          {!searching &&
            Object.keys(error).length === 0 && (
              <div>
                <DataLayer />
                {results.length > 0 ? (
                  <div>
                    <SearchIcon />
                    <CatH1>{Translation.search.search_results}: {titleClean}</CatH1>
                    <BookWrapper>{this.renderBooks()}</BookWrapper>
                  </div>
                ) : (
                    <CatH1>{Translation.search.found_nothing_on}: {titleClean}</CatH1>
                  )}
                {!searching &&
                  !loadmore &&
                  morepages && <Waypoint onEnter={this.loadMore} bottomOffset={'-50px'} />}
                {loadmore && morepages && <LoadingIndicator />}
              </div>
            )}
        </BookListWrapper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    booksearch: state.search,
  };
}

export default connect(mapStateToProps, { searchRequest, searchLoadmore })(SearchView);
