import styled from 'styled-components';
import media from 'theme/styled-utils';
import Input from 'components/Form/Input';
import Form from 'components/Form/Form';
import H1 from 'components/Typography/H1';

export const Head = styled.section`
  background-color: ${props => props.theme.colorWhite};
  position: relative;
  z-index: 200;
  padding: 1.3rem 1.5rem 0.5rem;
  max-width: 90rem;
  margin-right: auto;
  margin-left: auto;
  ${media.large`
    padding: 2rem 1.5rem;
  `};
`;

export const StyledForm = styled(Form)`
  max-width: 30rem;
  margin: 4rem auto auto auto;
  position: relative;
  top: 3px;
  display: inline-block;
  width: 40%;
  margin: 0;
  vertical-align: top;
  ${media.medium`
    top: 0;
    max-width: 22rem;
  `};
`;

export const SearchInput = styled(Input)`
  margin: -0.1rem 0 0 0;
  padding-top: 0.9rem;
  padding-bottom: 0.9rem;
  height: 4.2rem;
`;

export const Submit = styled(Input)`
  position: absolute;
  top: 0;
  right: 0;
  width: 4.2rem;
  margin: 0;
  padding-top: 0.9rem;
  border-radius: 0 0.3rem 0.3rem 0;
  box-shadow: none;
  cursor: pointer;
  background-color: transparent;
  border: 0.1rem solid transparent;
  height: 3.5rem;
  right: 0.1rem;
  top: 0.1rem;
  &:hover {
    background-color: transparent;
  }
  &:focus {
    border: 0.1rem solid transparent;
  }
`;

export const CatH1 = styled(H1)`
  color: ${props => props.theme.colorDarkBlue};
  display: inline-block;
  margin-bottom: 2.8rem;
  font-size: 1.8rem;
  ${media.medium`
    margin-bottom: 4rem;
    font-size: 2.5rem;
  `} ${media.large`
    font-size: 3.1rem;
  `};
`;

export const BookWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  div {
    margin: 0.5rem;
    ${media.medium`
      margin: 1rem;
    `};
  }
`;

export const DropdownGroup = styled.div`
  display: inline-block;
  width: 60%;
  user-select: none;
  ${media.large`
    margin-top: .9rem;
    float:right;
    width:auto;
  `};
`;

export const DropDown = styled.ul`
  font-weight: 400;
  text-align: left;
  position: absolute;
  z-index: 510;
  top: 80%;
  right: 0;
  width: 160px;
  padding: 2.3rem 1.5rem 1.5rem;
  list-style: none;
  text-transform: none;
  border: 0.1rem solid ${props => props.theme.colorGrey};
  border-radius: 0.5rem;
  background-color: ${props => props.theme.colorWhite};
  box-shadow: 0 0.1rem 1rem rgba(0, 0, 0, 0.2);
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;

  li {
    line-height: 1.2;
    display: block;
    margin: 0 2% 0.9rem;
    display: block;
    padding-bottom: 0.8rem;
    border-bottom: 0.1rem solid #d7d7d7;
    color: ${props => props.theme.colorBlack};
    cursor: pointer;
    &:hover {
      color: ${props => props.theme.colorBlue};
      border-bottom: 0.1rem solid ${props => props.theme.colorBlue};
    }
  }
`;
