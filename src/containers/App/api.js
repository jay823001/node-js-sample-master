import { getSubscriptionIdByName } from './common';
import i18next from 'newui/i18n'
/**
 * Here we try to collect everything that calls the API
 * Important to notice is that we use different @apiUrl and @adyenKey
 * for development and for production build.
 * For local dev, add this url as prefix to avoid cors error:
 * https://cors-anywhere.herokuapp.com
 */

//  ******************
//    API
//  ******************

// ******** languages setting ********************
const tld = window.location.origin.split('.').pop()
let lang = localStorage.getItem('locale') === 'sv_SE' ? 'SW' : 'FI'

var LANGUAGE = lang; //<== For finland use FI, For sweden use SW.

var LANGUAGE_PARAMS_A;
var LANGUAGE_PARAMS_B;

if (LANGUAGE === "SW") {

  LANGUAGE_PARAMS_A = `?locale=${i18next.language}`//"?locale=sv_SE";
  LANGUAGE_PARAMS_B = `&locale=${i18next.language}`//"&locale=sv_SE";

} else if (LANGUAGE === "FI") {

  LANGUAGE_PARAMS_A = `?locale=${i18next.language}`; // ?locale=fi_FI
  LANGUAGE_PARAMS_B = `&locale=${i18next.language}`; // &locale=fi_FI

}
// ************************************************

export const internationalization_a = LANGUAGE_PARAMS_A;
export const internationalization_b = LANGUAGE_PARAMS_B;
export const internationalizationLanguage = LANGUAGE;
localStorage.setItem('LANGUAGE', LANGUAGE);
//  ******************
/* DEV API CONSTANTS */
//  ******************
let environment = window.location.host.includes('nextory') ? 'live' : 'test'
let apiUrl_
let adyenKey_
let smallCover_
let largeCover_
if (environment === 'test') {
  apiUrl_ = 'http://104.155.208.232/api/web/5.0/';

  adyenKey_ =
    '10001|8E8A9925D9CD9C1E3D3508AF09A58163A722DE04DEAD0CFCB145B765ED9C719132D99421FA08C476D0E1033A730FD9211CC36CDB4355226EF228E65C30C9EA22C5EC1183B006B221A6704559E2F9B02BFF2BDB7E7278BD778EE28A7E870D51E5CCF473002B06A5AFC250AC1CA4911784E7FED2A65B17098388E1404C6F73268317A04A0251F6D1D75D2B3455F112CE92364432AAC460342CEDEB375CAE7ABFE5A66ED9157C3D2724B116DAC8F39ED1EDE20758447512633E2C39753EFBBEBB647C857600B1938260833A0053E5FE9297867DA8F5B84D4C20694AE61A7F3CFB3417D09D4C56BB31F6E2E2E8A4C64A605BE0EE60B9ADF9E1CDED22C931000D72B1';
  smallCover_ = 'http://104.155.208.232:9190/coverimg/130/';
  largeCover_ = 'http://104.155.208.232:9190/coverimg/274/';
} else if (environment === 'live') {

  //  ******************
  /* PRODUCTION API CONSTANTS */
  // ******************
  apiUrl_ = `https://www.nextory.${tld}/api/web/5.0/`;
  adyenKey_ =
    '10001|DB7C97720201F9ABE9A580275D98D33EAE1C9D2CA8D73468D0E83B4624DA567E2D14A3696B44A67783DEA8C12E4EA36A3A927AA0959F1C50DB04BB17E3896AC9253930CF06939E4345E6F06331A4E8612A83A2A1B739D7DFBFCFF710EFB2DA4F1505B1D58E9FB0D0CBB74556B6372169819E9ABCD4B7632B42CF5F2A14142EA7F171FABB0CE7589863DDB14C12BAE316C6E1DAD660CB231C2F6888CBC7A1BD66AC68F5D58DA38C88D121E8F83BABE08FE66C0B8168B8D8C04FE8AF577714843C61EEEED2FD7C17C56DC28C426B702B82D1B642EC1B9A71E1F92998122D2246919D8E3BEF4F164AF17DC89FDB557F0B766A2742540CD2EDBDBAC769F9FD896481';
  smallCover_ = `https://www.nextory.${tld}/coverimg/130/`;
  largeCover_ = `https://www.nextory.${tld}/coverimg/274/`;
}
export const apiUrl = apiUrl_
export const adyenKey = adyenKey_
export const smallCover = smallCover_
export const largeCover = largeCover_

// global constant
export const fetchAuth = 'Basic bmV4dG9yeXVpOnRvYmVkZWNpZGVk';

export function getHeaders(authkey) {
  const headers = {
    'Cache-Control': 'no-cache',
    Authorization: fetchAuth,
  };
  if (authkey.authkey) headers['nx-at'] = authkey.authkey;
  return headers;
}

/**
 * This fetches all books shown on home and /bocker
 * */
export const fetchData = async authkey => {
  try {
    const response = await fetch(
      `${apiUrl}MaincategorySpecificLoadStep?format=-1&windowsize=18&pagenumber=1${internationalization_b}`,
      {
        headers: await getHeaders(authkey),
        credentials: 'same-origin',
      }
    );
    const books = await response.json();
    return books;
  } catch (e) {
    return e;
  }
};

/**
 * This fetches all books /e-bocker
 * */
export const fetchDataEbook = async authkey => {
  try {
    const response = await fetch(
      `${apiUrl}MaincategorySpecificLoadStep?format=1&windowsize=18&pagenumber=1${internationalization_b}`,
      {
        headers: await getHeaders(authkey),
        credentials: 'same-origin',
      }
    );
    const ebooks = await response.json();
    return ebooks;
  } catch (e) {
    return e;
  }
};

/**
 * This fetches all books /ljudbocker
 * */
export const fetchDataSbook = async authkey => {
  try {
    const response = await fetch(
      `${apiUrl}MaincategorySpecificLoadStep?format=2&windowsize=18&pagenumber=1${internationalization_b}`,
      {
        headers: await getHeaders(authkey),
        credentials: 'same-origin',
      }
    );
    const sbooks = await response.json();
    return sbooks;
  } catch (e) {
    return e;
  }
};

/**
 * This fetches all book categoriyes
 * */
export const fetchDataCategories = async () => {
  try {
    const response = await fetch(`${apiUrl}categories${internationalization_a}`, {
      headers: {
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
    });
    const categories = await response.json();
    return categories;
  } catch (e) {
    return e;
  }
};

/**
 * Login post
 */
export const fetchLogin = async data => {
  const fd = new FormData();
  // if (!data.data.authkey) {
  fd.append('email', data.data.email);
  fd.append('password', data.data.password);
  // }
  fd.append('serverdetails', '127.0.0.1');
  try {
    const response = await fetch(`${apiUrl}login${internationalization_a}`, {
      method: 'POST',
      headers: {
        'nx-at': data.data.authkey ? data.data.authkey : null,
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      body: fd,
    });
    const userdata = await response.json();
    const refreshkey = await response.headers.get('nx-rt');
    const authkey = await response.headers.get('nx-at');
    userdata.refreshkey = refreshkey;
    userdata.authkey = authkey;
    // This storage used for check internal login
    if (data.data.password) {
      localStorage.setItem('pword', data.data.password)
    }
    return userdata;
  } catch (error) {
    return error;
  }
};

/**
 * Login via websitekey
 */
export const fetchWebsitekey = async key => {
  const fd = new FormData();
  fd.append('webaccesskey', key);
  try {
    const response = await fetch(`${apiUrl}reload${internationalization_a}`, {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      body: fd,
    });
    const userdata = await response.json();
    const refreshkey = await response.headers.get('nx-rt');
    const authkey = await response.headers.get('nx-at');
    userdata.refreshkey = refreshkey;
    userdata.authkey = authkey;
    return userdata;
  } catch (error) {
    return error;
  }
};

/**
 * Refresh authkey
 * */
export const refreshAuthkey = async data => {
  try {
    const response = await fetch(`${apiUrl}token${internationalization_a}`, {
      headers: {
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
        'nx-at': data.authkey,
        'nx-rt': data.refreshkey,
      },
      credentials: 'same-origin',
    });
    const userdata = await response.json();
    const authkey = await response.headers.get('nx-at');
    userdata.newauthkey = authkey;
    return userdata;
  } catch (e) {
    return e;
  }
};

/**
 * User detail request
 * */
export const fetchUserDetails = async key => {
  // console.log(key)
  try {
    const response = await fetch(`${apiUrl}userdetails${internationalization_a}`, {
      headers: {
        'Cache-Control': 'no-cache',
        'nx-at': key,
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
    });
    const UserDetails = await response.json();
    return UserDetails.data;
  } catch (error) {
    return error;
  }
};

/**
 * Update profile post
 * */
export const postUpdateProfile = async data => {
  let firstname = '';
  if (data.data.firstname) {
    firstname = `&firstname=${data.data.firstname}`;
  }

  let lastname = '';
  if (data.data.lastname) {
    lastname = `&lastname=${data.data.lastname}`;
  }

  let address = '';
  if (data.data.address) {
    address = `&address=${data.data.address}`;
  }

  let city = '';
  if (data.data.city) {
    city = `&city=${data.data.city}`;
  }

  let postcode = '';
  if (data.data.postcode) {
    postcode = `&postcode=${data.data.postcode}`;
  }

  let cellphone = '';
  if (data.data.cellphone) {
    cellphone = `&cellphone=${data.data.cellphone}`;
  }

  let password = '';
  if (data.data.password) {
    password = `&password=${data.data.password}`;
  }

  try {
    const response = await fetch(
      `${apiUrl}updateprofile?newsletter=true${firstname}${lastname}${address}${city}&email=${
      data.data.email
      }${cellphone}${postcode}${password}${internationalization_b}`,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'nx-at': data.data.auth,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(data),
      }
    );
    const status = await response.json();
    return status;
  } catch (error) {
    return error;
  }
};

/**
 * Update subscription post
 * */
export const postUpdateSubscription = async data => {
  let subscriptionid = getSubscriptionIdByName(data.data.updatesub)
  try {
    const response = await fetch(` ${apiUrl}subscription/update?newsubscriptionid=${subscriptionid}${internationalization_b}`, {
      headers: {
        'Cache-Control': 'no-cache',
        'nx-at': data.data.authKey,
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      method: 'POST',
      body: JSON.stringify(data),
    });
    const status = await response.json();
    return status;
  } catch (error) {
    return error;
  }
};

/**
 * Forgot password post
 * */
export const postForgotPassword = async data => {
  try {
    const response = await fetch(`${apiUrl}forgotpassword?email=${data.data}${internationalization_b}`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      method: 'POST',
      body: JSON.stringify(data),
    });
    const status = await response.json();
    return status;
  } catch (error) {
    return error;
  }
};

/**
 * before change check the password
 * */
export const validateChangePassword = async data => {
  try {
    // console.log(data);
    const response = await fetch(
      `${apiUrl}changepassword?userid=${data.data.id}&hash=${data.data.authkey}&newpassword=${
      data.data.password
      }&processType=VALIDATE_FORGOT_PASSWORD${internationalization_b}`,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(data),
      }
    );
    const status = await response.json();
    // console.log(status);
    return status;
  } catch (error) {
    return error;
  }
};

/**
 * Change password post
 * */
export const postChangePassword = async data => {
  try {
    const response = await fetch(
      `${apiUrl}changepassword?userid=${data.data.id}&hash=${data.data.authkey}&newpassword=${
      data.data.password
      }&processType=PROCESS_FORGOT_PASSWORD${internationalization_b}`,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(data),
      }
    );
    const status = await response.json();
    return status;
  } catch (error) {
    return error;
  }
};

/**
 * Create user post
 * */
export const postUserCreate = async data => {
  console.log('api >> #Tw1Mx')
  // console.log(data);
  const giftV = 'giftvoucher' in data.data;
  const campC = 'campaigncode' in data.data;
  const fd = new FormData();
  fd.append('email', data.data.email);
  fd.append('confirmemail', data.data.email);
  fd.append('password', data.data.password);
  fd.append('serverdetails', '127.0.0.1');
  fd.append('newsletter', 'true');

  if (giftV) {
    // if giftvoucher exits in request
    fd.append('giftvoucher', data.data.giftvoucher);
    fd.append('subscriptionid', data.data.subscriptionid);
    fd.append('signuptype', data.data.signuptype);
  } else if (campC) {
    // if campaigncode exits in request
    fd.append('campaigncode', data.data.campaigncode);
    // fd.append('subscriptionid', '6');
    fd.append('signuptype', 'REDEEM_CAMPAIGN');
  } else {
    // if neither campaigncode nor giftvoucher exits in request
    fd.append('subscriptionid', data.data.subid);
  }

  try {
    const response = await fetch(`${apiUrl}usersignup${internationalization_a}`, {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      body: fd,
    });
    const userdata = await response.json();
    const authkey = await response.headers.get('nx-at');
    const refreshauthkey = await response.headers.get('nx-at');
    userdata.authkey = authkey;
    userdata.refreshauthkey = refreshauthkey;
    console.log('api >> #Ie7KF') // log a reference point (random number with a hash) for troubleshooting
    if (userdata.authkey) localStorage.setItem('sub-period', JSON.stringify(userdata.data.paymentdetails.period))
    return userdata;
  } catch (error) {
    return error;
  }
};

/**
 * Register payment
 * */
export const postRegPayment = async data => {
  const fd = new FormData();
  fd.append('adyenancrypteddata', data.data.adyenancrypteddata);
  fd.append('subscriptionid', data.data.subscriptionid);
  fd.append('paymenttype', data.data.paymenttype);
  fd.append('paymentgateway', data.data.paymentgateway);
  fd.append('orderid', data.data.orderid);
  if (data.data.signuptype) {
    fd.append('signuptype', data.data.signuptype);
  }
  if (data.data.customerid) {
    fd.append('customerid', data.data.customerid);
  }
  try {
    const response = await fetch(`${apiUrl}payment${internationalization_a}`, {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        'nx-at': data.data.authkey,
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      body: fd,
    });
    const userdata = await response.json();
    return userdata;
  } catch (error) {
    return error;
  }
};

/**
 * Redeem campaign for logged in user
 * */
export const postUserCampaign = async data => {
  const fd = new FormData();
  fd.append('email', data.data.email);
  fd.append('confirmemail', data.data.email);
  fd.append('password', data.data.password);
  fd.append('serverdetails', '127.0.0.1');
  fd.append('newsletter', 'true');
  fd.append('campaigncode', data.data.campaigncode);
  fd.append('subscriptionid', '6');
  fd.append('signuptype', 'REDEEM_CAMPAIGN');

  try {
    const response = await fetch(`${apiUrl}usersignup${internationalization_a}`, {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      body: fd,
    });
    const userdata = await response.json();
    const authkey = await response.headers.get('nx-at');

    userdata.authkey = authkey;

    return userdata;
  } catch (error) {
    return error;
  }
};

/*
* Validate giftvoucher code
*/
export const getValidateGiftvoucher = async data => {
  try {
    const response = await fetch(`${apiUrl}validategiftcard?giftvoucher=${data.giftvoucher}&${internationalization_b}`, {
      method: 'GET',
      headers: {
        'Cache-Control': 'no-cache',
        'nx-at': data.authkey,
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      //body: fd,
    });
    const giftcode = await response.json();
    const authkey = await response.headers.get('nx-at');
    giftcode.authkey = authkey;
    console.log('api >> #1UBaw')
    // console.log(giftcode)
    // console.log(data)
    return giftcode;
  } catch (error) {
    return error;
  }
};

/*
* Redeem giftvoucher for existing user
*/
export const postRedeemGiftvoucer = async data => {
  let subscriptionid = data.subscriptionid ? data.subscriptionid : ''
  const fd = new FormData();
  fd.append('email', data.email);
  fd.append('confirmemail', data.email);
  fd.append('password', data.password);
  fd.append('serverdetails', '127.0.0.1');
  fd.append('newsletter', 'true');
  fd.append('giftvoucher', data.giftvoucher);
  fd.append('subscriptionid', subscriptionid);
  fd.append('signuptype', 'REDEEM_GIFTCARD');
  try {
    const response = await fetch(`${apiUrl}usersignup${internationalization_a}`, {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        'nx-at': data.authkey,
        Authorization: fetchAuth,
      },
      credentials: 'same-origin',
      body: fd,
    });
    const userdata = await response.json();
    const authkey = await response.headers.get('nx-at');
    userdata.authkey = authkey;
    console.log('api >> #Ie7KF12345')
    // console.log(userdata)
    // console.log(data)
    return userdata;
  } catch (error) {
    return error;
  }
};

/*
* Retry payment
*/
export const retryPayment = async newdata => {
  try {

    //Get NewToken  and then use that token to make the payment
    const response = await fetch(
      `${apiUrl}order/recreate?paymenttype=${newdata.paymenttype}&subscriptionid=${
      newdata.subscriptionid
      }&orderid=${newdata.orderid}${internationalization_b}`,
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          'nx-at': newdata.authkey,
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
        method: 'POST',
        body: JSON.stringify(newdata),
      }
    );
    const neworderid = await response.json();
    return neworderid;
  } catch (error) {
    return error;
  }
};

/*
* Logout
*/
export const logoutAPI = async authkey => {
  try {
    const response = await fetch(`${apiUrl}logout${internationalization_a}`, {
      headers: {
        'Cache-Control': 'no-cache',
        Authorization: fetchAuth,
        'nx-at': authkey,
      },
      credentials: 'same-origin',
    });
    const value = await response.json();
    return value;
  } catch (e) {
    return e;
  }
};

/*
* Search
*/
export const fetchSearch = async params => {
  try {
    const response = await fetch(
      `${apiUrl}search?type=${params.searchtype}&keyword=${params.query}&pagenumber=${
      params.page
      }&windowsize=50&format=${params.format}${internationalization_b}`,
      {
        headers: {
          'Cache-Control': 'no-cache',
          Authorization: fetchAuth,
        },
        credentials: 'same-origin',
      }
    );
    const data = await response.json();
    return data;
  } catch (e) {
    return e;
  }
};
