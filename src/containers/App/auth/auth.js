/* eslint-disable */
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import Translation from 'translation/nextory-web-se';

const cookies = new Cookies();

const auth = {

  // Checks if anybody is logged in
  loggedIn() {
    if (cookies.get('user')) {
      return true;
    } else {
      return false;
    }
  },

  // Keep userdata in state on page refresh
  userData() {
    const empty = {
      userid: '',
      subscriptiontype: '',
      membertypecode: '',
      authkey: '',
      ismember: '',
      isoldmember: '',
      subscriptionid: '',
      userCountry: '',
      email: '',
      p: '',
      UserDetails: {},
    };
    if (cookies.get('user')) {
      return JSON.parse(Base64.decode(cookies.get('user')));
    } else {
      return empty;
    }
  },

  // Keep registration in state
  activeRegistration() {
    if (cookies.get('reg')) {
      return true;
    } else {
      return false;
    }
  },
  choosenSubsciption() {
    //eslint-disable-next-line
    let CHOSENSUBSCRIPTION
    const Silver = Translation.app.common.silver
    const SilverPlus = Translation.app.common.silverplus
    const Gold = Translation.app.common.gold
    const FamilyPlus = Translation.app.common.familyplus
    const Family2 = Translation.app.common.family2

    // SET THE DEFAULT CHOSEN SUBSCRIPTION HERE
    // Existing package are retrieved from Translations and stored in above constants
    // Usage eample: CHOSENSUBSCRIPTION = Family2

    CHOSENSUBSCRIPTION = Family2 // Change here

    // Do not Edit below
    try {
      const serializedState = localStorage.getItem('sub-state');
      if (serializedState === null) {
        return CHOSENSUBSCRIPTION;
      }
      return JSON.parse(serializedState);
    } catch (err) {
      return CHOSENSUBSCRIPTION;
    }
  },
  chooseMonth() {
    //eslint-disable-next-line
    let CHOSENMONTH
    const chooseOneUser = '1 månad'
    const chooseThreeUsers = '3 månader'
    const chooseSixUsers = '6 månader'
    // const chooseOneUserAmount = 199 
    // const chooseThreeUsersAmount = 597 
    // const chooseSixUsersAmount = 1194 

    CHOSENMONTH = chooseOneUser

    // Do not Edit below
    try {
      const serializedMonth = localStorage.getItem('choosen-month');
      if (serializedMonth === null) {
        return CHOSENMONTH;
      }
      return JSON.parse(serializedMonth);
    } catch (err) {
      return CHOSENMONTH;
    }
  },
  chooseAmount() {
    //eslint-disable-next-line
    let CHOSENAMOUNT
    const chooseOneUserAmount = 199
    const chooseThreeUsersAmount = 597
    const chooseSixUsersAmount = 1194

    CHOSENAMOUNT = chooseOneUserAmount

    // Do not Edit below
    try {
      const serializedAmount = localStorage.getItem('choosen-amount');
      if (serializedAmount === null) {
        return CHOSENAMOUNT;
      }
      return JSON.parse(serializedAmount);
    } catch (err) {
      return CHOSENAMOUNT;
    }
  },
  userRegistration() {
    const empty = {
      email: '',
      campaigncode: '',
      campaignname: '',
      campaignprice: '',
      campaignintervel: '',
      campaignintervelindays: '',
      campaignperiod: '',
      campaigndays: '',
      campaigndiscounted: '',
      campaigntrialdays: '',
      campaignfixed: '',
      paymentdetails: '',
      paymentrequired: '',
      newuser: '',
      orderid: '',
      paymenttype: '',
      authkey: '',
      userid: '',
    };
    if (cookies.get('reg')) {
      return JSON.parse(Base64.decode(cookies.get('reg')));
    } else {
      return empty;
    }
  },

  onChange() { },
};

export default auth;
