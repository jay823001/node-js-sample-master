import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = (props) => (
  <Route
    {...props.routeProps}
    render={() => (
      props.LoggedIn
        ? (<div>{props.children}</div>)
        : (<Redirect to={{ pathname: '/' }} />)
    )}
  />
);

PrivateRoute.propTypes = {
  routeProps: PropTypes.func,
  LoggedIn: PropTypes.bool,
  children: PropTypes.node,
};

function mapStateToProps(state) {
  return {
    LoggedIn: state.account.loggedIn,
  };
}

export default connect(mapStateToProps, null, null, {
  pure: false,
})(PrivateRoute);
