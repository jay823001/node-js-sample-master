import React, { Component } from "react";
import { connect } from "react-redux";
import styled, { css } from "styled-components";
import { Helmet } from "react-helmet";
import { withRouter } from "react-router";
import { Switch, Route } from "react-router-dom";
import media from "theme/styled-utils";
import NavBar from "containers/Menu";
import NavBarNew from "containers/MenuNew";
import Footer from "components/Footer/";
import Overlay from "components/Overlay";
// import HomePageB from "containers/Views/HomePageB";
import HomePageSummer from "containers/Views/HomePageSummer";
import PageNotFound from "newui/containers/Views/Static/404";
import RedirectSubscriptionPage from "containers/Views/Registration/SubscriptionPage/redirect-bli-medlem";
import PrivateRoute from "containers/App/auth/privateroute";
// import SubscriptionPage from "containers/Views/Registration/SubscriptionPage";
// import RegisterUser from "containers/Views/Registration/RegisterUser";
// import RedirectCampaign from "containers/Views/Campaign/redirect-campaign";
// import CookieBanner from "containers/CookieBanner";
import * as Routes from "./routes";
import NextoryMetaImg from "./Nextory_blue.png";
import Translation from "translation/nextory-web-se";
import { withNamespaces } from 'react-i18next';
import i18next from 'newui/i18n'
import { GetFlowColor } from 'newui/utils/helperFunctions'
import { Redirect } from 'react-router-dom'
import { getbFlowUrls } from 'newui/utils/helperFunctions'

// IMPORTS FROM NEW UI
//----------------------------------------------
// import TrialFlow from 'newui/containers/Views/Register/TrialFlow'
import PrivateRouteNew from 'newui/containers/App/Auth/privateRoute'
import NavigationMenu from 'newui/containers/Views/Account/Menu'
import MyDetails from 'newui/containers/Views/Account/Details'
import CancelMessage from 'newui/containers/Views/Account/CancelSubscription/CancelMessage'
import CancelReason from 'newui/containers/Views/Account/CancelSubscription/CancelReason'
import CancelCompleted from 'newui/containers/Views/Account/CancelSubscription/CancelCompleted'
import MyOrders from 'newui/containers/Views/Account/MyOrders/Orders'
import MyReceipt from 'newui/containers/Views/Account/MyOrders/Receipt'
import MySubscription from 'newui/containers/Views/Account/ChangeSubscription/CurrentSubscription'
import Offers from 'newui/containers/Views/Account/MyOffers'
import PaymentDetails from 'newui/containers/Views/Account/ChangePayment/PaymentDetails'
import SilverFaqQuestions from 'newui/containers/Views/Static/App/Silver-faq/Questions'
import SilverFaqAnswer from 'newui/containers/Views/Static/App/Silver-faq/Answers'
import CMSPage from 'newui/containers/Views/Static/CMS'
//----------------------------------------------
// END OF IMPORTS FROM NEW UI

// New registration flow css TODO: make it blobal after hwole project updated
const scrollMinHeightLarge =
  window.location.pathname === i18next.t('lang_route:registration.trialFlow.subscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:registration.campaignFlow.createAccount') ||
    window.location.pathname === i18next.t('lang_route:registration.campaignFlow.subscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:mypages.reActivationFlow.subscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:mypages.account') ||
    window.location.pathname === i18next.t('lang_route:mypages.myDetails') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionMessage') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionReason') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionCompleted') ||
    window.location.pathname === i18next.t('lang_route:mypages.orders') ||
    window.location.pathname === i18next.t('lang_route:mypages.receipt') ||
    window.location.pathname === i18next.t('lang_route:mypages.login') ||
    window.location.pathname === i18next.t('lang_route:CMS.about') ||
    window.location.pathname === i18next.t('lang_route:CMS.cookies') ||
    window.location.pathname === i18next.t('lang_route:CMS.app') ||
    window.location.pathname === i18next.t('lang_route:CMS.privacy_policy') ||
    window.location.pathname === i18next.t('lang_route:CMS.terms') ||
    window.location.pathname === i18next.t('lang_route:giftCard.select') ||
    window.location.pathname === '/' ||
    window.location.pathname === '/404'
    ? "initial"
    : `36rem`;
const scrollMinHeightMedium =
  window.location.pathname === i18next.t('lang_route:registration.trialFlow.subscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:registration.campaignFlow.createAccount') ||
    window.location.pathname === i18next.t('lang_route:registration.campaignFlow.subscriptionBasic') ||
    window.location.pathname.includes(i18next.t('lang_route:registration.campaignFlow.customCampaign')) ||
    window.location.pathname === i18next.t('lang_route:mypages.reActivationFlow.subscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:mypages.account') ||
    window.location.pathname === i18next.t('lang_route:mypages.myDetails') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionMessage') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionReason') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionCompleted') ||
    window.location.pathname === i18next.t('lang_route:mypages.orders') ||
    window.location.pathname === i18next.t('lang_route:mypages.login') ||
    window.location.pathname === i18next.t('lang_route:CMS.about') ||
    window.location.pathname === i18next.t('lang_route:CMS.cookies') ||
    window.location.pathname === i18next.t('lang_route:CMS.app') ||
    window.location.pathname === i18next.t('lang_route:CMS.privacy_policy') ||
    window.location.pathname === i18next.t('lang_route:CMS.terms') ||
    window.location.pathname === i18next.t('lang_route:giftCard.select') ||
    window.location.pathname === '/' ||
    window.location.pathname.includes('/b') ||
    window.location.pathname === '/404'
    ? "initial"
    : `32.5rem`;
const mainDisplay =
  window.location.pathname === i18next.t('lang_route:registration.trialFlow.checkout') ||
    window.location.pathname === i18next.t('lang_route:mypages.userDetails') ||
    window.location.pathname === i18next.t('lang_route:mypages.getStarted') ||
    window.location.pathname === i18next.t('lang_route:registration.trialFlow.createAccount') ||
    window.location.pathname === i18next.t('lang_route:registration.campaignFlow.createAccount') ||
    window.location.pathname === i18next.t('lang_route:registration.campaignFlow.checkout') ||
    window.location.pathname === i18next.t('lang_route:mypages.reActivationFlow.subscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:mypages.reActivationFlow.checkout') ||
    window.location.pathname === i18next.t('lang_route:mypages.account') ||
    window.location.pathname === i18next.t('lang_route:mypages.myDetails') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionMessage') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionReason') ||
    window.location.pathname === i18next.t('lang_route:mypages.cancelSubscriptionCompleted') ||
    window.location.pathname === i18next.t('lang_route:mypages.orders') ||
    window.location.pathname === i18next.t('lang_route:mypages.receipt') ||
    window.location.pathname === i18next.t('lang_route:mypages.mySubscription') ||
    window.location.pathname === i18next.t('lang_route:mypages.offers') ||
    window.location.pathname === i18next.t('lang_route:mypages.singleOffer') ||
    window.location.pathname === i18next.t('lang_route:mypages.changeMySubscriptionBasic') ||
    window.location.pathname === i18next.t('lang_route:mypages.login') ||
    window.location.pathname === i18next.t('lang_route:CMS.about') ||
    window.location.pathname === i18next.t('lang_route:CMS.cookies') ||
    window.location.pathname === i18next.t('lang_route:CMS.app') ||
    window.location.pathname === i18next.t('lang_route:CMS.privacy_policy') ||
    window.location.pathname === i18next.t('lang_route:CMS.terms') ||
    window.location.pathname === i18next.t('lang_route:giftCard.select') ||
    window.location.pathname === '/' ||
    window.location.pathname === '/404'
    ? "initial"
    : "block";

const SiteWrapper = styled.div`
  height: 100%;
`;

const ContentWrapper = styled.main`
  min-height: 100%;
  margin-bottom: -36rem;
  background-color: ${props =>
    window.location.pathname === "/register/subscription"
      ? props.theme.greenBg
      : window.location.pathname === "/register/email" &&
        window.location.hash === "#steg-2"
        ? props.theme.colorMenuPink
        : window.location.pathname === "/register/email"
          ? props.theme.colorMenuPink
          : window.location.pathname === "/register-campaign/subscription"
            ? props.theme.greenBg
            : props.theme.beige};

  &:after {
    content: "";
    display: ${mainDisplay};
    height: ${scrollMinHeightLarge};
  }
  ${media.medium`
    margin-bottom: -32.5rem;
    &:after {
      height: ${scrollMinHeightMedium};
    }
  `};
  ${props =>
    props.wrapper &&
    css`
      min-height: 100%;
      font-family: "Maison";
      margin-bottom: 0;
      overflow: hidden;
      &:after {
        display: none;
      }
    `};
`;

const Clear = styled.div`
  clear: both !important;
`;

class App extends Component {
  state = {
    popupVisible: false
  };


  alternateMenu = () => {
    if (
      window.location.pathname === "/presentkort/inlosen" ||
      //window.location.pathname === "/presentkort/subscription" ||
      //window.location.pathname ===      ("/presentkort/subscription" && window.location.hash === "#familj") ||
      window.location.pathname === "/presentkort/purchase" ||
      window.location.pathname === "/presentkort/yourorder" ||
      window.location.pathname === "/presentkort/thankyou" ||
      window.location.pathname === "/presentkort/card"
    ) {
      return <NavBarNew setPopUp={this.state.popupVisible} />;
    } else if (getbFlowUrls().includes(window.location.pathname)) {
      // hididng the current top nav bar on new ui
      document.body.style.fontsize = 'initial !important';
      return null;
    } else {

      if (window.location.pathname === "/kom-igang") {
        return <NavBar />;
      } else {
        document.body.style.fontsize = 'initial !important';
        return null
      }
    }
  };

  /*
   **@event: change html font-size depending on newUI and oldUI
   **@return: none
   **@parameter description: none
   **@parameter type: none
   */
  checkNewUIimport() {
    // document.documentElement.style.fontSize = "initial";
    // if (getbFlowUrls().includes(window.location.pathname) || (window.location.pathname.includes('/b') && (window.location.pathname.includes('kampanj') || window.location.pathname.includes('campaign')))) {
    //   // Below condition in above statement is because i18n url has a variable value in the middle of url,
    //   // therefor it can not be tracked like other routes
    //   // (window.location.pathname.includes('/b') && (window.location.pathname.includes('kampanj') || window.location.pathname.includes('campaign')))
    //   document.documentElement.style.fontSize = "initial";
    // } else {
    //   document.documentElement.style.fontSize = `62.5%`;
    // }

    // NOTE: This is temp fix for style issue of old and new system when navigating to old gift card flow
    if (localStorage.getItem('giftCard')) {
      if (window.location.pathname.includes('/presentkort') || window.location.pathname === '/kom-igang') {
        if (localStorage.getItem('giftCard') === 'OUT') {
          window.location.reload()
          localStorage.setItem('giftCard', 'IN')
        }
      } else {
        if (localStorage.getItem('giftCard') === 'IN') {
          window.location.reload()
          localStorage.setItem('giftCard', 'OUT')
        }
      }
    } else {
      localStorage.setItem('giftCard', 'OUT')
    }

  }

  render() {
    // triger html chnage function whenever url change
    this.checkNewUIimport();

    const seotitle = Translation.app.seotitle;
    const seodesc = Translation.app.seodescription;

    let wrapper = false;
    if (
      this.props.Route === "/" ||
      this.props.Route.includes("/app/") ||
      this.props.Route === "/b"
    ) {
      wrapper = true;
    }

    return (
      <SiteWrapper>
        <Helmet>
          <title>{seotitle}</title>
          <meta name="description" content={seodesc} />
          <meta property="og:title" content={seotitle} />
          <meta property="og:description" content={seodesc} />
          <meta property="og:url" content={window.location.href} />
          <meta property="og:image" content={NextoryMetaImg} />
          <meta name="twitter:title" content={seotitle} />
        </Helmet>
        <ContentWrapper wrapper={wrapper}>
          <Overlay />
          {/* <CookieBanner /> */}
          {this.alternateMenu()}
          <Clear />
          <Switch>
            <Route exact path="/switchboard" component={Routes.switchboard} />
            <Route exact path="/thankyoupage" component={Routes.thankyoupage} />
            {/* <Route exact path="/summer" component={HomePageSummer} /> */}
            {/* <Route exact path="/" component={HomePageB} /> */}
            <Route exact path="/b" component={HomePageSummer} />

            {/* Book routes */}
            {/* <Route exact path="/bocker" component={Routes.Books} />
            <Route exact path="/bocker/:slug" component={Routes.TopList} />
            <Route exact path="/e-bocker" component={Routes.Ebooks} />
            <Route exact path="/ljudbocker" component={Routes.Soundbooks} />
            <Route exact path="/kategori/:catslug" component={Routes.Category} />
            <Route path="/kategori/:catslug/:subcatslug" component={Routes.ListAllFromCategory} />
            <Route exact path="/search" component={Routes.SearchView} />
            <Route path="/contributor" component={Routes.SearchView} />
            <Route path="/series" component={Routes.SearchView} /> */}
            {/* Registration routes */}
            {/* <Route exact path="/register/subscription" component={SubscriptionPage} /> */}
            {/* <Route exact path="/register/email" component={RegisterUser} /> */}
            {/* <Route exact path="/register/card" component={Routes.RegisterCardPage} /> */}
            <Route exact path="/bli-medlem" component={RedirectSubscriptionPage} />

            <PrivateRoute exact path="/transresp">
              <Routes.RegisterSuccessPage />
            </PrivateRoute>

            <Route path="/trustly/fel" component={Routes.TrustlyFail} />
            <Route path="/trustly/success" component={Routes.TrustlySuccess} />

            {/* Campaign routes */}
            {/* <Route exact path="/kampanjkod" component={Routes.CampaignPage} />
            <Route exact path="/kampanj" component={RedirectCampaign} />

            <Route exact path="/register-campaign/subscription" component={Routes.CampaignSubscriptionPage} />
            <Route exact path="/register-campaign/card" component={Routes.CampaignCard} /> */}

            {/* Specific/ Custom Campaign routes */}
            <Route path="/sjprio" component={Routes.SjprioRedirect} />
            <Route path="/varmlandstrafik" component={Routes.VarmlandstrafikRedirect} />

            {/* Giftcard routes */}
            <Route exact path="/presentkort" component={Routes.GiftcardRedirect} />
            <Route exact path="/inlosen" component={Routes.GiftcardRedirect} />

            {/* Giftcard New routes */}            <Route exact path="/presentkort/purchase" component={Routes.GiftcardPurchasePage} />
            <Route exact path="/presentkort/yourorder" component={Routes.GiftcardOrderPage} />
            <Route exact path="/presentkort/card" component={Routes.GiftcardPageNew} />
            <Route path="/presentkort/thankyou" component={Routes.GiftcardThankyouPage} />
            <Route exact path="/presentkort/inlosen" component={Routes.GiftcardInlosenPage} />
            <Route exact path="/presentkort/subscription" component={Routes.GiftcardSubscriptionPage} />
            <PrivateRoute exact path="/completed">
              <Routes.RegisterSuccessPage />
            </PrivateRoute>

            {/* Login routes */}
            <Route path="/logga-in" component={Routes.LoginPage} />
            <Route path="/register-card-new" component={Routes.RegisterCard} />
            <Route path="/glomt-losenord" component={Routes.ForgotPassPage} />
            <Route path="/InvalidLinkChangPass" component={Routes.InvalidLink} />
            <Route path="/glomt-losenord-skickat" component={Routes.MailSent} />
            {/* <Route path="/konto/andra-losenord" component={Routes.ChangePassPage} /> */}

            {/* NOTE: Do not move this route below */}
            <Route exact
              path={this.props.t('mypages.resetPassword')}
              render={props => (
                <Routes.resetPassword
                  {...props}
                  bgColor='secondaryTint1'
                  footer='default'
                  header="default"
                />
              )}
            />

            <PrivateRoute exact path="/konto">
              <Routes.MyAccount />
            </PrivateRoute>
            <PrivateRoute exact path="/konto/kophistorik">
              <Routes.Receipt />
            </PrivateRoute>
            <PrivateRoute path="/konto/andra-abonnemang">
              <Routes.ChangeSubscription />
            </PrivateRoute>
            <PrivateRoute exact path="/konto/andra-betalning">
              <Routes.ChangePayment />
            </PrivateRoute>
            <PrivateRoute exact path="/konto/aktivera-abonnemang">
              <Routes.ReactivateSubscription />
            </PrivateRoute>
            <PrivateRoute exact path="/konto/aktivera-betalning">
              <Routes.ReactivatePayment />
            </PrivateRoute>
            <PrivateRoute exact path="/konto/avsluta">
              <Routes.CloseAccount />
            </PrivateRoute>
            <PrivateRoute exact path="/konto/avslutat">
              <Routes.AfterCloseAccount />
            </PrivateRoute>

            {/* Direct activation */}
            <Route path="/showConfirmationFrCampaign" component={Routes.DirectActivation} />

            {/* Static pages routes */}
            {/* <Route path="/om-nextory-ab" component={Routes.AboutPage} /> */}
            {/* <Route path="/medlemsvillkor" component={Routes.TermsPage} /> */}
            {/* <Route path="/integritetspolicy" component={Routes.IntegrityPage} /> */}
            {/* <Route path="/appar" component={Routes.AppPage} /> */}
            <Route exact path="/kom-igang" component={Routes.GetStartedPage} />
            {/* <Route path="/om-cookies" component={Routes.CookiesPage} /> */}
            {/* <Route path="/salja-pa-nextory" component={Routes.SaljaPage} /> */}
            <Route path="/nextory-stipendiet" component={Routes.StipendiumPage} />
            <Route path="/nextory-stipendiet-ansokan" component={Routes.StipendiumPageAnsokning} />
            <Route path="/nextory-stipendiet-allmanna-villkor" component={Routes.StipendiumPageVillkor} />
            <Route path="/press" component={() => (window.location = "https://www.mynewsdesk.com/se/nextory")} />

            {/* App pages */}
            <Route path="/app/medlemsvillkor" component={Routes.TermsPage} />
            <Route path="/app/fragor_svar" component={Routes.FAQPage} />
            <Route path="/app/changesubscription/auth" component={Routes.AccountApp} />

            {/* Silver FAQ pages */}
            {/* <Route exact path="/app/silver_faq" component={Routes.SilverFaqPage} /> */}
            {/* <Route path="/app/silver_faq/varfor-kan-jag-inte-lyssna-på-de-senaste-bockerna-i-silver" component={Routes.link1} />
            <Route path="/app/silver_faq/varfor-ar-vissa-aldre-bocker-inte-tillgangliga-i-silver" component={Routes.link2} />
            <Route path="/app/silver_faq/hur-skiljer-sig-utbudet-i-silver-mot-guld-och-familj" component={Routes.link3} />
            <Route path="/app/silver_faq/hur-ofta-uppdateras-utbudet-i-silver" component={Routes.link4} />
            <Route path="/app/silver_faq/varfor-saknas-en-av-bockerna-i-en-serie-i-silver" component={Routes.link5} />
            <Route path="/app/silver_faq/kan-jag-uppgradera-abonnemanget-tillfalligt" component={Routes.link6} />
            <Route path="/app/silver_faq/vara-olika-abonnemang" component={Routes.link7} /> */}


            {/* ROUTES FOR NEW UI  */}
            <Route
              exact
              path={`${this.props.t('books.book')}/:slug`}
              render={props => (
                <Routes.Book
                  {...props}
                  slug={props.match.params.slug}
                  bgColor="secondaryTint1"
                  footer='default'
                  header="default"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('books.search')}
              render={props => (
                <Routes.Search
                  {...props}
                  footer='default'
                  header="default"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('books.categoriesEbooks')}
              render={props => (
                <Routes.Categories
                  {...props}
                  heading={i18next.t('lang_books:bookListHeader.e_heading')}
                  footer='default'
                  header="default"
                />
              )}
            />
            <Route
              exact
              path={`${this.props.t('books.categoriesEbooks')}/:slug`}
              render={props => (
                <Routes.Categories
                  {...props}
                  slug={props.match.params.slug}
                  footer='default'
                  header="default"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('books.categoriesAudioBooks')}
              render={props => (
                <Routes.Categories
                  {...props}
                  heading={i18next.t('lang_books:bookListHeader.audio_heading')}
                  footer='default'
                  header="default"
                />
              )}
            />
            <Route
              exact
              path={`${this.props.t('books.categoriesAudioBooks')}/:slug`}
              render={props => (
                <Routes.Categories
                  {...props}
                  slug={props.match.params.slug}
                  footer='default'
                  header="default"
                />
              )}
            />
            <Route
              path={this.props.t('registration.trialFlow.subscriptionBasic')}
              render={props => (
                <Routes.TrialSubscription
                  {...props}
                  bgColor="secondaryTint1"
                  header="clean"
                  flow="trial"
                  footer="none"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('registration.trialFlow.createAccount')}
              render={props => (
                <Routes.TrialRegistration
                  {...props}
                  bgColor="secondaryTint1"
                  header="clean"
                  footer="none"
                  flow="trial"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('registration.trialFlow.checkout')}
              render={props => (
                <Routes.TrialPayment
                  {...props}
                  bgColor="secondaryTint1"
                  header="clean"
                  footer="none"
                  flow="trial"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('mypages.userDetails')}
              render={props => (
                <Routes.UserDetails
                  {...props}
                  bgColor="secondaryTint1"
                  header="clean"
                  footer="none"
                />
              )}
            />
            <Route
              exact
              path={this.props.t('mypages.getStarted')}
              render={props => (
                <Routes.GetStarted
                  {...props}
                  bgColor={GetFlowColor()}
                  header="clean"
                  footer="none"
                />
              )}
            />

            <Route exact path={this.props.t('registration.campaignFlow.createAccount')}
              render={props => (
                <Routes.CampaignRegistration
                  {...props}
                  bgColor="secondaryTint2"
                  header="none"
                  footer="none"
                  flow="campaign"
                  type="default"
                />
              )}
            />

            <Route exact path={this.props.t('registration.campaignFlow.subscriptionBasic')}
              render={props => (
                <Routes.TrialSubscription
                  {...props}
                  bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="campaign"
                />
              )}
            />

            <Route exact
              path={this.props.t('registration.campaignFlow.checkout')}
              render={props => (
                <Routes.TrialPayment
                  {...props}
                  bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="campaign"
                />
              )}
            />

            <Route exact
              path={this.props.t('registration.campaignFlow.checkoutOptions')}
              render={props => (
                <Routes.TrialPayment
                  {...props}
                  bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="campaign"
                />
              )}
            />

            <Route exact
              path={this.props.t('mypages.reActivationFlow.subscriptionBasic')}
              render={props => (
                <Routes.TrialSubscription
                  {...props}
                  bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="re-activate"
                />
              )}
            />

            <Route exact
              path={this.props.t('giftCard.select')}
              render={props => (
                <Routes.GiftCardOptions
                  {...props}
                  // bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="re-activate"
                />
              )}
            />

            <Route exact path={this.props.t('registration.campaignFlow.customCampaign')}
              render={props => (
                <Routes.CampaignRegistration
                  {...props}
                  bgColor="secondaryTint2"
                  header="none"
                  footer="none"
                  flow="campaign"
                  type="default"
                />
              )}
            />

            <Route exact
              path={this.props.t('mypages.reActivationFlow.checkout')}
              render={props => (
                <Routes.TrialPayment
                  {...props}
                  bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="re-activate"
                />
              )}
            />

            <Route exact path={`${this.props.t('registration.campaignFlow.customCampaignGiven')}`}
              render={props => (
                <Routes.CampaignRegistration
                  {...props}
                  bgColor="secondaryTint2"
                  header="none"
                  footer="none"
                  flow="campaign"
                  type="custom"
                />
              )}
            />

            <Route exact
              path={this.props.t('mypages.reActivationFlow.checkoutOptions')}
              render={props => (
                <Routes.TrialPayment
                  {...props}
                  bgColor="secondaryTint2"
                  header="clean"
                  footer="none"
                  flow="re-activate"
                />
              )}
            />

            {/* This hsould be below new ui route for new ui to function properly */}
            <Route path="/kampanj/:value" component={Routes.CustomCampaignPage} />

            <PrivateRouteNew exact path={this.props.t('mypages.account')} component={NavigationMenu}
              bgColor='white'
              footer='none'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.myDetails')} component={MyDetails}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.cancelSubscriptionMessage')} component={CancelMessage}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.cancelSubscriptionReason')} component={CancelReason}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.cancelSubscriptionCompleted')} component={CancelCompleted}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.mySubscription')} component={MySubscription}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.orders')} component={MyOrders}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.receipt')} component={MyReceipt}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.offers')} component={Offers}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.singleOffer')} component={Offers}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.paymentDetails')} component={PaymentDetails}
              bgColor='smoke'
              footer='default'
              header='default'
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.checkoutOptions')} component={Routes.paymentDetailsOptions}
              bgColor='smoke'
              footer='default'
              header='default'
              flow="change-payment"
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.changeMySubscriptionBasic')} component={Routes.mySubscriptionBasic}
              bgColor='smoke'
              footer='default'
              header='default'
              flow="change-payment"
              auth={this.props.isLoggedIn}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.subscriptionChangedMessage')} component={Routes.subscriptionChangedMessage}
              bgColor='smoke'
              footer='default'
              header='default'
              flow="change-payment"
              auth={this.props.isLoggedIn}
            />

            <Route exact path={`${this.props.t('paymentresponse')}`}
              render={props => (
                <Routes.PaymentResponse
                  {...props}
                  bgColor="secondaryTint2"
                  header="none"
                  footer="none"
                  flow="paymentresponse"
                  type="custom"
                />
              )}
            />

            <PrivateRouteNew exact path={this.props.t('mypages.changeMySubscriptionFamily')} component={Routes.mySubscriptionFamily}
              bgColor='smoke'
              footer='default'
              header='default'
              flow="change-payment"
              auth={this.props.isLoggedIn}
            />

            <Route exact
              path={this.props.t('mypages.login')}
              render={props => (
                !this.props.isLoggedIn ?
                  <Routes.login
                    {...props}
                    bgColor='secondaryTint1'
                    footer='default'
                    header="default"
                  /> : <Redirect to={this.props.t('mypages.account')} />
              )}
            />

            <Route exact
              path={this.props.t('mypages.forgotPasswordEmail')}
              render={props => (
                <Routes.forgotPasswordEmail
                  {...props}
                  bgColor='secondaryTint1'
                  footer='default'
                  header="default"
                />
              )}
            />

            <Route exact
              path={this.props.t('mypages.forgotPasswordMessage')}
              render={props => (
                <Routes.forgotPasswordMessage
                  {...props}
                  bgColor='secondaryTint1'
                  footer='default'
                  header="default"
                />
              )}
            />

            <Route exact path={`${this.props.t('mypages.checkout')}`}
              render={props => (
                <Routes.TrialPayment
                  {...props}
                  bgColor="smoke"
                  header="default"
                  footer="default"
                  flow="change-payment"
                />
              )}
            />

            <Route exact path={`${this.props.t('mypages.paymentSuccess')}`}
              render={props => (
                <Routes.paymentDetailsCompleted
                  {...props}
                  bgColor="smoke"
                  header="default"
                  footer="default"
                  flow="change-payment"
                />
              )}
            />

            <Route exact path="/"
              render={props => (
                <Routes.newHomePage
                  {...props}
                  bgColor="secondaryTint1"
                  header="default"
                  footer="default"
                />
              )}
            />

            <Route exact path={this.props.t('silver_FAQ')}
              render={props => (
                <SilverFaqQuestions
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='none'
                  header='none'
                />
              )}
            />

            <Route exact path={`${this.props.t('silver_FAQ')}/:slug`}
              render={props => (
                <SilverFaqAnswer
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='none'
                  header='none'
                />
              )}
            />

            <Route exact path={`${this.props.t('CMS.about')}`}
              render={props => (
                <CMSPage
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='default'
                  header='default'
                />
              )}
            />

            <Route exact path={`${this.props.t('CMS.cookies')}`}
              render={props => (
                <CMSPage
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='default'
                  header='default'
                />
              )}
            />

            <Route exact path={`${this.props.t('CMS.app')}`}
              render={props => (
                <CMSPage
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='default'
                  header='default'
                />
              )}
            />

            <Route exact path={`${this.props.t('CMS.privacy_policy')}`}
              render={props => (
                <CMSPage
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='default'
                  header='default'
                />
              )}
            />

            <Route exact path={`${this.props.t('CMS.terms')}`}
              render={props => (
                <CMSPage
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='default'
                  header='default'
                />
              )}
            />

            <Route exact path={`${this.props.t('CMS.sell')}`}
              render={props => (
                <CMSPage
                  {...props}
                  bgColor='SecondaryTint1'
                  footer='default'
                  header='default'
                />
              )}
            />

            <Route exact path="/bok" component={Routes.SingleBook} />

            {/* END OF ROUTES FOR NEW UI  */}

            {/* 404 */}
            <Route path='/404' component={PageNotFound}
              bgColor="smoke"
              header="none"
              footer="default"
              flow="404" />
            <Redirect from='*' to='/404' />
          </Switch>
        </ContentWrapper>
        <Footer />
      </SiteWrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    Route: state.route.location.pathname,
    isLoggedIn: state.userDetails.isLoggedIn
  };
}

export default withNamespaces(['lang_route'])(withRouter(connect(mapStateToProps)(App)));
