/*
 * Action creators
 */

import {
  MENU_TOGGLE,
  MENU_CLOSE,
  DROPDOWN_TYPE_TOGGLE,
  DROPDOWN_CAT_TOGGLE,
  DROPDOWN_SORT_TOGGLE,
  DROPDOWN_CLOSE,
  STICKY_CTA,
} from '../constants/app-constants';

// app actions
export const toggleMenu = () => ({ type: MENU_TOGGLE });
export const closeMenu = () => ({ type: MENU_CLOSE });
export const openDropdownType = () => ({ type: DROPDOWN_TYPE_TOGGLE });
export const openDropdownCat = () => ({ type: DROPDOWN_CAT_TOGGLE });
export const openDropdownSort = () => ({ type: DROPDOWN_SORT_TOGGLE });
export const closeDropdown = () => ({ type: DROPDOWN_CLOSE });
export const toggleStickyCta = (bool) => ({ type: STICKY_CTA, bool });