import React from 'react';
import PropTypes from 'prop-types';
import { apiUrl, internationalization_a, internationalization_b, fetchAuth } from 'containers/App/api';
import { connect } from 'react-redux';
import { parseJwt } from 'utils/helpFunctions';
import Cookies from 'universal-cookie';
import { Base64 } from 'js-base64';
import {
    notifyCardUpdated,
    notifySubChanged,
    notifySubActivated,
    notifyCampaignCode,
    notifycancelDowngrade,
} from 'containers/App/notifications';

import { refreshAuth } from '../../containers/Views/Account/MyAccount/actions';
import { logoutRequest, refreshStore } from '../../containers/Views/Account/MyAccount/actions';
import AfterUncleaned from '../Views/Account/ChangeSubscription/ThankyouPage';
import MyAccount from '../Views/Account/MyAccount';
import { getSubscriptionIdByName } from './common';

class switchboard extends React.PureComponent {
    static propTypes = {
        logoutRequest: PropTypes.func,
        userData: PropTypes.object,
        routeSearch: PropTypes.string,
    };

    state = {
        loading: false,
        uncancelled: false,
    };

    componentDidMount() {
        this.notifications();
    }

    clickLogout = () => {
        this.props.logoutRequest(this.props.userData.authkey);
    };

    notifications = () => {
        if (this.props.routeSearch.includes('updated-card')) {
            notifyCardUpdated();
        } else if (this.props.routeSearch.includes('subscription-changed')) {
            notifySubChanged();
        } else if (this.props.routeSearch.includes('subscription-activated')) {
            notifySubActivated();
        } else if (this.props.routeSearch.includes('campaigncode-added')) {
            notifyCampaignCode();
        }
    };

    startActivateAccount = () => {
        this.checkAuthActivate();
        this.setState({ loading: true });
    };
    checkAuthActivate = () => {
        const data = JSON.parse(Base64.decode(new Cookies().get('user')));
        const jwt = parseJwt(data.authkey);
        const current_time = Date.now() / 1000;
        if (jwt.exp < current_time) {
            return this.props.refreshAuth();
        } else {
            return this.activateAccount();
        }
    };

    activateAccount = async () => {
        try {
            let storedSubscription = localStorage.getItem('SwitchBoardSetSub')
            let subscriptionName = !storedSubscription ? this.props.userData.subscriptionid : storedSubscription;
            let subscriptionid = getSubscriptionIdByName(subscriptionName)

            const response = await fetch(
                `${apiUrl}usersignup?email=${this.props.userData.UserDetails.email}&confirmemail=${
                this.props.userData.UserDetails.email
                }&password=${this.props.userData.p}&subscriptionid=${
                subscriptionid
                }&serverdetails=127.0.0.1&signuptype=PURCHASE_SUBSCRIPTION${internationalization_b}`,
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Cache-Control': 'no-cache',
                        'nx-at': this.props.userData.authkey,
                        Authorization: fetchAuth,
                    },
                    credentials: 'same-origin',
                    method: 'POST',
                    body: JSON.stringify(data), // eslint-disable-line
                }
            );
            const data = await response.json();
            if (data.status === 200) {
                this.props.refreshStore(this.props.userData.authkey);
                //notifySubActivated();
                this.setState({ loading: false, uncancelled: true, });

            }
        } catch (error) {
            console.log(error);
        }
        return console.log('success');
    };

    clickCancelDowngrade = () => {
        this.checkAuthCancel();
        this.setState({ loading: true });
    };
    checkAuthCancel = () => {
        const data = JSON.parse(Base64.decode(new Cookies().get('user')));
        const jwt = parseJwt(data.authkey);
        const current_time = Date.now() / 1000;
        if (jwt.exp < current_time) {
            return this.props.refreshAuth();
        } else {
            return this.cancelDowngrade();
        }
    };
    cancelDowngrade = async () => {
        try {
            const response = await fetch(`${apiUrl}cancelsubscriptiondowngrade${internationalization_a}`, {
                headers: {
                    'Cache-Control': 'no-cache',
                    'nx-at': this.props.userData.authkey,
                    Authorization: fetchAuth,
                },
                credentials: 'same-origin',
            });
            const data = await response.json();
            if (data.status === 200) {
                this.props.refreshStore(this.props.userData.authkey);
                notifycancelDowngrade();
                this.setState({ loading: false });
            }
            return console.log('success');
        } catch (error) {
            return error;
        }
    };

    //************ Change the subscriptions ***********************
    render() {

        try {
            //Not only this are valid params for change the subscriptions
            //Silver, Guld, Familj

            if (!this.props.userData.UserDetails.currentState) {

                if (window.location.search) {
                    var subscription = window.location.search.split('=')[1];
                    localStorage.setItem('SwitchBoardSetSub', subscription);
                    window.location.replace('register/card');
                } else {
                    window.location.replace('register/card');
                }

            } else {

                switch (this.props.userData.UserDetails.currentState) {
                    case "FREE_TRIAL":
                        window.location.replace('/konto');
                        break;

                    case "NONMEMBER":

                        if (window.location.search) {
                            subscription = window.location.search.split('=')[1];
                            localStorage.setItem('SwitchBoardSetSub', subscription);
                            window.location.replace('konto/aktivera-betalning');
                        } else {
                            window.location.replace('konto/aktivera-abonnemang');
                        }
                        break;

                    case "MEMBER_PAYING":

                        //********** Canceled member **********************************
                        if (this.props.userData.UserDetails.userEvent === "CANCELLED") {
                            if (window.location.search) {
                                subscription = window.location.search.split('=')[1];
                                localStorage.setItem('SwitchBoardSetSub', subscription);
                                this.startActivateAccount();
                            } else {
                                this.startActivateAccount();
                            }

                            //********** Fully paying member **********************************
                        } else {
                            window.location.replace('/konto');
                        }
                        break;

                    default:
                        break;
                }
            }
        }
        catch (err) {
            console.log(err);
            //window.location.replace('register/card');
        }

        if (this.state.uncancelled === false) {
            return (<MyAccount />);
        } else {
            return (<AfterUncleaned />);
        }
    }
}

function mapStateToProps(state) {
    return {
        userData: state.account.userData,
        routeSearch: state.route.location.search,
        sendingData: state.account.currentlySending,
    };
}

export default connect(mapStateToProps, { logoutRequest, refreshStore, refreshAuth })(switchboard);
