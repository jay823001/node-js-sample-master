import Translation from 'translation/nextory-web-se';

const CONSOLE_LOGS = false // true/false ==> Set to true to see console.logs. and vise versa

let subscriptionid;
let showLogs = CONSOLE_LOGS // Do not change this

export const SilverPlus = () => { return Translation.app.common.silverplus } // Destined to be Legacy package soon as of 30/09/2018
export const FamilyPlus = () => { return Translation.app.common.familyplus } // Destined to be Legacy package soon as of 30/09/2018
export const Silver     = () => { return Translation.app.common.silver }
export const Gold       = () => { return Translation.app.common.gold }
export const Family     = () => { return Translation.app.common.family }
export const Family2    = () => { return Translation.app.common.family2	}
export const Family3    = () => { return Translation.app.common.family3	}
export const Family4    = () => { return Translation.app.common.family4	}

export const getSubscriptionIdByName = (subscriptionname) => {
    if (SilverPlus() === subscriptionname) { // Destined to be Legacy package soon as of 30/09/2018
        subscriptionid = 5;
    } else if (FamilyPlus() === subscriptionname) { // Destined to be Legacy package soon as of 30/09/2018
        subscriptionid = 7;
    }
    // New Packages as of 30/09/2018
    else if (Silver() === subscriptionname ) {
        subscriptionid = 8;
    } else if (Gold() === subscriptionname) {
        subscriptionid = 6;
    } else if (Family2() === subscriptionname) {
        subscriptionid = 9;
    } else if (Family3() === subscriptionname) {
        subscriptionid = 10;
    } else if (Family4() === subscriptionname) {
        subscriptionid = 11;
    }    
    showLogs = showLogs ? console.log(subscriptionname + ' -- name >> id -- ' + subscriptionid) : null
    return subscriptionid
}

export const getSubscriptionNameById = (subscriptionid) => {
    let subscriptionname = '';
    if (subscriptionname === SilverPlus() ) { // Destined to be Legacy package soon as of 30/09/2018
        subscriptionid = 5;
    } else if (subscriptionname === FamilyPlus() ) { // Destined to be Legacy package soon as of 30/09/2018
        subscriptionid = 7;
    }
    // New Packages as of 30/09/2018
    if (subscriptionname === Silver() ) {
        subscriptionname = 8;
    } else if (subscriptionid === Gold() ) {
        subscriptionname = 6;
    } else if (subscriptionid === Family2() ) {
        subscriptionname = 9;
    } else if (subscriptionid === Family3() ) {
        subscriptionname = 10;
    } else if (subscriptionid === Family4() ) {
        subscriptionname = 11;
    }    
    showLogs = showLogs ? console.log(subscriptionid + ' -- id >> name -- ' + subscriptionname) : null
    return subscriptionname
}

/*
 * Check if the given subscription is a family package
 * subscription parameter is optional but be thoughtful when using this
 * Use subscription parameter only when you want to check the users existing subscription
 * Do not use it when you want to check dynamic results
 */
export const isFamilyPackage = (subscription) => {
    subscription = subscription ? subscription : localStorage.getItem('sub-state')
    let condition = (subscription === Translation.app.common.silver || subscription === Translation.app.common.gold )
    let result = condition ? false : true
    showLogs = showLogs ? console.log("isFamilyPackage : " + result) : null
    return result
}

export const getMonthOrMonths = (monthNumber) => {
    return monthNumber === 1 ?
      Translation.campaign.subscriptionpage.registerwrapper.campaignmessage.month :
      Translation.campaign.subscriptionpage.registerwrapper.campaignmessage.months;
  }