import loadable from "loadable-components";
import Loading from "newui/components/Loader";
import PageNotFound from "newui/containers/Views/Static/404";

export const RegisterCardPage = loadable(
  () => import("containers/Views/Registration/RegisterCard"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const RegisterSuccessPage = loadable(
  () => import("containers/Views/Registration/RegisterSuccess"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const TrustlyFail = loadable(
  () => import("containers/Views/Registration/Trustly/trustly-fail"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const TrustlySuccess = loadable(
  () => import("containers/Views/Registration/Trustly/trustly-success"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const Ebooks = loadable(() => import("containers/Views/Books/Ebooks"), {
  LoadingComponent: Loading,
  ErrorComponent: PageNotFound
});
export const Books = loadable(() => import("containers/Views/Books/Books"), {
  LoadingComponent: Loading,
  ErrorComponent: PageNotFound
});
export const Soundbooks = loadable(
  () => import("containers/Views/Books/Soundbooks"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const Category = loadable(
  () => import("containers/Views/Books/Category"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const ListAllFromCategory = loadable(
  () => import("containers/Views/Books/Category/ListAll"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const TopList = loadable(
  () => import("containers/Views/Books/Toplist"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const SingleBook = loadable(
  () => import("containers/Views/Books/SingleBook"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const LoginPage = loadable(
  () => import("containers/Views/Account/LoginPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const ForgotPassPage = loadable(
  () => import("containers/Views/Account/ForgotPassPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const InvalidLink = loadable(
  () => import("containers/Views/Account/ForgotPassPage/InvalidLink"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const MailSent = loadable(
  () => import("containers/Views/Account/ForgotPassPage/MailSent"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const ChangePassPage = loadable(
  () => import("containers/Views/Account/ChangePassPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const MyAccount = loadable(
  () => import("containers/Views/Account/MyAccount"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const Receipt = loadable(
  () => import("containers/Views/Account/Receipt"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const ChangeSubscription = loadable(
  () => import("containers/Views/Account/ChangeSubscription"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const AccountApp = loadable(
  () => import("containers/Views/Account/FromApp"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const ReactivateSubscription = loadable(
  () => import("containers/Views/Account/ReactivateSubscription"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const ReactivatePayment = loadable(
  () => import("containers/Views/Account/ReactivatePayment"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const ChangePayment = loadable(
  () => import("containers/Views/Account/ChangePayment"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const CloseAccount = loadable(
  () => import("containers/Views/Account/CloseAccount"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const AfterCloseAccount = loadable(
  () => import("containers/Views/Account/CloseAccount/AfterClose"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const CampaignPage = loadable(
  () => import("containers/Views/Campaign"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const CustomCampaignPage = loadable(
  () => import("containers/Views/Campaign/CustomCampaign"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const SjprioRedirect = loadable(
  () => import("containers/Views/Campaign/Sjprio.Redirect"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const VarmlandstrafikRedirect = loadable(
  () => import("containers/Views/Campaign/Varmlandstrafik.Redirect"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const CampaignSubscriptionPage = loadable(
  () => import("containers/Views/Campaign/SubscriptionPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const CampaignCard = loadable(
  () => import("containers/Views/Campaign/RegisterCard"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
//Giftcard new routes
export const GiftcardOrderPage = loadable(
  () =>
    import("containers/Views/GiftcardNew/GiftcardPurchase/GiftcardOrderPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GiftcardPageNew = loadable(
  () => import("containers/Views/GiftcardNew/GiftcardPurchase/GiftcardPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GiftcardPurchasePage = loadable(
  () =>
    import("containers/Views/GiftcardNew/GiftcardPurchase/GiftcardPurchasePage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GiftcardThankyouPage = loadable(
  () =>
    import("containers/Views/GiftcardNew/GiftcardPurchase/GiftcardPage/thankyou"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GiftcardInlosenPage = loadable(
  () =>
    import("containers/Views/GiftcardNew/GiftcardRedeem/GiftcardInlosenPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GiftcardSubscriptionPage = loadable(
  () =>
    import("containers/Views/GiftcardNew/GiftcardRedeem/GiftcardSubscriptionPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GiftcardRedirect = loadable(
  () => import("containers/Views/GiftcardNew/GiftcardRedirect"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const DirectActivation = loadable(
  () => import("containers/Views/DirectActivation"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const AboutPage = loadable(
  () => import("containers/Views/Static/AboutPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const TermsPage = loadable(
  () => import("containers/Views/Static/TermsPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const IntegrityPage = loadable(
  () => import("containers/Views/Static/IntegrityPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const CookiesPage = loadable(
  () => import("containers/Views/Static/CookiesPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const AppPage = loadable(
  () => import("containers/Views/Static/AppPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GetStartedPage = loadable(
  () => import("containers/Views/Static/GetStartedPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const SaljaPage = loadable(
  () => import("containers/Views/Static/SaljaPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const StipendiumPage = loadable(
  () => import("containers/Views/Static/StipendiumPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const StipendiumPageAnsokning = loadable(
  () => import("containers/Views/Static/StipendiumPage/ansokning"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const StipendiumPageVillkor = loadable(
  () => import("containers/Views/Static/StipendiumPage/villkor"),
  { LoadingComponent: Loading, ErrorComponent: PageNotFound }
);
export const FAQPage = loadable(
  () => import("containers/Views/Static/FAQPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const SilverFaqPage = loadable(
  () => import("containers/Views/Static/SilverFaqPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link1 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link1"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link2 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link2"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link3 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link3"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link4 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link4"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link5 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link5"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link6 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link6"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const link7 = loadable(
  () => import("containers/Views/Static/SilverFaqPage/Link7"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const SearchView = loadable(() => import("containers/Search"), {
  LoadingComponent: Loading,
  ErrorComponent: PageNotFound
});
export const RegisterCard = loadable(
  () => import("containers/Views/Registration/RegisterCardNew"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const switchboard = loadable(
  () => import("containers/App/switchboard"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const thankyoupage = loadable(
  () => import("containers/Views/Account/ChangeSubscription/ThankyouPage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

//eslint-disable-next-line
{
  /** ROUTES FOR NEW UI */
}

export const Categories = loadable(
  () => import("newui/containers/Views/Books/Categories"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const Search = loadable(
  () => import("newui/containers/Views/Books/Search"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const Book = loadable(
  () => import("newui/containers/Views/Books/Book"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const TrialSubscription = loadable(
  () => import("newui/containers/Views/Common/Subscription"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const TrialRegistration = loadable(
  () => import("newui/containers/Views/Register/TrialFlow"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const TrialPayment = loadable(
  () => import("newui/containers/Views/Common/Checkout"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const UserDetails = loadable(
  () => import("newui/containers/Views/Common/UserDetails"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const GetStarted = loadable(
  () => import("newui/containers/Views/Common/GetStarted"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);
export const CampaignRegistration = loadable(
  () => import("newui/containers/Views/Register/CampaignFlow"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const mySubscriptionBasic = loadable(
  () => import("newui/components/SubscriptionTable"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const mySubscriptionFamily = loadable(
  () => import("newui/components/SubscriptionTable"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const GiftCardOptions = loadable(
  () => import("newui/containers/Views/GiftCard"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const subscriptionChangedMessage = loadable(
  () => import("newui/containers/Views/Account/ChangeSubscription/ChangedMessage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const paymentDetailsOptions = loadable(
  () => import("newui/containers/Views/Common/Checkout/CheckoutOptions"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const login = loadable(
  () => import("newui/containers/Views/Member/Login"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const forgotPasswordEmail = loadable(
  () => import("newui/containers/Views/Member/ForgotPassword/ConfirmEmail"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const forgotPasswordMessage = loadable(
  () => import("newui/containers/Views/Member/ForgotPassword/ConfirmMessage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const resetPassword = loadable(
  () => import("newui/containers/Views/Member/ForgotPassword/Resetpassword"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const PaymentResponse = loadable(
  () => import("newui/containers/Views/Common/Checkout/PaymentResponse"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const paymentDetailsCheckout = loadable(
  () => import("newui/containers/Views/Common/Checkout/PaymentResponse"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const paymentDetailsCompleted = loadable(
  () => import("newui/containers/Views/Account/ChangePayment/PaymentSuccess"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

export const newHomePage = loadable(
  () => import("newui/containers/Views/Static/HomePage"),
  {
    LoadingComponent: Loading,
    ErrorComponent: PageNotFound
  }
);

//eslint-disable-next-line
{
  /** END ROUTES FOR NEW UI */
}
