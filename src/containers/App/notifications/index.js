import { toast, style } from 'react-toastify';
import Translation from 'translation/nextory-web-se';

export const notifyCardUpdated = () => {
  toast.success((Translation.app.notifications.p1), {
    position: toast.POSITION.BOTTOM_CENTER,
    style: style({
      colorSuccess: '#ff3a54',
      width: '380px',
    }),
  });
};

export const notifySubChanged = () => {
  toast.success((Translation.app.notifications.p2), {
    position: toast.POSITION.BOTTOM_CENTER,
    style: style({
      colorSuccess: '#ff3a54',
    }),
  });
};

export const notifycancelDowngrade = () => {
  toast.success((Translation.app.notifications.p3), {
    position: toast.POSITION.BOTTOM_CENTER,
    style: style({
      colorSuccess: '#ff3a54',
      textAlign: 'center',
    }),
  });
};

export const notifySubActivated = () => {
  toast.success((Translation.app.notifications.p4), {
    position: toast.POSITION.BOTTOM_CENTER,
    style: style({
      colorSuccess: '#ff3a54',
      width: '380px',
    }),
  });
};

export const notifyCampaignCode = () => {
  toast.success((Translation.app.notifications.p5), {
    position: toast.POSITION.BOTTOM_CENTER,
    style: style({
      colorSuccess: '#ff3a54',
      width: '260px',
    }),
  });
};
