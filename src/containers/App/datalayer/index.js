import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { gup } from 'utils/helpFunctions';

class DataLayer extends Component {
  static propTypes = {
    Routepath: PropTypes.string,
  };

  componentDidMount() {
    // send originalLocation from session storage
    // set in index.html to dataLayer, do some tries :)
    if (sessionStorage.getItem('originalLoc')) {
      this.timer = setTimeout(() => {
        this.dataLayer(sessionStorage.getItem('originalLoc'));
      }, 100);
    } else {
      this.timer = setTimeout(() => {
        if (sessionStorage.getItem('originalLoc')) {
          this.dataLayer(sessionStorage.getItem('originalLoc'));
        }
      }, 700);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.Route.pathname.includes('/bok/')) {
      if (this.props.Route.pathname !== prevProps.Route.pathname) {
        if (sessionStorage.getItem('originalLoc')) {
          this.dataLayer(sessionStorage.getItem('originalLoc'));
        }
      }
    }
  }

  dataLayer = originalLocation => {
    // if loaded route is /transresp
    if (this.props.Route.pathname.includes('transresp')) {
      const orderid = gup('orderid', this.props.Route.search);
      return window.dataLayer.push({
        event: 'virtualPageview',
        virtualUrl: this.props.Route.pathname,
        orderID: orderid,
        originalLocation: originalLocation,
      });

      // if loaded route is a book
    } else if (this.props.Route.pathname.includes('/bok/')) {
      return window.dataLayer.push({
        event: 'virtualPageview',
        virtualUrl: this.props.Route.pathname,
        originalLocation: originalLocation,
        isbn: this.props.Route.pathname.split('/').pop(),
      });
      // if loaded route is anything else
    } else {
      return window.dataLayer.push({
        event: 'virtualPageview',
        virtualUrl: this.props.Route.pathname,
        originalLocation: originalLocation,
      });
    }
  };

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    return <span />;
  }
}

function mapStateToProps(state) {
  return {
    Route: state.route.location,
  };
}

export default connect(mapStateToProps)(DataLayer);
