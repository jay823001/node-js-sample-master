/*
 * Reducers takes care of our data. Using actions, we can change our
 * application state.
 */

import {
  MENU_TOGGLE,
  MENU_CLOSE,
  DROPDOWN_TYPE_TOGGLE,
  DROPDOWN_SORT_TOGGLE,
  DROPDOWN_CAT_TOGGLE,
  DROPDOWN_CLOSE,
  STICKY_CTA,
} from '../constants/app-constants';

// app reducer
const initialAppState = {
  DropDownType: false,
  DropDownCat: false,
  DropDownSort: false,
  MenuOpen: false,
  StickyCta: false,
};

export function AppReducers(state = initialAppState, { type, bool }) {
  switch (type) {

    case MENU_TOGGLE:
      return { ...state, MenuOpen: !state.MenuOpen };

    case STICKY_CTA:
      return { ...state, StickyCta: bool };

    case MENU_CLOSE:
      return { ...state, MenuOpen: false };

    case DROPDOWN_TYPE_TOGGLE:
      return { ...state, DropDownType: !state.DropDownType, DropDownCat: false, DropDownSort: false };

    case DROPDOWN_CAT_TOGGLE:
      return { ...state, DropDownCat: !state.DropDownCat, DropDownType: false, DropDownSort: false };

    case DROPDOWN_SORT_TOGGLE:
      return { ...state, DropDownSort: !state.DropDownSort, DropDownType: false, DropDownCat: false };

    case DROPDOWN_CLOSE:
      return { ...state, DropDownCat: false, DropDownType: false, DropDownSort: false };

    default:
      return state;
  }
}
