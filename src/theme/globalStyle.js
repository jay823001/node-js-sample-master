/**
 * These are styles that are injected globally throughout the app
 */
import {
  injectGlobal
} from 'styled-components';
// import { getbFlowUrls } from 'newui/utils/helperFunctions'

let htmlFontSize = 'initial';
let bodyFontSize = 'initial';
let bodyFontSizeMedia = 'initial';
let bodyLineHeight = `1.3`;

if (window.location.pathname === "/presentkort/inlosen" ||
window.location.pathname === "/presentkort/subscription" ||
window.location.pathname ===
("/presentkort/subscription" && window.location.hash === "#familj") ||
window.location.pathname === "/presentkort/purchase" ||
window.location.pathname === "/presentkort/yourorder" ||
window.location.pathname === "/presentkort/thankyou" ||
window.location.pathname === "/presentkort/card" ||
window.location.pathname === "/kom-igang" || 
window.location.pathname === "/completed") {
  htmlFontSize = `62.5%`;
  bodyFontSize = `1.3rem`;
  bodyFontSizeMedia = `1.5rem`;
  bodyLineHeight = `1.525`;
}

// New registration flow css TODO: make it blobal after hwole project updated
// let htmlFontSize = `62.5%`;
// let bodyFontSize = `1.3rem`;
// let bodyFontSizeMedia = `1.5rem`;
// let bodyLineHeight = `1.525`;

// let htmlFontSize = 'initial';
// let bodyFontSize = 'initial';
// let bodyFontSizeMedia = 'initial';
// let bodyLineHeight = `1.3`;


// getbFlowUrls().forEach(function (path) {
//   if (window.location.pathname === path || window.location.pathname.includes(path)) {
//     htmlFontSize = 'initial';
//     bodyFontSize = 'initial';
//     bodyFontSizeMedia = 'initial';
//     bodyLineHeight = `1.3`;
//   }
//     // TODO: This has to be removed once all new ui components imported
//     const modifiedURL = (window.location.pathname).replace((window.location.pathname).split('/')[2], ':value')
//     if (modifiedURL === path) {
//       htmlFontSize = 'initial';
//       bodyFontSize = 'initial';
//       bodyFontSizeMedia = 'initial';
//       bodyLineHeight = `1.3`;
//     }
// });

injectGlobal `

  @font-face {
    font-family: 'Maison';
    src: url('/fonts/MaisonNeue-Book.woff') format('woff');
    src: url('/fonts/MaisonNeue-Book.woff') format('woff2');
    font-weight: normal;
    font-style: normal;
  }
  @font-face {
    font-family: 'MaisonDemi';
    src: url('/fonts/MaisonNeue-Demi.woff') format('woff');
    src: url('/fonts/MaisonNeue-Demi.woff') format('woff2');
    font-weight: normal;
    font-style: normal;
  }
  html,
  body,
  #root {
    height: 100%;
  }

  html {
    font-size: ${htmlFontSize};
    font-smooth: always;
    overflow-y: scroll;
    box-sizing: border-box;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
    -moz-font-smoothing: antialiased;
    -webkit-font-smoothing: antialiased;
  }

  body {
    font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    font-weight: 400;
    font-size: ${bodyFontSize};
    line-height: ${bodyLineHeight};
    color: #333333;
    background: white;
    @media (min-width: 768px) {
      font-size: ${bodyFontSizeMedia};
    }
  }

  a {
    text-decoration: none;
    background-color: transparent;
    transition: all .15s ease;
    cursor: pointer;
    &:visited,
    &:hover,
    &:focus,
    &:active {
      text-decoration: none;
      outline: 0;
    }
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    &:after {
      display: block;
      clear: both;
      content: '';
    }
    li {
      margin: 0;
      padding: 0;
    }
  }

  select::-ms-expand {
    display: none;
  }

  #tabs {
    position: relative;
    z-index: 3;
  }

  /* Used for page transitions */

  .SlideUp-appear {
    transform: translateY(1rem);
    opacity: 0;
  }

  .SlideUp-appear.SlideUp-appear-active {
    opacity: 1;
    transform: translateY(0);
    ;
    transition: all 0.5s linear;
  }

  .SlideUp-enter {
    opacity: 0;
    transform: translateY(1rem);
  }

  .SlideUp-enter.SlideUp-enter-active {
    opacity: 1;
    transform: translateY(0);
    transition: all 0.2s linear 0.4s;
  }

  .SlideUp-leave {
    opacity: 1.0;
    transform: translateY(0);
  }

  .SlideUp-leave.SlideUp-leave-active {
    opacity: 0;
    position: absolute;
    width: 100%;
    transform: translateY(-1rem);
    transition: all 0.2s linear;
  }

  /* Used for page transitions */

  .FadeIn-appear {
    opacity: 0;
  }

  .FadeIn-appear.FadeIn-appear-active {
    opacity: 1;
    transition: all 0.2s linear;
  }

  .FadeIn-enter {
    opacity: 0;
  }

  .FadeIn-enter.FadeIn-enter-active {
    opacity: 1;
    transition: all 0.2s linear 0.2s;
  }

  .FadeIn-leave {
    opacity: 1.0;
  }

  .FadeIn-leave.FadeIn-leave-active {
    opacity: 0;
    position: absolute;
    width: 100%;
    transition: all 0.2s linear;
  }

  /* Slick */

  .slick-list,
  .slick-slider,
  .slick-track {
    position: relative;
    display: block;
  }

  .slick-loading .slick-slide,
  .slick-loading .slick-track {
    visibility: hidden;
  }

  .slick-slider {
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
    touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
  }

  .slick-list {
    overflow: hidden;
    margin: 0;
    padding: 0;
  }

  .slick-list:focus {
    outline: 0;
  }

  .slick-list.dragging {
    cursor: pointer;
    cursor: hand;
  }

  .slick-slider .slick-list,
  .slick-slider .slick-track {
    -webkit-transform: translate3d(0, 0, 0);
    -moz-transform: translate3d(0, 0, 0);
    -ms-transform: translate3d(0, 0, 0);
    -o-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  .slick-track {
    top: 0;
    left: 0;
  }

  .slick-track:after,
  .slick-track:before {
    display: table;
    content: "";
  }

  .slick-track:after {
    clear: both;
  }

  .slick-slide {
    display: none;
    float: left;
    height: 100%;
    min-height: 1px;
  }

  [dir="rtl"] .slick-slide {
    float: right;
  }

  .slick-slide img {
    display: block;
  }

  .slick-slide.slick-loading img {
    display: none;
  }

  .slick-slide.dragging img {
    pointer-events: none;
  }

  .slick-initialized .slick-slide {
    display: block;
  }

  .slick-vertical .slick-slide {
    display: block;
    height: auto;
    border: 1px solid transparent;
  }

  .slick-arrow.slick-hidden {
    display: none;
  }

  .slick-dots,
  .slick-next,
  .slick-prev {
    position: absolute;
    display: block;
    padding: 0;
  }

  .slick-next:before,
  .slick-prev:before {
    display: none;
  }

  .slick-next,
  .slick-prev {
    position: absolute;
    top: 50%;
    width: 44px;
    height: 44px;
    border: none;
    border-radius: 50%;
    background-color: rgba(255, 255, 255, .5);
    cursor: pointer;
    transform: translateY(-50%);
  }

  .slick-next:focus,
  .slick-next:hover,
  .slick-prev:focus,
  .slick-prev:hover {
    color: transparent;
    outline: 0;
    background-color: rgba(255, 255, 255, .9);
  }

  .slick-prev {
    left: -25px;
  }

  [dir="rtl"] .slick-prev {
    right: -25px;
    left: auto;
  }

  .slick-next {
    right: -25px;
  }

  [dir="rtl"] .slick-next {
    right: auto;
    left: -25px;
  }

  .slick-dotted.slick-slider {
    margin-bottom: 30px;
  }

  .slick-dots {
    bottom: -25px;
    width: 100%;
    margin: 0;
    list-style: none;
    text-align: center;
  }

  .slick-dots li {
    position: relative;
    display: inline-block;
    width: 20px;
    height: 20px;
    margin: 0 5px;
    padding: 0;
    cursor: pointer;
  }

  .slick-dots li button {
    font-size: 0;
    line-height: 0;
    display: block;
    width: 20px;
    height: 20px;
    padding: 5px;
    cursor: pointer;
    color: transparent;
    border: 0;
    outline: 0;
    background: 0 0;
  }

  .slick-dots li button:focus,
  .slick-dots li button:hover {
    outline: 0;
  }

  .slick-dots li button:focus:before,
  .slick-dots li button:hover:before {
    opacity: 1;
  }

  .slick-dots li button:before {
    font-size: 6px;
    line-height: 20px;
    position: absolute;
    top: 0;
    left: 0;
    width: 20px;
    height: 20px;
    content: "â€¢";
    text-align: center;
    opacity: 0.25;
    color: #000;
  }

  .slick-dots li.slick-active button:before {
    opacity: 0.75;
    color: #000;
  }

  .slick-disabled {
    opacity: 0.6;
  }

  /* fade in bookcovers */

  @keyframes FadeIn {
    0% {
      opacity: 0;
    }
    70% {
      opacity: 1;
    }
    100% {
      opacity: 1;
    }
  }

  .visibletrue li img {
    animation: FadeIn 2s linear;
    animation-fill-mode: both;
    opacity: 0;
  }

  .visibletrue li:nth-child(1) img:nth-child(1) { animation-delay: .3s }
  .visibletrue li:nth-child(1) img:nth-child(2) { animation-delay: 1.7s }
  .visibletrue li:nth-child(2) img:nth-child(1) { animation-delay: 1.2s }
  .visibletrue li:nth-child(2) img:nth-child(2) { animation-delay: .7s }
  .visibletrue li:nth-child(3) img:nth-child(1) { animation-delay: 1.7s }
  .visibletrue li:nth-child(3) img:nth-child(2) { animation-delay: .3s }
  .visibletrue li:nth-child(4) img:nth-child(1) { animation-delay: 1.7s }
  .visibletrue li:nth-child(4) img:nth-child(2) { animation-delay: 0s }
  .visibletrue li:nth-child(5) img:nth-child(1) { animation-delay: .3s }
  .visibletrue li:nth-child(5) img:nth-child(2) { animation-delay: 0s }
  .visibletrue li:nth-child(6) img:nth-child(1) { animation-delay: 1.2s }
  .visibletrue li:nth-child(6) img:nth-child(2) { animation-delay: 1.7s }
  .visibletrue li:nth-child(7) img:nth-child(1) { animation-delay: .7s }
  .visibletrue li:nth-child(7) img:nth-child(2) { animation-delay: .3s }
  .visibletrue li:nth-child(8) img:nth-child(1) { animation-delay: 0s }
  .visibletrue li:nth-child(8) img:nth-child(2) { animation-delay: 1.7s }

`;