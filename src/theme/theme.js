/**
 * Below are global variables that can be used
 * in the apps styled components
 * Usage: color: ${props => props.theme.colorWhite}
 */
export const theme = {
  colorWhite: '#f3f3f3',
  colorMenu: '#f1f1f1',
  colorMenuPink: '#f6edec',
  colorLightGrey: '#e1e1e1',
  colorGrey: '#bdbdbd',
  colorDarkGrey: '#696969',
  colorBlack: '#333333',
  colorLightBlue: '#66afe9',
  colorBlue: '#2226db',
  colorDarkBlue: '#03256c',
  colorHoverBlue: '#373ae0',
  colorGreen: '#07a71c',
  colorRed: '#fb1818',

  blue: '#363bec',
  bluehover: '#4E53F8',
  beige: '#f6f5f1',
  black: '#333333',
  black2:'#444444',
  black3:'#555555',
  grey: '#cccccc',
  darkgrey: '#a9a9a9',
  teal: '#e1f0ed',

  summerWhite: '#eaf0e2',
  summerDarkWhite: '#f3f3e3',
  summerWhiteLight: '#ffffff',
  summerPink: '#f6edec',

  seccondGrey: '#555555',
  buttonSecconBlue: '#353bec',
  greenBg: '#eaf0e2',
  blueBg: '#eaf3f5',
  themeWhite: '#ffffff',
  tableFont: '#585858',

  //Other commons
  navBoxShadow: '0 0 3px rgba(0, 0, 0, 0.2)',
  noBoxShadow: 'initial',
};

//Usage Example
//${props => props.theme.colorName};
