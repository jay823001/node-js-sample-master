import { Base64 } from 'js-base64';

/* eslint-disable */
export function fancyTimeFormat(time) {
  // Hours, minutes and seconds
  const hrs = ~~(time / 3600);
  const mins = ~~((time % 3600) / 60);
  const secs = time % 60;

  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = '';

  if (hrs > 0) {
    ret += '' + hrs + 'h ' + (mins < 10 ? '0' : '');
  }

  ret += '' + mins + 'min ' + (secs < 10 ? '0' : '');
  ret += '' + secs + 's';

  return ret;
}

export function boldString(str, find) {
  const re = new RegExp(find, 'g');
  return str.replace(re, '<b>' + find + '</b>');
}

export function gup(name, url) {
  if (!url) url = location.href;
  name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
  const regexS = '[\\?&]' + name + '=([^&#]*)';
  const regex = new RegExp(regexS);
  const results = regex.exec(url);
  return results == null ? null : results[1];
}


/* scroll to element */
export function findPos(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
      do {
          curtop += obj.offsetTop;
      } while (obj = obj.offsetParent);
  return [curtop];
  }
}

// Parse jwt
export function parseJwt(token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(Base64.decode(base64));
};
