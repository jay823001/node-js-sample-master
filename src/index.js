/**
 * Here we wrap our app in
 * Provider for Redux,
 * ThemeProvider for Styled components,
 * ConnectedRouter to handle routing and
 * ScrollToTop that makes the app scroll to top after each route change
 */
import "babel-polyfill";
import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { history, store, persistor } from "./store";
import "sanitize.css/sanitize.css";
import { ThemeProvider } from "styled-components";
import "./theme/globalStyle";
import { theme } from "./theme/theme";
import App from "containers/App";
import ScrollToTop from "utils/scrolltotop";
import './newui/i18n';
// fixtest for thirdpaty redirect styling issues
import './newui/assets/styles/main.scss';
import * as Raven from 'raven-js';

// hot reloading for development
if (module.hot) {
  module.hot.accept();
}

Raven.config('https://1e0251d95a14436cb3776a6ff5318bf9@sentry.io/1338609').install();

// Use only for development purpose
// persistor.purge();

render(
  <PersistGate persistor={persistor}>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <ConnectedRouter history={history}>
          <ScrollToTop>
            <App />
          </ScrollToTop>
        </ConnectedRouter>
      </ThemeProvider>
    </Provider>
  </PersistGate>,
  document.querySelector("#root")
);