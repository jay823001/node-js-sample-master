import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Slider from 'react-slick'

import Arrow from './arrow.svg';


const Left  = styled.button`
  position: absolute;
  z-index: 50;
  top: 50%;
  left: 0;
  width: 4rem;
  height: 4rem;
  border: none;
  border-radius: 50%;
  background-color: rgba(255,255,255,.8);
  cursor: pointer;
  transform: translateY(-50%);
  ${media.medium`
    width: 4.4rem;
    height: 4.4rem;
  `}
  &:after {
    position: absolute;
    margin: auto;
    content: '';
    width: 1.7rem;
    height: 1.7rem;
    left: 0;
    right: 2px;
    z-index: 999;
    bottom: 0;
    top: 0;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 1.7rem 1.7rem;
    background-image: url(${Arrow});
    transform: rotate(180deg);
    ${media.medium`
      width: 2rem;
      height: 2rem;
      background-size: 2rem 2rem;
    `}
  }

`;

const Right  = styled.button`
  position: absolute;
  z-index: 50;
  top: 50%;
  right:0;
  width: 4rem;
  height: 4rem;
  border: none;
  border-radius: 50%;
  background-color: rgba(255,255,255,.8);
  cursor: pointer;
  transform: translateY(-50%);
  ${media.medium`
    width: 4.4rem;
    height: 4.4rem;
  `}
  &:after {
    position: absolute;
    margin: auto;
    content: '';
    width: 1.7rem;
    height: 1.7rem;
    left: 2px;
    right: 0;
    z-index: 999;
    bottom: 0;
    top: 0;
    background-repeat: no-repeat;
    background-position: center;
    background-size: 1.7rem 1.7rem;
    background-image: url(${Arrow});
    ${media.medium`
      width: 2rem;
      height: 2rem;
      background-size: 2rem 2rem;
    `}
  }

`;

class BookSlider extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    var settings = {
      nextArrow: <Right />,
      prevArrow: <Left />,
      initialSlide: 0,
      adaptiveHeight: false,
      infinite: false,
      responsive: [ {
        breakpoint: 450, settings: { slidesToShow: 3, slidesToScroll: 2 } }, { 
        breakpoint: 560, settings: { slidesToShow: 4, slidesToScroll: 3 } }, { 
        breakpoint: 660, settings: { slidesToShow: 5, slidesToScroll: 4 } }, { 
        breakpoint: 767, settings: { slidesToShow: 6, slidesToScroll: 5 } }, { 
        breakpoint: 830, settings: { slidesToShow: 5, slidesToScroll: 4 } }, { 
        breakpoint: 940, settings: { slidesToShow: 6, slidesToScroll: 5 } }, { 
        breakpoint: 100000, settings: { slidesToShow: 7, slidesToScroll: 6 } } ],
    };

    return (
      <Slider {...settings}>
        {this.props.children}
      </Slider>
    );
  }
}

export default BookSlider;
