import styled from 'styled-components';
import media from 'theme/styled-utils';

const SliderWrapper = styled.div`
  max-width: 90rem;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 4rem;
  ${media.medium`
    margin-bottom: 5rem;
  `}
  ${media.large`
    margin-bottom: 6rem;
  `}
`;

export default SliderWrapper;
