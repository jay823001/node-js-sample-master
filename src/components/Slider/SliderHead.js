import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import H3 from 'components/Typography/H3';
import Button from 'components/Buttons';
import Translation from 'translation/nextory-web-se';

const Head = styled.div`
  margin: 0 0 2.0rem 0;
  text-align: left;
  ${media.large`
    margin: 0 0 2.5rem 0;
  `}
`;

const StyledH3 = styled(H3)`
  display: inline-block;
  position: relative;
  top: .6rem;
  margin: 0;
  color: ${props => props.theme.colorDarkGrey};
  max-width: 68%;
  ${media.medium`
    font-size: 2.3rem;
    top: 1.0rem;
    max-width: 80%;
  `}
`;

const ButtonStyled = styled(Button)`
  float:right;
  position: relative;
  bottom: 0.3rem;
`;

class SliderHead extends React.PureComponent {
  static propTypes = {
    heading: PropTypes.string,
    link: PropTypes.string,
  };

  render() {
    return (
      <Head>
        <StyledH3>{this.props.heading}</StyledH3>
        <ButtonStyled small framed to={this.props.link}>{Translation.components.slider.button}</ButtonStyled>
      </Head>
    );
  }
}

export default SliderHead;
