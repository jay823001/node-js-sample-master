import styled from 'styled-components';
import media from 'theme/styled-utils';

const Hero = styled.section`
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 16rem;
  padding: 3rem 1.5rem;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  transform-style: preserve-3d;
  &:after {
    position: absolute;
    z-index: 1;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    content: '';
    background-color: rgba(0,0,0,.2);
  }

  ${media.medium`
    height: 26.5rem;
  `}

`;

export default Hero;
