import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

const HeroText = styled.div`
  position: absolute;
  z-index:10;
  top: 50%;
  right: 0;
  bottom: auto;
  left: 0;
  margin: auto;
  padding: 0 1.5rem;
  color:white;
  transform: translateY(-55%);
  text-align: center;

  ${media.xlarge`
    width: 120rem;
    left: 0rem;
  `}

  ${props => props.centered && css`
		max-width:90rem;
	`}
`;

export default HeroText;
