import styled from 'styled-components';

const Center = styled.div`
  text-align: center;
  a+a {
    margin-top: 1.5rem;
  }
`;

export default Center;
