import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';


const Input = styled.input`
  font-size: 1.6rem;
  line-height: 1.4;
  font-weight: 500;
  width: 100%;
  margin: 0 0 1.7rem;
  padding: 1.2rem 1.0rem;
  border: .1rem solid ${props => props.theme.colorGrey};
  border-radius: .5rem;
  background-color: white;
  appearance: none;

  ${media.medium`
    font-size: 1.4rem;
    line-height: 1.4;
  `}

  &:focus {
    border: 1px solid ${props => props.theme.colorLightBlue} !important;
    outline: none;
  }

  ${props => props.error && css`
    border: 1px solid ${props => props.theme.colorRed} !important;
    &:focus {
      border: 1px solid ${props => props.theme.colorRed} !important;
    }
  `}

  ${props => props.valid && css`
    border: 1px solid ${props => props.theme.colorGreen} !important;
    &:focus {
      border: 1px solid ${props => props.theme.colorGreen} !important;
    }
  `}

  ${props => props.warning && css`
    border: 1px solid ${props => props.theme.colorGrey} !important;
    &:focus {
      border: 1px solid ${props => props.theme.colorGrey} !important;
    }
  `}
  
`;

export default Input;

