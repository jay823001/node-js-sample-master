import styled from 'styled-components';
import media from 'theme/styled-utils';


const RadioList = styled.ul`
  padding: 0;
  margin: 30px 0 15px;
  ${media.medium`
    margin: 50px 0 15px;
  `}
  li {
    color: ${props => props.theme.colorGrey};
    display: block;
    position: relative;
    margin-bottom: 15px;
    ${media.medium`
      width: 33%;
      float: left;
      margin-bottom: 0;
    `}
    input {
      position: absolute;
      visibility: hidden;
      &:checked~span {
        color: ${props => props.theme.colorBlue};
      }
      &:checked~div::before {
        background: ${props => props.theme.colorBlue};
      }
      &:checked~label {
        color: ${props => props.theme.colorBlue};
      }
      &:checked~div {
        border: 3px solid ${props => props.theme.colorBlue};
      }
    }
    span {
      display: block;
      position: relative;
      padding: 0 0 0 27px;
      z-index: 9;
      cursor: pointer;
      -webkit-transition: all .15s linear;
      font-size: 14px;
      line-height: 1.2;
      text-align: left;
      ${media.medium`
        font-size: 17px;
      `}
      strong {
        display: block;
        font-size: 19px;
        ${media.medium`
          font-size: 22px;
        `}
      }
    }
    div {
      display: block;
      position: absolute;
      border: 3px solid ${props => props.theme.colorGrey};
      border-radius: 100%;
      height: 20px;
      width: 20px;
      top: 10px;
      left: 0;
      z-index: 5;
      transition: border .25s linear;
      -webkit-transition: border .25s linear;
      &:before {
        display: block;
        position: absolute;
        content: '';
        border-radius: 100%;
        height: 8px;
        width: 8px;
        top: 3px;
        left: 3px;
        margin: auto;
        transition: background 0.15s linear;
        -webkit-transition: background 0.15s linear;
      }
      ${media.medium`
        top: 12px;
      `}
    }
  }
`;

export default RadioList;
