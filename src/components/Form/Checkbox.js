import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';

import Input from 'components/Form/Input';
import Label from 'components/Form/Label';
import erroricon from './exclamation.svg';

const InputWrap = styled.div`
  max-width: 100%;
  margin: 1rem 0 2.5rem 0;
  position: relative;
  top: 0;
  left: 4.2rem;
  display: inline-block;
  cursor: pointer;
  pointer-events: initial;
  line-height: 1.2;
  input:checked ~ small {
    background-color: ${props => props.theme.blue};
  }
  input:checked ~ small:after {
    display: block;
  }
`;

const LabelStyl = Label.extend`
  display: inline-block;
`;

const InputStyl = Input.extend`
  position: absolute;
  width: auto;
  margin: 0;
  opacity: 0;
  appearance: checkbox;
`;

const Span = styled.small`
  position: absolute;
  top: 0.5rem;
  left: -3.9rem;
  width: 2.3rem;
  height: 2.3rem;
  background-color: ${props => props.theme.colorGrey};
  border-radius: 0.3rem;
  cursor: pointer;
  &:after {
    position: absolute;
    top: 0.3rem;
    left: 0.7rem;
    display: none;
    width: 0.8rem;
    height: 1.3rem;
    content: '';
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    border: solid white;
    border-width: 0 0.3rem 0.3rem 0;
  }
`;

const Error = styled.span`
  font-size: 1.2rem;
  position: absolute;
  bottom: 0;
  left: 0;
  color: ${props => props.theme.colorRed};
`;

const Icon = styled.img`
  position: absolute;
  top: -1.9rem;
  right: 0.8rem;
  width: 1.7rem;
  height: 1.7rem;
  ${media.medium`
      right: -1.9rem;
    `};
`;

const Errorcontainer = styled.div`
  position: relative;
`;

export const renderCheck = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <InputWrap>
      <LabelStyl>
        {label}
        <InputStyl {...input} type={type} />
        <Span />
      </LabelStyl>
    </InputWrap>
    <Errorcontainer>
      {touched && (error && <Error>{error}</Error>)}
      {touched && (error && <Icon src={erroricon} alt="errormark" />)}
    </Errorcontainer>
  </div>
);

renderCheck.propTypes = {
  input: PropTypes.object,
  label: PropTypes.object,
  meta: PropTypes.object,
  type: PropTypes.string,
};
