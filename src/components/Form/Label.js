import styled from 'styled-components';

const Label = styled.label`
  font-size: 1.4rem;
  position: relative;
  display: block;
  text-align: left;
  color: ${props => props.theme.colorDarkGrey};
`;

export default Label;
