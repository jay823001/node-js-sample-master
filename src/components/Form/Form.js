import styled from 'styled-components';

const Form = styled.form`
  max-width: 100%;
  margin: auto auto .7rem auto;
`;

export default Form;

