import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Input from 'components/Form/Input';
import Label from 'components/Form/Label';
import checkicon from './checked.svg';
import erroricon from './exclamation.svg';

const InputWrap = styled.div`
  position: relative;
`;

const Error = styled.span`
  font-size: 1.2rem;
  position: absolute;
  bottom: 0;
  left: 0;
  color: ${props => props.theme.colorRed};
`;

const Icon = styled.img`
  position: absolute;
  top: 3.5rem;
  right: 0.8rem;
  width: 1.7rem;
  height: 1.7rem;
  ${media.medium`
      right: -2.1rem;
    `};
`;

export const renderField = ({
  input,
  label,
  placeholder,
  type,
  disabled,
  pattern,
  meta: { touched, error, warning },
}) => (
  <InputWrap>
    <Label>{label}</Label>
    <Input
      pattern={pattern}
      disabled={disabled}
      placeholder={placeholder}
      warning={warning && touched && !error}
      valid={touched && !error && !warning}
      error={touched && error && error.length > 0}
      {...input}
      type={type}
    />
    {touched && (error && <Error>{error}</Error>)}
    {/* {touched &&
      (warning && <Error>{warning}</Error>)
    } */}
    {touched && !warning && (error && <Icon src={erroricon} alt="errormark" />)}
    {touched && !warning && (!error && <Icon src={checkicon} alt="checkmark" />)}
  </InputWrap>
);

renderField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  meta: PropTypes.object,
  disabled: PropTypes.bool,
  pattern: PropTypes.string,
};
