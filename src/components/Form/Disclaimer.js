import styled from 'styled-components';
import media from 'theme/styled-utils';

const Disclaimer = styled.p`
  margin: 2.5rem 0 0;
  font-size: 1.2rem;
  text-align: center;
  ${media.medium`
    margin: 2.0rem 0 0;
    font-size: 1.3rem;
  `}
`;

export default Disclaimer;
