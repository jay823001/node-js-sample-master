import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';
import Arrow from './caret-down.svg';

const Select = styled.select`
  font-size: 1.6rem;
  line-height: 1.4;
  font-weight: 500;
  width: 100%;
  margin: 0 0 1.7rem;
  padding: 1.2rem 1.0rem;
  border: .1rem solid #bdbdbd;
  border-radius: .5rem;
  cursor:pointer;
  background-color: white;
  -webkit-appearance: none;
  -moz-appearance: none;
  background-image: url(${Arrow});
  background-repeat: no-repeat;
  background-position: 89% center;
  background-size: 1.1rem 1.1rem;
  ${media.xsmall`
    background-position: 91% center;
  `}
  ${media.medium`
    background-position: 95% center;
  `}
  ${media.large`
    font-size: 1.4rem;
    line-height: 1.4;
  `}

  &:focus {
    border: .1rem solid ${props => props.theme.colorLightBlue};
    outline: none;
  }

  ${props =>
    props.error &&
    css`
      border: 1px solid ${props => props.theme.colorRed};
      &:focus {
        border: 1px solid ${props => props.theme.colorRed};
      }
    `}

  ${props =>
    props.valid &&
    css`
      border: 1px solid ${props => props.theme.colorGreen};
      &:focus {
        border: 1px solid ${props => props.theme.colorGreen};
      }
    `}
  
`;

export default Select;
