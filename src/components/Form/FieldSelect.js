import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Select from 'components/Form/Select';
import Label from 'components/Form/Label';
import checkicon from './checked.svg';
import erroricon from './exclamation.svg';

const InputWrap = styled.div`
  position: relative;
`;

const Error = styled.span`
  font-size: 1.2rem;
  position: absolute;
  bottom: 0;
  left: 0;
  color: ${props => props.theme.colorRed};
`;

const Icon = styled.img`
  position: absolute;
  top: 3.5rem;
  right: 0.8rem;
  width: 1.7rem;
  height: 1.7rem;
  ${media.medium`
      right: -1.9rem;
    `};
`;

export const renderFieldSelect = ({
  input,
  label,
  children,
  disabled,
  meta: { touched, error },
}) => (
  <InputWrap>
    <Label>{label}</Label>
    <Select
      disabled={disabled}
      valid={touched && !error}
      error={touched && error && error.length > 0}
      {...input}
    >
      {children}
    </Select>
    {touched && (error && <Error>{error}</Error>)}
    {touched && (error && <Icon src={erroricon} alt="errormark" />)}
    {touched && (!error && <Icon src={checkicon} alt="checkmark" />)}
  </InputWrap>
);

renderFieldSelect.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  children: PropTypes.node,
  meta: PropTypes.object,
  disabled: PropTypes.bool,
};
