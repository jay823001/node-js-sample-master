import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Img from '../Img';
import Google from './googleplay.png';
import Appstore from './appstore.png';
import enGoogle from './googleplay_en.png';
import enAppstore from './appstore_en.png';
import { internationalizationLanguage } from '../../containers/App/api';

const StyledImg = styled(Img)`
  width: 130px;
  height: auto;
  display: block;
  margin:0 0 1rem 0;
  ${media.small`
    display: inline-block;
    margin:0 1rem 0 0;
  `}
`;

const AppWrapper = styled.div`
  margin: 4rem 0 0 0;
`;

export default function AppBadge() {

  if (internationalizationLanguage === "FI") {
    return (
      <AppWrapper>
        <a href="https://play.google.com/store/apps/details?id=com.gtl.nextory" target="_blank" rel="noopener noreferrer">
          <StyledImg src={enGoogle} alt="Google Play" />
        </a>
        <a href="https://itunes.apple.com/us/app/nextory/id993578896?l=sv&amp;ls=1&amp;mt=8" target="_blank" rel="noopener noreferrer">
          <StyledImg src={enAppstore} alt="Appstore Play" />
        </a>
      </AppWrapper>
    );
  } else {
    return (
      <AppWrapper>
        <a href="https://play.google.com/store/apps/details?id=com.gtl.nextory" target="_blank" rel="noopener noreferrer">
          <StyledImg src={Google} alt="Google Play" />
        </a>
        <a href="https://itunes.apple.com/us/app/nextory/id993578896?l=sv&amp;ls=1&amp;mt=8" target="_blank" rel="noopener noreferrer">
          <StyledImg src={Appstore} alt="Appstore Play" />
        </a>
      </AppWrapper>
    );
  }

}
