import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import { closeDropdown } from 'containers/App/actions/app-actions';

const OverlayStyled = styled.div`
  position: fixed;
  z-index: 500;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: transparent;
  display: none;
  ${props => props.visibleMenu && css`
    display: block;
  `}
  ${props => props.visibleCat && css`
    display: block;
  `}
  ${props => props.visibleType && css`
    display: block;
  `}
  ${props => props.visibleSort && css`
    display: block;
  `}
  ${props => props.visibleSearch && css`
    display: block;
  `}
`;

class Overlay extends React.PureComponent {
  static propTypes = {
    Menu: PropTypes.bool,
    DropDownCat: PropTypes.bool,
    DropDownType: PropTypes.bool,
    DropDownSort: PropTypes.bool,
    DropDownSearch: PropTypes.bool,
    closeDropdown: PropTypes.func,
  };

  render() {
    const { closeDropdown, Menu, DropDownCat, DropDownType, DropDownSort, DropDownSearch } = this.props;

    return (
      <OverlayStyled
        onClick={closeDropdown}
        visibleMenu={Menu}
        visibleCat={DropDownCat}
        visibleType={DropDownType}
        visibleSort={DropDownSort}
        visibleSearch={DropDownSearch}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    Menu: state.appstate.MenuOpen,
    DropDownCat: state.appstate.DropDownCat,
    DropDownType: state.appstate.DropDownType,
    DropDownSort: state.appstate.DropDownSort,
    DropDownSearch: state.appstate.DropDownSearch,
  };
}

export default connect(mapStateToProps, { closeDropdown })(Overlay);
