import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

const H2 = styled.h2`
  font-size: 1.9rem;
  font-weight: 400;
  line-height: 1.2;
  margin: 0;
  &+p {
    margin-top: .5rem;
  }
  &+h4 {
    margin-top: 1.2rem;
    ${media.medium`
      margin-top: 2rem;
    `}
  }
  ${media.small`
    font-size: 2.3rem;
  `}

  ${props => props.huge && css`
  font-size: 1.3rem;
    ${media.xsmall`
      font-size: 1.5rem;
    `}
    ${media.medium`
      font-size: 2.7rem;
    `}
  `}

`;

export default H2;
