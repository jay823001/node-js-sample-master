import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

const P = styled.p`
  margin:0 0 1.5rem 0;
  font-size: 1.3rem;
  ${media.medium`
    margin:0 0 1.7rem 0;
    font-size: 1.5rem;
  `}
  +h2 {
    margin-top: 3rem;
  }
  +h3,+h4 {
    margin-top: 2.5rem;
  }

  ${props => props.ingress && css`
    font-size: 1.5rem;
    line-height: 1.4;
    ${media.medium`
      font-size: 1.7rem;
    `}
    ${media.large`
      font-size: 1.8rem;
    `}
  `}

`;

export default P;
