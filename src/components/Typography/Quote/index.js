import styled from 'styled-components';
import media from 'theme/styled-utils';

const Quote = styled.blockquote`
  margin: 1.2rem 0 2rem 0;
  padding-left: 2rem;
  display: block;
  font-style:italic;
  border-left: .3rem solid ${props => props.theme.colorBlue};
  ${media.medium`
    margin: 1.5rem 0 2.3rem 0;
    padding-left: 3rem;
    border-left: .5rem solid ${props => props.theme.colorBlue};
  `}
`;

export default Quote;
