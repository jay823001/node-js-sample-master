import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

const H1 = styled.h1`
  font-weight: 700;
  font-size: 2.5rem;
  margin: 0 0 .6rem 0;
  line-height: 1.1;
  & + h3 {
    margin-top: 1rem;
  }
  ${media.medium`
    font-size: 3.1rem;
    margin: 0 0 .7rem 0;
  `}

  ${props => props.huge && css`
    font-size: 3.4rem;
    margin-bottom: 2rem;
    ${media.medium`
      font-size: 5.2rem;
      margin-bottom: 2.3rem;
    `}
  `}

`;

export default H1;
