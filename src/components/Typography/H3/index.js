import styled from 'styled-components';
import media from 'theme/styled-utils';

const H3 = styled.h3`
  font-size: 1.6rem;
  font-weight: 600;
  line-height: 1.2;
  margin: 0 0 1.0rem 0;
  &+p {
    margin-top: -.5rem;
  }
  ${media.small`
    font-size: 2.1rem;
  `}
`;

export default H3;
