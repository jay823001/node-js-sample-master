import styled from 'styled-components';
import media from 'theme/styled-utils';

const H4 = styled.h4`
  line-height: 1.2;
  margin: 0 0 .5rem;
  font-weight: 600;
  font-size: 1.5rem;
  ${media.medium`
    font-size: 1.8rem;
  `}
  p+& {
    margin-top: 2rem;
    ${media.medium`
      margin-top: 2.5rem;
    `}
  }
`;

export default H4;
