import styled from 'styled-components';
import media from 'theme/styled-utils';

const OL = styled.ol`
  padding-left: 2rem;
  li {
    margin: 0 0 .4rem 0;
    ${media.medium`
      margin: 0 0 .6rem 0;
    `}
  }
`;

export default OL;
