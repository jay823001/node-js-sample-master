import styled from 'styled-components';

const A = styled.a`
  color: ${props => props.theme.colorBlue};
  &:hover {
    text-decoration:underline;
  }
`;

export default A;
