import styled, { css } from 'styled-components';
import Icons from './icon-both.svg';

const BookIcon = styled.aside`
  position: absolute;
  right: 0.2rem;
  bottom: 0.5rem;
  width: 6.1rem;
  height: 2.6rem;
  background-image: url(${Icons});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 6.1rem 2.6rem;
  ${props =>
    props.book &&
    css`
      width: 3rem;
      right: 0.4rem;
      background-position: -0.1rem center;
    `} ${props =>
    props.sound &&
    css`
      width: 3.1rem;
      background-position: -3rem center;
    `};
`;

export default BookIcon;
