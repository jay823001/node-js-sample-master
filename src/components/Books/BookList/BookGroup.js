import styled from 'styled-components';
import media from 'theme/styled-utils';

const BookGroup = styled.div`
  margin-bottom: 3rem;
  ${media.medium`
    margin-bottom: 4rem;
  `}
  ${media.large`
    margin-bottom: 6rem;
  `}
`;

export default BookGroup;
