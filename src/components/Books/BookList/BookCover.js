import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { smallCover } from 'containers/App/api';
import media from 'theme/styled-utils';
import { Link } from 'react-router-dom';
import Img from 'components/Img';
import BookIcon from 'components/Books/BookIcons';

const BookCover = styled(Link)`
  display: inline-block;
  position: relative;
  width: 8rem;
  height: 12.3rem;
  ${media.xsmall`
    width: 10rem;
    height: 15.4rem;
  `} ${media.medium`
    width: 12.6rem;
    height: 19.6rem;
    margin: .6rem 0;
  `}
  img {
    transition: all 0.1s ease;
    position: absolute;
    bottom: 0;
    pointer-events: none;
    left: 0;
    right: 0;
  }
  ${media.large`
    &:hover {
      img{
        transform: scale(1.02);
        box-shadow: 0 1px 3px rgba(0,0,0,.2);
      }
    }
  `};
`;

class Book extends React.PureComponent {
  static propTypes = {
    bookCover: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    relatedbook: PropTypes.number,
    formattype: PropTypes.number.isRequired,
    booktitle: PropTypes.string,
  };

  render() {
    const { url, bookCover, relatedbook, formattype, booktitle } = this.props;

    return (
      <BookCover to={url}>
        <Img alt={booktitle} src={smallCover + bookCover} />

        {relatedbook > 0 && <BookIcon />}
        {(relatedbook === 0 || !relatedbook) && formattype === 1 && <BookIcon book />}
        {(relatedbook === 0 || !relatedbook) && formattype === 2 && <BookIcon sound />}
      </BookCover>
    );
  }
}

export default Book;
