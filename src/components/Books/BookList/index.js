import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import media from 'theme/styled-utils';

const OuterWrapper = styled.section`
  background-color: ${props => props.theme.colorWhite};
  padding: 3rem 1.5rem;
  ${media.medium`
    padding: 4.5rem 1.5rem;
  `}
`;

const InnerWrapper = styled.div`
  max-width: 92rem;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
`;

class BooklistWrapper extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    return (
      <OuterWrapper>
        <InnerWrapper>
          {this.props.children}
        </InnerWrapper>
      </OuterWrapper>
    );
  }
}

export default BooklistWrapper;
