/**
 *
 * Img.react.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

function Img(props) {
  return (
    <img className={props.className} src={props.src} alt={props.alt} />
  );
}

const ImgNormal = styled(Img)`
  max-width: 100%;
  height: auto;
  user-select: none;
  border: 0;
  user-drag: none;
`;

// We require the use of src and alt, only enforced by react in dev mode
Img.propTypes = {
  src: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  alt: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default ImgNormal;
