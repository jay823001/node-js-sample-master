import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";
import media from "theme/styled-utils";
//import { GreyLogo } from './grey-logo';
import GreyLogo from "./grey-logo.svg";
import H3 from "../Typography/H3";
import FooterMenu from "./FooterMenu";
import InnerWrapper from "./InnerWrapper";
import Copyright from "./Copyright";
import Translation from "translation/nextory-web-se";
import { withNamespaces } from 'react-i18next';
import { getbFlowUrls } from 'newui/utils/helperFunctions'

const FooterStyled = styled.footer`
  background-color: ${props => props.theme.colorLightGrey};
  color: ${props => props.theme.colorDarkGrey};
  padding: 2.7rem 15px 3rem 15px;
  height: 36rem;
  ${media.medium`
    height: 32.5rem;
    padding: 4.5rem 15px 6rem 15px;
  `};
`;

const H3Styled = styled(H3)`
  font-weight: 400;
`;
const LogoImg = styled.img`
  width: 130px;
  height: 34px;
  display: block;
  ${media.medium`
    display: inline-block;
  `}
  polygon, path {
    fill: #888888;
  }
`;

class Footer extends React.PureComponent {
  static propTypes = {
    Route: PropTypes.string
  };

  render() {
    return (
      !this.props.Route.includes("register/subscription") &&
      !this.props.Route.includes("register/email") &&
      !this.props.Route.includes("register/card") &&
      !this.props.Route.includes("register-campaign/subscription") &&
      !this.props.Route.includes("register-campaign/email") &&
      !this.props.Route.includes("register-campaign/card") &&
      !this.props.Route.includes("/app/") &&
      this.props.Route !== "/" &&
      this.props.Route !== "/b" &&
      !this.props.Route.includes("/summer") &&
      !this.props.Route.includes(this.props.t('mypages.getStarted')) &&
      !this.props.Route.includes(this.props.t('mypages.userDetails')) &&
      !this.props.Route.includes(this.props.t('registration.campaignFlow.createAccount')) &&
      !this.props.Route.includes(this.props.t('mypages.reActivationFlow.subscriptionBasic')) &&
      !getbFlowUrls().includes(this.props.Route) && (
        <FooterStyled>
          <InnerWrapper>
            <H3Styled>{Translation.footer.heading}</H3Styled>
            <FooterMenu />
            <LogoImg src={GreyLogo} alt="Grey Logo at Footer" />
            <Copyright />
          </InnerWrapper>
        </FooterStyled>
      )
    );
  }
}

function mapStateToProps(state) {
  return {
    Route: state.route.location.pathname
  };
}

export default withNamespaces(['lang_route'])(connect(mapStateToProps)(Footer));
