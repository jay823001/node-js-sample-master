import React from 'react';
import styled from 'styled-components';
import media from 'theme/styled-utils';
import Translation from 'translation/nextory-web-se';

const Copy = styled.span`
  float: left;
  width: auto;
  margin-top: 5px;
  font-size: 1rem;
  ${media.medium`
    float: right;
  `}
`;

const year = new Date().getFullYear();

const Copyright = () => (
  <Copy>
    {Translation.footer.copyrights.copyright_text} {year} {Translation.footer.copyrights.address.p1}<br /> {Translation.footer.copyrights.address.p2}
  </Copy>
);

export default Copyright;
