import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import media from 'theme/styled-utils';

const StaticWrapper = styled.section`
  padding: 50px 0 60px;
  ${media.medium`
    padding: 90px 0 110px;
  `} ${props =>
    props.white &&
    css`
      background: white;
    `};
`;

const InnerWrapper = styled.section`
  max-width: 630px;
  padding: 0 1.5rem;
  margin: auto;
  ul {
    padding-left: 2rem;
    list-style-type: disc;
    li {
      margin: 0 0 0.4rem 0;
    }
  }
`;

class StaticpageWrapper extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    const white = this.props.white;

    return (
      <StaticWrapper white={white}>
        <InnerWrapper>{this.props.children}</InnerWrapper>
      </StaticWrapper>
    );
  }
}

export default StaticpageWrapper;
