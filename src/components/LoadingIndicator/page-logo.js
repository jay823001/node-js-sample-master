import React from 'react';
import styled from 'styled-components';
import Wrapper from './Wrapper';
import LoadingIcon from './LoadingIcon';

const LoadingWrapper = styled.div`
  min-height:100%;
  height:100vh;
  width:100%;
`;

const InnerWrapper = Wrapper.extend`
  width: 5rem;
  height: 5rem;
  position: absolute;
  top: 50%;
  right: 0;
  bottom: auto;
  left: 0;
  margin: auto;
  transform: translateY(-55%);
`;

const LoadingIndicatorPageLogo = () => (
  <LoadingWrapper>
    <InnerWrapper>
      <LoadingIcon />
    </InnerWrapper>
  </LoadingWrapper>
);

export default LoadingIndicatorPageLogo;
