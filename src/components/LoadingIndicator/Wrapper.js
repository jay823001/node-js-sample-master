import styled, { css } from 'styled-components';

const Wrapper = styled.div`
  margin: 2em auto;
  width: 40px;
  height: 40px;
  position: relative;
  ${props => props.small && css`
    display: inline-block;
    height: 2.3rem;
    width: 2.3rem;
    position: relative;
    margin: auto auto -.5rem auto;
  `}
  ${props => props.input && css`
    position: absolute;
    right: 1rem;
    z-index: 100;
    top: 0.7rem;
  `}
`;

export default Wrapper;
