import React from 'react';
import styled, { keyframes } from 'styled-components';
import logo from './n-logo.svg';

const circleFadeDelay = keyframes`
 0% {
  -webkit-transform: rotate(0deg) scale(1);
  }
  50% { 
  -webkit-transform: rotate(180deg) scale(1.2);
  }
  100% { 
  -webkit-transform: rotate(360deg) scale(1);
  }
`;

const Loading = styled.img`
  -webkit-animation-name:             rotate; 
  -webkit-animation-iteration-count:  infinite;
  -webkit-animation-timing-function: ease-in-out;
  animation: ${circleFadeDelay} 1.3s infinite ease-in-out both;
`;

const LoadingIcon = () => (
  <Loading src={logo} alt="loading icon" />
);

export default LoadingIcon;
