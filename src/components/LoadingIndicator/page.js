import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import H3 from "components/Typography/H3";
import Circle from "./Circle";
import Wrapper from "./Wrapper";

const LoadingWrapper = styled.div`
  min-height: 100%;
  height: 100vh;
  width: 100%;
`;

const InnerWrapper = Wrapper.extend`
  width: 5rem;
  height: 5rem;
  position: absolute;
  top: 50%;
  right: 0;
  bottom: auto;
  left: 0;
  margin: auto;
  transform: translateY(-55%);
`;

const H3Load = H3.extend`
  max-width: 30rem;
  position: absolute;
  top: 43%;
  text-align: center;
  right: 0;
  bottom: auto;
  left: 0;
  margin: auto;
  transform: translateY(-55%);
`;

class LoadingIndicatorPage extends React.PureComponent {
  static propTypes = {
    title: PropTypes.string
  };

  render() {
    return (
      <LoadingWrapper>
        {this.props.title && <H3Load>{this.props.title}</H3Load>}
        <InnerWrapper>
          <Circle />
          <Circle rotate={30} delay={-1.1} />
          <Circle rotate={60} delay={-1} />
          <Circle rotate={90} delay={-0.9} />
          <Circle rotate={120} delay={-0.8} />
          <Circle rotate={150} delay={-0.7} />
          <Circle rotate={180} delay={-0.6} />
          <Circle rotate={210} delay={-0.5} />
          <Circle rotate={240} delay={-0.4} />
          <Circle rotate={270} delay={-0.3} />
          <Circle rotate={300} delay={-0.2} />
          <Circle rotate={330} delay={-0.1} />
        </InnerWrapper>
      </LoadingWrapper>
    );
  }
}

export default LoadingIndicatorPage;
