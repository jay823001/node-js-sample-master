import React from 'react';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import media from 'theme/styled-utils';


const Button = styled(({ primary, small, framed, large, red, sticky, ...rest }) => <Link {...rest} />)`
  font-size: 1.4rem;
  font-family: "Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
  font-weight: 500;
  display: inline-block;
  padding: .7rem 1.6rem .8rem;
  transition: all .15s ease;
  color: white;
  border: .1rem solid ${props => props.theme.colorBlue};
  border-radius: .3rem;
  background-color: ${props => props.theme.colorBlue};
  box-shadow: 0 .1rem .1rem rgba(0,0,0,.5);
  cursor:pointer;
  line-height: 1.525;
  ${media.medium`
    font-size: 1.8rem;
    padding: .8rem 2.3rem .9rem;
  `}
  &:hover {
    background-color: ${props => props.theme.colorHoverBlue};
  }
  &:focus {
    outline:none;
  }


  ${props => props.primary && css`
    letter-spacing: .1rem;
    text-transform: uppercase;
    padding: 1.3rem 2.2rem;
    ${media.medium`
      padding: 1.4rem 2.4rem;
      font-size: 1.8rem;
    `}
  `}


  ${props => props.framed && css`
    color: ${props => props.theme.colorBlue};
    border: .1rem solid ${props => props.theme.colorGrey};
    background-color: transparent;
    box-shadow:none;
    padding: .5rem 1.6rem .7rem;
    ${media.small`
      padding: .7rem 2.3rem .9rem;
    `}
    &:hover {
      text-decoration: underline;
      background-color: transparent;
      color: ${props => props.theme.colorBlue};
    }
  `}

  
  ${props => props.small && css`
    font-size: 1.3rem;
    padding: .7rem 1.8rem .8rem;
    text-transform: none;
    ${media.small`
      font-size: 1.4rem;
      padding: .8rem 2rem 1rem;
    `}
  `}

  ${props => props.red && css`
    background-color: ${props => props.theme.colorRed};  
    border: .1rem solid ${props => props.theme.colorRed};
    &:hover {
      background-color: #f93e3e;  
      border: .1rem solid #f93e3e;
    }
  `}


  ${props => props.large && css`
    width: 100%;
    max-width: 36rem;
    text-align: center;
    padding: 1.3rem 2.0rem;
    font-size: 1.8rem;
    letter-spacing: 0.02rem;
  `}

  
`;

export default Button;
