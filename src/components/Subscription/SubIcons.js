import React from 'react';
import styled from 'styled-components';

const Cross = styled.svg`
  width: .9rem;
  height: .9rem;
`;

const Check = styled.svg`
  width: 1.1rem;
  height: 1.1rem;
`;

const TickNew = styled.svg`
  margin-top: 2px;
  width: 2.45rem;
  height: 2.45rem;
`;

const CrossNew = styled.svg`
  width: 2.4rem;
  height: 2.4rem;
`;
const Dot = styled.svg`
  width: 2.4rem;
  height: 2.4rem;
  fill:#585858;
`;

const CrossNew2 = styled.svg`
  width: 1.2rem;
  height: 1.2rem;
`;

const TickNew2 = styled.svg`
  width: 1.4rem;
  height: 1.4rem;
`;

const Bullet = styled.svg`
  width: 1.2rem;
  height: 1.2rem;
`;

export const SubCrossNew2 = () => (
  <CrossNew2 viewBox="0 0 18 18">
    <g transform="translate(-3.000000, -3.000000)">
      <path d="M13.4141,12 L20.7071,4.707 C21.0981,4.316 21.0981,3.684 20.7071,3.293 C20.3161,2.902 19.6841,2.902 19.2931,3.293 L12.0001,10.586 L4.7071,3.293 C4.3161,2.902 3.6841,2.902 3.2931,3.293 C2.9021,3.684 2.9021,4.316 3.2931,4.707 L10.5861,12 L3.2931,19.293 C2.9021,19.684 2.9021,20.316 3.2931,20.707 C3.4881,20.902 3.7441,21 4.0001,21 C4.2561,21 4.5121,20.902 4.7071,20.707 L12.0001,13.414 L19.2931,20.707 C19.4881,20.902 19.7441,21 20.0001,21 C20.2561,21 20.5121,20.902 20.7071,20.707 C21.0981,20.316 21.0981,19.684 20.7071,19.293 L13.4141,12 Z" id="Fill-1"></path>
    </g>
  </CrossNew2>
);

export const SubTickNew2 = () => (
  <TickNew2 viewBox="0 0 14 18">
    <g transform="translate(-5.000000, -3.000000)">
      <path d="M10.2571,20.9995 C9.9861,20.9995 9.7251,20.8905 9.5341,20.6905 L5.2781,16.2465 C4.8961,15.8485 4.9091,15.2155 5.3091,14.8325 C5.7061,14.4505 6.3391,14.4645 6.7231,14.8635 L9.9841,18.2685 L17.1001,3.5635 C17.3401,3.0685 17.9361,2.8595 18.4361,3.0995 C18.9331,3.3405 19.1411,3.9375 18.9011,4.4355 L11.1571,20.4355 C11.0151,20.7295 10.7361,20.9365 10.4131,20.9865 C10.3611,20.9955 10.3091,20.9995 10.2571,20.9995" id="Fill-1"></path>
    </g>
  </TickNew2>
);

export const SubBullet = () => (
  <Bullet viewBox="0 0 80 80">
    <g>
      <circle cx="40" cy="40" r="40"/>
    </g>
  </Bullet>
);

export const SubCheck = () => (
  <Check viewBox="0 0 20 20">
    <path d="M20,5.3c0,0.3-0.1,0.6-0.4,0.9l-9.3,9.3l-1.8,1.8c-0.2,0.2-0.5,0.4-0.9,0.4S7,17.5,6.8,17.3L5,15.5l-4.7-4.7 C0.1,10.6,0,10.3,0,10s0.1-0.6,0.4-0.9l1.8-1.8C2.4,7.1,2.6,7,3,7s0.6,0.1,0.9,0.4l3.8,3.8l8.5-8.5c0.2-0.2,0.5-0.4,0.9-0.4c0.3,0,0.6,0.1,0.9,0.4l1.8,1.8C19.9,4.7,20,5,20,5.3z"></path>
  </Check>
);

export const SubCross = () => (
  <Cross viewBox="0 0 20 20">
    <path d="M20,16.1c0,0.4-0.2,0.8-0.5,1.1l-2.3,2.3c-0.3,0.3-0.7,0.5-1.1,0.5c-0.4,0-0.8-0.2-1.1-0.5L10,14.6l-4.9,4.9C4.7,19.8,4.4,20,3.9,20c-0.4,0-0.8-0.2-1.1-0.5l-2.3-2.3C0.2,16.9,0,16.5,0,16.1c0-0.4,0.2-0.8,0.5-1.1L5.4,10L0.5,5.1C0.2,4.7,0,4.4,0,3.9s0.2-0.8,0.5-1.1l2.3-2.3C3.1,0.2,3.5,0,3.9,0c0.4,0,0.8,0.2,1.1,0.5L10,5.4l4.9-4.9C15.3,0.2,15.6,0,16.1,0c0.4,0,0.8,0.2,1.1,0.5l2.3,2.3C19.8,3.1,20,3.5,20,3.9s-0.2,0.8-0.5,1.1L14.6,10l4.9,4.9C19.8,15.3,20,15.6,20,16.1z"></path>
  </Cross>
);

export const SubTickNew = () => (
  <TickNew xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        <circle cx="11" cy="11" r="11" />
        <path fill="#FFF" fillRule="nonzero" d="M10.198 15.917a.46.46 0 0 1-.33-.13l-2.72-2.45a.5.5 0 0 1 0-.71.5.5 0 0 1 .7 0l2.23 2 4-8.39a.5.5 0 0 1 .9.42l-4.28 9a.46.46 0 0 1-.35.28l-.15-.02z" ></path>
  </TickNew>
);

export const SubCrossNew = () => (
  <CrossNew xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        <circle cx="11" cy="11" r="11" transform="translate(1 1)"/>
        <rect width="12" height="1" x="6" y="12" fill="#FFF" fillRule="nonzero" rx=".5" transform="rotate(-45 12 12.5)"/>
        <rect width="12" height="1" x="6" y="12" fill="#FFF" fillRule="nonzero" rx=".5" transform="rotate(45 12 12.5)"/>
  </CrossNew>
);


export const BlackDot = () => (
    <Dot xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"> 
    <g fill="none" fillRule="evenodd" transform="translate(6 6)">
      <circle cx="5" cy="5" r="5" fill="#585858"/>
      <path d="M4.635 7.235a.21.21 0 0 1-.15-.059L3.25 6.062a.227.227 0 0 1 0-.322.227.227 0 0 1 .318 0l1.014.909 1.818-3.814a.227.227 0 0 1 .41.191l-1.946 4.09a.21.21 0 0 1-.16.128l-.068-.009z" > </path>
      </g>
    </Dot>
);
