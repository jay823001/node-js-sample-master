/**
 * Combine all  Redux sagas in this file and export the combined global saga.
 */
import { fork, all } from 'redux-saga/effects';
import { bookSaga, ebookSaga, sbookSaga, categorySaga } from './containers/Views/Books/saga';
import {
  createUserSaga,
  createUserLogoutSaga,
  regPaymentSaga,
} from './containers/Views/Registration/saga';
import { userCampaignSaga } from './containers/Views/Campaign/saga';
import { loginUserSaga } from './containers/Views/Account/LoginPage/saga';
import { giftloginUserSaga } from './containers/Views/GiftcardNew/GiftcardRedeem/GiftcardInlosenPage/saga';
import {
  logoutSaga,
  refreshStoreSaga,
  refreshAuthSaga,
} from './containers/Views/Account/MyAccount/saga';
import {
  updateProfileSaga,
  updateSubscriptionSaga,
  forgotPassSaga,
  changepassFlowSaga,
} from './containers/Views/Account/saga';
import { loginTrustlySaga } from './containers/Views/Registration/Trustly/saga';
import { loginAppSaga } from './containers/Views/Account/FromApp/saga';
import { searchSaga, searchMoreSaga } from './containers/Search/saga';

// NEWUI SAGAS
import { userRegisterSaga } from './newui/domain/Sagas/registration-saga'
import { subscriptionDetailSaga } from './newui/domain/Sagas/checkoutOptions-saga'
import { createPaymentRequestSaga } from './newui/domain/Sagas/checkout-saga'
import { userDetailSaga, fetchUserDetailSaga, activateAgainSaga } from './newui/domain/Sagas/user-saga'
import { refreshAuthTokenSaga } from './newui/domain/Sagas/refreshAuth-saga'
import { logoutFlowSaga } from './newui/domain/Sagas/logout-saga'
import { bookInfoSaga } from './newui/domain/Sagas/bookinfo-saga'
import { bookGroupSaga } from './newui/domain/Sagas/bookgroup-saga'
import { bookSearchSaga } from './newui/domain/Sagas/booksearch-saga'
import { campaignRegisterSaga, campaignDetailsSaga } from './newui/domain/Sagas/campaign-registration-saga'
import { cancelSubscriptionSaga } from './newui/domain/Sagas/subscription-cancel-saga'
import { ordersSaga, orderReceiptSaga } from './newui/domain/Sagas/orders-saga'
import { offersSaga } from './newui/domain/Sagas/offers-saga'
import { updateSubscriptionNewSaga } from './newui/domain/Sagas/subscription-update-saga'
import { loginSaga } from './newui/domain/Sagas/login-saga'
import { forgotPasswordSaga } from './newui/domain/Sagas/forgot-password-saga'
import { updateUserStoreSaga } from './newui/domain/Sagas/store-saga'
import { paymentResponseSaga } from './newui/domain/Sagas/paymentResponse-saga'
import { resetPasswordSaga } from './newui/domain/Sagas/resetPassword-saga'
import { cmsSaga } from './newui/domain/Sagas/cms-saga'
// END NEWUI SAGAS

const sagas = [
  giftloginUserSaga,
  loginUserSaga,
  bookSaga,
  ebookSaga,
  sbookSaga,
  categorySaga,
  updateProfileSaga,
  updateSubscriptionSaga,
  logoutSaga,
  forgotPassSaga,
  changepassFlowSaga,
  createUserSaga,
  createUserLogoutSaga,
  regPaymentSaga,
  userCampaignSaga,
  refreshStoreSaga,
  refreshAuthSaga,
  loginTrustlySaga,
  loginAppSaga,
  searchSaga,
  searchMoreSaga,
  // NOTE: put other app sagas here

  // NEWUI SAGAS
  userRegisterSaga,
  subscriptionDetailSaga,
  createPaymentRequestSaga,
  userDetailSaga,
  refreshAuthTokenSaga,
  logoutFlowSaga,
  bookInfoSaga,
  bookGroupSaga,
  bookSearchSaga,
  campaignDetailsSaga,
  campaignRegisterSaga,
  campaignDetailsSaga,
  cancelSubscriptionSaga,
  ordersSaga,
  orderReceiptSaga,
  offersSaga,
  updateSubscriptionNewSaga,
  loginSaga,
  forgotPasswordSaga,
  updateUserStoreSaga,
  paymentResponseSaga,
  resetPasswordSaga,
  cmsSaga,
  fetchUserDetailSaga,
  activateAgainSaga
  // END NEWUI SAGAS
];

function* globalSagas() {
  const globalSagasForks = sagas.map(saga => fork(saga));
  yield all([...globalSagasForks]);
}

export default globalSagas;
