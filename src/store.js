import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware } from "react-router-redux";
import {
  persistStore,
  persistReducer
} from "redux-persist";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import createSagaMiddleware from "redux-saga";
import createHistory from "history/createBrowserHistory";
import rootReducer from "./reducers";
import globalSagas from "./sagas";

export const history = createHistory();

const sagaMiddleware = createSagaMiddleware();

const enhancers = [];

const middleware = [
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  sagaMiddleware,
  routerMiddleware(history)
];

const reducer = persistReducer(
  {
    key: "root",
    storage: storage,
    stateReconciler: autoMergeLevel2,
    whitelist: ["selectedSubscription", "authKeys", "campaignInfo", "userDetails"]
  },
  rootReducer
);

if (process.env.NODE_ENV === "development") {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

export const store = createStore(reducer, {}, composedEnhancers);
export const persistor = persistStore(store);

sagaMiddleware.run(globalSagas);
