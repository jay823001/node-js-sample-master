##
# Put this file in /etc/nginx/conf.d folder and make sure
# you have a line 'include /etc/nginx/conf.d/*.conf;'
# in your main nginx configuration file
##
server {

  listen 80;
  listen 443 ssl;

  ##
  # Server, root and index
  ##
  server_name www.nextory.se;
  root /var/www/nextory.se/build/;
  index index.html;

  if ($host = 'nextory.se') {
    return 301 https://www.nextory.se$request_uri;
  }

  if ($uri = '/bocker/virekommenderar') {
    return 301 https://www.nextory.se/bocker/virekommenderar/;
  }
  if ($query_string ~ 'harry%2520potter') {
    return 301 'https://www.nextory.se/search#harry potter';
  }
  ##
  # Sitemap and robots
  ##
  location /sitemap.xml {
    root   /var/www/nextory.se/sitemap/;
  }
  location /views_sitemap.xml {
    root   /var/www/nextory.se/sitemap/;
  }
  location /categories_sitemap.xml {
    root   /var/www/nextory.se/sitemap/;
  }
  location /books_sitemap.xml {
    root   /var/www/nextory.se/sitemap/;
  }
  location /robots.txt {
    root   /var/www/nextory.se/robots/;
  }

  ##
  # Browser cache
  ##
  location / {
    if ($request_uri ~* ".(jpg|jpeg|gif|gz|zip|flv|rar|wmv|avi|css|js|swf|png|htc|ico|mpeg|mpg|txt|mp3|mov|woff)(\?v=[0-9.]+)?$") {
      expires 30d;
      access_log off;
      break;
    }
    try_files $uri @prerender;
  }
  
  ##
  # Prerender settings
  #
  location @prerender {
    proxy_set_header X-Prerender-Token UBHfwjwIupItD8MoXAY6;
    
    set $prerender 0;
    if ($http_user_agent ~* "googlebot|baiduspider|twitterbot|facebookexternalhit|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator") {
      set $prerender 1;
    }
    if ($args ~ "_escaped_fragment_") {
      set $prerender 1;
    }
    if ($http_user_agent ~ "Prerender") {
      set $prerender 0;
    }
    if ($uri ~* "\.(js|css|xml|less|png|jpg|jpeg|gif|pdf|doc|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff|svg|eot)") {
      set $prerender 0;
    }

    #resolve using Google's DNS server to force DNS resolution and prevent caching of IPs
    resolver 8.8.8.8;

    if ($prerender = 1) {
      #setting prerender as a variable forces DNS resolution since nginx caches IPs and doesnt play well with load balancing
      set $prerender "service.prerender.io";
      rewrite .* /https://$host$request_uri? break;
      proxy_pass http://$prerender;
    }
    if ($prerender = 0) {
      rewrite .* /index.html break;
    }
  }
}

